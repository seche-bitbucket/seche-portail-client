window.PFAS_Dict = (function() {
    return {
        getDict: function() {
            return {
                'fr' : {
                    'not_found' : 'Cette page n\'existe pas',
                    'progress_message' : 'Enregistrement des compléments de FIPs et Substances...',
                    'step_1_name': 'Étape 1',
                    'step_1_title': 'Votre Identité',
                    'step_2_name': 'Étape 2',
                    'step_2_title': 'Données CAP',
                    'step_3_name': 'Étape 3',
                    'step_3_title': 'Validation',
                    'error_max_min': 'La valeur du %Minimum ne doit pas être supérieur au %Maximum.',
                    'error_unfinished': 'Veuillez remplir tous les champs de vos compléments de FIPs.',
                    'error_saving': 'Erreur lors de l\'enregistrement des compléments de FIPs!',
                    'saving_message': 'Envoi de vos documents pour signature...',
                    'saved_message': 'Vos données ont bien été sauvegardées !',
                    'nocap_message': 'Vous n\'avez aucun CAP à compléter ou vous avez déjà signé vos compléments FIP.',
                    'finished_message': 'Vous allez recevoir un email pour signer électroniquement votre complément de FIP',
                    'info_message': 'Si vous constatez avoir saisi une erreur, vous pourrez dans l\'espace de signature appuyer sur \'Rejeter\' pour accéder à nouveau à votre complément FIP dans le mail d\'origine.',
                    'nextStep' : 'Étape suivante',
                    'previousStep' : 'Étape précédente',
                    'saveButton' : 'Enregistrer',
                    'sendButton' : 'Valider et terminer',
                    'substanceNameHelpText' : 'Si vous avez plusieurs polymères à déclarer, veuillez les séparer par des virgules.',
                    'substanceTypeLabel' : 'Type de substance',
                    'helpTextPoly' : 'P si Polymère / NP si Non-Polymère.',
                    'substanceName' : 'Nom des substances',
                    'CASNumber' : 'N°CAS',
                    'noSubstance' : 'Pas de substance',
                    'chooseSubstance' : 'Choisir une substance'
                },
                'en_US' : {
                    'not_found' : 'Page not found',
                    'progress_message' : 'Saving FIPs Complements and Substances...',
                    'step_1_name': 'Step 1',
                    'step_1_title': 'Personal Information',
                    'step_2_name': 'Step 2',
                    'step_2_title': 'CAP Data',
                    'step_3_name': 'Step 3',
                    'step_3_title': 'Summary & Validation',
                    'error_max_min': '%Minimum value cannot be higher than %Maximum.',
                    'error_unfinished': 'You must fill all your FIPs Complements Fields.',
                    'error_saving': 'Error while saving your data!',
                    'saving_message': 'Sending your documents for signature...',
                    'saved_message': 'All tour data have been saved successfully !',
                    'nocap_message': 'You have no CAP to complete, or you have already signed your FIP supplements.',
                    'finished_message': 'You will receive an email to electronically sign your FIP supplement',
                    'info_message': 'If you realize you have entered an error, you can press \'Reject\' in the signature space to access your FIP supplement again in the original email.',
                    'nextStep' : 'Next Step',
                    'previousStep' : 'Previous Step',
                    'saveButton' : 'Save',
                    'sendButton' : 'Validate and Finish',
                    'substanceNameHelpText' : 'If you have more than one polymer to declare, please use commas to separate them.',
                    'substanceTypeLabel' : 'Substance type',
                    'helpTextPoly' : 'P if Polymer / NP if Non-Polymer.',
                    'substanceName' : 'Substances name',
                    'CASNumber' : 'CAS Number',
                    'noSubstance' : 'No substance',
                    'chooseSubstance' : 'Choose a substance'
                }
            }
        }
    };
}());
