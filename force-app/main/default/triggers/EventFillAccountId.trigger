trigger EventFillAccountId on Event (before insert, before update) {
    
    // Permet de déterminer le type d'objet associé au WhatId de l'event
    Map<String, String> s2o = new Map<String, String>();
    Map<String, Schema.SObjectType> schemas = Schema.getGlobalDescribe();
    Set<String> keys = schemas.keySet();
    
    for (String key : keys) {
        Schema.SObjectType schema = schemas.get(key);
        Schema.DescribeSObjectResult d = schema.getDescribe();
        s2o.put(d.getKeyPrefix(), d.getName());
    }
    
    for (Event e : Trigger.New)
    {
        if (e.WhatId != null)
        {
            String prefixType = ((String)(e.WhatId)).substring(0,3);
            String eventType = s2o.get(prefixType);
            if (eventType.equals('VisitReport__c'))
            {
                VisitReport__c vr = [Select Id, Account__c From VisitReport__c Where Id =: e.WhatId];
                e.Compte__c = vr.Account__c;               
            }
            else if (eventType.equals('Case'))
            {
                Case ca = [Select Id, AccountId From Case Where Id =: e.WhatId];
                e.Compte__c = ca.AccountId;
            }
            else if (eventType.equals('Opportunity'))
            {
                Opportunity opp = [Select Id, AccountId From Opportunity Where Id =: e.WhatId];
                e.Compte__c = opp.AccountId;
            }
            else if (eventType.equals('Account'))
            {
                e.Compte__c = e.WhatId;
            }
        }
        else if (e.WhoId != null)
        {
            String prefixType = ((String)(e.WhoId)).substring(0,3);
            String eventType = s2o.get(prefixType);
            if (eventType.equals('Contact'))
            {
                Contact c = [Select Id, AccountId From Contact Where Id =: e.WhoId];
                e.Compte__c = c.AccountId;
            }
        }
    }
}