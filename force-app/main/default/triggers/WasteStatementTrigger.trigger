trigger WasteStatementTrigger on WST_Waste_Statement__c (before insert, after insert, before update) {

    
    if(Trigger.IsBefore){
        If(Trigger.isInsert){
            wasteStatementTriggerUtil.manageWSTType(Trigger.New);
            wasteStatementTriggerUtil.manageStateStatus(Trigger.New);
            wasteStatementTriggerUtil.assignSampleNumber(Trigger.New);
        }else{
            //wasteStatementTriggerUtil.managePricebookBeforeUpdate(Trigger.New);
            wasteStatementTriggerUtil.manageStateStatus(Trigger.New);
            wasteStatementTriggerUtil.manageQuoteStatus(Trigger.New);
        }
    }else{
        If(Trigger.isInsert){
            wasteStatementTriggerUtil.managePricebookAfterInsert(Trigger.New);
        }else{
            
        }
    }
    
}