/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ContentDocumentTrigger
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ContentDocumentTrigger trigger
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 23-03-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
trigger ContentDocumentTrigger on ContentDocument (before delete) {
    if(Trigger.IsBefore && Trigger.isDelete){
        ContentDocumentTriggerHandler.onBeforeDelete(Trigger.newMap,Trigger.oldMap);
    }
}