trigger CDLTrigger on ContentDocumentLink (after insert) {

    for (ContentDocumentLink cdl : trigger.new) {

        if(cdl.LinkedEntityId.getSObjectType() == FIP_FIP__c.sObjectType){
           //ConnectiveCallout.sendCreatedFlow(cdl.LinkedEntityId);
           ConnectiveV4ApiManager.createInstantPackageFIPTrigger(cdl.LinkedEntityId);
        }
        
    }

      /* if(TRIGGER.isDelete){
        if(TRIGGER.isBefore){
            AttachmentTriggerUtil.checkApprouvedQuoteFiles(TRIGGER.OLD);
        }
    } */
}