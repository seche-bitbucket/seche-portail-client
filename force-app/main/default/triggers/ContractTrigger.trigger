/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-10-25
* @modified       2018-10-25
* @systemLayer    Trigger         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Trigger on contract object
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
trigger ContractTrigger on Contract__c (before update, after update) {
    Boolean byPass = ByPassUtils.isByPassed('ContractTrigger') != null ? ByPassUtils.isByPassed('ContractTrigger') : false ;
    if(!byPass){

        if(Trigger.isUpdate && Trigger.isBefore) {
            ContractTriggerHandler.ContractUpdate(Trigger.new,Trigger.oldMap);
        }

        if(Trigger.isUpdate && Trigger.isAfter) {
            List<Contract__c> objs = Trigger.new;
            List<Contract__c> contracts = Database.query('SELECT '+String.join(new List<String>(Schema.getGlobalDescribe().get('Contract__c').getDescribe().fields.getMap().keySet()), ', ')+', ContactRelated__r.Actif__c, Account__r.Name, AssociatedSalesRep__r.Trigramme__c, (SELECT '+String.join(new List<String>(Schema.getGlobalDescribe().get('Amendment__c').getDescribe().fields.getMap().keySet()), ', ')+' FROM Avenants__r) FROM Contract__c WHERE Id IN :objs');
            System.debug(contracts.size());
            ContractTriggerHandler.ContractConvention(contracts);
        }

    }   
}