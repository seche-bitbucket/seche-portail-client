/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Lucas DORMOY <lucas.dormoy@capgemini.com>
 * @version        1.0
 * @created        2020-12-131
 * @systemLayer    Manager
 *
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Description: This class contains the trigger for Questionnaire Lycée
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

trigger QLYTrigger on QLY_SchoolForm__c (before update, after update) {
	if(Trigger.isUpdate && Trigger.isBefore){
            QLYTriggerHandler.onBeforeUpdate(Trigger.oldMap, Trigger.newMap);
    }
	if(Trigger.isUpdate && Trigger.isAfter){
            QLYTriggerHandler.onAfterUpdate(Trigger.oldMap, Trigger.newMap);
    }
}