trigger OpportunityLineItemTrigger on OpportunityLineItem (before insert , after update, after insert) {
    
    private Set<ID> lstProduct = new Set<ID>();
    
    Boolean byPass = ByPassUtils.isByPassed('OpportunityLineItemTrigger') != null ? ByPassUtils.isByPassed('OpportunityLineItemTrigger') : false ;
    if(!byPass){
        System.debug(Trigger.New.size());

        /*if(Trigger.IsInsert && Trigger.IsAfter) {
            
            system.debug('isAfterInsert');
            //OppLineItemSyncTriggerV2Handler.execute(Trigger.new, null);
            OppLineItemSyncTriggerV2Handler.copy(Trigger.new);
            return;
            
        } else*/ if(Trigger.IsUpdate && Trigger.IsAfter) {
            system.debug('isAfterUpdate');
            OppLineItemSyncTriggerV2Handler.execute(Trigger.new, Trigger.oldMap);
            
        }
        
        if(Trigger.isBefore && Trigger.isInsert) {
            
            system.debug('isBeforeInsert');

            for(OpportunityLineItem oli : Trigger.New){
                
                lstProduct.add(oli.Product2Id);
            }
            
            Map<ID, Product2> mapProduct = new Map<ID, Product2>();
            
            mapProduct = new Map<ID, Product2>([SELECT Id, Name, Classe__c, EuropeanCode__c, 	Features__c, GE__c, IncludedTGAP__c,
                                                                Label__c, Family, Container__c, TransportType__c, Treatment__c, UNCode__c, Unit__c
                                                                FROM Product2 WHERE ID in : lstProduct ]);
            
            System.debug(mapProduct);
            
            for(OpportunityLineItem oli : Trigger.New){
                
                Product2 prd = mapProduct.get(oli.Product2Id);
                
                try{
                    if(oli.Classe__c == null){
                        oli.Classe__c = prd.Classe__c;
                    }
                    
                    if(oli.TransportType__c == null){
                        oli.TransportType__c = prd.TransportType__c;
                    }
                    
                    if(oli.Treatment__c == null){
                        oli.Treatment__c = prd.Treatment__c;
                    }
            
                    if(oli.Classe__c == null){
                        oli.Classe__c = prd.Classe__c;
                    }
            
                    if(oli.EuropeanCode__c == null){
                        oli.EuropeanCode__c = prd.EuropeanCode__c;
                    }
            
                    if(oli.Features__c == null){
                        oli.Features__c = prd.Features__c;
                    }
            
                    if(oli.GE__c == null){
                        oli.GE__c = prd.GE__c;
                    }
                            
                    if(oli.IncludedTGAP__c == null){
                        oli.IncludedTGAP__c = prd.IncludedTGAP__c;
                    }
                    
                    if(oli.Label__c == null){
                        oli.Label__c = prd.Label__c;
                    }
                            
                    if(oli.Nature__c == null){
                        oli.Nature__c = prd.Family;
                    }
                    
                    if(oli.Packaging__c == null){
                        oli.Packaging__c = prd.Container__c;
                    }
                    
                    if(oli.TransportType__c == null){
                        oli.TransportType__c = prd.TransportType__c;
                    }
                    
                    if(oli.Treatment__c == null){
                        oli.Treatment__c = prd.Treatment__c;
                    }
            
                    if(oli.UNCode__c == null){
                        oli.UNCode__c = prd.UNCode__c;
                    }
                    
                    if(oli.Unit__c == null){
                        oli.Unit__c = prd.Unit__c;
                    }
                } catch(Exception e) {
                    System.debug(e.getMessage()+', at line '+e.getLineNumber());
                }
                        
            }
        }
    }
    
}