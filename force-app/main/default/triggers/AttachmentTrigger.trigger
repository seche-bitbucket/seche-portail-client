trigger AttachmentTrigger on Attachment (before delete) {

    if(TRIGGER.isDelete){
        if(TRIGGER.isBefore){
            AttachmentTriggerUtil.checkApprouvedQuoteDeletion(TRIGGER.OLD);
        }
    }
    
}