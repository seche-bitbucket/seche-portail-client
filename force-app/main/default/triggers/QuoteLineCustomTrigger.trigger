trigger QuoteLineCustomTrigger on QuoteLine__c (before insert, before update) {

	Set<ID> PbeID = new Set<ID>();
    List<PricebookEntry> pbes = new List<PricebookEntry>();
    Map<ID, PricebookEntry> mapPbe = new Map<ID, PricebookEntry>();
    
    for(QuoteLine__c ql : Trigger.New){
        PbeID.add(ql.TECH_PriceBookEntryID__c);
    }
    
    pbes = [Select ID, UnitPrice,Product2Id FROM PricebookEntry WHERE ID in : PbeID];
    for(PricebookEntry pbe : pbes){
        mapPbe.put(pbe.ID, pbe);
    }
    
    
    for(QuoteLine__c ql : Trigger.New){
        
        
         
        if(ql.Code__c != 'DEFAUT' && ql.Code__c != null){
            ql.TECH_PriceBookEntryPrice__c = mapPbe.get(ql.TECH_PriceBookEntryID__c).UnitPrice;
        }else if(ql.Code__c != null){
            ql.TECH_PriceBookEntryPrice__c = 0;
        }
        
    }

}