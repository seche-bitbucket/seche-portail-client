/**
* @author Thomas Prouvot * @date Creation 16.09.2016 
* @description A l'insertion d'un case, on remplace l'owner ID par la valeur du champ Salesman__c,
puis on partage le case avec la personne qui l'a créé.
*/ 
trigger CaseTrigger on Case (after insert) {

    Boolean byPass = ByPassUtils.isByPassed('CaseTrigger') != null ? ByPassUtils.isByPassed('CaseTrigger') : false;
    if(!byPass) {
        Map<Id, Case> mapC = new Map<Id, Case>();
        List<Case> toUpdate = new List<Case>();
        for(Case c : [SELECT Id, OwnerId, Salesman__c FROM Case WHERE (Id in :Trigger.new)]){
            if(c.Salesman__c!= null && c.Salesman__c != UserInfo.getUserId()){
                mapC.put(c.OwnerId, c);
                c.OwnerId = c.Salesman__c;
                toUpdate.add(c);
            }
        }
        update toUpdate;
        
        if(mapC.size() > 0) {
            List<CaseShare> cs = new List<CaseShare>();
            for(Id own : mapC.keySet()){
                CaseShare c = new CaseShare(CaseId=mapC.get(own).Id,
                                            UserOrGroupId=own,
                                            CaseAccessLevel='Edit');
                cs.add(c);
                
            }
            insert cs;
        }
    }
}