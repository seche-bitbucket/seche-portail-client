/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : LogBatchApexErrorEvent
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : LogBatchApexErrorEvent trigger
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 27-02-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
trigger LogBatchApexErrorEvent on BatchApexErrorEvent (after insert) {
    if(Trigger.IsInsert && Trigger.isAfter){
        LogBatchApexErrorEvent_Handler.onAfterInsert(Trigger.newMap);
    }
}