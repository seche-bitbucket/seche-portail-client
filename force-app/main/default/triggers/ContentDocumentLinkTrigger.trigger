/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ContentDocumentLinkTrigger
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ContentDocumentLinkTrigger trigger
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 23-03-2020 PMB 1.0 - Initial version
* -- 04-06-2020 MGR 2.0 - Comment AfterInsert (Ticket 00951), and inactivate trigger
* -- 07-07-2020 PMB 3.0 - UnComment IsAfter IsInsert
* --------------------------------------------------------------------------------------------------------------------
*/
trigger ContentDocumentLinkTrigger on ContentDocumentLink (after insert) {
    if(Trigger.IsAfter && Trigger.IsInsert){
        ContentDocumentLinkTriggerHandler.onAfterInsert(Trigger.newMap,Trigger.oldMap);
    }
}