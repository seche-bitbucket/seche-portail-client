/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ContentVersionTrigger
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ContentVersionTrigger trigger
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 23-03-2020 PMB 1.0 - Initial version
* -- 04-06-2020 MGR 2.0 - Comment AfterInsert (Ticket 00951)
* --------------------------------------------------------------------------------------------------------------------
*/
trigger ContentVersionTrigger on ContentVersion (before update) {
    if(Trigger.IsBefore && Trigger.isUpdate) {
        ContentVersionTriggerHandler.onBeforeUpdate(Trigger.newMap,Trigger.oldMap);
    }
    
    /*else if(Trigger.IsAfter && Trigger.IsInsert){
        ContentVersionTriggerHandler.onAfterInsert(Trigger.newMap,Trigger.oldMap);
    }*/
}