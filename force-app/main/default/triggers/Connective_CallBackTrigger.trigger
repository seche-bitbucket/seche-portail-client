/**
 * @description       : 
 * @author            : MAIT - @moidriss
 * @group             : 
 * @last modified on  : 11-20-2023
 * @last modified by  : MAIT - @moidriss
**/
trigger Connective_CallBackTrigger on Connective_CallBack__e (after insert) {   
    if(trigger.isInsert && trigger.isAfter) {
        Connective_CallBackTriggerHandler.UpdateAfterSignature(Trigger.new);  
    }
}