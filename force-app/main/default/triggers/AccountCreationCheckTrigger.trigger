//Inactive since Ellisphere deployment (January 2021)

trigger AccountCreationCheckTrigger on Account (before insert) {
    
    If(Trigger.isBefore){
        if(Trigger.isInsert)
           AccountCreationCheckTriggerUtil.CreateAccount(Trigger.New);
           }
  }