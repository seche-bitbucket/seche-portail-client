trigger UserTrigger on User (before insert, before update) {
    
    if(TRIGGER.isUpdate || TRIGGER.isInsert){
        for(User vUser : Trigger.new){
            
            if(vUser.ContactId != null){
                system.debug('vUser.ContactId ' + vUser.ContactId );
                UserTriggerUtil.Generate_Tech_UID(vUser.ContactId );
            } 
        }
    }
}