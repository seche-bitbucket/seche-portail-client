/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : LogBatchApexErrorEvent
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : LogBatchApexErrorEvent trigger
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 27-02-2020 PMB 1.0 - Initial refactoring
* --------------------------------------------------------------------------------------------------------------------
*/
trigger LeadTrigger on Lead (before insert,before update, after update) {
    if (Trigger.IsBefore && Trigger.IsInsert){
        LeadTriggerHandler.onBeforeInsert(Trigger.new);
    } else if(Trigger.IsBefore && Trigger.isUpdate) {
        LeadTriggerHandler.onBeforeUpdate(Trigger.new,Trigger.oldMap);
    } else if(Trigger.IsAfter && Trigger.IsUpdate){
        LeadTriggerHandler.onAfterUpdate(Trigger.newMap,Trigger.oldMap);
    }
}