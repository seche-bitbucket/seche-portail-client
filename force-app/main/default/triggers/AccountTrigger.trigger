/**
        * @author Thomas Prouvot * @date Creation 07.09.2016 
        * @description  A l'insertion, la modification ou la suppression d'un account, vérifier s'il comporte un parent account(Maison_mere__c).
            Si oui, incrémenter le champ TECH_NbComptesEnfants__c pour une insertion, décrémenter pour une suppression, et tester pour une insertion.
    
    */ 
trigger AccountTrigger on Account (before insert, before delete, before update, after update) {
    
    if(Trigger.isInsert){
        AccountTriggerHandler.handler(true, false, false, false, false, Trigger.new, null, null);
    }
    else if(Trigger.isDelete && Trigger.isBefore){
        AccountTriggerHandler.handler(false, true, false, true, false, null, Trigger.old, null);
    }
    else if(Trigger.isUpdate){
        
        if(Trigger.isAfter){
            AccountTriggerHandler.handler(false, false, true, false, true, Trigger.new, null, Trigger.oldMap);
        }if(Trigger.isBefore){
            AccountTriggerHandler.handler(false, false, true, true, false, Trigger.new, null, Trigger.oldMap);
        }
    }
    
}