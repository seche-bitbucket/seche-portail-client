trigger ContactInvitationTrigger on Contacts_Invitation__c (before insert) {

    if(Trigger.isInsert && Trigger.isBefore){
        ContactInvitationHandler.CheckDuplicate(Trigger.new);
    }

}