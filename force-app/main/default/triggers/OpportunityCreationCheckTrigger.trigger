trigger OpportunityCreationCheckTrigger on Opportunity (before insert, before update) {
    
    /*if(CheckRecursive.runforRights()){
        User usr = [Select ID, profileID, SubsidiaryCreationRight__c From User Where Id = : UserInfo.getUserId()];
        System.debug('//////////////////////CreationCheck');
        System.debug('//////////////////////CreationCheck'+usr.profileID);
        
		//Get Profiles 
        Profile profAdmin = [select id from profile where name ='System Administrator' or name ='Administrateur système' limit 1];
        Profile profDirco = [select id from profile where name ='Directeur commercial' limit 1];
        Profile profChargeAffaire = [select id from profile where name ='Chargé(e) d\'affaires' limit 1];
        //Get RecordType
        RecordType recordTypeIntraGroup = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_OpportuniteIntraGroupe' LIMIT 1];

        List<String> allFilialeFilterLs = new List<String>();
        for(Opportunity opp : Trigger.New){
            allFilialeFilterLs.add(opp.Filiale__c + '%');
        }
        //get all groups for opportunity's filiale in trigger and for user group members
        List<Group> allGrpLs = [SELECT Id, Name FROM Group WHERE Name LIKE :allFilialeFilterLs
                                AND Id IN (SELECT GroupId FROM GroupMember WHERE UserOrGroupId = :UserInfo.getUserId())];

        for(Opportunity opp : Trigger.New){
            
            //Check for Assistante and Responsable Co profile
            
            Boolean isAuthorized = false; 
            
            if(usr.ProfileId == profAdmin.Id || opp.RecordTypeID == recordTypeIntraGroup.Id){
                
                isAuthorized = true;
            }else if((usr.ProfileId == profChargeAffaire.Id || usr.ProfileId == profDirco.Id) && opp.RecordTypeID !=recordTypeIntraGroup.Id){
                
                
                if (usr.SubsidiaryCreationRight__c !=null){
                    
                    List<String> lstFiliales = usr.SubsidiaryCreationRight__c.split(';');
                    
                    for(String filiale : lstFiliales){
                        if(Opp.Filiale__c == filiale){
                            isAuthorized = true;
                            break;
                        }
                    }
                }
                
            }else if(opp.RecordTypeID != recordTypeIntraGroup.Id){
                //simulate the filter but it avoids soql query in a loop
                Boolean groupFound = false;
                for (Group grp : allGrpLs){
                    //Filiale__c + '%'
                    if (grp.Name.startsWith(opp.Filiale__c)){
                        groupFound = true;
                        break; // we break cause only one is needed to check if ok to insert/update
                    }
                }
                if(Trigger.isInsert && opp.RecordTypeID != recordTypeIntraGroup.Id ){
                    if(!groupFound){
                        opp.addError('Vous ne pouvez pas créer des opportunités pour des filiales auxquelles vous n\'êtes pas rattaché');
                    }else{
                          isAuthorized = true;
                    }
                }else if(Trigger.isUpdate && opp.RecordTypeID != recordTypeIntraGroup.Id){
                    if(!groupFound && opp.Filiale__c != Trigger.oldMap.get(opp.ID).Filiale__c ){
                        opp.addError('Vous ne pouvez pas créer des opportunités pour des filiales auxquelles vous n\'êtes pas rattaché');
                    }else{
                          isAuthorized = true;
                    }
                }else{
                      isAuthorized = true;
                }
                
            }
            
            if((opp.Filiale__c == null || opp.Filiere__c == null) && usr.ProfileId != profAdmin.Id){
                opp.addError('Vous devez renseigner la filiale et la filiere de la piste');	
            }       
            else if(!isAuthorized){
                opp.addError('Vous ne pouvez pas créer des opportunités pour des filiales auxquelles vous n\'êtes pas rattaché');
            }
                
        }
    }*/
}