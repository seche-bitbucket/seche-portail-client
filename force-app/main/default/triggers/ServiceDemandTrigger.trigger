trigger ServiceDemandTrigger on SD_ServiceDemand__c (before insert, after insert, after update) {
    
   
        If(Trigger.isBefore){
            if(Trigger.isInsert){
               	ServiceDemandTriggerUtil.ManageOwner(Trigger.New);
                ServiceDemandTriggerUtil.updateDetails(Trigger.New);
                ServiceDemandTriggerUtil.updateLocalContact(Trigger.New);
                ServiceDemandTriggerUtil.updateAddress(Trigger.New);
                
            }
        }else if(Trigger.isAfter){
            if(Trigger.isInsert){
                ServiceDemandTriggerUtil.updatePriceBook(Trigger.New);
            }else if(Trigger.IsUpdate){
                If(checkRecursive.runOnce()){
                    List<WST_Waste_Statement__c> lstWSTs =  ServiceDemandTriggerUtil.manageWSTStatus(Trigger.New);
                    update lstWSTs;
                }
                
            }
        }
    
}