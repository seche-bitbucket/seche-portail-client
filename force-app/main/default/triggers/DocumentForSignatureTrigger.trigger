/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-11-26
* @modified       2018-11-26
* @systemLayer    Trigger         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Trigger on DocumentForSignature Object to Update Opportunities to Won when quotes are signed 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
trigger DocumentForSignatureTrigger on DocumentsForSignature__c (after update) {

    if(Trigger.isAfter && Trigger.isUpdate){
        DocumentForSignatureTriggerHandler.DocumentForSignatureAfterUpdate(Trigger.new);
        //DocumentForSignatureTriggerHandler.SendEmailNotif(Trigger.new, Trigger.oldMap);
    }
}