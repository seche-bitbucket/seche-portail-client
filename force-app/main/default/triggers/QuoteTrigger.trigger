//QuoteTrigger afterInsert to generate a code with the date of creation, the trigramme of the user and an autonmuber
// Before is to check copy Address to Picking Address if checkbox same adresse is checked
trigger QuoteTrigger on Quote (after insert, before insert, before update, after update, before delete) {
    Boolean byPass = ByPassUtils.isByPassed('QuoteTrigger') != null ? ByPassUtils.isByPassed('QuoteTrigger') : false ;
    if(!byPass){
        if(Trigger.isAfter && Trigger.isInsert){
            
            List<Quote> quoteToUpdate = new List<Quote>();
            List<ID> quotesID = new List<ID>();
            for (Quote quote: Trigger.new){
                quotesID.add(quote.ID);
            }
            quoteToUpdate = [Select ID, QuoteCode__c, QuoteNumber, OpportunityId, Status,Opportunity.Salesman__r.Trigramme__c FROM Quote WHERE ID in : quotesID];
            QuoteTriggerHandler.updateQte(quoteToUpdate);
            
        }else if(Trigger.isBefore){
            if(Trigger.isDelete) {
                for(Quote quote: Trigger.old) {
                    if(!quote.HasDeleteAccess__c) {
                        quote.addError('Vous ne pouvez pas supprimer les devis qui ne sont pas au statut brouillon');
                    }
                }
            } else {
                for (Quote quote: Trigger.New){
                    if(quote.SameAddress__c){
                        quote.ShippingCity = quote.BillingCity;
                        quote.ShippingStreet = quote.BillingStreet;
                        quote.ShippingState = quote.BillingState;
                        quote.ShippingPostalCode = quote.BillingPostalCode;
                        quote.ShippingCountry = quote.BillingCountry; 
                    }   
                }
            }
        }
        
        // delete attachement Quote's file  , if user refuse approbation    
        if( Trigger.isUpdate ){

            if(Trigger.isBefore) {
                List<Profile> adminIDsList = [SELECT Id FROM profile WHERE Name LIKE '%admin%'];
                List<Quote> quotes = Trigger.new;
                List<Id> quotesId = new List<Id>();
                for(Quote q : quotes) {
                    quotesId.add(q.Id);
                }
                Map<Id, List<ContentDocumentLink>> myMap = new Map<Id, List<ContentDocumentLink>>();
                List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
                if(!Test.isRunningTest()) {
                    cdlList = [SELECT LinkedEntityId, ContentDocument.title FROM ContentDocumentLink WHERE LinkedEntityId IN :quotesId];    
                }
                for(ContentDocumentLink cdl : cdlList) {
                    if(!myMap.containsKey(cdl.LinkedEntityId)) {
                        myMap.put(cdl.LinkedEntityId, new List<ContentDocumentLink>());
                    }
                    myMap.get(cdl.LinkedEntityId).add(cdl);
                }
                for(Quote quo : quotes) {
                    if(quo.Status == 'Signé') {
                        Boolean isFileFound = false;
                        Boolean isUserAdmin = false;
                        List<ContentDocumentLink> docList = myMap.get(quo.Id);
                        if(docList != null) {
                            for(ContentDocumentLink cdl2 : docList) {
                                if(cdl2.ContentDocument.title.contains('Signé') || cdl2.ContentDocument.title.contains('signé')) {
                                    isFileFound = true;
                                    break;
                                }
                            }
                        }
                        for(Profile p : adminIDsList) {
                            if(UserInfo.getProfileId() == p.Id) {
                                isUserAdmin = true;
                                break;
                            }
                        }
                        if(!isFileFound && !isUserAdmin) {
                            quo.addError('Veuillez d\'abord ajouter un document signé dans la liste des Fichiers');
                        }
                    }
                }
            }
                                
            if(Trigger.isAfter) {
                QuoteSyncTriggerV2Handler.execute(Trigger.new, Trigger.oldMap);
            }
            
            List<Quote> quoteTo = new List<Quote>();
            List<ID> quotesID = new List<ID>();
            List<String> lstFindByNameAtt = new List<String>();
            List<String> lstFindByNameCDL = new List<String>();
            List<Attachment>  listAttTodelete = new  List<Attachment> ();
            List<ContentDocumentLink>  listFilesTodelete = new  List<ContentDocumentLink> ();
            
            for (Quote quote: Trigger.new){
                quotesID.add(quote.ID);
            }

            Boolean runBatch=false;
            /*quoteTo = [Select ID, QuoteCode__c,TECH_UnknownSignerInformation__c, TECH_SendForSignature__c, Quote_Type__c,
                    NameCustomer__c,ReferenceWasteOwner__c, QuoteNumber, OpportunityId, Status,Contact.FirstName,
                    Contact.LastName,Contact.Email,Contact.Phone FROM Quote WHERE ID in : quotesID];*/
            
            //Map<Id,List<Attachment>> mapQuoteIdAtt = getAttachments(quotesID);
            //Map<Id,List<ContentDocumentLink>> mapQuoteIdDocl = getFiles(quotesID);        
            
            Map<Id,List<ContentDocumentLink>> mapQuoteIdDocl = new Map<Id,List<ContentDocumentLink>>();
            for(ContentDocumentLink cdl: [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN :quotesID]) {
                if(mapQuoteIdDocl.containsKey(cdl.LinkedEntityId)) {
                    List<ContentDocumentLink> lst_cdls = new List<ContentDocumentLink>();
                    lst_cdls = mapQuoteIdDocl.get(cdl.LinkedEntityId);
                    lst_cdls.add(cdl);
                    mapQuoteIdDocl.put(cdl.LinkedEntityId, lst_cdls);
                } else {
                    mapQuoteIdDocl.put(cdl.LinkedEntityId, new List<ContentDocumentLink>{cdl});
                }
            }
            
            for (Quote quote: Trigger.new){
                
                if(quote.TECH_IsLocked__c == true && mapQuoteIdDocl.get(quote.Id) == null){
                    quote.addError('Veuillez d\'abord générer un document et l\'attacher à la liste des Fichiers'); 
                }

                if(quote.Status =='Révision nécessaire'){
                    
                    String vTypeDevis;
                    if(Quote.Quote_Type__c == 'Convention')
                        vTypeDevis = 'Convention';
                    else
                        vTypeDevis = 'Devis';
                    
                    String fileName = quote.QuoteCode__c +' - '+vTypeDevis+' - '+Quote.NameCustomer__c+' - '+Quote.ReferenceWasteOwner__c+'.pdf';
                    lstFindByNameAtt.add(fileName);
                    lstFindByNameCDL.add(fileName);
                    
                    /*if(mapQuoteIdAtt.get(quote.Id) != null){
                        for( Attachment att :mapQuoteIdAtt.get(quote.Id)){ 
                            String vTypeDevis;
                            if(Quote.Quote_Type__c == 'Convention')
                                vTypeDevis = 'Convention';
                            else
                                vTypeDevis = 'Devis';
                            System.debug('test nom = '+ quote.QuoteCode__c +' - '+vTypeDevis+' - '+Quote.NameCustomer__c+' - '+Quote.ReferenceWasteOwner__c+'.pdf');
                            System.debug('att.name =' + att.name);
                            
                            String vQuoteNameNumber=quote.QuoteCode__c +' - '+vTypeDevis+' - '+Quote.NameCustomer__c+' - '+Quote.ReferenceWasteOwner__c+'.pdf';
                            System.debug('vQuoteNameNumber =' + vQuoteNameNumber);
                            Integer QuoteNumber = Integer.valueOf(quote.QuoteNumber);
                            if(att.name == vQuoteNameNumber)
                            listAttTodelete.add(att);
                                
                        }
                    }
                    if(mapQuoteIdDocl.get(quote.Id) != null){
                            for( ContentDocumentLink fi :mapQuoteIdDocl.get(quote.Id)){ 
                            String vTypeDevis;
                            if(Quote.Quote_Type__c == 'Convention')
                                vTypeDevis = 'Convention';
                            else
                                vTypeDevis = 'Devis';
                            System.debug('test nom = '+ quote.QuoteCode__c +' - '+vTypeDevis+' - '+Quote.NameCustomer__c+' - '+Quote.ReferenceWasteOwner__c+'.pdf');
                            System.debug('fi.name =' + fi.ContentDocument.title);
                            
                            String vQuoteNameNumber=quote.QuoteCode__c +' - '+vTypeDevis+' - '+Quote.NameCustomer__c+' - '+Quote.ReferenceWasteOwner__c+'.pdf';
                            System.debug('vQuoteNameNumber =' + vQuoteNameNumber);
                            Integer QuoteNumber = Integer.valueOf(quote.QuoteNumber);
                            if(fi.ContentDocument.title == vQuoteNameNumber)
                            listFilesTodelete.add(fi);
                                
                        }
                    }*/
                }
            }
            
            //delete attachement Quote's file
            if(lstFindByNameAtt.size()>0){
                for(List<Attachment> lst: getAttachments(quotesID, lstFindByNameAtt).values()) {
                    listAttTodelete.addAll(lst);
                }
                delete listAttTodelete;
            }
            if(lstFindByNameCDL.size()>0){
                for(List<ContentDocumentLink> lst: getFiles(quotesID, lstFindByNameCDL).values()){
                    listFilesTodelete.addAll(lst);
                }
                delete listFilesTodelete;
            }
        }
    }
    // Return a list of Attachment 
    private Map<Id,List<Attachment>> getAttachments(List<String> quoteIds, List<String> fileName) {
        Map<Id,List<Attachment>> result = new Map<Id,List<Attachment>>();
        List<Attachment> lstAtt = [SELECT id, Name, body, ContentType, ParentId FROM Attachment WHERE ParentId IN :quoteIds];
        for(Attachment att : lstAtt){
            for(String name :fileName) {
                system.debug(name);
                system.debug(att.name);
                if(att.name == name) {
                    if(result.containsKey(att.ParentId)){
                        List<Attachment> currentListAtt = result.get(att.ParentId);
                        currentListAtt.add(att);
                        result.put(att.ParentId,currentListAtt);
                    }
                    else{
                        List<Attachment> newListAtt = new List<Attachment>();
                        newListAtt.add(att);
                        result.put(att.ParentId,newListAtt);
                    } 
                }
            }
        }
        return result;
    }

    // Return a list of files 
    private Map<Id,List<ContentDocumentLink>> getFiles(List<String> quoteIds, List<String> fileName) {
        Map<Id,List<ContentDocumentLink>> result = new Map<Id,List<ContentDocumentLink>>();
        List<ContentDocumentLink> lstDocl = [SELECT Id, LinkedEntityId, ContentDocumentId, ContentDocument.title FROM ContentDocumentLink WHERE LinkedEntityId IN: quoteIds];
        for(ContentDocumentLink docl : lstDocl){
            for(String title: fileName) {
                system.debug(title);
                system.debug(docl.ContentDocument.Title);
                if(docl.ContentDocument.Title == title) {
                    if(result.containsKey(docl.LinkedEntityId)){
                        List<ContentDocumentLink> currentListDocl = result.get(docl.LinkedEntityId);
                        currentListDocl.add(docl);
                        result.put(docl.LinkedEntityId,currentListDocl);
                    }
                    else{
                        List<ContentDocumentLink> newListDocl = new List<ContentDocumentLink>();
                        newListDocl.add(docl);
                        result.put(docl.LinkedEntityId,newListDocl);
                    }
                }
            }
        }
        return result;
    }
    
}