trigger EquipeTrigger on Equipe__c (before update) {
//Collect old data
    List<Equipe__c> oldEs = Trigger.Old;
    
    //Map Old data by there Id
    Map<ID,Equipe__c> oldEosMap = new Map<ID,Equipe__c>();
    for(Equipe__c eOld : [Select Id, Directeur__c, Assistant__c, Filiere__c from Equipe__c]){
        oldEosMap.put(eOld.ID, eOld);
    }
    
    for(Equipe__c e : Trigger.new){
        
        List<OpportunityTeamMember> otmDirector = new List<OpportunityTeamMember>();
        List<OpportunityTeamMember> otmNewDirector = new List<OpportunityTeamMember>();
        
        List<OpportunityTeamMember> otmAssistant = new List<OpportunityTeamMember>();
        List<OpportunityTeamMember> otmNewAssistant = new List<OpportunityTeamMember>();
        
        //Get the Old to compare Equipe__c
        Equipe__c oldE = oldEosMap.get(e.ID);
        
        
        //Check if the Director has changed
        if(e.Directeur__c != oldE.Directeur__c){
            //Load all the Director Role in OpportunityTeam
            otmDirector = [Select Id, UserId, OpportunityId From OpportunityTeamMember Where Tech_Equipe__c = : e.Id AND TeamMemberRole = 'Directeur Commercial'];
            //Call function that return a list of new Director
            otmNewDirector = EquipeHelper.getNewOpportunityTeamMemberDirector(e.ID, e.Directeur__c, otmDirector);
            
        }
        //Check if the Secretary has changed and the new one isn't empty
        if(e.Assistant__c != oldE.Assistant__c && e.Assistant__c != null){
            //Load all the Secretary Role in OpportunityTeam
            otmAssistant = [Select Id, UserId, OpportunityId From OpportunityTeamMember Where Tech_Equipe__c = : e.Id AND TeamMemberRole = 'Assistant(e)'];
            //Check if we have already load all director in OTM has the are used as basis => Director are mandatory and not Secretary
            if(otmDirector.size() == 0){
                otmDirector = [Select Id, UserId, OpportunityId From OpportunityTeamMember Where Tech_Equipe__c = : e.Id AND TeamMemberRole = 'Directeur Commercial'];
            }
            //Call function that return a list of new Secretary
            otmNewAssistant = EquipeHelper.getNewOpportunityTeamMemberAssistant(e.ID, e.Assistant__c, otmDirector);
        
        //Check 
        }else if(e.Assistant__c == null){
            otmAssistant = [Select Id, UserId, OpportunityId From OpportunityTeamMember Where Tech_Equipe__c = : e.Id AND TeamMemberRole = 'Assistant(e)'];
        }
        
        if(otmDirector.size() == otmNewDirector.size() && otmDirector.size() != 0){
            delete otmDirector;
            insert otmNewDirector;
        }
        
        if(otmAssistant.size() != 0 && otmNewAssistant.size() != 0){
            delete otmAssistant;
            insert otmNewAssistant;
        }else if(otmNewAssistant.size() != 0){
            insert otmNewAssistant;
        }else if(otmAssistant.size() != 0){
            delete otmAssistant;
        }
        
    }
}