/**
* @author Thomas Prouvot * @date Creation 16.09.2016 
* @description A l'insertion d'un compte rendu, on remplace l'owner ID par la valeur du champ Commercial_associe__c,
puis on partage le CR avec la personne qui l'a créé.
*/ 
trigger VisitReportTrigger on VisitReport__c (after insert) {
    
    Map<Id, VisitReport__c> mapV = new Map<Id, VisitReport__c>();
    List<VisitReport__c> vrs =  [SELECT Id, OwnerId, Commercial_associe__c, Opportunity__c, Opportunity__r.Salesman__c FROM VisitReport__c WHERE Id in :Trigger.new ];
    List<VisitReport__c> toUpdate = new List<VisitReport__c>();
    for(VisitReport__c v : vrs ){
        if(v.Commercial_associe__c != null && v.Commercial_associe__c != UserInfo.getUserId() && v.Commercial_associe__c != v.OwnerId){
            mapV.put(v.OwnerId, v);
            v.OwnerId = v.Commercial_associe__c;
            toUpdate.add(v);
        }
        
    }    
    update toUpdate;
    
    List<VisitReport__Share> vs = new List<VisitReport__Share>();
    for(Id own : mapV.keySet()){
        VisitReport__Share v = new VisitReport__Share(ParentId=mapV.get(own).Id,
                                                      UserOrGroupId=own,
                                                      AccessLevel='Edit');
        vs.add(v);        
    }
    
    for(VisitReport__c vr : vrs){
        if(vr.Opportunity__c != null && vr.Opportunity__r.Salesman__c != vr.Commercial_associe__c){
            VisitReport__Share v = new VisitReport__Share(ParentId= vr.Id,
                                                          UserOrGroupId= vr.Opportunity__r.Salesman__c,
                                                          AccessLevel='Edit');
            vs.add(v);        
        }
    }
    insert vs;
}