trigger OpportunityTrigger on Opportunity (before insert, after insert, before update, after update) {
    //Collect old data
    List<Opportunity> oldOpp = Trigger.Old;
    static Map<ID, Opportunity> opps = new Map<ID, Opportunity>();
    static List<RecordType> rts = new List<RecordType>();
    static Map<String, Equipe__c> equipes = new  Map<String, Equipe__c>();
    static Map<ID, List<OpportunityTeamMember>> otms = new Map<ID, List<OpportunityTeamMember>>();
    List<Opportunity> oppChapeauToUpdate = new List<Opportunity>();
    
    List<OpportunityTeamMember> otmToInsert =  new List<OpportunityTeamMember>();
    List<OpportunityTeamMember> otmToDelete =  new List<OpportunityTeamMember>();
    
    
    //List of User to subscribe on the Opportunity
    List<EntitySubscription> ess = new List<EntitySubscription>(); 
    //List of user To Unsubscribe of won opportunities
    List<EntitySubscription> subscriptionToDelete = new List<EntitySubscription>();
    
    Boolean byPass = ByPassUtils.isByPassed('OpportunityTrigger') != null ? ByPassUtils.isByPassed('OpportunityTrigger') : false ;
    if(!byPass){
        
        if(Trigger.isBefore) {
            List<Opportunity> opps = Trigger.new;
    
            if(Trigger.isInsert) {
                List<Id> salesIds = new List<Id>();
                for(Opportunity opp: opps) {
                    if(opp.Salesman__c != null) {
                        salesIds.add(opp.Salesman__c);
                    }
                }
                List<User> salesmen = [SELECT Assistant__r.Id, Assistant__r.SubsidiaryCreationRight__c FROM User WHERE Id IN :salesIds];
                for(Opportunity opp: opps) {
                    if(opp.Assistant__c != null) {
                        User assistant = [SELECT SubsidiaryCreationRight__c FROM User WHERE Id = :opp.Assistant__c];
                        if(assistant.SubsidiaryCreationRight__c != null && !assistant.SubsidiaryCreationRight__c.contains(opp.Filiale__c) || assistant.SubsidiaryCreationRight__c == null) {
                            opp.addError('Vous ne pouvez pas choisir cet assistant car il n\'est pas rattaché à la filiale porteuse de l\'offre : '+opp.Filiale__c);
                        }
                    }
                    if(salesmen != null) {
                        for(User salesman: salesmen) {
                            System.debug(salesman);
                            if(salesman.Id == opp.Salesman__c && opp.Assistant__c == null && (salesman.Assistant__r.SubsidiaryCreationRight__c != null && salesman.Assistant__r.SubsidiaryCreationRight__c.contains(opp.Filiale__c))) {
                                opp.Assistant__c = salesman.Assistant__r.Id;
                            }
                        }
                    }
                }
            } else if(Trigger.isUpdate) {
                for(Opportunity opp: opps) {
                    if(opp.Assistant__c != null) {
                        User assistant = [SELECT SubsidiaryCreationRight__c FROM User WHERE Id = :opp.Assistant__c];
                        if(assistant.SubsidiaryCreationRight__c != null && !assistant.SubsidiaryCreationRight__c.contains(opp.Filiale__c) || assistant.SubsidiaryCreationRight__c == null) {
                            opp.addError('Vous ne pouvez pas choisir cet assistant car il n\'est pas rattaché à la filiale porteuse de l\'offre : '+opp.Filiale__c);
                        }
                    }
                }
            }
        }
        
        
        if(checkRecursive.runOnceTriggerLoads()){
            opps = new Map<ID, Opportunity>([Select ID, Name, Opportunit_mere__c ,Opportunit_mere__r.StageName, Opportunit_mere__r.Amount, Opportunit_mere__r.TonnageN__c FROM Opportunity WHERE ID =: Trigger.NEW]);
            rts = [Select Id, DeveloperName From RecordType Where SObjectType = 'Opportunity'];
            equipes = OpportunityTriggerUtil.getOpportunityTeams(Trigger.New);
            otms = OpportunityTriggerUtil.getAllOpportunityTeams(Trigger.New);
        }
        
        if(Trigger.isInsert){
            System.debug('//////////////////////Insert');
            if(Trigger.isBefore && checkRecursive.runOnceBefore()){
                System.debug('//////////////////////Before');
                OpportunitytriggerUtil.manageSubOppName(Trigger.New);
                //OpportunityTriggerUtil.checkUserCreation(Trigger.New, Trigger.OldMap );    
            }else if(Trigger.isAfter && checkRecursive.runOnceAfter()){
                System.debug('//////////////////////After');
                oppChapeauToUpdate = OpportunitytriggerUtil.manageOppMotherWithInitialization(Trigger.New, opps);
                ess = OpportunitytriggerUtil.manageUserSubscriptions(Trigger.New);
                otmToDelete = OpportunityTriggerUtil.getOpportunityTeamToDelete(Trigger.New, otms);
                otmToInsert = OpportunityTriggerUtil.getOpportunityTeamToInsert(Trigger.New, equipes);
                System.debug('//////////OTM to Insert ; '+otmToInsert);    
            }
        }else if(Trigger.isUpdate){
            System.debug('//////////////////////Update');
            if(Trigger.isBefore && checkRecursive.runOnceBefore()){
                System.debug('//////////////////////Before' + checkRecursive.runOnceBefore());
                oppChapeauToUpdate = OpportunitytriggerUtil.manageOppMotherWithWonDaughter(Trigger.New, opps);
                OpportunitytriggerUtil.manageOpportunityRecordType(Trigger.new, rts);
                otmToDelete = OpportunityTriggerUtil.getOpportunityTeamToDelete(Trigger.New, otms);
                otmToInsert = OpportunityTriggerUtil.getOpportunityTeamToInsert(Trigger.New, equipes);
                //OpportunityTriggerUtil.checkUserCreation(Trigger.New, Trigger.OldMap );    
            }else if(Trigger.isAfter && checkRecursive.runOnceAfter()){
                System.debug('//////////////////////After' + checkRecursive.runOnceAfter());
                subscriptionToDelete = OpportunityTriggerUtil.manageUserToUnsubscribe(Trigger.New);  
            }  
        }
        
        if(Trigger.isAfter && Trigger.isUpdate) {
            OppSyncTriggerV2Handler.execute(Trigger.new, Trigger.oldMap);
        }
        
        if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
            
            User usr;
            
            User[] usrs = [Select ID, profileID, SubsidiaryCreationRight__c From User Where Id = : UserInfo.getUserId()];
            
            if(usrs != null && usrs.size() == 1) { //do not run trigger on platformevent modif
                usr = usrs[0];
                if(checkRecursive.runforRights()){
                    System.debug('//////////////////////CreationCheck');
                    System.debug('//////////////////////CreationCheck'+usr.profileID);
                    
                    //Get Profiles 
                    Profile profAdmin = [select id from profile where name ='System Administrator' or name ='Administrateur système' limit 1];
                    Profile profDirco = [select id from profile where name ='Directeur commercial' limit 1];
                    Profile profChargeAffaire = [select id from profile where name ='Chargé(e) d\'affaires' limit 1];
                    //Get RecordType
                    RecordType recordTypeIntraGroup = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_OpportuniteIntraGroupe' LIMIT 1];
                    
                    List<String> allFilialeFilterLs = new List<String>();
                    for(Opportunity opp : Trigger.New){
                        allFilialeFilterLs.add(opp.Filiale__c + '%');
                    }
                    //get all groups for opportunity's filiale in trigger and for user group members
                    List<Group> allGrpLs = [SELECT Id, Name FROM Group WHERE Name LIKE :allFilialeFilterLs
                                            AND Id IN (SELECT GroupId FROM GroupMember WHERE UserOrGroupId = :UserInfo.getUserId())];
                    
                    for(Opportunity opp : Trigger.New){
                        
                        //Check for Assistante and Responsable Co profile
                        
                        Boolean isAuthorized = false; 
                        
                        if(usr.ProfileId == profAdmin.Id || opp.RecordTypeID == recordTypeIntraGroup.Id){
                            isAuthorized = true;
                        }else if(opp.RecordTypeID != recordTypeIntraGroup.Id){
                            
                            if (usr.SubsidiaryCreationRight__c !=null){
                                
                                List<String> lstFiliales = usr.SubsidiaryCreationRight__c.split(';');
                                
                                for(String filiale : lstFiliales){
                                    if(Opp.Filiale__c == filiale){
                                        isAuthorized = true;
                                        break;
                                    }
                                }
                            }
    
                        }
                        // }else if(opp.RecordTypeID != recordTypeIntraGroup.Id){
                        //     //simulate the filter but it avoids soql query in a loop
                        //     Boolean groupFound = false;
                        //     for (Group grp : allGrpLs){
                        //         //Filiale__c + '%'
                        //         if (grp.Name.startsWith(opp.Filiale__c)){
                        //             groupFound = true;
                        //             break; // we break cause only one is needed to check if ok to insert/update
                        //         }
                        //     }
                        //     if(Trigger.isInsert && opp.RecordTypeID != recordTypeIntraGroup.Id ){
                        //         if(!groupFound){
                        //             opp.addError('Vous ne pouvez pas créer des opportunités pour des filiales auxquelles vous n\'êtes pas rattaché');
                        //         }else{
                        //             isAuthorized = true;
                        //         }
                        //     }else if(Trigger.isUpdate && opp.RecordTypeID != recordTypeIntraGroup.Id){
                        //         if(!groupFound && opp.Filiale__c != Trigger.oldMap.get(opp.ID).Filiale__c ){
                        //             opp.addError('Vous ne pouvez pas créer des opportunités pour des filiales auxquelles vous n\'êtes pas rattaché');
                        //         }else{
                        //             isAuthorized = true;
                        //         }
                        //     }else{
                        //         isAuthorized = true;
                        //     }
                            
                        // }
                        
                        if((opp.Filiale__c == null || opp.Filiere__c == null) && usr.ProfileId != profAdmin.Id){
                            opp.addError('Vous devez renseigner la filiale et la filiere de la piste'); 
                        }   
                            
                        // else 
                        if(!isAuthorized){
                            opp.addError('Vous ne pouvez pas créer ou modifier des opportunités pour des filiales auxquelles vous n\'êtes pas rattaché');
                        }
                        
                    }
                }
                
            }
        }
        
        if(oppChapeauToUpdate.size() > 0){
            update oppChapeauToUpdate;
        }
        
        if(otmToDelete.size() > 0){
            delete otmToDelete;
        }
        
        if(otmToInsert.size() > 0){
            insert otmToInsert;
        }
        
        
        //insert list of user to subcribe on the opportunity
        if(ess.size() > 0){
            try{
                insert ess;    
            }catch(Exception e){
                System.debug('Error : '+e);
            }
            
        }
        
        //Delete users subcribed on Won/loose opportunity
        if(subscriptionToDelete.size() > 0){
            Delete subscriptionToDelete;
        }    
    }
}