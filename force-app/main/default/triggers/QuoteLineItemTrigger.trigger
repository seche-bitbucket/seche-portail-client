trigger QuoteLineItemTrigger on QuoteLineItem (before insert, after update, after insert) {
    Boolean byPass = ByPassUtils.isByPassed('QuoteLineItemTrigger') != null ? ByPassUtils.isByPassed('QuoteLineItemTrigger') : false ;
    if(!byPass){
        if(Trigger.isUpdate && Trigger.isAfter) { 
            
            QteLineItemSyncTriggerV2Handler.execute(Trigger.New, Trigger.oldMap);
            
        } else if(Trigger.isBefore && Trigger.isInsert) {
        
            //Bypass the trigger for some Test to avoid Pricebook error
            if(!checkRecursive.avoidRun){
                
                Set<ID> lstProduct = new Set<ID>();
                Boolean setQLI = false;
                for(QuoteLineItem qli : Trigger.New){
                    
                    lstProduct.add(qli.Product2Id);
                }
                
                if(Trigger.New[0] != null){
                    QuoteLineItem qli0 = Trigger.new[0];
                    ID quoteID = qli0.QuoteID;
                    Decimal OppLineItemsCount = [Select TECH_NombreProduits__c FROM Opportunity WHERE ID in (Select OpportunityID FROM Quote WHERE ID =: quoteID) LIMIT 1].TECH_NombreProduits__c;
                    Quote quo = [Select TECH_hasTraitementQuoteLine__c, TECH_hasPrestationQuoteLine__c, TECH_hasConditionnementQuoteLine__c,TECH_hasTTQuoteLine__c, IsSyncing FROM Quote WHERE ID =: quoteID];
                    Decimal QuoLineItemsCount = quo.TECH_hasTraitementQuoteLine__c + quo.TECH_hasPrestationQuoteLine__c + quo.TECH_hasConditionnementQuoteLine__c+quo.TECH_hasTTQuoteLine__c;
                    
                    if((OppLineItemsCount  <=  QuoLineItemsCount && quo.IsSyncing) || !quo.IsSyncing ){
                        setQLI = true;
                    }else{}
                    System.debug('setQLI'+setQLI+'OppLineItemsCount'+OppLineItemsCount+'QuoLineItemsCount'+QuoLineItemsCount+'quo.IsSyncing'+quo.IsSyncing);
                }
                
                Map<ID, Product2> mapProduct = new Map<ID, Product2>([SELECT Id, Name, Classe__c, EuropeanCode__c,  Features__c, GE__c, IncludedTGAP__c,
                                                                    Label__c, Family, Container__c, TransportType__c, Treatment__c, UNCode__c, Unit__c
                                                                    FROM Product2 WHERE ID in : lstProduct ]);
                if(setQLI){
                    for(QuoteLineItem qli : Trigger.New){
                        Product2 prd = mapProduct.get(qli.Product2Id);
                        if(prd != null){
                            if(qli.Classe__c == null && prd.Classe__c != null){
                                qli.Classe__c = prd.Classe__c;
                            }
                            
                            if(qli.TransportType__c == null && prd.TransportType__c != null){
                                qli.TransportType__c = prd.TransportType__c;
                            }
                            
                            if(qli.Treatment__c == null && prd.Treatment__c != null){
                                qli.Treatment__c = prd.Treatment__c;
                            }
                            
                            if(qli.Classe__c == null && prd.Classe__c != null){
                                qli.Classe__c = prd.Classe__c;
                            }
                            
                            if(qli.EuropeanCode__c == null && prd.EuropeanCode__c != null){
                                qli.EuropeanCode__c = prd.EuropeanCode__c;
                            }
                            
                            if(qli.Features__c == null && prd.Features__c != null){
                                qli.Features__c = prd.Features__c;
                            }
                            
                            if(qli.GE__c == null && prd.GE__c != null){
                                qli.GE__c = prd.GE__c;
                            }
                            
                            if(qli.IncludedTGAP__c == null && prd.IncludedTGAP__c != null){
                                qli.IncludedTGAP__c = prd.IncludedTGAP__c;
                            }
                            
                            if(qli.Label__c == null && prd.Label__c != null){
                                qli.Label__c = prd.Label__c;
                            }
                            
                            if(qli.Nature__c == null && prd.Family != null){
                                qli.Nature__c = prd.Family;
                            }
                            
                            if(qli.Packaging__c == null && prd.Container__c != null){
                                qli.Packaging__c = prd.Container__c;
                            }
                            
                            if(qli.TransportType__c == null && prd.TransportType__c != null){
                                qli.TransportType__c = prd.TransportType__c;
                            }
                            
                            if(qli.Treatment__c == null && prd.Treatment__c != null){
                                qli.Treatment__c = prd.Treatment__c;
                            }
                            
                            if(qli.UNCode__c == null && prd.UNCode__c != null){
                                qli.UNCode__c = prd.UNCode__c;
                            }
                            
                            if(qli.Unit__c == null && prd.Unit__c != null){
                                qli.Unit__c = prd.Unit__c;
                            }
                            
                            if(qli.TECH_Name2__c == null && prd.Name != null){
                                qli.TECH_Name2__c = prd.Name;
                            }
                        }
                    }
                }else{
                    for(QuoteLineItem qli : Trigger.New){
                        
                        Product2 prd = mapProduct.get(qli.Product2Id);
                        if(prd != null){
                            if(qli.TECH_Name2__c == null){
                                qli.TECH_Name2__c = prd.Name;
                            }
                        }
                    }
                }
            }
            
        } else if(Trigger.isAfter && Trigger.isInsert) {
            
            QteLineItemSyncTriggerV2Handler.copy(Trigger.New);
            
        }
    }
}