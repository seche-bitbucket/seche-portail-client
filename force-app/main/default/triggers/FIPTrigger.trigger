trigger FIPTrigger on FIP_FIP__c (before insert, after update) {
    
    If(TRIGGER.IsInsert){
        if(TRIGGER.ISBefore){
            Map<ID, Account> extMap = new Map<ID, Account>();
            Set<ID> extIdSet = new Set<ID>();
            for(FIP_FIP__c fip : Trigger.new){
                If(fip.TECH_AccountID__c != null){
                    extIdSet.add(fip.TECH_AccountID__c);
                }else{
                    extIdSet.add(fip.Collector__c);
                }
                
                System.debug('IDs Account : '+extIdSet);
                
            }
            for(Account a : [Select ID, Name, FavoriteSubsidiaryAsbestos__c, TECH_X3SECHEID__c, X3AddressCode__c, ChargedX3Code__c, X3ChargedAddressCode__c, BillingStreet, BillingCity, BillingPostalCode, SIRET_Number__c FROM Account Where ID IN :extIdSet]){
                extMap.put(a.ID, a);
            }
            
            
            for(FIP_FIP__c fip : Trigger.New){
                
                Account acc = new Account();
                
                If(fip.TECH_AccountID__c != null){
                    acc = extMap.get(fip.TECH_AccountID__c);
                    fip.Collector__c = fip.TECH_AccountID__c;
                }else{
                    acc = extMap.get(fip.Collector__c);
                }
                if(fip.state__c != 'En attente qualification par le collecteur'){
                    fip.state__c = 'En attente de qualification'; 
                }
                
                fip.ChargedX3Code__c = acc.ChargedX3Code__c;
                fip.ChargedX3AddressCode__c = acc.X3ChargedAddressCode__c;
                fip.subsidiary__c = acc.FavoriteSubsidiaryAsbestos__c;
                //Set the exutoire depending of the Favorite Subsidiary
                if(acc.FavoriteSubsidiaryAsbestos__c == 'Séché Eco Industries Le Vigeant'){
                    fip.Exutoire__c = 'SEV';
                }else{
                     fip.Exutoire__c = 'AMI';
                }
                                
                if(fip.contact__c == null){
                    fip.Contact__c = [Select ContactID FROM USER WHERE ID =: UserInfo.getUserId() ].ContactID;
                }
                
                if(fip.ProductorAdress1__c == null && fip.ProductorCity__c == null && fip.ProductorPostalCode__c == null ){
                    
                    
                    fip.ProductorBusinessName__c = acc.name;
                    if(acc.BillingStreet != null){fip.ProductorAdress1__c = (acc.BillingStreet).abbreviate(30);}
                    fip.ProductorCity__c = acc.BillingCity;
                    fip.ProductorCompanyIdentificationSystem__c =  String.valueOf(acc.SIRET_Number__c);
                    fip.ProductorPostalCode__c = acc.BillingPostalCode;
                    
                }
                
                if(fip.ChargedAdress__c == null && fip.ChargedCity__c == null && fip.ChargedPostalCode__c == null){
                    fip.ChargedBusinessName__c = acc.name;
                    fip.ChargedAdress__c = acc.BillingStreet;
                    fip.ChargedCity__c = acc.BillingCity;
                    fip.ChargedCompanyIdentificationSystem__c =  String.valueOf(acc.SIRET_Number__c);
                    fip.ChargedPostalCode__c = acc.BillingPostalCode;
                    fip.AskerCodeX3__c = acc.ChargedX3Code__c;
                    fip.AskerX3AddressCode__c = 'GEN';
                }
            }
            
        }   
        
    }
    

        
    
}