trigger CongaTemplateTrigger on APXTConga4__Conga_Template__c (Before Insert, Before Update) {
    
    for(APXTConga4__Conga_Template__c template : Trigger.New){
        
        if(template.FilialeTemplate__c != null){
            template.APXTConga4__Template_Group__c = template.FilialeTemplate__c;
        }
        
    }

}