import { api, LightningElement } from 'lwc'
import { NavigationMixin } from 'lightning/navigation'
import LightningAlert from 'lightning/alert';

import imgs from '@salesforce/resourceUrl/Pollutec'

import retrieveFieldsFromLayout from '@salesforce/apex/CAP_Global_SF_SysTools.retrieveFieldsFromLayout'

export default class lwc_NewButtonFipAmiante extends NavigationMixin(LightningElement){
    
    @api objectApiName
    @api recordId //for edit
    @api useLayoutsFields//specify if we get fields from layout
    @api displayAsOneColumn
    @api overrideSubmit // uses polymerfic architecture to save record
    @api prefillUponCreationJSON
    @api createRecord
    @api previewData
    @api fields
    @api flowMode
    @api createdRecordId
    @api hasSections 
    @api recordTypeId
    @api displayActions
    @api provinceData
    @api countryOptions
    @api lockField
    @api fipId
    
    layoutName = 'FIP_FIP__c-FIP_SEI';
    prefillUponCreation
    provincesAreDependent = false
    provinceOptions
    addressData = {}
    layoutFields
    layoutFieldsPart1 = []
    layoutFieldsPart2 = []
    lang
    fipModule
    form
    wait = true
    display = false

    get getStreetLabel() {
        return (this.lang === 'fr') ? 'Rue' : 'Street'
    }

    get getCityLabel() {
        return (this.lang === 'fr') ? 'Ville' : 'City'
    }

    get getCountryLabel() {
        return (this.lang === 'fr') ? 'Pays' : 'Country'
    }

    get getProvinceLabel() {
        return (this.lang === 'fr') ? 'Région' : 'Province'
    }

    get getPostalCodeLabel() {
        return (this.lang === 'fr') ? 'Code Postal' : 'Postal Code'
    }

    get columnStyle() {
        return (this.displayAsOneColumn) ? "min-w-100" : "min-w-50"
    }

    get saveLabel() {
        return (this.lang === 'fr') ? 'Créer' : 'Save'
    } 

    get cancelLabel() {
        return (this.lang === 'fr') ? 'Annuler' : 'Cancel'
    }

    displayModal() {
        this.display = true
    }

    async error(message, theme, label) {
        // eslint-disable-next-line no-await-in-loop
        await LightningAlert.open({
            message: message,
            theme: theme, // a red theme intended for error states
            label: label, // this is the header text
        })
        this.wait = false
    }

    hideModal() {
        this.display = false
    }

    async connectedCallback() {  
        try {
            this.lang = 'fr'
            this.useLayoutFields = true
            this.main_logo_img = imgs + '/Brand.png'

            if(this.layoutName) {
                const data = JSON.parse(await retrieveFieldsFromLayout({layoutName: this.layoutName, hasSections: true}))

                let part1Done = false
                for(let sectionIndex = 0; sectionIndex < data.length; sectionIndex++) {
                    let section = data[sectionIndex]

                    section.columnSize = 'width: 100%;'
                    if(section.sectionLabel !== 'Code nomenclature déchet') {
                        section.columnSize = 'width: 48%;'
                    }

                    if(!part1Done) {
                        this.layoutFieldsPart1.push(section)
                    }
                    if(part1Done && section.sectionLabel !== 'System Information') {
                        this.layoutFieldsPart2.push(section)
                    }

                    if(section.sectionLabel === 'Identification Déchet') {
                        part1Done = true
                    }
                }
            }
            
            if(!this.layoutName && !this.recordTypeId) {
                this.error('Form cannot be generated useLayoutsFields is Checked but no Layout Name nor RecordType is mentionned, reload the page to retry.', 'error', 'Erreur')
            }

            this.wait = false

        } catch(e) {
            this.error(e, 'error', 'Erreur')
        }
    }

    handleSubmitEvent(event) {
        try{
            this.wait = true
            event.preventDefault()
            event.stopPropagation()
            let record = JSON.parse(JSON.stringify(event.detail.fields))
            console.log(event.detail.fields)
            record.RecordTypeId = '012580000006JILAA2'
            for(const key in record) {
                if(record[key].length === 0) {
                    delete record[key]
                }
            }
            console.log(JSON.parse(JSON.stringify(record)))
            this.template.querySelector("lightning-record-edit-form").submit(record)
        } catch(e) {
            this.error(e, 'error', 'Erreur')
        }
    }

    addressChange(event) {
        try {
            let addressObj = event.detail
            Object.keys(addressObj).forEach(function(key) {
                if(key !== 'validity') {
                    if(key === 'province') {
                        if(addressObj[key].length > 0) {
                            this.prefillUponCreation.find(pF => pF.name.toLowerCase().includes('statecode')).value = addressObj[key]
                            this.prefillUponCreation.find(pF => pF.name.toLowerCase().includes('state')).value = this.provinceOptions.find(opt => opt.value === addressObj[key]).label
                        }
                    } else if(key === 'country') {
                        if(addressObj[key].length > 0) {
                            this.prefillUponCreation.find(pF => pF.name.toLowerCase().includes('countrycode')).value = addressObj[key]
                            this.prefillUponCreation.find(pF => pF.name.toLowerCase().includes('country')).value = this.countryOptions.find(opt => opt.value === addressObj[key]).label
                        }
                    } else {
                        this.prefillUponCreation.find(pF => pF.name.toLowerCase().includes(key.toLowerCase())).value = addressObj[key]
                    }
                }
            }.bind(this))
        } catch(e) {
            this.error(e, 'error', 'Erreur')
        }
    }

    handleCancel() {
        this.hideModal()
    }

    handleSuccess(event) {
        try{
            console.log(JSON.parse(JSON.stringify(event.detail)))
            const myPromise = new Promise ((resolve, reject) => {
                try {
                    this.sendData(event.detail.id)
                    resolve("true")
                } catch(e) {
                    reject(e)
                }
            })
            myPromise.then(
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: event.detail.id,
                    objectApiName: 'FIP_FIP__c',
                    actionName: 'view'
                }
            }))
        } catch(e) {
            this.error(e, 'error', 'Erreur')
        }
    }

    async sendData(id) {
        await this.template.querySelector('c-lwc_-f-i-p-component').updateComponents(id)
    }

    handleError(event) {
        console.log(JSON.parse(JSON.stringify(event.detail)))
        this.error(JSON.stringify(event.detail.detail), 'error', event.detail.message)
    }

}