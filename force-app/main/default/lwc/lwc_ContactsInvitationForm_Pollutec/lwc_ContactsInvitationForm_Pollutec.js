import { LightningElement, api, wire } from 'lwc'
import { CurrentPageReference } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'

import imgs from '@salesforce/resourceUrl/Pollutec'

import getRecordByGUID from '@salesforce/apex/SecheSystemUtils.getRecordByGUID'

const DICT = {
    'en_US': {
        'form_title_1': 'Evening',
        'form_title_2': 'Invite',
        'footer_notice': 'Legal Notice',
        'send_form': 'Submit form',
        'success_msg': 'Your submission has been sent',
        'page_not_found': 'Page not found',
        'already_submitted': 'You already have submitted this form',
        'error_max_exceeded': 'You cannot invite more than 10 guests to the Evening event.'
    },

    'fr': {
        'form_title_1': 'Invitation',
        'form_title_2': 'Soirée',
        'footer_notice': 'Mentions Légales',
        'send_form': 'Envoyer le formulaire',
        'success_msg': 'Votre formulaire a bien été envoyé',
        'page_not_found': 'Cette page n\'existe pas',
        'already_submitted': 'Vous avez déjà répondu au formulaire',
        'error_max_exceeded': 'Vous ne pouvez pas avoir plus de 10 accompagnateurs à la soirée.'
    }
}

export default class Lwc_ContactsInvitationForm_Pollutec extends LightningElement {
    success
    isLoading = false
    displayCompanions= false
    displayError = false
    @api guid
    @api v2
    firstName
    lastName
    email
    recordId
    pageNotFound = false
    errorMsg
    isLoadingOutput = true
    isLoadingInput = true
    langUp = false

    langHeight = 53.25
    formTop

    get found() {
        return !this.pageNotFound && !this.isLoading
    }

    get showForm() {
        return !this.isLoading && !this.success && !this.alreadyAnswered
    }

    get notFound() {
        return this.pageNotFound && !this.isLoading
    }

    get loading() {
        return this.isLoadingOutput || this.isLoadingInput                                        
    }

    @wire(CurrentPageReference)
    setCurrentPageReference(currentPageReference) {
        this.currentPageReference = currentPageReference;
    }

    handleLoadOutputForm() {
        this.isLoadingOutput = false
    }

    handleLoadInputForm() {
        try {
            this.isLoadingInput = false
            let form = this.template.querySelector('div.form')
            if(form) {
                this.formTop = form.getBoundingClientRect().top
            }
        } catch(e) {
            console.log(e)
        }
    }

    handleInvitee(event) {
        if (event.detail.value === 'Oui') {
            this.displayCompanions = true
        }
        if (event.detail.value === 'Non') {
            this.displayCompanions = false
        }
    }

    handleSuccess() {
        this.isLoading = false
        this.success = true
    }

    handleError(event) {
        this.isLoading = false
        let error = JSON.stringify(event.detail)
        if (error.includes('The requested resource does not exist')) {
            this.success = true
            return
        }
        
        this.showNotification('Error', JSON.stringify(event.detail), 'error')
    }

    handleSubmit(event) {

        this.isLoading = true
        
        event.preventDefault()
        try{

            let inputFields = event.detail.fields

            inputFields.Email__c = this.email
            inputFields.TECH_ToDelete__c = true
            inputFields.GUID__c = this.recordId

            if (inputFields.NumberOfCompanions__c > 10 ) {
                this.showNotification(DICT[this.lang].error_max_exceeded, null, 'warning')
                this.isLoading = false
                return
            }

            this.template.querySelector('lightning-record-edit-form').submit(inputFields)

        } catch (e) {
            this.isLoading = false
            this.error = true
            this.errorMsg = JSON.stringify(e)
        }
        
        
    }

    displayLanguageSelector() {
        try {
            let lang_selec = this.template.querySelector('div.language-selector-container')
            if(!this.langUp) {
                this.langUp = true
                lang_selec.style.top = (this.formTop - this.langHeight) + "px"
                lang_selec.style.opacity = "1"
            } else {
                this.langUp = false
                lang_selec.style.top = "300px"
                lang_selec.style.opacity = "0"
            }
        } catch(e) {
            console.log(e)
        }
    }

    showNotification(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        })
        this.dispatchEvent(evt)
    }

    async connectedCallback() {

        this.isLoading = true

        //this.mainStyle = 'background-image: url("'+imgs+'/Background.jpg");'
        this.mainStyle = 'background-image: url("'+imgs+'/SoireeFond.jpg");',
        this.main_logo_img = imgs + '/Brand.png'
        this.not_found_img = imgs + '/Not-Found.png'
        this.seche_img = imgs + '/Seche.png'
        this.warning_img = imgs + '/Warning.png'
        this.error_img = imgs + '/Error.png'
        this.brand_white_img = imgs + '/Brand_White.png'
        this.france_flag_img = imgs + '/FR.png'
        this.england_flag_img = imgs + '/EN.png'

        this.lang = this.currentPageReference.state.language

        this.form_title_1 = DICT[this.lang].form_title_1
        this.form_title_2 = DICT[this.lang].form_title_2
        this.footer_notice = DICT[this.lang].footer_notice
        this.send_form = DICT[this.lang].send_form
        this.success_msg = DICT[this.lang].success_msg
        this.page_not_found = DICT[this.lang].page_not_found
        this.already_submitted = DICT[this.lang].already_submitted
        

        getRecordByGUID({GUID: this.guid, objectApiName: 'Contacts_Invitation__c', GUID_FIELD: 'GUID__c'})
        .then(data => {
            if(data) {
                this.email = data.Email__c
                this.recordId = data.Id
                if(data.InviteeEventValidation__c != undefined) {
                    this.alreadyAnswered = true
                }
            } else {
                this.pageNotFound = true
            }
        })
        .finally(() => {
            setTimeout(function() {
                this.isLoading = false;
            }.bind(this), 2000)
        })
    }

    renderedCallback() {
        let form = this.template.querySelector('div.form')
        if(form) {
            this.formTop = form.getBoundingClientRect().top
            let lang_selec = this.template.querySelector('div.language-selector-container')
            lang_selec.style.top = (this.formTop - this.langHeight) + "px"
        }
    }

    changeLang(event) {
        try {
            window.location.search = window.location.search.replace(/language=[^&$]*/i, 'language='+event.target.dataset.lang);

            // window.location.assign(window.location.href)
        } catch(e) {
            console.log(e)
        }
    }

}