import { LightningElement, api, track } from 'lwc';
import { getCurrentYear} from 'c/ldsUtils';
import labelDomainName from '@salesforce/label/c.QLY_CommunityDomainName';

export default class LwcRequestInputForm extends LightningElement {

    @api userInfo;

    @track showAddressInput = false;
    @track showRequestInput = false;
    @track currentYear = getCurrentYear();
    
  
    // LWC lifecycle hooks
    connectedCallback() {       
        console.log('LwcRequestInputForm.connectedCallback.userInfo.isSameAddressForWaste', JSON.stringify(this.userInfo));       
            }  

    renderedCallback() {}


    get displayStreet(){
        if(this.userInfo.removalAddress){
            return this.userInfo.removalAddress.street;
        }else {
            return '';
        }
    }

    get firstCollectLabel(){
        return '1ère collecte mai/ juin ' +this.currentYear;
    }

    get secondCollectLabel(){
        return '2ème collecte octobre/ décembre ' +this.currentYear;
    }

    get truckUrl19T() {
        return labelDomainName + '/resource/QLY_TrucksIcon/19T.jpg'
    }

    get truckUrl7T() {
        return labelDomainName + '/resource/QLY_TrucksIcon/7T.jpg'
    }

    get truckUrl3T() {
        return labelDomainName + '/resource/QLY_TrucksIcon/3.5T.jpg'
    }
    
    get displayCity(){
        if(this.userInfo.removalAddress){
            return this.userInfo.removalAddress.city;
        }else {
            return '';
        }
    }

    get displayCountry(){
        if(this.userInfo.removalAddress){
            return this.userInfo.removalAddress.country;
        }else {
            return '';
        }
    }

    get displayPostalCode(){
        if(this.userInfo.removalAddress){
            return this.userInfo.removalAddress.postalCode;
        }else {
            return '';
        }
    }  

    //method to send form inout field to parent (lwc-Main)
    handleUserInputChange(event) {
        const field = event.target.name;          
        const fieldType = event.target.dataset.type;
        const time = event.target.dataset.time;
        const rank = event.target.dataset.rank;
        const when = {rank : rank, day : field, time : time};
        //to notify where the event fire from
        const eventOrigin = 'fromChild';

        console.log('LwcRequestInputForm.handleUserInputChange.field === ', field);        
        console.log('LwcRequestInputForm.handleUserInputChange.fieldType === ', fieldType); 
        console.log('LwcRequestInputForm.handleUserInputChange.time === ', time);        
        console.log('LwcRequestInputForm.handleUserInputChange.rank === ', rank); 
        
        if (fieldType.includes("Address")) {  //fieldType == address            
            console.log('Street => ', event.target.street);
            console.log('City => ', event.target.city);
            console.log('Province => ', event.target.province);
            console.log('Country => ', event.target.country);
            console.log('postal Code => ', event.target.postalCode);
            let address = {
                street: event.target.street,
                city: event.target.city,
                province: event.target.province,
                country: event.target.country,
                postalCode: event.target.postalCode
            };

            console.log('LwcRequestInputForm.handleUserInputChange.address === ', address); 

            let userInputValue = { field: field, fieldType: fieldType, inputValue: address, eventOrigin : eventOrigin};
            const userInputEvent = new CustomEvent(
                "userinputvalue", { detail: userInputValue }
            );
            this.dispatchEvent(userInputEvent);                     

        }
        else if (fieldType === 'checkbox') { //fieldType == checkbox          
            const inputValue = event.target.checked; 
            console.log('LwcRequestInputForm.handleUserInputChange.inputValue === ', inputValue);           
            let userInputValue = { field: field, inputValue: inputValue, fieldType: fieldType, eventOrigin : eventOrigin, when : when};
            const userInputEvent = new CustomEvent(
                "userinputvalue", { detail: userInputValue }
            );
            this.dispatchEvent(userInputEvent);
           
        } else { //fieldType input
            const inputValue = event.target.value;   
            console.log('LwcRequestInputForm.handleUserInputChange.inputValue === ', inputValue); 
            let userInputValue = { field: field, fieldType: fieldType, inputValue: inputValue, eventOrigin : eventOrigin };
            const userInputEvent = new CustomEvent(
                "userinputvalue", { detail: userInputValue }
            );
            this.dispatchEvent(userInputEvent);           
        }

    }
}