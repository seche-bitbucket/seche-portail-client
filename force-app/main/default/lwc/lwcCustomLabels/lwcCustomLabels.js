import { LightningElement } from 'lwc';
import step1 from '@salesforce/label/c.lwcStep1';
import step2 from '@salesforce/label/c.lwcStep2';
import step3 from '@salesforce/label/c.lwcStep3';
import lwcYourInfo from '@salesforce/label/c.lwcYourInfo';
import lwcYourServices from '@salesforce/label/c.lwcYourServices';
import lwcSummaryValidation from '@salesforce/label/c.lwcSummaryValidation';

export default class lwcCustomLabels extends LightningElement  {
    //lwc main Labels
    label = {step1, step1, step1, lwcYourInfo, lwcYourServices, lwcSummaryValidation };

}