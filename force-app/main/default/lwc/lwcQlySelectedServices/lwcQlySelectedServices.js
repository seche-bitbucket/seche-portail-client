import { LightningElement, api, track, wire } from 'lwc';
import { getPicklistValues, getObjectInfo } from 'lightning/uiObjectInfoApi';
import QLY_SERVICE_OBJECT from '@salesforce/schema/QLY_Service__c';
import PACKAGING_FIELD from '@salesforce/schema/QLY_Service__c.Packaging__c';

export default class LwcQlySelectedServices extends LightningElement {

    @api userSelectedServices;   
    @api showAction = false;
    @api haveSelectedWaste;  
    @api itemDisabled = false; 
    @api formId;

    @track listSelectedWaste =[];
    @track keyIndex = 0;
    @track options;
    value = '';

   
     // LWC lifecycle hooks
     connectedCallback() {
         if( this.userSelectedServices){
            console.log('LwcQlySelectedServices.connectedCallback.this.userSelectedServices.haveSelectedWaste ',this.haveSelectedWaste); 
            console.log('LwcQlySelectedServices.connectedCallback.this.userSelectedServices.listSelectedWaste ', JSON.stringify(this.userSelectedServices.listSelectedWaste)); 
            console.log('LwcQlySelectedServices.connectedCallback.this.userSelectedServices.listSelectedContainer ', JSON.stringify(this.userSelectedServices.listSelectedContainer)); 
            console.log('LwcQlySelectedServices.connectedCallback.this.userSelectedServices.listSelectedPrestation ',JSON.stringify(this.userSelectedServices.listSelectedPrestation)); 
            
         }
       
    }
    
    renderedCallback() {

        /*let containerDiv = this.template.querySelector(".container");
        console.log('container html elmnt: '+containerDiv);
        containerDiv.scrollTop = containerDiv.scrollHeight;*/

    }

    @wire(getObjectInfo, { objectApiName: QLY_SERVICE_OBJECT })
    objectMetadata;  

    @wire(getPicklistValues, { recordTypeId: '$objectMetadata.data.defaultRecordTypeId', fieldApiName: PACKAGING_FIELD })
    getPicklistValues({ error, data }) {
        if (data) {
            // Map picklist values
            this.options = data.values.map(plValue => {
                return {
                    label: plValue.label,
                    value: plValue.value
                };
            });

        } else if (error) {
            // Handle error
            throw new Error(error);
            
        }
    }

    get containerStyle(){
        if(!this.itemDisabled){
            return 'container itemTable';
        }else{
            return 'container-summary';
        }
    }

    get productNameStyle() {
        return 'slds-p-around_x-small';
    }

    handleRowAction(event) {
        console.log('LwcQlySelectedServices.handleRowAction.productstart ---', event.detail.productName);
        console.log('LwcQlySelectedServices.handleRowAction.name start ---', event.detail.name);
        console.log('LwcQlySelectedServices.handleRowAction.action start ---', event.detail.action);
        console.log('LwcQlySelectedServices.handleRowAction.index start ---', event.detail.currentIndex);

        const productName =  event.detail.productName;
        const action = event.detail.action;
        const field = event.detail.name;
        const currentIndex = event.detail.currentIndex;
        const unitPrice = event.detail.unitPrice

        let processAction = { productName : productName, name: field, action : action, currentIndex : currentIndex, unitPrice:unitPrice,packaging: event.detail.packaging, options: event.detail.options};
        const rowaction = new CustomEvent(
            "rowaction", { detail: processAction }
        );
        this.dispatchEvent(rowaction);

    }

    handleUserInputChange(event) {
        console.log('LwcQlySelectedServices.handleUserInputChange.product start ---', event.detail.productName);
        console.log('LwcQlySelectedServices.handleUserInputChange. event.target.name; start ---',  event.detail.field);
        console.log('LwcQlySelectedServices.handleUserInputChange. event.target.value start ---',event.detail.inputValue);
        console.log('LwcQlySelectedServices.handleUserInputChange.displayStyle start ---', event.detail.displayStyle);
        console.log('LwcQlySelectedServices.handleUserInputChange.currentIndex start ---', event.detail.currentIndex);
        console.log('LwcQlySelectedServices.handleUserInputServiceChange.eventOrigin start ---', event.detail.eventOrigin);

        const productName =  event.detail.productName;
        const field = event.detail.field;       
        const displayStyle = event.detail.displayStyle;
        const currentIndex =event.detail.currentIndex; 
        const inputValue = event.detail.inputValue;      
        const eventOrigin = event.detail.eventOrigin;
           
        let userInputValue = { field: field, inputValue: inputValue, displayStyle : displayStyle, currentIndex : currentIndex, productName : productName, eventOrigin : eventOrigin};
        const userInputEvent = new CustomEvent(
            "userinputvalue", { detail: userInputValue }
        );
        this.dispatchEvent(userInputEvent);

    }

    handleUpload(event){

        console.log('LwcQlySelectedServices.handleUpload event.detail: '+JSON.stringify(event.detail.pendingFilesIds));

        const pendingFilesIds =  event.detail.pendingFilesIds;

        let processAction = { pendingFilesIds : pendingFilesIds };
        const uploadfiles = new CustomEvent(
            "uploadfiles", { detail: processAction }
        );
        this.dispatchEvent(uploadfiles);

    }

    get 
    noSelectedWaste(){
       return (this.userSelectedServices.listSelectedWaste.length > 0);
    }

    selectHandler(event) {
        // Prevents the anchor element from navigating to a URL.
        event.preventDefault();
        //used to send selected flux infos
        let  selectedFlux = {action:"select", fluxId:this.flux.id};
        // Creates the event with the slected flux data.
        const selectedEvent = new CustomEvent('selected', { detail:selectedFlux});
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
    }

    editRow(event) {
         // Prevents the anchor element from navigating to a URL.
         event.preventDefault();
         let  selectedFlux = {action:"edit", fluxId:this.flux.id}; 
         // Creates the event with the slected flux data.
         const selectedEvent = new CustomEvent('selected', { detail:selectedFlux}); 
         // Dispatches the event.
         this.dispatchEvent(selectedEvent);
     /*   ++this.keyIndex;
        var newItem = [{ id: this.keyIndex }];
        this.itemList = this.itemList.concat(newItem);*/
    }

    removeRow(event) {
          // Prevents the anchor element from navigating to a URL.
         event.preventDefault();
          //used to send selected flux infos
         let  selectedFlux = {action:"remove", fluxId:this.flux.id}; 
         // Creates the event with the slected flux data.
         const selectedEvent = new CustomEvent('selected', { detail:selectedFlux}); 
         // Dispatches the event.
         this.dispatchEvent(selectedEvent);

       /* if (this.itemList.length >= 2) {
            this.itemList = this.itemList.filter(function (element) {
                return parseInt(element.id) !== parseInt(event.target.accessKey);
            });
        }*/
    }
}