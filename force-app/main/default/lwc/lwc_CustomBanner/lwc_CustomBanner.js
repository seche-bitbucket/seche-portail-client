import { LightningElement, api, track, wire } from 'lwc';
import { getObjectInfo, getPicklistValues } from 'lightning/uiObjectInfoApi';
// import getAccount from '@salesforce/apex/AP_DDE_Manager.getAccount';
import save from '@salesforce/apex/LWC_CustomBannerCtrl.save';
import getObjectApiName from '@salesforce/apex/LWC_CustomBannerCtrl.getObjectApiName';
import WasteCollectRequest__c from '@salesforce/schema/WasteCollectRequest__c';
import RequestType__c from '@salesforce/schema/WasteCollectRequest__c.RequestType__c';
import getRecordTypeId from '@salesforce/apex/LWC_CustomBannerCtrl.getRecordTypeId';
import insertWithDML from '@salesforce/apex/LWC_CustomBannerCtrl.customInsert';
import custom from '@salesforce/apex/LWC_CustomBannerCtrl.custom';
import init from '@salesforce/apex/LWC_CustomBannerCtrl.init';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import baseUrl from '@salesforce/apex/LWC_CustomBannerCtrl.getUrl';
import Id from '@salesforce/user/Id';

import basePath from '@salesforce/community/basePath';

import getFieldsLabelAndValue from '@salesforce/apex/LWC_CustomBannerCtrl.getFieldsLabelAndValue';

import jQuery from '@salesforce/resourceUrl/jQuery';
import { loadScript } from 'lightning/platformResourceLoader';

export default class Lwc_CustomBanner extends LightningElement {

    @api recordId;
    @api fields;
    @api fieldsToEdit;
    @api title;
    @api forListView; 
    @api listViewObjectApiName;
    @api newFieldsRequired;
    @api newRecordTypeName;

    newRecordTypeId;

    fields_new = [];
    oApiName = (this.listViewObjectApiName != undefined) ? this.listViewObjectApiName : 'Case';

    object = [];
    headTitle;
    userId = Id;

    @track record = {};
    date;

    disable;

    wait;

    @wire(getObjectInfo, { objectApiName: '$listViewObjectApiName' }) 
    objectInfo;

    account;
    edit;
    @track button_label = 'Modifier';

    @track showHover = false;
    showAddress = false;

    @track objColor;
    @track objIcon;

    @track modalStyle;

    picklistValues = [];

    @track img;
    @track render_this = false;

    @track size = {};

    config;

    connectedCallback() {

        this.wait = (this.forListView) ? false : true;

        if(this.recordId != undefined && this.recordId.length > 0){
            init({o: this.recordId, recordId: this.recordId})
            .then(data => {
                if(data != null) {
                    this.config = data;
                }
            });
        }

        if(this.newRecordTypeName != undefined) {
            getRecordTypeId({name: this.newRecordTypeName})
            .then(data => {
                console.log(data);
                if(data) {
                    this.newRecordTypeId = data;
                }
            })
        }

        if(this.newFieldsList != undefined) {
            this.newFieldsList.forEach(f => { 
                if(this.newFieldsRequired != undefined && this.newFieldsRequired.includes(f)) {
                    this.fields_new.push({field: f, required: true});
                } else {
                    this.fields_new.push({field: f, required: false});
                }
            });
            this.render_this = true;
        }

        Promise.all([
            loadScript(this, jQuery),
        ]).then(() => {
            console.log('script loaded sucessfully');
        }).catch(error => {
            this.error = error;
            console.log(error);
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error!!',
                    message: error,
                    variant: 'error',
                }),
            );
        });

        if(this.recordId) {
            getObjectApiName({recordId: this.recordId})
            .then(data => {
                this.oApiName = data;
            })
        }

        baseUrl()
        .then(data => {
            this.url = data+basePath;
        })

        try{
            if(!this.forListView) {
                this.loadAll();
            }
        } catch(e) {
            console.log(e);
        }

    }

    blockSubmit;
    toggle;

    onsubmitOverride(event) {
        event.preventDefault();       // stop the form from submitting
        var fields = event.detail.fields;
        fields.RecordTypeId = this.newRecordTypeId; // modify a field
        insertWithDML({o: this.listViewObjectApiName, data: JSON.stringify(fields), apiName: this.listViewObjectApiName})
        .then(data => {
            this.open = false;
            window.location.href = this.url+'/'+this.listViewObjectApiName.toLowerCase()+'/'+data;
        })
    }

    loadAll() {

        //make sure to add title field
        let lst = [];
        lst.push.apply(lst, this.fields.split(','));

        if(this.title.split(';').length > 1) {
            this.title.split(';')[1].split(',').forEach(f => {
                if(!lst.includes(f)) lst.push(f);
            })
        }

        this.object = [];
        
        getFieldsLabelAndValue({o: this.recordId, recordId: this.recordId, fields: lst.join(',')})
        .then(data => {
            if(data) {
                
                console.log(data);

                if(this.title.includes(';')) {
                    var p = this.title.split(';');
                    if(p.length > 0) {
                        this.headTitle = this.title.split(';')[0];
                        this.title.split(';')[1].split(',').forEach(f => {
                            console.log(f);
                            this.headTitle += ' '+data.find(d => d.name == f).value;
                        })
                    } else {
                        this.headTitle = p;
                    }
                } else {
                    this.headTitle = this.title;
                }
                this.record = {Id: this.recordId};
                
                if(this.fields != undefined && this.fields.length > 0) {
                    this.fields.split(',').forEach(f => {

                        let el = data.find(d => d.name == f);

                        let editable = false;

                        if(this.fieldsToEdit != undefined && this.fieldsToEdit.includes(f)) {
                            editable = true
                        }

                        this.record[f] = el.value;
                        this.object.push({name: f, label: el.label, value: el.value, editable: editable});
                    });
                }

            }
        })
        .then(() => {
            this.wait = false;
        })
    }

    @wire(getObjectInfo, { objectApiName: '$oApiName' })
    handleResult({error, data}) {
        console.log(error);
        console.log(error);
        if(data) {
            let objectInformation = data;


            this.objColor = "background-color: #"+objectInformation.themeInfo.color;
            this.objIcon = objectInformation.themeInfo.iconUrl;

            // access theme info here
            // icon URL is availe as themeInfo.iconUrl
            //this.themeInfo = objectInformation.themeInfo || {};
        }
        if(error) {
            // handle error
        }
    }

    @wire(getObjectInfo, { objectApiName: WasteCollectRequest__c })
    objInfos;

    // now get the industry picklist values
    @wire(getPicklistValues,

        {
            recordTypeId: '$objInfos.data.defaultRecordTypeId', 
            fieldApiName: RequestType__c
        }

    )
    typePicklistValues;

    open = false;

    newRecord(event) {
        $('body').css('overflow', 'hidden');
        this.open = true;
    }

    handleCancel(event) {
        $('body').css('overflow', 'auto');
        this.open = false;
    }

    handleOnOver(event) {

        try{
            
            var bb = this.template.querySelector('[data-index="'+event.target.dataset.index+'"]').getBoundingClientRect();
            // Correct and works with touch.
            this.position = {
                b: bb.bottom,
                x: bb.left,
                y: bb.top,
                h: bb.height,
                w: bb.width,
                r: bb.right
            }

            let eltTop = parseFloat(this.position.y);
            let processed1 = eltTop-61;
            let processed2 = parseFloat(this.position.r) + 30;

            this.modalStyle = "top: "+processed1+"px !important; left: "+processed2+"px !important; visibility: visible !important; z-index: 10 !important;";
            this.showHover = true;

        } catch(e) {
            console.log(e);
        }

    }

    handleMouseOut(event) {

         this.showHover = false;
         this.modalStyle = "visibility: hidden !important; z-index: -1 !important;";

    }

    handleOnEdit(){

        this.edit = (this.edit) ? false: true;
        if(this.button_label == 'Enregistrer') {
            this.button_label = 'Modifier';
            try{

                this.wait = true;
                let rcd = this.record;
                if(rcd.accountObj != undefined) delete rcd.accountObj;
                save({o: this.recordId, metadata: JSON.stringify([rcd]), dataType: this.oApiName})
                .then( error => {
                        this.loadAll();
                });
            } catch(e) {
                console.log(e);
            }
        } else {
            this.button_label = 'Enregistrer';
        }
        
    }

    instruction='';

    handleChange(event) {
        this.record[event.target.name] = event.target.value;
    }

    popupShow = false;

    openPopup(event){
        $('body').css('overflow', 'hidden');
        this.popupShow = true;
        this.popupOpened = false;
    }

    closeScreen(event) {
        this.popupShow = false;
        delete this.action;
        delete this.popUpData
        $('body').css('overflow', 'auto');
    }

    popupSpinner = false;
    handleLoad(event) {
        this.popupSpinner = true;
    }

    popupOpened = false;
    action;
    @track popupTitle;
    @track popupBody;
    @track popupField;

    customAction(event) {
        if(event.target.dataset.action) {
            let action = event.target.dataset.action;
            this.action = action;
        }

        if(event.target.dataset.popup) {
            this.popupOpened = true;
            this.popupTitle = event.target.dataset.popupTitle;
            this.popupBody = event.target.dataset.popupBody;
            this.popupField = JSON.parse(event.target.dataset.popupField);
            this.openPopup();
        } else if(!event.target.dataset.popup) {

            this.wait = true;

            this.template.querySelectorAll('[data-id="popupForm"] lightning-input-field').forEach(item => {
                let fieldValue=item.value;
                let fieldLabel=item.label; 
                let fieldRequired = item.required;
                if(fieldRequired && !fieldValue){
                    return;
                }
            });
            
            const fields = event.detail.fields;
            this.template.querySelector('lightning-record-edit-form').submit(fields);

            custom({o: this.recordId, recordJSON: JSON.stringify(this.record), action: this.action})
            .then(data => {
                
                if(data.source != undefined) {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Unexpected Error',
                            message: data.message+' at '+data.line,
                            variant: 'error',
                        })
                    );
                    return;
                }

                this.dispatchEvent(
                    new ShowToastEvent({
                        title: data.title,
                        message: data.message,
                        variant: data.type,
                    })
                );

                if(data.reload) {
                    setTimeout(function() {
                        this.wait = false;
                        $('body').css('overflow', 'auto');
                        window.location.reload();
                    }, 2000);
                } else {
                   this.wait = false; 
                }

            })
        }
    }

    get lockRecord() {
        return (this.config != null) ? this.config.disabled : false;
    }

    get additionalDetail() {
        return (this.config != null) ? this.config.additionalDetail : [];
    }

    get addonHtml() {
        return (this.config != null) ? this.config.htmlAddons : [];
    }

    get customButtons() {
        return (this.config != null) ? this.config.customButtons : [];
    }

    get titleClass() {

        if(this.config != undefined && this.config.headerSize) {
            return this.config.headerSize;
        } else if(this.forListView) {
            return 'slds-col slds-size_9-of-12';
        } else if(this.lockRecord) {
            return 'slds-col slds-size_10-of-12';
        }

    }

    get multiCondition() {
        return this.fields.split(',').length > 0 && !this.forListView;
    }

    get newFieldsList() {
        if(this.fields != undefined)
            return this.fields.split(',');
    }

    get recordTypeId() { 

        const rtis = this.objectInfo.data.recordTypeInfos; 
    
        return Object.keys(rtis).find(rti => rtis[rti].name === 'Requête Demande d\'enlèvement'); 
    
    }

    get hideEdit() {
        return !(this.fieldsToEdit != undefined && this.fieldsToEdit.length > 0);
    }

}