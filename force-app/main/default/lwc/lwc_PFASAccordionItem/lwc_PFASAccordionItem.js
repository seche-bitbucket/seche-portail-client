import { LightningElement, api, track } from 'lwc';
import { loadScript } from "lightning/platformResourceLoader";

import jsdict from '@salesforce/resourceUrl/PFAS_Translation'
import LightningAlert from 'lightning/alert';

import sendData from '@salesforce/apex/PFAS_Controller.sendData';
import getSubstances from '@salesforce/apex/PFAS_Controller.getSubstances';

import imgs from '@salesforce/resourceUrl/Pollutec'

export default class Lwc_PFASAccordionItem extends LightningElement {

    // Event Data
    @api item
    @api recordSObjectType
    @api language

    // Contextual Vars
    @track record = {}
    @track pComponents = []
    @track npComponents = []
    @track softComponent = {
        PfasType__c: '',
        isPolymere: false
    }
    @track substanceNameHelpText
    @track substanceTypeLabel
    @track helpTextPoly
    @track substanceName
    @track CASNumber
    @track noSubstance
    @track chooseSubstance
    recordId
    components = []
    substanceTypes
    substanceType
    substanceTypeValue
    numberOfComponent
    componentFormLoaded = 0

    // Loading Vars
    waiting = true

    async connectedCallback() {
        await loadScript(this, jsdict+"/PFAS_TranslationDict.js")
        this.DICT = PFAS_Dict.getDict()
        this.substanceNameHelpText = this.DICT[this.language]?.substanceNameHelpText
        this.substanceTypeLabel = this.DICT[this.language]?.substanceTypeLabel
        this.helpTextPoly = this.DICT[this.language]?.helpTextPoly
        this.substanceName = this.DICT[this.language]?.substanceName
        this.CASNumber = this.DICT[this.language]?.CASNumber
        this.noSubstance = this.DICT[this.language]?.noSubstance
        this.chooseSubstance = this.DICT[this.language]?.chooseSubstance

        this.main_logo_img = imgs + '/Brand.png'
        this.substanceTypes = await getSubstances()
        this.item?.Constituants__r?.forEach(element => {
            let record = JSON.parse(JSON.stringify(element))
            record.localVars = {
                type: (record.PfasType__c === 'Polymère') ? 'P' : 'NP',
                typeP: (record.PfasType__c === 'Polymère'),
                class: (record.PfasType__c === 'Polymère') ? 'polymer' : '',
                newCAS: (record.ComponentType__c === 'Autre')
            }
            if(record.Id.includes(record.Name)) {
                record.Name = ''
            }
            this.components.push(record)
        })
        this.pComponents = this.components.filter(item => item.localVars.typeP)
        this.npComponents = this.components.filter(item => !item.localVars.typeP)
        this.numberOfComponent = this.components.length
        this.record = (this.item) ? JSON.parse(JSON.stringify(this.item)) : {HasPfas__c: ''}

        delete this.record?.Constituants__r
        delete this.record?.localVars
        this.recordId = this.item?.Id

        if(this.numberOfComponent === 0) {
            this.waiting = false
        }

        await this.sendEventToApp({Name: 'componentschange', Data: {FIP_FIP__c: (this.recordSObjectType === 'FIP_FIP__c') ? this.record : undefined, CAP__c: (this.recordSObjectType === 'CAP__c') ? this.record : undefined, Components: this.components, Name: 'componentschange'}})
        this.waiting = false
    }

    get noData() {
        return this.components.length === 0 && this.waiting === false
    }

    get displayPComponentsRows() {
        return this.pComponents.length > 0 && this.waiting === false
    }

    get displayNPComponentsRows() {
        return this.npComponents.length > 0 && this.waiting === false
    }

    get disablePfasType() {
        return !this.record.HasPfas__c || this.record.HasPfas__c === 'Non' || this.record.HasPfas__c === ''
    }

    get isPolymere() {
        return this.softComponent.PfasType__c === 'Polymère'
    }

    get disableInputs() {
        return this.waiting || this.disablePfasType
    }

    get disableAddSubstance() {
        return this.disableInputs || this.softComponent.PfasType__c === undefined || this.softComponent.PfasType__c === ''
    }

    get displayTableInfo() {
        return this.displayNPComponentsRows || this.displayPComponentsRows
    }

    handleSubstanceChange(event) {
        this.substanceType = event.detail.value
        this.substanceTypeValue = this.substanceTypes.find(substance => substance.value === event.detail.value)?.label
    }

    async handleAddComponent(event) {
        event.preventDefault()

        let componentArray = []

        if(!this.record.HasPfas__c) {
            await LightningAlert.open({
                message: 'Veuillez remplir tous les champs du complément de FIP.',
                theme: 'error', // a red theme intended for error states
                label: 'Erreur lors de la création d\'une substance!', // this is the header text
            });
            return
        }

        this.waiting = true
        
        if(this.substanceTypeValue) {
            this.softComponent.Name = this.substanceTypeValue
            this.softComponent.ComponentType__c = this.substanceType
        }

        this.softComponent.CAP__c = this.recordId

        if(this.softComponent.Name.includes(',') && this.softComponent.isPolymere) {
            const multiPolymere = this.softComponent.Name
            multiPolymere.split(',').forEach(polymere => {
                let copy = JSON.parse(JSON.stringify(this.softComponent))
                copy.Name = polymere
                delete copy.localVars
                delete copy.isPolymere
                componentArray.push(copy)
            })
        }

        const recordInput = (componentArray.length > 0) ? componentArray : [this.softComponent]
        
        try {
            const createdRecord = await sendData({processName: 'insertComponents', jsonData: JSON.stringify(recordInput)})
            JSON.parse(JSON.stringify(createdRecord)).forEach(record => {
                let recordFields = {
                    Id: record.Id,
                    Name: record.Name,
                    ComponentType__c: record.ComponentType__c,
                    CasNumber__c: record.CasNumber__c,
                    MinimumPercentage__c: record.MinimumPercentage__c,
                    MaximumPercentage__c: record.MaximumPercentage__c
                }

                recordFields.localVars = {
                    type: (record.PfasType__c === 'Polymère') ? 'P' : 'NP',
                    typeP: (record.PfasType__c === 'Polymère'),
                    class: (record.PfasType__c === 'Polymère') ? 'polymer' : '',
                    newCAS: (record.ComponentType__c === 'Autre')
                }

                if(recordFields.Id.includes(record.Name)) {
                    recordFields.Name = ''
                }
                this.components.push(recordFields)        
                this.pComponents = this.components.filter(item => item.localVars.typeP)
                this.npComponents = this.components.filter(item => !item.localVars.typeP)       
            })
            await this.sendEventToApp({Name: 'componentschange', Data: {FIP_FIP__c: (this.recordSObjectType === 'FIP_FIP__c') ? this.record : undefined, CAP__c: (this.recordSObjectType === 'CAP__c') ? this.record : undefined, Components: this.components, Name: 'componentschange'}})
            delete this.substanceType
        } catch(e) {
            await LightningAlert.open({
                message: e,
                theme: 'error', // a red theme intended for error states
                label: 'Erreur', // this is the header text
            });
        }

        this.waiting = false
    }

    handleComponentFormLoad() {
        this.componentFormLoaded++

        if(this.componentFormLoaded === this.numberOfComponent) {
            this.waiting = false
        }
    }

    async handleDeleteComponent(event) {
        this.waiting = true
        event.preventDefault()
        const recordInput = [event.currentTarget.dataset.componentId]
        try {
            const result = await sendData({processName: 'deleteComponents', jsonData: JSON.stringify(recordInput)})
            this.components = this.components.filter(element => element.Id !== recordInput[0])
            this.pComponents = this.components.filter(item => item.localVars.typeP)
            this.npComponents = this.components.filter(item => !item.localVars.typeP)    
            await this.sendEventToApp({Name: 'componentschange', Data: {FIP_FIP__c: (this.recordSObjectType === 'FIP_FIP__c') ? this.record : undefined, CAP__c: (this.recordSObjectType === 'CAP__c') ? this.record : undefined, Components: this.components, Name: 'componentschange'}})
        } catch(e) {
            console.log(e)
            console.log(JSON.parse(JSON.stringify(e)))
        }
        this.waiting = false
    }

    async sendEventToApp(event) {
        await this.dispatchEvent(new CustomEvent(event.Name, {detail: event.Data}), {bubbles : true, composed : false})
    }

    async handleRecordChanges(event) {
        if(event.target.name === 'HasPfas') {
            this.record.HasPfas__c = event.detail.value
        }

        if(this.record.HasPfas__c === 'Non') {
            this.record.PfasType__c = ''
            this.record.PolymeresList__c = ''
        }

        if(this.record.HasPfas__c === 'Oui' && this.record.PfasType__c === 'Non-Ploymère') {
            this.record.PolymeresList__c = ''
        }

        await this.sendEventToApp({Name: 'componentschange', Data: {FIP_FIP__c: (this.recordSObjectType === 'FIP_FIP__c') ? this.record : undefined, CAP__c: (this.recordSObjectType === 'CAP__c') ? this.record : undefined, Components: this.components, Name: 'componentschange'}})
    }

    async handleComponentChanges(event) {
        if(event.currentTarget.dataset.recordId) {
            if(event.target.name === 'Name') {
                this.components.find(element => element.Id === event.currentTarget.dataset.recordId).Name = event.detail.value
            }

            if(event.target.name === 'CasNumber__c') {
                this.components.find(element => element.Id === event.currentTarget.dataset.recordId).CasNumber__c = event.detail.value
            }

            if(event.target.name === 'MinimumPercentage__c') {
                this.components.find(element => element.Id === event.currentTarget.dataset.recordId).MinimumPercentage__c = event.detail.value
            }

            if(event.target.name === 'MaximumPercentage__c') {
                this.components.find(element => element.Id === event.currentTarget.dataset.recordId).MaximumPercentage__c = event.detail.value
            }

            await this.sendEventToApp({Name: 'componentschange', Data: {FIP_FIP__c: (this.recordSObjectType === 'FIP_FIP__c') ? this.record : undefined, CAP__c: (this.recordSObjectType === 'CAP__c') ? this.record : undefined, Components: this.components, Name: 'componentschange'}})
            return
        }

        if(event.target.name === 'Name') {
            this.softComponent.Name = event.detail.value
        }

        if(event.target.name === 'PfasType__c') {
            this.softComponent.PfasType__c = event.detail.value
            this.softComponent.isPolymere = this.softComponent.PfasType__c === 'Polymère'
        }
    }

}