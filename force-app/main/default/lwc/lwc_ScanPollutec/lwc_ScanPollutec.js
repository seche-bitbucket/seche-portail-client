// barcodeScannerContinuous.js
import { LightningElement, track } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { getBarcodeScanner } from "lightning/mobileCapabilities";
import codeValidation from "@salesforce/apex/ScanPollutec.CodeValidation";

export default class Lwc_ScanPollutec extends LightningElement {
  sessionScanner;
  @track scannedBarcodes;
  codeValid;
  @track showModal = false;


  connectedCallback() {
    this.sessionScanner = getBarcodeScanner();
    //this.beginScanning()
  }

  closeModal() {
    this.showModal = false;
    this.beginScanning();
  }

  beginScanning() {
    // Reset scannedBarcodes before starting new scanning session
    this.scannedBarcodes = [];

    // Make sure BarcodeScanner is available before trying to use it
    if (this.sessionScanner != null && this.sessionScanner.isAvailable()) {
      const scanningOptions = {
        barcodeTypes: [this.sessionScanner.barcodeTypes.QR],
        instructionText: "Scannez les QR Codes — Cliquez ✖︎ quand vous avez terminé",
        successText: "Code bien scanné.",
        showSuccessCheckMark: false,
        previewBarcodeData: false,
      };
      this.sessionScanner
        .beginCapture(scanningOptions)
        .then(async (scannedBarcode) => {
          await this.processScannedBarcode(scannedBarcode);
          if (this.codeValid == 'false') {
            this.showNotification('Erreur', 'QR code non valide', 'error')
            this.sessionScanner.endCapture();
          } else if (this.codeValid == 'true') {
            this.showModal = true;
            this.sessionScanner.endCapture();
            setTimeout(() => {
              this.closeModal();
            }, 2000); 
          } else {
            this.showNotification('Erreur', 'QR code déja validé le : ' + this.codeValid , 'error')
            this.sessionScanner.endCapture();
          }
          
          //this.continueScanning();
        })
        .catch((error) => {
          this.processError(error);
          //this.showNotification('Erreur', JSON.stringify(error), 'error')
          this.sessionScanner.endCapture();
        });
    } else {
      console.log("Scanner de QR Codes indisponible. Êtes-vous bien sur l'application mobile Salesforce?");
      this.showNotification('Erreur', "Scanner de QR Codes indisponible. Êtes-vous bien sur l'application mobile Salesforce?", 'error')
    }
  }

  async continueScanning() {
    // Pretend to do some work; see timing note below.
    await new Promise((resolve) => setTimeout(resolve, 1000));
    this.sessionScanner
      .resumeCapture()
      .then((scannedBarcode) => {
        this.processScannedBarcode(scannedBarcode);
        if (!this.codeValid) {
          this.showNotification('Erreur', 'QR code non valide', 'error')
          this.sessionScanner.endCapture();
        } 
        this.continueScanning();
      })
      .catch((error) => {
        this.processError(error);
        //this.showNotification('Erreur', error, 'error')
        this.sessionScanner.endCapture();
      });
  }

  async processScannedBarcode(barcode) {
    this.codeValid = await codeValidation({url: JSON.stringify(barcode)})
    /* .then(result => {
      this.codeValid = result;
    }) */
  }

  processError(error) {
    // Check to see if user ended scanning
    if (error.code == "userDismissedScanner") {
      console.log("User terminated scanning session via Cancel.");
    } else {
      console.error(error);
    }
  }

  get scannedBarcodesAsString() {
    return this.scannedBarcodes
      .map((barcodeResult) => {
        return barcodeResult.value;
      })
      .join("\n\n");
  }

  showNotification(title, message, variant) {
    const evt = new ShowToastEvent({
        title: title,
        message: message,
        variant: variant,
    });
    this.dispatchEvent(evt);
}
}