import { LightningElement, api, track, wire } from 'lwc';
import { generateRandomId } from 'c/ldsUtils';
import QLY_SERVICE_OBJECT from '@salesforce/schema/QLY_Service__c';
import PACKAGING_FIELD from '@salesforce/schema/QLY_Service__c.Packaging__c';
import { getPicklistValues, getObjectInfo } from 'lightning/uiObjectInfoApi';
import getContentVersionFromForm from '@salesforce/apex/AP_QLyManager.getContentVersionFromForm';
import deleteContentDocument from '@salesforce/apex/AP_QLyManager.deleteContentDocument';
import piklistValues from '@salesforce/apex/AP_QLyManager.getPiklistValues';


export default class LwcQlyPrestations extends LightningElement {
    //list products from PriceBook
    @api listProducts;
    //list to save all selected user services
    @api userSelectedServices;
    @api formId;
    @api wasteData;
    @api globalWasteFiles;

    @track showContainerDIV;

    @track listSelectedPresta = [];
    @track listSelectedWaste = [];
    @track listSelectedContainer = [];
    @track header;
    @track size = 'small';
    @track multiSelect = true;
    @track vertical = true; 
    
    @track displaySelectedWasteForm = true;
    @track displaySelecteContainerForm = false; 
   
    @track productArray = [];
    @track isFilesUploaded = false;

    @track openModal = false;
    waitingApproval;

    SERVICE_TYPE = 'Collecte et service';
    CONTAINER_TYPE = 'Contenant';
    WASTE_TYPE = 'Traitement déchets';  

    @track isDisabled = false;
    @track options;
    @track existingFiles = [];

    @api containerPackaging;
    @api topElement;

    start = false;

    // LWC lifecycle hooks
    connectedCallback() {

        if(this.userSelectedServices.listSelectedContainer.length > 0) {
            this.showContainerDIV = true;
        }

        console.log('LwcQlyPrestations.connectedCallback.this.wasteData ', JSON.stringify(this.wasteData));
        console.log('LwcQlyPrestations.connectedCallback.this.userSelectedServices ', JSON.stringify(this.userSelectedServices));
        console.log('LwcQlyPrestations.connectedCallback.this.listProducts ', JSON.stringify(this.listProducts.serviceProducts));
        console.log('LwcQlyPrestations.connectedCallback.this.containerPackaging ', JSON.stringify(this.containerPackaging));

        piklistValues({objApiName: 'QLY_Service__c', fieldName: 'Packaging__c' })
            .then(data => {
                console.log('LwcQlyPrestations.connectedCallback.this.options = data ', data);
                this.options = data;
            })

        if(this.formId)
        {
            getContentVersionFromForm({formID: String(this.formId), isSummary: false})
            .then((result) => {
                let pendingFilesMap = [];
                console.log('LwcQlyPrestations.getContentDocumentFromForm result: '+JSON.stringify(result));
                result.forEach(doc => {
                    console.log('LwcQlyPrestations.getContentDocumentFromForm doc: '+JSON.stringify(doc));
                    let filename = doc.Title;
                    console.log('LwcQlyPrestations.getContentDocumentFromForm filename: '+filename);
                    let prodId = doc.QLY_Product2__c;
                    console.log('prodId: '+prodId);
                    let prod = this.wasteData.find(waste => waste.id === prodId).name;
                    console.log('prod: '+prod);
                    this.existingFiles.push({prod: prod, name: filename, documentId: doc.ContentDocumentId, prodId: prodId});
                    pendingFilesMap.push({prod: prod, Id: doc.ContentDocumentId, prodId: prodId, name: filename});
                });

                let processAction;
                if(pendingFilesMap.length == 1){
                    processAction = { pendingFilesIds : pendingFilesMap, isNotGlobal: true, multipleFiles: false };
                } else {
                    processAction = { pendingFilesIds : pendingFilesMap, isNotGlobal: true, multipleFiles: true };
                }
                
                const uploadfiles = new CustomEvent(
                    "uploadfiles", { detail: processAction }
                );
                this.dispatchEvent(uploadfiles);

                this.productArray = [];
                this.regroupByProd(this.existingFiles);

            })
        } else {
            if(this.globalWasteFiles.length > 0) {
                let pendingFilesMap = [];
                console.log('LwcQlyPrestations.globalWasteFiles: '+JSON.stringify(this.globalWasteFiles));
                this.globalWasteFiles.forEach(file => {
                    this.existingFiles.push({prod: file.prod, name: file.name, documentId: file.documentId, prodId: file.prodId});
                    pendingFilesMap.push({prod: file.prod, Id: file.documentId, prodId: file.prodId, name: file.name});
                });

                let processAction = { pendingFilesIds : pendingFilesMap, isNotGlobal: true };
                const uploadfiles = new CustomEvent(
                    "uploadfiles", { detail: processAction }
                );
                this.dispatchEvent(uploadfiles);

                this.regroupByProd(this.globalWasteFiles);
            }
        }

        this.start = true;

    }

    /*file processing for template*/
    regroupByProd(fileList){
        
        let groupedFileMap = new Map();
        fileList.forEach(file => {
            console.log('LwcQlyPrestations.regroupByProd file: '+JSON.stringify(file));
            if (groupedFileMap.has(file.prod)) {
                groupedFileMap.get(file.prod).files.push(file);
                console.log('LwcQlyPrestations.regroupByProd groupedFileMap: '+JSON.stringify(groupedFileMap));
            } else {
                let newProduct = {};
                newProduct.prod = file.prod;
                newProduct.files = [file];
                console.log('LwcQlyPrestations.regroupByProd newProduct: '+JSON.stringify(newProduct));
                groupedFileMap.set(file.prod, newProduct);
                console.log('LwcQlyPrestations.regroupByProd groupedFileMap: '+JSON.stringify(groupedFileMap));
            }
        });

        let itr = groupedFileMap.values();
        console.log('LwcQlyPrestations.regroupByProd itr: '+JSON.stringify(itr));
        let result = itr.next();
        console.log('LwcQlyPrestations.regroupByProd result: '+JSON.stringify(result));
        while (!result.done) {
            console.log('LwcQlyPrestations.regroupByProd result.value: '+JSON.stringify(result.value));
            this.productArray.forEach(el => {
                console.log('LwcQlyPrestations.regroupByProd testing: '+el.files.some(i => i.documentId === result.value.files.documentId));
            })
            result.value.rowspan = result.value.files.length + 1;
            this.productArray.push(result.value);
            result = itr.next();
        }

        if(this.productArray.length>0){
            this.isFilesUploaded = true
        }

        console.log('LwcQlyPrestations.regroupByProd productArray: '+JSON.stringify(this.productArray));

    }

    renderedCallback() {
        console.log('prestation renderedcallback');
        if(this.start) {
            this.start = false;
            this.topElement.scrollIntoView({
                behavior: 'smooth',
                block: 'start'
            });
        }
    }

    get addContainer(){
       return (this.showAddContainer || this.userSelectedServices.listSelectedContainer.length > 0);
    }
    
    get haveSelectedContainer (){
        return (this.userSelectedServices.listSelectedContainer.length > 0);
    }
    /*
    @wire(getObjectInfo, { objectApiName: QLY_SERVICE_OBJECT })
    objectMetadata;  
    @wire(getPicklistValues, { recordTypeId: '$objectMetadata.data.defaultRecordTypeId', fieldApiName: PACKAGING_FIELD })
    getPicklistValues({ error, data }) {
        console.log('LwcQlySelectedServicesItemRowItem.this.data ', JSON.stringify(data ));
        if (data) {
            // Map picklist values
            this.options = data.values.map(plValue => {
                return {
                    label: plValue.label,
                    value: plValue.value
                };
            });

            //this.DEFAULT_PACKAGING = this.options[0].value;
            console.log('LwcQlySelectedServicesItemRowItem.this.options ', JSON.stringify(this.options));
        } else if (error) {
            // Handle error
            throw new Error(error);
            
        }
    } */

    handleClose(){
        this.openModal = false;
        console.log(this.waitingApproval.productId);
        this.template.querySelector('c-lwc-visual-picker').uncheck(this.waitingApproval.productId);
    }

    handleOK(){

        this.openModal=false;
        try {
            let selectedService = { what: this.SERVICE_TYPE, selectedService: this.waitingApproval };
            const userInputEvent = new CustomEvent(
                "selectedservice", { detail: selectedService }
            );
            this.dispatchEvent(userInputEvent);

            delete this.waitingApproval;
        } catch(e) {
            console.log(e);
        }

    }

    imageExists(image_url){

        var http = new XMLHttpRequest();
    
        http.open('HEAD', image_url, false);
        http.send();
    
        return http.status == 404;
    
    }
       
    //method to handle service(wastes, conntainers, prestations) by the user to create the first selected product line in service componant :
    // in this method we create the first selected product row
    handleSelectedServices(event) {
        console.log('LwcQlyPrestations.handleSelectedServices.id start ---', event.detail.id);
        console.log('LwcQlyPrestations.handleSelectedServices.productId start ---', event.detail.productId);
        console.log('LwcQlyPrestations.handleSelectedServices.name start ---', event.detail.name);
        console.log('LwcQlyPrestations.handleSelectedServices.description start ---', event.detail.description);
        console.log('LwcQlyPrestations.handleSelectedServices.unitPrice start ---', event.detail.unitPrice);
        console.log('LwcQlyPrestations.handleSelectedServices.inputValue start ---', event.detail.inputValue);
        console.log('LwcQlyPrestations.handleSelectedServices.request start ---', event.detail.request);
        console.log('LwcQlyPrestations.handleSelectedServices.displayStyle start ---', event.detail.displayStyle);
        console.log('LwcQlyPrestations.handleSelectedServices.serviceIcon start ---', event.detail.serviceIcon);
        console.log('LwcQlyPrestations.handleSelectedServices.features start ---', event.detail.features);
        console.log('LwcQlyPrestations.handleSelectedServices.adrIcon start ---', event.detail.adrIcon);
        console.log('LwcQlyPrestations.handleSelectedServices.clpIcon start ---', event.detail.clpIcon);

        const id = event.detail.id;
        const productId = event.detail.productId;
        const name = event.detail.name; // fieldValue = productName
        const description = event.detail.description;
        const unitPrice = event.detail.unitPrice;
        const value = event.detail.inputValue;
        const request = event.detail.request;
        const displayStyle = event.detail.displayStyle;
        const serviceIcon = event.detail.serviceIcon;
        const features = event.detail.features;
        const adrIcon = event.detail.adrIcon;
        const clpIcon = event.detail.clpIcon;
        const icon = event.detail.icon;

        // create the selected collect request
        if (request === this.SERVICE_TYPE) {
            let selectedPresta = {}
            if (value) {

                if(name.includes('vrac')) {
                    this.openModal = true;
                    this.waitingApproval = {
                        tempId: id,
                        id: '',
                        productId : productId,
                        name: name,
                        unitPrice : unitPrice,
                        serviceIcon: serviceIcon,
                        isSelected: value
                    };
                    return;
                }

                selectedPresta = {
                    id: '',
                    productId : productId,
                    name: name,
                    unitPrice : unitPrice,
                    serviceIcon: serviceIcon,
                    isSelected: value

                }
            } else {// this object will be used to remove it from list : here  isSelected = false
                selectedPresta = {
                    id: id,
                    productId : productId,
                    name: name,
                    unitPrice : unitPrice,
                    isSelected: value

                }
            }
            //send Event to main
            let selectedService = { what: this.SERVICE_TYPE, selectedService: selectedPresta };
            const userInputEvent = new CustomEvent(
                "selectedservice", { detail: selectedService }
            );
            this.dispatchEvent(userInputEvent);
            //sendDataToParent();          

            console.log('LwcQlyPrestations.handleSelectedServices.selectedPresta ---', selectedPresta);

            // create the first waste service request
        } else if (request === this.WASTE_TYPE) {
           
            let containers = (this.containerPackaging.findIndex(cp => cp.Id == productId) != -1) ? this.containerPackaging[this.containerPackaging.findIndex(cp => cp.Id == productId)].QLY_AvailablePackaging__c : '';
            let containerOpt = [];
            if (containers && containers.includes(';')) {
                let lst = containers.split(';');
                lst.forEach(cnt => {
                    containerOpt.push({label: cnt, value: cnt});
                });
            } else {
                containerOpt = [{label: containers, value: containers}];
            }

            let selectedWaste = {};

            if (value) {
                //the object that containe a selectd waste data
                selectedWaste = {
                    id: id,                   
                    productId : productId,
                    name: name,                  
                    rows: [
                        
                        {
                            wasteId: '',
                            index: 0,
                            description : description,
                            packaging: containerOpt[0].value,
                            containerPkg: containerOpt,
                            firstCnt: containerOpt[0].value,
                            nbrPackaging: '',
                            quantity:'' ,
                            unitPrice : unitPrice,
                            containerId : '',
                            originTreatmentService :'',
                            peroxyName  : '',
                            classification : '',
                            peroxydeQuantity : '',
                            photoQuantity :'' ,
                            photoListQuantity : '',
                            features : features,
                            adrIcon : adrIcon,
                            clpIcon : clpIcon,
                            files : [], 
                            isSelected: value,
                            displayStyle: displayStyle
                        }
                    ]

                   // isSelected: value,
                   // displayStyle: displayStyle
                }
                console.log('LwcQlyPrestations.handleSelectedServices.selectedWaste ---1', selectedWaste);
                this.listSelectedWaste.push(selectedWaste);
            } else {//we use the name/id to remove record from list 
                selectedWaste = {
                    id: id,
                    wasteId : '',
                    productId : productId,
                    name: name,
                    unitPrice : unitPrice,
                    isSelected: value,
                }
                this.listSelectedWaste.push(selectedWaste);             

            } 
            //send Event to main
            let selectedService = { what: this.WASTE_TYPE, selectedService: selectedWaste, displayStyle: displayStyle };
            const userInputEvent = new CustomEvent(
                "selectedservice", { detail: selectedService }
            );
            this.dispatchEvent(userInputEvent);
            //sendDataToParent();

            console.log('LwcQlyPrestations.handleSelectedServices.selectedWaste ---2', selectedWaste);
            //when user request to get container
        } else if (request === this.CONTAINER_TYPE) {

            let listSelectedContainer = [];
            let selectedContainer = {}

            //if product is bouchon/Bidon we add plug           
            let havePlug = (name.includes('Bonbonne'));

            if (value) {
                selectedContainer = {
                    id: '',
                    productId : productId,
                    href: '#'+productId,
                    name: name,
                    unitPrice : unitPrice,
                    numberContainer: '',
                    havePlug : havePlug,
                    isSelected: value,
                    containerId : '',
                    addContainer: havePlug,
                    numberPlug : '',
                    // addHelpText - AID 09/01/21
                    features: features,
                    icon: icon,
                    hide: this.imageExists(icon)

                }
                listSelectedContainer.push(selectedContainer);
            } else {//this object is used to remove from list
                selectedContainer = {
                    id: id,
                    productId : productId,
                    name: name,
                    unitPrice : unitPrice,
                    quantity: '',
                    isSelected: value

                }
            }

            //send Event to main
            let selectedService = { what: this.CONTAINER_TYPE, selectedService: selectedContainer };
            const userInputEvent = new CustomEvent(
                "selectedservice", { detail: selectedService }
            );
            this.dispatchEvent(userInputEvent);
            //sendDataToParent();          

            console.log('LwcQlyPrestations.handleSelectedServices.selectedContainer ---', selectedContainer);
        }

    }

    @api handleUIChange(arr){
        console.log('@api handleUIChange '+JSON.stringify(arr));
        console.log('LwcQlyPrestations existingFIles-start: '+JSON.stringify(this.existingFiles));

        if(arr) {

            let Ids = [];
            arr.forEach(el => {
                el.filesId.forEach(e=> {
                    Ids.push(e);
                })
            })

            console.log('LwcQlyPrestations Ids: '+JSON.stringify(Ids));

            let indexesToDelete = [];

            this.existingFiles.forEach(el => {
                let index = this.existingFiles.indexOf(el);
                console.log(index);
                console.log(el.documentId)
                console.log(Ids.includes(el.documentId));
                if(!Ids.includes(el.documentId)) {
                    indexesToDelete.push(el);
                }
            })

            console.log('LwcQlyPrestations indexesToDelete: '+JSON.stringify(Ids));

            indexesToDelete.forEach(i => {
                this.existingFiles.splice(this.existingFiles.indexOf(i), 1);
            })

            console.log('LwcQlyPrestations existingFIles-finish: '+JSON.stringify(this.existingFiles));

            this.productArray = [];
            this.regroupByProd(this.existingFiles);

        } else {
            this.productArray = [];
            this.existingFiles = [];
        }

    }

    handleSave(event){ 
        this.isDisabled = true;
        console.log('LwcQlyPrestations.handleSave --- start' );       
        //send to main
        const saveEvent = new CustomEvent(
            "save" );  
            console.log('lucas save event : ' + saveEvent);
            this.dispatchEvent(saveEvent);      
     }

    //method to handle adding/romving rows from existing row by listing childs event
    handleRowAction(event) {
        console.log('LwcQlyPrestations.handleRowAction.product start ---', event.detail.productName);
        console.log('LwcQlyPrestations.handleRowAction.name start ---', event.detail.name);
        console.log('LwcQlyPrestations.handleRowAction.action start ---', event.detail.action);
        console.log('LwcQlyPrestations.handleRowAction.index start ---', event.detail.currentIndex);

        const productName = event.detail.productName;
        const action = event.detail.action;
        const field = event.detail.name;
        const currentIndex = event.detail.currentIndex;
        const unitPrice = event.detail.unitPrice;

        let processAction = { productName: productName, name: field, action: action, currentIndex: currentIndex, unitPrice:unitPrice,packaging: event.detail.packaging, options: event.detail.options };
        const rowaction = new CustomEvent(
            "rowaction", { detail: processAction }
        );
        this.dispatchEvent(rowaction);

    }

    handleUserInputChange(event) {
        const field = event.target.name;
        const inputValue = event.target.value;

        if (field === 'addContainerCheckBox') {
            // this.showRequestInput = event.target.checked;
            // const inputValue = event.target.checked;
            // let userInputValue = { field: field, inputValue: inputValue };
            // const userInputEvent = new CustomEvent(
            //     "userinputvalue", { detail: userInputValue }
            // );
            // this.dispatchEvent(userInputEvent);
            // //sendDataToParent();
            this.showContainerDIV = event.target.checked;
        }

        else if (field === 'productCheckBox') {
            const inputValue = event.target.checked;
            let userInputValue = { field: field, inputValue: inputValue };
            const userInputEvent = new CustomEvent(
                "userinputvalue", { detail: userInputValue }
            );
            this.dispatchEvent(userInputEvent);
            //sendDataToParent();

        } else {
            let userInputValue = { field: field, inputValue: inputValue };
            const userInputEvent = new CustomEvent(
                "userinputvalue", { detail: userInputValue }
            );
            this.dispatchEvent(userInputEvent);
            //sendDataToParent();
        }
        this.isDisabled = false;

    }

    //method to handle user input form service resquest form to update slected rows values
    handleUserInputServiceChange(event) {
        console.log('LwcQlyPrestations.handleUserInputServiceChange start ---', event.detail.productName);
        console.log('LwcQlyPrestations.handleUserInputServiceChange. event.target.name; start ---', event.detail.field);
        console.log('LwcQlyPrestations.handleUserInputServiceChange. event.target.value start ---', event.detail.inputValue);
        console.log('LwcQlyPrestations.handleUserInputServiceChange.displayStyle start ---', event.detail.displayStyle);
        console.log('LwcQlyPrestations.handleUserInputServiceChange.currentIndex start ---', event.detail.currentIndex);
        console.log('LwcQlyPrestations.handleUserInputServiceChange.eventOrigin start ---', event.detail.eventOrigin);

        const productName =  event.detail.productName;
        const field = event.detail.field;
        const displayStyle = event.detail.displayStyle;
        const currentIndex = event.detail.currentIndex;
        const inputValue = event.detail.inputValue;
        const eventOrigin = event.detail.eventOrigin;

        let userInputValue = { field: field, inputValue: inputValue, displayStyle: displayStyle, currentIndex: currentIndex, productName : productName, eventOrigin : eventOrigin};
        const userInputEvent = new CustomEvent(
            "userserviceinputvalue", { detail: userInputValue }
        );
        this.dispatchEvent(userInputEvent);
        this.isDisabled = false;
    }

    handleUpload(event){

        console.log('LwcQlyPrestation.handleUpload event.detail: '+JSON.stringify(event.detail.pendingFilesIds));
        console.log('LwcQlyPrestations existingFIles: '+JSON.stringify(this.existingFiles));

        const pendingFilesIds =  event.detail.pendingFilesIds;
        let pendingFilesMap = [];

        pendingFilesIds.forEach(file => {
            console.log('LwcQlyPrestations.getContentDocumentFromForm file: '+JSON.stringify(file));
            let filename = file.name;
            console.log('LwcQlyPrestations.getContentDocumentFromForm filename: '+filename);
            let prodId = file.productId;
            console.log('prodId: '+prodId);
            let prod = this.wasteData.find(waste => waste.id === prodId).name;
            console.log('prod: '+prod);
            console.log('fileName: '+filename);
            this.existingFiles.push({prod: prod, name: filename, documentId: file.Id, prodId: prodId});
            pendingFilesMap.push({prod: prod, Id: file.Id, prodId: prodId, name: filename});
        });
        
        this.productArray = [];
        this.regroupByProd(this.existingFiles);

        let processAction = { pendingFilesIds : pendingFilesMap };
        const uploadfiles = new CustomEvent(
            "uploadfiles", { detail: processAction }
        );
        this.dispatchEvent(uploadfiles);

        /*let processActionWaste = { wasteData : pendingFilesMap };
        const waste = new CustomEvent(
            "waste", { detail: processActionWaste }
        );
        this.dispatchEvent(waste);*/

    }

    deleteHandler(event){
        console.log('LwcQlyPrestations existingFIles: '+JSON.stringify(this.existingFiles));
        console.log(event.target.dataset.id);
        console.log(event.target.dataset.name);
        console.log(event.target.dataset.product);

        let prodId = event.target.dataset.product;
        let fileId = event.target.dataset.id;

        let fileName = event.target.dataset.name;
        console.log('LwcQlyPrestation.deleteContentDocument fileName: '+fileName);

        let index;
        let i=0;
        while(!index && i<this.productArray.length)
        {
            let e = this.productArray[i];
            console.log('LwcQlyPrestation.deleteContentDocument e: '+JSON.stringify(e));
            e.files.forEach(el => {
                console.log('LwcQlyPrestation.deleteContentDocument el: '+el);
                if(el.documentId === event.target.dataset.id){
                    index = i;
                }
            });

            i++;
        }

        console.log('LwcQlyPrestation.deleteContentDocument index: '+index);
        let filesArr = this.productArray[index].files;
        console.log('LwcQlyPrestation.deleteContentDocument filesArr: '+JSON.stringify(filesArr));
        let indexOfDoc = filesArr.findIndex(doc => doc.documentId == event.target.dataset.id);
        console.log('LwcQlyPrestation.deleteContentDocument indexOfDoc: '+indexOfDoc);
        let indexInExisting = this.existingFiles.findIndex(doc => doc.documentId == event.target.dataset.id);
        console.log('LwcQlyPrestation.deleteContentDocument indexInExisting: '+indexInExisting);

        deleteContentDocument({docId: event.target.dataset.id})
            .then((result) => {
                console.log('LwcQlyPrestation.deleteContentDocument result: '+JSON.stringify(result));
                filesArr.splice(indexOfDoc, 1);
                console.log('LwcQlyPrestation.deleteContentDocument filesArr: '+JSON.stringify(filesArr));
                this.productArray[index].files = filesArr;
                this.existingFiles.splice(indexInExisting, 1);
                /*if(filesArr.length === 0)
                {
                    this.productArray.splice(index, 1);
                }*/
                if(this.existingFiles.length === 0)
                {
                    this.isFilesUploaded = false;
                }

                let processAction = { deleteFile : {prodId: prodId, Id: fileId} };
                const deletefile = new CustomEvent(
                    "deletefile", { detail: processAction }
                );
                this.dispatchEvent(deletefile);

                this.productArray = [];
                this.regroupByProd(this.existingFiles);

            })
            .catch((error) => {
                console.log('error: '+JSON.stringify(error));
            });

    }

}