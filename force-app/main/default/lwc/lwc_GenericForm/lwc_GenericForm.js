import { api, track, LightningElement } from 'lwc';
import customInsert from '@salesforce/apex/LWC_PolymorphicProcedures.customInsert';
import retrieveFieldsFromLayout from '@salesforce/apex/CAP_Global_SF_SysTools.retrieveFieldsFromLayout';

import {
    FlowNavigationNextEvent
} from 'lightning/flowSupport';

export default class Lwc_GenericForm extends LightningElement {
    
    @api objectApiName;
    @api recordId; //for edit
    @api useLayoutsFields; //specify if we get fields from layout
    @api layoutName;
    @api displayAsOneColumn;
    @api useDefaultAction;
    @api overrideSubmit; // uses polymerfic architecture to save record
    @api prefillUponCreationJSON;
    @api createRecord;
    @api previewData;
    @api fields;
    @api flowMode
    @api createdRecordId
    
    prefillUponCreation;
    @track formField
    @track wait = true

    get columnStyle() {
        return (this.displayAsOneColumn) ? "min-w-100" : "min-w-50";
    }

    connectedCallback() {
        try {
            if(this.useLayoutsFields && this.layoutName) {
                retrieveFieldsFromLayout({layoutName: this.layoutName, hasSections: false})
                .then(data => {
                    if(data) {
                        this.formField = JSON.parse(data);
                        this.wait = false
                    }
                })
            } else if(this.fields) {
                this.formField = this.fields
                this.wait = false
            } else {
                this.wait = false
            }
            this.prefillUponCreation = (this.prefillUponCreationJSON) ? JSON.parse(this.prefillUponCreationJSON) : null
            
            this.dispatchEvent(new CustomEvent('startloading'))
        } catch(e) {
            console.log(e)
        }
    }

    async handleSubmitCreateEvent(event) {
        var record = event.detail.fields;

        if(this.prefillUponCreation) {
            this.prefillUponCreation.forEach(field => {
                record[field.name] = field.value;
            });
        }

        if(this.overrideSubmit) {
            event.preventDefault();
            const recordCreated = await customInsert({o: this.objectApiName, data: JSON.stringify(record), apiName: this.objectApiName})
            console.log(recordCreated)
            this.createdRecordId = recordCreated
        } else {
            this.template.queySelector('lightning-record-edit-form[name="createRecord"]').submit();
        }

        if(this.flowMode) {
            const navigateEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateEvent);
        }
    }

    handleSubmitEditEvent(event) {
        var record = event.detail.fields;
        if(this.prefillUponCreation) {
            this.prefillUponCreation.forEach(field => {
                record[field.name] = field.value;
            });
        }
        if(this.overrideSubmit) {
            event.preventDefault();
            customInsert({o: this.recordId, data: JSON.stringify(record), apiName: this.objectApiName})
        } else {
            this.template.queySelector('lightning-record-edit-form[name="editRecord"]').submit();
        }

        if(this.flowMode) {
            const navigateEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateEvent);
        }
    }

    handleLoad() {
        window.console.time("LDS call");
        //details coming on the load of form
        // The LDS will take a few seconds to load the component.
        window.console.timeEnd("LDS call");
        this.dispatchEvent(new CustomEvent('formloaded'))
    }

}