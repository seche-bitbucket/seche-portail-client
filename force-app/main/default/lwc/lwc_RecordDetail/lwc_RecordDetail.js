import { LightningElement, api, track, wire } from 'lwc';
import getFieldsLabelAndValue from '@salesforce/apex/LWC_CustomBannerCtrl.getFieldsLabelAndValue';
import getRecord from '@salesforce/apex/LWC_CustomBannerCtrl.getRecord';
import save from '@salesforce/apex/LWC_CustomBannerCtrl.save';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import jQuery from '@salesforce/resourceUrl/jQuery';
import { loadScript } from 'lightning/platformResourceLoader';
import KeySize from '@salesforce/schema/EmailDomainKey.KeySize';

export default class Lwc_RecordDetail extends LightningElement {
    
    @api sectionTitles;
    @api fieldsBySection;
    @api requiredFields;
    @api recordId;

    i = 0;

    record = {};

    @track sections = [];
    edit = false;

    lookupRc;
    lookupRcId;
    inSection;
    nameLookUp;

    triggerEdit(event) {
        this.edit = true;
    }

    firstRun = true;
    @track noAnime = '';

    handleChange(event) {
        try{
            this.record[event.target.name] = event.target.value;
            this.sections.find(s => s.title == event.target.dataset.section).fields.find(f => f.name == event.target.name).value = event.target.value;
            if(event.target.dataset.lookup) this.lookupRcId = event.target.value;
        } catch(e) {
            console.log(e);
        }
    }

    connectedCallback() {

        try{

            Promise.all([loadScript(this, jQuery)])
            .then(() => {

                if(this.requiredFields){
                    this.requiredFields = this.requiredFields.split(',');
                }

                if(this.sectionTitles && this.fieldsBySection) {
                    if(this.sectionTitles.split(',').length == this.fieldsBySection.split(',').length) {

                    var fs = [];
                    //list to array
                    this.fieldsBySection.split(',').forEach(f => {
                        if(f.includes(';')) {
                            fs.push(f.split(';').join(','));
                        } else {
                            fs.push(f);
                        }
                    })

                    getFieldsLabelAndValue({o: this.recordId, recordId: this.recordId, fields: fs.join(',')})
                    .then(data => {
                        if(data) {

                            var lookUpId = [];

                            for(var i=0; i < this.sectionTitles.split(',').length; i++) {

                                var fields = []
                                this.fieldsBySection.split(',')[i].split(';').forEach(f => {

                                    let el = data.find(d => d.name == f);

                                    let type;
                                    let standard = true;
                                    let options;
                                    let required = (this.requiredFields) ? this.requiredFields.includes(f) : false;
                                    
                                    switch (el.type) {

                                        case 'TEXTAREA':
                                            type = 'textarea';
                                            standard = false;
                                            break;

                                        case 'STRING', 'ID':
                                            type = 'text';
                                            break;

                                        case 'PICKLIST':
                                            type = 'picklist';
                                            standard = false;
                                            options = el.options;
                                            break;

                                        case 'REFERENCE':
                                            type = 'lookup';
                                            standard = false;
                                            lookUpId.push(el.value);
                                            break;

                                        case 'DATE':
                                            type = 'date';
                                            break;

                                        case 'DATETIME':
                                            type = 'datetime';
                                            break;

                                    }

                                    fields.push({name: f, label: el.label, value: el.value, standard: standard, 
                                        type: type, options: options, textarea: (type == 'textarea' && !standard), 
                                        picklist: (type == 'picklist' && !standard), lookup: (type == 'lookup' && !standard),
                                        nameLookUp: '', required: required});
                                        
                                    this.record[f] = el.value;

                                })
                                this.sections.push({title: this.sectionTitles.split(',')[i], fields: fields});

                            }

                            // get name for lookupid
                            lookUpId.forEach(id => {
                                getRecord({o: this.recordId, recordId: id})
                                .then(data => {

                                    if(data) {
                                        var lookupRc = data;
                                        var ks = Object.keys(lookupRc);
                                        console.log(ks);
                                        ks.forEach(k => {
                                            if(k.toLowerCase() == 'name' || k.toLowerCase().includes('name')) {
                                                this.sections.forEach(s => {
                                                    if(s.fields.find(f => f.value == id) != undefined)
                                                        s.fields.find(f => f.value == id).nameLookUp = lookupRc[k];
                                                });
                                            }
                                        })
                                    }

                                });
                            })

                        }
                    })
                
                    } else {
                        const evt = new ShowToastEvent({
                            title: 'Sections are not properly setup',
                            message: 'Please verify sectionTitles('+this.sectionTitles.split(',').length+') and fieldsBySection('+this.fieldsBySection.split(',').length+'), they must be equal',
                            variant: 'error',
                            mode: 'sticky'
                        });
                        this.dispatchEvent(evt);
                    }
                } else {
                    const evt = new ShowToastEvent({
                        title: 'Component not setup',
                        message: 'We did not find any configuration, componenent display is disabled',
                        variant: 'warning',
                        mode: 'sticky'
                    });
                    this.dispatchEvent(evt);
                }
            })

        } catch(e) {
            console.log(e);
        }

    }

    handleSave(event) {
        var noEmpty = true;

        try {
            for (const [key, value] of Object.entries(this.record)) {
                console.log(`${key}: ${value}`);
                if(this.requiredFields != undefined && this.requiredFields.includes(key) && ( value == null || value === "" )){
                    noEmpty = false;
                }
            }
            
            if(noEmpty){
                console.log('saving...')
                console.log(this.record)
                this.record.Id = this.recordId;
                save({o: this.recordId, metadata: JSON.stringify([this.record]), dataType: 'Case'})
                .then(() => {
                    this.edit = false;
                })
                return;
            }
            
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Erreur',
                    message:  'Vérifiez que tous les champs requis sont remplis avant d\'enregistrer!!',
                    variant: 'error',
                }),
            );
        } catch(e) {
            console.log(e)
        }
        
    }

    handleCancel(event) {
        this.edit = false;
    }

    get lineClass() {
        if(this.edit && this.sections.length > 1) {
            return 'input no-border-bottom';
        } else if (this.sections.length == 1 && this.edit) {
            return 'input no-border-top no-border-bottom';
        } else if(this.sections.length == 1 && !this.edit) {
            return 'input no-border-top';
        } else if(this.sections.length > 1 && !this.edit) {
            return 'input';
        }
    }
}