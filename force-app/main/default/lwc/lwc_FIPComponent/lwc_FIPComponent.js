import { LightningElement, api } from 'lwc';
import sendData from '@salesforce/apex/PFAS_Controller.sendData';

export default class Lwc_FIPComponent extends LightningElement {

    @api fip

    components = []
    fipData

    handleEventData(event) {
        this.components = JSON.parse(JSON.stringify(event.detail.Components))
        this.fipData = JSON.parse(JSON.stringify(event.detail.FIP_FIP__c))
    }

    @api
    async updateComponents(fipId) {
        for(let i = 0; i < this.components.length; i++) {
            delete this.components[i].localVars
            this.components[i].FIP__c = fipId
        }
        await sendData({processName: 'updateComponents', jsonData: JSON.stringify(this.components)})
        await sendData({processName: 'updateFIP', jsonData: JSON.stringify({Id: fipId, HasPfas__c: this.fipData.HasPfas__c})})
    }

}