import { LightningElement, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { loadScript } from "lightning/platformResourceLoader";

import jsdict from '@salesforce/resourceUrl/PFAS_Translation'
import imgs from '@salesforce/resourceUrl/Pollutec'

import getRecordByGUID from '@salesforce/apex/SystemUtils.getRecordByGUID';

export default class Lwc_PFASMain extends LightningElement {

    // Contextual Vars
    guid
    waiting = true
    displayModal = false
    loader = true
    notFound
    page_not_found
    progressBarState = '--i: 0'
    progressMessage
    overrideStep = 0

    // IMG Vars
    main_logo_img
    warning_img
    error_img
    

    @wire(CurrentPageReference)
    getStateParameters(currentPageReference) {
       if (currentPageReference) {
          this.urlStateParameters = currentPageReference.state
          this.setParametersBasedOnUrl()
       }
    }

    setParametersBasedOnUrl() {
        this.language = this.urlStateParameters.language
        this.guid = this.urlStateParameters.recordId || null
        this.overrideStep = this.urlStateParameters.step || 0
    }

    async connectedCallback() {
        // Setting Translation
        //var PFAS_Dict
        await loadScript(this, jsdict+"/PFAS_TranslationDict.js")
        //await loadScript(PFAS_Dict, jsdict+"/PFAS_TranslationDict.js")
        this.DICT = PFAS_Dict.getDict()
        this.page_not_found = this.DICT[this.language]?.not_found
        this.progressMessage = this.DICT[this.language]?.progressMessage

        this.main_logo_img = imgs + '/Brand.png'
        this.warning_img = imgs + '/Warning.png'
        this.error_img = imgs + '/Error.png'

        // fetch guid from portalVerification
        this.recordInfo = await getRecordByGUID({GUID: this.guid, objectApiName: 'Contact', GUID_FIELD: 'GUID__c'})

        if(!this.recordInfo) {
            this.notFound = true
        }
    
        this.waiting = false
    }

    handleFormLoaded() {
        this.waiting = false
    }

    handleFormStartLoading() {
        this.waiting = true
    }

    handleProgressBarRefresh(event) {
        this.progressBarState = '--i: '+event.detail.state
    }

    handleModalDisplay(event) {
        if(event.detail.loader === false) {
            this.loader = false
        }
        if(event.detail.display === false) {
            this.loader = true
            this.progressMessage = this.DICT[this.language]?.progressMessage
        }
        this.displayModal = event.detail.display
    }

    handleModalMessage(event) {
        this.progressMessage = event.detail.message
    }
}