window.PFAS_Dict = (function() {
    return {
        getDict: function() {
            return {
                'fr' : {
                    'not_found' : 'Cette page n\'existe pas',
                    'progress_message' : 'Enregistrement des compléments de FIPs et Substances...',
                    'step_1_name': 'Étape 1',
                    'step_1_title': 'Votre Identité',
                    'step_2_name': 'Étape 2',
                    'step_2_title': 'Données CAP',
                    'step_3_name': 'Étape 3',
                    'step_3_title': 'Validation',
                    'error_max_min': 'La valeur du %Minimum ne doit pas être supérieur au %Maximum.',
                    'error_unfinished': 'Veuillez remplir tous les champs de vos compléments de FIPs.',
                    'error_saving': 'Erreur lors de l\'enregistrement des compléments de FIPs!',
                    'saving_message': 'Envoi de vos documents pour signature...',
                    'saved_message': 'Vos données ont bien été sauvegardées !'
                },
                'en_US' : {
                    'not_found' : 'Page not found',
                    'progress_message' : 'Saving FIPs Complements and Substances...',
                    'step_1_name': 'Step 1',
                    'step_1_title': 'Personal Information',
                    'step_2_name': 'Étape 2',
                    'step_2_title': 'CAP Data',
                    'step_3_name': 'Étape 3',
                    'step_3_title': 'Summary & Validation',
                    'error_max_min': '%Minimum value cannot be higher than %Maximum.',
                    'error_unfinished': 'You must fill all your FIPs Complements Fields.',
                    'error_saving': 'Error while saving your data!',
                    'saving_message': 'Sending your documents for signature...',
                    'saved_message': 'All tour data have been saved successfully !'
                }
            }
        }
    };
}());