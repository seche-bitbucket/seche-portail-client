import Title from '@salesforce/schema/Contact.Title';
import { api,track, LightningElement } from 'lwc';

export default class LwcQlySelectProducts extends LightningElement {
    @api title;
    @api listProducts;
     //list to save all selected user services
     @api userSelectedServices;

    @track SERVICE_TYPE = 'Collecte et service';
    @track CONTAINER_TYPE = 'Contenant';
    @track WASTE_TYPE = 'Traitement déchets';
    @track openModal = false;
     renderedCallback() {
        //restore selected product
        this.setSelectedProduct(this.userSelectedServices);
     }

    //check if product is in selected service liste
    get isSelected(){
        return true;
    }

    handleSelectedServices(event) {
        console.log('LwcQlySelectProducts.handleSelectedServices.event.target.dataset.productId start ---', event.currentTarget.dataset.productId);
        console.log('LwcQlySelectProducts.handleSelectedServices.event.target.dataset.display-type start ---', event.currentTarget.dataset.displayType);
        console.log('LwcQlySelectProducts.handleSelectedServices.event.target.name start ---', event.target.name);
        console.log('LwcQlySelectProducts.handleSelectedServices.event.target.dataset.description start ---', event.currentTarget.dataset.description);
        console.log('LwcQlySelectProducts.handleSelectedServices.event.target.UnitPrice start ---', event.currentTarget.dataset.unitPrice);
        console.log('LwcQlySelectProducts.handleSelectedServices.event.target.checked start ---', event.target.checked);
        console.log('LwcQlySelectProducts.handleSelectedServices.event.currentTarget.dataset.features ---',  event.currentTarget.dataset.features);
        console.log('LwcQlySelectProducts.handleSelectedServices.event.currentTarget.dataset.adrIcon ---',  event.currentTarget.dataset.adrIcon);
        console.log('LwcQlySelectProducts.handleSelectedServices.event.currentTarget.dataset.clpIcon ---',  event.currentTarget.dataset.clpIcon);
        
        const id = event.currentTarget.dataset.id;
        const productId = event.currentTarget.dataset.productId;
        const field = event.target.name;
        const description = event.currentTarget.dataset.description;
        const unitPrice = event.currentTarget.dataset.unitPrice;
        const inputValue = event.target.checked;
        const requestType = event.currentTarget.dataset.request;
        const displayStyle = event.currentTarget.dataset.displayType;
        const features = event.currentTarget.dataset.features;
        const adrIcon = event.currentTarget.dataset.adrIcon;
        const clpIcon= event.currentTarget.dataset.clpIcon;
        const icon = event.currentTarget.dataset.icon;

        if(field.includes('Produits chimiques de laboratoire (160506*)') && event.target.checked) {
            this.openModal = true;
        }

        let   wasteInputValue = {icon: icon, productId: productId, id : id, name: field, description : description, inputValue: inputValue, request: requestType, displayStyle : displayStyle, unitPrice : unitPrice, features : features, adrIcon : adrIcon, clpIcon : clpIcon};

        const wasteInputEvent = new CustomEvent(
            "selectedwastevalue", { detail: wasteInputValue }
        );
        this.dispatchEvent(wasteInputEvent);
        //sendDataToParent();


    }


    handleOK(){

        this.openModal=false;
        try {
            let   wasteInputValue = {icon: icon, productId: productId, id : id, name: field, description : description, inputValue: inputValue, request: requestType, displayStyle : displayStyle, unitPrice : unitPrice, features : features, adrIcon : adrIcon, clpIcon : clpIcon};
             const wasteInputEvent = new CustomEvent(
            "selectedwastevalue", { detail: wasteInputValue }
            );
             this.dispatchEvent(wasteInputEvent);
         
        } catch(e) {
            console.log(e);
        }
    }

     // methode to restore selected product by user
     setSelectedProduct(listSelectedItems) {
        console.log('LwcQlySelectProducts.setSelectedProduct ---', JSON.stringify(listSelectedItems));
        let i;
        let checkboxes = this.template.querySelectorAll('[data-id="checkbox"]')
        for(i=0; i<checkboxes.length; i++) {
            console.log(' checkboxes[i] ---',  checkboxes[i].dataset.request);
            console.log(' listSelectedItems[i] ---',  JSON.stringify(listSelectedItems));
            //check if current prestation is already selected by user
            if( checkboxes[i].dataset.request === this.WASTE_TYPE && listSelectedItems.listSelectedWaste){
                console.log('this.userSelectedServices.listSelectedWaste.some ---', listSelectedItems.listSelectedWaste.some(waste => waste.id.includes(checkboxes[i].id)));
                checkboxes[i].checked = listSelectedItems.listSelectedWaste.some(waste =>  waste.name === checkboxes[i].name);

            }else if( checkboxes[i].dataset.request === this.CONTAINER_TYPE &&  listSelectedItems.listSelectedContainer){
                console.log('this.userSelectedServices.listSelectedContainer.some ---', listSelectedItems.listSelectedContainer.some(cont => cont.id.includes(checkboxes[i].id)));
                checkboxes[i].checked = listSelectedItems.listSelectedContainer.some(cont =>  cont.name === checkboxes[i].name);
            }
           
        }
    }
}