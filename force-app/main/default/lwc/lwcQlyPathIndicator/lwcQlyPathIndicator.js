import { LightningElement, track, api } from 'lwc';
export default class LwcQlyPathIndicator extends LightningElement {

    @api selectedStep;
    @track className1;
    @track className2;
    @track className3;

    @track stepTextP1;

    type = 'base';
    variant = 'base';
    hasError = false;

    renderedCallback() {
        console.log('LwcQlyPathIndicator selectedStep: '+this.selectedStep);
        if(this.selectedStep === 'Step1')
        {
            this.className1 = 'slds-theme--inverse step-container slds-align_absolute-center slds-col slds-size_1-of-3 slds-grid slds-wrap slds-box';
            this.stepTextP1 = 'slds-text-heading_medium step-text_p';

            this.className2 = 'slds-theme--default step-container slds-align_absolute-center slds-col slds-size_1-of-3 slds-grid slds-wrap slds-box slds-text-color_inverse-weak step-text';
            this.stepTextP2 = 'slds-text-heading_medium';
            this.className3 = this.className2;
            this.stepTextP3 = this.stepTextP2;

        } else if (this.selectedStep === 'Step2') {

            this.className2 = 'slds-theme--inverse step-container slds-align_absolute-center slds-col slds-size_1-of-3 slds-grid slds-wrap slds-box';
            this.className1 = 'slds-theme--default step-container slds-align_absolute-center slds-col slds-size_1-of-3 slds-grid slds-wrap slds-box slds-text-color_inverse-weak step-text';
            this.className3 = this.className1;

        } else if (this.selectedStep === 'Step3') {

            this.className3 = 'slds-theme--inverse step-container slds-align_absolute-center slds-col slds-size_1-of-3 slds-grid slds-wrap slds-box';
            this.className1 = 'slds-theme--default step-container slds-align_absolute-center slds-col slds-size_1-of-3 slds-grid slds-wrap slds-box slds-text-color_inverse-weak step-text';
            this.className2 = this.className1;
        }
    }

}