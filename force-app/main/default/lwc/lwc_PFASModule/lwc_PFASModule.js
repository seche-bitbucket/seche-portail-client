/* eslint-disable @lwc/lwc/no-async-operation */
import { LightningElement, api, track } from 'lwc';
import LightningAlert from 'lightning/alert';
import { loadScript } from "lightning/platformResourceLoader";

import jsdict from '@salesforce/resourceUrl/PFAS_Translation'

import sendData from '@salesforce/apex/PFAS_Controller.sendData';
import checkIfSentForSig from '@salesforce/apex/PFAS_Controller.checkIfSentForSig';
import getCapByContactId from '@salesforce/apex/PFAS_Controller.getCapByContactId';

import * as constants from './constants';

export default class Lwc_PFASModule extends LightningElement {

    // Event Data
    @api language
    @api recordData
    @api mainLogo
    @api warningImg
    @api overrideStep

    // Contextual Vars
    finalize = false
    mapCAPComponents = []
    caps
    isLoading = false
    noCAPs = false
    stepIndicator = 0
    formLoaded = 0
    CONSTANTS = constants
    @track STEPS = [{},{},{}]
    @track isFR
    @track isEN

    // Elements Vars
    progressBarContainer
    progressBar
    progressBarText
    progressBarEndState

    // Render Event
    renderedCallback() {
        this.progressBarContainerr = this.template.querySelector('.progress-bar__container')
        this.progressBarr = this.template.querySelector('.progress-bar')
        this.progressBarTextext = this.template.querySelector('.progress-bar__text')
    }

    get hasActions() {
        return this.stepIndicator < this.STEPS.length-1
    }

    get finalStep() {
        return this.stepIndicator === this.STEPS.length-2 // Final Step is muteable
    }

    get actionsStyle() {
        return (this.stepIndicator === 0) ? 'justify-content: flex-end !important;' : ''
    }

    get step1() {
        return this.stepIndicator === 0
    }

    get step2() {
        return this.stepIndicator === 1
    }

    get step3() {
        return this.stepIndicator === 2
    }

    async connectedCallback() {


        await loadScript(this, jsdict+"/PFAS_TranslationDict.js")
        this.DICT = PFAS_Dict.getDict() 
        this.step1Title = this.DICT[this.language].step_1_title
        this.nocap_message = this.DICT[this.language].nocap_message
        this.finished_message = this.DICT[this.language].finished_message
        this.info_message = this.DICT[this.language].info_message

        this.STEPS = [
            {
                id: 'step1',
                name: this.DICT[this.language].step_1_name,
                title: this.DICT[this.language].step_1_title,
                class: this.CONSTANTS.activeClass,
                nameClass: this.CONSTANTS.activeClassStep,
                titleClass: this.CONSTANTS.activeClassTitle,
                done: false
            },
            {
                id: 'step2',
                name: this.DICT[this.language].step_2_name,
                title: this.DICT[this.language].step_2_title,
                class: this.CONSTANTS.doneClass,
                nameClass: this.CONSTANTS.doneClassStep,
                titleClass: this.CONSTANTS.doneClassTitle,
                done: false
            },
            {
                id: 'step3',
                name: this.DICT[this.language].step_3_name,
                title: this.DICT[this.language].step_3_title,
                class: this.CONSTANTS.doneClass,
                nameClass: this.CONSTANTS.doneClassStep,
                titleClass: this.CONSTANTS.doneClassTitle,
                done: false
            }
        ]

        this.caps = await getCapByContactId({contactId: this.recordData.Id})

        if(this.caps.length === 0) {
            this.noCAPs = true
            this.dispatchEvent(new CustomEvent('formloaded'))
        }

        if(this.overrideStep === '1') {
            // removing step query
            window.history.replaceState({}, 'PFAS', window.location.href.split('?')[0] + '?' + window.location.href.split('?')[1].split('&')[0])
            this.changeStep(this.stepIndicator, 1)
            this.dispatchEvent(new CustomEvent('formloaded'))
        }

        if (this.language === 'fr') {
            this.isFR = true
        }
        if (this.language === 'en_US') {
            this.isEN = true
        }
    }  

    changeStep(stepIndex, incrementor) {
        //nextStep
        this.STEPS[stepIndex+incrementor].class = this.CONSTANTS.activeClass
        this.STEPS[stepIndex+incrementor].nameClass = this.CONSTANTS.activeClassStep
        this.STEPS[stepIndex+incrementor].titleClass = this.CONSTANTS.activeClassTitle
        this.STEPS[stepIndex+incrementor].done = false;

        //previous
        this.STEPS[stepIndex].class = this.CONSTANTS.doneClass
        this.STEPS[stepIndex].nameClass = this.CONSTANTS.doneClassStep
        this.STEPS[stepIndex].titleClass = this.CONSTANTS.doneClassTitle
        this.STEPS[stepIndex].done = (incrementor === 1) ? true : false;

        this.stepIndicator += incrementor
    }

    async handleNext() {
        if(this.stepIndicator === 1) {
            this.finalize = true
            await this.handleSave()
            return
        }

        this.changeStep(this.stepIndicator, 1)
    }

    handlePrevious() {
        if(this.stepIndicator === 1) {
            this.changeStep(this.stepIndicator, -1)
        }
    }

    async handleSave() {
        try {
            
            let pbState = 0
            // Display Modal on parent
            await this.dispatchEvent(new CustomEvent('modaldisplay', {detail: {display: true, loader: true}}))

            // Organizing CAPs and Components
            let capsArray = []
            let capsIdsArray = []
            let componentsArray = []
            for(let i = 0; i < this.mapCAPComponents.length; i++) {
                const item = this.mapCAPComponents[i]
                if('HasPfas__c' in item.CAP) {
                    for(let j = 0; j < item.Components.length; j++) {
                        let component = item.Components[j]
                        delete component.localVars
                        if((component.MinimumPercentage__c != null && component.MinimumPercentage__c > 0) 
                            && (component.MaximumPercentage__c != null && component.MaximumPercentage__c > 0)
                            && component.MaximumPercentage__c <= component.MinimumPercentage__c
                            && item.CAP.HasPfas__c === 'Oui') {
                                let error_max_min = ''
                                if (this.language === 'fr'){
                                    error_max_min = 'La valeur du %Minimum ne doit pas être supérieur au %Maximum.'
                                } else {
                                    error_max_min = '%Minimum value cannot be higher than %Maximum.'
                                }
                                //this.error(this.this.DICT[this.language].error_max_min, 'warning', 'Attention: '+item.CAP.Name+' Substance-'+component.Name)
                                this.error(error_max_min, 'warning', 'Attention: '+item.CAP.Name+' Substance-'+component.Name)
                                return
                        }
                        componentsArray.push(component)
                    }

                    if(item.CAP.HasPfas__c === 'Non' || (item.CAP.HasPfas__c === 'Oui' && (item.Components && item.Components.length > 0))) {
                        capsArray.push(item.CAP)
                        capsIdsArray.push(item.CAP.Id)
                    }
                }
            }

            if(capsArray.length < this.caps.length && this.finalize) {
                let error_unfinished = ''
                let error_saving = ''
                if (this.language === 'fr'){
                    error_unfinished = 'La valeur du %Minimum ne doit pas être supérieur au %Maximum.'
                    error_saving = 'Erreur lors de l\'enregistrement des compléments de FIPs!'
                } else {
                    error_unfinished = '%Minimum value cannot be higher than %Maximum.'
                    error_saving = 'Error while saving your data!'
                }
                //this.error(this.this.DICT[this.language].error_unfinished, 'error', this.this.DICT[this.language].error_saving)
                this.error(error_unfinished, 'error', error_saving)
                return
            }

            const singleState = 100/(capsArray.length+componentsArray.length)

            let capsIdForComponentsPurge = []

            // Updating CAPs
            const capsResult = await sendData({processName: 'updateCAP', jsonData: JSON.stringify(capsArray)})
            capsResult.forEach(cap => {
                if(cap.HasPfas__c === 'Non' || cap.PfasType__c === 'Polymère') {
                    capsIdForComponentsPurge.push(cap.Id)
                }
            })
            pbState = singleState * capsArray.length
            this.handleProgressBarState(pbState)

            const componentToDelete = componentsArray.filter(item => capsIdForComponentsPurge.includes(item.CAP__c))
            let componentsToDeleteIds = []
            componentToDelete.forEach(item => {
                componentsToDeleteIds.push(item.Id)
            })
            let componentsToUpdate = componentsArray.filter(item => !capsIdForComponentsPurge.includes(item.CAP__c))

            // Update Components
            if(componentsToUpdate.length > 0) {
                await sendData({processName: 'updateComponents', jsonData: JSON.stringify(componentsToUpdate)})
                pbState += (singleState*componentsToUpdate.length)
            }
            this.handleProgressBarState(pbState)
            
            // Delete Components on clean when PFAS is False
            if(componentToDelete.length > 0) {
                await sendData({processName: 'deleteComponents', jsonData: JSON.stringify(componentsToDeleteIds)})
            }
            this.handleProgressBarState(100)

            if(this.finalize) {
                //await this.dispatchEvent(new CustomEvent('modalmessagechange', {detail: {message: this.this.DICT[this.language].saving_message}}))
                let savingMessage = ''
                if (this.language === 'fr'){
                    savingMessage = 'Envoi de vos documents pour signature...'
                } else {
                    savingMessage = 'Sending your documents for signature...'
                }
                await this.dispatchEvent(new CustomEvent('modalmessagechange', {detail: {message: savingMessage}}))
                // Sending For Signature
                capsArray.forEach(item => {
                    item.TECH_GenerateCongaDoc__c = true
                })
                await sendData({processName: 'updateCAP', jsonData: JSON.stringify(capsArray)})
                const interval = setInterval(async () => {
                    const docsCreated = await checkIfSentForSig({capsIds: capsIdsArray})
                    if(docsCreated === true) {
                        this.dispatchEvent(new CustomEvent('modaldisplay', {detail: {display: false}}))
                        this.changeStep(this.stepIndicator, 1)
                        clearInterval(interval)
                    }
                }, 5000);
                return;
            }

            // Notify User
            // Display Modal on parent
            await this.dispatchEvent(new CustomEvent('modaldisplay', {detail: {display: true, loader: false}}))
            //await this.dispatchEvent(new CustomEvent('modalmessagechange', {detail: {message: this.this.DICT[this.language].saved_message}}))
            let savedMessage = ''
                if (this.language === 'fr'){
                    savedMessage = 'Vos données ont bien été sauvegardées !'
                } else {
                    savedMessage = 'All your data have been saved successfully !'
                }
            await this.dispatchEvent(new CustomEvent('modalmessagechange', {detail: {message: savedMessage}}))

            // Hide Modal on parent 
            setTimeout(async function() {
                let url = window.location.href+'&step=1'
                window.location.replace(url)
                //await this.dispatchEvent(new CustomEvent('modaldisplay', {detail: {display: false}}))
            }, 2000)
        } catch(e) {
            console.log(e)
            console.log(JSON.parse(JSON.stringify(e)))
        }
    }

    async error(message, theme, label) {
        this.finalize = false
        // eslint-disable-next-line no-await-in-loop
        await LightningAlert.open({
            message: message,
            theme: theme, // a red theme intended for error states
            label: label, // this is the header text
        })
        this.dispatchEvent(new CustomEvent('modaldisplay', {detail: {display: false}}))
    }

    handleStartLoading() {
        if(!this.isLoading) {
            this.dispatchEvent(new CustomEvent('formstartloading'))
        }
    }

    handleFormLoad() {
        window.console.time('Form Load')
        if(this.formLoaded < this.CONSTANTS.IDENTITY_FORMS) {
            this.formLoaded++
        }

        if(this.formLoaded === this.CONSTANTS.IDENTITY_FORMS) {
            this.dispatchEvent(new CustomEvent('formloaded'))
        }
        window.console.timeEnd('Form Load')
    }

    handleEventData(event) {
        if(this.mapCAPComponents.length === 0 || this.mapCAPComponents.findIndex(element => element.CAP.Id === event.detail.CAP__c.Id) === -1) {
            this.mapCAPComponents.push({
                CAP: JSON.parse(JSON.stringify(event.detail.CAP__c)),
                Components: JSON.parse(JSON.stringify(event.detail.Components))
            })
        }

        if(this.mapCAPComponents.findIndex(element => element.CAP.Id === event.detail.CAP__c.Id) > -1) {
            this.mapCAPComponents.find(element => element.CAP.Id === event.detail.CAP__c.Id).CAP = JSON.parse(JSON.stringify(event.detail.CAP__c))
            this.mapCAPComponents.find(element => element.CAP.Id === event.detail.CAP__c.Id).Components = JSON.parse(JSON.stringify(event.detail.Components))
        }
    }

    handleProgressBarState(state) {
        this.dispatchEvent(new CustomEvent('refreshprogressbar', {detail: {state: state}}))
    }

}