export const activeClass = 'slds-theme--inverse step-container slds-align_absolute-center slds-col slds-size_3-of-12 slds-grid slds-wrap slds-box cont'
export const activeClassStep = 'step'
export const activeClassTitle = 'step-title'

export const doneClass = 'slds-theme--default step-container slds-align_absolute-center slds-col slds-size_3-of-12 slds-grid slds-wrap slds-box slds-text-color_inverse-weak step-text cont cont-done'
export const doneClassStep = 'step done'
export const doneClassTitle = 'step-title done'

export const ACCOUNT_NAME = [
    {
        label: 'Nom du compte',
        name: 'Name'
    }
]

export const ACCOUNT_NAME_EN = [
    {
        label: 'Account name',
        name: 'Name'
    }
]

export const ACCOUNT_ADDRESS = [
    {
        label: 'Adresse du compte',
        name: 'BillingAddress'
    }
]

export const ACCOUNT_ADDRESS_EN = [
    {
        label: 'Account address',
        name: 'BillingAddress'
    }
]

export const CONTACT_ERP_NUMBER = [
    {
        label: 'Code client',
        name: 'ERP_CustomerCode__c'
    }
]

export const CONTACT_ERP_NUMBER_EN = [
    {
        label: 'Client code',
        name: 'ERP_CustomerCode__c'
    }
]

export const CONTACT_FROM_FIELDS = [
    {
        label: 'Email client',
        name: 'Email'
    },
    {
        label: 'Téléphone',
        name: 'Phone'
    },
    {
        label: 'Nom du contact',
        name: 'Name'
    }
]

export const CONTACT_FROM_FIELDS_EN = [
    {
        label: 'Client email',
        name: 'Email'
    },
    {
        label: 'Phone',
        name: 'Phone'
    },
    {
        label: 'Contact name',
        name: 'Name'
    }
]

export const IDENTITY_FORMS = 4