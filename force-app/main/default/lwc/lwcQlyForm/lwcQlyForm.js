import { LightningElement, api, track } from 'lwc';
import getApprovStatus from '@salesforce/apex/AP_QLyManager.getApprovStatus';

export default class LwcQlyForm extends LightningElement {

    @api userInfo; 
    @track isDisabled = false;

    isRejected = false;

    // LWC lifecycle hooks
    connectedCallback() {
        console.log('LwcQlyForm.connectedCallback.userInfo -->', JSON.stringify(this.userInfo.companyId));
        getApprovStatus({accountId: this.userInfo.companyId})
        .then((data) => {
            console.log('LwcQlyForm.connectedCallback.data: '+data);
            if(data.length > 0){
                console.log('LwcQlyForm.connectedCallback.getApprovStatus: '+data[0].ApprovalStatus__c);
                if(String(data[0].ApprovalStatus__c) === 'Rejeté')
                {
                    this.isRejected = true;
                }
                console.log('LwcQlyForm.connectedCallback.isRejected -->', this.isRejected);
            }
        })
        .catch((error) => {
            console.log('LwcQlyForm.connectedCallback.error: '+error);
        });
     }

    renderedCallback() { 
        
    }


    get displayStreet(){
        if(this.userInfo.companyAddress){
            return this.userInfo.companyAddress.street;
        }else {
            return '';
        }
    }

    get displayCity(){
        if(this.userInfo.companyAddress){
            return this.userInfo.companyAddress.city;
        }else {
            return '';
        }
    }

    get displayCountry(){
        if(this.userInfo.companyAddress){
            return this.userInfo.companyAddress.country;
        }else {
            return '';
        }
    }

    get displayPostalCode(){
        if(this.userInfo.companyAddress){
            return this.userInfo.companyAddress.postalCode;
        }else {
            return '';
        }
    }  

    //method to send form inout field to parent (lwc-Main)
    handleUserInputChange(event) {

        let field = '';      
        let fieldType ='';
         //to save where the event notify from
        const eventOrigin = event.detail.eventOrigin;        
        if(eventOrigin === 'fromChild'){
             field = event.detail.field;      
             fieldType =  event.detail.fieldType;
        }else{
             field = event.target.name ;      
             fieldType =  event.currentTarget.dataset.type;
        } 
        //get the day and time for service 
        const when = event.detail.when;

        console.log('LwcQlyForm.handleUserInputChange.when === ', when); 
        console.log('LwcQlyForm.handleUserInputChange.eventOrigin === ', eventOrigin); 
        console.log('LwcQlyForm.handleUserInputChange.field === ', field); 
        console.log('LwcQlyForm.handleUserInputChange.fieldType === ', fieldType);      
      
        if (fieldType.includes("Address")) {
            let address ={};
            if(eventOrigin === 'fromChild'){                      
                console.log('Street => ', event.detail.inputValue.street);
                console.log('City => ', event.detail.inputValue.city);
                console.log('Province => ', event.detail.inputValue.province);
                console.log('Country => ', event.detail.inputValue.country);
                console.log('postal Code => ', event.detail.inputValue.postalCode);
                address = {
                    street: event.detail.inputValue.street,
                    city:event.detail.inputValue.city,
                    province: event.detail.inputValue.province,
                    country: event.detail.inputValue.country,
                    postalCode: event.detail.inputValue.postalCode
                };
            }else{
                console.log('Street => ', event.target.street);
                console.log('City => ', event.target.city);
                console.log('Province => ', event.target.province);
                console.log('Country => ', event.target.country);
                console.log('postal Code => ', event.target.postalCode);
                address = {
                    street: event.target.street,
                    city: event.target.city,
                    province: event.target.province,
                    country: event.target.country,
                    postalCode: event.target.postalCode
                };
            }           

            console.log('LwcQlyForm.handleUserInputChange.companyAddress === ', address);  
            let userInputValue = { field: field, inputValue: address, fieldType : fieldType };
            const userInputEvent = new CustomEvent(
                "userinputvalue", { detail: userInputValue }
            );
            this.dispatchEvent(userInputEvent);
            //sendDataToParent();           

        } else if (fieldType === 'checkbox') { 
            const inputValue = eventOrigin === 'fromChild' ? event.detail.inputValue : event.target.checked;
            console.log('LwcQlyForm.handleUserInputChange.inputValue === ', inputValue); 
            console.log('LwcQlyForm.handleUserInputChange.fieldType === ', fieldType);        
            let userInputValue = { field: field, inputValue: inputValue, fieldType: fieldType, when : when};
            const userInputEvent = new CustomEvent(
                "userinputvalue", { detail: userInputValue }
            );
            this.dispatchEvent(userInputEvent);
            //sendDataToParent();
            
        }    
        else if (fieldType === 'input') {         
            const inputValue = eventOrigin === 'fromChild' ? event.detail.inputValue : event.detail.inputValue.referentCollectcInput ;
            let userInputValue = { field: field, inputValue: inputValue, fieldType: fieldType, when : when};
            const userInputEvent = new CustomEvent(
                "userinputvalue", { detail: userInputValue }
            );
            this.dispatchEvent(userInputEvent);
            //sendDataToParent();
            
        }    
        else {//others input fieldType
            const inputValue = event.target.value;
            console.log('LwcQlyForm.handleUserInputChange.inputValue === ', inputValue); 
                
            let userInputValue = { field: field, inputValue: inputValue, fieldType: fieldType };
            const userInputEvent = new CustomEvent(
                "userinputvalue", { detail: userInputValue }
            );
            this.dispatchEvent(userInputEvent);
            //sendDataToParent();
        }
        this.isDisabled = false;
    }

    handleSave(event){ 
        this.isDisabled = true;
        console.log('LwcQlyForm.handleSave --- start' );       
        //send to main
        const saveEvent = new CustomEvent(
            "save" );  
            console.log('lucas save event : ' + saveEvent);
            this.dispatchEvent(saveEvent);      
     }
}