import { LightningElement, api, track } from 'lwc';
import cancelForm from '@salesforce/apex/AP_QLyManager.cancelForm';

import {
    FlowNavigationNextEvent,FlowNavigationFinishEvent
} from 'lightning/flowSupport';

export default class LwcQlyCancelForm extends LightningElement {

    @api qlyFormId;
    @api fromFlow;

    @track wait = false;
    @track init = true;
    @track success = false;

    @track msg;

    handleClose(event) {
        if(this.fromFlow){
            event.preventDefault();
            event.stopPropagation();
            const navigateFinishEvent = new FlowNavigationFinishEvent();
            this.dispatchEvent(navigateFinishEvent);
        } else {
            this.dispatchEvent(new CustomEvent('close'));
        }
    }

    handleSubmit(event) {
        try {
            this.wait = true;
            event.preventDefault();
            var fields = event.detail.fields;
            fields.Id = this.qlyFormId;
            cancelForm({jsonSobject: JSON.stringify(fields)})
            .then((data) => {
                if(!data || data == 'NOK') {
                    this.msg = 'Il y a eu une erreur lors de l\'annulation de votre collecte, veuillez réessayer ou contacter un administrateur.';
                } else if(data && data == 'OK') {
                    this.msg = 'La collecte a bien été annulée.';
                    this.success = true;
                    if(!this.fromFlow) {
                        setTimeout(function() {
                            window.location.reload();
                        }, 3000);
                    }
                }
                this.wait = false;
                this.init = false;
            })
        } catch(e) {
            console.log(e)
        }
    }

    retry(event) {
        this.init = true;
        this.wait = false;
    }

}