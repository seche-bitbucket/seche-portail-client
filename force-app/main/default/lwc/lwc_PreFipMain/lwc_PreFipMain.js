import { LightningElement, api, track } from 'lwc'
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

// Polymerphic controller
import getRecord from '@salesforce/apex/LWC_PolymorphicProcedures.getRecord'

// System Utils
import getSObjectFields from '@salesforce/apex/CAP_Global_SF_SysTools.getSObjectFields';
import uploadFile from '@salesforce/apex/CAP_Global_SF_SysTools.uploadFile';
import deleteBySingleId from '@salesforce/apex/CAP_Global_SF_SysTools.deleteBySingleId';

//Seche Sys Utils
import getRecordByGUID from '@salesforce/apex/SecheSystemUtils.getRecordByGUID'

// PreFip Manager
import validateSign from '@salesforce/apex/AP_PreFipManager.validateSign';
import fetchQuestions from '@salesforce/apex/AP_PreFipManager.fetchQuestions';
import fetchLinkedAttestation from '@salesforce/apex/AP_PreFipManager.fetchLinkedAttestation';
import fetchLinkedAttestationsFromForm from '@salesforce/apex/AP_PreFipManager.fetchLinkedAttestationsFromForm';
import { RecordFieldDataType } from 'lightning/uiRecordApi';
import PreFipCaracterisationTemplateFR from '@salesforce/resourceUrl/PreFip_Caracterisation_Template_FR';

const activeClass = 'slds-theme--inverse step-container slds-align_absolute-center slds-col slds-size_3-of-12 slds-grid slds-wrap slds-box cont'
const activeClassStep = 'step'
const activeClassTitle = 'step-title'

const doneClass = 'slds-theme--default step-container slds-align_absolute-center slds-col slds-size_3-of-12 slds-grid slds-wrap slds-box slds-text-color_inverse-weak step-text cont cont-done'
const doneClassStep = 'step done'
const doneClassTitle = 'step-title done'

export default class Lwc_PreFipMain extends LightningElement {

    // Shared Vars
    @api guid

    // Tracked Vars
    @track recordInfo
    @track userInfo
    @track accountInfo
    @track stepState = {
        step1: false,
        step2: false,
        step3: false,
        step4: false
    }
    @track notProducer = null
    @track producer = {
        address: {}
    }
    @track currentStep = 0
    @track dataGridRecords
    @track attestations
    @track questions
    @track globalTitle = 'Vérifier votre saisie.'
    @track globalMessage = 'Veuillez répondre à toutes les questions, renseigner des descriptions et ajouter des infos complémentaires.'
    @track defaultAnswer = []
    @track files = []
    @track wait = true
    @track validated = false
    @track pageNotFoud = false
    @track error = false
    
    // Const
    userFormFields = [
        {
            label: 'Prénom',
            name: 'FirstName'
        },
        {
            label: 'Nom',
            name: 'LastName'
        },
        {
            label: 'Téléphone',
            name: 'Phone'
        },
        {
            label: 'E-mail',
            name: 'Email'
        },
    ]
    accountFormFields = [
        {
            label: 'Société / Collectivité',
            name: 'Name'
        },
        {
            label: 'N° SIRET',
            name: 'SIRET_Number__c'
        },
        {
            label: 'Adresse',
            name: 'BillingStreet'
        },
        {
            label: 'Ville',
            name: 'BillingCity'
        },
    ]
    iamNotProducerOptions = [
        {
            label: 'Oui',
            value: 'Non'
        },
        {
            label: 'Non',
            value: 'Oui'
        }
    ]
    answerOptions = [
        {
            value: 'Oui',
            label: 'Oui'
        },
        {
            value: 'Non concerné',
            label: 'Non concerné'
        }
    ]
    wasteRegulationOptions = [
        {
            value: 'Oui',
            label: 'Oui'
        },
        {
            value: 'Non',
            label: 'Non'
        }
    ]
    exclude = [
        'Non : Déchets dont la valorisation est interdite ou dont l’élimination est prescrite en vertu de l\'arrêté du 31/12/2018',
        'Non : Déchets et refus de tri issus d’une installation de tri performante en vertu de l\' Arrêté du 29 juin 2021',
        'Non : Résidus de tri issus d’installations qui réalisent un tri de déchets'
    ]

    @track STEPS = [
        {
            id: 'step1',
            name: 'Étape 1',
            title: 'Vos informations',
            class: activeClass,
            nameClass: activeClassStep,
            titleClass: activeClassTitle,
            done: false
        },
        {
            id: 'step2',
            name: 'Étape 2',
            title: 'Informations sur vos déchets',
            class: doneClass,
            nameClass: doneClassStep,
            titleClass: doneClassTitle,
            done: false
        },
        {
            id: 'step3',
            name: 'Étape 3',
            title: 'Détails sur vos déchets',
            class: doneClass,
            nameClass: doneClassStep,
            titleClass: doneClassTitle,
            done: false
        },
        {
            id: 'step4',
            name: 'Étape 4',
            title: 'Synthèse & validation',
            class: doneClass,
            nameClass: doneClassStep,
            titleClass: doneClassTitle,
            done: false
        }
    ]
    @track columns = [
        {
            value: 'TECH_WasteName__c',
            label: 'Déchet',
            visible: true,
            editable: false,
            index: 0
        },
        {
            value: 'ExemptionReason__c',
            label: 'Déchet soumis à caractérisation',
            editable: true,
            edit: true,
            required: true,
            visible: true,
            valueMissing: 'Veuiller renseigner la condition',
            index: 1
        },
        {
            value: 'ExemptionReasonAdditionalInfo__c',
            label: 'Infos complémentaires / PJ attendue',
            editable: false,
            visible: true,
            index: 2
        },
        {
            value: 'Renewal__c',
            label: 'Renouvellement',
            editable: false,
            visible: true,
            index: 3
        },
        {
            value: 'N_CAP__c',
            label: 'N°CAP',
            editable: false,
            visible: true,
            index: 4
        },
        {
            value: 'DisplayQuestions__c',
            label: 'DisplayQuestions__c',
            editable: false,
            visible: false,
            index: 5
        },
        {
            value: 'TECH_MandatoryFile__c',
            label: 'TECH_MandatoryFile__c',
            editable: false,
            visible: false,
            index: 6,
            dependant: {}
        }
    ]
    @track preview = [
        {
            value: 'TECH_WasteName__c',
            label: 'Déchet',
            visible: true,
            editable: false,
            index: 0
        },
        {
            value: 'ExemptionReason__c',
            label: 'Déchet soumis à caractérisation',
            editable: false,
            visible: true,
            index: 1
        },
        {
            value: 'Files',
            label: 'Justificatifs à recevoir',
            editable: false,
            visible: true,
            index: 2
        },
        {
            value: 'AdditionnalFiles',
            label: 'Vos documents',
            editable: false,
            visible: true,
            index: 3
        }
    ]
    @track displayFields = []
    @track displayPreviewFields = []

    @track loadingAddress = {}

    get displayForm() {
        return (this.notProducer == 'Oui') ? true : false
    }

    get displayPrevious() {
        return (this.currentStep > 0) ? true : false;
    }

    get displayFieldsString() {
        return JSON.stringify(this.displayFields);
    }

    get displayPreviewFieldsString() {
        return JSON.stringify(this.preview);
    }

    get wasteNumber() {
        return '('+this.dataGridRecords.length+')'
    }

    get questionsFormTitle() {
        return (this.dataGridRecords[0].TECH_AccountNature__c == 'Collectivité et administration') ? 'Liste des obligations de collecte séparées' : 'Liste des obligations de tri'
    }

    connectedCallback() {
        
        window.onbeforeunload = function() {
            return "Vous allez perdre vos données.";
        }

        // fetch guid from portalVerification
        // getRecordByGUID({GUID: this.guid, objectApiName: 'Opportunity', GUID_FIELD: 'GUID__c'})
        // .then(data => {
        //     this.wait = true
        //     if(data) {
        //         this.recordInfo = data
        //         getRecord({o: 'OpportunityLineItem', recordId: this.recordInfo.Id})
        //         .then(data => {

        //             this.dataGridRecords = data
        //             this.dataGridRecords[0].firstOne = true
        //             this.dataGridRecords.forEach(record => {
        //                 record.WasteRegulaitonAnswer = null
        //                 record.AnnualWasteRegulation__c = null
        //                 record.FirstCss = ''
        //                 record.RestCss = ''
        //                 record.AnswerYes = false
        //                 if(!record.firstOne) record.firstOne = false
        //             })
        //             var lstIds = []
        //             this.dataGridRecords.forEach(d => {
        //                 lstIds.push(d.Id)
        //             })
        //             fetchLinkedAttestation({oppsIds: lstIds})
        //             .then(data => {
        //                 this.attestations = data
        //                 getRecord({o: 'Contact', recordId: this.attestations[0].Contact__c})
        //                 .then(data => {
        //                     this.userInfo = data[0]
        //                     this.stepState.step1 = true
        //                     getRecord({o: 'OpportunityLineItem', recordId: this.recordInfo.Id})
        //                     .then(data => {
        //                         this.dataGridRecords = data
        //                         this.dataGridRecords[0].firstOne = true
        //                         this.dataGridRecords.forEach(record => {
        //                             record.WasteRegulaitonAnswer = null
        //                             record.AnnualWasteRegulation__c = null
        //                             record.FirstCss = ''
        //                             record.RestCss = ''
        //                             record.AnswerYes = false
        //                             record.DisplayAttachments = false
        //                             if(!record.firstOne) record.firstOne = false
        //                         })
        //                         getSObjectFields({sobjectName: 'OpportunityLineItem', withRef: false})
        //                         .then(data => {
        //                             try {
        //                                 var lst = []
        //                                 data.forEach(field => {
        //                                     var element = this.columns.find(e => e.value == field.value)
        //                                     if(element) {
        //                                         field.label = element.label
        //                                         field.editable = element.editable
        //                                         field.index = element.index
        //                                         field.edit = element.edit
        //                                         field.required = element.required
        //                                         field.valueMissing = element.valueMissing
        //                                         field.visible = element.visible
        //                                         lst.push(field)
        //                                     }
        //                                 })
        //                                 this.displayFields = lst.sort((a, b) => (a.index > b.index) ? 1 : -1)
        //                                 this.dataGridRecords.forEach(record => {
        //                                     record.Attestation = this.attestations.find(att => att.OpportunityLineItem__c == record.Id).Id
        //                                 })
        //                                 fetchQuestions({accountNature: this.dataGridRecords[0].TECH_AccountNature__c})
        //                                 .then(data => {
        //                                     this.questions = data
        //                                     getSObjectFields({sobjectName: 'OpportunityLineItem', withRef: false})
        //                                     .then(data => {
        //                                         try {
        //                                             this.columns.find(clmn => clmn.label == 'TECH_MandatoryFile__c').dependant = {
        //                                                 fieldName: data.find(d => d.value == 'TECH_MandatoryFile__c').controllingField,
        //                                                 options: data.find(d => d.value == 'TECH_MandatoryFile__c').controllingValues
        //                                             }
        //                                             this.wait = false
        //                                         } catch(e) {
        //                                             console.log(e)
        //                                         }
        //                                     })
        //                                 })
        //                             } catch(e) {
        //                                 console.log(e)
        //                             }
        //                         });
        //                     })
        //                 })
        //             })
        //         })
        //     } else {
        //         this.pageNotFoud = true
        //     }
        //     this.wait = false
        // })

        // =================================================================

                // fetch guid from portalVerification
                getRecordByGUID({GUID: this.guid, objectApiName: 'Form__c', GUID_FIELD: 'GUID__c'})
                .then(data => {
                    this.wait = true
                    if(data) {
                        this.recordInfo = data
                        getRecord({o: 'OpportunityLineItem', recordId: this.guid})
                        .then(data => {
                            this.dataGridRecords = data
                            this.dataGridRecords[0].firstOne = true
                            this.dataGridRecords.forEach(record => {
                                record.WasteRegulaitonAnswer = null
                                record.AnnualWasteRegulation__c = null
                                record.FirstCss = ''
                                record.RestCss = ''
                                record.AnswerYes = false
                                if(!record.firstOne) record.firstOne = false
                            })
                            var lstIds = []
                            this.dataGridRecords.forEach(d => {
                                lstIds.push(d.Id)
                            })
                            fetchLinkedAttestationsFromForm({formsIds: this.recordInfo.Id})
                            .then(data => {
                                this.attestations = data
                                this.accountInfo = this.attestations[0].Account__c
                                console.log('Alex debug accountInfo', JSON.stringify(this.accountInfo))
                                getRecord({o: 'Contact', recordId: this.recordInfo.Contact__c})
                                .then(data => {
                                    this.userInfo = data[0]
                                    this.stepState.step1 = true
                                    getRecord({o: 'OpportunityLineItem', recordId: this.guid})
                                    .then(data => {
                                        this.dataGridRecords = data
                                        this.dataGridRecords[0].firstOne = true
                                        this.dataGridRecords.forEach(record => {
                                            record.WasteRegulaitonAnswer = null
                                            record.AnnualWasteRegulation__c = null
                                            record.FirstCss = ''
                                            record.RestCss = ''
                                            record.AnswerYes = false
                                            record.DisplayAttachments = false
                                            record.DisplayAttachmentsTri = true
                                            record.ExemptionR= false
                                            if(!record.firstOne) record.firstOne = false
                                        })
                                        getSObjectFields({sobjectName: 'OpportunityLineItem', withRef: false})
                                        .then(data => {
                                            try {
                                                var lst = []
                                                data.forEach(field => {
                                                    var element = this.columns.find(e => e.value == field.value)
                                                    if(element) {
                                                        field.label = element.label
                                                        field.editable = element.editable
                                                        field.index = element.index
                                                        field.edit = element.edit
                                                        field.required = element.required
                                                        field.valueMissing = element.valueMissing
                                                        field.visible = element.visible
                                                        lst.push(field)
                                                    }
                                                })
                                                this.displayFields = lst.sort((a, b) => (a.index > b.index) ? 1 : -1)
                                                this.dataGridRecords.forEach(record => {
                                                    record.Attestation = this.attestations.find(att => att.OpportunityLineItem__c == record.Id).Id
                                                })
                                                fetchQuestions({accountNature: this.dataGridRecords[0].TECH_AccountNature__c})
                                                .then(data => {
                                                    this.questions = data
                                                    getSObjectFields({sobjectName: 'OpportunityLineItem', withRef: false})
                                                    .then(data => {
                                                        try {
                                                            this.columns.find(clmn => clmn.label == 'TECH_MandatoryFile__c').dependant = {
                                                                fieldName: data.find(d => d.value == 'TECH_MandatoryFile__c').controllingField,
                                                                options: data.find(d => d.value == 'TECH_MandatoryFile__c').controllingValues
                                                            }
                                                            this.wait = false
                                                        } catch(e) {
                                                            console.log(e)
                                                        }
                                                    })
                                                })
                                            } catch(e) {
                                                console.log(e)
                                            }
                                        });
                                    })
                                })
                            })
                        })
                    } else {
                        this.pageNotFoud = true
                    }
                    this.wait = false
                })

    }

    renderedCallback() {
        this.loadingAddress.province = 'TEST'
        if(this.producer.address) this.producer.address.province = 'TEST'
    }

    disconnectedCallback() {
        // delete files when browser closed
        if(!this.validated) {

        }
    }

    handleUserInput(event) {
        try {
            event.stopPropagation()
            var name = (event.target.name) ? event.target.name : event.target.dataset.name
            if(name == 'wasteRegulaitonFile') {
                this.dataGridRecords.find(record => record.Id == event.target.dataset.recordId).WasteRegulaitonAnswer = true
                this.dataGridRecords.find(record => record.Id == event.target.dataset.recordId).AnnualWasteRegulation__c = event.target.value 
                this.dataGridRecords.find(record => record.Id == event.target.dataset.recordId)['WasteRegulaitonFile'] = (event.target.value == 'Oui') ? true : false 
                this.dataGridRecords.find(record => record.Id == event.target.dataset.recordId)['WasteRegulaitonFile'] = (event.target.value == 'Oui') ? true : false 
            } else if(name == "iamnotproducer") {
                this.notProducer = event.target.value
                this.producer.address =  {} 
            } else if(name.includes("producer")) {
                if(name.includes('Address')) {
                    this.producer.address = {
                        street: event.target.street,
                        city: event.target.city,
                        postalCode: event.target.postalCode,
                        address: event.target.street+', '+event.target.postalCode+' '+event.target.city+', '+event.target.country
                    }
                } else {
                    this.producer[event.currentTarget.dataset.position] = event.target.value
                }

            } else if(name == 'loadAddress') {
                this.loadingAddress = {
                    street: event.target.street,
                    city: event.target.city,
                    postalCode: event.target.postalCode,
                    address: event.target.street+', '+event.target.postalCode+' '+event.target.city+', '+event.target.country
                }
            } else if(name == 'dataGrid') {
                event.detail.changes.forEach(modif => {
                    this.dataGridRecords.find(record => record.Id == modif.recordId)[modif.field] = modif.value
                    this.dataGridRecords.find(record => record.Id == modif.recordId).DisplayAttachments = (this.exclude.includes(modif.value)) ? false : true
                })
                event.detail.processedRecords.forEach(record => {
                    if(record.isChanged) {
                        record.fields.forEach(field => {
                            if(['ExemptionReason__c', 'ExemptionReasonAdditionalInfo__c'].includes(field.name)) {
                                if(field.name == 'ExemptionReason__c') field.disable = true
                                field.numChanges = 0
                                this.dataGridRecords.find(r => r.Id == record.Id)[field.name.substring(0, field.name.length - 3)] = field
                            }
                        })
                    }
                })
                this.dataGridRecords.forEach(record => {
                  /*  record.ExemptionReason = false;
                    if (record.ExemptionReason__c =="Oui"){ record.ExemptionReason =true;}*/
                    if(record.DisplayQuestions__c && record.DisplayQuestions__c == 'Oui') {
                        record.features = []
                        this.lastQuestionPositioning()
                        this.questions.forEach(qst => {
                            record.features.push(
                                {
                                    isLastQuestion__c: qst.isLastQuestion__c,
                                    Question__c: qst.Question__c,
                                    Attestation__c: this.attestations.find(att => att.OpportunityLineItem__c == record.Id).Id,
                                    TECH_DescriptionPlaceholder__c: qst.DescriptionPlaceholder__c,
                                    index: record.features.length
                                }
                                )
                            })
                    } else {
                        delete record.features
                    }
                })
            } else if(name == 'additional-info') {
                this.dataGridRecords.find(r => r.Id == event.target.dataset.recordId)[event.target.dataset.fieldName] = event.target.value
                // this.dataGridRecords.find(r => r.Id == event.target.dataset.recordId).TECH_MandatoryFile__c = this.columns.find(clmn => clmn.label == 'TECH_MandatoryFile__c').dependant.options[event.target.value][0].value
            } else if(name == 'feature') {
                if(this.dataGridRecords.find(r => r.Id == event.target.dataset.recordId).firstOne && (!this.defaultAnswer.find(qst => qst.index == event.target.dataset.featureIndex) || !this.defaultAnswer.find(qst => qst.index == event.target.dataset.featureIndex)[event.target.dataset.fieldName])) {
                    this.defaultAnswer.push({
                        index: event.target.dataset.featureIndex
                    }) 
                    this.defaultAnswer.find(qst => qst.index == event.target.dataset.featureIndex)[event.target.dataset.fieldName] = event.target.value
                    this.dataGridRecords.forEach(record => {
                        if(!record.firstOne && record.features && !record.features.find(f => f.index == event.target.dataset.featureIndex)[event.target.dataset.fieldName]) {
                            record.features.find(f => f.index == event.target.dataset.featureIndex)[event.target.dataset.fieldName] = event.target.value
                        }
                    })
                }
                this.dataGridRecords.find(r => r.Id == event.target.dataset.recordId).features.find(f => f.index == event.target.dataset.featureIndex)[event.target.dataset.fieldName] = event.target.value
                if(event.target.value == 'Oui' && event.target.dataset.fieldName == 'Answer__c') {
                    this.dataGridRecords.find(r => r.Id == event.target.dataset.recordId).features.find(f => f.index == event.target.dataset.featureIndex).AnswerYes = true
                } else if(event.target.value == 'Non concerné' && event.target.dataset.fieldName == 'Answer__c') {
                    this.dataGridRecords.find(r => r.Id == event.target.dataset.recordId).features.find(f => f.index == event.target.dataset.featureIndex).AnswerYes = false 
                }
            }
        } catch(e) {
            console.log(e)
        }
    }

    handleUpload(event) {
        try {
            this.wait = true
            const oliId = event.target.dataset.recordId
            const attestationId = this.attestations.find(att => att.OpportunityLineItem__c == event.target.dataset.recordId).Id
            const attestationOwner = this.attestations.find(att => att.OpportunityLineItem__c == event.target.dataset.recordId).OwnerId
            const targetName = event.target.name
            const file = event.target.files[0]
            const toBase64 = (file) =>
            new Promise((resolve, reject) => {
                const reader = new FileReader()
                reader.onload = () => {
                    resolve(reader.result)                   
                }
                reader.readAsDataURL(file)
                reader.onerror = (error) => reject(error);
            });
            toBase64(file)
            .then(result => {
                const base64Constant = 'base64,';
                const base64Value = result.indexOf(base64Constant) + base64Constant.length;
                var base64Data = result.substring(base64Value);
                var fileName = file.name.substring(0, file.name.lastIndexOf('.'))+'_'+this.attestations.find(att => att.Id == attestationId).Name.substring(0, 13)+file.name.substring(file.name.lastIndexOf('.'), file.name.length)
                if(fileName.length > 62) {
                    var title = 'Erreur lors du chargement de la pj'
                    var message = 'Le nombre de caractère du titre ne doit pas être supérieur à 62'
                    this.displayToast(title, message, 'error')
                    this.wait = false
                    return;
                }
                if(file.size > 2900000) {
                    var title = 'Erreur lors du chargement de la pj'
                    var message = 'Taille de fichier maximale autorisée dépassée. Veuillez ajouter une pièce jointe inférieure à 2.9 Mb.'
                    this.displayToast(title, message, 'error')
                    this.wait = false
                    return;
                }
                this.masterUpload(base64Data, fileName, attestationId, oliId, attestationOwner, targetName)
            });
        } catch(e) {
            console.log(e)
        }
    }

    // upload
    masterUpload(base64, filename, attestationId, oliId, ownerId, fieldName) {
        try { 
            this.wait = true
            uploadFile({ base64: base64, filename: filename, recordId: attestationId, ownerId: ownerId}).then(result=>{
                if(result) {
                    let title = filename+' uploaded successfully!!'
                    this.displayToast(title, null, 'success')
                    if(this.dataGridRecords.find(record => record.Id == oliId).files 
                        && this.dataGridRecords.find(record => record.Id == oliId).files[fieldName]) {
                            this.dataGridRecords.find(record => record.Id == oliId).files[fieldName].push({
                                Id: result,
                                title: filename
                            })
                    } else if(this.dataGridRecords.find(record => record.Id == oliId).files
                        && !this.dataGridRecords.find(record => record.Id == oliId).files[fieldName]) {
                            this.dataGridRecords.find(record => record.Id == oliId).files[fieldName] = [{
                                Id: result,
                                title: filename
                            }]
                    } else {
                        this.dataGridRecords.find(record => record.Id == oliId).files = {
                            Id: oliId,
                            [fieldName]: [{
                                Id: result,
                                title: filename
                            }]
                        }
                    }
                    delete this.dataGridRecords.find(record => record.Id == oliId).uploadInput
                    delete this.dataGridRecords.find(record => record.Id == oliId).regulationUploadInput
                    this.wait = false
                }
            })
        } catch(e) {
            console.log(e)
        }
    }

    async lastQuestionPositioning() {
        let lastQuestion = this.questions.find(q => q.isLastQuestion__c == true)
        if(lastQuestion) {
            let tmp = this.questions.filter(q => !q.isLastQuestion__c)
            tmp.push(lastQuestion)
            this.questions = tmp
        }
    }

    async handleNext(event) {
        try{
            var title = this.globalTitle
            var message = null
            if(this.currentStep == 0) {
                if(this.notProducer == 'Non') {
                    this.producer = {
                        FirstName: this.userInfo.FirstName,
                        LastName: this.userInfo.LastName,
                        Corp: this.userInfo.TECH_AccountName__c,
                        Address: this.userInfo.TECH_AccountAddress__c,
                        OperationalAgency: this.producer.OperationalAgency
                    }
                }
            } else if(this.currentStep == 1) {
                title = 'Informations manquantes'
                message = 'Vous devez saisir un motif dérogatoire pour chaque déchet'
                for(let record of this.dataGridRecords) {
                    if(!record.ExemptionReasonAdditionalInfo && !record.ExemptionReason) {
                        const result = await this.template.querySelector('c-lwc_-data-grid').apiTriggerChange(record.Id, 'ExemptionReason__c', record.ExemptionReason__c, null)
                        // .then(result => {
                            result.changes.forEach(modif => {
                                record[modif.field] = modif.value
                                record.DisplayAttachments = (this.exclude.includes(modif.value)) ? false : true
                            })
                            result.processedRecords.forEach(pRecord => {
                                if(record.Id == pRecord.Id) {
                                    pRecord.fields.forEach(field => {
                                        if(['ExemptionReason__c', 'ExemptionReasonAdditionalInfo__c'].includes(field.name)) {
                                            if(field.name == 'ExemptionReason__c') field.disable = true
                                            field.numChanges = 0
                                            record[field.name.substring(0, field.name.length - 3)] = field
                                        }
                                    })
                                }
                            })
                            if(record.DisplayQuestions__c && record.DisplayQuestions__c == 'Oui') {
                                if(!record.features) {
                                    record.features = []
                                    await this.lastQuestionPositioning()
                                    this.questions.forEach(qst => {
                                        record.features.push(
                                            {
                                                Question__c: qst.Question__c,
                                                Attestation__c: this.attestations.find(att => att.OpportunityLineItem__c == record.Id).Id,
                                                TECH_DescriptionPlaceholder__c: qst.DescriptionPlaceholder__c,
                                                index: record.features.length
                                            }
                                        )
                                    })
                                }
                            } else {
                                delete record.features
                            }
                        // })
                    }
                }
            } else if(this.currentStep == 2) {
                message = this.globalMessage
                var missingFile = false
               // alert(record.ExemptionReason.value);
                try {
                    this.dataGridRecords.forEach(record => {
                        delete record.Files
                        if(!missingFile && (record.TECH_MandatoryFile__c == 'Oui' && (!record.files || record.files && !record.files["additionalFile"]))) {
                            title = 'Informations manquantes'
                            message = 'Vous devez obligatoirement ajouter un fichier'
                            record.uploadInput = 'error'
                            this.displayToast(title, message, 'warning')
                            missingFile = true
                        } 
                        else if(!missingFile && ( (!record.files || record.files && !record.files["caracterisationFile"]))  && record.ExemptionR== true) {
                            title = 'Informations manquantes'
                            message = 'Vous devez obligatoirement ajouter un fichier de la caractérisation annuelle réglementaire de votre déchet'
                            record.regulationUploadInput = 'error'
                            this.displayToast(title, message, 'warning')
                            missingFile = true
                        } 
                        
                        else {
                            if(!missingFile && record.files && record.files["additionalFile"]) {
                                record.files["additionalFile"].forEach(f => {
                                    if(record.Files) {
                                        record.Files += ', '+f.title
                                    } else {
                                        record.Files = f.title
                                    }
                                })
                            }
                            
                            if(record.files && record.files["descriptionFile"]) {
                                record.files["descriptionFile"].forEach(f => {
                                    if(record.AdditionnalFiles) {
                                        record.AdditionnalFiles += ', '+f.title
                                    } else {
                                        record.AdditionnalFiles = f.title
                                    }
                                })
                            }
    
                            if(record.files && record.files["caracterisationFile"]) {
                                record.files["caracterisationFile"].forEach(f => {
                                    if(record.AdditionnalFiles) {
                                        record.AdditionnalFiles += ', '+f.title
                                    } else {
                                        record.AdditionnalFiles = f.title
                                    }
                                })
                            }
                        }
                        
                    })
                } catch(e) {
                    console.log(e)
                } finally {
                    if(missingFile) {
                        return null
                    }
                }
            }
            this.navigation(1, message, title)
            if(this.currentStep == 2) {
                try {
                    this.dataGridRecords.forEach(record => {
                        if(record.ExemptionReason.value == 'Oui') {
                            record.ExemptionR = true
                            record.DisplayLine = false
                            record.FirstCss = 'width: calc(100%/1);'
                            record.RestCss = 'display: none;'
                        }
                        else if(record.ExemptionReason.value == 'Oui : Déchets et refus de tri issus d’une installation de tri performante en vertu de l\' Arrêté du 29 juin 2021' || record.ExemptionReason.value == 'Oui : Résidus de tri issus d’installations qui réalisent un tri de déchets') {
                                record.ExemptionR = true
                                record.DisplayAttachments =true;
                                record.DisplayAttachmentsTri =false;
                                record.DisplayLine = true
                                record.RestCss = record.FirstCss = 'width: calc(100%/3);'
                             
                        }
                        else {
                            record.ExemptionR = false
                            record.DisplayLine = true
                            record.RestCss = record.FirstCss = 'width: calc(100%/3);'
                        }
                        /* if(record.ExemptionReasonAdditionalInfo.dependentValues && record.ExemptionReasonAdditionalInfo.dependentValues.length == 1) {
                            record.TECH_MandatoryFile__c = this.columns.find(clmn => clmn.label == 'TECH_MandatoryFile__c').dependant.options[record.ExemptionReasonAdditionalInfo.dependentValues[0].value][0].value
                        } else */ if(!record.ExemptionReasonAdditionalInfo.dependentValues) {
                            record.ExemptionReasonAdditionalInfo.disable = true
                        }

                        if(record.ExemptionReasonAdditionalInfo.value == 'Liste de choix à l\'étape suivante') {
                            record.ExemptionReasonAdditionalInfo.value = null
                            record.ExemptionReasonAdditionalInfo__c = null
                        }
                    })
                    setTimeout(function() {
                        const details = Array.from(this.template.querySelectorAll('details'))
                        details[0].open = true
                        details.forEach((detail) => {
                            detail.addEventListener('click', (e) => {
                                const active = details.find(d => d.open)
                                if (!e.currentTarget.open && active) {
                                    active.open = false
                                }
                            })
                        })
                    }.bind(this), 10)
                } catch(e) {
                    console.log(e)
                }
            } else if(this.currentStep == 3) {
                this.wait = true
                await this.sendPreFip()
            }
        } catch(e) {
            console.log(e)
        }
    }

    handlePrevious(event) {
        var title = this.globalTitle
        var message
        this.navigation(-1, message, title)
    }

    async sendPreFip() {
        try {
            var features = []
            var attCopy = JSON.parse(JSON.stringify(this.attestations))
            var dataCopy = JSON.parse(JSON.stringify(this.dataGridRecords))
            dataCopy.forEach(record => {
                if(record.features) {
                    record.features.forEach(f => {
                        delete f.AnswerYes
                    })
                }
                Array.prototype.push.apply(features, record.features)
                if(record.files) {
                    var htmlFilesUL = '<ul>' //	<ul><li>file1</li><li>file2</li><li>file3</li></ul>
                    if(record.files.additionalFile)
                        record.files.additionalFile.forEach(file => {
                            htmlFilesUL += '<li>'+file.title+'</li>'
                        })
                    if(record.files.descriptionFile)
                        record.files.descriptionFile.forEach(file => {
                            htmlFilesUL += '<li>'+file.title+'</li>'
                        })
                    if(record.files.caracterisationFile)
                        record.files.caracterisationFile.forEach(file => {
                            htmlFilesUL += '<li>'+file.title+'</li>'
                        })
                    htmlFilesUL += '</ul>'
                    attCopy.find(att => att.OpportunityLineItem__c == record.Id)['TECH_AddedFiles__c'] = htmlFilesUL
                }
                attCopy.find(att => att.OpportunityLineItem__c == record.Id)['AnnualWasteRegulation__c'] = record.WasteRegulaitonFile
                delete record.AnnualWasteRegulation__c
                delete record.WasteRegulaitonFile
                delete record.WasteRegulaitonAnswer
                delete record.features
                delete record.files
                delete record.ExemptionReason
                delete record.ExemptionReasonAdditionalInfo
                delete record.Files
                delete record.isChanged
                delete record.DisplayAttachments
                delete record.DisplayAttachmentsTri
            })
            
            if(this.producer && this.notProducer == 'Oui') {
                this.producer.Address = this.producer.address.address
                delete this.producer.address
            }
            console.log('About to ValidateSign')
            const result = await validateSign({oppsJson: JSON.stringify(dataCopy), featuresJson: JSON.stringify(features), producerJson: JSON.stringify(this.producer), loadingAddress: this.loadingAddress.address, attestationsJson: JSON.stringify(attCopy)})
            this.wait = false
            this.error = !result
            
        } catch(e) {
            console.log(e)
        }
    }

    goHome(event) {
        window.location.href = 'https://www.groupe-seche.com/fr'
    }

    reload(event) {
        window.location.reload
    }

    displayToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'dismissible'
        });
        this.dispatchEvent(evt);
    }

    reportValidity(array) {
        try {
            var result = true;
            array.forEach(input => {
                result = result && input.reportValidity()
            })
            return result
        } catch(e) {
            console.log(e)
        }
    }

    navigation(incrementor, message, title) {
        try {
            var inputMissing = false
            var inputs = [...this.template.querySelectorAll('lightning-input')]
            var inputValidity = (inputs && inputs.length > 0) ? this.reportValidity(inputs) : true
            var combobox = [...this.template.querySelectorAll('lightning-combobox')]
            var comboboxValidity = (combobox && combobox.length > 0) ? this.reportValidity(combobox) : true
            var dataGridValidity = true
            if(this.template.querySelector('c-lwc_-data-grid')) {
                dataGridValidity = this.template.querySelector('c-lwc_-data-grid').reportValidity()
            }
            var lightTextareas = [...this.template.querySelectorAll('lightning-textarea')]
            var lightTextareasValidity = (lightTextareas && lightTextareas.length > 0) ? this.reportValidity(lightTextareas) : true
            var textareas = [...this.template.querySelectorAll('textarea')]
            var textareasValidity = (textareas && textareas.length > 0) ? this.reportValidity(textareas) : true
            var addresses = [...this.template.querySelectorAll('lightning-input-address')]
            var addresseValidity = (addresses && addresses.length > 0) ? this.reportValidity(addresses) : true
            
            inputMissing = (incrementor > 0) ? inputValidity && comboboxValidity && lightTextareasValidity && addresseValidity && dataGridValidity && textareasValidity : true
            
            if(inputMissing) {
                this.stepState['step'+parseInt(this.currentStep+1)] = null
                this.changeStep(this.currentStep, incrementor)
                this.stepState['step'+parseInt(this.currentStep+1)] = true
            } else {
                this.displayToast(title, message, 'warning')
            }

        } catch(e) {
            console.log(e)
        }
    }

    changeStep(stepIndex, incrementor) {
        //nextStep
        this.STEPS[stepIndex+incrementor].class = activeClass
        this.STEPS[stepIndex+incrementor].nameClass = activeClassStep
        this.STEPS[stepIndex+incrementor].titleClass = activeClassTitle
        this.STEPS[stepIndex+incrementor].done = false;

        //previous
        this.STEPS[stepIndex].class = doneClass
        this.STEPS[stepIndex].nameClass = doneClassStep
        this.STEPS[stepIndex].titleClass = doneClassTitle
        this.STEPS[stepIndex].done = (incrementor == 1) ? true : false;

        this.currentStep += incrementor
    }

    deleteFile(event) {
        const recordId = event.target.dataset.recordId
        const position = event.target.dataset.position
        const parentId = event.target.dataset.parentId
        deleteBySingleId({recordId: recordId})
        .then(result => {
            if(result) {
                this.dataGridRecords.find(record => record.files && record.files[position].find(file => file.Id == recordId)).files[position] = this.dataGridRecords.find(record => record.files && record.files[position].find(file => file.Id == recordId)).files[position].filter(file => file.Id != recordId)
                if(this.dataGridRecords.find(record => record.Id == parentId).files[position].length == 0) {
                    delete this.dataGridRecords.find(record => record.Id == parentId).files[position]
                }
            }
        })
    }

    download() {
        const resourcePath = PreFipCaracterisationTemplateFR;
        window.open(resourcePath, '_blank');
    }

}