import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import process from '@salesforce/apex/NewQuoteController.process';
import getSobjectType from '@salesforce/apex/NewQuoteController.getSobjectType';

export default class Lwc_CreateNewQuote extends LightningElement {

    @api recordId;

    @track init = false;
    @track success = null;
    @track wait = true;

    @track result = {
        dev_heads_up: ''
    };

    recordSobjectType;

    connectedCallback() {
        getSobjectType({recordId: this.recordId})
        .then(data => {
            if(data == 'Opportunity') {
                // automate creation
                this.createNewVersion();
            } else {
                this.init = true;
                this.wait = false;
            }
        })
    }

    copyData(event) {
        console.log(this.result.dev_heads_up)
        navigator.clipboard.writeText(this.result.dev_heads_up).then(function() {
            const event = new ShowToastEvent({
                title: 'Erreur copié dans le presse papier',
                variant: 'success',
                mode: 'dismissable'
            });
            this.dispatchEvent(event);
        }, function(err) {
            alert('Async: Could not copy text: ', err);
        });
    }

    createNewVersion(event) {
        this.init = false;
        this.wait = true;
        process({recordId: this.recordId})
        .then(data => {
            if(data) {
                this.result = JSON.parse(data);
                this.success = this.result.success;
                this.wait = false;
                if(this.success) {
                    window.location.href = this.result.recordUrl;
                }
            }
        });
    }

    cancel(event) {
        console.log('cancel')
        window.history.go(-1);
    }

}