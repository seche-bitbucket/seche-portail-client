import { LightningElement, api } from 'lwc';

export default class LwcQlyUserInfo extends LightningElement {
    @api userInfo;

    connectedCallback() {       
        console.log('LwcQlySummaryValidation.connectedCallback.userInfo', JSON.stringify(this.userInfo)); 
            }

    get haveCollectThisYear(){
        return (this.userInfo.haveCollectThisYear)
    }

    get collect() {
        return this.userInfo.firstCollect
    }
}