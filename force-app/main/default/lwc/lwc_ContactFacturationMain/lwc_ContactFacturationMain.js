import { LightningElement, api, track } from 'lwc'
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import { createRecord } from 'lightning/uiRecordApi'

import CASE_OBJECT from '@salesforce/schema/Case'

import CASE_RECORDTYPE_FIELD from '@salesforce/schema/Case.RecordTypeId'
import CASE_CONTACT_FIELD from '@salesforce/schema/Case.ContactId'
import CASE_SUBJECT_FIELD from '@salesforce/schema/Case.Subject'
import CASE_TYPE_FIELD from '@salesforce/schema/Case.Type'
import CASE_REASON_FIELD from '@salesforce/schema/Case.Reason'
import CASE_DESCRIPTION_FIELD from '@salesforce/schema/Case.Description'
import CASE_STATUS_FIELD from '@salesforce/schema/Case.Status'
import CASE_ORIGIN_FIELD from '@salesforce/schema/Case.Origin'
import CASE_DEFINITION_FIELD from '@salesforce/schema/Case.Definition__c'
import CASE_EMAIL_FIELD from '@salesforce/schema/Case.SuppliedEmail'
import CASE_OWNER_FIELD from '@salesforce/schema/Case.OwnerId'
import CASE_SALESMAN_FIELD from '@salesforce/schema/Case.Salesman__c'
import CASE_BILLING_SITE_CODE_FIELD from '@salesforce/schema/Case.BillingSiteCode__c'

import CONTACT_PHONE_FIELD from '@salesforce/schema/Contact.ERP_BillingPhone__c'
import CONTACT_PRIMARY_EMAIL_FIELD from '@salesforce/schema/Contact.PrimaryBillingContactEmail__c'
import CONTACT_SECONDARY_EMAIL_FIELD from '@salesforce/schema/Contact.SecondaryBillingContactEmail__c'

import getRecordByGUID from '@salesforce/apex/SecheSystemUtils.getRecordByGUID'
import getAutomationInputsSettings from '@salesforce/apex/ContactFacturationManager.getAutomationInputsSettings'
import getRecordTypeId from '@salesforce/apex/SystemUtils.getRecordTypeId'
import createCase from '@salesforce/apex/ContactFacturationManager.createCase'
import updateContact from '@salesforce/apex/ContactFacturationManager.updateContact'

const activeClass = 'slds-theme--inverse step-container slds-align_absolute-center slds-col slds-size_1-of-3 slds-grid slds-wrap slds-box cont'
const activeClassStep = 'step'
const activeClassTitle = 'step-title'

const doneClass = 'slds-theme--default step-container slds-align_absolute-center slds-col slds-size_1-of-3 slds-grid slds-wrap slds-box slds-text-color_inverse-weak step-text cont cont-done'
const doneClassStep = 'step done'
const doneClassTitle = 'step-title done'

export default class Lwc_ContactFacturationMain extends LightningElement {


    @api guid

    @track pageNotFound = false
    @track recordInfo
    @track currentStep = 0
    @track wait = true
    @track editDisabled = true
    @track newUserInfos
    @track stepBlocked
    @track newUserFacturationInfos
    @track badInput
    @track stepState = {
        step1: false,
        step2: false,
        step3: false,
    }
    @track STEPS = [
        {
            id: 'step1',
            name: 'Étape 1',
            title: 'Contrôler vos données',
            class: activeClass,
            nameClass: activeClassStep,
            titleClass: activeClassTitle,
            done: false
        },
        {
            id: 'step2',
            name: 'Étape 2',
            title: 'Vos contacts de facturation',
            class: doneClass,
            nameClass: doneClassStep,
            titleClass: doneClassTitle,
            done: false
        },
        {
            id: 'step3',
            name: 'Étape 3',
            title: 'Validation',
            class: doneClass,
            nameClass: doneClassStep,
            titleClass: doneClassTitle,
            done: false
        },
    ]

    contactFormFields = [
        {
            label: 'Rue',
            name: 'ERP_Street__c'
        },
        {
            label: 'Code postal',
            name: 'ERP_PostalCode__c'
        },
        {
            label: 'Ville',
            name: 'ERP_City__c'
        },
        {
            label: 'Pays',
            name: 'ERP_Country__c'
        },
        {
            label: 'TVA Intracommunautaire',
            name: 'ERP_VAT__c'
        },
        {
            label: 'SIRET',
            name: 'ERP_SIRET__c'
        },
    ]

    get displayPrevious() {
        return (this.currentStep > 0 && this.currentStep !== 2) ? true : false
    }

    get editRequired() {
        return !this.editDisabled
    }

    connectedCallback() {
        getRecordByGUID({GUID: this.guid, objectApiName: 'Contact', GUID_FIELD: 'GUID__c'})
        .then(data => {
            this.wait = true
            if(data) {
                this.recordInfo = data
                if (!this.recordInfo.ERP_AccountName__c )  this.recordInfo.ERP_AccountName__c = '';
                if (!this.recordInfo.ERP_CustomerCode__c )  this.recordInfo.ERP_CustomerCode__c = '';
                if (!this.recordInfo.ERP_Street__c )  this.recordInfo.ERP_Street__c = '';
                if (!this.recordInfo.ERP_PostalCode__c )  this.recordInfo.ERP_PostalCode__c = '';
                if (!this.recordInfo.ERP_City__c )  this.recordInfo.ERP_City__c = '';
                if (!this.recordInfo.ERP_Country__c )  this.recordInfo.ERP_Country__c = '';
                if (!this.recordInfo.ERP_VAT__c )  this.recordInfo.ERP_VAT__c = '';
                if (!this.recordInfo.ERP_SIRET__c )  this.recordInfo.ERP_SIRET__c = '';
                this.stepState.step1 = true
                this.wait = false
            } else {
                this.pageNotFound = true
            }
        })
    }

    navigation(incrementor, message, title) {
        try {
            var inputMissing = false
            var inputs = [...this.template.querySelectorAll('input')]
            var inputValidity = (inputs && inputs.length > 0) ? this.reportValidity(inputs) : true

            
            inputMissing = (incrementor > 0) ? inputValidity : true
            
            if(inputMissing) {
                this.stepState['step'+parseInt(this.currentStep+1)] = false
                this.changeStep(this.currentStep, incrementor)
                this.stepState['step'+parseInt(this.currentStep+1)] = true
                this.stepBlocked = false
            } else {
                this.displayToast(title, message, 'warning')
                this.stepBlocked = true
            }

        } catch(e) {
            console.log(e)
        }
    }

    reportValidity(array) {
        try {
            var result = true;
            this.badInput = null
            array.forEach(input => {
                if(!input.reportValidity() && !this.badInput) {
                    this.badInput = input.name
                }
                result = result && input.reportValidity()
            })
            return result
        } catch(e) {
            console.log(e)
        }
    }

    changeStep(stepIndex, incrementor) {
        //nextStep
        this.STEPS[stepIndex+incrementor].class = activeClass
        this.STEPS[stepIndex+incrementor].nameClass = activeClassStep
        this.STEPS[stepIndex+incrementor].titleClass = activeClassTitle
        this.STEPS[stepIndex+incrementor].done = false;

        //previous
        this.STEPS[stepIndex].class = doneClass
        this.STEPS[stepIndex].nameClass = doneClassStep
        this.STEPS[stepIndex].titleClass = doneClassTitle
        this.STEPS[stepIndex].done = (incrementor == 1) ? true : false;

        this.currentStep += incrementor
    }

    handlePrevious(event) {
        var title = this.globalTitle
        var message
        this.navigation(-1, message, title)
    }

    async handleNext(event) {
        try{
            var title = 'Informations incorrectes ou manquantes'
            var message = 'Des champs requis sont incorrects ou manquants'
            if(this.currentStep == 0) {
                this.navigation(1, message, title)
            } else if(this.currentStep == 1) {
                //this.wait = true
                
                this.navigation(1, message, title)
                if(!this.stepBlocked) {
                    if(!this.editDisabled && this.newUserInfos) {
                        this.createCase()
                    }
                    this.updateContact()
                }
            }
        } catch(e) {
            console.log(e)
        }
    }

    enableEdit(event) {
        this.editDisabled = !event.target.checked
    }

    displayToast(title, message, variant) {
        if(this.badInput == 'siret') {
            message += ' : le SIRET doit contenir 14 chiffres  (ex. : 12356894100056)'
        } else if(this.badInput == 'zip') {
            message += ' : le code postal doit contenir 5 chiffres (ex. : 75001)'
        } else if(this.badInput == 'email') {
            message += ' : veuillez entrer une adresse email valide'
        } else if(this.badInput == 'vat') {
            message += ' : veuillez entrer une TVA intracommunautaire valide (ex. : FR32123456789)'
        }
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'dismissible'
        });
        this.dispatchEvent(evt);
    }

    handleChange(event) {
        const eventName = event.target.name
        if(this.currentStep == 0) {
            let infos = {
                street: this.template.querySelector('.streetInput').value,
                zip: this.template.querySelector('.zipInput').value,
                city: this.template.querySelector('.cityInput').value,
                country: this.template.querySelector('.countryInput').value,
                vat: this.template.querySelector('.vatInput').value,
                siret: this.template.querySelector('.siretInput').value,
            }

    
            this.newUserInfos = infos
        } else if(this.currentStep == 1) {
            let infosFacturation = {
                phone: this.template.querySelector('.phoneInput').value,
                emailPrimary: this.template.querySelector('.emailPrimaryInput').value,
                emailSecondary: this.template.querySelector('.emailSecondaryInput').value,
            }


            this.newUserFacturationInfos = infosFacturation
        }
    }

    createCase() {
        getRecordTypeId({objectApiName: 'Case', recordTypeName: 'Requête standard'})
        .then(data => {
            getAutomationInputsSettings()
            .then(data2 => {
                const fields = {}

                const descriptionInfos = 'Adresse :\n' + this.newUserInfos.street + '\n' + this.newUserInfos.zip + '\n' + this.newUserInfos.city + '\n' + this.newUserInfos.country + '\nTVA Intracommunautaire :\n' + this.newUserInfos.vat + '\nSIRET :\n' + this.newUserInfos.siret
    
                fields[CASE_RECORDTYPE_FIELD.fieldApiName] = data
                fields[CASE_STATUS_FIELD.fieldApiName] = 'Nouvelle'
                fields[CASE_ORIGIN_FIELD.fieldApiName] = 'Internet'
                fields[CASE_DEFINITION_FIELD.fieldApiName] = 'Evolution'
                fields[CASE_CONTACT_FIELD.fieldApiName] = this.recordInfo.Id
                fields[CASE_SUBJECT_FIELD.fieldApiName] = 'Demande de modification des données de facturation'
                fields[CASE_REASON_FIELD.fieldApiName] = 'Facturation'
                fields[CASE_DESCRIPTION_FIELD.fieldApiName] = descriptionInfos
                fields[CASE_EMAIL_FIELD.fieldApiName] = this.recordInfo.email
                fields[CASE_OWNER_FIELD.fieldApiName] = data2.BillingDataUpdateCaseOwner__c
                fields[CASE_SALESMAN_FIELD.fieldApiName] = data2.BillingDataUpdateCaseOwner__c
                fields[CASE_BILLING_SITE_CODE_FIELD.fieldApiName] = this.recordInfo.BillingSiteCode__c
    
                const caseRecord = {apiName : 'Case', fields: fields}
                createCase({fields: fields})
                .then(result => {
                })
                .catch(error => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error creating record',
                            message: error.body.message,
                            variant: 'error',
                        }),
                    );
                })
            })
        })
    }

    updateContact() {
        const fields = {}
        fields[CONTACT_PHONE_FIELD.fieldApiName] = this.newUserFacturationInfos.phone
        fields[CONTACT_PRIMARY_EMAIL_FIELD.fieldApiName] = this.newUserFacturationInfos.emailPrimary
        fields[CONTACT_SECONDARY_EMAIL_FIELD.fieldApiName] = this.newUserFacturationInfos.emailSecondary
        updateContact({contactId: this.recordInfo.Id, fields: fields})
        .then(result => {
            if(result) {
                this.wait = false
            }
        })
        .catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error updating record',
                    message: error.body.message,
                    variant: 'error',
                })
            )
        })
    }
}