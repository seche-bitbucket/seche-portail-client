import { LightningElement, api, track } from 'lwc';

export default class LwcQlyTrackDechet extends LightningElement {
    @api userInfo;

    @track isDisabled = false;
    @track userTD = {}

    @track pin

    get pin_pos0() {
        return (this.pin[0] == ' ') ? null : this.pin[0]
    }

    get pin_pos1() {
        return (this.pin[1] == ' ') ? null : this.pin[1]
    }

    get pin_pos2() {
        return (this.pin[2] == ' ') ? null : this.pin[2]
    }

    get pin_pos3() {
        return (this.pin[3] == ' ') ? null : this.pin[3]
    }

    connectedCallback() {   
        console.log('LwcQlyTrackDechet.connectedCallback.userInfo', JSON.stringify(this.userInfo)); 
        this.userTD.HasWasteTrackAccess__c = this.userInfo.HasWasteTrackAccess__c
        this.userTD.IsUserWasteTrackUser__c = this.userInfo.IsUserWasteTrackUser__c
        this.userTD.WasteTrackFirstName__c = this.userInfo.WasteTrackFirstName__c
        this.userTD.WasteTrackSIRET__c = this.userInfo.WasteTrackSIRET__c
        this.userTD.WasteTrackLastName__c = this.userInfo.WasteTrackLastName__c
        this.userTD.WasteTrackEmail__c = this.userInfo.WasteTrackEmail__c
        this.pin = (this.userInfo.WasteTrackSignCode__c) ? this.userInfo.WasteTrackSignCode__c : '    '
        this.start = true;      
    }  

    get trackDechetOptions() {
        return [
            { label: 'Oui', value: 'Oui' },
            { label: 'Non', value: 'Non' },
        ]
    }

    get showTrackDechetInfos() {
        return (this.userTD.HasWasteTrackAccess__c == 'Oui') ? true : false
    }

    get disableUserTD() {
        return (this.userTD.HasWasteTrackAccess__c == 'Oui') ? false : true
    }

    get showTrackDechetMoreInfos() {
        return (this.userTD.IsUserWasteTrackUser__c) ? true : false
    }

    handleChangeTrackDechet(event) {
        this.userTD.HasWasteTrackAccess__c = event.target.value

        if(this.userTD.HasWasteTrackAccess__c == 'Non') {
            this.userTD.IsUserWasteTrackUser__c = 'Non'
            delete this.userTD.WasteTrackEmail__c
            delete this.userTD.WasteTrackLastName__c
            delete this.userTD.WasteTrackFirstName__c
            delete this.userTD.WasteTrackSIRET__c
        }

        const field = 'HasWasteTrackAccess__c';
        const inputValue = this.userTD.HasWasteTrackAccess__c;
        const source = 'TrackDechet'
        let userInputValue = { field: field, inputValue: inputValue, source: source };
        const userInputEvent = new CustomEvent(
            "userinputvalue", { detail: userInputValue }
        );
        this.dispatchEvent(userInputEvent); 
    }

    handleChangeTrackDechetUser(event) {
        this.userTD.IsUserWasteTrackUser__c = event.target.value

        if(this.userTD.IsUserWasteTrackUser__c == 'Oui') {
            this.userTD.WasteTrackEmail__c = this.userInfo.email;
            this.userTD.WasteTrackLastName__c = this.userInfo.lastName;
            this.userTD.WasteTrackFirstName__c = this.userInfo.firstName;
            this.userTD.WasteTrackSIRET__c = this.userInfo.siret;
            const source2 = 'TrackDechet PreFill'
            const userInputEvent2 = new CustomEvent(
                "userinputvalue", { detail: {infos: this.userTD, source: source2 }}
            );
            this.dispatchEvent(userInputEvent2);
        } else {
            delete this.userTD.WasteTrackEmail__c
            delete this.userTD.WasteTrackLastName__c
            delete this.userTD.WasteTrackFirstName__c
            delete this.userTD.WasteTrackSIRET__c
        }

        const field = 'IsUserWasteTrackUser__c';
        const inputValue = this.userTD.IsUserWasteTrackUser__c;
        const source = 'TrackDechet'
        let userInputValue = { field: field, inputValue: inputValue, source: source };
        const userInputEvent = new CustomEvent(
            "userinputvalue", { detail: userInputValue }
        );
        this.dispatchEvent(userInputEvent); 
    }

    handleUserInputChangeTD(event) {
        console.log('LwcQlySummaryValidation.handleUserInputChangeTD start ---', event.target.name);
        console.log('LwcQlySummaryValidation.handleUserInputChangeTD.userInputValue ---', event.target.value);
        const field = event.target.name;
        const inputValue = event.target.value;
        const source = 'TrackDechet'
        let userInputValue = { field: field, inputValue: inputValue, source: source };
        const userInputEvent = new CustomEvent(
            "userinputvalue", { detail: userInputValue }
        );
        this.dispatchEvent(userInputEvent);  
        this.isDisabled = false;  
    }

    handlePinCodeChange(event) {
        try{
            const {target} = event
            if (!target.value.length) { return target.value = null; }
    
            const inputLength = target.value.length;
            let currentIndex = Number(target.dataset.position);
            
            if (inputLength > 1) {
                const inputValues = target.value.split('');
                
                inputValues.forEach((value, valueIndex) => {
                    const nextValueIndex = currentIndex + valueIndex;
                    
                    if (nextValueIndex >= 4) { return; }
                    
                    this.template.querySelector('lightning-input[data-position="'+nextValueIndex+'"]').value = value;

                    let pinSplitted = this.pin.split('')
                    pinSplitted[nextValueIndex] = value
                    console.log('handlePinCode => pinSplitted', pinSplitted)
                    this.pin = pinSplitted.join('')
                    console.log('handlePinCode => this.pin', this.pin)

                });
                
                currentIndex += inputValues.length - 2;
            } else {
                let pinSplitted = this.pin.split('')
                pinSplitted[currentIndex] = target.value
                console.log('handlePinCode => pinSplitted', pinSplitted)
                this.pin = pinSplitted.join('')
                console.log('handlePinCode => this.pin', this.pin)
            }

            const field = 'WasteTrackSignCode__c';
            const inputValue = this.pin;
            const source = 'TrackDechet'
            let userInputValue = { field: field, inputValue: inputValue, source: source };
            const userInputEvent = new CustomEvent(
                "userinputvalue", { detail: userInputValue }
            );
            this.dispatchEvent(userInputEvent); 
            
            const nextIndex = currentIndex + 1;
            
            if(nextIndex < 4) {
                this.template.querySelector('lightning-input[data-position="'+nextIndex+'"]').focus();
            }
        } catch(e) {
            console.log('handlePinCode', e)
        }
    }

    handlePinCode(event) {

        try {

            const {code, target} = event
            console.log('handlePinCode => code', JSON.stringify(code))
            
            const position = Number(event.target.dataset.position)
            const value = String(event.target.value)

            const previousIndex = position - 1
            const nextIndex = position + 1

            const hasPreviousIndex = previousIndex >= 0
            const hasNextIndex = nextIndex <= 3

            switch(code) {
                case 'ArrowLeft':
                case 'ArrowUp':
                    if (hasPreviousIndex) {
                        this.template.querySelector('lightning-input[data-position="'+previousIndex+'"]').focus();
                    }
                    event.preventDefault();
                    break;
                
                case 'ArrowRight':
                case 'ArrowDown':
                    if (hasNextIndex) {
                        this.template.querySelector('lightning-input[data-position="'+nextIndex+'"]').focus();
                    }
                    event.preventDefault();
                    break;

                case 'Backspace':
                if (!event.target.value.length && hasPreviousIndex) {
                    this.template.querySelector('lightning-input[data-position="'+previousIndex+'"]').value = null;

                    let pinSplitted = this.pin.split('')
                    pinSplitted[previousIndex] = ' '
                    this.pin = pinSplitted.join('')

                    const field = 'WasteTrackSignCode__c';
                    const inputValue = this.pin;
                    const source = 'TrackDechet'
                    let userInputValue = { field: field, inputValue: inputValue, source: source };
                    const userInputEvent = new CustomEvent(
                        "userinputvalue", { detail: userInputValue }
                    );
                    this.dispatchEvent(userInputEvent); 

                    this.template.querySelector('lightning-input[data-position="'+previousIndex+'"]').focus();
                }
                break;

                default:
                break;
            }
        } catch(e) {
            console.log('handlePinCode', e)
        }

    }

    reportValidity(array) {
        try {
            var result = true;
            array.forEach(input => {
                result = result && input.reportValidity()
            })
            return result
        } catch(e) {
            console.log(e)
        }
    }

    @api validateInputs(event) {
        try{
            let inputsLst = [...this.template.querySelectorAll('lightning-input')]
            let comboboxLst = [...this.template.querySelectorAll('lightning-combobox')]

            let cbsReportResult = (comboboxLst && comboboxLst.length > 0) ? this.reportValidity(comboboxLst) : true 
            let inputsReportResult = (inputsLst && inputsLst.length > 0) ? this.reportValidity(inputsLst) : true 

            return cbsReportResult && inputsReportResult
        } catch(e) {
            console.log(e)
        }
    }
}