import { LightningElement } from 'lwc';
import getAllAvailableLanguages from '@salesforce/apex/CAP_Global_SF_SysTools.getAllAvailableLanguages';

import imgs from '@salesforce/resourceUrl/Languages'

export default class Lwc_LanguageSwitch extends LightningElement {

    languages

    async connectedCallback() {
        this.france_flag_img = imgs + '/FR.png'
        this.england_flag_img = imgs + '/EN.png'

        this.languages = JSON.parse(JSON.stringify(await getAllAvailableLanguages()))
        this.languages.forEach(language => {
            language.Icon = imgs + '/Languages/' + language.DeveloperName + '.png'
        })
    }

    changeLang(event) {
        try {
            if(window.location.search.includes('language')) {
                window.location.search = window.location.search.replace(/language=[^&$]*/i, 'language='+event.target.dataset.lang);
            }
            window.location.search.concat('&language='+event.target.dataset.lang)
        } catch(e) {
            console.log(e)
        }
    }

}