import { LightningElement, api, wire, track } from 'lwc';
import getPickListValuesIntoList from '@salesforce/apex/LWC_CustomBannerCtrl.getPickListValuesIntoList';
import getRecord from '@salesforce/apex/LWC_CustomBannerCtrl.getRecord';

export default class Lwc_CustomPath extends LightningElement {

    @api recordId;
    @api pathField;
    @api elementColor;

    @track currentStep;
    @track currentStatus;
    @track status = [];

    connectedCallback() {

        getRecord({o: this.recordId, recordId: this.recordId})
        .then(data => {
            if(data) {
                this.currentStatus = data[0][this.pathField];
            }
        });

        getPickListValuesIntoList({o: this.recordId, obj: this.recordId, field: this.pathField})
            .then(data => {
                if(this.status.length == 0) {
                    data.forEach(d => {
                        this.status.push({label: d.label, index: data.findIndex(e => e.label == d.label)+1, class: 'slds-path__item slds-is-incomplete'});
                    })
                }
            })
            .then (() => {
                if(this.status.length > 1) {
                    
                    var label = this.status[this.status.findIndex(e => e.label == this.currentStatus)].label;

                    var elmtColorArr = undefined;
                    if(this.elementColor != undefined) {
                        elmtColorArr = this.elementColor.split(';');
                    }

                    if(elmtColorArr != undefined && elmtColorArr.find(e => e.includes(label)) != undefined) {
                        var color = elmtColorArr.find(e => e.includes(label)).split('_')[1];
                        this.template.host.style.setProperty( '--background-color', color );
                        this.status[this.status.findIndex(e => e.label == this.currentStatus)].class = 'slds-path__item slds-is-incomplete slds-is-active custom';
                    } else {
                        this.status[this.status.findIndex(e => e.label == this.currentStatus)].class = 'slds-path__item slds-is-incomplete slds-is-active';
                    }

                    let notInSteps = this.status.filter(e => e.label != this.currentStatus);
                    notInSteps.forEach(e => {
                        this.template.querySelector('[data-index="'+e.index+'"]').className = "slds-path__item slds-is-incomplete";
                    })

                }
            })

    }

}