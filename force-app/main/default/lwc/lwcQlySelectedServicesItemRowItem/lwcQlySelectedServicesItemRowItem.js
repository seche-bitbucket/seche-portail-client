import { LightningElement, api, wire, track } from 'lwc';
import { getPicklistValues, getObjectInfo } from 'lightning/uiObjectInfoApi';
import QLY_SERVICE_OBJECT from '@salesforce/schema/QLY_Service__c';
import PACKAGING_FIELD from '@salesforce/schema/QLY_Service__c.Packaging__c';
import CLASSIFICATION_FIELD from '@salesforce/schema/QLY_Service__c.PeroxideClassification__c';
import { showToast, getCurrentYear } from 'c/ldsUtils';
import piklistValues from '@salesforce/apex/AP_QLyManager.getPiklistValues';
import uploadFile from '@salesforce/apex/AP_QLyManager.uploadFile';
import uploadFileWithRecordId from '@salesforce/apex/AP_QLyManager.uploadFileWithRecordId';
import getDocFromVersion from '@salesforce/apex/AP_QLyManager.getDocFromVersion';
import getAvailablePackaging from '@salesforce/apex/AP_QLyManager.getAvailablePackaging';


export default class LwcQlySelectedServicesItemRowItem extends LightningElement {
    
    @api row;
    @api product;
    @api prodId;
    @api index;
    @api displayStyle;
    @api itemDisabled;
    @api formId;

    @track modal;

    firstOpt;
    nbFiles;
    classifications;

    DEFAULT_DISPLAY = 'Normal';
    PHOTOS_QUANTITY = 'Quantite_Photos';
    PHOTOS_QUANTITY_LIST = 'Quantite_Liste_Photos';
    PEROXYDE_CLASSIFICATION_QUANTITY_PHOTOS = 'Peroxyde_Classification_Quantite_Photos';
    ERROR_FILENAME;
    isSuccess = true;

    opts= [];

    // LWC lifecycle hooks
    connectedCallback() {

        console.log('LwcQlySelectedServicesItemRowItem.connectedCallback.product', JSON.stringify(this.product));
        console.log('LwcQlySelectedServicesItemRowItem.connectedCallback.prodId', JSON.stringify(this.prodId));
        console.log('LwcQlySelectedServicesItemRowItem.connectedCallback.index', JSON.stringify(this.index));
        console.log('LwcQlySelectedServicesItemRowItem.connectedCallback.displayStyle', JSON.stringify(this.displayStyle));
        console.log('LwcQlySelectedServicesItemRowItem.connectedCallback.row', JSON.stringify(this.row));
        console.log('LwcQlySelectedServicesItemRowItem.connectedCallback.row', JSON.stringify(this.row.containerPkg));
        this.opts = this.row.containerPkg;
        
    }

    renderedCallback() {  

        let last = this.template.querySelector('.last-added');

        if(last) {

            last.scrollIntoView({
                behavior: 'smooth',
                block: 'center'
            });

            last.removeAttribute('class');
        }

    } 

    @wire(getObjectInfo, { objectApiName: QLY_SERVICE_OBJECT })
    objectMetadata;    
    
  
    // why we don't use the lwc api to get pickListvalues, because the custom community user don't have access to the QLY_Service__c so we enforced the access
    @wire(piklistValues, {objApiName: 'QLY_Service__c', fieldName: 'PeroxideClassification__c' })
    classificationValues({ error, data }) {       
        if (data) {      
            this.classifications = data;
        } else if (error) {
            // Handle error
            throw new Error(error);            
        }
    }

    get defaultDisplayStyle() {
        return (this.displayStyle === this.DEFAULT_DISPLAY);
    }

    get photoQtyDisplayStyle() {
        return (this.displayStyle === this.PHOTOS_QUANTITY);
    }

    get photoQtyListDisplayStyle() {
        return (this.displayStyle === this.PHOTOS_QUANTITY_LIST);
    }

    get peroxydeClassDisplayStyle() {
        return (this.displayStyle === this.PEROXYDE_CLASSIFICATION_QUANTITY_PHOTOS);
    }

    get productNameStyle() {
        return 'slds-m-around_x-small';
    }

    get productName() {
        if (this.index === 0) {
            return this.product;
        } else {
            return ' ';
        }
    }
  

    get displayAddButton() {
        return (this.index === 0 && !this.itemDisabled );
    }

    get displayRemoveButton() {
       return (this.index !== 0 && !this.itemDisabled);
    }

    get displayText() {
        return (this.index == 0);
     }

    get  rowStyle() {
        if (this.row.index % 2 == 0) {
            return 'row-even-index slds-grid slds-grid--align-space ';
        } else {
            return 'row-odd-index slds-grid slds-grid--align-space ';
        }
    }

    get acceptedFormats() {
        return ['.pdf', '.png','.jpg','.jpeg', '.xls', '.xlsx', '.docx', '.doc'];
    }  

    showContent(reader) {       
       return reader.result;
    }

    handleUploadFinished(event) {

        console.log('LwcQlySelectedServicesItemRowItem.handleUpload event:' + JSON.stringify(event));

        let strFileNames = '';
        // Get the list of uploaded files
        const uploadedFiles = event.detail.files;

        for(let i = 0; i < uploadedFiles.length; i++) {
            strFileNames += uploadedFiles[i].name + ', ';
        }

        uploadedFiles.forEach(function(e) {
            e.Id = e.documentId;
            delete e.documentId;
        });

        console.log('LwcQlyFinalization.handleUploadFinished uploadedFiles: '+JSON.stringify(uploadedFiles));

        console.log('LwcQlyFinalization.handleUploadFinished event: '+JSON.stringify(event.currentTarget));

        let processAction = { pendingFilesIds : uploadedFiles };
        const uploadfiles = new CustomEvent(
            "uploadfiles", { detail: processAction }
        );
        this.dispatchEvent(uploadfiles);

        this.dispatchEvent(showToast('success', undefined, undefined, strFileNames + ' fichier chargé !'));       
      
    }
    

    handleAddRow(event) {

        console.log('LwcQlySelectedServicesItemRowItem.handleAddRow.event.target.name start ---', event.target.name);
        console.log('LwcQlySelectedServicesItemRowItem.handleAddRow.event.target.index start ---', event.currentTarget.dataset.currentIndex);
        console.log(event.currentTarget.dataset.valuePackaging);
        console.log(event.currentTarget.dataset.options);
        
        try{
            const opt = event.currentTarget.dataset.options;
            opt.forEach(op => {
                console.log(JSON.stringify(op));
            });
        }catch(e) {
            console.log(e);
        }
        const action = 'add';
        const field = event.target.name;
        const currentIndex = event.currentTarget.dataset.currentIndex;

        let removeAction = { name: field, action: action, currentIndex: currentIndex, packaging: event.currentTarget.dataset.valuePackaging, options: this.opts};
        const rowaction = new CustomEvent(
            "rowaction", { detail: removeAction }
        );
        this.dispatchEvent(rowaction);

    }

    handleRemoveRow(event) {

        console.log('LwcQlySelectedServicesItemRowItem.handleRemoveRow. event.target.name start ---', event.target.name);
        console.log('LwcQlySelectedServicesItemRowItem.handleRemoveRow.event.target.currentIndex start ---', event.currentTarget.dataset.currentIndex);

        const currentIndex = event.currentTarget.dataset.currentIndex;
        const action = 'remove';
        const field = event.target.name;       

        let removeAction = { name: field, action: action , currentIndex: currentIndex };
        const rowaction = new CustomEvent(
            "rowaction", { detail: removeAction }
        );
        this.dispatchEvent(rowaction);
    }

    handleUserInputChange(event) {
        console.log('LwcQlySelectedServicesItemRowItem.handleUserInputChange. event.target.name; start ---',  event.target.name);
        console.log('LwcQlySelectedServicesItemRowItem.handleUserInputChange. event.target.value start ---',event.target.value);
        console.log('LwcQlySelectedServicesItemRowItem.handleUserInputChange.displayStyle start ---', this.displayStyle);
        console.log('LwcQlySelectedServicesItemRowItem.handleUserInputChange.currentIndex start ---', event.currentTarget.dataset.currentIndex);

        const productName = this.product;
        const field = event.target.name;       
        const displayStyle = this.displayStyle;
        const currentIndex = event.currentTarget.dataset.currentIndex;
        //to notify where the event fire from
        const eventOrigin = 'fromSelectedService';       
        
       if (field.includes('CheckBox')) {//all checkbox name must include the word 'checkbox'                    
            const inputValue = event.target.checked;
            let userInputValue = { field: field, inputValue: inputValue, displayStyle : displayStyle, currentIndex : currentIndex, productName :productName, eventOrigin : eventOrigin};
            const userInputEvent = new CustomEvent(
                "userinputvalue", { detail: userInputValue }
            );
            this.dispatchEvent(userInputEvent);
           
        } else {
            const inputValue = event.target.value;
            let userInputValue = { field: field, inputValue: inputValue, displayStyle : displayStyle, currentIndex : currentIndex, productName : productName, eventOrigin : eventOrigin};
            const userInputEvent = new CustomEvent(
                "userinputvalue", { detail: userInputValue }
            );
            this.dispatchEvent(userInputEvent);
            
        }

    }

    // Uploading alternative
    files = [];
    handleUpload2(event) {
        console.log('LwcQlySelectedServicesItemRowItem.handleUpload2 event: '+event.target.dataset.id);  
        console.log('LwcQlySelectedServicesItemRowItem.handleUpload2 event.target.files.length: '+Object.values(event.target.files));
        this.modal = true;
        this.nbFiles = event.target.files.length;
        for (let i = 0; i < event.target.files.length; i++) {
            console.log('LwcQlySelectedServicesItemRowItem.handleUpload2 event.target.files: '+event.target.files[i].name); 
            const file = event.target.files[i];
            let fileName = file.name;
            let lastIndex = fileName.lastIndexOf('.');
            console.log('lastindex: '+lastIndex);

            let fileStr = fileName.substr(0, lastIndex);
            console.log('length: '+fileStr.length);
            if(fileName.length > 56)
            {
                this.modal = false;
                this.nbFiles = 0;
                this.dispatchEvent(showToast('warning', 'sticky', 'Vérifiez le nom de vos fichiers', 'La taille du titre des fichiers doit être inférieure à 75 caractères.'));
                break;
            }

            let reader = new FileReader();
            let prodId = event.target.dataset.id;
            let fileData;
            reader.onload = () => {
                var base64 = reader.result.split(',')[1];
                fileData = {
                    'filename': fileName,
                    'base64': base64,
                    'recordId': this.formId
                }
                console.log(fileData);
                console.log(fileData.recordId);
                console.log('base64: '+fileData.base64);

                if(fileData.base64){
                    this.isSuccess = true;
                    if(!fileData.recordId){

                        uploadFile({base64: fileData.base64, filename: fileData.filename, productId: prodId})
                        .then((result) => {
                            console.log('LwcQlySelectedServicesItemRowItem.handleUpload2.uploadFile result: '+JSON.stringify(result));
                            this.uploadPostProcess(result);
                        });

                    } else {
                        
                        uploadFileWithRecordId({base64: fileData.base64, filename: fileData.filename, recordId: fileData.recordId, productId: prodId})
                        .then((result) => {
                            console.log('LwcQlySelectedServicesItemRowItem.handleUpload2.uploadFileWithRecordId result: '+JSON.stringify(result)); 
                            this.uploadPostProcess(result);

                        })
                        .catch((err) => {
                            console.log('LwcQlySelectedServicesItemRowItem.handleUpload2.uploadFileWithRecordId result: '+err); 
                        });
                    }
                } else {
                    this.isSuccess = false;
                    if(!this.ERROR_FILENAME) { this.ERROR_FILENAME = fileData.filename; }
                    else { this.ERROR_FILENAME = this.ERROR_FILENAME +', '+ fileData.filename; }
                    this.nbFiles -= 1;
                    if(this.nbFiles === 0)
                    {
                        this._callback();
                    }
                }
                //console.log('LwcQlySelectedServicesItemRowItem.handleUpload2 this.files: '+this.files); 
            }
            reader.readAsDataURL(file);
        }

    }

    _callback(){
        console.log('LwcQlySelectedServicesItemRowItem._callback. start --'); 
        this.modal = false;
        if(this.isSuccess) {
            this.dispatchEvent(showToast('success', undefined, undefined,'Vos fichiers ont été chargés avec succès !'));
        } else {
            this.dispatchEvent(showToast('warning', 'sticky', undefined, 'Le ou les fichiers suivants n\'ont pas pu être chargés : '+this.ERROR_FILENAME));
            this.ERROR_FILENAME = '';
        }  
    }

    uploadPostProcess(result){
        let docId;
        getDocFromVersion({versionId: result.Id})
        .then((document) => {
            console.log('LwcQlySelectedServicesItemRowItem.uploadPostProcess document: '+JSON.stringify(document));
            docId = document.Id;
            let processAction = { pendingFilesIds : [{name: result.Title, Id: docId, productId: result.QLY_Product2__c}] };
            const uploadfiles = new CustomEvent("uploadfiles", { detail: processAction });
            this.dispatchEvent(uploadfiles);
            this.nbFiles -= 1;
            if(this.nbFiles === 0)
            {
                this._callback();
            }
        });
    }

}