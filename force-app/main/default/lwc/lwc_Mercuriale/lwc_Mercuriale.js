import { LightningElement, api, wire, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getObjectInfo, getPicklistValues } from 'lightning/uiObjectInfoApi';
import { createRecord, updateRecord } from 'lightning/uiRecordApi';

import { refreshApex } from '@salesforce/apex';
import { CloseActionScreenEvent } from 'lightning/actions';

import DETAIL_OBJECT from '@salesforce/schema/IndexValue__c';
import MASTER_ID_FIELD from '@salesforce/schema/IndexValue__c.Indices__c';
import DETAIL_SOURCE_FIELD from '@salesforce/schema/IndexValue__c.Source__c';
import DETAIL_VALUE_FIELD from '@salesforce/schema/IndexValue__c.Value__c';
import DETAIL_DATE_FIELD from '@salesforce/schema/IndexValue__c.Date__c';
import DETAIL_FAMILY_FIELD from '@salesforce/schema/IndexValue__c.ProductFamily__c';
import DETAIL_SUBFAMILY_FIELD from '@salesforce/schema/IndexValue__c.ProductSubFamily__c';

import getMercuriale from '@salesforce/apex/MercurialeController.getMercuriale';
import getSFDomain from '@salesforce/apex/MercurialeController.getSFDomain';

const DELAY = 200;
const DATE = new Date();
const THIS_MONTH = (DATE.getMonth() + 1).toString();
const MASTER_ID_API_NAME = MASTER_ID_FIELD.fieldApiName;
const DETAIL_SOURCE_API_NAME = DETAIL_SOURCE_FIELD.fieldApiName;
const DETAIL_VALUE_API_NAME = DETAIL_VALUE_FIELD.fieldApiName;
const DETAIL_DATE_API_NAME = DETAIL_DATE_FIELD.fieldApiName;
const DETAIL_FAMILY_API_NAME = DETAIL_FAMILY_FIELD.fieldApiName;
const DETAIL_SUBFAMILY_API_NAME = DETAIL_SUBFAMILY_FIELD.fieldApiName;

// Formalize the error message
function getErrorMessage(error) {
    const fieldErrors = error.body.output.fieldErrors;
    if (fieldErrors) {
        let message = '';
        const fieldKey = Object.keys(fieldErrors);
        for (const msg of fieldErrors[fieldKey]) {
            if (msg.message) {
                message += msg.message + '\n';
            }
        }
        return message;
    } else if (Array.isArray(error.body)) {
        return error.body.map(e => e.message).join(', ');
    } else if (typeof error.body.message === 'string') {
        return error.body.message;
    } else {
        return 'Unknown error';
    }
}

export default class Lwc_Mercuriale extends LightningElement {
    delayTimeout;
    error;
    sources;
    hasMasterRecord;
    hasDetailRecord;
    selectedSourceLabel;
    selectedSourceValue = '';
    defaultRecordTypeId = '012000000000000AAA';
    draftValues = new Map();
    dataMap = new Map();
    @track SFdomain;

    selectedMonth = THIS_MONTH.length == 1 ? '0' + THIS_MONTH : THIS_MONTH;
    get selectedDate() {
        return new Date(Date.UTC(DATE.getFullYear(), parseInt(this.selectedMonth) - 1));
    }

    get months() {
        return [
            { value: '01', label: 'Janvier ' + DATE.getFullYear() },
            { value: '02', label: 'Février ' + DATE.getFullYear() },
            { value: '03', label: 'Mars ' + DATE.getFullYear() },
            { value: '04', label: 'Avril ' + DATE.getFullYear() },
            { value: '05', label: 'Mai ' + DATE.getFullYear() },
            { value: '06', label: 'Juin ' + DATE.getFullYear() },
            { value: '07', label: 'Juillet ' + DATE.getFullYear() },
            { value: '08', label: 'Août ' + DATE.getFullYear() },
            { value: '09', label: 'Septembre ' + DATE.getFullYear() },
            { value: '10', label: 'Octobre ' + DATE.getFullYear() },
            { value: '11', label: 'Novembre ' + DATE.getFullYear() },
            { value: '12', label: 'Décembre ' + DATE.getFullYear() }
        ];
    }

    connectedCallback() {
        /* this.SFdomain = window.location.hostname;
        console.log('Domaine Salesforce : ' + this.SFdomain); */
        getSFDomain()
        .then(result => {
            this.SFdomain = result
        })
        console.log('Domaine Salesforce : ' + this.SFdomain);

    } 

    @wire(getObjectInfo, { objectApiName: DETAIL_OBJECT })
    wireDetailObjectInfo({ data, error }) {
        if (data) {
            this.defaultRecordTypeId = data.defaultRecordTypeId;
            this.error = undefined;
        } else if (error) {
            console.error('error: ', error);
            this.dispatchEvent(
                new ShowToastEvent({
                    title: "Error: Something wrong with IndexValue__c Object",
                    message: getErrorMessage(error),
                    variant: "error",
                }),
            )
        }
    }

    @wire(getPicklistValues, { recordTypeId: '$defaultRecordTypeId', fieldApiName: DETAIL_SOURCE_FIELD })
    wireSource({ data, error }) {
        if (data) {
            this.sources = [{ attributes: null, label: '--None--', validFor: [], value: '' }];
            this.sources.push(...data.values);
            this.error = undefined;
            console.log(this.sources);
        } else if (error) {
            this.data = undefined;
            console.error('error: ', error);
            this.dispatchEvent(
                new ShowToastEvent({
                    title: "Error: Something wrong with Source PicklistValues",
                    message: getErrorMessage(error),
                    variant: "error",
                }),
            )
        }
    }

    @wire(getMercuriale, { selectedSource: '$selectedSourceValue', selectedDate: '$selectedDate' }) getMercuriale;
    /* @wire(getSFDomain) 
    getSFDomain({ data, error}) {
        if (data) {
            console.log("data : " )
            this.SFdomain = data;
        }
    }  */

    get getMercurialeData() {
        getSFDomain;
        const dataList = [];
        const { data, error } = this.getMercuriale;
        if (data) {
            this.dataMap = new Map(Object.entries(data));
            this.hasMasterRecord = this.dataMap.size>=1 ? true : false;
            this.dataMap.forEach((row, key) => {
                dataList.push({
                    masterId: row[MASTER_ID_API_NAME],
                    value: row[DETAIL_VALUE_API_NAME],
                    source: row[DETAIL_SOURCE_API_NAME],
                    productName: row['Tech_ProductName__c'],
                    family: row[DETAIL_FAMILY_API_NAME],
                    subFamily: row[DETAIL_SUBFAMILY_API_NAME],
                    //url: this.SFdomain + row[MASTER_ID_API_NAME]
                    url: this.SFdomain + row[MASTER_ID_API_NAME]
                    

                });
                console.log("url" + this.SFdomain + "/" + row[MASTER_ID_API_NAME])
                if (row[DETAIL_VALUE_API_NAME]) {
                    this.hasDetailRecord = true;
                }
            });
            return dataList
        } else if (error) {
            console.error('error: ', error);
            this.dispatchEvent(
                new ShowToastEvent({
                    title: "Error: Something wrong with IndexValue__c data",
                    message: getErrorMessage(error),
                    variant: "error",
                }),
            )
        }
    }

    handleChangeMonth(event) {
        this.getMercuriale = Object();
        this.draftValues = new Map();
        this.hasDetailRecord = false;
        const selectedMonth = event.detail.value;
        window.clearTimeout(this.delayTimeout);
        this.delayTimeout = setTimeout(() => {
            this.selectedMonth = selectedMonth;
        }, DELAY);
    }

    handleChangeSource(event) {
        this.getMercuriale = Object();
        this.draftValues = new Map();
        this.hasDetailRecord = false;
        const selectedSourceValue = event.target.value;
        const selectedSourceLabel = event.target.options.find(opt => opt.value === event.detail.value).label;
        window.clearTimeout(this.delayTimeout);
        this.delayTimeout = setTimeout(() => {
            this.selectedSourceValue = selectedSourceValue;
            this.selectedSourceLabel = selectedSourceLabel;
        }, DELAY);
    }

    handleInputValue(event) {
        // const template = this.template.querySelector('.index-value')
        // const draftValue = {};
        // draftValue[DETAIL_SOURCE_API_NAME] = template.dataset.indexSrc;
        // draftValue[DETAIL_VALUE_API_NAME] = event.target.value;
        // draftValue[DETAIL_DATE_API_NAME] = this.selectedDate;
        const template = this.template.querySelector('.index-value').classList.add('changed-value');
        const indexMasterId = event.target.name;
        const indexDetailValue = event.target.value;
        this.draftValues.set(indexMasterId, indexDetailValue);
    }

    handleCancel() {
        this.selectedSourceValue = '';
        this.getMercuriale = Object();
        this.draftValues = new Map();
        this.hasDetailRecord = false;
        this.dispatchEvent(new CloseActionScreenEvent());
    }

    async handleSave(event) {
        if (this.draftValues.size >= 1) {
            const recordPromises = Array.from(this.draftValues.keys()).map(masterId => {
                const draftValue = this.draftValues.get(masterId);
                const row = this.dataMap.get(masterId);
                if (row.Id) {
                    const allValid = [...this.template.querySelectorAll(".changed-value")].reduce((validSoFar, inputFields) => {
                        inputFields.reportValidity();
                        return validSoFar && inputFields.checkValidity();
                    }, true,);
                    console.log(allValid);
                    if (allValid) {
                        const fields = {};
                        fields['Id'] = row.Id;
                        // fields[MASTER_ID_API_NAME] = masterId; // by default you cannot "REPARENT" master-detail record, so the record is not writable.
                        fields[DETAIL_SOURCE_API_NAME] = row[DETAIL_SOURCE_API_NAME];
                        fields[DETAIL_VALUE_API_NAME] = draftValue;
                        fields[DETAIL_DATE_API_NAME] = this.selectedDate;
                        fields[DETAIL_FAMILY_API_NAME] = row[DETAIL_FAMILY_API_NAME];
                        fields[DETAIL_SUBFAMILY_API_NAME] = row[DETAIL_SUBFAMILY_API_NAME];
                        return updateRecord({ fields });
                    }
                } else {
                    const fields = {};
                    fields[MASTER_ID_API_NAME] = masterId;
                    fields[DETAIL_SOURCE_API_NAME] = row[DETAIL_SOURCE_API_NAME];
                    fields[DETAIL_VALUE_API_NAME] = draftValue;
                    fields[DETAIL_DATE_API_NAME] = this.selectedDate;
                    fields[DETAIL_FAMILY_API_NAME] = row[DETAIL_FAMILY_API_NAME];
                    fields[DETAIL_SUBFAMILY_API_NAME] = row[DETAIL_SUBFAMILY_API_NAME];
                    return createRecord({ apiName: DETAIL_OBJECT.objectApiName, fields });
                }
            });

            await Promise.all(recordPromises)
                .then((record) => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success',
                            message: 'Les changements sont bien pris en compte',
                            variant: 'success'
                        })
                    )
                    this.selectedSourceValue = '';
                    this.draftValues = new Map();
                    this.hasDetailRecord = false;
                    setTimeout(() => {
                        this.dispatchEvent(new CloseActionScreenEvent());
                    }, DELAY);
                    return refreshApex(this.getMercuriale);
                })
                .catch((error) => {
                    console.error('error: ', error);
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: "Error: Something wrong when creating records",
                            message: getErrorMessage(error),
                            variant: "error",
                        }),
                    )
                })
        } else {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Warning',
                    message: `Aucune modification n'a été apportée.`,
                    variant: 'warning'
                })
            );
        }
    }
}