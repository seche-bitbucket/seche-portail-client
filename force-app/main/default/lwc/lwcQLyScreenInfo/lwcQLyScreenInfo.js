import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation'
import okImageUrl from '@salesforce/resourceUrl/OkImage';
export default class LwcQLyScreenInfo extends NavigationMixin(LightningElement) {

    @api qlyFormId;
    @track showModal = false;
    @track msg;

    fromFlow = false;

    get okImageUrl() {
        return okImageUrl;
    }

    goToCancel(event) {
        this.showModal = true;
    }

    handleClose(event) {
        this.init = true;
        this.showModal = false;
    }

}