import { LightningElement, api, track } from 'lwc';
import {
    FlowAttributeChangeEvent,
    FlowNavigationNextEvent,
} from 'lightning/flowSupport';
import save from '@salesforce/apex/LWC_PolymorphicProcedures.save';

import DataGridCss from '@salesforce/resourceUrl/DataGridCss';
import { loadStyle } from 'lightning/platformResourceLoader';

export default class Lwc_DataGrid extends LightningElement {

    @api selectedRecords;
    @api draftRecords;
    @api records;
    @api columns;
    @api state;
    @api buttonActionName;
    @api actionName;
    @api previewMode;
    @api title;
    @api flowMode;
    @api tableIcon;
    @api objectApiName;
    @api displayInCard;
    @api source
    @api withSelection
    @api actions
    @api useActions
    @api allSelected

    get usedInPublicCommunity() {
        return (this.source == 'public') ? true : false
    }

    get addArticle() {
        return (this.displayInCard == 'displayInCard') ? true : false
    }

    @track draftValues = [];
    @track clmns;
    @track processedRecords = [];
    @track colWidth;
    @track headerColWidth;
    @track masterChecked;

    @track picklistPlaceholder;

    connectedCallback() {

        const maxWidth = (this.withSelection) ? 95 : 100

        this.picklistPlaceholder = (window.navigator.language.includes('fr')) ? 'Sélectionner une valeur' : 'Select Value'

        Promise.all([
            loadStyle(this, DataGridCss)
        ])

        this.clmns = JSON.parse(this.columns);

        try{
            var visibleClmnNum = 0;
            this.clmns.forEach(clmn => {
                if(clmn.visible) {
                    visibleClmnNum += 1
                }
            })

            this.headerColWidth = 'width: '+maxWidth/parseInt(visibleClmnNum)+'%;';
            this.colWidth = 'width: '+100/parseInt(visibleClmnNum)+'%;';

            this.records.forEach(record => {

                var recordCopy= {};

                for (const key in record) {
                    recordCopy[key] = record[key];
                }

                this.draftValues.push(recordCopy);
                
                var rcd = {
                    Id: record.Id,
                    referenceName: this.clmns[0].objectType,
                    fields: [],
                    selected: false
                };

                this.clmns.forEach(clmn => {
                    rcd.fields.push(
                        {
                            name: clmn.value,
                            label: clmn.label,
                            value: record[clmn.value],
                            displayedValue: (clmn.list && record[clmn.value]) ? clmn.options.find(opt => opt.value == record[clmn.value]).label : record[clmn.value],
                            edit: (clmn.edit) ? clmn.edit : false,
                            controllingValues: clmn.controllingValues,
                            controllingField: clmn.controllingField,
                            controlled: clmn.controlled,
                            editable: clmn.editable,
                            required: (clmn.required) ? clmn.required : false,
                            type: clmn.type,
                            list: clmn.list,
                            numberOnly: clmn.numberOnly,
                            options: clmn.options,
                            changed: false,
                            visible: clmn.visible,
                            valueMissing: clmn.valueMissing,
                            class: "div-table-col"
                        }
                    )
                });

                this.processedRecords.push(rcd);

            })
        } catch(e) {
            console.log(e)
        }

        console.log(JSON.stringify(this.processedRecords))
    }

    renderedCallback() {
        if(this.allSelected) {
            this.selectAll(true)
            this.allSelected = false
        }
    }

    getSelectedName(event) {
        this.selectedRecords = event.detail.selectedRows;
    }

    handleClick(event) {
        this.state = event.target.name;
        this.draftRecords = this.draftValues;
        setTimeout(function() {
            const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);
        }.bind(this), 0)
    }

    // handleSave(event) {
    //     this.draftRecords = event.detail.draftValues;
    //     this.state = 'save';

    //     if(this.flowMode) {
    //         setTimeout(function() {
    //             const navigateNextEvent = new FlowNavigationNextEvent();
    //             this.dispatchEvent(navigateNextEvent);
    //         }.bind(this), 0)
    //     } else {
            
    //     }
    // }

    editLine(event) {
        event.stopPropagation();
        this.processedRecords.find(r => r.Id == event.target.dataset.recordId || r.Id == event.currentTarget.id.substring(0, event.currentTarget.id.indexOf('-'))).fields.forEach(f => {
            f.edit = f.editable && true;
        })
    }

    closeAllEdit(event) {
        event.stopPropagation();
        this.processedRecords.forEach(record => {
            record.fields.forEach(field => {
                field.edit = false;
            })
        })
    }

    preventPropagation(event) {
        event.preventDefault();
        event.stopPropagation();
    }

    handleSelect(event) {
        const checked = event.target.checked

        if(event.target.name == 'master') {
            this.selectAll(checked)
        } else if(event.target.name == 'single') {
            
            if(!this.selectedRecords) this.selectedRecords = []

            const recordId = event.target.dataset.recordId
            const record = this.draftValues.find(r => r.Id == recordId)
            let parent = this.template.querySelector('div[data-record-id="'+recordId+'"]')
            if(checked) {
                parent.classList.add('selectedRow')
                this.selectedRecords.push(record)
                this.processedRecords.find(record => record.Id == recordId).selected = true
            } else {
                const index = this.selectedRecords.indexOf(record)
                parent.classList.remove('selectedRow')
                if(index > -1) {
                    this.selectedRecords.splice(index, 1)
                }
                if(this.selectedRecords.length == 0) {
                    this.selectedRecords = undefined
                }
                this.processedRecords.find(record => record.Id == recordId).selected = false
            }
        }
        
        console.log(this.selectedRecords)

        this.masterChecked = this.processedRecords.every(r => r.selected)

        const selectEvent = new CustomEvent(
            'select', 
            { 
                detail: this.selectedRecords
            }
        );

        if(this.flowMode == 'false') {
            if(event) event.stopPropagation()
            this.dispatchEvent(selectEvent)
        }
    }

    selectAll(checked) {
        this.masterChecked = checked
        this.draftValues.forEach(r => {
            let parent = this.template.querySelector('div[data-record-id="'+r.Id+'"]')
            if(checked) {
                parent.classList.add('selectedRow')
                this.processedRecords.find(record => record.Id == r.Id).selected = true
                this.selectedRecords = this.draftValues
            } else {
                this.selectedRecords = undefined
                this.processedRecords.find(record => record.Id == r.Id).selected = false
                parent.classList.remove('selectedRow')
            }
        })
    }

    handleInlineEdit(event) {
        this.draftValues.find(record => record.Id == event.target.dataset.recordId)[event.target.dataset.fieldName] = event.target.value
        if(this.selectedRecords && this.selectedRecords.length > 0) {
            if(this.selectedRecords.find(record => record.Id == event.target.dataset.recordId))
                this.selectedRecords.find(record => record.Id == event.target.dataset.recordId)[event.target.dataset.fieldName] = event.target.value
        }
        let details = this.processChanges(event.target.dataset.recordId, event.target.dataset.fieldName, event.target.value, event)
        const changeEvent = new CustomEvent(
            'change', 
            { 
                detail: details
            }
        );

        if(this.flowMode == 'false') {
            if(event) event.stopPropagation()
            this.dispatchEvent(changeEvent)
        }
    }

    processChanges(recordId, fieldName, value, event) {
        try {
            let modifications = [];
            var record = this.processedRecords.find(r => r.Id == recordId);
            var controllingFields = []
            record.fields.forEach(f => {
                if(f.controllingField == fieldName) {
                    controllingFields.push(f)
                }
            });
            var field = record.fields.find(f => {f.value == fieldName});

            if(controllingFields.length > 0) {
                controllingFields.forEach(controllingField => {
                    var dependentValues = controllingField.controllingValues[value]
                    this.processedRecords.find(r => r.Id == recordId).fields.find(field => field.name == controllingField.name).dependentValues = dependentValues;
                    if(dependentValues && dependentValues.length > 1) {
                        // display message for completion
                        this.processedRecords.find(r => r.Id == recordId).fields.find(field => field.name == controllingField.name).value = 'Liste de choix à l\'étape suivante';
                        modifications.push({
                            recordId: recordId,
                            value: 'Liste de choix à l\'étape suivante',
                            field: controllingField.name
                        })
                    } else if(dependentValues && dependentValues.length == 1) {
                        this.processedRecords.find(r => r.Id == recordId).fields.find(field => field.name == controllingField.name).value = dependentValues[0].value;
                        this.draftValues.find(r => r.Id == recordId)[controllingField.name] = dependentValues[0].value;
                        if(this.selectedRecords && this.selectedRecords.length > 0) {
                            if(this.selectedRecords.find(r => r.Id == recordId))
                                this.selectedRecords.find(r => r.Id == recordId)[controllingField.name] = dependentValues[0].value;
                        }
                        modifications.push({
                            recordId: recordId,
                            value: dependentValues[0].value,
                            field: controllingField.name
                        })
                    } else {
                        this.processedRecords.find(r => r.Id == recordId).fields.find(field => field.name == controllingField.name).value = '';
                        this.draftValues.find(r => r.Id == recordId)[controllingField.name] = '';
                        if(this.selectedRecords && this.selectedRecords.length > 0) {
                            if(this.selectedRecords.find(r => r.Id == recordId))
                                this.selectedRecords.find(r => r.Id == recordId)[controllingField.name] = '';
                        }
                        modifications.push({
                            recordId: recordId,
                            value: '',
                            field: controllingField.name
                        })
                    }
                })
            }

            this.processedRecords.find(r => r.Id == recordId).fields.find(field => field.name == fieldName).value = value;    
            modifications.push({
                recordId: recordId,
                value: value,
                field: fieldName
            })

            this.processedRecords.find(r => r.Id == recordId).isChanged = true;
            this.processedRecords.find(r => r.Id == recordId).fields.find(field => field.name == fieldName).displayedValue = (this.processedRecords.find(r => r.Id == recordId).fields.find(field => field.name == fieldName).list) ? this.processedRecords.find(r => r.Id == recordId).fields.find(field => field.name == fieldName).options.find(opt => opt.value == value).label : value;   

            let draft = JSON.parse(JSON.stringify(this.draftValues.find(r => r.Id == recordId))); //... deep clone object
            draft[fieldName] = value;
            this.draftValues[this.draftValues.findIndex(r => r.Id == recordId)] = draft;

            return {
                changes: [...modifications],
                processedRecords: this.processedRecords
            }

        } catch(e) {
            console.log(e)
        }
    }

    checkAllCheckboxes(event) {
        let eventChange = new Event('change');
        let i
        let checkboxes = this.template.querySelectorAll('[data-id="checkbox"]')
        for(i=0; i<checkboxes.length; i++) {
            checkboxes[i].checked = event.target.checked
            checkboxes[i].dispatchEvent(eventChange)
        }
    }

    selectRow(event) {
        const recordId = event.target.value
        const record = this.draftValues.find(r => r.Id == recordId)
        const checked = event.target.checked
        let parent = this.template.querySelector('[data-id="'+recordId+'"]')
        if(checked) {
            parent.classList.add('selectedRow')
            this.selectedRecords.push(record)
        } else {
            const index = this.selectedRecords.indexOf(record)
            parent.classList.remove('selectedRow')
            if(index > -1) {
                this.selectedRecords.splice(index, 1)
            }
        }
    }

    @api
    apiTriggerChange(recordId, fieldName, value, event) {
        return new Promise((resolve, reject) => {
            resolve(this.processChanges(recordId, fieldName, value, event))
        });
    }

    @api
    reportValidity() {
        var datagrid_combobox = [...this.template.querySelectorAll('lightning-combobox')];
        var datagrid_comboboxValidity = true
        if(datagrid_combobox && datagrid_combobox.length > 0) {
            for(let input of datagrid_combobox) {
                if(datagrid_comboboxValidity && !input.reportValidity()) {
                    datagrid_comboboxValidity = false
                    break;
                }
            }
        }
        return datagrid_comboboxValidity
    }

}