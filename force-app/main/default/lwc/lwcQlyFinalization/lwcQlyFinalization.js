import { LightningElement, api,track, wire } from 'lwc';
import prefixSummary from '@salesforce/apex/AP_QLyManager.prefixSummary';
import getContentDocumentFromForm from '@salesforce/apex/AP_QLyManager.getContentDocumentFromForm';
import deleteContentDocument from '@salesforce/apex/AP_QLyManager.deleteContentDocument';
export default class LwcQlyFinalization extends LightningElement {
    
    @track isUploaded = false;
    uploadedFiles = [];
    @track existingFiles = [];

    @api finalizationCommentary;
    @api placeholder;
    @api isNotify;
    @api isAccepted;
    @api formId;

    connectedCallback(){
        // get all uploaded files in form
        console.log('retrieve all uploaded file on step3 => '+this.formId);
        getContentDocumentFromForm({formID: String(this.formId), isSummary: true})
            .then((result) => {
                if(result){
                    console.log('getContentDocumentFromForm result: '+JSON.stringify(result));
                    result.forEach(doc => {
                        this.existingFiles.push(doc);
                    });

                    if(this.existingFiles.length > 0)
                    {
                        this.isUploaded = true;
                    }
                }
            })
            .catch((error) => {
                console.log('error: '+JSON.stringify(error));
            });
    }

    //method to send form inout field to parent (lwc-Main)
    handleUserInputChange(event) {        
        console.log('LwcFinalization.handleUserInputChange start ---', event.target.name);
        const field = event.target.name;
        if ('commentName' === field) {           
            const inputValue = event.target.value;
            let userInputValue = { field: field, inputValue: inputValue };
            const userInputEvent = new CustomEvent(
                "userinputvalue", { detail: userInputValue }
            );
            console.log('LwcFinalization.handleUserInputChange.userInputValue ---', userInputValue);
            this.dispatchEvent(userInputEvent);
            //sendDataToParent();
        }else if (field === 'acceptCheckBox' || field === 'notifCheckBox') {           
            const inputValue = event.target.checked;
            let userInputValue = { field: field, inputValue: inputValue };
            const userInputEvent = new CustomEvent(
                "userinputvalue", { detail: userInputValue }
            );
            console.log('LwcFinalization.handleUserInputChange.userInputValue ---', userInputValue);
            this.dispatchEvent(userInputEvent);
            //sendDataToParent();

        } 

    }

    // add Upload file step3 - AID 10/01/21

    get acceptedFormats() {
        return ['.pdf', '.png','.jpg','.jpeg', '.xls', '.xlsx', '.docx', '.doc'];
    }  

    handleUploadFinished(event) {

        var documentIds = [];
        let strFileNames = '';
        // Get the list of uploaded files
        const uploadedFiles = event.detail.files;
        console.log('LwcQlyFinalization-uploadedFiles ==> '+JSON.stringify(uploadedFiles));
        console.log(this.formId);

        // Using foreach for better performance
        // Author: Amine IDRISSI
        uploadedFiles.forEach(file => {
            strFileNames += file.name + ', ';
            documentIds.push(String(file.documentId));
            console.log('file id: '+file.documentId);
        });

        prefixSummary({documentIds: documentIds})
            .then((result) => {
                console.log('prefixSummary result: '+JSON.stringify(result));
                result.forEach(file => {
                    this.existingFiles.push(file);
                });

                this.isUploaded = true;

                console.log('LwcQlyFinalization.handleUploadFinished event: '+JSON.stringify(event));

                const pendingFilesIds =  this.existingFiles;

                let processAction = { pendingFilesIds : pendingFilesIds };
                const uploadfiles = new CustomEvent(
                    "uploadfiles", { detail: processAction }
                );
                this.dispatchEvent(uploadfiles);
                
            })
            .catch((error) => {
                console.log('error: '+error);
            });

        /*for(let i = 0; i < uploadedFiles.length; i++) {
            strFileNames += uploadedFiles[i].name + ', ';
        }*/

        //this.dispatchEvent(showToast('success', undefined, undefined, strFileNames + ' fichier chargé !'));       

    }

    deleteHandler(event){
        console.log(event.target.dataset.id);
        console.log(this.existingFiles.findIndex(doc => doc.Id == event.target.dataset.id));

        let indexOfDoc = this.existingFiles.findIndex(doc => doc.Id == event.target.dataset.id);

        deleteContentDocument({docId: event.target.dataset.id})
            .then((result) => {
                console.log('deleteContentDocument result: '+JSON.stringify(result));
                this.existingFiles.splice(indexOfDoc, 1);

                if(this.existingFiles.length === 0)
                {
                    this.isUploaded = false;
                }

            })
            .catch((error) => {
                console.log('error: '+JSON.stringify(error));
            });

    }
    //
}