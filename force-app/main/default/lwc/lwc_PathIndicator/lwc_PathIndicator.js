import { LightningElement, api } from 'lwc';

export default class Lwc_PathIndicator extends LightningElement {

    @api steps;
    width

    connectedCallback() {
        this.width = 'width: '+100/this.steps.length+'%'
    }

}