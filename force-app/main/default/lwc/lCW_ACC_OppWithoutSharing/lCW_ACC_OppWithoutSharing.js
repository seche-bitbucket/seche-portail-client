import { LightningElement, track, wire, api } from 'lwc';
import listUsersNbOpp from '@salesforce/apex/LWC_AccOppWithoutSharingCtrl.getOpportunitiesWithoutSharingByAccount';

const columns = [
    { label: 'UTILISATEUR', fieldName: 'OwnerLink', type: 'url', typeAttributes: { label: {fieldName: 'OwnerName'}, target:'_blank'}},
    { label: 'FILIALE', fieldName: 'CompanyName'},
    { label: 'N°TÉLÉPHONE', fieldName: 'phone', type: 'phone', initialWidth: 141 },
    { label: 'NB OPPORTUNITÉS', fieldName: 'nbOpp', type : 'number'}
];

const _title = 'Opportunités depuis 1 an et utilisateurs liés à ce compte'

export default class LCW_ACC_OppWithoutSharing extends LightningElement {    
    @track columns = columns;
    @track error;
    @track dataReturn;
    @api recordId;

    get title() {
        return _title;
    }

    @wire(listUsersNbOpp, {accId: '$recordId'}) wiredListUsersNbOpp({ error, data }) {
        if (data) {
            var dataStruc = [];
            data.forEach(function(item){
                if(item){
                    dataStruc.push(
                        {
                            OwnerName : item['OwnerName'],
                            OwnerLink : '/lightning/r/User/' +  item['ownerID'] + '/view',
                            CompanyName : item['CompanyName'],
                            phone : item['phone'],
                            nbOpp : item['cnt']
                        }
                    );
                }
            });
            this.dataReturn = dataStruc;
            this.error = undefined;      
        } else if (error) {
            this.dataShow = undefined;
            this.error = error;
            console.log('error : ' + error);
        }
    }
}