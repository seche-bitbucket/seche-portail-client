import { LightningElement, api, track, wire } from 'lwc';
import getWasteItems from '@salesforce/apex/LWC_CustomBannerCtrl.getRecord';
import copyProducts from '@salesforce/apex/LWC_DDECtrl.copyProducts';
import getDefaultProd from '@salesforce/apex/LWC_DDECtrl.getDefaultProd';
import save from '@salesforce/apex/LWC_CustomBannerCtrl.save';
import deleteAll from '@salesforce/apex/LWC_DDECtrl.deleteAll';
import Product2 from '@salesforce/schema/Product2';
import WasteCollectRequestItem__c from '@salesforce/schema/WasteCollectRequestItem__c';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getPickListValuesIntoList from '@salesforce/apex/LWC_CustomBannerCtrl.getPickListValuesIntoList';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { deleteRecord } from 'lightning/uiRecordApi';
import init from '@salesforce/apex/LWC_CustomBannerCtrl.init';
import getRecord from '@salesforce/apex/LWC_CustomBannerCtrl.getRecord';

export default class Lwc_DDE extends LightningElement {

    FIELDS = {
        'qte': 'Quantite__c',
        'container': 'Container__c',
        'containerNb': 'ContainerNumber__c'
    };

    @api recordId;
    @track listWastes = [];
    @track showHover = false;

    @track objColor;
    @track objIcon;

    @track wait=true;

    @track modalStyle;

    @track toggleSpecial;

    @track showModal = false;

    @track productSpecial = [];

    position;

    popup = {
        title: '',
        message: ''
    };

    product = {
        Name: '',
        Code: '',
        Container: ''
    };

    channel;
    disabled;

    type;
    typeView;

    @track disableInput = false;

    forceDisableSave = false;

    //An Object to fill show user all available options
    @track optionValues = { controlling: [], dependent: [] };
    //To fill all controlling value and its related valid values
    @track allDependentOptions = {};
    //An Api Name for Controlling PickList Field
    controllingPicklistApiName = 'WasteType__c';
    //An Api Name for Dependent Picklist for any Object
    dependentPicklistApiName = 'Container__c';
    //An Api Name for Dependent Picklist for any Object
    subDependentPicklistApiName = 'ContainerType__c';

    barOffset;
    navbar;
    width;
    height;
    outMarket;
    outMarketOffset;
    outMarketWidth;

    prodId;

    connectedCallback() {

        init({o: this.recordId, recordId: this.recordId})
        .then(data => {
            if(data){
                this.disableInput = data.disabled;
            }
        });

        getRecord({o: this.recordId, recordId: this.recordId})
        .then((data) =>{
            if(data){
                this.record = data[0];
                this.type = data[0].RequestType__c;
            }
        })

        try{

            getDefaultProd()
            .then(data => {
                if(data) {
                    this.prodId = data;
                }
            });
            
            getPickListValuesIntoList({o: 'WasteCollectRequestItem__c', obj: 'WasteCollectRequestItem__c', field: 'Packaging__c'})
            .then(data => {
                this.packaging = data;
            });

            this.refreshState();

            this.template.addEventListener('wheel', evt => {  
                this.template.querySelectorAll('input[type=number]').forEach(i => i.blur()); // disable scroll on input fields for number
            });

        } catch(e) {
            console.log(e);
        }

    }

    @track packaging = [];

    status;

    highDiv;
    id;

    record;
    state;

    handleToggle(event) {

        try{

            this.toggleSpecial = event.target.checked;

            if(this.toggleSpecial == true) {

                setTimeout(function() {
                    try{
                        const topDiv = this.template.querySelector('[data-id="special"]');
                        topDiv.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
                    } catch(e) {
                        console.log(e);
                    }
                }.bind(this), 100);

            } else {

                window.scrollTo({top: 0, left:0, behavior: 'smooth'});
                    
            }

            if(!(this.productSpecial.length > 0)) {
                this.productSpecial.push({
                    index: 0,
                    Id: '',
                    Demande_Enlevement__c: this.recordId,
                    Product__c: this.prodId,
                    TECH_OutOfMarket__c: true,
                    ADR__c: '',
                    CAP__c: '',
                    Packaging__c: '',
                    ProductName__c: '',
                    Quantite__c :'',
                    ContainerNumber__c: '',
                    containerInputDisabled: true
                });
                this.hasOnlyOne = true;
            }

        } catch(e) {
            console.log(e);
        }

    }

    handleAddItem(event) {
        this.productSpecial.push({
            index: this.productSpecial.length,
            Id: '',
            Demande_Enlevement__c: this.recordId,
            Product__c: this.prodId,
            TECH_OutOfMarket__c: true,
            ADR__c: '',
            CAP__c: '',
            Packaging__c: '',
            ProductName__c: '',
            Quantite__c :'',
            ContainerNumber__c: '',
            containerInputDisabled: true
        });
        this.hasOnlyOne = (this.productSpecial.length == 1) ? true: false;

        setTimeout(() => {
            const table = this.template.querySelector('.spec');
            table.scrollIntoView({behavior: "smooth", block: "end", inline: "end"});
        }, 50);

    }

    handleDeleteAll(event) {
        this.popup.title = 'Suppression de données';
        this.popup.message = 'Etes-vous sûr de vouloir supprimer tous les déchets que vous avez ajoutés ? Cette action est irréversible.'
        this.showModal = true;
    }

    hasOnlyOne;

    handleDelete(event) {

        try{

            let index = event.target.dataset.id;
            let t = this.productSpecial;

            if('Id' in this.productSpecial.find(e => e.index == index) && this.productSpecial.find(e => e.index == index).Id.length > 0) {

                deleteRecord(this.productSpecial.find(e => e.index == index).Id)
                .then(() => {

                    for( var i = 0; i < this.productSpecial.length; i++){ 

                        if ( this.productSpecial[i].index == index) { 
                    
                            t.splice(i, 1); 
                        }
                    
                    }
                    this.hasOnlyOne = (this.productSpecial.length == 1) ? true: false;

                })

            } else {

                for( var i = 0; i < this.productSpecial.length; i++){ 
            
                    if ( this.productSpecial[i].index == index) { 
                        t.splice(i, 1); 
                    }
                
                }

                this.hasOnlyOne = (this.productSpecial.length == 1) ? true: false;

            }

        } catch(e) {
            console.log(e);
        }
        
    }

    handleOK(event) {

        let ids;

        this.productSpecial.forEach(e => {
            console.log(e.Id);
            if(ids == undefined) {
                ids = e.Id;
            } else {
                ids += ', '+e.Id;
            }
        });

        console.log(ids);
        
        deleteAll({ids: ids})
        .then( () => {
            this.productSpecial = [{
                index: 0,
                Id: '',
                Demande_Enlevement__c: this.recordId,
                Product__c: this.prodId,
                TECH_OutOfMarket__c: true,
                ADR__c: '',
                CAP__c: '',
                Packaging__c: '',
                ProductName__c: '',
                Quantite__c :'',
                ContainerNumber__c: '',
                containerInputDisabled: true
            }];
            this.showModal = false;
            this.hasOnlyOne = (this.productSpecial.length == 1) ? true: false;

        })
    }

    handleNO(event) {
        this.showModal = false;
    }

    refreshState() {

        this.wait = true;

        this.listWastes = [];
        this.productSpecial = [];
        getWasteItems({o: 'WasteCollectRequestItem__c', recordId: this.recordId})
        .then(data => {
            if(data && data.length > 0) {
                data.forEach(e => {
                    e.index = data.indexOf(e);
                    if(e.TECH_OutOfMarket__c) {
                        e.index = this.productSpecial.length;
                        if(e.CAP__c == undefined) e.CAP__c = ' ';
                        if(e.ProductName__c == undefined) e.ProductName__c = ' ';
                        if(e.ADR__c == undefined) e.ADR__c = ' ';
                        e.containerInputDisabled = (this.disableInput) ? true : (e.Quantite__c > 0) ? false : true;
                        this.productSpecial.push(e);
                        this.hasOnlyOne = (this.productSpecial.length == 1) ? true : false;
                    } else {
                        e.containerInputDisabled = (this.disableInput) ? true : (e.Quantite__c > 0) ? false : true;
                        this.listWastes.push(e);
                    }
                });

                if(this.productSpecial.length == 0 && this.toggleSpecial) {
                    this.productSpecial.push({
                        index: 0,
                        Id: '',
                        Demande_Enlevement__c: this.recordId,
                        Product__c: this.prodId,
                        TECH_OutOfMarket__c: true,
                        ADR__c: '',
                        CAP__c: '',
                        Packaging__c: '',
                        ProductName__c: '',
                        Quantite__c :'',
                        ContainerNumber__c: '',
                        containerInputDisabled: true
                    });
                    this.hasOnlyOne = true;
                }

            } else if((data == undefined || !data || data == null || data.length == 0) && this.type != undefined) {
                if(this.type.length > 1) {

                    copyProducts({type: this.type, masterId: this.recordId})
                    .then(result => {
                        this.refreshState();
                    })
        
                }
            }

            this.toggleSpecial = (this.productSpecial.length > 0);
        })
        
        this.wait = false;

    }

    renderedCallback() { }

    @wire(getObjectInfo, { objectApiName: Product2 })
    handleResult({error, data}) {
        if(data) {


            let objectInformation = data;

            this.objColor = "background-color: #"+objectInformation.themeInfo.color;
            this.objIcon = objectInformation.themeInfo.iconUrl;

            // access theme info here
            // icon URL is availe as themeInfo.iconUrl
            //this.themeInfo = objectInformation.themeInfo || {};
        }
        if(error) {
            // handle error
        }
    }

    objIconWCRI;
    objColorWCRI;

    @wire(getObjectInfo, { objectApiName: WasteCollectRequestItem__c })
    handleResult2({error, data}) {
        if(data) {


            let objectInformation = data;

            this.objColorWCRI = "background-color: #"+objectInformation.themeInfo.color;
            this.objIconWCRI = objectInformation.themeInfo.iconUrl;

            // access theme info here
            // icon URL is availe as themeInfo.iconUrl
            //this.themeInfo = objectInformation.themeInfo || {};
        }
        if(error) {
            // handle error
        }
    }

    handleOnOver(event) {

        try{
            
            var bb = this.template.querySelector('[data-index="'+event.target.dataset.index+'"]').getBoundingClientRect();
            // Correct and works with touch.
            this.position = {
                b: bb.bottom,
                x: bb.left,
                y: bb.top,
                h: bb.height,
                w: bb.width,
                r: bb.right
            }

            let ddeItem_Index = event.target.dataset.index;
            let ddeItem = this.listWastes[this.listWastes.findIndex(e => e.index == ddeItem_Index)];

            this.product.Name = ddeItem.Product__r.Name;
            this.product.ADR__c = ddeItem.Product__r.ADR__c;
            this.product.N_CAP__c = ddeItem.Product__r.N_CAP__c;

            let eltTop = parseFloat(this.position.y);
            let processed1 = eltTop-58;
            let processed2 = parseFloat(this.position.r) + 30;

            this.modalStyle = "top: "+processed1+"px !important; left: "+processed2+"px !important; visibility: visible !important; z-index: 10 !important;";
            this.showHover = true;

        } catch(e) {
            console.log(e);
        }

    }

    handleMouseOut(event) {

        this.showHover = false;
        this.modalStyle = "visibility: hidden !important; z-index: -1 !important;";

    } 

    handleButton(event) {

        if(this.type.length > 1) {

            copyProducts({type: this.type, masterId: this.recordId})
            .then(result => {
                this.refreshState();
            })

        }

    }

    handleInput(event) {

        try{

            if(event.target.name == 'Quantite__c' && event.target.value == 0) {
                this.listWastes[this.listWastes.findIndex(e => e.Id === event.target.dataset.id)].containerInputDisabled = true;
            } else if(event.target.name == 'Quantite__c' && event.target.value != undefined && event.target.value > 0) {
                this.listWastes[this.listWastes.findIndex(e => e.Id === event.target.dataset.id)].containerInputDisabled = false;
            }

            this.listWastes[this.listWastes.findIndex(e => e.Id === event.target.dataset.id)][event.target.name] = event.target.value;

        } catch(e) {
            console.log(e);
        }

    }

    handleInputSpec(event) {

        try{

            if(event.target.name == 'Quantite__c' && event.target.value == 0) {
                this.productSpecial.find(e => e.index == event.target.dataset.id).containerInputDisabled = true;
            } else if(event.target.name == 'Quantite__c' && event.target.value != undefined && event.target.value > 0) {
                this.productSpecial.find(e => e.index == event.target.dataset.id).containerInputDisabled = false;
            }

            this.productSpecial.find(e => e.index == event.target.dataset.id)[event.target.name] = event.target.value;

        } catch(e) {
            console.log(e);
        }

    }

    isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    checkNum(event) {

        if((event.keyCode >= 48) && (event.keyCode <= 57)) {
            return true;
        } else {
            event.returnValue = false;
        }

    }

    handleSave(event) {

        try{

            let marketWastesEmpty = false;
            let outMarketWastesEmpty = false;

            this.wait = true;
            
            let wcr = {};
            for (var i in this.record)
                wcr[i] = this.record[i];

            console.log(wcr);

            let lstSave = [];
            let prodctSpecs = this.productSpecial;

            for(let t=0; t < this.listWastes.length; t++) {
                if(this.listWastes[t].Quantite__c != undefined) {
                    marketWastesEmpty = false;
                    break;
                } else {
                    marketWastesEmpty = true;
                }
            }
            
            if(!marketWastesEmpty) {
                console.log('not empty')
                Array.prototype.push.apply(lstSave, this.listWastes);
            }

            for(let v=0; v < this.productSpecial.length; v++) {
                if(this.productSpecial[v].Quantite__c != undefined) {
                    outMarketWastesEmpty = false;
                    break;
                } else {
                    outMarketWastesEmpty = true;
                }
            }

            if(marketWastesEmpty || (this.toggleSpecial && outMarketWastesEmpty)) {
                if(this.toggleSpecial && outMarketWastesEmpty) {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Erreur lors de l\'enregistrement - Hors Marché',
                            message: 'Veuillez bien renseigner un ou plusieurs déchets hors marché ou décochez cette option',
                            variant: 'warning',
                        })
                    ); 
                    this.wait = false;
                    return;
                }

                if(marketWastesEmpty && this.toggleSpecial && outMarketWastesEmpty) {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Erreur lors de l\'enregistrement',
                            message: 'Veuillez bien renseigner un ou plusieurs déchets',
                            variant: 'warning',
                        })
                    ); 
                    this.wait = false;
                    return;
                }

            }

            console.log(JSON.stringify(lstSave));

            if(this.toggleSpecial) {
                prodctSpecs.forEach(e => {

                    if(!e.Id || e.Id == undefined || e.Id.length == 0) {
                        delete e.Id;
                    }
                    if(!lstSave.includes(e)) lstSave.push(e);

                })
            }

            lstSave.forEach(e => {
                delete e.index;
            });

            if(wcr['OutOfMarket__c'] != this.toggleSpecial) {
                wcr['OutOfMarket__c'] = this.toggleSpecial;
                var lst = [wcr];
                save({o: this.recordId, metadata: JSON.stringify(lst), dataType: 'WasteCollectRequest__c'})
                .then((data, error) => {
                    console.log(data);
                    if(error) {
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: 'Erreur lors de l\'enregistrement',
                                message: JSON.stringify(error),
                                variant: 'error',
                            })
                        ); 
                        this.wait = false;
                        return;
                    }

                    save({o: 'WasteCollectRequestItem__c', metadata: JSON.stringify(lstSave), dataType: 'WasteCollectRequestItem__c'})
                    .then((data, error) => {
                        console.log(data);
                        
                        this.wait = false;
                        if(error) {
                            this.dispatchEvent(
                                new ShowToastEvent({
                                    title: 'Erreur lors de l\'enregistrement',
                                    message: JSON.stringify(error),
                                    variant: 'error',
                                })
                            ); 
                            return;
                        }

                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: 'Déchets enregistrés',
                                message: 'Les déchets insérés ont bien été enregistrés',
                                variant: 'success',
                            })
                        );

                        // setTimeout(function() {
                        //     window.location.reload();
                        // }, 2000);

                    });
                })
            } else {
                save({o: 'WasteCollectRequestItem__c', metadata: JSON.stringify(lstSave), dataType: ' WasteCollectRequestItem__c'})
                .then((data, error) => {
                    console.log(data);
                    if(error) {
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: 'Erreur lors de l\'enregistrement',
                                message: JSON.stringify(error),
                                variant: 'error',
                            })
                        ); 
                        this.wait = false;
                        return;
                    }

                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Déchets enregistrés',
                            message: 'Les déchets insérés ont bien été enregistrés',
                            variant: 'success',
                        })
                    );
                    this.refreshState();
                    this.wait = false;
                });
            }

        } catch(e) {
            console.log(e);
        }

    }

    handleChangeCBB(event) {
        const field = event.target.name;
        const selected = event.target.value;

        this.listWastes[this.listWastes.findIndex(e => e.Id === event.target.dataset.id)].Packaging__c = selected;

    }

    get showWastes() {
        return (this.listWastes && this.listWastes.length > 0) ? true : false;
    }

}