import { LightningElement, api, track } from 'lwc';
import cancelRequest from '@salesforce/apex/CB_WasteCollectRequest.lwcCancelRequest';
import { CloseActionScreenEvent } from "lightning/actions";
import {
    FlowNavigationNextEvent,FlowNavigationBackEvent
} from 'lightning/flowSupport';

export default class Lwc_DDE_CancelScreen extends LightningElement {

    @api wasteCollectId;
    @api state;

    @track wait = false;
    @track init = true;
    @track success = false;

    @track msg;

    connectedCallback() {
        console.log(this.wasteCollectId);
    }

    handleClose(event) {
        event.preventDefault();
        event.stopPropagation();
        const navigateFinishEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateFinishEvent);
    }

    handleReturn(event) {
        event.preventDefault();
        event.stopPropagation();
        this.state = 'close';
        const navigateFinishEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateFinishEvent);
    }

    handleSubmit(event) {
        try {
            this.wait = true;
            event.preventDefault();
            var fields = event.detail.fields;
            fields.Id = this.wasteCollectId;
            console.log(this.wasteCollectId);
            cancelRequest({jsonSobject: JSON.stringify(fields)})
            .then((data) => {
                if(!data || data == 'NOK') {
                    this.msg = 'Il y a eu une erreur lors de l\'annulation de la demande, veuillez réessayer ou contacter un administrateur.';
                } else if(data && data == 'OK') {
                    this.msg = 'La demande a bien été annulée.';
                    this.success = true;
                }
                this.wait = false;
                this.init = false;
            })
        } catch(e) {
            console.log(e)
        }
    }

    retry(event) {
        this.init = true;
        this.wait = false;
    }

}