import { LightningElement, api, track } from 'lwc';

export default class LwcQlySummaryValidation extends LightningElement {
    @api userSelectedServices;
    @api userInfo;
    @api listProducts;

    @track itemDisabled = true;
    @track size = 'small';
    @track multiSelect = true;
    @track vertical = false;
    @track arePrestationSelectedForuserInfo = false;
    @track isDisabled = false;
    @track userTD = {}

    @api finalizationCommentary;
    @api isNotify;
    @api isAccepted;
    @api formId;

    @track pin

    get pin_pos0() {
        return (this.pin[0] == ' ') ? null : this.pin[0]
    }

    get pin_pos1() {
        return (this.pin[1] == ' ') ? null : this.pin[1]
    }

    get pin_pos2() {
        return (this.pin[2] == ' ') ? null : this.pin[2]
    }

    get pin_pos3() {
        return (this.pin[3] == ' ') ? null : this.pin[3]
    }

    start = false;

    @api topElement;

    @track placeHolder = '';

    connectedCallback() {   
        console.log('LwcQlySummaryValidation.connectedCallback.userInfo', this.userInfo.contactId); 
        console.log('LwcQlySummaryValidation.connectedCallback.userSelectedServices', JSON.stringify(this.userSelectedServices));
        this.userTD.HasWasteTrackAccess__c = this.userInfo.HasWasteTrackAccess__c
        this.userTD.IsUserWasteTrackUser__c = this.userInfo.IsUserWasteTrackUser__c
        this.userTD.WasteTrackFirstName__c = this.userInfo.WasteTrackFirstName__c
        this.userTD.WasteTrackSIRET__c = this.userInfo.WasteTrackSIRET__c
        this.userTD.WasteTrackLastName__c = this.userInfo.WasteTrackLastName__c
        this.userTD.WasteTrackEmail__c = this.userInfo.WasteTrackEmail__c
        this.pin = (this.userInfo.WasteTrackSignCode__c) ? this.userInfo.WasteTrackSignCode__c : '    '
        this.start = true;      
    }  

    renderedCallback() {
        if(this.start) {
            this.start = false;
            this.topElement.scrollIntoView({
                behavior: 'smooth',
                block: 'start'
            });
        }
    }

    get arePrestationSelected(){
        this.arePrestationSelectedForuserInfo = this.userSelectedServices.listSelectedPresta.length > 0;
        return (this.userSelectedServices.listSelectedPresta.length > 0)
    }

    get areWasteSelected(){
        return (this.userSelectedServices.listSelectedWaste.length > 0)
    }

    
    get areContainerSelected(){
        return (this.userSelectedServices.listSelectedContainer.length > 0)
    }

    handleUpload(event){

        console.log('LwcQlySummaryValidation.handleUpload event.detail: '+JSON.stringify(event.detail.pendingFilesIds));

        const pendingFilesIds =  event.detail.pendingFilesIds;

        let processAction = { pendingFilesIds : pendingFilesIds };
        const uploadfiles = new CustomEvent(
            "uploadfiles", { detail: processAction }
        );
        this.dispatchEvent(uploadfiles);

    }

    handleUserInputChange(event) {
        console.log('LwcQlySummaryValidation.handleUserInputChange start ---', event.detail.field);
        console.log('LwcQlySummaryValidation.handleUserInputChange.userInputValue ---', event.detail.inputValue);
        const field = event.detail.field;
        const inputValue = event.detail.inputValue;
        let userInputValue = { field: field, inputValue: inputValue };
        const userInputEvent = new CustomEvent(
            "userinputvalue", { detail: userInputValue }
        );
        this.dispatchEvent(userInputEvent);  
        this.isDisabled = false;               
    }

    handleUserTrackDechetInputs(event) {
        const userInputEvent = new CustomEvent(
            "userinputvalue", { detail: event.detail }
        );
        this.dispatchEvent(userInputEvent); 
    }
   
    handleValueInput(event) {
        if((event.keyCode >= 48) && (event.keyCode <= 57)) {
            return true;
        } else {
            event.returnValue = false;
        }
    }

    @api validateTrackDechet(event) {
        try{
            return this.template.querySelector('c-lwc-qly-track-dechet').validateInputs()
        } catch(e) {
            console.log(e)
        }
    }

    handleSave(event){ 
        this.isDisabled = true;
        console.log('LwcQlyForm.handleSave --- start' );       
        //send to main
        const saveEvent = new CustomEvent(
            "save" );  
            console.log('lucas save event : ' + saveEvent);
            this.dispatchEvent(saveEvent);      
    }
}