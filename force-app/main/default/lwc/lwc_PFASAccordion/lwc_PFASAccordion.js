import { LightningElement, api, track } from 'lwc';

export default class Lwc_PFASAccordion extends LightningElement {

    @api records
    @api language

    index = 1
    items = []
    @track nbItems

    connectedCallback() {
        this.records.forEach(element => {
            let elementJsonParsed = JSON.parse(JSON.stringify(element))
            elementJsonParsed.localVars = {
                open: false,
                index: this.index,
                class: 'tab' + ((element.HasPfas__c === 'Oui' && element.Constituants__r) || element.HasPfas__c === 'Non' ? ' done' : ''),
                nbComponents: (element.Constituants__r) ? element.Constituants__r.length + (element.Constituants__r.length > 1 ? ' Substances' : ' Substance') : '0 Substance'
            }
            this.items.push(elementJsonParsed)
            this.index++
        });

        this.items[0].localVars.open = true;
        this.nbItems = this.items.length;
    }

    handleEventData(event) {
        try {
            this.dispatchEvent(new CustomEvent(event.detail.Name, {detail: event.detail}))
        } catch(e) {
            console.log(e)
        }
    }

}