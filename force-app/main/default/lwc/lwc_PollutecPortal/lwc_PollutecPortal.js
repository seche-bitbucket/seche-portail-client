import { LightningElement, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';

export default class Lwc_PollutecPortal extends LightningElement {

    @track GUID
    @track v2

    pollutec_img
    seche_img

    @wire(CurrentPageReference)
    getStateParameters(currentPageReference) {
        console.log('page : ' + currentPageReference)
       if (currentPageReference) {
          this.urlStateParameters = currentPageReference.state
          this.setParametersBasedOnUrl()
          console.log('guid : ' + this.GUID)
       }
    }

    setParametersBasedOnUrl() {
        this.GUID = this.urlStateParameters.recordId || null
        this.v2 = this.urlStateParameters.version == 2
    }

}