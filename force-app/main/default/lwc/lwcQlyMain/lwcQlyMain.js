import { LightningElement, api, track, wire } from 'lwc';
// impoting CURRENT USER id
import USER_ID from '@salesforce/user/Id';
// importing to get the record details based on record id
import { getRecord } from 'lightning/uiRecordApi';
import { showToast, getCurrentYear, groupByKey } from 'c/ldsUtils';

import lwcQlyMain from './lwcQlyMain.html';

import label from 'c/lwcCustomLabels';
import fetchUserInfo from '@salesforce/apex/AP_QLyManager.fetchUserInfo';
import fetchUserCampaignInfo from '@salesforce/apex/AP_QLyManager.fetchUserCampaignInfo';
import fetchCampaignProducts from '@salesforce/apex/AP_QLyManager.fetchCampaignProducts';
import fetchSchoolServices from '@salesforce/apex/AP_QLyManager.fetchSchoolServices';
import createForm from '@salesforce/apex/AP_QLyManager.createForm';
import processSave from '@salesforce/apex/AP_QLyManager.processSave';
import processDelete from '@salesforce/apex/AP_QLyManager.processDelete';
import isCampaignOver from '@salesforce/apex/AP_QLyManager.isCampaignOver';
import deleteContentDocuments from '@salesforce/apex/AP_QLyManager.deleteContentDocuments';
import piklistValues from '@salesforce/apex/AP_QLyManager.getPiklistValues';
import linkDocToForm from '@salesforce/apex/AP_QLyManager.linkDocToForm';
import getApprovStatus from '@salesforce/apex/AP_QLyManager.getApprovStatus';
import getAvailablePackaging from '@salesforce/apex/AP_QLyManager.getAvailablePackaging';
import getCampaignProductsPrices from '@salesforce/apex/AP_QLyManager.getCampaignProductsPrices';
import processCampaignMemberStatus from '@salesforce/apex/AP_QLyManager.processCampaignMemberStatus';
import { refreshApex } from '@salesforce/apex';
import orgUrl from '@salesforce/label/c.QLY_CommunityDomainName';
import {loadStyle} from 'lightning/platformResourceLoader';
import cssOverride from '@salesforce/resourceUrl/CssOverride';


export default class LwcQlyMain extends LightningElement {
    @track selectedStep = 'Step1';
    // Reactive field using @track decorator
    @track  userId = USER_ID;
   // @track userId = '0056E0000072AchQAE';
    @track contactId;
    @track companyId;
    @track campaignPriceBookId;
    @track campaignMemberId;
    @track campaignProducts;
    @track listProducts = [];
    @track showAddContainer = false;
    @track userSelectedServices = { listSelectedContainer: [], listSelectedWaste: [], listSelectedPresta: [] };
    @track nextYear = getCurrentYear() + 1;
    @track options;
    @track origin;
    @track wasteFamily;
    @track waste;
    @track campaignOver;
    @track userInfo = {};
    @track steps = [{ label: "{label.lwcYourInfo}", value: "{label.step1}" }, { label: "{label.lwcYourServices}", value: "{label.step2}" }, { label: "{label.lwcSummaryValidation}", value: "{label.step3}" }];
    @track isNoAdd = true;
    @track testLabel = ' Étape 1 :<br/> Vos informations';
    @track qlyFormId;
    @track qlyCreatedDate;

    @track showForm = false;
    @track formIsNotFinished = false;
    @track formIsFinished = false;
    @track formCancelled = false;
    isRejected = false;
    isAccepted = false;

    @track isDisabled_button = false;

    topElement;
    formStatus;
    formId = false;
    FIRST_COLLECT_LABEL = '1ère collecte mai/juin 2023';
    SECOND_COLLECT_LABEL = '2ème collecte octobre/décembre 2023';
    SERVICE_TYPE = 'Collecte et service';
    CONTAINER_TYPE = 'Contenant';
    WASTE_TYPE = 'Traitement déchets';
    MONDAY = 'Lundi';
    TUESDAY = 'Mardi';
    WEDNESDAY = 'Mercredi';
    THURSDAY = 'Jeudi';
    FRIDAY = 'Vendredi';
    MORNING = 'matin';
    AFTERNOON = 'après-midi';
    EVENT_FROM_CONTAINER = 'fromSelectedContainer';
    EVENT_FROM_WASTE = 'fromSelectedService';
    WARNING_MESSAGE_FIELDS_MISSING = 'Certains champs sont manquants ou incorrects';
    WARNING_MESSAGE_MISSING_FILES = 'Veuillez ajouter au moins un fichier pour le déchet';
    WASTE_NAME;
    WARNING_MESSAGE;
    DISPLAY_STYLE = 'Quantite_Photos';
    idsToDelete = [];
    idsServToDelete = [];
    idsWastesToDelete = [];
    idsContToDelete = [];
    pendingFiles = [];
    wasteData = [];
    globalWasteData = [];
    wasteIdsWithRelatedFiles = [];
    finalizationCommentary;

    CONTAINER_PACKAGING = [{}];

    PHONE_PATTERN = '(\\+\\d+(\\s|-))?0\\d(\\s|-)?(\\d{2}(\\s|-)?){4}';


    //productPriceMap;

    // LWC lifecycle hooks
    async connectedCallback() {

        
        //loadStyle(this, cssOverride);

        window.addEventListener('beforeunload', this.windowsUnload.bind(this));

        await this.getAvailablePackaging()
        await this.fetchUserInfo(this.userId)
        await this.fetchUserCampaignInfo(this.contactId)
        if(this.campaignInfo) {
            await this.isCampaignOver(this.campaignInfo.id)
            await this.getApprovStatus(this.companyId)
            await this.fetchSchoolServices(this.companyId, this.campaignInfo.collection__c)
            await this.serviceData(this.companyId, this.campaignInfo.collection__c)
        } else {
            this.showForm = true
            this.formIsNotFinished = true
        }
    }


    renderedCallback() {

        this.topElement = this.template.querySelector('.topMain');

        /*fetchSchoolServices({ accountId: this.companyId })
        .then(data => {
            window.console.log('renderedCallback.fetchSchoolServices.data reren ====> ', JSON.stringify(data));
            if (data.length == 0) {
                this.formIsNotFinished = true;
            } else {
                window.console.log('renderedCallback.fetchSchoolServices.data reren FormStatus__c ====> ', data[0].FormStatus__c);
                this.formStatus = data[0].FormStatus__c;
                this.formIsFinished = (data[0].FormStatus__c == 'Terminé' || data[0].FormStatus__c == 'Envoyé') ? true : false;
                if (this.formIsFinished){
                    this.formIsNotFinished = false;
                } else {
                    this.formIsNotFinished = true;
                }
                window.console.log('renderedCallback.fetchSchoolServices.else.data reren====> ', this.formIsFinished);
            }

            this.showForm = true;

        });*/

    }

    async getAvailablePackaging() {
        const data = await getAvailablePackaging()
        console.log('packagig data: ', JSON.stringify(data));
        this.CONTAINER_PACKAGING = data;
        console.log('container pack: ', this.CONTAINER_PACKAGING);
    }

    async fetchUserInfo(userId) {
        const data = await fetchUserInfo({ userId: userId })
        window.console.log('LwcQlyMain.connectedCallback.fetchUserInfo.data ====> ', JSON.stringify(data));
        console.log(data[0]);
        //get contactId (Customer User)
        this.contactId = data[0].ContactId;
        this.companyId = data[0].AccountId;
        this.userInfo = {
            firstName: data[0].Contact.FirstName,
            lastName: data[0].Contact.LastName,
            siret: data[0].Account.SIRET_Number__c,
            name: data[0].Contact != undefined ? data[0].Contact.Name : data[0].Name,
            address: {
                city: (data[0].Account != undefined && data[0].Account.BillingAddress != undefined) ? data[0].Account.BillingAddress.city : '',
                street: (data[0].Account != undefined && data[0].Account.BillingAddress != undefined) ? data[0].Account.BillingAddress.street : '',
                postalCode: (data[0].Account != undefined && data[0].Account.BillingAddress) ? data[0].Account.BillingAddress.postalCode : '',
                country: (data[0].Account != undefined && data[0].Account.BillingAddress != undefined) ? data[0].Account.BillingAddress.country : '',
            },
            companyId: data[0].AccountId,
            companyPhone: data[0].Account != undefined ? data[0].Account.Phone : '0000000000',
            companyAddressText: (data[0].Account != undefined && data[0].Account.BillingAddress != undefined) ? ''.concat(data[0].Account.BillingAddress.street, ', ', data[0].Account.BillingAddress.postalCode, ' ', data[0].Account.BillingAddress.city) : ''.concat(',', data[0].Account != undefined ? data[0].Account.BillingAddress.city : '',
                (data[0].Account != undefined && data[0].Account.BillingAddress) ? data[0].Account.BillingAddress.postalCode : '', (data[0].Account != undefined && data[0].Account.BillingAddress != undefined) ? data[0].Account.BillingAddress.country : ''),
            contactId: data[0].ContactId,
            contactName: data[0].Account.Name,
            schoolCode: data[0].Account.QLY_SchoolCode__c,
            phone: data[0].Contact != undefined ? data[0].Contact.Phone : data[0].Phone,
            email: data[0].Contact != undefined ? data[0].Contact.Email : data[0].Email,
            company: data[0].Account != undefined ? data[0].Account.Name : '',
            haveCollectThisYear: true /*false modif du 25 mai pour la deuxieme collecte*/,
            // firstCollectCheckBox: false /*false modif du 25 mai pour la deuxieme collecte*/,
            firstCollect: null /*modif du 25 mai pour la deuxieme collecte*/,
            wantChangeAddress: false,
            isSameAddressForWaste: true,
            days: [
                {
                    rank: 0,
                    name: this.MONDAY,
                    morning: { morning: this.MORNING, selected: true /*false modif du 25 mai pour la deuxieme collecte*/},
                    afternoon: { afternoon: this.AFTERNOON, selected: true /*false modif du 25 mai pour la deuxieme collecte*/ }
                },
                {
                    rank: 1,
                    name: this.TUESDAY,
                    morning: { morning: this.MORNING, selected: true /*false modif du 25 mai pour la deuxieme collecte*/ },
                    afternoon: { afternoon: this.AFTERNOON, selected: true /*false modif du 25 mai pour la deuxieme collecte*/ }
                },
                {
                    rank: 2,
                    name: this.WEDNESDAY,
                    morning: { morning: this.MORNING, selected: true /*false modif du 25 mai pour la deuxieme collecte*/ },
                    afternoon: { afternoon: this.AFTERNOON, selected: true /*false modif du 25 mai pour la deuxieme collecte*/ }
                },
                {
                    rank: 3,
                    name: this.THURSDAY,
                    morning: { morning: this.MORNING, selected: true /*false modif du 25 mai pour la deuxieme collecte*/ },
                    afternoon: { afternoon: this.AFTERNOON, selected: true /*false modif du 25 mai pour la deuxieme collecte*/ }
                },
                {
                    rank: 4,
                    name: this.FRIDAY,
                    morning: { morning: this.MORNING, selected: true /*false modif du 25 mai pour la deuxieme collecte*/ },
                    afternoon: { afternoon: this.AFTERNOON, selected: true /*false modif du 25 mai pour la deuxieme collecte*/ }
                }],

            mondayMorning: this.MONDAY + ' ' + this.MORNING,
            tuesdayMorning: this.TUESDAY + ' ' + this.MORNING,
            wednesdayMorning: this.WEDNESDAY + ' ' + this.MORNING,
            thursdayMorning: this.THURSDAY + ' ' + this.MORNING,
            fridayMorning: this.FRIDAY + ' ' + this.MORNING,
            mondayAfternoon: this.MONDAY + ' ' + this.AFTERNOON,
            tuesdayAfternoon: this.TUESDAY + ' ' + this.AFTERNOON,
            wednesdayAfternoon: this.WEDNESDAY + ' ' + this.AFTERNOON,
            thursdayAfternoon: this.THURSDAY + ' ' + this.AFTERNOON,
            fridayAfternoon: this.FRIDAY + ' ' + this.AFTERNOON,
            selectedDays: [],
            comment: '',
            isAccepted: true,
            isNotify: true
        }

        window.console.log('LwcQlyMain.connectedCallback.this.userInfo ====> ', JSON.stringify(this.userInfo));
    }
    
    async fetchUserCampaignInfo(contactId) {
        const data = await fetchUserCampaignInfo({ contactId: contactId })
        if (data && data.length >= 1) {
            window.console.log('fetchUserCampaignInfo.data ====> ', data);
            //get contactId (Customer User)
            this.contactId = data[0].ContactId;
            this.campaignMemberId = data[0].Id
            console.log('fetchUserCampaignInfo this.campaignMemberId : ', this.campaignMemberId);
            this.campaignInfo = {
                id: data[0].CampaignId,
                name: data[0].Campaign.Name,
                startDate: data[0].Campaign.StartDate,
                endDate: data[0].Campaign.EndDate,
                priceBook__c: data[0].Campaign.PriceBook__c,
                status: data[0].Status,
                collection__c: data[0].Campaign.Collection__c,
            }
            this.userInfo.firstCollect = this.campaignInfo.collection__c
            this.campaignPriceBookId = data[0].Campaign.PriceBook__c;

            this.userInfo.priceBookId = this.campaignPriceBookId;

            window.console.log(' this.campaignInfo ====> ', this.campaignInfo);
        }
    }

    async isCampaignOver(campaignId) {
        const data = await isCampaignOver({campaignId: campaignId})
        this.campaignOver = data
    }

    async fetchSchoolServices(accountId, collection) {
        const data = await fetchSchoolServices({ accountId: accountId, collection: collection })
        window.console.log('LwcQlyMain.connectedCallback.fetchSchoolServices.data ====> ', JSON.stringify(data));
        this.formData = data;
        if (data.length == 0) {
            this.formIsNotFinished = true;
            if(this.campaignOver) {
                this.campaignMemberId = null
            }
        } else {
            this.qlyFormId = data[0].Id;
            this.userInfo.HasWasteTrackAccess__c = data[0].HasWasteTrackAccess__c
            this.userInfo.IsUserWasteTrackUser__c = data[0].IsUserWasteTrackUser__c
            this.userInfo.WasteTrackFirstName__c = data[0].WasteTrackFirstName__c
            this.userInfo.WasteTrackSIRET__c = data[0].WasteTrackSIRET__c
            this.userInfo.WasteTrackLastName__c = data[0].WasteTrackLastName__c
            this.userInfo.WasteTrackEmail__c = data[0].WasteTrackEmail__c
            this.userInfo.WasteTrackSignCode__c = data[0].WasteTrackSignCode__c
            window.console.log('LwcQlyMain.connectedCallback.fetchSchoolServices.data reren FormStatus__c ====> ', data[0].FormStatus__c);
            this.finalizationCommentary = data[0].Comments__c;
            this.formStatus = data[0].FormStatus__c;
            this.formCancelled = (data[0].FormStatus__c == 'Annulé') ? true : false;
            this.qlyCreatedDate = data[0].CreatedDate;
            if(!this.formCancelled) {
                if(this.isRejected || (this.qlyCreatedDate >= this.campaignInfo.startDate && this.campaignInfo.status == 'Répondu' && this.campaignOver && this.isAccepted)) {
                    this.formIsFinished = false
                } else {
                    this.formIsFinished = (data[0].FormStatus__c == 'Terminé' || data[0].FormStatus__c == 'Envoyé') ? true : false
                }
                if (this.formIsFinished){
                    this.formIsNotFinished = false;
                } else {
                    this.formIsNotFinished = true;
                }
            }            
            window.console.log('LwcQlyMain.connectedCallback.fetchSchoolServices.else.data reren====> ', this.formIsFinished);
        }

        this.showForm = true;
    }

    async getApprovStatus(accountId) {
        const data = await getApprovStatus({accountId: accountId})
        console.log('LwcQlyMain.connectedCallback.getApprovStatus.data: '+JSON.stringify(data));
        if(data.length > 0){
            console.log('LwcQlyMain.connectedCallback.getApprovStatus.getApprovStatus: '+data[0].ApprovalStatus__c);
            if(String(data[0].ApprovalStatus__c) === 'Refusé') {
                this.isRejected = true
            } else if(data[0].ApprovalStatus__c === 'Validé') {
                this.isAccepted = true
            }
            console.log('LwcQlyMain.connectedCallback.getApprovStatus.isRejected -->', this.isRejected);
        }
    }

    async serviceData(accountId, collection) {
        const data = await fetchSchoolServices({accountId: accountId, collection: collection})
        this.serviceData = data;

        console.log('refreshApex2');
        if (data) {
            window.console.log('fetchSchoolServices.data.length ====> ', data);
            //this.serviceData = data;//use to refresh data by calling ApexRefresh
            if (data.length > 0) {//recup info
                this.formId = data[0].Id;
                this.userInfo.formId = this.formId;
                this.userInfo.haveCollectThisYear = true;
                this.userInfo.access19TCheckBox = data[0].Truck19T__c;
                this.userInfo.access7TCheckBox = data[0].Truck7T__c;
                this.userInfo.access3TCheckBox = data[0].Truck3_5T__c;
                this.userInfo.referentCollect = data[0].WasteCollectionReferent__c;
               
                if(this.userInfo.access19TCheckBox || this.userInfo.access7TCheckBox || this.userInfo.access3TCheckBox) {
                    this.isTruckSelected = true;
                }

                let firstCollectCheckBox = false;
                let secondCollectCheckBox = false;
                if (data[0].SelectedCollections__c){
                    data[0].SelectedCollections__c.split(";").forEach(function(selected){
                        if (selected[0] == '1'){
                            firstCollectCheckBox = true;
                        }
                        if (selected[0] == '2'){
                            secondCollectCheckBox = true;
                        }
                    });
                }
                this.userInfo.firstCollect = this.campaignInfo.collection__c


                this.userInfo.isSameAddressForWaste = data[0].IsSameAddressForWaste__c;
                this.userInfo.removalAddressText = data[0].RemovalAddress__c;
             
                if(this.userInfo.removalAddressText){
                    let removalAddressText = this.userInfo.removalAddressText;
                    this.userInfo.removalAddress = {
                        street: removalAddressText.split(', ')[0],
                        postalCode: removalAddressText.split(', ')[1].slice(0, 5),
                        city: removalAddressText.split(', ')[1].slice(6)
                    }
                }

                let listSelectedCollectionsDays = data[0].CollectionDays__c;

                this.userInfo.days = [
                    {
                        rank: 0,
                        name: this.MONDAY,
                        morning: { morning: this.MORNING, selected: listSelectedCollectionsDays.includes(this.MONDAY + ' ' + this.MORNING) },
                        afternoon: { afternoon: this.AFTERNOON, selected: listSelectedCollectionsDays.includes(this.MONDAY + ' ' + this.AFTERNOON) }
                    },
                    {
                        rank: 1,
                        name: this.TUESDAY,
                        morning: { morning: this.MORNING, selected: listSelectedCollectionsDays.includes(this.TUESDAY + ' ' + this.MORNING) },
                        afternoon: { afternoon: this.AFTERNOON, selected: listSelectedCollectionsDays.includes(this.TUESDAY + ' ' + this.AFTERNOON) }
                    },
                    {
                        rank: 2,
                        name: this.WEDNESDAY,
                        morning: { morning: this.MORNING, selected: listSelectedCollectionsDays.includes(this.WEDNESDAY + ' ' + this.MORNING) },
                        afternoon: { afternoon: this.AFTERNOON, selected: listSelectedCollectionsDays.includes(this.WEDNESDAY + ' ' + this.AFTERNOON) }
                    },
                    {
                        rank: 3, 
                        name: this.THURSDAY,
                        morning: { morning: this.MORNING, selected: listSelectedCollectionsDays.includes(this.THURSDAY + ' ' + this.MORNING) },
                        afternoon: { afternoon: this.AFTERNOON, selected: listSelectedCollectionsDays.includes(this.THURSDAY + ' ' + this.AFTERNOON) }
                    },
                    {
                        rank: 4,
                        name: this.FRIDAY,
                        morning: { morning: this.MORNING, selected: listSelectedCollectionsDays.includes(this.FRIDAY + ' ' + this.MORNING) },
                        afternoon: { afternoon: this.AFTERNOON, selected: listSelectedCollectionsDays.includes(this.FRIDAY + ' ' + this.AFTERNOON) }
                    }];
                this.userInfo.mondayMorning = this.userInfo.days[0].morning.selected == true ? this.MONDAY + ' ' + this.MORNING : '';
                this.userInfo.mondayAfternoon = this.userInfo.days[0].afternoon.selected == true ? this.MONDAY + ' ' + this.AFTERNOON : '';
                this.userInfo.tuesdayMorning = this.userInfo.days[1].morning.selected == true ? this.TUESDAY + ' ' + this.MORNING : '';
                this.userInfo.tuesdayAfternoon = this.userInfo.days[1].afternoon.selected == true ? this.TUESDAY + ' ' + this.AFTERNOON : '';
                this.userInfo.wednesdayMorning = this.userInfo.days[2].morning.selected == true ? this.WEDNESDAY + ' ' + this.MORNING : '';
                this.userInfo.wednesdayAfternoon = this.userInfo.days[2].afternoon.selected == true ? this.WEDNESDAY + ' ' + this.AFTERNOON : '';
                this.userInfo.thursdayMorning = this.userInfo.days[3].morning.selected == true ? this.THURSDAY + ' ' + this.MORNING : '';
                this.userInfo.thursdayAfternoon = this.userInfo.days[3].afternoon.selected == true ? this.THURSDAY + ' ' + this.AFTERNOON : '';
                this.userInfo.fridayMorning = this.userInfo.days[4].morning.selected == true ? this.FRIDAY + ' ' + this.MORNING : '';
                this.userInfo.fridayAfternoon = this.userInfo.days[4].afternoon.selected == true ? this.FRIDAY + ' ' + this.AFTERNOON : '';


                // TODO : try to get back to data[0].SelectedStep__c after saving data
                //if(data[0].SelectedStep__c){
                //    this.userInfo.selectStep = data[0].SelectedStep__c;
                //    this.selectedStep = this.userInfo.selectedStep;
                //}


                //data[0].CollectionDays__c;
                let listSelectedPresta = [];
                let listSelectedContainer = [];
                let selectedPresta;
                let selectedContainer;
                let listSelectedWaste = [];
                let selectedWaste;

                //-------------------------------------Restore all wastes------------------------------------------//
                //get differents waste Type
                let normalDisplay = [];
                let QuantityPhotosDisplay = [];
                let QuantityListePhotosDisplay = [];
                let peroxydeDisplay = [];

                if(data[0].QLY_Services__r){
                    normalDisplay = data[0].QLY_Services__r.filter(waste => waste.Product__r.QLY_DisplayType__c === 'Normal');
                    window.console.log('fetchSchoolServices.normalDisplay ====> ', normalDisplay);
                    QuantityPhotosDisplay = data[0].QLY_Services__r.filter(waste => waste.Product__r.QLY_DisplayType__c === 'Quantite_Photos');
                    window.console.log('fetchSchoolServices.QuantityPhotosDisplay ====> ', QuantityPhotosDisplay);
                    QuantityListePhotosDisplay = data[0].QLY_Services__r.filter(waste => waste.Product__r.QLY_DisplayType__c === 'Quantite_Liste_Photos');
                    window.console.log('fetchSchoolServices.QuantityListePhotosDisplay ====> ', QuantityListePhotosDisplay);
                    peroxydeDisplay = data[0].QLY_Services__r.filter(waste => waste.Product__r.QLY_DisplayType__c === 'Peroxyde_Classification_Quantite_Photos');
                    window.console.log('fetchSchoolServices.peroxydeDisplay ====> ', peroxydeDisplay);
                }
                //

                //get unique productName normal waste
                let uniqueProducNameList = [];
                //use to avoid duplicate
                let uniqueProducNameListTemp = [];

                normalDisplay.forEach(function (currentValue) {
                    if(!uniqueProducNameListTemp.includes(currentValue.Product__c)){
                        uniqueProducNameListTemp.push(currentValue.Product__c);
                        uniqueProducNameList.push({productId : currentValue.Product__c, productName : currentValue.Product__r.Name});
                    }
                });

                 //get unique productName peroxyde waste
                 let uniquePeroxydeNameList = [];
                 //use to avoid duplicate
                 let uniquePeroxydeNameListTemp = [];

                 peroxydeDisplay.forEach(function (currentValue) {
                     if(!uniquePeroxydeNameListTemp.includes(currentValue.Product__c)){
                        uniquePeroxydeNameListTemp.push(currentValue.Product__c);
                        uniquePeroxydeNameList.push({productId : currentValue.Product__c, productName : currentValue.Product__r.Name});
                     }
                 });

                window.console.log('fetchSchoolServices.uniqueProducNameList ====> ',JSON.stringify(uniqueProducNameList));

                //Restore normal waste display by product Name
                uniqueProducNameList.forEach(function (product) {
                    selectedWaste = {
                        id: 'checkbox',
                        productId: product.productId,
                        name: product.productName
                    };

                     //get product rows
                     let rows = [];
                     let indx = 0;

                    normalDisplay.filter(waste => waste.Product__c === product.productId).forEach(function (currentValue) {
                        //get related container to currentValue(current waste)
                        let relatedContainer = data[0].QLY_Services__r.filter(cont => cont.OriginTreatmentService__c === currentValue.Id);

                        let containers = (currentValue.Product__r.QLY_AvailablePackaging__c) ? currentValue.Product__r.QLY_AvailablePackaging__c : '';
                        let containerOpt = [];
                        if (containers && containers.includes(';')) {
                            let lst = containers.split(';');
                            lst.forEach(cnt => {
                                containerOpt.push({label: cnt, value: cnt});
                            });
                        } else {
                            containerOpt = [{label: containers, value: containers}];
                        }

                        window.console.log('fetchSchoolServices.relatedContainer ====> ', relatedContainer);
                        rows.push({
                            wasteId: currentValue.Id,
                            index: indx,
                            description: currentValue.Product__r.Description,
                            packaging: currentValue.Packaging__c,
                            containerPkg: containerOpt,
                            nbrPackaging: currentValue.PackagingQuantity__c,
                            quantity: currentValue.Quantity__c,
                            unitPrice: currentValue.UnitPrice__c,
                            containerId : relatedContainer [0] != undefined ? relatedContainer [0].Id : '',
                            addContainer: relatedContainer.length > 0 ? true : false,
                            numbContainer: relatedContainer [0] != undefined ? relatedContainer[0].Quantity__c : '',
                            originTreatmentService: '',
                            peroxyName: '',
                            classification: '',
                            peroxydeQuantity: '',
                            photoQuantity: '',
                            photoListQuantity: '',
                            features: currentValue.Product__r.Features__c,
                            adrIcon: currentValue.Product__r.QLY_ADRIcon__c,
                            clpIcon: currentValue.Product__r.QLY_CLPIcon__c,
                            files: [],
                            isSelected: true,
                            displayStyle: 'Normal'
                        });

                        indx++;

                    });

                    selectedWaste.rows = rows;
                    listSelectedWaste.push(selectedWaste);

                    window.console.log('fetchSchoolServices.lselectedWaste ====> ',selectedWaste);
                });


                //Restore  Quantity photo rows
                QuantityPhotosDisplay.forEach(function (currentValue) {
                    selectedWaste = {
                        id: 'checkbox',
                        productId: currentValue.Product__c,
                        name: currentValue.Product__r.Name,
                        rows: [
                            {
                                wasteId: currentValue.Id,
                                index: 0,
                                description: currentValue.Product__r.Description,
                                packaging: '',
                                nbrPackaging: '',
                                quantity: currentValue.Quantity__c,
                                unitPrice: currentValue.UnitPrice__c,
                                addContainer: false,
                                numbContainer: false,
                                originTreatmentService: '',
                                peroxyName: '',
                                classification: '',
                                peroxydeQuantity: '',
                                photoQuantity: currentValue.Quantity__c,
                                photoListQuantity: '',
                                features: currentValue.Product__r.Features__c,
                                adrIcon: currentValue.Product__r.QLY_ADRIcon__c,
                                clpIcon: currentValue.Product__r.QLY_CLPIcon__c,
                                files: [],
                                isSelected: true,
                                displayStyle: 'Quantite_Photos'
                            }
                        ]

                    }
                    listSelectedWaste.push(selectedWaste);
                });

                  //Restore Peroxyde rows
                  uniquePeroxydeNameList.forEach(function (product) {
                    selectedWaste = {
                        id: 'checkbox',
                        productId: product.productId,
                        name: product.productName
                    };

                     //get product rows
                     let rows = [];
                     let indx = 0;

                    peroxydeDisplay.filter(waste => waste.Product__c === product.productId).forEach(function (currentValue) {

                        rows.push({
                            wasteId: currentValue.Id,
                            index: indx,
                            description: currentValue.Product__r.Description,
                            packaging: '',
                            nbrPackaging: '',
                            quantity: currentValue.Quantity__c,
                            unitPrice: currentValue.UnitPrice__c,
                            addContainer: false,
                            numbContainer: '',
                            originTreatmentService: '',
                            peroxyName: currentValue.ProxideName__c,
                            classification: currentValue.PeroxideClassification__c,
                            peroxydeQuantity: currentValue.Quantity__c,
                            photoQuantity: '',
                            photoListQuantity: '',
                            features: currentValue.Product__r.Features__c,
                            adrIcon: currentValue.Product__r.QLY_ADRIcon__c,
                            clpIcon: currentValue.Product__r.QLY_CLPIcon__c,
                            files: [],
                            isSelected: true,
                            displayStyle: 'Peroxyde_Classification_Quantite_Photos'
                        });

                        indx++;

                    });

                    selectedWaste.rows = rows;
                    listSelectedWaste.push(selectedWaste);

                    window.console.log('fetchSchoolServices.lselectedWaste ====> ',selectedWaste);
                });


                //Restore  Quantity photo  list rows
                QuantityListePhotosDisplay.forEach(function (currentValue) {
                    selectedWaste = {
                        id: 'checkbox',
                        productId: currentValue.Product__c,
                        name: currentValue.Product__r.Name,
                        rows: [
                            {
                                wasteId: currentValue.Id,
                                index: 0,
                                description: currentValue.Product__r.Description,
                                packaging: '',
                                nbrPackaging: '',
                                quantity: currentValue.Quantity__c,
                                unitPrice: currentValue.UnitPrice__c,
                                addContainer: false,
                                numbContainer: false,
                                originTreatmentService: '',
                                peroxyName: '',
                                classification: '',
                                peroxydeQuantity: '',
                                photoQuantity: '',
                                photoListQuantity: currentValue.Quantity__c,
                                features: currentValue.Product__r.Features__c,
                                adrIcon: currentValue.Product__r.QLY_ADRIcon__c,
                                clpIcon: currentValue.Product__r.QLY_CLPIcon__c,
                                files: [],
                                isSelected: true,
                                displayStyle: 'Quantite_Liste_Photos'
                            }
                        ]
                    }
                    listSelectedWaste.push(selectedWaste);
                });



                //-------------------------------------Restore selected Collect service------------------------------------------//
                let selectedPrestas = data[0].QLY_Services__r.filter(serv => serv.Product__r.QLY_ServiceType__c === 'Collecte et service');

                selectedPrestas.forEach(function (currentValue) {
                    window.console.log('fetchSchoolServices.currentValue ====> ', JSON.stringify(currentValue));
                    window.console.log('fetchSchoolServices.currentValue.QLY_ServiceIcon__c ====> ', currentValue.Product__r.QLY_ServiceIcon__c);
                    // userSelectedServices = { listSelectedContainer: [], listSelectedWaste: [], listSelectedPresta: [] }
                    selectedPresta = {
                        id: currentValue.Id,
                        productId: currentValue.Product__c,
                        name: currentValue.Product__r.Name,
                        unitPrice: currentValue.UnitPrice__c,
                        serviceIcon: String(currentValue.Product__r.QLY_ServiceIcon__c),
                        isSelected: true
                    }
                    window.console.log('fetchSchoolServices.selectedPresta ====> ', JSON.stringify(selectedPresta));
                    listSelectedPresta.push(selectedPresta);


                });

                //-------------------------------------Restore selected Containers------------------------------------------//
                 //get only containers wich are parents
                let selectedContainers = data[0].QLY_Services__r.filter(cont => cont.Product__r.QLY_ServiceType__c === 'Contenant'  && cont.OriginContenantService__c == undefined && cont.OriginTreatmentService__c == undefined);
                selectedContainers.forEach(function (currentValue) {
                    window.console.log('fetchSchoolServices.currentValue OriginContenantService__c====> ', currentValue.OriginContenantService__c == undefined);
                    window.console.log('fetchSchoolServices.currentValue OriginTreatmentService__c====> ', currentValue.OriginTreatmentService__c == undefined);

                     //get related container to currentValue(current waste)
                     let relatedContainer = data[0].QLY_Services__r.filter(cont => cont.OriginContenantService__c === currentValue.Id);

                     window.console.log('fetchSchoolServices.relatedContainer in cont ====> ', relatedContainer);
                    selectedContainer = {
                        id: currentValue.Id,
                        productId: currentValue.Product__c,
                        name: currentValue.Product__r.Name,
                        numberContainer: currentValue.Quantity__c,
                        havePlug:  relatedContainer.length > 0 ? true : false,
                        isSelected: true,
                        containerId : relatedContainer [0] != undefined ? relatedContainer [0].Id : '',
                        addContainer: relatedContainer.length > 0 ? true : false,
                        numberPlug: relatedContainer [0] != undefined ? relatedContainer[0].Quantity__c : ''

                    }
                    listSelectedContainer.push(selectedContainer);
                });

                this.userSelectedServices.listSelectedPresta = listSelectedPresta;
                this.userSelectedServices.listSelectedWaste = listSelectedWaste;
                this.userSelectedServices.listSelectedContainer = listSelectedContainer;
                window.console.log('fetchSchoolServices.this.userSelectedServices ====> ', JSON.stringify(this.userSelectedServices.listSelectedPresta));

            }

        }
        else if (error) {
            window.console.log('error ====> ' + JSON.stringify(error))
        }
    }

    /* sort function */
    sortByWasteName(a,b) {

        let nameA = a.Name.toUpperCase();
        let nameB = b.Name.toUpperCase();

        if ( nameA < nameB ){
            return -1;
          }
          if ( nameA > nameB ){
            return 1;
          }
          return 0;
        
    }

    // @wire(fetchUserCampaignInfo, { contactId: '$contactId' })
    // processCampaign({ data, error }){
    //     if (data && data.length >= 1) {
    //         window.console.log('fetchUserCampaignInfo.data ====> ', data);
    //         //get contactId (Customer User)
    //         this.contactId = data[0].ContactId;
    //         this.campaignMemberId = data[0].Id
    //         console.log('fetchUserCampaignInfo this.campaignMemberId : ', this.campaignMemberId);
    //         this.campaignInfo = {
    //             id: data[0].CampaignId,
    //             name: data[0].Campaign.Name,
    //             startDate: data[0].Campaign.StartDate,
    //             endDate: data[0].Campaign.EndDate,
    //             priceBook__c: data[0].Campaign.PriceBook__c,
    //             status: data[0].Status,
    //             collection__c: data[0].Campaign.Collection__c,
    //         }
    //         this.campaignPriceBookId = data[0].Campaign.PriceBook__c;

    //         this.userInfo.priceBookId = this.campaignPriceBookId;

    //         window.console.log(' this.campaignInfo ====> ', this.campaignInfo);
    //     }
    //     else if (error) {
    //         window.console.log('error ====> ' + JSON.stringify(error))
    //     }
    // }

    @wire(fetchCampaignProducts, { priceBookId: '$campaignPriceBookId' })
    productsData({ error, data }) {
        if (data) {
            window.console.log('fetchCampaignProducts.data ====> ', data);
            let campaignProducts = data;
            let containerProducts = data.filter(prod => prod.Product2.QLY_ServiceType__c === this.CONTAINER_TYPE);
            let wasteProducts = data.filter(prod => prod.Product2.QLY_ServiceType__c === this.WASTE_TYPE);

            /* track waste data */
            wasteProducts.forEach(waste => {
                this.wasteData.push({id: waste.Product2Id, name: waste.Name});
            });
            window.console.log('  fetchCampaignProducts.wasteData ====> ', JSON.stringify(this.wasteData));

            let serviceProducts = data.filter(prod => prod.Product2.QLY_ServiceType__c === this.SERVICE_TYPE);
            window.console.log('  fetchCampaignProducts.wasteProducts ====> ', JSON.stringify(wasteProducts.sort(this.sortByWasteName)));
            window.console.log('  fetchCampaignProducts.containerProducts ====> ', JSON.stringify(containerProducts));
            window.console.log('  fetchCampaignProducts.serviceProducts ====> ', JSON.stringify(serviceProducts));
            this.listProducts = {
                campaignProducts: campaignProducts,
                containerProducts: containerProducts.sort(this.sortByWasteName),
                wasteProducts: wasteProducts.sort(this.sortByWasteName),
                serviceProducts: serviceProducts.sort(this.sortByWasteName)
            }

            //Map products prices
            this.listProducts.campaignProducts.forEach(function (prod) {
                window.console.log('  this.listProducts.campaignProducts.forEach ====> ', prod);
                // this.productPriceMap.set(prod.Name, prod.unitPrice);

            });

            window.console.log('  this.listProducts.campaignProducts ====> ', this.listProducts.campaignProducts);
            //window.console.log(' this.listProducts.productPriceMap ====> ', JSON.stringify(this.productPriceMap));
            window.console.log('  this.listProducts.containerProducts ====> ', this.listProducts.containerProducts);
            window.console.log('  this.listProducts.wasteProducts ====> ', JSON.stringify(this.listProducts.wasteProducts));
            window.console.log('  this.listProducts.serviceProducts ====> ', this.listProducts.serviceProducts);
        }
        else if (error) {
            window.console.log('error ====> ' + JSON.stringify(error))
        }
    }

    isInputOk(userInfo, listSelectedWaste, listSelectedContainer, wasteArray) {
        let userInfoOk = this.isUserInfoInputOk(userInfo);
        window.console.log('isInputOk.userInfoOk ====> ' + userInfoOk);
        let wasteSelectionOk = this.isWasteSelectionOk(listSelectedWaste);
        window.console.log('isInputOk.wasteSelectionOk ====> ' + wasteSelectionOk);
        let containerSelectionOk = this.isContainerSelectionOk(listSelectedContainer);
        window.console.log('isInputOk.containerSelectionOk ====> ' + containerSelectionOk);
        let wasteFilesOk = this.isUserUploadedFiles(wasteArray);
        window.console.log('isInputOk.wasteFilesOk ====> ' + wasteFilesOk);
        let prestaSelected = this.isPrestaSelected(this.userSelectedServices.listSelectedPresta, this.userSelectedServices.listSelectedWaste, this.userSelectedServices.listSelectedContainer);
        window.console.log('isInputOk.prestaSelected ====> ' + prestaSelected);

        if(!((userInfoOk || wasteSelectionOk || containerSelectionOk || wasteFilesOk) && prestaSelected)) {
            this.WARNING_MESSAGE = 'Veuillez selectionner au moins une prestation.'
        } else {
            if(!wasteFilesOk) {
                console.log('waste @wire : '+ JSON.stringify(this.WASTE_NAME));
                this.WARNING_MESSAGE = this.WARNING_MESSAGE_MISSING_FILES + ' ' + this.WASTE_NAME;
            } else {
                this.WARNING_MESSAGE = this.WARNING_MESSAGE_FIELDS_MISSING;
            }
        }

        if(this.selectedStep == 'Step1'){
            this.WARNING_MESSAGE = this.WARNING_MESSAGE_FIELDS_MISSING;
            return userInfoOk;
        }
        
        return (userInfoOk && wasteSelectionOk && containerSelectionOk && wasteFilesOk && prestaSelected);
    }

    //Check if any presta is selected
    isPrestaSelected(listPresta, listWaste, listCont) {

        if(this.selectedStep == 'Step3') {
            return true
        }

        if(listPresta.length === 0 && listWaste.length === 0 && listCont.length === 0) {
            return false
        } else {
            return true
        }

    }

    //check if require fields on step 2, wates section
    isWasteSelectionOk(listSelectedWaste){
        let aFieldIsMissing = false;
        listSelectedWaste.forEach(function(currentValue){
            currentValue.rows.forEach(function(row){
                if(row.displayStyle == 'Normal' && (!Number(row.quantity) || !Number(row.nbrPackaging) || (row.addContainer && !Number(row.numbContainer)))){
                    aFieldIsMissing = true
                    console.log('lwcQlyMain.isWasteSelectionOk row ', JSON.stringify(row));
                }
                if(row.displayStyle == 'Quantite_Photos' && !Number(row.photoQuantity)){
                    aFieldIsMissing = true
                    console.log('lwcQlyMain.isWasteSelectionOk row ', JSON.stringify(row));
                }
                if(row.displayStyle == 'Peroxyde_Classification_Quantite_Photos' && (!row.peroxyName || !row.classification || !Number(row.peroxydeQuantity))){
                    aFieldIsMissing = true
                    console.log('lwcQlyMain.isWasteSelectionOk row ', JSON.stringify(row));
                }
                if(row.displayStyle == 'Quantite_Liste_Photos' && !Number(row.photoListQuantity)){
                    aFieldIsMissing = true
                    console.log('lwcQlyMain.isWasteSelectionOk row ', JSON.stringify(row));
                }
            })
        });
        return !aFieldIsMissing;
    }

    //check if require fields on step 2, container section
    isContainerSelectionOk(listSelectedContainer){
        let aFieldIsMissing = false;
        listSelectedContainer.forEach(function(container){
            if(!Number(container.numberContainer) || (container.addContainer && !Number(container.numberPlug))){
                aFieldIsMissing = true;
                console.log('lwcQlyMain.isContainerSelectionOk container ', JSON.stringify(container));
            }
        });
        return !aFieldIsMissing;
    }

    //check if require fields on step 1
    isUserInfoInputOk(userInfo) {
        let regexpPhone = new RegExp(this.PHONE_PATTERN);

        if(!(regexpPhone.test(userInfo.phone) && userInfo.email && regexpPhone.test(userInfo.companyPhone))){
            console.log('phone', regexpPhone.test(userInfo.phone))
            console.log('companyPhone', regexpPhone.test(userInfo.companyPhone))
            console.log('email', userInfo.email)
            return false
        }else if(userInfo.wantChangeAddress == true){
            let companyAddress = userInfo.companyAddress
            if(!(companyAddress && companyAddress.street && companyAddress.city && companyAddress.postalCode)){
                console.log('companyAddress', companyAddress)
                return false
            }
        }else if(userInfo.isSameAddressForWaste == false){
            let removalAddress = userInfo.removalAddress
            if(!(removalAddress && removalAddress.street && removalAddress.city && removalAddress.postalCode)){
                console.log('removalAddress', removalAddress)
                return false
            }
        }
        return true

    }

    //check if files are uploaded
    isUserUploadedFiles(wasteArray) {
        
        console.log('lwcQlyMain.isUserUploadedFiles wasteArray: '+JSON.stringify(wasteArray));
        let isEmpty = false;

        for(let i=0; i < wasteArray.length; i++) {
            let arr = wasteArray[i].filesId;
            console.log('lwcQlyMain.isUserUploadedFiles arr: '+JSON.stringify(arr));
            console.log('lwcQlyMain.isUserUploadedFiles arr.length: '+arr.length);
            if(wasteArray[i].filesId.length > 0) {

                isEmpty = false;

            } else {

                isEmpty = true;
                this.WASTE_NAME = wasteArray[i].wasteName;
                break;
            }
        }

        return !isEmpty

    }

    get isFormAvailable() {
        if(this.campaignMemberId) {
            return true
        }
        return false
    }

    get isAccepted() {
        console.log('isAccepted/isNotify', this.isAccepted, isNotify);
        return (!this.userInfo.isAccepted || !this.userInfo.isNotify);
    }


    get getPathStyle() {
        if (this.selectedStep === "Step4") {
            return 'slds-col slds-size_1-of-1 slds-hide';
        } else {
            return 'slds-col slds-size_1-of-1';
        }
    }

    get getFooterStyle() {
        if (this.selectedStep === "Step1") {
            return 'slds-card__footer slds-float_right footer-form';
        } else {
            return 'slds-card__footer footer';
        }
    }

    get firstCollectLabel() {
        return this.FIRST_COLLECT_LABEL;
    }

    get secondCollectLabel() {
        return this.SECOND_COLLECT_LABEL;
    }

    get isDisabled() {
        try {
            console.log(this.isTruckSelected && this.isDaysSelected && true /*this.isCollect modif du 25 mai pour la deuxieme collecte*/);
            return !(this.isTruckSelected && this.isDaysSelected && true /*this.isCollect modif du 25 mai pour la deuxieme collecte*/)
        } catch(e) {
            console.log(e);
        }
    }

    @track isTruckSelected = false /*true modif du 25 mai pour la deuxieme collecte*/;
    @track isDaysSelected = true;
    @track isCollect = true;

    //method to handle input user from form the main in childs component: we catch the element that fire the event by it's name
    handleUserInputChange(event) {
        console.log('lwcQlyMain.handleUserInputChange.field start ---', event.detail.field);
        console.log('lwcQlyMain.handleUserInputChange.fieldType start ---', event.detail.fieldType);
        console.log('lwcQlyMain.handleUserInputChange.value start ---', JSON.stringify(event.detail.inputValue));
        console.log('lwcQlyMain.handleUserInputChange.when start ---', JSON.stringify(event.detail.when));

        if(event.detail.source && event.detail.source == 'TrackDechet') {
            this.userInfo[event.detail.field] = event.detail.inputValue
            if(event.detail.field == 'IsUserWasteTrackUser__c' && event.detail.inputValue == 'Non') {
                this.userInfo.WasteTrackEmail__c = ''
                this.userInfo.WasteTrackLastName__c = ''
                this.userInfo.WasteTrackFirstName__c = ''
                this.userInfo.WasteTrackSIRET__c = ''
            }

            if(event.detail.field == 'HasWasteTrackAccess__c' && event.detail.inputValue == 'Non') {
                this.userInfo.WasteTrackEmail__c = ''
                this.userInfo.WasteTrackLastName__c = ''
                this.userInfo.WasteTrackFirstName__c = ''
                this.userInfo.WasteTrackSIRET__c = ''
                this.userInfo.IsUserWasteTrackUser__c = 'Non'
            }

        } else if(event.detail.source && event.detail.source == 'TrackDechet PreFill') {
            let userTD = event.detail.infos
            console.log(JSON.stringify(userTD))
            this.userInfo.WasteTrackEmail__c = userTD.WasteTrackEmail__c
            this.userInfo.WasteTrackLastName__c = userTD.WasteTrackLastName__c
            this.userInfo.WasteTrackFirstName__c = userTD.WasteTrackFirstName__c
            this.userInfo.WasteTrackSIRET__c = userTD.WasteTrackSIRET__c

        }

        if ('firstNameInput' === event.detail.field) {
            this.userInfo.firstName = event.detail.inputValue;
        } else if ('lastNameInput' === event.detail.field) {
            this.userInfo.lastName = event.detail.inputValue;
        } else if ('contactPhoneInput' === event.detail.field) {
            this.userInfo.phone = event.detail.inputValue;
        } else if ('companyPhoneInput' === event.detail.field) {
            this.userInfo.companyPhone = event.detail.inputValue;
        }
        else if ('emailInput' === event.detail.field) {
            this.userInfo.email = event.detail.inputValue;
        }
        else if ('companyInput' === event.detail.field) {
            this.userInfo.company = event.detail.inputValue;
        } else if ('siretInput' === event.detail.field) {
            this.userInfo.siret = event.detail.inputValue;
        } else if (event.detail.fieldType == 'companyAddress') {
            this.userInfo.companyAddress = event.detail.inputValue;
            this.userInfo.companyAddressText = this.userInfo.companyAddress.street.concat(', ', this.userInfo.companyAddress.postalCode, ' ', this.userInfo.companyAddress.city);
        } else if (event.detail.fieldType == 'removalAddress') {
            this.userInfo.removalAddress = event.detail.inputValue;
            this.userInfo.removalAddressText = this.userInfo.removalAddress.street.concat(', ', this.userInfo.removalAddress.postalCode, ' ', this.userInfo.removalAddress.city);
        } else if ('collectCheckBox' === event.detail.field) {
            this.userInfo.haveCollectThisYear = event.detail.inputValue;
        } else if ('wasteAddressCheckBox' === event.detail.field) {
            this.userInfo.isSameAddressForWaste = event.detail.inputValue;
        } else if ('editAddressCheckBox' === event.detail.field) {
            this.userInfo.wantChangeAddress = event.detail.inputValue;
        } else if ('commentName' === event.detail.field) {
            this.userInfo.comment = event.detail.inputValue;
            this.finalizationCommentary = event.detail.inputValue;
        } else if ('productCheckBox' === event.detail.field) {
            this.userInfo.imProducer = event.detail.inputValue;
        } else if ('acceptCheckBox' === event.detail.field) {
            this.userInfo.isAccepted = event.detail.inputValue;
        } else if ('notifCheckBox' === event.detail.field) {
            this.userInfo.isNotify = event.detail.inputValue;
        } else if ('addContainerCheckBox' === event.detail.field) {
            this.showAddContainer = event.detail.inputValue;
        } else if ('access19TCheckBox' === event.detail.field) {
            this.userInfo.access19TCheckBox = event.detail.inputValue;
            this.userInfo.access7TCheckBox = event.detail.inputValue;
            this.userInfo.access3TCheckBox = event.detail.inputValue;
        } else if ('access7TCheckBox' === event.detail.field) {
            this.userInfo.access7TCheckBox = event.detail.inputValue;
            this.userInfo.access3TCheckBox = event.detail.inputValue;
        } else if ('access3TCheckBox' === event.detail.field) {
            this.userInfo.access3TCheckBox = event.detail.inputValue;
        } else if ('firstCollectCheckBox' === event.detail.field) {
            this.userInfo.firstCollectCheckBox = event.detail.inputValue;
            this.userInfo.firstCollect = event.detail.inputValue == true ? this.firstCollectLabel : '';
        } else if ('secondCollectCheckBox' === event.detail.field) {
            this.userInfo.secondCollectCheckBox = event.detail.inputValue;
            this.userInfo.secondCollect = event.detail.inputValue == true ? this.secondCollectLabel : '';
        } else if ('referentCollectcInput' === event.detail.field) {
            this.userInfo.referentCollect = event.detail.inputValue;
        } else if (event.detail.when) {// => update user selected days and time
            let selectedDayToUpdate = this.userInfo.days[event.detail.when.rank];
            let filteredList = this.userInfo.days.filter(day => day.name != event.detail.when.day);
            if (event.detail.when.time == this.MORNING) {
                selectedDayToUpdate.morning.selected = event.detail.inputValue;
                //TODO put in globals variables Lundi après-midi, Mardi matin"...
                //set the AM time of the slected day
                if (event.detail.when.rank == 0) {
                    console.log('lwcQlyMain.handleUserInputServiceChange.this.userInfo.mondayMorning ---', JSON.stringify(this.userInfo.mondayMorning));
                    this.userInfo.mondayMorning = event.detail.inputValue == true ? this.MONDAY + ' ' + this.MORNING : '';
                } else if (event.detail.when.rank == 1) {
                    this.userInfo.tuesdayMorning = event.detail.inputValue == true ? this.TUESDAY + ' ' + this.MORNING : '';
                } else if (event.detail.when.rank == 2) {
                    this.userInfo.wednesdayMorning = event.detail.inputValue == true ? this.WEDNESDAY + ' ' + this.MORNING : '';
                } else if (event.detail.when.rank == 3) {
                    this.userInfo.thursdayMorning = event.detail.inputValue == true ? this.THURSDAY + ' ' + this.MORNING : '';
                } else if (event.detail.when.rank == 4) {
                    this.userInfo.fridayMorning = event.detail.inputValue == true ? this.FRIDAY + ' ' + this.MORNING : '';
                }

            } else if (event.detail.when.time == this.AFTERNOON) {
                selectedDayToUpdate.afternoon.selected = event.detail.inputValue;
                //set the PM time of the slected day
                if (event.detail.when.rank == 0) {
                    this.userInfo.mondayAfternoon = event.detail.inputValue == true ? this.MONDAY + ' ' + this.AFTERNOON : '';
                    console.log('lwcQlyMain.handleUserInputServiceChange.this.userInfo.mondayAfternoon ---', JSON.stringify(this.userInfo.mondayAfternoon));
                } else if (event.detail.when.rank == 1) {
                    this.userInfo.tuesdayAfternoon = event.detail.inputValue == true ? this.TUESDAY + ' ' + this.AFTERNOON : '';
                } else if (event.detail.when.rank == 2) {
                    this.userInfo.wednesdayAfternoon = event.detail.inputValue == true ? this.WEDNESDAY + ' ' + this.AFTERNOON : '';
                } else if (event.detail.when.rank == 3) {
                    this.userInfo.thursdayAfternoon = event.detail.inputValue == true ? this.THURSDAY + ' ' + this.AFTERNOON : '';
                } else if (event.detail.when.rank == 4) {
                    this.userInfo.fridayAfternoon = event.detail.inputValue == true ? this.FRIDAY + ' ' + this.AFTERNOON : '';
                }
            }
            //add new updated object to list at the same index
            filteredList.splice(event.detail.when.rank, 0, selectedDayToUpdate);
            //update days
            this.userInfo.days = [...filteredList];
            console.log('lwcQlyMain.handleUserInputServiceChange. this.userInfo.days new ---', JSON.stringify(this.userInfo.days));
        } else if ('acceptCheckBox' === event.detail.field) {
            this.userInfo.isAccepted = event.detail.inputValue;
        } else if ('notifCheckBox' === event.detail.field) {
            this.userInfo.isNotify = event.detail.inputValue;
        } else if ('commentName' === event.detail.field) {
            this.userInfo.comment = event.detail.inputValue;
        }

        try{
            
            if(this.userInfo.haveCollectThisYear && !this.letUserChange) {

                this.letUserChange = true;

                for(let i=0; i<this.userInfo.days.length; i++) {
                    this.userInfo.days[i].morning.selected = true;
                    this.userInfo.days[i].afternoon.selected = true;
                }
                
            } else if((!this.userInfo.haveCollectThisYear && this.letUserChange) || (!this.userInfo.haveCollectThisYear && !this.letUserChange)) {

                this.letUserChange = false;

                for(let i=0; i<this.userInfo.days.length; i++) {
                    this.userInfo.days[i].morning.selected = false;
                    this.userInfo.days[i].afternoon.selected = false;
                }

                this.userInfo.mondayMorning = this.userInfo.days[0].morning.selected == true ? this.MONDAY + ' ' + this.MORNING : '';
                this.userInfo.mondayAfternoon = this.userInfo.days[0].afternoon.selected == true ? this.MONDAY + ' ' + this.AFTERNOON : '';
                this.userInfo.tuesdayMorning = this.userInfo.days[1].morning.selected == true ? this.TUESDAY + ' ' + this.MORNING : '';
                this.userInfo.tuesdayAfternoon = this.userInfo.days[1].afternoon.selected == true ? this.TUESDAY + ' ' + this.AFTERNOON : '';
                this.userInfo.wednesdayMorning = this.userInfo.days[2].morning.selected == true ? this.WEDNESDAY + ' ' + this.MORNING : '';
                this.userInfo.wednesdayAfternoon = this.userInfo.days[2].afternoon.selected == true ? this.WEDNESDAY + ' ' + this.AFTERNOON : '';
                this.userInfo.thursdayMorning = this.userInfo.days[3].morning.selected == true ? this.THURSDAY + ' ' + this.MORNING : '';
                this.userInfo.thursdayAfternoon = this.userInfo.days[3].afternoon.selected == true ? this.THURSDAY + ' ' + this.AFTERNOON : '';
                this.userInfo.fridayMorning = this.userInfo.days[4].morning.selected == true ? this.FRIDAY + ' ' + this.MORNING : '';
                this.userInfo.fridayAfternoon = this.userInfo.days[4].afternoon.selected == true ? this.FRIDAY + ' ' + this.AFTERNOON : '';

            }
            
            if(this.userInfo.haveCollectThisYear) {
                if(this.userInfo.firstCollectCheckBox || this.userInfo.secondCollect) {
                    this.isCollect = true;
                } else {
                    this.isCollect = false;
                }

                for(let i=0; i<this.userInfo.days.length; i++) {
                    if(this.userInfo.days[i].morning.selected || this.userInfo.days[i].afternoon.selected) {
                        this.isDaysSelected = true;
                        break;
                    } else {
                        this.isDaysSelected = false;
                    }
                }

                if(this.userInfo.access3TCheckBox || this.userInfo.access7TCheckBox || this.userInfo.access19TCheckBox) {
                    this.isTruckSelected = true;
                } else {
                    this.isTruckSelected = false;
                }
            }else{
                this.isDaysSelected = true;
                this.isTruckSelected = true;
                this.isCollect = true;
            }

        } catch(e) {
            console.log(e);
        }

    }

    letUserChange = false;

    //method to handle user input from service items (from childs component : prestation, waste, container ) : we catch the element that fire the event by it's name
    handleUserInputServiceChange(event) {
        console.log('lwcQlyMain.handleUserInputServiceChange. event.target.productName; start ---', event.detail.productName);
        console.log('lwcQlyMain.handleSelectedServices.unitPrice start ---', event.detail.unitPrice);
        console.log('lwcQlyMain.handleUserInputServiceChange. event.target.name; start ---', event.detail.field);
        console.log('lwcQlyMain.handleUserInputServiceChange. event.target.value start ---', event.detail.inputValue);
        console.log('lwcQlyMain.handleUserInputServiceChange.displayStyle start ---', event.detail.displayStyle);
        console.log('lwcQlyMain.handleUserInputServiceChange.currentIndex start ---', event.detail.currentIndex);
        console.log('lwcQlyMain.handleUserInputServiceChange.eventOrigin start ---', event.detail.eventOrigin);

        const productName = event.detail.productName;
        const unitPrice = event.detail.unitPrice;
        const field = event.detail.field;
        const displayStyle = event.detail.displayStyle;
        const currentIndex = event.detail.currentIndex;
        const inputValue = event.detail.inputValue;
        const eventOrigin = event.detail.eventOrigin;
        const icon = event.detail.icon;

        let updatedListWaste = [];
        let updatedListContainer = [];

        if (eventOrigin === this.EVENT_FROM_WASTE) {//event from selected waste item row
            //update the waste object values
            this.userSelectedServices.listSelectedWaste.forEach(function (currentValue) {

                console.log('lwcQlyMain.handleUserInputServiceChange.currentValue.rows---', JSON.stringify(currentValue.rows));

                if (currentValue.name === productName) {
                    //before filter to remove the old row to update
                    let filteredRows = currentValue.rows.filter(function (row, index) {
                        return row.index != currentIndex;
                    });
                    console.log('lwcQlyMain.handleUserInputServiceChange.filteredRows---old', JSON.stringify(filteredRows));

                    //clone row using the row index
                    let rowToUpdate = currentValue.rows[currentIndex];

                    console.log('lwcQlyMain.handleUserInputServiceChange.rowToUpdate---Old.packaging', JSON.stringify(rowToUpdate.packaging));
                    console.log('lwcQlyMain.handleUserInputServiceChange.rowToUpdate---Old.index', JSON.stringify(rowToUpdate.index));

                    //update the cloned row
                    if ('wastePackaging' === field) {
                        rowToUpdate.packaging = inputValue;
                    } else if ('numberPack' === field) {
                        rowToUpdate.nbrPackaging = inputValue;
                        rowToUpdate.numbContainer = inputValue;
                    } else if ('quantity' === field) {
                        rowToUpdate.quantity = inputValue;
                    } else if ('addContainerToWasteCheckBox' === field) {
                        rowToUpdate.addContainer = inputValue;
                    } else if ('numberContAddToWaste' === field) {
                        rowToUpdate.numbContainer = inputValue;
                    } else if ('photoListQuantity' === field) {
                        rowToUpdate.photoListQuantity = inputValue;
                    } else if ('peroxydeName' === field) {
                        rowToUpdate.peroxyName = inputValue;
                    } else if ('perxoydeClassification' === field) {
                        rowToUpdate.classification = inputValue;
                    } else if ('peroxydeQuantity' === field) {
                        rowToUpdate.peroxydeQuantity = inputValue;
                    } else if ('photoQuantity' === field) {
                        rowToUpdate.photoQuantity = inputValue;
                    }

                    console.log('lwcQlyMain.handleUserInputServiceChange.rowToUpdate---New', JSON.stringify(rowToUpdate));
                    //insert the updated row to list
                    filteredRows.splice(currentIndex, 0, rowToUpdate);

                    console.log('lwcQlyMain.handleUserInputServiceChange.filteredRows---New', JSON.stringify(filteredRows));

                    currentValue.rows = [...filteredRows];
                }

                console.log('lwcQlyMain.handleUserInputServiceChange.currentValue ---', JSON.stringify(currentValue));
                updatedListWaste.push(currentValue);
                console.log('lwcQlyMain.handleUserInputServiceChange.updatedListWaste ---', JSON.stringify(updatedListWaste));

            });

        } else if (eventOrigin === this.EVENT_FROM_CONTAINER) {//event from container selected row

            //remove current row from list before upadate (we will re-insert it after it updated)
            updatedListContainer = this.userSelectedServices.listSelectedContainer.filter(cont => cont.name != productName);
            //update the waste object values
            this.userSelectedServices.listSelectedContainer.forEach(function (currentValue) {

                if (currentValue.name === productName) {
                    console.log('lwcQlyMain.handleUserInputServiceChange.rowToUpdate---Old', JSON.stringify(currentValue));

                    //update the selected row
                    if ('numberContainer' === field) {
                        currentValue.numberContainer = inputValue;
                        currentValue.numberPlug = inputValue;
                    } else if ('containerCheckBox' === field) {
                        currentValue.addContainer = inputValue;
                    } else if ('numberPlug' === field) {
                        currentValue.numberPlug = inputValue;
                    }

                    console.log('lwcQlyMain.handleUserInputServiceChange.rowToUpdate---New', JSON.stringify(currentValue));

                    //re-insert the row at the same index in the list
                    updatedListContainer.splice(currentIndex, 0, currentValue);

                }

            });
            this.userSelectedServices.listSelectedContainer = [...updatedListContainer];
            console.log('lwcQlyMain.handleUserInputServiceChange.updatedListContainer ---New', JSON.stringify(updatedListContainer));
        }


    }

    //method to handle selected serivces : adding row of (waste, collect, container)
    handleSelectedServices(event) {

        console.log('lwcQlyMain.handleSelectedServices.event.detail.what start ---', event.detail.what);
        console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedService.start ---', JSON.stringify(event.detail.selectedService));
        console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedService.productId start ---', event.detail.selectedService.productId);
        console.log('lwcQlyMain.handleSelectedServices.event.detail.displayStyle start ---', event.detail.displayStyle);

        const selectedService = event.detail.selectedService;
        const what = event.detail.what;

        if (this.SERVICE_TYPE === what) {// request service = prestation
            if (selectedService.isSelected) {//add waste to liste
                this.userSelectedServices.listSelectedPresta.push(selectedService);
                console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedPresta.rowToAdd.wasteToAdd---',JSON.stringify(this.userSelectedServices.listSelectedPresta));
            } else {//remove waste from liste
                let serviceToRemove =  this.userSelectedServices.listSelectedPresta.filter(serv => serv.name == selectedService.name);
                console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedService.rowToremove.serviceToRemove---',JSON.stringify(serviceToRemove));

                if(serviceToRemove != undefined){

                    if(String(serviceToRemove[0].id).length > 0) this.idsServToDelete.push(serviceToRemove[0].id);
                }

                console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedService.rowToremove.idsServToDelete---', JSON.stringify(this.idsServToDelete));

                let filteredList = this.userSelectedServices.listSelectedPresta.filter(waste => waste.name != selectedService.name);
                this.userSelectedServices.listSelectedPresta = [...filteredList];
            }

        } else if (this.WASTE_TYPE === what) {// request service = waste
            if (selectedService.rows != undefined && selectedService.rows[0].isSelected) {//add waste to liste
                //
                if(event.detail.displayStyle === 'Quantite_Photos' || event.detail.displayStyle === 'Quantite_Liste_Photos' || event.detail.displayStyle === 'Peroxyde_Classification_Quantite_Photos')
                {
                    this.wasteIdsWithRelatedFiles.push({wasteId: event.detail.selectedService.productId, filesId: [], wasteName: event.detail.selectedService.name});
                    console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedWaste.rowToAdd.wasteToAdd---',JSON.stringify(this.wasteIdsWithRelatedFiles));
                }
                this.userSelectedServices.listSelectedWaste.push(selectedService);
            } else {//remove waste from liste
                let wasteToRemove =  this.userSelectedServices.listSelectedWaste.filter(waste => waste.name == selectedService.name);
                let idsToDeleteW = [];
                console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedService.rowToremove.wasteToRemove---',JSON.stringify(this.wasteIdsWithRelatedFiles));
                console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedService.rowToremove.wasteToRemove---',JSON.parse(JSON.stringify(wasteToRemove)));

                //get Ids To remove
                JSON.parse(JSON.stringify(wasteToRemove)).forEach(function (currentValue){
                        currentValue.rows.forEach(function (row){
                        console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedService.rowToremove.row.wasteId---', JSON.stringify(row.wasteId));
                        if(row.wasteId){
                            idsToDeleteW.push(row.wasteId);
                        }
                    });
                });

                this.idsWastesToDelete = [...idsToDeleteW];
                console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedService.rowToremove.idsWastesToDelete---', JSON.stringify(this.idsWastesToDelete));

                if(event.detail.displayStyle === 'Quantite_Photos' || event.detail.displayStyle === 'Quantite_Liste_Photos' || event.detail.displayStyle === 'Peroxyde_Classification_Quantite_Photos')
                {
                    // find index of waste on wasteIdsWithRelatedFiles
                    let waste = this.wasteIdsWithRelatedFiles.find(waste => waste.wasteId === String(event.detail.selectedService.productId));
                    console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedService.rowToremove.wasteToRemove waste --- ',waste);
                    let wasteIndex = this.wasteIdsWithRelatedFiles.indexOf(waste);
                    console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedService.rowToremove.wasteToRemove waste index --- ',wasteIndex);
                    if(waste.filesId.length > 0)
                    {
                        deleteContentDocuments({ docIds: waste.filesId })
                        .then(result => {
                            console.log('lwcQlyMain.handleSelectedServices deleteContentDocuments result : ' + JSON.stringify(result));
                        })
                        .catch(error => {
                            console.log('lwcQlyMain.handleSelectedServices deleteContentDocuments error : ' + JSON.stringify(error));
                        });
                    }

                    let tmparr = this.wasteIdsWithRelatedFiles.filter(function(entry) { return entry.wasteId !== waste.wasteId; });
                    console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedService.rowToremove.wasteToRemove tmparr --- ',tmparr);
                    this.wasteIdsWithRelatedFiles = [];
                    this.wasteIdsWithRelatedFiles = tmparr;
                    console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedService.rowToremove.wasteToRemove this.wasteIdsWithRelatedFiles --- ',this.wasteIdsWithRelatedFiles);
                    this.template.querySelector("c-lwc-qly-prestations").handleUIChange(this.wasteIdsWithRelatedFiles);

                    if(this.wasteIdsWithRelatedFiles.length === 0) {
                        this.pendingFiles = [];
                    }

                    //

                    //
                }

                let filteredList = this.userSelectedServices.listSelectedWaste.filter(waste => waste.name != selectedService.name);
                this.userSelectedServices.listSelectedWaste = [...filteredList];
            }

        } else if (this.CONTAINER_TYPE === event.detail.what) {// request service = container

            if (selectedService.isSelected) {//add waste to liste
                this.userSelectedServices.listSelectedContainer.push(selectedService);
                console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedService.this.userSelectedServices.listSelectedContainer---', JSON.stringify(this.userSelectedServices.listSelectedContainer));
            } else {//remove waste from liste
                let containerToRemove =  this.userSelectedServices.listSelectedContainer.filter(cont => cont.name == selectedService.name);
                console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedService.rowToremove.containerToRemove---',JSON.stringify(containerToRemove));

                if(containerToRemove != undefined){
                    if(String(containerToRemove[0].id).length > 0) this.idsContToDelete.push(containerToRemove[0].id);
                }

                console.log('lwcQlyMain.handleSelectedServices.event.detail.selectedService.rowToremove.idsContToDelete---', JSON.stringify(this.idsContToDelete));

                let filteredList = this.userSelectedServices.listSelectedContainer.filter(service => service.name != selectedService.name);

                this.userSelectedServices.listSelectedContainer = [...filteredList];
            }
        }
        console.log('lwcQlyMain.handleSelectedServices.this.userSelectedServices ---', JSON.stringify(this.userSelectedServices));
    }


    handleRowAction(event) {
        console.log('lwcQlyMain.handleRowAction.product start ---', event.detail.productName);
        console.log('lwcQlyMain.handleRowAction.name start ---', event.detail.name);
        console.log('lwcQlyMain.handleRowAction.action start ---', event.detail.action);
        console.log('lwcQlyMain.handleRowAction.currentIndex---', event.detail.currentIndex);

        const productName = event.detail.productName;
        const action = event.detail.action;
        const field = event.detail.name;
        const currentIndex = event.detail.currentIndex;
        const unitPrice = event.detail.unitPrice;

        const packaging = event.detail.packaging;
        const options = event.detail.options;

        console.log(packaging, options);

        console.log('lwcQlyMain.handleRowAction.this.userSelectedServices.listSelectedWastestart ---', JSON.stringify(this.userSelectedServices.listSelectedWaste));
        let currentProduct = this.userSelectedServices.listSelectedWaste.filter(waste => waste.name == productName);
        let updatedListWaste = [];
        let idsToDeleteW = [];

        this.userSelectedServices.listSelectedWaste.forEach(function (currentValue) {
            let nextIndex = currentValue.rows.length;
            console.log('lwcQlyMain.handleRowAction.nextIndex---', JSON.stringify(nextIndex));
            console.log('lwcQlyMain.handleRowAction.currentValue---', JSON.stringify(currentValue));
            console.log('lwcQlyMain.handleRowAction.currentValue.rows---', JSON.stringify(currentValue.rows));

            // console.log('lwcQlyMain.handleRowAction.currentValue.this.options[0]---', JSON.stringify(this.options[0]));
            
            if (currentValue.name === productName) {
                if (action === 'add') {
                    if (field === 'addDefaultRow'){
                        let currentRows = currentValue.rows;
                        let row = {
                            index: nextIndex,
                            packaging: (packaging) ? packaging : 'Bonbonne ou bidon avec bouchon 10 litres minimum',
                            containerPkg: options,
                            nbrPackaging: '',
                            quantity: '',
                            addContainer: false,
                            displayStyle: 'Normal',
                            unitPrice: unitPrice
                        }
                        currentRows.push(row);
                        currentValue.rows = [...currentRows];
                    }else if (field === 'addPeroxydeRow'){
                        let currentRows = currentValue.rows;
                        let row = {
                            index: nextIndex,
                            peroxyName: '',
                            classification: '',
                            peroxydeQuantity: '',
                            addContainer: false,
                            displayStyle: 'Peroxyde_Classification_Quantite_Photos',
                            unitPrice: unitPrice
                        }
                        currentRows.push(row);
                        currentValue.rows = [...currentRows];
                    }
                } else if (action == 'remove'){
                    const rows = currentValue.rows;

                    let rowsToRemove = currentValue.rows.filter(function (row, index) {
                        return row.index == currentIndex;
                    });
                    console.log('lwcQlyMain.handleRowAction rowsToRemove ---', JSON.stringify(rowsToRemove));
                    rowsToRemove.forEach(function(rowToRemove){
                        if(rowToRemove.wasteId){
                            idsToDeleteW.push(rowToRemove.wasteId);
                        }
                    });
                    console.log('lwcQlyMain.handleRowAction idsToDeleteW ---', JSON.stringify(idsToDeleteW));

                    let filteredRows = currentValue.rows.filter(function (row, index) {
                        return row.index != currentIndex;
                    });
                    //we decrease the index of rows that have index above the one we removed
                    filteredRows.forEach(function(currentRow){
                        if(currentRow.index > currentIndex){
                            currentRow.index += -1;
                        }
                    });
                    currentValue.rows = [...filteredRows];
                }

            }

            console.log('lwcQlyMain.handleRowAction.currentValue ---', JSON.stringify(currentValue));
            updatedListWaste.push(currentValue);
            console.log('lwcQlyMain.handleRowAction.updatedListWaste ---', JSON.stringify(updatedListWaste));
        });
        this.idsWastesToDelete = [...idsToDeleteW];


        // this.userSelectedServices.listSelectedWaste.length = 0;
        this.userSelectedServices.listSelectedWaste = [...updatedListWaste];

    }

    @track openModal = false;
    isInterested = true;

    handleOK() {
        try{
            this.openModal = false;
            this.isInterested = false;
            this.handleNext();
        } catch(e) {
            console.log(e);
        }
    }

    handleClose() {
        this.openModal = false;
    }

    handleNext() {

        try{

            console.log('here 1568')
            if(this.isInterested){
                if(this.selectedStep == 'Step1' && !this.userInfo.haveCollectThisYear) {
                    this.openModal = true;
                    return
                }
            }

            var getselectedStep = this.selectedStep;
            
            console.log('here')
            console.log(this.isUserUploadedFiles(this.wasteIdsWithRelatedFiles));

            if (!this.isInputOk(this.userInfo, this.userSelectedServices.listSelectedWaste, this.userSelectedServices.listSelectedContainer, this.wasteIdsWithRelatedFiles)) {
                this.dispatchEvent(showToast('warning', 'dismissable', 'Vérifiez votre saisie svp', this.WARNING_MESSAGE));
            } else {

                console.log('hummmm => '+this.template.querySelector('.topMain'));

                setTimeout(function() {
                    window.scrollTo({
                        behavior: "smooth",
                        top: this.template.querySelector('.topMain').offsetTop
                    });
                }.bind(this), 10);
                
                console.log('this.userInfo.haveCollectThisYear : ' + this.userInfo.haveCollectThisYear)
                if (getselectedStep === 'Step1') {
                    console.log('Step 1 : this.userInfo.haveCollectThisYear : ' + this.userInfo.haveCollectThisYear)
                    if (this.userInfo.haveCollectThisYear) {
                        this.selectedStep = 'Step2';
                    } else {
                        this.selectedStep = 'Step3';
                    }

                }
                else if (getselectedStep === 'Step2') {
                    this.selectedStep = 'Step3';
                }
                else if (getselectedStep === 'Step3') {
                    this.selectedStep = 'Step4';
                }
            }
        } catch(e) {
            console.log('erreur 1610')
            console.log('exception => '+e);
        }
    }

    handlePrev() {

        this.template.querySelector('.topMain').scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });

        var getselectedStep = this.selectedStep;
        if (getselectedStep === 'Step2') {
            this.selectedStep = 'Step1';
        }
        else if (getselectedStep === 'Step3') {
            if (this.userInfo.haveCollectThisYear) {
                this.selectedStep = 'Step2';
            } else {
                this.selectedStep = 'Step1';
            }

        }
        else if (getselectedStep === 'Step4') {
            this.selectedStep = 'Step3';
        }

    }

    createForm(userInfo) {
        window.console.log('LwcQlyMain.createForm ====> ', userInfo);
        createForm({form: userInfo, status: 'Brouillon'})
        .then(data => {
            window.console.log('LwcQlyMain.connectedCallback.createForm.data ====> ', data);
            this.formId = data;
            this.userInfo.formId = this.formId;
            this.userInfo.access19TCheckBox = false;
            this.userInfo.access7TCheckBox = false;
            this.userInfo.access3TCheckBox = false;
            this.userInfo.firstCollectCheckBox = false;
            this.userInfo.secondCollectCheckBox = false;
        });

    }

    handleFinish() {

        console.log('c-lwc-qly-summary-validation', this.template.querySelector('c-lwc-qly-summary-validation'))
        const TD_REPORT_RESULT = this.template.querySelector('c-lwc-qly-summary-validation').validateTrackDechet();
        if(!TD_REPORT_RESULT) {
            console.log('lwcQlyMain.handleFinish track déchets input is not ok')
            this.dispatchEvent(showToast('warning', 'dismissable', 'Track Déchets : Vérifiez votre saisie svp', this.WARNING_MESSAGE));
            return
        }

        if (!this.isInputOk(this.userInfo, this.userSelectedServices.listSelectedWaste, this.userSelectedServices.listSelectedContainer, true)) {
            console.log('lwcQlyMain.handleFinish input is not ok')
            this.dispatchEvent(showToast('warning', 'dismissable', 'Vérifiez votre saisie svp', this.WARNING_MESSAGE));
        }else{

            this.template.querySelector('.topMain').scrollIntoView({
                behavior: 'smooth',
                block: 'start'
            });

            this.showForm = false;
            let formToLink = false;

            //check if the form exists
            if(!this.formId)
            {
                formToLink = true;
            }

            this.selectedStep = 'Step4';
            this.userInfo.selectedStep = this.selectedStep;

            //process datas save
            let userInfo = JSON.stringify(this.userInfo);
            console.log('userinfo string: ',userInfo);
            let service = JSON.stringify(this.userSelectedServices.listSelectedPresta);
            console.log('lwcQlyMain.handleFinish service : '+service);
            let waste = JSON.stringify(this.userSelectedServices.listSelectedWaste);
            console.log('lwcQlyMain.handleFinish waste : '+waste);
            let container = JSON.stringify(this.userSelectedServices.listSelectedContainer);

            let campaignMemberId = this.campaignMemberId;
            console.log('lwcQlyMain.handleFinish campaignMemberId : ' + campaignMemberId);
           
            processSave({ form: userInfo, waste: waste, service: service, container: container, status: 'Terminé' })
                .then(result => {
                    console.log('lwcQlyMain.handleFinish result : ' + JSON.stringify(result));
                    let resultDeserialized = JSON.parse(result)
                    this.formId = resultDeserialized[0].formId;

                    if(formToLink)
                    {
                        //process linking doc
                        linkDocToForm({listDocIds: this.pendingFiles, formId: this.formId})
                            .then(result => {
                                console.log('lwcQlyMain.handleSave linkDocToForm result : ' + JSON.stringify(result));
                                delete this.pendingFiles;
                            })
                            .catch(error => {
                                console.log('lwcQlyMain.handleSave linkDocToForm error : ' + JSON.stringify(error));
                            });
                    }

                    this.userInfo.formId = this.formId;
                    this.userSelectedServices.listSelectedPresta = resultDeserialized[0].services;
                //process delete if exists
                this.idsToDelete = [...this.idsServToDelete, ...this.idsContToDelete,...this.idsWastesToDelete];
                console.log('lwcQlyMain.process deleteIds ---', JSON.stringify(this.idsToDelete));
                processDelete({ ids:  this.idsToDelete})
                .then(result => {
                    processCampaignMemberStatus({campaignMemberId : campaignMemberId})
                })
                .then(() => {
                    this.dispatchEvent(showToast('success', 'dismissable', undefined, 'Votre fiche est enregistrée'));
                })
                .then( () => {
                    this.isRejected = false;
                    this.showForm = true;
                });
                //

                

                })
                .catch(error => {
                    console.log('lwcQlyMain.handleSave error : ', JSON.stringify(error));
                    this.dispatchEvent(showToast('error', 'sticky', undefined, 'Fiche non enregistrée. Veuillez contacter l\'administrateur du site'));
                });

                //refresh data
                return refreshApex(this.serviceData);
            }

    }

    handleSave() {
        this.isDisabled_button = true;
        if (!this.isInputOk(this.userInfo, this.userSelectedServices.listSelectedWaste, this.userSelectedServices.listSelectedContainer, this.wasteIdsWithRelatedFiles)) {
            console.log('lwcQlyMain.handleFinish input is not ok')
            this.dispatchEvent(showToast('warning', 'dismissable', 'Vérifiez votre saisie svp', this.WARNING_MESSAGE));

        }else{

            this.showForm = false;
            console.log('handle save showform: '+this.showForm);
            let formToLink = false;

            //check if the form exists
            if(!this.formId)
            {
                formToLink = true;
            }

            console.log('userInfo: '+JSON.stringify(this.userInfo));

            // this.selectedStep = 'Step4';
            this.userInfo.selectedStep = this.selectedStep;

            //process datas save
            let userInfo = JSON.stringify(this.userInfo);
            let service = JSON.stringify(this.userSelectedServices.listSelectedPresta);
            let waste = JSON.stringify(this.userSelectedServices.listSelectedWaste);
            let container = JSON.stringify(this.userSelectedServices.listSelectedContainer);
            console.log('lwcQlyMain.handleSave userInfo', userInfo);

            processSave({ form: userInfo, waste: waste, service: service, container: container, status: 'Brouillon' })
                .then(result => {
                    console.log('lwcQlyMain.handleSave result : ' + JSON.stringify(result));
                    let resultDeserialized = JSON.parse(result)
                    this.formId = resultDeserialized[0].formId;
                    this.userInfo.formId = this.formId;
                    this.userSelectedServices.listSelectedPresta = resultDeserialized[0].services;

                    if(formToLink)
                    {
                        //process linking doc
                        linkDocToForm({listDocIds: this.pendingFiles, formId: this.formId})
                            .then(result => {
                                console.log('lwcQlyMain.handleSave linkDocToForm result : ' + JSON.stringify(result));
                                delete this.pendingFiles;
                            })
                            .catch(error => {
                                console.log('lwcQlyMain.handleSave linkDocToForm error : ' + JSON.stringify(error));
                            });
                    }

                    //process delete if exists
                    this.idsToDelete = [...this.idsServToDelete, ...this.idsContToDelete,...this.idsWastesToDelete];
                    console.log('lwcQlyMain.processSave deleteIds ---', JSON.stringify(this.idsToDelete));
                    processDelete({ ids:  this.idsToDelete})
                    .then(result => {
                    });
                    //
                    this.dispatchEvent(showToast('success', 'dismissable', undefined, 'Votre fiche est enregistrée'));

                })
                .then(() => {
                    setTimeout(function() {
                        window.location.reload();
                    }, 3000);
                })
                .catch(error => {
                    console.log(error);
                    this.dispatchEvent(showToast('error', 'sticky', undefined, 'Fiche non enregistrée. Veuillez contacter l\'administrateur du site'));
                });
            }
        //refresh data
        console.log('refreshApex1');
        this.isDisabled_button = false;
        return refreshApex(this.serviceData);
    }

    //redirect to home page
    handleRedirect(event) {
        console.log('lwcQlyMain.handleRedirect --- start');
        //clear data
        //this.userSelectedServices.listSelectedContainer.length = 0;
        //this.userSelectedServices.listSelectedWaste.length = 0;
        //this.userSelectedServices.listSelectedPresta.length = 0;


        window.location.replace(orgUrl);

    }

    handleDelete(event){

        console.log(JSON.stringify(event.detail.deleteFile));
        console.log('lwcQlyMain.handleDelete this.wasteIdsWithRelatedFiles: '+JSON.stringify(this.wasteIdsWithRelatedFiles));

        let prodId = event.detail.deleteFile.prodId;
        let fileId = event.detail.deleteFile.Id;
        console.log('lwcQlyMain.handleDelete prodId: '+prodId);
        console.log('lwcQlyMain.handleDelete fileId: '+fileId);

        this.wasteIdsWithRelatedFiles.forEach(waste => {
            console.log('lwcQlyMain.handleDelete waste: '+JSON.stringify(waste));
            console.log(waste.wasteId === prodId);
            if(waste.wasteId === prodId)
            {
                console.log('lwcQlyMain.handleDelete waste.filesId.indexOf(fileId): '+waste.filesId.indexOf(fileId));
                //waste.filesId.splice(waste.filesId.indexOf(fileId), 1);
                let tmparr = waste.filesId.filter(function(entry) { return entry !== fileId; });
                console.log('lwcQlyMain.handleDelete tmparr --- ',tmparr);
                waste.filesId = tmparr;
                console.log('lwcQlyMain.handleDelete waste.filesId --- ',waste.filesId);
                this.pendingFiles.splice(this.pendingFiles.indexOf(fileId), 1);
                console.log('lwcQlyMain.handleDelete this.pendingFiles --- ',this.pendingFiles);
                tmparr = this.globalWasteData.filter(function(entry) {return entry.documentId !== fileId});
                console.log('lwcQlyMain.handleDelete tmparr global --- ',tmparr);
                this.globalWasteData = tmparr;
                console.log('lwcQlyMain.handleDelete this.globalWasteData --- ',this.globalWasteData);

            }
        });

        console.log('lwcQlyMain.handleDelete this.wasteIdsWithRelatedFiles: '+JSON.stringify(this.wasteIdsWithRelatedFiles));

    }

    handleUpload(event){
        console.log('lwcQlyMain.handleUpload --- start');
        console.log('lwcQlyMain.handleUpload event.detail.pendingFilesIds: '+JSON.stringify(event.detail.pendingFilesIds));
        console.log('lwcQlyMain.handleUpload this.globalWasteData: '+JSON.stringify(this.globalWasteData));
        console.log('lwcQlyMain.handleUpload this.wasteIdsWithRelatedFiles: '+JSON.stringify(this.wasteIdsWithRelatedFiles));
        console.log('lwcQlyMain.handleUpload formId', this.formId);

        const isNotGlobal = (event.detail.isNotGlobal) ? true : false;
        let file = event.detail.pendingFilesIds[0];
        console.log('lwcQlyMain.handleUpload file: '+JSON.stringify(file));

        if(!isNotGlobal){
            if(this.globalWasteData.length === 0) {
                this.globalWasteData.push({prod: file.prod, name: file.name, documentId: file.Id, prodId: file.prodId});
                console.log('lwcQlyMain.handleUpload this.globalWasteData: '+JSON.stringify(this.globalWasteData));
            } else {
                this.globalWasteData.forEach(el => {
                    if(!(this.globalWasteData.indexOf(el)))  
                    {
                        this.globalWasteData.push({prod: file.prod, name: file.name, documentId: file.Id, prodId: file.prodId});
                        console.log('lwcQlyMain.handleUpload not equal this.globalWasteData: '+JSON.stringify(this.globalWasteData));
                        //{prod: prod, name: filename, documentId: doc.ContentDocumentId, prodId: prodId}
                    }
                });
            }
        }

        let exists = false;

        if(this.wasteIdsWithRelatedFiles.length > 0) {

            this.wasteIdsWithRelatedFiles.forEach(waste => {
                console.log('lwcQlyMain.handleUpload waste: '+JSON.stringify(waste));
                console.log(waste.filesId.length);
                console.log(waste.wasteId == file.prodId);
                if(waste.wasteId == file.prodId)
                {
                    exists = true;
                    console.log(exists);
                    if(waste.filesId.length === 0) { 
                        waste.filesId.push(file.Id); 
                    } else {
                        let filesId = waste.filesId;
                        console.log(filesId);
                        let i=0;
                        while(i<filesId.length){
                            console.log(i);
                            console.log(filesId[i]);
                            if(filesId[i] !== file.Id) { filesId.push(file.Id); }
                            i++;
                        }
                        waste.filesId = filesId;
                        console.log(waste.filesId);
                    }

                }
            })
            if(!exists) {
                this.wasteIdsWithRelatedFiles.push({wasteId: file.prodId, filesId:[file.Id], wasteName: file.prod});
            }
        
        } else {
            if(event.detail.pendingFilesIds.length > 1)
            {
                // we are sure we are in init state
                try {
                    event.detail.pendingFilesIds.forEach(f => {
                        this.wasteIdsWithRelatedFiles.push({wasteId: f.prodId, filesId:[f.Id], wasteName: f.prod});
                    });
                    console.log('formData', this.formData);
                    let lst = ['Peroxyde_Classification_Quantite_Photos', 'Quantite_Liste_Photos', 'Quantite_Photos'];
                    let selectedData = this.formData[0].QLY_Services__r;
                    selectedData.forEach(wasteData => {
                        console.log('wasteData', wasteData);
                        console.log(wasteData.QLY_DisplayType__c, lst.includes(wasteData.Product__r.QLY_DisplayType__c));
                        if(this.wasteIdsWithRelatedFiles.findIndex(e => e.wasteId == wasteData.Product__c) == -1 && lst.includes(wasteData.Product__r.QLY_DisplayType__c)) {
                            this.wasteIdsWithRelatedFiles.push({wasteId: wasteData.Product__c, filesId:[], wasteName: wasteData.Product__r.Name});
                        }
                    });
                } catch(e) {
                    console.log(e);
                }
            } else {
                this.wasteIdsWithRelatedFiles.push({wasteId: file.prodId, filesId:[file.Id], wasteName: file.prod});
            }
        }

        console.log('pending section...');
        console.log(this.pendingFiles);
        console.log(this.pendingFiles.length);

        if(this.pendingFiles.length === 0) {
            this.pendingFiles.push(file.Id); 
        } else {
            let isIn=false;
            let i=0;
            let lengthPFiles = this.pendingFiles.length;
            while(i<lengthPFiles){
                console.log(i);
                console.log(this.pendingFiles[i]);
                if(this.pendingFiles[i] === file.id) {
                    isIn = true;
                    break;
                }

                if(!isIn) {
                    this.pendingFiles.push(file.Id); 
                }
                i++;
            }
        }


        console.log('lwcQlyMain.handleUpload pendingFiles: '+JSON.stringify(this.pendingFiles));
        console.log('lwcQlyMain.handleUpload wasteIdsWithRelatedFiles: '+JSON.stringify(this.wasteIdsWithRelatedFiles));
        console.log('lwcQlyMain.handleUpload --- end');
    }

    windowsUnload(){
        if(!this.formId && this.pendingFiles.length > 0)
        {
            deleteContentDocuments({ docIds: this.pendingFiles })
            .then(result => {
                console.log('lwcQlyMain.windowsUnload deleteContentDocuments result : ' + JSON.stringify(result));
            })
            .catch(error => {
                console.log('lwcQlyMain.windowsUnload deleteContentDocuments error : ' + JSON.stringify(error));
            });
        }
    }

    //redirect to home page
    selectStep1() {
        this.selectedStep = 'Step1';
    }

    //redirect to prestations Screen
    selectStep2() {
        this.selectedStep = 'Step2';
    }

    //redirect to summary page
    selectStep3() {
        this.selectedStep = 'Step3';
    }

    //redirect to finish page
    selectStep4() {
        this.selectedStep = 'Step4';
    }

    get isSelectStep1() {
        return this.selectedStep === "Step1";
    }

    get isSelectStep2() {

        return this.selectedStep === "Step2";
    }

    get isSelectStep3() {
        return this.selectedStep === "Step3";
    }

    get isSelectStep4() {
        return this.selectedStep === "Step4";
    }


    get isShowBtnPreced() {
        if (this.selectedStep === "Step2" || this.selectedStep === "Step3") {
            return true;
        } else {
            return false;
        }
    }

    get isShowBtnNext() {
        if (this.selectedStep === "Step1" || this.selectedStep === "Step2") {
            return true;
        } else {
            return false;
        }
    }

    get isShowBtnFinish() {
        if (this.selectedStep === "Step3") {
            return true;
        } else {
            return false;
        }
    }
}