import { LightningElement } from 'lwc';
import okImageUrl from '@salesforce/resourceUrl/OkImage';

export default class LwcQlyEndScreen extends LightningElement {
    get okImageUrl() {
        return okImageUrl;
    }

    handleRedirect(event){ 
        console.log('LwcQlyEndScreen.handleRedirect --- start' );       
        //send to main
        const redirectEvent = new CustomEvent(
            "redirecthomepage",{detail: 'step1'}
            );  
            this.dispatchEvent(redirectEvent);
      
     }
}