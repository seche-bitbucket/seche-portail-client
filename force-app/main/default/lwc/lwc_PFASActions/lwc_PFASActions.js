import { LightningElement, api, track } from 'lwc';
import { loadScript } from "lightning/platformResourceLoader";

import jsdict from '@salesforce/resourceUrl/PFAS_Translation'

export default class Lwc_PFASActions extends LightningElement {

    // Event Vars
    @api finalStep
    @api actionsStyle
    @api language
    @track nextStep
    @track previousStep
    @track saveButton
    @track sendButton
    
    handleNext() {
        this.sendEvent('next')
    }

    handleSave() {
        this.sendEvent('save')
    }

    handlePrevious () {
        this.sendEvent('previous')
    }

    sendEvent(eventName) {
        this.dispatchEvent(new CustomEvent(eventName))
    }

    async connectedCallback() {
        await loadScript(this, jsdict+"/PFAS_TranslationDict.js")
        this.DICT = PFAS_Dict.getDict()
        this.nextStep = this.DICT[this.language]?.nextStep
        this.previousStep = this.DICT[this.language]?.previousStep
        this.saveButton = this.DICT[this.language]?.saveButton
        this.sendButton = this.DICT[this.language]?.sendButton
        

    }

}