import { LightningElement, api, track, wire } from 'lwc';
import { getPicklistValues, getObjectInfo } from 'lightning/uiObjectInfoApi';
import QLY_SERVICE_OBJECT from '@salesforce/schema/QLY_Service__c';
import PACKAGING_FIELD from '@salesforce/schema/QLY_Service__c.Packaging__c';
import { showToast } from 'c/ldsUtils';
import {loadStyle} from 'lightning/platformResourceLoader';
import cssOverride from '@salesforce/resourceUrl/T5F_CssOverride';

export default class LwcQlySelectedContainers extends LightningElement {
    @api userSelectedServices;
    @api listSelectedWaste;    
    @api showAction = false;
    @api haveSelectedContainer;   
    @api itemDisabled = false;

    @track keyIndex = 0;
    @track options;
    value = '';

   
     // LWC lifecycle hooks
     connectedCallback() {

        //loadStyle(this, cssOverride);

         if(this.userSelectedServices){           
            console.log('LwcQlySelectedContainers.connectedCallback.this.userSelectedServices.listSelectedContainer ', JSON.stringify(this.userSelectedServices.listSelectedContainer));            
         }
       
    }

    get haveContainer (){
        return (this.userSelectedServices.listSelectedContainer.length > 0) ;
    }

    get containerStyle(){
        if(!this.itemDisabled){
            return 'container';
        }else{
            return 'container-summary';
        }
    }

    handleUserInputChange(event) {
        const field = event.target.name;          
        const fieldType = event.target.dataset.type;
        const productName =  event.target.dataset.product;
        const currentIndex =  event.target.dataset.index;
       
        //to notify where the event fire from
        const eventOrigin = 'fromSelectedContainer';

        console.log('LwcQlySelectedContainers.handleUserInputChange.field === ', field);        
        console.log('LwcQlySelectedContainers.handleUserInputChange.fieldType === ', fieldType); 
        
        if (fieldType === 'checkbox') { //fieldType == checkbox          
            const inputValue = event.target.checked; 
            console.log('LwcQlySelectedContainers.handleUserInputChange.inputValue === ', inputValue);           
            let userInputValue = { field: field, inputValue: inputValue, fieldType: fieldType, eventOrigin :eventOrigin, productName : productName, currentIndex : currentIndex};
            const userInputEvent = new CustomEvent(
                "userinputvalue", { detail: userInputValue }
            );
            this.dispatchEvent(userInputEvent);
           
        } else { //fieldType input
             const inputValue = event.target.value; 
             //user can't input more 20 caisses palette 
            if (productName.includes("Caisse palette") && field == "numberContainer" && inputValue > 10) {
                this.dispatchEvent(showToast('warning', 'dismissable', undefined, 'Vous ne pouvez pas saisir plus de 10 caisses palettes'));
                event.target.value = 10;
                let userInputValue = { field: field, fieldType: fieldType, inputValue: event.target.value, eventOrigin :eventOrigin, productName : productName, currentIndex : currentIndex };
                const userInputEvent = new CustomEvent(
                    "userinputvalue", { detail: userInputValue }
                );
                this.dispatchEvent(userInputEvent); 
            } else{
                console.log('LwcQlySelectedContainers.handleUserInputChange.inputValue === ', inputValue); 
                let userInputValue = { field: field, fieldType: fieldType, inputValue: inputValue, eventOrigin :eventOrigin, productName : productName, currentIndex : currentIndex };
                const userInputEvent = new CustomEvent(
                    "userinputvalue", { detail: userInputValue }
                );
                this.dispatchEvent(userInputEvent);       
            }           
               
        }

    }

    
}