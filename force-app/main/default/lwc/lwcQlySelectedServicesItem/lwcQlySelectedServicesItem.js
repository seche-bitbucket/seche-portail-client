import { api, LightningElement, track } from 'lwc';

export default class LwcQlySelectedServicesItem extends LightningElement {
    @api item;
    @api itemDisabled;
    @api formId;
    

    DEFAULT_DISPLAY = 'Normal';
    PHOTOS_QUANTITY = 'Quantite_Photos';
    PHOTOS_QUANTITY_LIST = 'Quantite_Liste_Photos';
    PEROXYDE_CLASSIFICATION_QUANTITY_PHOTOS ='Peroxyde_Classification_Quantite_Photos';

     // LWC lifecycle hooks
     connectedCallback() {       
           console.log('LwcQlySelectedServicesItem.connectedCallback.item', JSON.stringify(this.item));       
    }       
        
    get defaultDisplayStyle(){
        return (this.item.displayStyle === this.DEFAULT_DISPLAY);
    }

    get photoQtyDisplayStyle(){
        return (this.item.displayStyle === this.PHOTOS_QUANTITY);
    }

    get photoQtyListDisplayStyle(){
        return (this.item.displayStyle === this.PHOTOS_QUANTITY_LIST);
    }

    get peroxydeClassDisplayStyle(){
        return (this.item.displayStyle === this.PEROXYDE_CLASSIFICATION_QUANTITY_PHOTOS);
    }

    //Get method use to return greeting message.
    @api
    get currentIndex(){
        return this._currentIndex;
    }

    //Set method use to setup greeting message in upper case.
    set currentIndex(value){
        this._currentIndex = value;
    }
   

    handleRowAction(event) {       
        console.log('LwcQlySelectedServicesItem.handleRowAction event.detail.name start ---', event.detail.name);
        console.log('LwcQlySelectedServicesItem.handleRowAction event.detail.action start ---', event.detail.action);
        console.log('LwcQlySelectedServicesItem.handleRowAction event.detail.currentIndex start ---', event.detail.currentIndex);
        console.log('LwcQlySelectedServicesItem.handleRowAction this.item.unitPrice start ---',  this.item.rows[0].unitPrice);  

        console.log(event.detail.packaging); 
        console.log(event.detail.options);  

        const productName =  this.item.name;
        const action = event.detail.action;
        const field = event.detail.name;
        const currentIndex = event.detail.currentIndex;
        const unitPrice = this.item.rows[0].unitPrice;

        let processAction = { productName : productName, name: field, action : action, currentIndex: currentIndex, unitPrice:unitPrice, packaging: event.detail.packaging, options: event.detail.options};
        const rowaction = new CustomEvent(
            "rowaction", { detail: processAction }
        );
        this.dispatchEvent(rowaction);

    }

    handleAddRow(event) {     

        console.log('LwcQlySelectedServicesItem.handleAddRow.event.currentTarget.dataset.productName start ---', event.currentTarget.dataset.productName);
        console.log('LwcQlySelectedServicesItem.handleAddRow. event.target.name start ---',  event.target.name);            
        const productName= event.currentTarget.dataset.productName;
        const unitPrice = this.item.unitPrice;
        const action = 'add';
        const field = event.target.name;

        let removeAction = { productName : productName, name: field, action : action, unitPrice: unitPrice};
        const rowaction = new CustomEvent(
            "rowaction", { detail: removeAction }
        );
        this.dispatchEvent(rowaction);
       
    }

    handleUpload(event){

        console.log('LwcQlySelectedServicesItem.handleUpload event.detail: '+JSON.stringify(event.detail.pendingFilesIds));

        const pendingFilesIds =  event.detail.pendingFilesIds;

        let processAction = { pendingFilesIds : pendingFilesIds };
        const uploadfiles = new CustomEvent(
            "uploadfiles", { detail: processAction }
        );
        this.dispatchEvent(uploadfiles);

    }

    handleRemoveRow(event) {
        if (this.itemList.length >= 2) {
            this.itemList = this.itemList.filter(function (element) {
                return parseInt(element.id) !== parseInt(event.target.accessKey);
            });
        }
        console.log('LwcQlySelectedServicesItem.handleAddRow.event.currentTarget.dataset.productName start ---', event.currentTarget.dataset.productName);
        console.log('LwcQlySelectedServicesItem.handleAddRow. event.target.name start ---',  event.target.name);       
        const productName= event.currentTarget.dataset.productName;
        const action = 'remove';
        const field = event.target.name;

        let removeAction = { productName : productName, name: field, action : action};
        const rowaction = new CustomEvent(
            "rowaction", { detail: removeAction }
        );
        this.dispatchEvent(rowaction);
    }

    handleUserInputChange(event) {
        console.log('LwcQlySelectedServicesItem.handleUserInputChange. start ---', event.detail.productName);
        console.log('LwcQlySelectedServicesItem.handleUserInputChange. event.target.name; start ---',  event.detail.field);
        console.log('LwcQlySelectedServicesItem.handleUserInputChange. event.target.value start ---',event.detail.inputValue);
        console.log('LwcQlySelectedServicesItem.handleUserInputChange.displayStyle start ---', event.detail.displayStyle);
        console.log('LwcQlySelectedServicesItem.handleUserInputChange.currentIndex start ---', event.detail.currentIndex);
        console.log('LwcQlySelectedServicesItem.handleUserInputServiceChange.eventOrigin start ---', event.detail.eventOrigin);

        const productName =  event.detail.productName;
        const field = event.detail.field;       
        const displayStyle = event.detail.displayStyle;
        const currentIndex = event.detail.currentIndex;  
        const inputValue = event.detail.inputValue;   
        const eventOrigin = event.detail.eventOrigin;

        let userInputValue = { field: field, inputValue: inputValue, displayStyle : displayStyle, currentIndex : currentIndex, productName : productName, eventOrigin : eventOrigin};
        const userInputEvent = new CustomEvent(
            "userinputvalue", { detail: userInputValue }
        );
        this.dispatchEvent(userInputEvent);

    }
   
}