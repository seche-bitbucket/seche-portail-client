import { LightningElement, api, track } from 'lwc';

export default class LwcVisualPicker extends LightningElement 
{
    @api show = false;
    @api header;
    @api options;
    @api size = 'small';  
    @api multiSelect = false;
    @api context;
    @api userSelectedServices;
    @api itemDisabled = false;

    @track items = [];
    @track pickerClass;
    @track pickerType;
    @track labelCss;

    selectedValue;

     // LWC lifecycle hooks
     connectedCallback() {
        console.log('LwcVisualPicker.connectedCallback.listSelectedPresta start ',JSON.stringify(this.userSelectedServices.listSelectedPresta));
        console.log('LwcVisualPicker.connectedCallback.options start ',JSON.stringify(this.options));
     }
     
    @api
    uncheck(id) {
        this.template.querySelector('[data-product-id="'+id+'"]').checked = false;
    }

    renderedCallback() {      
        if (this.show) {
            if (this.multiSelect) {
                this.selectedValue = [];
            }
            //set style & size of picker
            this.pickerClass = 'slds-visual-picker  lds-visual-picker_' + this.size;
            this.labelCss = 'slds-visual-picker__figure slds-visual-picker__text slds-align_absolute';

            //set type of picker
            if (this.multiSelect) {
                this.pickerType = 'checkbox';
            } else {
                this.pickerType = 'radio';
            }

            if(this.itemDisabled){ //use selected prestations by the user to display in the summary component (next component)
                if (this.options.length > 0  && this.options.length != this.items.length) {
                    let iterator = 0;
                    let items = [];
                    this.options.forEach(option => {
                        let item = { 
                        id : option.id,
                        name : option.name,
                        unitPrice : option.UnitPrice,
                        serviceIcon: option.serviceIcon,
                        isSelected : option.isSelected,
                        disabled : this.itemDisabled }
                        items.push(item);
                        iterator++;   
                        console.log('LwcVisualPicker.renderedCallback.item start ',JSON.stringify(item));                
                    });
                    this.items = items;
                    console.log('LwcVisualPicker.renderedCallback.items for summary ',JSON.stringify(items));
                }
            }else{ //use to display all available prestations(prestation pico menu on the haeader) 
                if (this.options.length > 0
                    && this.options.length != this.items.length) {
                    let iterator = 0;
                    let items = [];
                    this.options.forEach(option => {
                        let item = { ...option };
                        item.id = option.Product2Id;
                        item.name = option.Name;
                        item.unitPrice = option.UnitPrice;
                        item.disabled = this.itemDisabled;
                        items.push(item);
                        iterator++;   
                        //console.log('LwcVisualPicker.renderedCallback.options start -- ',JSON.stringify(this.options));  
                        console.log('LwcVisualPicker.renderedCallback.item start -- ',JSON.stringify(item));               
                    });
                    this.items = items;
                    console.log('LwcVisualPicker.renderedCallback.items start -- ',JSON.stringify(this.items)); 
                }
            }
           
        }
        //restore selected prestations
        this.setSelectedPresta(this.userSelectedServices.listSelectedPresta);
    }

    get isSelected(){
        if(this.userSelectedServices.listSelectedPresta.some(prest => prest.name === "Peter")){
            alert("Object found inside the array.");
        } else{
            alert("Object not found.");
        }
    }

    // methode to restore selected prestations by user
    setSelectedPresta(listSelectedItems) {
        let i;
        let checkboxes = this.template.querySelectorAll('[data-id="checkbox"]')
        for(i=0; i<checkboxes.length; i++) {
            console.log(' checkboxes[i] ---',  checkboxes[i]);
            console.log(' listSelectedItems[i] ---',  JSON.stringify(listSelectedItems));
            //check if current prestation is already selected by user
            console.log('this.userSelectedServices.listSelectedPresta.some ---', listSelectedItems.some(prest => prest.id.includes(checkboxes[i].id)));
            checkboxes[i].checked = listSelectedItems.some(prest =>  prest.name === checkboxes[i].name);
        }
    }


    handleSelectedPrestation(event) {
        console.log('LwcVisualPicker.handleSelectedServices.event.target.id start ---', event.target.id);
        console.log('LwcVisualPicker.handleSelectedServices.event.target.productId start ---', event.target.dataset.productId);
        console.log('LwcVisualPicker.handleSelectedServices.event.target.unitPrice start ---', event.target.dataset.price);
        console.log('LwcVisualPicker.handleSelectedServices.event.target.name start ---', event.target.name);
        console.log('LwcVisualPicker.handleSelectedServices.event.target.checked start ---', event.target.checked);
        console.log('LwcVisualPicker.handleSelectedServices.event.target.dataset.request ---',  event.target.dataset.request);
        console.log('LwcVisualPicker.handleSelectedServices.event.target.dataset.serviceIcon ---',  event.target.dataset.icon);

        const id = event.target.id;
        const field = event.target.name;
        const unitPrice = event.target.dataset.price;
        const inputValue = event.target.checked;
        const requestType = event.currentTarget.dataset.request;
        const serviceIcon = event.target.dataset.icon;
        //TODO Why sometimes we have -xx number after the productId ? (it's why we split de productId)
        const productId = event.target.dataset.productId.split('-')[0];

        let wasteInputValue = { id : id, productId : productId, name: field, unitPrice : unitPrice, inputValue: inputValue, request: requestType, serviceIcon : serviceIcon};
        const wasteInputEvent = new CustomEvent(
            "selectedwastevalue", { detail: wasteInputValue }
        );
        this.dispatchEvent(wasteInputEvent);
        //sendDataToParent();


    }
  
}