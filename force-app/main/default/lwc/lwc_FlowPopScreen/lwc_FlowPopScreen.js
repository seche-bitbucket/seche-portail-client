import { LightningElement, api, track } from 'lwc';
import {
    FlowNavigationNextEvent,
    FlowNavigationBackEvent
} from 'lightning/flowSupport';

export default class Lwc_FlowPopScreen extends LightningElement {

    @api popHead;
    @api message;
    @api okAction;
    @api cancelAction;
    @api backAction;
    @api state;
    @api labelOk
    @api labelCancel
    @api labelBack

    connectedCallback() {
        if(!this.labelOk) {
            this.labelOk = 'Oui'
        }

        if(!this.labelCancel) {
            this.labelCancel = 'Non'
        }
    }

    handleClick(event) {
        this.state = event.target.dataset.action
        if(event.target.name == 'back') {
            const navigateEvent = new FlowNavigationBackEvent();
            this.dispatchEvent(navigateEvent);
        } else {
            const navigateEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateEvent);
        }
    }

}