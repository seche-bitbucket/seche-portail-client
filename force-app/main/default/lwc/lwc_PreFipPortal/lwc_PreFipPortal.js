import { LightningElement, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';

export default class Lwc_PreFipPortal extends LightningElement {

    @track GUID;

    @wire(CurrentPageReference)
    getStateParameters(currentPageReference) {
       if (currentPageReference) {
          this.urlStateParameters = currentPageReference.state
          this.setParametersBasedOnUrl()
       }
    }

    setParametersBasedOnUrl() {
        this.GUID = this.urlStateParameters.recordId || null
    }

}