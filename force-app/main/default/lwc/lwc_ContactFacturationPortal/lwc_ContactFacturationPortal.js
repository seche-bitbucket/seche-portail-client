import { LightningElement, track, wire } from 'lwc'
import { CurrentPageReference } from 'lightning/navigation'

export default class Lwc_ContactFacturationPortal extends LightningElement {

    @track guid

    @wire(CurrentPageReference)
    getStateParameters(currentPageReference) {
        if(currentPageReference) {
            this.urlStateParameters = currentPageReference.state
            this.setParametersBasedOnUrl()
        }
    }

    setParametersBasedOnUrl() {
        this.guid = this.urlStateParameters.recordId || null
    }
}