({
    
    ////////////////////////////////
    // INIT DATA ON PAGE LOADING //
    //////////////////////////////
    
    //* FIRST FUNCTION GET BOOLEAN IF LIST OF QUOTE LINE ITEM NEEDS TO BE DISPLAYED*//
    startInit : function(component) {
        // Get a reference to the getPriceBookEntries() function defined in the Apex controller
        var action = component.get("c.CheckQuoteStatus");
        action.setParams({
            "wasteInstanceId": component.get("v.recordId")
        });
        // Register the callback function
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.displayQuote", response.getReturnValue());
                if(response.getReturnValue() === true){
                    this.getQuoteLines(component);
                }
            }            
        });
        // Invoke the service
        $A.enqueueAction(action);
    },
    
    //* SECOND FUNCTION GET LIST OF QUOTE LINE ITEM THAT NEEDS TO BE DISPLAYED*//
    getQuoteLines : function(component) {
        // Get a reference to the getPriceBookEntries() function defined in the Apex controller
        var action = component.get("c.getQuoteLineItems");
        action.setParams({
            "wasteInstanceId": component.get("v.recordId")
        });
        // Register the callback function
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.QuoteLine", response.getReturnValue());
                
                //Call 2nd method
                this.getTotalPrice(component);
            }
            
        });
        // Invoke the service
        $A.enqueueAction(action);
        
    },
    
    //* THIRD FUNCTION GET THE TOTAL PRICE OF THE QUOTE *//
    getTotalPrice : function(component) {
        // Get a reference to the getPriceBookEntries() function defined in the Apex controller
        var action = component.get("c.GetTotalPrice");
        action.setParams({
            "wasteInstanceId": component.get("v.recordId")
        });
        // Register the callback function
        action.setCallback(this, function(response) {

            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.QuoteTotalPrice", response.getReturnValue());
            }
                            //Call 3rd method
                this.QuoteType(component);
        });
        // Invoke the service
        $A.enqueueAction(action);
    },
    
	//* FOURTH FUNCTION GET BOOLEAN TO DISPLAY OR NOT THE ELIMINATION BUTTON OPTION*//    
    QuoteType : function(component) {
        var action = component.get("c.getQuoteType");
        action.setParams({
            "wasteInstanceId" : component.get("v.recordId")
        });
        // set call back 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() === 'Destruction'){
                    component.set("v.displayEliminationButton", false);
                }else{
                    component.set("v.displayEliminationButton", true);
                }
            }
        });
        
        // Invoke the service
        $A.enqueueAction(action);
    },


	 ConfirmAcceptation : function(component) {
        
            if (confirm('Etes-vous sûr de vouloir accepter le devis ?')) {
                this.ConfirmAccepted(component);
            }  
    },
       
    
    ConfirmAccepted : function(component) {
        var action = component.get("c.AcceptQuoteController");
        action.setParams({
            "wasteInstanceId" : component.get("v.recordId")
        });
        // set call back 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() === true) {
                alert('Devis validé avec succès');
            }
        });
        
        // Invoke the service
        $A.enqueueAction(action);
    },
    
    ConfirmDecline : function(component) {
        
            if (confirm('Etes-vous sûr de vouloir refuser le devis ?')) {
                this.ConfirmDeclined(component);
            }   
    },
       
    ConfirmDeclined : function(component) {
        
        var action = component.get("c.DeclineQuoteController");
        action.setParams({
            "wasteInstanceId" : component.get("v.recordId")
        });
        // set call back 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() === true) {
                alert('Devis refusé avec succès');
            }
        });
        
        // Invoke the service
        $A.enqueueAction(action);
    },
    
    
    ConfirmElimination : function(component) {
        
        if (confirm('Etes-vous sûr de vouloir envoyer le transformateur en élimination ?')) {
            this.EliminationConfirmed(component);
        }   
    },
    
    EliminationConfirmed : function(component) {
        
        var action = component.get("c.SendToEliminationController");
        action.setParams({
            "wasteInstanceId" : component.get("v.recordId")
        });
        
        // set call back 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() === true) {
                alert('Tranformateur envoyé en élimination avec succès');
            }
        });
        
        // Invoke the service
        $A.enqueueAction(action);
    }  
  
})