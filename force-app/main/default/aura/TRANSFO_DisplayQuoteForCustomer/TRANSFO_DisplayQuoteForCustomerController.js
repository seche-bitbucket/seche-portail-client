({
    
    // function call on component Load
    doInit: function(component, event, helper) {
        
        helper.startInit(component);
        
        
    },
    
    AcceptQuote: function(component, event, helper) {
       
        helper.ConfirmAcceptation(component);
       
    },
    
    DeclineQuote: function(component, event, helper) {
        helper.ConfirmDecline(component);
    },
    
    SendToElimination: function(component, event, helper) {
        
        helper.ConfirmElimination(component);
       
    }
})