({
  // function automatic called by aura:waiting event
  showSpinner: function (component, event, helper) {
    // make Spinner attribute true for displaying loading spinner
    component.set("v.spinner", true);
  },

  // function automatic called by aura:doneWaiting event
  hideSpinner: function (component, event, helper) {
    // make Spinner attribute to false for hiding loading spinner
    component.set("v.spinner", false);
  },

  //Method to get report datas
  getReportData: function (component, event, helper, InputData) {

    try{
      //List to store the report rows
      var listRowReportData = [];
      var columns = [];
      var columnsEuro = [];
      var tempColumns = []; // create a temp to store names only
      //List to store the report rows
      var listRowReportDataResult = [];
      var listRowReportDataResultEuro = [];
      var accountsIds = [];

      //get the comment on the contract throw the accountId (we have already set the map)
      var commentMap = component.get("v.mapContractCommentByAccountId");
      console.log("comments map => "+ commentMap.size);

      //get keys element
      var filiale = component.get("v.filiale");
      var filiere = component.get("v.filiere");
      var idSalesRep = component.get("v.userId");
      let faf = component.get("v.faf");

      var contractStatus;
      var url;
    
      console.log('InputData => '+JSON.stringify(InputData.groupingsDown.groupings));

      if (InputData.groupingsDown.groupings) {
        columns.push({value:"Nom du client",isSortable:true, class: 'slds-is-sortable slds-cell-wrap caption'});
        columnsEuro.push({value:"Nom du client",isSortable:true, class: 'slds-is-sortable slds-cell-wrap caption'});
        if((component.get("v.faf"))) {
          columns.push({value:"Statut",isSortable:true, class: 'slds-is-sortable slds-cell-wrap caption'});
          columnsEuro.push({value:"Statut",isSortable:true, class: 'slds-is-sortable slds-cell-wrap caption'});
        }
        //iterate we only have one group we filter in advance for faf or not
        for (var i = 0; i < InputData.groupingsDown.groupings.length; i++) {
          //for each group iterate on accounts items
          //object to store row report Data by Account
          var tempRowReportData = [];

          //for each account item to get values on all Type (nature : Traitement, prestation, conditionnement)
          var key = InputData.groupingsDown.groupings[i].key;
          var accountId =
            InputData.groupingsDown.groupings[i].value;
          var accountName =
            InputData.groupingsDown.groupings[i].label;

          // handle second grouping TECH Status
          contractStatus = (InputData.groupingsDown.groupings[i].groupings != null) ? InputData.groupingsDown.groupings[i].groupings[0].value+'-icon' : null;

          tempRowReportData.push(accountId);
          console.log("ID "+accountId+" Name "+accountName+" Status "+contractStatus);
          tempRowReportData.push(accountName);
          //set the grouping of the contact (Hors faf/ faf)

          tempRowReportData.push(contractStatus);
          
          console.log('groupingsAcross => '+JSON.stringify(InputData.groupingsAcross.groupings));

          for (var k = 0; k < 4; k++) {

            console.log('groupingsAcross['+k+'] => '+JSON.stringify(InputData.groupingsAcross.groupings[k]));

            //InputData.groupingsAcross.groupings.length = 3 (we must always have three groupings nature(Presta, Trait, Condi) but no sometime because of the report dynamic grouping. So we hard code the value to lenght 3 to ensure this requirement  )
            //set Nature dynamically for the columns header
            if (InputData.groupingsAcross.groupings[k]) {
              let nature =
                "% Aug " +
                InputData.groupingsAcross.groupings[k].label.substring(0, 5);
              //set the report nature
              if (!tempColumns.includes(nature)) { //now compares to tempColumns
                columns.push({value:nature, isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
                tempColumns.push(nature); //and also push to the tempColumns
              }

              let natureEuro =
                "€ Aug " +
                InputData.groupingsAcross.groupings[k].label.substring(0, 5);
              //set the report natureEuro
              if (!tempColumns.includes(natureEuro)) { //now compares to tempColumns
                columnsEuro.push({value:natureEuro, isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
                tempColumns.push(natureEuro); //and also push to the tempColumns
              }

            } else {
              //Add missing nature name in columns list
              if (!tempColumns.includes("€ Aug Prest")) {
                columnsEuro.push({value:"€ Aug Prest", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
               
                tempColumns.push("€ Aug Prest");
              }
              if (!tempColumns.includes("% Aug Prest")) {
                columns.push({value:"% Aug Prest", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
               
                tempColumns.push("% Aug Prest");
              }

              if (!tempColumns.includes("€ Aug Trait")) {
                columnsEuro.push({value:"€ Aug Trait", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
                tempColumns.push("€ Aug Trait");
              }
              if (!tempColumns.includes("% Aug Trait")) {
                columns.push({value:"% Aug Trait", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
                tempColumns.push("% Aug Trait");
              }

              if (!tempColumns.includes("€ Aug Condi")) {
                columnsEuro.push({value:"€ Aug Condi", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
                tempColumns.push("€ Aug Condi");
              }
              if (!tempColumns.includes("% Aug Condi")) {
                columns.push({value:"% Aug Condi", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
                tempColumns.push("% Aug Condi");
              }

              if (!tempColumns.includes("€ Aug Valor")) {
                columnsEuro.push({value:"€ Aug Valor", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
                tempColumns.push("€ Aug Valor");
              }
              if (!tempColumns.includes("% Aug Valor")) {
                columns.push({value:"% Aug Valor", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
                tempColumns.push("% Aug Valor");
              }
            }

            //get values on all Type (nature :Traitement, prestation, conditionnement)
            for (var q = 0; q < 4; q++) {
              //InputData.groupingsAcross.groupings.length = 3 (we must always have three groupings nature(Presta, Trait, Condi) but no sometime because of the report dynamic grouping. So we hard code the value to lenght 3 to ensure this requirement  )
              if (
                InputData.factMap[key + "!" + q] &&
                InputData.factMap[key + "!" + q].aggregates
              ) {
                for (
                  var l = 0;
                  l < InputData.factMap[key + "!" + q].aggregates.length;
                  l++
                ) {
                  // reportRowDataObject.colum  = InputData.factMap[key + "!"+k].aggregates[l].value;
                  let val =
                    InputData.factMap[key + "!" + q].aggregates[l].value != null
                      ? InputData.factMap[key + "!" + q].aggregates[l].value
                      : "";
                  tempRowReportData.push(val);
                }
              } else {
                // no value from the report for others nature grouping then init with blank
                for (var p = 0; p < 7; p++) {
                  //p <5 because we have always 6 columns by nature
                  tempRowReportData.push("");
                }
              }

              console.log('tempRowReportData => '+JSON.stringify(tempRowReportData));

            }

            //get Total for ecah row(Account) of the report
            //get Total for ecah row(Account) of the report
            //get Total for ecah row(Account) of the report
            for (
              var t = 0;
              t < InputData.factMap[key + "!T"].aggregates.length;
              t++
            ) {
              for (
                var t = 0;
                t < InputData.factMap[key + "!T"].aggregates.length;
                t++
              ) {
                for (
                  var t = 0;
                  t < InputData.factMap[key + "!T"].aggregates.length;
                  t++
                ) {
                  // reportRowDataObject.colum  = InputData.factMap[key + "!"+k].aggregates[l].value;
                  let val =
                    InputData.factMap[key + "!T"].aggregates[t].value != null
                      ? InputData.factMap[key + "!T"].aggregates[t].value
                      : "";
                  tempRowReportData.push(val);
                }
              }
            }

            if (commentMap) {
              let comTemp =
                commentMap.get(accountId + filiale + filiere + idSalesRep + faf) != undefined
                  ? commentMap.get(accountId + filiale + filiere + idSalesRep + faf)
                  : "";
              tempRowReportData.push(comTemp);
              console.log("Comments temp=> "+commentMap.get(accountId + filiale + filiere + idSalesRep + faf));
            } else {
              tempRowReportData.push(" ");
            }
          }
          
          listRowReportData.push(tempRowReportData);
        }

        console.log('listRowReportData1', listRowReportData);
        var sousContrat = component.get("v.sousContrat");
        var currentUser = component.get("v.currentUser");
        console.log('currentUser', currentUser);
  
      
        //Set columns
        columns.push({value:"Tonnage N-1", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
        columns.push({value:"Tonnage N", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
        columns.push({value:"CA N", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
        columns.push({value:"CA N+1", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
        columns.push({value:"% Aug globale", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
        if(sousContrat == false ){
          columns.push({value:"Commentaire augmentation", isSortable:false,class: 'slds-is-sortable slds-cell-wrap caption'});
        }
        if (currentUser) {
          if(currentUser.Profile.Name != 'Chargé(e) d\'affaires' && currentUser.Profile.Name !='Responsable commercial'){
            if(sousContrat == false ){
              columns.push({value:"Commentaires de l'approbateur en cas de refus", isSortable:false,class: 'slds-is-sortable slds-cell-wrap caption'});
            }
          }
       } else{
        columns.push({value:"Commentaires de l'approbateur en cas de refus", isSortable:false,class: 'slds-is-sortable slds-cell-wrap caption'});
       }
        component.set("v.columns", columns);

        columnsEuro.push({value:"Tonnage N-1", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
        columnsEuro.push({value:"Tonnage N", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
        columnsEuro.push({value:"CA N", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
        columnsEuro.push({value:"CA N+1", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
        columnsEuro.push({value:"€ Aug globale", isSortable:true,class: 'slds-is-sortable slds-cell-wrap caption'});
        if(sousContrat == false ){
          columnsEuro.push({value:"Commentaire augmentation", isSortable:false,class: 'slds-is-sortable slds-cell-wrap caption'});
        }
        if (currentUser) {
          if(currentUser.Profile.Name != 'Chargé(e) d\'affaires' && currentUser.Profile.Name !='Responsable commercial'){
            if(sousContrat == false ){
              columnsEuro.push({value:"Commentaires de l'approbateur en cas de refus", isSortable:false,class: 'slds-is-sortable slds-cell-wrap caption'});
            }
          }
        }else{
          columnsEuro.push({value:"Commentaires de l'approbateur en cas de refus", isSortable:false,class: 'slds-is-sortable slds-cell-wrap caption'});
        }
        component.set("v.columnsEuro", columnsEuro);
   

      }
      console.log('listRowReportData2', listRowReportData);
     
      //
      console.log("row report datas by account", listRowReportData);
      //Select from report datas specific fields to display(we dont need to display all Report data column)
      //const filedsToDisplay = [1,2,7,17,27,28,29,30,31,32,33];
      //const filedsToDisplay = [1, 2, 7, 12, 17, 22, 23, 24, 25, 26, 27, 28];
      //const filedsFafToSet = [7, 12, 17, 22, 26];

       //before add fild augmentation euro
       var filedsToDisplay =  [1, 2, 8, 15, 22, 29, 31, 32, 33, 34 ,36 ,38];
                            
       //after add fild augmentation euro
       var filedsToDisplayEuro = [1, 2, 7, 14, 21, 28, 31, 32, 33, 34, 35, 38];

      
      if(sousContrat == true){
        filedsToDisplay =  [1, 2, 8, 15, 22, 29, 31, 32, 33, 34 ,36];
        filedsToDisplayEuro = [1, 2, 7, 14, 21, 28, 31, 32, 33, 34, 35];
      }
   
  
      const filedsFafToSet = [8, 13, 18, 23];
      for (var i = 0; i < listRowReportData.length; i++) {
        var tempRowReport = [];
        var tempRowReportEuro = [];
        console.log('Test----listRowReportData', listRowReportData)
        for (var j = 0; j < listRowReportData[i].length; j++) {
          // get the contract category (hors faf / faf )
          let cat = listRowReportData[i][2];
          if (filedsToDisplay.includes(j)) {
            //fields hors faf initialize with NC/ FAF
            if (cat == "Faf" && filedsFafToSet.includes(j)) {
              //faf fields
              // if (j == 27) {
              //   tempRowReport.push("FAF");
              // } else {
              //   tempRowReport.push("NC");
              // }
            } else {
              tempRowReport.push(listRowReportData[i][j]);
            }
          }

          if (filedsToDisplayEuro.includes(j)) {
            //fields hors faf initialize with NC/ FAF
            if (cat == "Faf" && filedsFafToSet.includes(j)) {
              //faf fields
              // if (j == 27) {
              //   tempRowReport.push("FAF");
              // } else {
              //   tempRowReport.push("NC");
              // }
            } else {
              tempRowReportEuro.push(listRowReportData[i][j]);
            }
          }
        }
        listRowReportDataResult.push({url: 'https://'+component.get("v.instance")+'/'+listRowReportData[i][0], data: tempRowReport, class: listRowReportData[i][2].substr(0, listRowReportData[i][2].indexOf('-'))});
        listRowReportDataResultEuro.push({url: 'https://'+component.get("v.instance")+'/'+listRowReportData[i][0], data: tempRowReportEuro, class: listRowReportData[i][2].substr(0, listRowReportData[i][2].indexOf('-'))});
        accountsIds.push(listRowReportData[i][0])
      }

      console.log("row to display listRowReportDataResultEuro", listRowReportDataResultEuro);
    } catch(e) {
      console.log(e);
    }

    component.set("v.accountsIds", accountsIds)
    return [listRowReportDataResult, listRowReportDataResultEuro] ;
  },

  setMapContractCommentByAccountId: function (component, event, helper){
    console.log(" ");
  },

  getAllReportsData: function (component, event, helper) {
    helper.showSpinner(component, event, helper);
    //set accounts comments
    var dataRows = [];
    var associatedSalesRep = component.get("v.userId");
    var associatedSalesRepName = component.get("v.userName");
    var filiale = component.get("v.filiale");
    var filiere = component.get("v.filiere");
    var faf = component.get("v.faf");
    var sousContrat = component.get("v.sousContrat");
    console.log("faf type: "+faf+" "+typeof faf);
    
    var currentUser = component.get("v.currentUser");

    //Set report's API names to display
    var lstReportName = ["Approb_12_Synthese_8LA"];
    var action = component.get("c.getReportData");
    action.setParams({
      lstReportName: lstReportName,
      associatedSalesRep: associatedSalesRepName,
      filiale: filiale,
      filiere: filiere,
      faf: faf,
      sousContrat: sousContrat
    });

    action.setCallback(this, function (response) {
      var state = response.getState(); 
      var errors = response.getError();

      if (state === "SUCCESS") {
        var reportsResultDatas = response.getReturnValue();

        console.log("reportsResultDatas ====" + reportsResultDatas);
        // Iterate to get report Datas
        reportsResultDatas.forEach(function (rep) {
          var chartInputData = JSON.parse(rep);
          var reportDevName = chartInputData.reportMetadata.developerName;
          //helper.setMapContractCommentByAccountId(component,event,helper);
          var arrayListReportDataRows = helper.getReportData(
            component,
            event,
            helper,
            chartInputData
          );
          var listReportDataRows = arrayListReportDataRows[0];
          var listReportDataRowsEuro = arrayListReportDataRows[1];
          // var listrep = [];
          // for(var i=0; i<listReportDataRows.length; i++){
          //   if(faf){
          //     if(listReportDataRows[i][1] == "Faf"){
          //       console.log("I'm in");
          //       listrep.push(listReportDataRows[i]);
          //     }
          //   }else{
          //     if(listReportDataRows[i][1] == "horsFaf"){
          //       console.log("I'm out");
          //       listrep.push(listReportDataRows[i]);
          //     }
          //   }
          // }

          component.set("v.listOfListDataRows", listReportDataRows);
          component.set("v.listOfListDataRowsEuro", listReportDataRowsEuro);
            
          console.log("listReportDataRowsEuro ====", listReportDataRowsEuro);

          if(component.get("v.isDisabled") != undefined) {
            if(component.get("v.listOfListDataRows").length > 0 || component.get("v.listOfListDataRowsEuro").length > 0) {
              for(let i in listReportDataRows){
                if(listReportDataRows[i].data[1].toLowerCase().includes('soumis')) {
                  component.set("v.isDisabled", false);
                  break;
                } else {
                  component.set("v.isDisabled", true);
                }
              }

              for(let i in listReportDataRowsEuro){
                if(listReportDataRowsEuro[i].data[1].toLowerCase().includes('soumis')) {
                  component.set("v.isDisabled", false);
                  break;
                } else {
                  component.set("v.isDisabled", true);
                }
              }

            } else {
              component.set("v.isDisabled", true);
            }

            if (currentUser) {
              if(currentUser.Profile.Name.includes('Assistante') ){
                component.set("v.isDisabled", true);
              }
            }
          }

          console.log(
            ' component.set("v.listOfListDataRows") --> ',
            JSON.stringify(component.get("v.listOfListDataRows"))
          );
          let columns = component.get("v.columns");
          let columnsEuro = component.get("v.columnsEuro");
          console.log(' component.set("v.columns") --> ', JSON.stringify(columns));
          console.log(' component.set("v.columnsEuro") --> ', JSON.stringify(columnsEuro));
        });
        // helper.generateDataTable(component, event, helper, listOfListDataRows);
      
      } else {
        console.log("error calling action --> getReportData");
      }

      helper.hideSpinner(component, event, helper);
      console.log("sorted: "+localStorage.getItem('sorted'));
      if(localStorage.getItem('sorted') == 'true'){
          (component.get("v.faf")) ? component.set("v.sortDirection", localStorage.getItem('sortDirectionFaf')) : component.set("v.sortDirection",localStorage.getItem('sortDirectionStd'));
          var sortedBy = (component.get("v.faf")) ? parseInt(localStorage.getItem('sortedByFaf')) : parseInt(localStorage.getItem('sortedByStd'));
          console.log('sortedBy => '+sortedBy);
          helper.handleSort(component, event, sortedBy);
      }
    });
    $A.enqueueAction(action);
  },

  validate: function (component, event, helper) {
    var faf = component.get("v.faf");
    var accountsIds = component.get("v.selectedAccountsIds")
    component.set("v.selectedAccountsIds", [])
    if(accountsIds.length == 0 && faf) {
      helper.toastdMessage('Aucun compte sélectionné', 'Veuillez sélectionner au moins un compte à approuver', 'warning')
    } else {
      $A.createComponent(
        "c:LC_validationActionConfirmation_AugTarif",
        {
          previousReference: component.get("v.pageReference"),
          associatedSalesRep: component.get("v.userId"),
          filiale: component.get("v.filiale"),
          filiere: component.get("v.filiere"),
          action: "validate",
          faf: faf,
          accountsIds: accountsIds
        },
        function (modalComponent, status, errorMessage) {
          if (status === "SUCCESS") {
            var body = component.find("showConfirmForm").get("v.body");
            body.push(modalComponent);
            component.find("showConfirmForm").set("v.body", body);
          } else if (status === "INCOMPLETE") {
            console.log("Server issue or client is offline.");
          } else if (status === "ERROR") {
            console.log("error");
          }
        }
      );
    }
  },

  reject: function (component, event, helper) {
    //get input comments
    var listAccountComments = helper.getInputComments(component, event, helper);
    var faf = component.get("v.faf");
    var accountsIds = component.get("v.selectedAccountsIds")
    console.log('listAccountComments',listAccountComments)
    if(accountsIds.length == 0 && faf) {
      helper.toastdMessage('Aucun compte sélectionné', 'Veuillez sélectionner au moins un compte à rejeter', 'warning')
    } else {
      $A.createComponent(
        "c:LC_validationActionConfirmation_AugTarif",
        {
          previousReference: component.get("v.pageReference"),
          associatedSalesRep: component.get("v.userId"),
          filiale: component.get("v.filiale"),
          filiere: component.get("v.filiere"),
          listAccountComments: listAccountComments,
          action: "reject",
          faf: faf,
          accountsIds: accountsIds
        },
        function (modalComponent, status, errorMessage) {
          if (status === "SUCCESS") {
            var body = component.find("showConfirmForm").get("v.body");
            body.push(modalComponent);
            component.find("showConfirmForm").set("v.body", body);
          } else if (status === "INCOMPLETE") {
            console.log("Server issue or client is offline.");
          } else if (status === "ERROR") {
            console.log("error");
          }
        }
      );
    }
  },

  selectAll: function(component, event, helper) {
    let checked = event.target.checked
    let checkboxes = document.getElementsByName('accountCheckbox')
    for(let i = 0; i < checkboxes.length; i++) {
      checkboxes[i].checked = checked
      const e = new Event("change")
      checkboxes[i].dispatchEvent(e)
    }
  },

  selectAccount: function(component, event, helper) {
    let accountsIds = component.get("v.accountsIds")
    let selectedAccountsIds = component.get("v.selectedAccountsIds")
    let acc = accountsIds[event.target.id];
    if(event.target.checked) {
      if(!selectedAccountsIds.includes(acc)) {
        selectedAccountsIds.push(acc)
      }
    } else {
      let index = selectedAccountsIds.indexOf(acc);
      if(index !== -1) {
        selectedAccountsIds.splice(index, 1);
      }
    }
    component.set("v.selectedAccountsIds", selectedAccountsIds)

  },

  handleAvenantsButton: function (component, event, helper) {
    let component_target = event.currentTarget
    let attribute = component_target.dataset.myvalue
    let n = attribute.lastIndexOf('/')
    let accountId = attribute.substring(n + 1)
    let url = 'https://' + component.get("v.instance")+'/lightning/r/Account/' + accountId + '/related/Avenants__r/view'
    // let urlEvent = $A.get("e.force:navigateToURL")
    // urlEvent.setParams({
    //   "url": url
    // })
    // urlEvent.fire()
    window.open(url, '_blank')
  },

  toastdMessage: function (title, message, type) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "pester",
      title: title,
      message: message,
      type: type,
      duration: "2000"
    });
    toastEvent.fire();
  },

  getInputComments: function (component, event, helper) {
    //we have : displayedTableRows.lengt = cmps.lengt
    var displayedTableRows = component.get("v.listOfListDataRows");
    var displayedTableRowsEuro = component.get("v.listOfListDataRowsEuro");
  
    console.log('displayedTableRows', displayedTableRows);
    const cmps = component.find("comment");
    console.log('cmps :'+$A.util.isArray(cmps)+' => '+cmps);
    var mapCommentByAccountName = new Map();
    var i = 0;
    if (!cmps) return;
    if ($A.util.isArray(cmps)) {
      cmps.forEach((cmp) => {
        console.log('cmp.get("v.value") => '+cmp.get("v.value"));
        console.log('displayedTableRows', displayedTableRows[i]);
        let accNameTemp = displayedTableRows[i].data[0];
        let accNameTempEuro = displayedTableRowsEuro[i].data[0];

        //we already have a comment on the Account we add it by concat to the old comment
        if (mapCommentByAccountName.has(accNameTemp)) {
          let existComm = mapCommentByAccountName.get(accNameTemp);
          let newComm = cmp.get("v.value");
          let comm;
          if (existComm) {
            comm = existComm + ". " + newComm;
          } else {
            comm = newComm;
          }

          mapCommentByAccountName.set(accNameTemp, comm);
        } else {
          mapCommentByAccountName.set(accNameTemp, cmp.get("v.value"));
        }

        if (mapCommentByAccountName.has(accNameTempEuro)) {
          let existComm = mapCommentByAccountName.get(accNameTempEuro);
          let newComm = cmp.get("v.value");
          let comm;
          if (existComm) {
            comm = existComm + ". " + newComm;
          } else {
            comm = newComm;
          }

          mapCommentByAccountName.set(accNameTempEuro, comm);
        } else {
          mapCommentByAccountName.set(accNameTempEuro, cmp.get("v.value"));
        }

        i++;
      });
    } else {

      let cmp = cmps;
      console.log('cmp.get("v.value") => '+cmp.get("v.value"));
      let accNameTemp = displayedTableRows[0].data[0];
      let accNameTempEuro = displayedTableRowsEuro[i].data[0];

      //we already have a comment on the Account we add it by concat to the old comment
      if (mapCommentByAccountName.has(accNameTemp)) {
        let existComm = mapCommentByAccountName.get(accNameTemp);
        let newComm = cmp.get("v.value");
        let comm;
        if (existComm) {
          comm = existComm + ". " + newComm;
        } else {
          comm = newComm;
        }

        mapCommentByAccountName.set(accNameTemp, comm);
      } else {
        mapCommentByAccountName.set(accNameTemp, cmp.get("v.value"));
      }

      if (mapCommentByAccountName.has(accNameTempEuro)) {
        let existComm = mapCommentByAccountName.get(accNameTemp);
        let newComm = cmp.get("v.value");
        let comm;
        if (existComm) {
          comm = existComm + ". " + newComm;
        } else {
          comm = newComm;
        }

        mapCommentByAccountName.set(accNameTempEuro, comm);
      } else {
        mapCommentByAccountName.set(accNameTempEuro, cmp.get("v.value"));
      }

      i++;
    }

    console.log("mapCommentByAccountName --- : ", mapCommentByAccountName);

    return Array.from(mapCommentByAccountName);
  },

  /*sortHelper: function (component, event, sortFieldName) {
    var currentDir = component.get("v.arrowDirection");

    if (currentDir == "arrowdown") {
      // set the arrowDirection attribute for conditionally rendred arrow sign
      component.set("v.arrowDirection", "arrowup");
      // set the isAsc flag to true for sort in Assending order.
      component.set("v.isAsc", true);
    } else {
      component.set("v.arrowDirection", "arrowdown");
      component.set("v.isAsc", false);
    }
    // call the onLoad function for call server side method with pass sortFieldName
    this.onLoad(component, event, sortFieldName);
  },*/

  handleSort: function (component, event, sortedBy) {
    var sortDirection = component.get("v.sortDirection");
    var listOfListDataRowsSort = [];
    var listOfListDataRows = component.get("v.listOfListDataRows");
    var listOfListDataRowsSortEuro = [];
    var listOfListDataRowsEuro = component.get("v.listOfListDataRowsEuro");
    if(listOfListDataRows == null) return;
    var parser = (v) => v;
    let sortMult = sortDirection === "asc" ? 1 : -1;
    //sort on 1st array instead
    listOfListDataRowsSort = listOfListDataRows.sort((a, b) => {
      console.log('a', a);
      console.log('b', b);
      let diff = (component.get('v.faf') && sortedBy > 0) ? 1 : 0;
      let a1 = parser(a.data[sortedBy-diff]),
        b1 = parser(b.data[sortedBy-diff]);

      console.log(sortedBy);
      console.log(a1, b1);
      
      var aNaN = isNaN(parseFloat(a1)); //get a1 NaN retrun
      var bNaN = isNaN(parseFloat(b1)); //get b1 NaN retrun
      //check if a1 type is different than b1 type to compare text to numbers
      if (a1 != null && b1 != null
        && ((aNaN && !bNaN) || (!aNaN && bNaN)) // if either is not a number then convert to string
        ){
          console.log(a1+b1);
          a1 = a1.toString();
          b1 = b1.toString();
      }
      let r1 = a1 < b1,
        r2 = a1 === b1;
      return r2 ? 0 : r1 ? -sortMult : sortMult;
    });
    component.set("v.listOfListDataRows", listOfListDataRowsSort);
    
    listOfListDataRowsSortEuro = listOfListDataRowsEuro.sort((a, b) => {
      console.log('a', a);
      console.log('b', b);
      let diff = (component.get('v.faf') && sortedBy > 0) ? 1 : 0;
      let a1 = parser(a.data[sortedBy-diff]),
        b1 = parser(b.data[sortedBy-diff]);

      console.log(sortedBy);
      console.log(a1, b1);
      
      var aNaN = isNaN(parseFloat(a1)); //get a1 NaN retrun
      var bNaN = isNaN(parseFloat(b1)); //get b1 NaN retrun
      //check if a1 type is different than b1 type to compare text to numbers
      if (a1 != null && b1 != null
        && ((aNaN && !bNaN) || (!aNaN && bNaN)) // if either is not a number then convert to string
        ){
          console.log(a1+b1);
          a1 = a1.toString();
          b1 = b1.toString();
      }
      let r1 = a1 < b1,
        r2 = a1 === b1;
      return r2 ? 0 : r1 ? -sortMult : sortMult;
    });
    component.set("v.listOfListDataRowsEuro", listOfListDataRowsSortEuro);
    
    //change the sort direction from desc to asc or asc to desc
    component.set("v.sortDirection", sortDirection === "desc" ? "asc" : "desc");

    //get all element with .sorted-asc to remove the class (resets the arrows)
    var elemsAsc = document.querySelectorAll(".sorted-asc");
    [].forEach.call(elemsAsc, function(el) {
        el.classList.remove("sorted-asc");
    });

    //get all element with .sorted-desc to remove the class (resets the arrows)
    var elemsDesc = document.querySelectorAll(".sorted-desc");
    [].forEach.call(elemsDesc, function(el) {
        el.classList.remove("sorted-desc");
    });

    console.log("sort Dir: "+localStorage.getItem("sortDirectionStd"));
    
    var docColIndex = (sortedBy == 0) ? sortedBy : sortedBy - 1;

    var columns = component.get('v.columns');
    var columnsEuro = component.get('v.columnsEuro');
    if(!isNaN(docColIndex) &&(columns != undefined || columnsEuro != undefined ) && columns.length > 1) {
      console.log('columns[docColIndex]');
      console.log(docColIndex);
      console.log(columns[docColIndex]);
      columns.forEach(c => {
        c.class = 'slds-is-sortable slds-cell-wrap caption';
      })
      if(columnsEuro != undefined){
        columnsEuro.forEach(c => {
         c.class = 'slds-is-sortable slds-cell-wrap caption';
        })}
      

      columns[docColIndex]['class'] = columns[docColIndex]['class']+' colored sorted-'+sortDirection;
      component.set('v.columns', columns);
      console.log(component.get('v.columns'));

      if(columnsEuro != undefined){
      columnsEuro[docColIndex]['class'] = columnsEuro[docColIndex]['class']+' colored sorted-'+sortDirection;
      component.set('v.columnsEuro', columnsEuro);
      console.log(component.get('v.columnsEuro'));
      }

      console.log("sorted By: "+docColIndex);
      console.log("document el : " +document.getElementById(docColIndex));
      console.log("event : " +event);
      //add the class asc/desc to the current element being clicked (column)
      var currentTarget = (event != undefined) ? event.currentTarget : document.getElementById(docColIndex);
      console.log("current target : "+ currentTarget);
      if(currentTarget != undefined) currentTarget.className += ' sorted-'+sortDirection;

      //localStorage.clear();
      localStorage.setItem('sorted', true);
      if(component.get("v.faf")){
        // if(localStorage.getItem('sortDirectionFaf') != undefined) {
        //   localStorage.setItem('sortDirectionFaf', sortDirection.toString());
        //   localStorage.setItem('sortedByFaf', sortedBy.toString());
        // }
        console.log("srt dir"+sortDirection);
        console.log("sort by "+sortedBy);
        localStorage.setItem('sortDirectionFaf', sortDirection != null ? sortDirection.toString() : 'desc');
        localStorage.setItem('sortedByFaf', sortedBy != null ? sortedBy.toString() : '0');
      }else{
        // if(localStorage.getItem('sortDirectionStd') != undefined) {
          //   localStorage.setItem('sortDirectionStd', sortDirection.toString());
          //   localStorage.setItem('sortedByStd', sortedBy.toString());
          // }
          console.log("srt dir"+sortDirection);
          console.log("sort by "+sortedBy);
          localStorage.setItem('sortDirectionStd', sortDirection != null? sortDirection.toString() : 'desc');
          localStorage.setItem('sortedByStd', sortedBy != null ? sortedBy.toString() : '0');
      }
    }
  },

  fireFaceaFaceEvent : function(component, event, helper) {
    var appEvent = $A.get("e.c:LE_ValidationReturnFaceaFace")
    appEvent.fire()
},
});