({
    doInit: function (component, event, helper) {

        
        var hostname = window.location.hostname;
        var arr = hostname.split("/");
        console.log('array-host', arr)
        var instance = arr[0];
        component.set("v.instance", instance);
        
        let status;
       
        console.log('faf => '+component.get("v.disableactions"));
        
        let isDisable;
        
        console.log((component.get("v.listOfListDataRows").length > 0));
        
        /*if(!component.get("v.faf")) {
            status = component.get("v.status");
            isDisable = ((status == 'Valide' ||  status == 'Rejete' || status == 'Brouillon')) ? true : false;
        }*/
        
        console.log('status => '+status);
        
        console.log("Disable => "+isDisable);
        
        helper.getAllReportsData(component,event,helper);
        
        if(component.get("v.disableactions")) {
            component.set("v.isDisabled", true);
        }

        //console.log("sorted: "+localStorage.getItem('sorted'));
        // document.addEventListener('DOMContentLoaded', function(event) 
        window.onload = (event) => {
            console.log("sorted: "+localStorage.getItem('sorted'));
            if(localStorage.getItem('sorted') == 'true'){
                (component.get("v.faf")) ? component.set("v.sortDirection", localStorage.getItem('sortDirectionFaf')) : component.set("v.sortDirection",localStorage.getItem('sortDirectionStd'));
                var sortedBy = (component.get("v.faf")) ? parseInt(localStorage.getItem('sortedByFaf')) : parseInt(localStorage.getItem('sortedByStd'));
                helper.handleSort(component, event, sortedBy);
            }
          };

            //profil Button visible  
            var currentUser = component.get("v.currentUser");
            if (currentUser) {
                console.log('currentUser.Profile.Name'+currentUser.Profile.Name);
                if (currentUser.Profile.Name.includes('Chargé(e) d\'affaires') || currentUser.Profile.Name == 'Responsable commercial') {
                    component.set("v.visibleButton",  false);
                }else{
                  component.set("v.visibleButton",  true);
                }
            }
            
    },

    clickLink: function (component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        var dataObj = component.get("v.listOfListDataRows")[idx];
        let action = component.get("c.getAccountId");
        console.log(dataObj);
        action.setParams({
            "name": dataObj[0]
        });
        var dataId;
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                dataId = response.getReturnValue();
                console.log('url => '+window.location.hostname+'/' + dataId);
                window.open('https://'+window.location.hostname+'/' + dataId, '_blank', focus()); 

            }
        });
        $A.enqueueAction(action);
    },

    selectAll: function (component, event, helper) {
        helper.selectAll(component, event, helper)
    },

    selectAccount: function (component, event, helper) {
        helper.selectAccount(component, event, helper)
    },

    validate: function (component, event, helper) {       
       helper.validate(component, event, helper);      
    },

    reject: function (component, event, helper) {       
        helper.reject(component, event, helper);
    },

    handleAvenantsButton: function(component, event, helper) {
        helper.handleAvenantsButton(component, event, helper);
    },

    sortAccount: function(component, event, helper) {
        // set current selected header field on selectedTabsoft attribute.     
        component.set("v.selectedTabsoft", 'acountName');
        // call the helper function with pass sortField Name   
        helper.handleSort(component, event, 0);
     },

    sortCol: function(component, event, helper){
        var index = parseInt(event.currentTarget.id); // get the id from the element clicked, index is setted in component
        index += 1; // add 1 to the index cause the 2nd index is always "horsFaf"
        helper.handleSort(component, event, index);
    },


    getToggleButtonValue:function(component,event,helper){
       var euro = component.find("augmentationsEuro").get("v.checked")
        component.set("v.augmentationsEuro",  euro);
        component.set("v.selectedAccountsIds", [])
        console.log ( component.get("v.augmentationsEuro"));
        
    },
})