({
    doInit : function(cmp, ev) {
        console.log('doInit START');
        
        var url = window.location.href;        
        var action = cmp.get("c.find_FIPById");
        action.setParams({
            URL: url
        });
        
        console.log('URL = ' +url);
        var action2 = cmp.get("c.getUserName");
                        console.log('action2 = ' +action2);

        
        
        action2.setCallback(this, function(response){
            var state = response.getState();
                        console.log('state = ' + state);

            if(response.getReturnValue() != 'FIP Amiante Utilisateur invité du site'){
                cmp.set("v.edit", true);
            }
            else {
                 cmp.set("v.edit", false);
            }
        });
        $A.enqueueAction(action2);
        
        
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("state de doInit=" +state);

            var result = response.getReturnValue();
            console.log(result);
            if (state === "SUCCESS" && result != null) {
                cmp.set('v.fip', response.getReturnValue());
            }
            else {
                alert('Error : ' + JSON.stringify(errors));
            }
            
        });
        $A.enqueueAction(action);       
    },
    
    fetchPickListVal: function(component, fieldName, elementId) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.objInfo"),
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- Aucun ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.find(elementId).set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },
    
    
    saveFip : function(cmp, ev) {
        var action = cmp.get("c.saveContents");
        var jsonFIP= cmp.get("v.fip");
        var stringFIp = JSON.stringify(jsonFIP);
        console.log('test jsonFIP = '+ jsonFIP);
        console.log('test stringFIp = '+ stringFIp);
        
        var selectCmp = cmp.find("NONU");
        var value = selectCmp.get("v.value");
        var stringFIp2 = JSON.stringify(value);
        
        action.setParams({
            "lfcJson" : stringFIp,
            "lfcJson2" : stringFIp2
        });
        
        
        action.setCallback(this, function(response) {
            
            var state = response.getReturnValue();
            var toastEvent = $A.get("e.force:showToast");
            
            if (state === "SUCCESS" || state === "Created") {
                toastEvent.setParams({
                    "type" : "success",
                    "title": "Réussie",
                    "message": "Votre FIP a bien été envoyé."
                });
            }
            else if (state === "ERROR") {
                toastEvent.setParams({
                    "type" : "Error",
                    "title": "Erreur",
                    "message": "Votre FIP n\'a pas été envoyé. "
                });
            }
                else {
                    toastEvent.setParams({
                        "type" : "Error",
                        "title": "Erreur",
                        "message": "Votre FIP n\'a pas été envoyé."
                    });
                }
            toastEvent.fire();
            
        });
        $A.enqueueAction(action);
        
    },
    
    
    saveFipOnly : function(cmp, ev) {
        var action = cmp.get("c.saveContents");
        
        var jsonFIP= cmp.get("v.fip");
        var stringFIp = JSON.stringify(jsonFIP);
        
        var selectCmp = cmp.find("NONU");
        var value = selectCmp.get("v.value");
        var stringFIp2 = JSON.stringify(value);
        
        action.setParams({
            "lfcJson" : stringFIp, 
            "lfcJson2" : stringFIp2
        });
        
        action.setCallback(this, function(response) {
            
            var state = response.getReturnValue();
            console.log('state = '+state );

            var toastEvent = $A.get("e.force:showToast");
            
            if (state === "SUCCESS" || state === "Created") {
                toastEvent.setParams({
                    "type" : "success",
                    "title": "Réussie",
                    "message": "Votre FIP a bien été envoyé."
                });
                cmp.set("v.fip.TypeFIP", null);
                cmp.set("v.fip.NCAP", null);
                cmp.set("v.fip.CAP", null);
                cmp.set("v.fip.Appelation", null);
                cmp.set("v.fip.CodeNomen", null);
                cmp.set("v.fip.Quantite", null);
                cmp.set("v.fip.TypeAmiante", null);
                cmp.set("v.fip.TypeCondi", null);
                cmp.set("v.fip.DepotBag", null);
                selectCmp.set("v.value", null);   
                cmp.set("v.fip.Classe", null);                
                cmp.set("v.fip.GEmballage", null);                
                cmp.set("v.fip.Rsociale", null);                
                cmp.set("v.fip.Siret", null);                
                cmp.set("v.fip.Adr1", null);                
                cmp.set("v.fip.Adr2", null);                
                cmp.set("v.fip.Adr3", null);                
                cmp.set("v.fip.CodePostal", null);           
                cmp.set("v.fip.Ville", null);                
                cmp.set("v.fip.AdrChantier", null);                
                cmp.set("v.fip.RsocilialDemandeur", null);                
                cmp.set("v.fip.SIRETDemandeur", null);                
                cmp.set("v.fip.AdrDemandeur", null);                
                cmp.set("v.fip.CodePostalDemandeur", null);                
                cmp.set("v.fip.VilleDemandeur", null);                
                cmp.set("v.fip.IdTVA", null);              
                
            }
            else if (state === "ERROR") {
                toastEvent.setParams({
                    "type" : "Error",
                    "title": "Erreur",
                    "message": "Votre FIP n\'a pas été envoyé. "
                });
                
            }
                else {
                    toastEvent.setParams({
                        "type" : "Error",
                        "title": "Erreur",
                        "message": "Votre FIP n\'a pas été envoyé. "
                    });
                    
                }
            toastEvent.fire();
        });
        $A.enqueueAction(action);
    },
    
    onclickCancelForm : function (cmp,ev){
        
        var selectCmp = cmp.find("NONU");
        var value = selectCmp.get("v.value");    
        cmp.set("v.fip.TypeFIP", null);
        cmp.set("v.fip.NCAP", null);
        cmp.set("v.fip.CAP", null);
        cmp.set("v.fip.Appelation", null);
        cmp.set("v.fip.CodeNomen", null);
        cmp.set("v.fip.Quantite", null);
        cmp.set("v.fip.TypeAmiante", null);
        cmp.set("v.fip.TypeCondi", null);
        cmp.set("v.fip.DepotBag", null);
        selectCmp.set("v.value", null);                
        cmp.set("v.fip.Classe", null);                
        cmp.set("v.fip.GEmballage", null);                
        cmp.set("v.fip.Rsociale", null);                
        cmp.set("v.fip.Siret", null);                
        cmp.set("v.fip.Adr1", null);                
        cmp.set("v.fip.Adr2", null);                
        cmp.set("v.fip.Adr3", null);                
        cmp.set("v.fip.CodePostal", null);           
        cmp.set("v.fip.Ville", null);                
        cmp.set("v.fip.AdrChantier", null);                
        cmp.set("v.fip.RsocilialDemandeur", null);                
        cmp.set("v.fip.SIRETDemandeur", null);                
        cmp.set("v.fip.AdrDemandeur", null);                
        cmp.set("v.fip.CodePostalDemandeur", null);                
        cmp.set("v.fip.VilleDemandeur", null);                
        cmp.set("v.fip.IdTVA", null);   
    }
    
})