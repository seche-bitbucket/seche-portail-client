({
    doInit : function(component, event, helper) {
        
        helper.fetchPickListVal(component, 'WasteNamingCode__c', 'CodeNomen');
        helper.fetchPickListVal(component, 'AsbestosType__c', 'TypeAmiante');
        helper.fetchPickListVal(component, 'PackagingType__c', 'TypeCondi');
        helper.fetchPickListVal(component, 'FIPType__c', 'TypeFIP');
        helper.doInit(component, event);
        
    },
    saveFip : function(component, event, helper) {
        // Validate form fields
        
          // Validate form fields
        var vErreur = true;
        var Appelation = component.find("Appelation");
        var value = Appelation.get("v.value");
        
        var CodeNomen = component.find("CodeNomen");
        var value2 = CodeNomen.get("v.value");
        
        var Quantite = component.find("Quantite");
        var value3 = Quantite.get("v.value");
        
        var TypeCondi = component.find("TypeCondi");
        var value4 = TypeCondi.get("v.value");
        
        var TypeAmiante = component.find("TypeAmiante");
        var value5 = TypeAmiante.get("v.value");
     
        var NONU = component.find("NONU");
        var value7 = NONU.get("v.value");

        if (value == null ) {
            Appelation.set("v.errors", [{message:"Champ obligatoire"}]);
            vErreur = true;
        } else {
            Appelation.set("v.errors", null);
            vErreur = false;
        }
        
        if (value2 == null ) {            
            CodeNomen.set("v.errors", [{message:"Champ obligatoire"}]);
            vErreur = true;
        } else {
            CodeNomen.set("v.errors", null);
            vErreur = false;
        }
        
        if (value3 == null ) {
            Quantite.set("v.errors", [{message:"Champ obligatoire"}]);
            vErreur = true;
        } else {
            Quantite.set("v.errors", null);
            vErreur = false;
        }
        
        if (value4 == null ) {
            TypeCondi.set("v.errors", [{message:"Champ obligatoire"}]);
            vErreur = true;
        } else {
            TypeCondi.set("v.errors", null);
            vErreur = false;
        }
        
        if (value5 == null ) {
            TypeAmiante.set("v.errors", [{message:"Champ obligatoire"}]);
            vErreur = true;
        } else {
            TypeAmiante.set("v.errors", null);
            vErreur = false;
        }
        
        
        if (value7 == null ) {
            NONU.set("v.errors", [{message:"Champ obligatoire"}]);
            vErreur = true;
        } else {
            NONU.set("v.errors", null);
            vErreur = false;
        }

        if (value != null && value2 != null && value3 != null && value4 != null
            && value5 != null && value7 != null){
        helper.saveFip(component, event);
        }
    },
    
    saveFipOnly : function(component, event, helper) {
        // Validate form fields
        var vErreur = true;
        var Appelation = component.find("Appelation");
        var value = Appelation.get("v.value");
        
        var CodeNomen = component.find("CodeNomen");
        var value2 = CodeNomen.get("v.value");
        
        var Quantite = component.find("Quantite");
        var value3 = Quantite.get("v.value");
        
        var TypeCondi = component.find("TypeCondi");
        var value4 = TypeCondi.get("v.value");
        
        var TypeAmiante = component.find("TypeAmiante");
        var value5 = TypeAmiante.get("v.value");  
       
        var NONU = component.find("NONU");
        var value7 = NONU.get("v.value");
                
        if (value == null ) {
            Appelation.set("v.errors", [{message:"Champ obligatoire"}]);
            vErreur = true;
        } else {
            Appelation.set("v.errors", null);
            vErreur = false;
        }
        
        if (value2 == null ) {            
            CodeNomen.set("v.errors", [{message:"Champ obligatoire"}]);
            vErreur = true;
        } else {
            CodeNomen.set("v.errors", null);
            vErreur = false;
        }
        
        if (value3 == null ) {
            Quantite.set("v.errors", [{message:"Champ obligatoire"}]);
            vErreur = true;
        } else {
            Quantite.set("v.errors", null);
            vErreur = false;
        }
        
        if (value4 == null ) {
            TypeCondi.set("v.errors", [{message:"Champ obligatoire"}]);
            vErreur = true;
        } else {
            TypeCondi.set("v.errors", null);
            vErreur = false;
        }
        
        if (value5 == null ) {
            TypeAmiante.set("v.errors", [{message:"Champ obligatoire"}]);
            vErreur = true;
        } else {
            TypeAmiante.set("v.errors", null);
            vErreur = false;
        }
        
        
        if (value7 == null ) {
            NONU.set("v.errors", [{message:"Champ obligatoire"}]);
            vErreur = true;
        } else {
            NONU.set("v.errors", null);
            vErreur = false;
        }
          
        if (value != null && value2 != null && value3 != null && value4 != null
            && value5 != null && value7 != null){
            helper.saveFipOnly(component, event);
        }
        
    },
    
    onclickCancelForm : function(component, event, helper) {
        helper.onclickCancelForm(component, event);
    }
    
})