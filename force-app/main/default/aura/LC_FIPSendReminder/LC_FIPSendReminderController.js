({
    doInit : function(component, event, helper) {
        helper.loadFip(component, event, helper);
    },
    requestSendReminder: function(component, event, helper) {
        helper.submitReminder(component, event, helper);
    },
    cancelRequest: function(component, event, helper) {
        helper.cancelCall(component, event, helper);
    }
})