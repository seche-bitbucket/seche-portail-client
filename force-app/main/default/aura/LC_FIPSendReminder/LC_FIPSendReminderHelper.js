({
    loadFip: function(component, event, helper) {
         //get FIP record Id
         var action = component.get("c.getFip");
         action.setParams({"fipId" : component.get("v.recordId")});
  
         //configure action handler
         action.setCallback(this, function(response){
             var state = response.getState();
             if(state === "SUCCESS"){
                 component.set("v.fip", response.getReturnValue());               
             }else{
                 console.log('Impossible de charger FIP : '+state);
             }
         });
         $A.enqueueAction(action);
    },
    submitReminder: function(component, event, helper) {
       
       var updateState= component.get("c.sendFipReminder");
       updateState.setParams({
           "fip" : component.get("v.fip")
       });
       //configure response handler for this action
       updateState.setCallback(this, function(response){
           var state = response.getState();
           if(state === "SUCCESS"){ 
             component.set("v.objClassController", response.getReturnValue());    
             console.log('SUCCESS : ',response.getReturnValue());  

               var resultsToast = $A.get("e.force:showToast");
               resultsToast.setParams({
                    mode: 'sticky',
                    type : response.getReturnValue().showToastMode,
                    title : "Message",
                    message :  response.getReturnValue().message
               });

               //Update the UI: closePanel, show toast, refresh page
               $A.get("e.force:closeQuickAction").fire();
               resultsToast.fire();
               $A.get("e.force:refreshView").fire();
           }else if(state === "ERROR"){
               console.log('Problem Sending rminder for FIP, response state '+state);
           }else{
               console.log('Problème inconnu: '+state);
           }
       });
       //send the request to updateCall
       $A.enqueueAction(updateState);
 
    },
    cancelCall: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }

})