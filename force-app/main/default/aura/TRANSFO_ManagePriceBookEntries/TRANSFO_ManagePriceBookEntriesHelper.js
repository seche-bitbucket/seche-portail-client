({
	createObjectData: function(component, event) {
        // get the contactList from component and add(push) New Object to List  
        var RowItemList = component.get("v.AddedQuoteLines");
        RowItemList.push({
            'sobjectType': 'QuoteLine__c',
            'Code__c': '',
            'Label__c': '',
            'Quantity__c': 0
        });
        // set the updated list to attribute (contactList) again    
        component.set("v.AddedQuoteLines", RowItemList);
    }
})