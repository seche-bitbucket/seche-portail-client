({
    doInit: function(component,event,helper){ 
         //Get related Type convention in the Map       
        // debugger;
       helper.getMapFilialeConvention(component,event,helper);         
    },
   

   closeModal : function(component, event, helper) {
    // when a component is dynamically created in lightning, we use destroy() method to destroy it.
      //Fire Event to not update List Contracts
      var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
      appEvent.setParams({
          "isCancel": true,
          "actionFromContrat": component.get('v.actionFromContrat')                          
      });
      appEvent.fire();

    component.destroy();
    }, 
       
    updateContracts: function(component,event,helper){
        var typeDeConvention = component.find("selectItem").get("v.value"); 
        console.log('typeDeConvention :',typeDeConvention);
        component.set("v.selectItem",typeDeConvention);
        var lstContractsId = component.get("v.lstContractsId");
        
        var action = component.get("c.updateContractsA");

        action.setParams({
            lstContractsId : lstContractsId,
            typeDeConvention : component.find("selectItem").get("v.value")
        }); 
        debugger;
         action.setCallback(this, function(response){
            var state = response.getState();          
            if(state === "SUCCESS"){
                helper.toastdMessage("Modification type de convention","Modification effectuée avec succès","success");
                  //Fire Event to update List Contracts
                  var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
                  appEvent.setParams({
                      "isCancel": false,
                      "actionFromContrat": component.get('v.actionFromContrat')                   
                  });
                  appEvent.fire();

                component.destroy();                            
            }else{
                debugger;
                helper.toastdMessage("Modification type de convention","Modification non effectuée","error");
                  //Fire Event to update List Contracts
                  var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
                  appEvent.setParams({
                      "isCancel": true,
                      "actionFromContrat": component.get('v.actionFromContrat')                         
                  });
                  appEvent.fire();
                  
                component.destroy();
            }
        });  
        $A.enqueueAction(action);
    },

    updateSelectedContracts: function(component,event,helper){
        var typeDeConvention = component.find("selectItem").get("v.value"); 
        console.log('typeDeConvention :',typeDeConvention);
        component.set("v.selectItem",typeDeConvention);

        var selectedListCont = component.get("v.selectedListCont");
        var selectedListCont2;
        if(selectedListCont.length> 300){  // divide the list of amendements  to avoid the CPU Limit(processe bulider & triggers are waiting somewhere ^^)       
            selectedListCont2 = selectedListCont.splice(0, Math.ceil(selectedListCont.length / 2));

           console.log('selectedListCont---',selectedListCont.length);
           console.log('selectedListCont2---',selectedListCont2.length);
       }
        var action = component.get("c.updateContractsConv");

        action.setParams({
            lstCont : selectedListCont,
            typeDeConvention : component.find("selectItem").get("v.value")
        });
        debugger;
         action.setCallback(this, function(response){
            var state = response.getState();          
            if(state === "SUCCESS"){
                debugger;
                if(selectedListCont2){
                    helper.processOnContractCallBack(component, event, helper,selectedListCont2);
                }
                helper.toastdMessage("Type de convention","Modification effectuée avec succès","success");
                  //Fire Event to update List Contracts
                  var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
                  appEvent.setParams({
                      "isCancel": false,
                      "actionFromContrat": component.get('v.actionFromContrat')                   
                  });
                  appEvent.fire();

                component.destroy();                            
            }else{
                debugger;
                helper.toastdMessage("Type de convention","Modification non effectuée","error");
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                }
                  //Fire Event to update List Contracts
                  var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
                  appEvent.setParams({
                      "isCancel": true,
                      "actionFromContrat": component.get('v.actionFromContrat')                            
                  });
                  appEvent.fire();

                component.destroy();
            }
        });  
        $A.enqueueAction(action);
    },

    fireRefreshEvent : function(component, event, helper) {
        // Get the  event by using the
        // name value from aura:registerEvent
        //debugger;
        var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");  
        appEvent.setParams({
            "actionFromContrat": component.get('v.actionFromContrat')                            
        });     
            appEvent.fire();
    }
    
}


)