({
    getMapFilialeConvention : function(component,event,helper) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        //debugger;
        var action = component.get("c.getTypeValues");  
        action.setParams({
            ctrField :"Filiale__c",
            depField: "Type_de_convention__c"
        });       

        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
               // debugger;
                // Alert the user with the value returned 
                // from the server

                //Get the current filiale name     
                var filiale = component.get("v.filiale"); 
                var res =   response.getReturnValue();             
               component.set('v.dependentFieldMap', response.getReturnValue());
               console.log('dependentFieldMap filiale : ', component.get('v.dependentFieldMap')[filiale]);                 
               helper.fetchDepValues(component, component.get('v.dependentFieldMap')[filiale]); 
               
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },

    fetchDepValues: function(component, ListOfDependentFields) {
        // create a empty array var for store dependent picklist values for controller field  
        var dependentFields = [];
        dependentFields.push('--- None ---');
        for (var i = 0; i < ListOfDependentFields.length; i++) {
            dependentFields.push(ListOfDependentFields[i]);        }
        // set the dependentFields variable values to store(dependent picklist field) on lightning:select
        component.set("v.listDependingValues", dependentFields);
        
    },
    processOnContractCallBack: function (component, event, helper,selectedListCont) {
        var typeDeConvention = component.find("selectItem").get("v.value"); 
        console.log('typeDeConvention :',typeDeConvention);
        component.set("v.selectItem",typeDeConvention);

        var selectedListCont = selectedListCont;
     
        var action = component.get("c.updateContractsConv");

        action.setParams({
            lstCont : selectedListCont,
            typeDeConvention : component.find("selectItem").get("v.value")
        });
        debugger;
         action.setCallback(this, function(response){
            var state = response.getState();          
            if(state === "SUCCESS"){
                helper.toastdMessage("Type de convention","Modification effectuée avec succès","success");
                  //Fire Event to update List Contracts
                  var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
                  appEvent.setParams({
                      "isCancel": false                            
                  });
                  appEvent.fire();

                component.destroy();                            
            }else{
                debugger;
                helper.toastdMessage("Type de convention","Modification non effectuée","error");
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                }
                  //Fire Event to update List Contracts
                  var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
                  appEvent.setParams({
                      "isCancel": true                            
                  });
                  appEvent.fire();

                component.destroy();
            }
        });  
        $A.enqueueAction(action);

    },

    toastdMessage : function(title,message,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    }         
})