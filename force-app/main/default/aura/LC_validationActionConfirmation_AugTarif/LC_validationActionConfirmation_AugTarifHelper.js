({
    processValidation: function (component, event, helper) {

        helper.showSpinner(component, event, helper);
        //debugger; 
        var filiale = component.get("v.filiale");
        var filiere = component.get("v.filiere");
        var associatedSalesRep = component.get("v.associatedSalesRep");
        var faf = component.get("v.faf");
        var accountsIds = component.get("v.accountsIds");
        console.log("Validate faf: "+faf);
        //call Apex
        var action = component.get("c.validate");
        action.setParams({
            associatedSalesRep: associatedSalesRep, 
            filiale : filiale,
            filiere : filiere,
            faf : faf,
            accountsIds : accountsIds
        });

        //configure action handler
        action.setCallback(this, function (response) {
           
            helper.hideSpinner(component, event, helper);
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                let statusFaf = response.getReturnValue();
                helper.toastdMessage("Soumettre pour validation", "L'augmentation sur le portefeuille a été validée...", "success");
                if(faf) {
                    // store a value which will be used in LC_ValidationItem_AugTarifController in order to go back to this component after the approval
                    localStorage.setItem('fromFaf', true);
                } else {
                    localStorage.setItem('fromStandard', true);
                }
                event.preventDefault();            
                component.destroy();
                setTimeout(function() {
                    var url = new URL(window.location.href)
                    var searchParams = url.searchParams
                    searchParams.set((faf) ? 'c__statusfaf' : 'c__status', statusFaf)
                    window.location.replace(url.toString())
                }, 1000)
            } else {
                console.log('Erreur rencontrée pendant la mise à jours veuillez contacter l\'administrateur' + state);
            }
        });
        $A.enqueueAction(action);
    },

    processReject: function (component, event, helper) {
        helper.showSpinner(component, event, helper);
        var inputList = [];           
        var filiale = component.get("v.filiale");
        var filiere = component.get("v.filiere");
        var associatedSalesRep = component.get("v.associatedSalesRep");        
        var listAccountComments = component.get("v.listAccountComments"); 
        var faf = component.get("v.faf");
        var accountsIds = component.get("v.accountsIds");

        console.log("Reject faf: "+faf);

        console.log('processReject.listAccountComments = ', listAccountComments); 

        for(var i=0; i<listAccountComments.length;i++ ){
            inputList.push(listAccountComments[i]);
        }    
        
        console.log('processReject.inputList = ', inputList); 

     //call Apex
        var action = component.get("c.reject");
        action.setParams({
            associatedSalesRep: associatedSalesRep, 
            filiale : filiale,
            filiere : filiere,
            comments : inputList,
            faf : faf,
            accountsIds: accountsIds,
        });

        //configure action handler
        action.setCallback(this, function (response) {
            helper.hideSpinner(component, event, helper);
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                let statusFaf = response.getReturnValue();
                //buiding a page reference for the component where we need to navigate            
                // var pageReference = component.get("v.previousReference"); //helper.setUrlNavigation(component, event);
                // //set page state to pass record id parameter
                // if(component.get("v.faf")) {
                //     pageReference.state.c__statusfaf = 'Rejete';
                // } else {
                //     pageReference.state.c__status = 'Rejete';
                // }
                
                helper.toastdMessage("Soumettre pour validation", "L'augmentation sur le portefeuille a été rejetée...", "success");  
                //firevent before redirect
                helper.fireConfirmEvent(component, event, helper);
                // store a value which will be used in LC_ValidationItem_AugTarifController in order to go back to this component after the rejection
                if(faf) {
                    localStorage.setItem('fromFaf', true);
                } else {
                    localStorage.setItem('fromStandard', true);
                }
                event.preventDefault();              
                component.destroy();
                setTimeout(function() {
                    var url = new URL(window.location.href)
                    var searchParams = url.searchParams
                    searchParams.set((faf) ? 'c__statusfaf' : 'c__status', statusFaf)
                    window.location.replace(url.toString())
                }, 1000)
            } else {
                console.log('Erreur rencontrée pendant la mise à jours veuillez contacter l\'administrateur' + state);
            }
        });
        $A.enqueueAction(action);

    },

    setUrlNavigation: function (component, event) {
        var pageReference = {
            "type": "standard__component",
            "attributes": {
                "componentName": "c__LC_ValidationList_AugTarif"
            }
        };
       return pageReference;    
    },

    fireConfirmEvent : function(component, event, helper) {
        // Get the  event by using the   
        var appEvent = $A.get("e.c:LE_ValidationRefreshAugEvent");
        appEvent.setParams({
            "refreshListData": true
        });
        appEvent.fire();
    },
    
    toastdMessage: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": 'dismissible',
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },

    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);       
    },
})