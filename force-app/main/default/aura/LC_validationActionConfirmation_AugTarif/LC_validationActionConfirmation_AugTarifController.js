({  
    closeModal: function (component, event, helper) {       
        component.destroy();
    },

    process: function (component, event, helper) {        

        var action = component.get("v.action");
        if(action == 'validate'){
            helper.processValidation(component, event, helper);
        }else if(action == 'reject'){
            helper.processReject(component, event, helper);
        }       
    },

    fireRefreshEvent : function(component, event, helper) {
        var objectType = component.get("v.objectType");      
        if(objectType === 'contract'){         
            var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
            appEvent.setParams({
                "isCancel": false                            
            });
        }else{           
            var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");   
            appEvent.setParams({
                "isCancel": false                            
            });               
        }
         appEvent.fire();

    }
    
})