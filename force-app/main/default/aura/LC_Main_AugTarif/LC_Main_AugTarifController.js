({
    getHeaderPickListEvent : function(component, event, helper) {
        helper.handleHeaderPickListEvent(component, event) ;
    },

    getDisplayOptions : function(component, event, helper) {
        helper.handleDisplayOptionsEvent(component, event) ;
    },


    init : function(component, event, helper) {       
        console.log('Main chart Test');
            helper.setupRadarChart(component);
    } ,

    handleActive: function (component, event, helper) {       
        helper.handleActive(component, event,helper);
    },

    fireRefreshEvent : function(component, event, helper) {
       helper.fireRefreshEvent(component, event, helper);
    }
    
})