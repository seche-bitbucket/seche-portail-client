({
    handleHeaderPickListEvent: function (component, event) {
        var filiale = event.getParam("filiale");
        var filiere = event.getParam("filiere");
        var natureCompte = event.getParam("natureCompte");
        var isShowContent = event.getParam("isShowContent");

        // set the handler attributes based on event data
        component.set("v.filiale", filiale);
        component.set("v.filiere", filiere);
        component.set("v.natureCompte", natureCompte);
        component.set("v.isShowContent", isShowContent);

        console.log('MainComponent.filiale>> ', component.get("v.filiale"));
        console.log('MainComponent.filiere >> ', component.get("v.filiere"));
        console.log('MainComponent.natureCompte >> ', component.get("v.natureCompte"));
        console.log('MainComponent.isShowContent >> ', component.get("v.isShowContent"));

    },

    handleDisplayOptionsEvent: function (component, event) {
        var displayOption = event.getParam("displayOption");       
     //debugger;
        component.set("v.displayOption", displayOption);

        console.log('MainComponent.displayOption>> ', component.get("v.displayOption"));

    },

    handleActive: function (component, event, helper) {
        var tab = event.getSource();
        switch (tab.get('v.id')) {
            case 'augTarif':
                helper.fireRefreshEvent(component, event, helper);
                break;
            /*  case 'Avenants':
                  this.injectComponent('lightningcomponentdemo:exampleIcon', tab);
                  break;
              case 'opportunity':
                  this.injectComponent('lightningcomponentdemo:exampleInputValidation', tab);
                  break; */
        }
    },

    fireRefreshEvent: function (component, event, helper) {
        // Get the  event by using the
        // name value from aura:registerEvent
        //debugger;
          //Fire Event to update List Contracts
          var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
          appEvent.setParams({
              "isCancel": false                            
          });
          appEvent.fire();

    }
})