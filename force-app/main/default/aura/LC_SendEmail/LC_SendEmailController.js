({

    doInit: function(component, event, helper) {
        helper.setContent(component, event, helper);
    },
    
    sendMail: function(component, event, helper) {
        helper.sendHelper(component, event, helper);
    }, 
 
    closeModal : function(component, event, helper) {
        // when a component is dynamically created in lightning, we use destroy() method to destroy it.
        $A.get("e.force:closeQuickAction").fire();
      
    }    
})