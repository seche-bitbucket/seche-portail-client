({
    setContent: function (component, event,helper) {

        var recordId = component.get('v.recordId');
        // call the server side controller method 	
        var action = component.get("c.getPreviewEmail");
        action.setParams({
            'Id': recordId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();              
                if(storeResponse.isEmailAddressBounced === 'isEmailAddressBounced'){
                    //reset the quote status
                    //component.set("v.initialStatus", storeResponse.initialStatus);
                   // helper.resetHelper(component, event, helper);
                   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "mode": 'sticky',
                        "title": 'Envoi',
                        "message": storeResponse.message,
                        "type": 'warning'
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();

                }else{
                    component.set("v.message", storeResponse.message);

                    component.set("v.isShowForm", storeResponse.isShowForm);
                    
                    if (storeResponse.isShowForm === 'true') {
                        component.set("v.contactId", storeResponse.contactId);
                        component.set("v.contactName", storeResponse.contactName);
                        component.set("v.toAddresses", storeResponse.toAddresses);
                        component.set("v.fromAddresses", storeResponse.replyTo);
                        component.set("v.cciEmail", storeResponse.ccaddresses);
                        component.set("v.senderDisplayName", storeResponse.senderDisplayName);
                        component.set("v.subject", storeResponse.subject);
                        component.set("v.body", storeResponse.body);
                        component.set("v.fileId", storeResponse.fileId);
                        component.set("v.initialStatus", storeResponse.status);
                        component.set("v.tech_uid", storeResponse.tech_uid);
    
                    } else {
                        $A.get("e.force:closeQuickAction").fire();
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "mode": 'sticky',
                            "title": 'Envoi',
                            "message": storeResponse.message,
                            "type": 'warning'
                        });
                        toastEvent.fire();
    
                    }
                }
                // if state of server response is comes "SUCCESS",
                // display the success message box by set mailStatus attribute to true         
              
            } else {
                console.log('error ---> setContent');
            }

        });
        $A.enqueueAction(action);
    },

    sendHelper: function (component, event, helper) {
        //set spinner    
        component.set('v.loaded', !component.get('v.loaded'));

        component.set('v.isCloseModal', false);
        var loaded = component.get('v.loaded');
        var recordId = component.get('v.recordId');
        var contactId = component.get('v.contactId');
        var contactName = component.get('v.contactName');
        var fromAddresses = component.find('fromAddresses').get("v.value");
        var toAddresses = component.find('toAddresses').get("v.value");
        var cciEmail = component.find('cciEmail').get("v.value");
        var senderDisplayName = component.get("v.senderDisplayName");
        var subject = component.find('subject').get("v.value");
        var body = component.find('body').get("v.value");
        var fileId = component.get('v.fileId');
        var tech_uid = component.get('v.tech_uid');

        console.log('sendHelper> tech_uid --- ',tech_uid);

        // call the server side controller method 	
        var action = component.get("c.sendMailMethod");

        var cciAddressValid = component.find('cciEmail').get('v.validity').valid;

        if (cciAddressValid) {
            // set the 3 params to sendMailMethod method   
            action.setParams({
                'contactId': contactId,
                'contactName': contactName,
                'fromAddresses': fromAddresses,
                'toAddresses': toAddresses,
                'cciEmail': cciEmail,
                'senderDisplayName': senderDisplayName,
                'subject': subject,
                'body': body,
                'Id': recordId,
                'tech_uid': tech_uid
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    component.set('v.loaded', !component.get('v.loaded'));
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "mode": 'sticky',
                        "title": 'Envoi',
                        "message": storeResponse.message,
                        "type": storeResponse.showToastMode
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get("e.force:refreshView").fire();
                } else {
                    console.log('error ---> sendHelper');
                }

            });
            $A.enqueueAction(action);
        } else {
            component.set('v.loaded', !component.get('v.loaded'));
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "mode": 'sticky',
                "title": $A.get("$Label.c.Warning_invalid_fields_title"),
                "message": $A.get("$Label.c.Warning_invalid_fields"),
                "type": 'warning'
            });
            toastEvent.fire();
        }
    },  



})