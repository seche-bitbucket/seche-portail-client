({

     // function automatic called by aura:waiting event  
     showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },

    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },

    getContractsHelper: function (component, event, helper) {
        try{
            var rowActions = helper.getRowActions.bind(this, component);
            helper.showSpinner(component, event, helper);
            component.set('v.mycolumns', [
                { label: 'Commercial', fieldName: 'AssociatedSalesRepName', type: 'text', sortable: true },
                { label: 'Filiale', fieldName: 'Filiale__c', type: 'text', sortable: true },
                { label: 'Filière', fieldName: 'Filiere__c', type: 'text', sortable: true },
            // { label: 'Statut du portefeuille',initialWidth: 150, fieldName: 'Status__c', type: 'text', sortable: true, cellAttributes: { alignment: 'center' } },
                {label: 'Statut portefeuille Standard', fieldName: 'Status__c', sortable: true,  type: 'text', initialWidth: 230, cellAttributes: { alignment: 'center', class: { fieldName: 'Status__c' } }},
                { label: 'Changement statut Standard', initialWidth: 230, fieldName: 'ChangedStatusDate__c', type: 'Date', sortable: true, cellAttributes: { alignment: 'center' } },
                {label: 'Statut portefeuille FaF', fieldName: 'Status_faf__c', sortable: true,  type: 'text', initialWidth: 180, cellAttributes: { alignment: 'center', class: { fieldName: 'Status_faf__c' } }},
                { label: 'Changement statut FaF', initialWidth: 250, fieldName: 'ChangedStatusDate_faf__c', initialWidth: 200, type: 'Date', sortable: true, cellAttributes: { alignment: 'center' } },
                { label: 'Détails', type: 'button', initialWidth: 110, typeAttributes: { label: '', iconName: 'utility:zoomin', name: 'view_details', title: 'Cliquez pour voir détails' }, cellAttributes: { alignment: 'center' } }          

            ]);
            var action = component.get("c.fetchContracts");
            action.setParams({
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
            
                if (state === "SUCCESS") {
                    var data = response.getReturnValue();

                    console.log('data -->', data);

                    let rows = [];
                    let rawData = [];
                    var listAccountIds = [];

                    data.forEach(d => {

                        let row = {AssociatedSalesRep__r: {}};

                        let std = d.standard;
                        let faf = d.faf;

                        console.log(std);

                        rawData.push(std);
                        rawData.push(faf);

                        row.Name =  (std != undefined) ? std.Name: faf.Name;
                        row.Id = (std != undefined) ? std.Id : faf.Id;
                        row.AssociatedSalesRep__c = (std != undefined) ? std.AssociatedSalesRep__c : faf.AssociatedSalesRep__c;
                        row.AssociatedSalesRep__r.Name = (std != undefined) ? std.AssociatedSalesRep__r.Name : faf.AssociatedSalesRep__r.Name;
                        row.disableFAF = (d.fafSettings) ? d.fafSettings.disableButtons : false;
                        row.AssociatedSalesRepName = (std != undefined) ? std.AssociatedSalesRep__r.Name : faf.AssociatedSalesRep__r.Name;
                        row.Filiere__c = (std != undefined) ? std.Filiere__c : faf.Filiere__c;
                        row.Filiale__c = (std != undefined) ? std.Filiale__c : faf.Filiale__c;
                        row.Status__c = (std != undefined) ? std.Status__c : null;
                        row.Status_faf__c = (faf != undefined) ? faf.Status__c : null;
                        row.idStd = (std != undefined) ? std.Id : null;
                        row.idFaF = (faf != undefined) ? faf.Id : null;
                        row.ChangedStatusDate__c = (std != undefined) ? $A.localizationService.formatDate(std.ChangedStatusDate__c, "yyyy-MM-dd") : null;
                        row.ChangedStatusDate_faf__c = (faf != undefined) ? $A.localizationService.formatDate(faf.ChangedStatusDate__c, "yyyy-MM-dd") : null;

                        rows.push(row);

                    })               
                    // for (var i = 0; i < rows.length; i++) {
                    //     let row = rows[i];
                    //     //get the accountId
                    //     listAccountIds.push(row.Account__c);
                    //     // checking if any AssociatedSalesRep related data in row
                    //     if (row.AssociatedSalesRep__c) {
                    //         row.AssociatedSalesRepName = row.AssociatedSalesRep__r.Name;
                    //     }
                    //     //row.Status_faf__c = "Brouillon";
                    //     console.log('row => '+JSON.stringify(row));
                    //     if(fafSubmittedInList){
                    //         row.Status_faf__c = "Soumis";
                    //         console.log("row faf =================> "+JSON.stringify(row));
                    //     } else if(fafValidatedInList) {
                    //         row.Status_faf__c = "Valide";
                    //         console.log("row faf =================> "+JSON.stringify(row));
                    //     } else {
                    //         row.Status_faf__c = "Brouillon";
                    //         console.log("row faf =================> "+JSON.stringify(row));
                    //     }
                    //     // formatting created date 
                    //     row.ChangedStatusDate__c = $A.localizationService.formatDate(row.ChangedStatusDate__c, "dd/MM/yyyy");
                    // }
                    
                    console.log("rows => "+JSON.stringify(rows));
                    // setting formatted data to the datatable
                    component.set("v.data", rows);
                    component.set("v.rawData", rawData);
                    component.set("v.listAccountIds", listAccountIds);
                }
                helper.hideSpinner(component,event,helper);

            });
            $A.enqueueAction(action);
        } catch(e) {
            console.log(e)
        }
    },    

    getAllAccountIds: function (component, event, helper) {
       // 
        var action = component.get("c.getAccountIds");
        action.setParams({
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
           
            if (state === "SUCCESS") {
                var rows = response.getReturnValue();
                var listAccountIds;               
                for (var i = 0; i < rows.length; i++) {
                    let row = rows[i];
                    //get the accountId
                    if(listAccountIds == undefined || listAccountIds === ""){
                        listAccountIds = row.Account__c;
                    }
                    else{
                        listAccountIds = listAccountIds.concat(",",row.Account__c);
                    }
                }
                // setting formatted data to the datatable
                component.set("v.listAllAccountIds", listAccountIds);
            }
        });
        $A.enqueueAction(action);
    },
 
    showRowDetails: function (component, event, helper, row) {
        //fire event to pass ids to navigated component
        helper.fireInputValuesEvent(component, event, helper);

        console.log('row clicked =>', JSON.stringify(row))
        
        //buiding a page reference for the component where we need to navigate            
        var pageReference =  helper.setUrlNavigation(component, event);
        //set page state to pass record id parameter
            pageReference.state = { 
        "c__objectName": row.Name,
        "c__recordId": row.Id,
        "c__respCommId" : row.AssociatedSalesRep__c,
        "c__respCommName" : row.AssociatedSalesRep__r.Name,
        "c__date" : row.ChangedStatusDate__c,
        "c__datefaf": row.ChangedStatusDate_faf__c,
        "c__filiale" : row.Filiale__c,
        "c__filiere" : row.Filiere__c,
        "c__status" : row.Status__c,
        "c__statusfaf" : row.Status_faf__c
        // "c__listAccountIds" : component.get("v.listAccountIds"),
        // "c__listAllAccountIds" : component.get("v.listAllAccountIds")
    };

        //navigate to component
        var navService = component.find("navService");
        event.preventDefault();
        //navigate function navigates to page reference
        navService.navigate(pageReference);
        

    },   

    getRowActions: function (component, row, doneCallback) {
        var actions = [{
            'label': 'Voir Details',
            'iconName': 'utility:zoomin',
            'name': 'show_details'
        }];    

    },

    sortBy: function (field, reverse, primer) {
        var key = primer
            ? function (x) {
                return primer(x[field]);
            }
            : function (x) {
                return x[field];
            };

        return function (a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    },

       handleSort: function (component, event) {

        var sortedBy = event.getParam('fieldName') != undefined ? event.getParam('fieldName') : 'Status__c';
        var sortDirection = event.getParam('sortDirection');
        var cloneData = component.get('v.data');
        var parser = (v) => v;

        if(sortedBy != 'Status__c'){
            var column = component.get('v.mycolumns').find(c=>c.fieldName===sortedBy);
            if(column.type==='Date' || column.type==='Datetime') {
                 parser = (v) => (v && new Date(v));
            }
        }

        let sortMult = sortDirection === 'asc'? 1: -1;
        cloneData = cloneData.sort((a,b) => {
            let a1 = parser(a[sortedBy]), b1 = parser(b[sortedBy]);
            let r1 = a1 < b1, r2 = a1 === b1;
            return r2? 0: r1? -sortMult: sortMult;
          });

        component.set('v.data', cloneData);
        component.set('v.sortDirection', sortDirection);
        component.set('v.sortedBy', sortedBy);
    },

    setUrlNavigation: function (component, event) {       
        var pageReference = {

            "type": "standard__component",
            "attributes": {
                "componentName": "c__LC_ValidationItem_AugTarif"
            }
        };
       
        return  pageReference;
    },

    toastdMessage: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": 'sticky',
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },

     fireInputValuesEvent : function(component, event, helper) {
        // Get the  event by using the   
        var appEvent = $A.get("e.c:LE_ValidationAugEvent");
        appEvent.setParams({
            "listAccountIds" : component.get("v.listAccountIds"), //.toString() ,
            "listAllAccountIds" :  component.get("v.listAllAccountIds")          
            });
            appEvent.fire();
    },

    handleEventValues: function (component, event, helper) {
           
        var refreshListData = event.getParam("refreshListData");     

        // set the handler attributes based on event data
        component.set("v.refreshListData", refreshListData);
     
        console.log("handleEventValues.refreshListData> ", component.get("v.refreshListData"));      
            
    }
})