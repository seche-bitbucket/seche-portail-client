({
    init : function(component, event, helper) { 
        try{     
            helper.getContractsHelper(component, event, helper);
            helper.getAllAccountIds(component, event, helper); 
            
            localStorage.setItem('myCat', 'Tom');
            console.log("existant cookie: "+localStorage.getItem('myCat'));
            console.log("non existant cookie: "+localStorage.getItem('myCat2'));
        } catch(e) {
            console.log(e)
        }
    },

    handleRowAction: function (component, event, helper) {
       // 
        var action = event.getParam('action');
        var row = event.getParam('row');

        switch (action.name) {
            case 'view_details':
                helper.showRowDetails(component, event, helper,row);
                break;          
            case 'validate':
                helper.validate(component, row);
                break;
            case 'reject':
                helper.reject(component, row);
                break;
          
        }
    },

    handleSort: function(component, event, helper) {
        helper.handleSort(component, event);
    },

    getValidationConfirmValuesEvent : function(component, event, helper) {
        console.log('LC_ValidationList_AugTarif.getValidationConfirmValuesEvent>>>>>>>>>>>Start ');
        helper.handleEventValues(component, event, helper) ;
    },

    refreshAction : function(component, event, helper) {
       let refreshListData =  component.get("v.refreshListData");
       if(refreshListData){
           component.set('v.refreshListData', false); //reset state so it won't fire the refresh infinitely
           $A.get('e.force:refreshView').fire();
       }
       
    },

    reInit : function(component, event, helper) {
        helper.getContractsHelper(component, event, helper);
        helper.getAllAccountIds(component, event, helper); 
      //  $A.get('e.force:refreshView').fire();
    }

    
})