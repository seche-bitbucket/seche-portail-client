({
    scriptsLoaded: function (component, event, helper) {
        console.log('Script loaded..');
        //get settings for table
        helper.getMetadata(component, event, helper);
        console.log('metadat retrieved..');
    },

    // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
       
        component.set("v.spinner", true); 
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },

    fireEvent: function (component, event, helper) {
        helper.fireTheContratListEvent(component, event, helper);
    },

    getPicklistEvent: function (component, event, helper) {
        console.log('AvenantListComponent.getHeaderEvent>>>>>>>>>>>Start ');
        helper.handleHeaderPickListEvent(component, event, helper);
    },

    setIsShowTable: function (component, event, helper) {
        component.set("v.isShowTable", false);
    },

    toggleVisibility: function (component, event, helper) {
        var ddDiv = component.find('ddId');
        $A.util.toggleClass(ddDiv, 'slds-is-open');
    },

    handlefireEvent: function (component, event, helper) {
        helper.fireEvent(component, event, helper);
    },

    onSuccess: function (component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The record has been Saved successfully."
        });
        toastEvent.fire();
    },

    showEditModal: function (component, event, helper) {
        //Dynamic creation of lightningModalChild component and appending its markup in a div
        helper.openEditModal(component, event, helper);
    },

    handleSelect: function (component, event, helper) {
        // This will contain the string of the "value" attribute of the selected
        // lightning:menuItem           
        var selectedMenu = event.detail.menuItem.get("v.value");
        console.log('selectedMenu-', selectedMenu);
        switch (selectedMenu) {
            case "fixedPrice":
                var action = "fixedPrice";
                var headerText = "Prix fixe";
                helper.showFixedPriceForm(component, event, helper, action, headerText);
                break;
            case "incfixedPrice":
                var action = "incfixedPrice";
                var headerText = "Augmentation en prix fixe";
                helper.showFixedPriceForm(component, event, helper, action, headerText);
                break;
            case "noIncrease":
                var action = "noIncrease";
                var headerText = "Ne pas augmenter";
                helper.showNoIncreaseForm(component, event, helper, action, headerText);
                break;
            case "increasePercentAmend":
                var action = "increasePercentAmend";
                var headerText = "Augmentation par pourcentage";
                helper.showIncreasePurcentForm(component, event, helper, action, headerText);
                break;
        }
    },

    updateAvenant : function(component, event, helper) {            
        helper.updateAvenant(component, event, helper);
    },

    getSelectedContract: function (component, event, helper) {
        console.log('getSelectedContract>>>>>>>>>>>Start ');
        helper.handleContractListEvent(component, event, helper);
    },

    doInit: function (component, event, helper) {

        console.log('doInit>>>>>>>>>>>Start ');
        component.set("v.keepState", true);
       ;
        var isCancel = event.getParam("isCancel");
        if(!isCancel){
            helper.updateSelectedAvenant(component, event, helper);
        }
      
    },

    genarateDataTable: function (component, event, helper) {
        console.log('genarateDataTable>>>>>>>>>>>Start ');
        helper.getMetadata(component, event, helper);
        var lististAvenant = component.get('v.lististAvenant');
        helper.generateDataTable(component, event, helper, lististAvenant);
    },


    validation: function (component, event, helper) {
        var action = "validationAmendement";
        var headerText = "Soumettre pour validation"
        helper.showValidationForm(component, event, helper, action, headerText);
    },

    increasePercent: function (component, event, helper) {
        var action = "increasePercentAmend";
        var headerText = "Augmentation par pourcentage";
        helper.showIncreasePurcentForm(component, event, helper, action, headerText);
    },

    incfixedPrice: function (component, event, helper) {
        var action = "incfixedPrice";
        var headerText = "Augmentation en prix fixe";
        helper.showFixedPriceForm(component, event, helper, action, headerText);
    },

    fixedPrice: function (component, event, helper) {
        var action = "fixedPrice";
        var headerText = "Prix fixe";
        helper.showFixedPriceForm(component, event, helper, action, headerText);
    },

    noIncrease: function (component, event, helper) {
        var action = "noIncrease";
        var headerText = "Ne pas augmenter";
        helper.showNoIncreaseForm(component, event, helper, action, headerText);
    },

    reset: function (component, event, helper) {
        var action = "reset";
        var headerText = "Réinitiliser";
        helper.showResetForm(component, event, helper, action, headerText);
    },

    exclude: function (component, event, helper) {
        var action = "exclude";
        var headerText = "Exclure";
        helper.showExcludeForm(component, event, helper, action, headerText);
    },

    getDisplayOptions : function(component, event, helper) {
        helper.handleDisplayOptionsEvent(component, event) ;
    }


})