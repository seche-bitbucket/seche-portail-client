({
    // function automatic called by aura:waiting event  
    showSpinner: function (component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        
        component.set("v.spinner", true);
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner: function (component, event, helper) {
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },
    
    handleContractListEvent: function (component, event, helper) {
        
        var selectedListCont = event.getParam("selectedListCont");
        
        console.log('selectedListCont : ' + selectedListCont);
        
        //show Table only when get amandements
        if (selectedListCont.length >= 1) {
            component.set("v.isShowTable", true);
            component.set("v.keepState", true);
        } else {
            component.set("v.isShowTable", false);
        }
        
        component.set("v.selectedListCont", selectedListCont);
        
        var newarrayAven = [];
        // 
        if (selectedListCont) {
            // set the handler attributes based on event data         
            for (var i = 0; i < selectedListCont.length; i++) {
                console.log("selectedListCont >> ", selectedListCont[i].Avenants__r);
                //Get related avenants                            
                if (selectedListCont[i] && selectedListCont[i].Avenants__r) {
                    for (var k = 0; k < selectedListCont[i].Avenants__r.length; k++) {
                        console.log("selectedListCont.Avenants__r.Nature__c >> ", selectedListCont[i].Avenants__r[k].Nature__c);
                        newarrayAven.push(selectedListCont[i].Avenants__r[k]);
                    }
                    
                }
            }
            component.set("v.lististAvenant", newarrayAven);
            //var varcolumnDefs = helper.setColumnsDefs(component, event, helper, component.get("v.filiale"), component.get("v.filiere"));
            
            helper.generateDataTable(component, event, helper, newarrayAven);
            component.set("v.spinner", false);
            
        }
        
    },
    
    
    /* Formatting function for row details - modify as you need */
    formatDetail: function format(component, event, helper, d) {
        // `d` is the original data object for the row
        var accountName = d.Account__r == null ? '' : d.Account__r.Name;
        var contactRelated = d.ContactRelated__c == null ? '' : d.ContactRelated__c;
        var thirdPartyProducerName = d.ThirdPartyProducerName__c == null ? '' : d.ThirdPartyProducerName__c;
        var thirdPartyProducerCode = d.ThirdPartyProducerCode__c == null ? '' : d.ThirdPartyProducerCode__c;
        var state = d.State__c == null ? '' : d.State__c;
        var customerNature = d.CustomerNature__c == null ? '' : d.CustomerNature__c;
        var contractName = d.Contrat2__r == null ? '' : d.Contrat2__r.Name
        var filiale = d.Filiale__c == null ? '' : d.Filiale__c;
        var commercial_Associe = d.Commercial_Associe__c == null ? '' : d.Commercial_Associe__c;
        var filiere = d.Filiere__c == null ? '' : d.Filiere__c;
        var indexationGasoil = d.Indexation_gasoil__c == true ? 'Oui' : 'Non';
        var communalTax = d.Communal_tax__c == true ? 'Oui' : 'Non';
        var Contribspecialeinflation = d.SpecialContribution__c == null ? '' : d.SpecialContribution__c;
        var treatmentQuantityPreviousYear = d.TreatmentQuantityPreviousYear__c == null ? '' : d.TreatmentQuantityPreviousYear__c;
        var treatmentQuantityCurrentYear = d.TreatmentQuantityCurrentYear__c == null ? '' : d.TreatmentQuantityCurrentYear__c;
        var pricePreviousYear = d.PricePreviousYear__c == null ? '' : d.PricePreviousYear__c;
        var priceCurrentYear = d.PriceCurrentYear__c == null ? '' : d.PriceCurrentYear__c;
        var unitPreviousYear = d.UnitPreviousYear__c == null ? '' : d.UnitPreviousYear__c;
        var unitCurrentYear = d.UnitCurrentYear__c == null ? '' : d.UnitCurrentYear__c;
        var quantityInvoiced2018 = d.QuantityInvoiced2018__c == null ? '' : d.QuantityInvoiced2018__c;
        var planned_next_year = d.Planned_next_year__c == null ? '' : d.Planned_next_year__c;
        var chiffre_d_affaires_N_du_Contrat = d.Chiffre_d_affaires_N_du_Contrat__c == null ? '' : d.Chiffre_d_affaires_N_du_Contrat__c;
        var cA_N = d.CA_N__c == null ? '' : d.CA_N__c;
        var isTreated = d.CompletedAmadement__c == true ? 'Oui' : 'Non';
        var augmentationRatio = d.AugmentationRatio__c == null ? '' : d.AugmentationRatio__c;
        var isFaceAFace = d.Face_face__c == true ? 'Oui' : 'Non';
        var priceNextYear = d.PriceNextYear__c == null ? '' : d.PriceNextYear__c.toFixed(2);
        var treatmentQuantityNextYear = d.TreatmentQuantityNextYear__c == null ? '' : d.TreatmentQuantityNextYear__c;
        var unitNextYear = d.UnitNextYear__c == null ? '' : d.UnitNextYear__c;
        var isRecurrent = d.Recurrence__c == true ? 'Oui' : 'Non';
        var keywords = d.Keywords__c == null ? '' : d.Keywords__c;
        var wasteLabel = d.WasteLabel__c == null ? '' : d.WasteLabel__c;
        var nature = d.Nature__c == null ? '' : d.Nature__c;
        var labelArticleCode = d.LabelArticleCode__c == null ? '' : d.LabelArticleCode__c;
        var conditionnement = d.Conditionnement__c == null ? '' : d.Conditionnement__c;
        var articleCode = d.ArticleCode__c == null ? '' : d.ArticleCode__c;
        var analyticSection = d.AnalyticSection__c == null ? '' : d.AnalyticSection__c;
        var technicInfo = d.TechnicInfo__c == null ? '' : d.TechnicInfo__c;
        var actifAcceptationContrat = d.ActifAcceptationContrat__c == null ? '' : d.ActifAcceptationContrat__c;
        var includedTGAP = d.IncludedTGAP__c == null ? '' : d.IncludedTGAP__c;
        var treatmentRange = d.TreatmentRange__c == null ? '' : d.TreatmentRange__c;
        var cEECode = d.CEECode__c == null ? '' : d.CEECode__c;
        var packagingGroup = d.PackagingGroup__c == null ? '' : d.PackagingGroup__c;
        var onuCode = d.OnuCode__c == null ? '' : d.OnuCode__c;
        var classe = d.Class__c == null ? '' : d.Class__c;
        var label = d.Label__c == null ? '' : d.Label__c;
        var tT_X3 = d.TT_X3__c == null ? '' : d.TT_X3__c;
        var headerContractX3 = d.HeaderContractX3__c == null ? '' : d.HeaderContractX3__c;
        var textSalesX3 = d.TextSalesX3__c == null ? '' : d.TextSalesX3__c;
        var footerContractX3 = d.FooterContractX3__c == null ? '' : d.FooterContractX3__c;
        var commentaryAmendementsX3 = d.CommentaryAmendementsX3__c == null ? '' : d.CommentaryAmendementsX3__c;
        var sheetNumber = d.SheetNumber__c == null ? '' : d.SheetNumber__c;
        var goLitrePrice = d.GoLitrePrice__c == null ? '' : d.GoLitrePrice__c;
        var fuelPart = d.FuelPart__c == null ? '' : d.FuelPart__c;
        console.log('part du carburant : ' + fuelPart);
        console.log('d : ' + JSON.stringify(d));
        
        return (
            '<Table border=1 class="tabWidth slds-wrap">' +
            '<tr>' +
            '<td colspan="4" style="background-color: rgb(248, 203, 173);">Identité du client et du producteur</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Client facturé :</td><td>' + accountName + '</td>' +
            '<td>Contact associé :</td><td>' + contactRelated + '</td>' +
            '</tr> ' +
            '<tr>' +
            '<td>Raison Sociale Tiers Producteur :</td><td>' + thirdPartyProducerName + '</td>' +
            '<td>Code Tiers producteur :</td><td>' + thirdPartyProducerCode + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Région :</td><td>' + state + '</td>' +
            '<td>Nature du client :</td><td>' + customerNature + '</td>' +
            '</tr>' +
            
            '<tr>' +
            '<td colspan="4" style="background-color: rgb(221, 235, 247);">Données X3</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Contrat :</td><td>' + contractName + '</td>' +
            '<td>Nom de Avenant :</td><td>' + d.Name + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Filiale :</td><td>' + filiale + '</td>' +
            '<td>Commercial Associé :</td><td>' + commercial_Associe + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Filière:</td><td>' + filiere + '</td>' +
            '<td>Indexation gasoil :</td><td>' + indexationGasoil + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Taxe communale :</td><td>' + communalTax + '</td>' +
            '<td>Contrib spéciale inflation :</td><td>' + Contribspecialeinflation + '</td>' +
            '</tr>' +
            
            '<tr>' +
            '<td colspan="4" style="background-color: rgb(226, 239, 218);">Statistiques de livraison et facturation des exercices passés</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Tonnage Traitement N-1 :</td><td>' + treatmentQuantityPreviousYear + '</td>' +
            '<td>Tonnage Traitement N :</td><td>' + treatmentQuantityCurrentYear + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Tarif N-1 :</td><td>' + pricePreviousYear + '</td>' +
            '<td>Tarif N :</td><td>' + priceCurrentYear + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Unité N-1 :</td><td>' + unitPreviousYear + '</td>' +
            '<td>Unité N :</td><td>' + unitCurrentYear + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Nb quantités facturées :</td><td>' + quantityInvoiced2018 + '</td>' +
            '<td>Prévu annuel</td><td>' + planned_next_year + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Prix du litre de GO :</td><td>' + goLitrePrice + '</td>' +
            '<td>Part du carburant :</td><td>' + fuelPart + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>CA N Contrat :</td><td>' + chiffre_d_affaires_N_du_Contrat + '</td>' +
            '<td colspan="2"></td>' +
            '</tr>' +
            '<tr>' +
            '<td>CA N Avenant :</td><td>' + cA_N + '</td>' +
            '<td colspan="2"></td>' +
            '</tr>' +
            
            '<tr>' +
            '<td colspan="4" style="background-color: rgb(242, 150, 150);">L\'augmentation appliquée</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Avenant traité :</td><td>' + isTreated + '</td>' +
            '<td>Taux augmentation :</td><td>' + augmentationRatio + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Face à face :</td><td>' + isFaceAFace + '</td>' +
            '<td colspan="2"></td>' +
            '</tr>' +
            '<tr>' +
            '<td>Tarif N+1 :</td><td>' + priceNextYear + '</td>' +
            '<td>Tonnage N+1 :</td><td>' + treatmentQuantityNextYear + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Unité N+1 :</td><td>' + unitNextYear + '</td>' +
            '<td colspan="2"></td>' +
            '</tr>' +
            
            '<tr>' +
            '<td colspan="4" style="background-color: rgb(255, 228, 143);">Le déchet</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Récurrent :</td><td>' + isRecurrent + '</td>' +
            '<td>Mot clé :</td><td>' + keywords + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Libellé Déchet :</td><td>' + wasteLabel + '</td>' +
            '<td>Nature :</td><td>' + nature + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Libellé code article :</td><td>' + labelArticleCode + '</td>' +
            '<td>Conditionnement :</td><td>' + conditionnement + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Code article :</td><td>' + articleCode + '</td>' +
            '<td>Section analytique :</td><td>' + analyticSection + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Info technique :</td><td>' + technicInfo + '</td>' +
            '<td>CAP Actif  :</td><td>' + actifAcceptationContrat + '</td>' +
            '</tr>' +
            
            '<tr>' +
            '<td>TGAP :</td><td>' + includedTGAP + '</td>' +
            '<td>Gamme Traitement :</td><td>' + treatmentRange + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Code Norme CEE :</td><td>' + cEECode + '</td>' +
            '<td>Groupe emballage :</td><td>' + packagingGroup + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Cone ONU :</td><td>' + onuCode + '</td>' +
            '<td>Classe :</td><td>' + classe + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Etiquette :</td><td>' + label + '</td>' +
            '<td>Modalités de transports  :</td><td>' + tT_X3 + '</td>' +
            '</tr>' +
            
            '<tr>' +
            '<td>En-tête avenant X3 :</td><td>' + headerContractX3 + '</td>' +
            '<td>Texte ligne tarif de vente X3 :</td><td>' + textSalesX3 + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Pied de page avenant X3 :</td><td>' + footerContractX3 + '</td>' +
            '<td>Commentaire Avenants X3 :</td><td>' + commentaryAmendementsX3 + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Numéro de feuille :</td><td>' + sheetNumber + '</td>' +
            '<td colspan="2"></td>' +
            '</tr>' +
            
            '</Table>'
            
        );
    },
    
    
    
    updateSelectedAvenant: function (component, event, helper) {
        //
        var listId = [];
        var lististAvenant = component.get('v.lististAvenant');
        lististAvenant.forEach(function (av) {
            listId.push(av.Id)
        });
        
        
        var action = component.get("c.getAmendmentById");
        // pass paramerters [object definition , contrller field name ,dependent field name] -
        // to server side function 
        action.setParams({
            'listIds': listId
        });
        //set callback   
        action.setCallback(this, function (response) {
            if (response.getState() == "SUCCESS") {
                //store the return response from server (map<string,List<string>>)  
                // 
                var storeResponse = response.getReturnValue();
                console.log('storeResponse -----> ', storeResponse);
                component.set('v.lististAvenant', storeResponse);//update list   
                //helper.generateDataTable(component, event, helper,StoreResponse);               
                
            } else {
                console.log('Something went wrong..');
            }
        });
        $A.enqueueAction(action);
    },
    
    getMetadata: function(component, event, helper) {
        
        const filiale = component.get("v.filiale");
        const filiere = component.get("v.filiere");
        
        console.log('getting metadata..')
        
        let action = component.get('c.getDefinitions');
        
        action.setParams({
            all: ''
        });
        
        action.setCallback(this, function(response) {
            if(response.getState() == 'SUCCESS') {
                let val = response.getReturnValue();
                let rsp;
                console.log('getMetadata val => '+JSON.stringify(val[0]));
                var settings = val[0];
                if(filiale != '--- None ---' && filiere != '--- None ---') {
                    for(let e in settings) {
                        console.log(filiere)
                        console.log(filiale)
                        console.log('rsp e', settings[e])
                        console.log('rsp Filiale__c', settings[e].Filiale__c)
                        console.log('rsp Filiere__c', settings[e].Filiere__c)
                        
                        var filiale_in = false, filiere_in = false;
                        
                        if((settings[e].Filiere__c != undefined && settings[e].Filiale__c != undefined) && (filiere != undefined && filiale != undefined)) {
                            console.log('cond 1')
                            if(settings[e].Filiale__c.includes(',')) {
                                var lst = settings[e].Filiale__c.split(',');
                                for(let i in lst) {
                                    if(filiale.includes(lst[i])) {
                                        filiale_in = true;
                                    }
                                }
                            } else {
                                if(filiale.includes(settings[e].Filiale__c)) {
                                    filiale_in = true;
                                }
                            }
                            
                            if(settings[e].Filiere__c.includes(',')) {
                                var lst = settings[e].Filiere__c.split(',');
                                for(let i in lst) {
                                    if(filiere.includes(lst[i])) {
                                        filiere_in = true;
                                    }
                                }
                            } else {
                                if(filiere.includes(settings[e].Filiere__c)) {
                                    filiere_in = true;
                                }
                            }
                            
                            if(filiere_in && filiale_in) {
                                rsp = settings[e];
                                break;
                            }
                            
                        } else if(settings[e].Filiale__c == undefined && settings[e].Filiere__c != undefined) {
                            console.log('cond 2')
                            if(settings[e].Filiere__c.includes(',')) {
                                var lst = settings[e].Filiere__c.split(',');
                                for(let i in lst) {
                                    if(filiere.includes(lst[i])) {
                                        rsp = settings[e];
                                        break;
                                    }
                                }
                            } else {
                                if(filiere.includes(settings[e].Filiere__c)) {
                                    rsp = settings[e];
                                    break;
                                }
                            }
                        } else if(settings[e].Filiere__c == undefined && settings[e].Filiale__c != undefined) {
                            console.log('cond 2')
                            if(settings[e].Filiale__c.includes(',')) {
                                var lst = settings[e].Filiale__c.split(',');
                                for(let i in lst) {
                                    if(filiale.includes(lst[i])) {
                                        rsp = settings[e];
                                        break;
                                    }
                                }
                            } else {
                                if(filiale.includes(settings[e].Filiale__c)) {
                                    rsp = settings[e];
                                    break;
                                }
                            }
                        }
                    }
                }
                
                if(rsp == undefined) {
                    console.log('standard')
                    rsp = settings.find(e => e.Label == 'standard');
                }
                
                console.log('rsp => '+rsp.Column_Avenant__c);
                let str = rsp.Column_Avenant__c;
                let tmp = str.toString();
                let columns = tmp.split(',');
                console.log('columns => '+columns);
                
                component.set('v.columnsLabels', columns);
                component.set('v.columnsTable', val[1]);
            }
        });
        $A.enqueueAction(action);
        
    },
    
    generateDataTable: function (component, event, helper, newarrayAven) {
        console.log('newarrayAven', newarrayAven)
        
        //getting settings
        
        console.log('avenant generateDataTable => '+JSON.stringify(component.get('v.columnsLabels')));
        
        component.set("v.spinner", true);
        var table;
        var mapFilterValues = new Map();
        setTimeout(function () {
            //set columns to display
            var varcolumnDefs;
            var groupColumn = 2;
            
            //if table exists destroy it before
            
            console.log('condition => '+$.fn.dataTable.isDataTable("#tableAvenantId"))
            if ($.fn.dataTable.isDataTable("#tableAvenantId")) {
                console.log('in');
                table = $("#tableAvenantId").DataTable();
                console.log('found => '+table);
                table.destroy();
                component.set('v.showColumn', false);
                $("#tableAvenantId tbody").empty();
                console.log('content => '+JSON.stringify($('#tableAvenantId').html()));
            }
            
            varcolumnDefs = helper.setColumnsDefs(component, event, helper, component.get("v.filiale"), component.get("v.filiere"), component.get('v.columnsTable'), component.get('v.columnsLabels'));
            
            component.set('v.showColumn', true);
            console.log('columnsLabels => '+component.get('v.columnsLabels'));
            
            console.log('varcolumnDefs => ' + JSON.stringify(varcolumnDefs));
            
            //clear table State
            var resetSaveStatus = component.get("v.resetSaveStatus");
            if(resetSaveStatus){
                localStorage.clear(); //reset status                
                component.set("v.resetSaveStatus", false);          
            }else if(!component.get("v.filterValues")  && !component.get("v.keepState")){
                localStorage.clear(); //reset status 
                component.set("v.resetSaveStatus", false);  
            }
            
            // Begin Filters ----- Setup - add a text input to each header cell -----------//
            // $('#tableId thead tr').clone(true).appendTo( '#tableId thead' );															
            $('#tableAvenantId thead tr:eq(1) th').each(function (i) {
                // 
                // var title = $(this).text();
                var title = '';
                var currentIndex = $(this).context.cellIndex;
                
                
                //Get saved filter input value  
                if(!resetSaveStatus && component.get("v.filterValues")){
                    var mapVal = component.get("v.filterValues");                       
                    if (mapVal && mapVal.get(currentIndex)) {
                        title = mapVal.get($(this).context.cellIndex);
                    }   
                }  
                
                if (i > 0) {
                    //$(this).html( '<input type="text" value="'+title+'" class="column_search"/>' );
                    //resize input field filter buy column
                    if ($(this).context.cellIndex == 6 || $(this).context.cellIndex == 7) {
                        $(this).html('<input  type="text" id="' + $(this).context.cellIndex + '"  value="' + title + '" size="10" class="column_search_av" />');
                    } else if ($(this).context.cellIndex == 8 || $(this).context.cellIndex == 9 || $(this).context.cellIndex == 10 || $(this).context.cellIndex == 11 || $(this).context.cellIndex == 12 || $(this).context.cellIndex == 13 || $(this).context.cellIndex == 14 || $(this).context.cellIndex == 15 || $(this).context.cellIndex == 16) {
                        $(this).html('<input type="text" id="' + $(this).context.cellIndex + '"  value="' + title + '" size="7" class="column_search_av" />');
                    } else {
                        $(this).html('<input  type="text" id="' + $(this).context.cellIndex + '"  value="' + title + '" size="15" class="column_search_av" />');
                    }
                    
                }
                //Event to handle filter requests
                $(".column_search_av", this).on('keyup change', function () {
                    // 
                    if(component.get("v.filterValues")){
                        mapFilterValues = component.get("v.filterValues");
                    }
                    mapFilterValues.set($(this).parent().index(), $(this).context.value);
                    if(table.column(i)){
                        
                        if (table.column(i).search() !== this.value) {
                            table
                            .column(i)
                            .search(this.value)
                            .draw();
                        } else if (component.get("v.isFilterSet") && table.column(i).search()) {
                            mapFilterValues.forEach(function (values, key) {
                                table
                                .column(key)
                                .search(values)
                                .draw();
                            });                      
                        }
                    }
                    //Save user input filter in Component map attribut 
                    component.set("v.filterValues", mapFilterValues);                    
                    
                });
                
            });

            
            
            table = $("#tableAvenantId").DataTable({
                "initComplete": function (settings, json) {
                    component.set("v.spinner", false);
                    //
                    
                },
                processing: true,
                scrollY: '90vh',
                scrollX: true,
                scrollCollapse: true,
                //deferRender: true,
                orderCellsTop: true,
                stateSave: true,
                //responsive: true,
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tout"]],
                iDisplayLength: component.get("v.selectedAvtLenght"),
                pagingType: "full_numbers",               
                language: {
                    sProcessing: "Traitement en cours...",
                    sSearch: "",
                    searchPlaceholder: "Rechercher",
                    sLengthMenu: "Afficher _MENU_ &nbsp;",
                    sInfo:
                    "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments&nbsp;",
                    sInfoEmpty:
                    "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:
                    "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix: "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable: "Aucune donn&eacute;e disponible dans le tableau",
                    oPaginate: {
                        sFirst: "Premier",
                        sPrevious: "Pr&eacute;c&eacute;dent",
                        sNext: "Suivant",
                        sLast: "Dernier"
                    },
                    oAria: {
                        sSortAscending:
                        ": activer pour trier la colonne par ordre croissant",
                        sSortDescending:
                        ": activer pour trier la colonne par ordre d&eacute;croissant"
                    },
                    select: {
                        rows: {
                            _: "&nbsp;",
                            0: "&nbsp;",
                            1: "&nbsp;"
                        }
                    }
                },
                data: newarrayAven,
                columnDefs: varcolumnDefs,
                select: {
                    style: "multi",
                    selector: 'input[type="checkbox"]'
                },
                // order: [[1, "asc"]],
                "order": [[groupColumn, 'asc']],
                
                
                "headerCallback": function (thead, data, start, end, display) {
                    let lastPageNumber = component.get("v.lastPageNumber");
                    // $('#tableAvenantId_wrapper').find('th').eq(0).html( 'Page '+ component.get("v.lastPageNumber") );                        
                    
                },
                
                "drawCallback": function (settings) {
                    var api = this.api();
                    console.log('api', api)
                    var rows = api.rows({ page: 'current' }).nodes();
                    var last = null;
                    
                    console.log('api.column(2, { page: \'current\' }).data()', api.column(2, { page: 'current' }).data())
                    
                    api.column(2, { page: 'current' }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="99">Contrat : ' + group + '</td></tr>'
                            );
                            last = group;
                        }
                    });
                },
                
                //Dynamic row color (if Amendement is treated => set backGroundColor)
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {         
                    console.log('aData.CompletedAmadement__c -----> ', aData.CompletedAmadement__c);
                    console.log('aData.TECH_Excluded__c -----> ', aData.TECH_Excluded__c);
                    console.log('aData.IsUnderContract__c -----> ', aData.IsUnderContract__c)  
                    console.log('aData ----->> ', aData)                
                    if (aData.TECH_Excluded__c) { //== Rank off column : Face_Face__c                 
                        helper.setRowBackgroundColor('TECH_Excluded', nRow)
                    }
                    else if (aData.IsUnderContract__c) { //== Rank off column : IsUnderContract__c     
                        helper.setRowBackgroundColor('IsUnderContract', nRow)
                    }
                    else if (aData.Face_face__c && aData.CompletedAmadement__c) { //== Rank off column : Face_face__c                 
                        helper.setRowBackgroundColor('Face_face', nRow)
                    }
                    else if (aData.CompletedAmadement__c) { //== Rank off column : CompletedAmadement__c
                        helper.setRowBackgroundColor('CompletedAmadement', nRow)
                    }
                    else if (aData.Face_face__c ) { //== Rank off column : Face_face__c                 
                        helper.setRowBackgroundColor('Face_face', nRow)
                    }
                    
                }
                
            });
            
            $('#tableAvenantId tbody').off('click');
            $('#tableAvenantId tbody').off('change');
            
            // Add event listener AllselectCheckbox forget selected contract
            // do this on load *only*
            $('#tableAvenantId_wrapper tr th:first-child').change(function () {
                var data = table.rows({ selected: true }).data();
                helper.getSelectedAvenant(component, data);
                //Alert when all contracts are selected
                
                let numSelected = data.length;
                if (numSelected > 0) {
                    $('#selectedRecordsNumb_').text( data.length +' ligne(s) séléctionnée(s)');
                    let filiere = data[0].Filiere__c != null ? data[0].Filiere__c : '';
                    //helper.toastdMessage("Augmentation tarifaire", "Vous avez sélectionné tous les "+numSelected+ " contrats de la filiale "+filiale+ " et filière "+filiere , "warning");
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        duration: '5000',
                        key: 'info_alt',
                        type: 'info',
                        mode: 'pester',
                        message: "Vous avez sélectionné tous les " + numSelected + " Avenants de la filière " + filiere
                    });
                    toastEvent.fire();
                }else {                   
                    $('#selectedRecordsNumb_').text( 0 +' ligne(s) séléctionnée(s)');                            
                }
            });
            
            // Add event listener checkbox forget selected contract
            $("#tableAvenantId_wrapper tbody").on("change", "td.select-checkbox", function (event) {
                //
                var data = table.rows({ selected: true }).data();
                if(event.target.checked) {
                    component.get("v.selectedRows").push(event.target.parentElement.parentElement)
                } else {
                    let index = component.get("v.selectedRows").indexOf(event.target.parentElement.parentElement);
                    if(index > -1) {
                        component.get("v.selectedRows").splice(index, 1)
                    }
                }
                helper.getSelectedAvenant(component, data);
                $('#selectedRecordsNumb_').text( data.length +' ligne(s) séléctionnée(s)');
            });
            
            // Add event listener for opening Edit pop up 
            $("#editAvenantButton").on("click", function () {
                //                 
                var selectedListAv = component.get("v.selectedListAvenant");
                if (component.get("v.selectedListAvenant").length > 0) {
                    helper.openEditModal(component, event, helper, component.get("v.selectedListAvenant"));
                } else {
                    helper.toastdMessage(" Modification/Visualisation", "Veuillez sélectionner au moins un contrat.", "warning");
                }
                
            });
            
            // Add event listener for opening and closing Pop updetails
            $("#tableAvenantId_wrapper tbody").on("click", "td.details-popup", function () {
                //
                var tr = $(this).closest("tr");
                //var tdi = tr.find("i.fa");
                var row = table.row(tr);
                var rowDatas = [];
                var d = row.data();
                rowDatas.push(d);
                helper.openEditModal(component, event, helper, rowDatas);
            });
            
            
            // Add event listener for opening and closing details
            $("#tableAvenantId_wrapper tbody").on("click", "td.details-avenant", function () {
                var tr = $(this).closest("tr");
                var tdi = tr.find("i.dec");
                var row = table.row(tr);
                
                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass("shown");
                    tdi.first().removeClass("fa-minus-square");
                    tdi.first().addClass("fa-plus-square");
                } else {
                    // Open this row
                    row.child(helper.formatDetail(component, event, helper, row.data())).show();
                    tr.addClass("shown");
                    tdi.first().removeClass("fa-plus-square");
                    tdi.first().addClass("fa-minus-square");
                }
            });
            
            //get lenght showed elements  
            $("select[name='tableAvenantId_length']").on("change", function () {
                let selectedAvtLenght = $(this).children("option:selected").val();                                     
                if(selectedAvtLenght == -1){
                    var allRecords = component.get("v.lististAvenant").length;
                    component.set("v.selectedAvtLenght", allRecords); 
                }else{
                    component.set("v.selectedAvtLenght", selectedAvtLenght);
                }  
                
            });
            
            
            $("#tableAvenantId_wrapper").on('stateLoadParams.dt', function (e, settings, data) { 
                //to trigger filter (simulate user filtering) 
                let valTemp = $('#1').val(); 
                $('#1').val(valTemp);                                                 
            } );
            
            $('#tableAvenantId_wrapper').on('stateSaveParams.dt', function (e, settings, data) {
                data.filters= component.get("v.filterValues");
                //data.search.search = "";                       
            }); 
            
        }, 500);
    },
    
    updateAvenant: function (component, event, helper) {
        if (component.get("v.selectedListAvenant").length == 1) {
            helper.openEditModal(component, event, helper, component.get("v.selectedListAvenant"));
        } else if (component.get("v.selectedListAvenant").length > 1) {
            helper.toastdMessage(" Modification/Visualisation", "Veuillez utiliser l'onglet Avenants pour la modification en masse des avenants.", "warning");
        } else {
            helper.toastdMessage(" Modification/Visualisation", "Veuillez sélectionner un avenant.", "warning");
        }
    },
    
    
    getSelectedAvenant: function (component, data) {
        var data = data;
        var newarrayAven = [];
        var selectedListAvenantIds = [];
        for (var i = 0; i < data.length; i++) {
            console.log("data >> ", data[i].Name);
            newarrayAven.push(data[i]);
            selectedListAvenantIds.push(data[i].Id);
        }
        component.set("v.selectedListAvenant", newarrayAven);
        component.set("v.selectedListAvenantIds", selectedListAvenantIds);
        console.log(
            "td.select-checkbox.selectedListAvenant >> ",
            component.get("v.selectedListAvenant")
        );
        
    },

    setRowBackgroundColor: function(status, row) {
        let color;
        switch(status) {
            case 'IsUnderContract':
                color = '#F36088'
                break
            case 'CompletedAmadement':
                color = '#8ac78a'
                break
            case 'Face_face':
                color = '#ffb962'
                break
            case 'TECH_Excluded':
                color = '#a3a3a3'
                break
            case 'blank':
                color = '#FFFFFF'
                break
            default:
                break

        }
        $('td', row).css('background-color', color);
    },

    setUnderContract: function(component, event, helper) {
        var action = component.get("c.setAmendmentsUnderContract");
        action.setParams({
            listAmendments: component.get("v.selectedListAvenant"),
            enableUnderContract: true
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == 'SUCCESS') {
                helper.setRowBackgroundColor('IsUnderContract', component.get("v.selectedRows"))
                helper.updateSelectedAvenant(component, event, helper)
                helper.refreshContracts(component, event, helper)
            }
        });
        $A.enqueueAction(action);
    },

    unsetUnderContract: function(component, event, helper) {
        var action = component.get("c.setAmendmentsUnderContract");
        action.setParams({
            listAmendments: component.get("v.selectedListAvenant"),
            enableUnderContract: false
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == 'SUCCESS') {
                
            }
        });
        $A.enqueueAction(action);
    },
    
    handleHeaderPickListEvent: function (component, event, helper) {
        //Reset savedStatus of datatable
        helper.initState(component, event); 
        var filiale = event.getParam("filiale");
        var filiere = event.getParam("filiere");
        
        // set the handler attributes based on event data 
        component.set("v.filiale", filiale);
        component.set("v.filiere", filiere);
        
        console.log("ContratListComponent.filiere >> ", component.get("v.filiere"));
    },
    
    //method to clear datatable state
    initState: function (component, event) {
        component.set("v.resetSaveStatus", true);       
    },
    
    fireEvent: function (component, event, helper) {
        console.log("fireEvent--- contractList Event  fire..");
        // Get the  event by using the
        // name value from aura:registerEvent
        var appEvent = $A.get("e.c:LE_ContractsList_AugTarif");
        var selectedListCont = component.get("v.selectedListCont");
        console.log(
            "LE_ContractsList_AugTarif.selectedListCont :",
            selectedListCont
        );
        appEvent.setParams({
            selectedListCont: selectedListCont
        });
        appEvent.fire();
    },

    refreshContracts: function(component, event, helper) {
        console.log("fireEvent--- contractList Event  fire..");
        // Get the  event by using the
        // name value from aura:registerEvent
        var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");
        var selectedListCont = component.get("v.selectedListCont");
        console.log(
            "LE_ContractsList_AugTarif.selectedListCont :",
            selectedListCont
        );
        appEvent.setParams({
            isCancel: false,
            isAvenantUpdate: true
        });
        appEvent.fire();
    },
    
    openEditForm: function (component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": "dismissible",
            "title": "Success!",
            "message": "The record has been Saved successfully."
        });
        toastEvent.fire();
    },
    openEditModal: function (component, event, helper, selectedListAv) {
        //Dynamic creation of lightningModalChild component and appending its markup in a div
        $A.createComponent('c:LC_RecordDetail_AugTarif', {
            'headerText': 'Détails de l\'avenant ',
            'selectedListSObject': selectedListAv,
            'sObjectName': 'Amendment__c'
        },
                           function (modalComponent, status, errorMessage) {
                               if (status === "SUCCESS") {
                                   //Appending the newly created component in div
                                   var body = component.find('showEditModal').get("v.body");
                                   body.push(modalComponent);
                                   component.find('showEditModal').set("v.body", body);
                               } else if (status === "INCOMPLETE") {
                                   console.log('Server issue or client is offline.');
                               } else if (status === "ERROR") {
                                   console.log('error');
                               }
                           }
                          );
    },
    
    toastdMessage: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": 'dismissible',
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },
    
    showIncreasePurcentForm: function (component, event, helper, action, headerText) {
        
        var selectedListCont = component.get("v.selectedListCont");
        var selectedListAvenant = component.get("v.selectedListAvenant");

        for (var k=0; k < selectedListAvenant.length; k++) {
            if (selectedListAvenant[k].PriceNextyearFromERP__c) {
                helper.toastdMessage("Augmentation par pourcentage", "Vous ne pouvez pas augmenter un avenant dont le tarif provient de l'ERP.", "warning");
                return;
                }
        }
        
        if (selectedListAvenant.length > 0) {
            let locked = false;
            let isPriceCurrentYearZero = false;
            let amendmentNamePriceZero = '';
            let isCompletedAmendent = false;
            
            selectedListAvenant.forEach(function (amend) {
                if (amend.PriceCurrentYear__c == 0) {
                    console.log(amend);
                    amendmentNamePriceZero = amend.Name;
                    isPriceCurrentYearZero = true;
                }
                if(amend.CompletedAmadement__c == true){
                    isCompletedAmendent = true;
                }
            });
            let isFaceFace = false;
            let isExclude = false;
            //check if contract are faceface
            selectedListCont.forEach(function (cont) {
                if (cont.Face_Face__c == true) {
                    console.log(cont);
                    isFaceFace = true;
                }
                if (cont.TECH_Excluded__c == true) {
                    isExclude = true;
                }
            });
            
            if (isPriceCurrentYearZero) {
                helper.toastdMessage(" Augmentation par pourcentage", "Le prix N+1 de l'avenant " + amendmentNamePriceZero + " sera nul car le prix N est lui aussi nul ; veuillez saisir un prix fixe pour cette ligne.", "warning");
            } else if (isExclude) {
                helper.toastdMessage(" Augmentation par pourcentage", "Vous avez sélectionné un avenant exclu et ne pouvez donc pas l'augmenter. Veuillez d'abord réinitialiser le contrat.", "warning");
            } else if (isCompletedAmendent) {
                helper.toastdMessage(" Augmentation par pourcentage", "Vous avez sélectionné un avenant traité. Veuillez d'abord le réinitialiser", "warning");
            } else {
                
                $A.createComponent('c:LC_InputModalForm_AugTarif', {
                    'headerText': headerText,
                    'selectedListCont': selectedListCont,
                    'selectedListAvenant': selectedListAvenant,
                    'action': action
                },
                                   function (modalComponent, status, errorMessage) {
                                       if (status === "SUCCESS") {
                                           
                                           var body = component.find('showInputForm').get("v.body");
                                           body.push(modalComponent);
                                           component.find('showInputForm').set("v.body", body);
                                       } else if (status === "INCOMPLETE") {
                                           console.log('Server issue or client is offline.');
                                       } else if (status === "ERROR") {
                                           console.log('error');
                                       }
                                   }
                                  );
            }
        } else {
            helper.toastdMessage("Augmentation par pourcentage", "Veuillez sélectionner au moins un avenant.", "warning");
        }
        
    },
    
    showFixedPriceForm: function (component, event, helper, action, headerText) {
        // 
        var selectedListCont = component.get("v.selectedListCont");
        var selectedListAvenant = component.get("v.selectedListAvenant");
        
        let isFaceFace = false;
        let isExclude = false;

        for (var k=0; k < selectedListAvenant.length; k++) {
            if (selectedListAvenant[k].PriceNextyearFromERP__c) {
                helper.toastdMessage("Augmenter prix fixe", "Vous ne pouvez pas augmenter un avenant dont le tarif provient de l'ERP.", "warning");
                return;
                }
        }
        
        //check if contract are faceface
        selectedListCont.forEach(function (cont) {
            if (cont.Face_Face__c == true) {
                console.log(cont);
                isFaceFace = true;
            }
            if (cont.TECH_Excluded__c == true) {
                isExclude = true;
            }
        });
        
        if (isExclude) {
            helper.toastdMessage(" Augmenter prix fixe", "Vous avez sélectionné un avenant exclu et ne pouvez donc pas l'augmenter. Veuillez d'abord réinitialiser le contrat.", "warning");
        } else if (selectedListAvenant.length > 0) {
            //Dynamic creation of lightningModalChild component and appending its markup in a div
            let isCompletedAmendent = false;
            selectedListAvenant.forEach(function (amend) {
                if(amend.CompletedAmadement__c == true){
                    isCompletedAmendent = true;
                }
            });
            
            if(isCompletedAmendent) {
                helper.toastdMessage(" Augmenter prix fixe", "Vous avez sélectionné un avenant traité. Veuillez d'abord le réinitialiser", "warning");
            }
            else {
                $A.createComponent('c:LC_InputModalForm_AugTarif', {
                    'headerText': headerText,
                    'action': action,
                    'selectedListAvenant': selectedListAvenant
                    
                },
                                   function (modalComponent, status, errorMessage) {
                                       if (status === "SUCCESS") {
                                           //Appending the newly created component in div
                                           var body = component.find('showInputForm').get("v.body");
                                           body.push(modalComponent);
                                           component.find('showInputForm').set("v.body", body);
                                       } else if (status === "INCOMPLETE") {
                                           console.log('Server issue or client is offline.');
                                       } else if (status === "ERROR") {
                                           console.log('error');
                                       }
                                   }
                                  );
            }
            
        } else {
            helper.toastdMessage("Augmenter prix fixe", "Veuillez sélectionner au moins un avenant.", "warning");
        }
        
    },
    
    showNoIncreaseForm: function (component, event, helper, action, headerText) {
        // 
        var selectedListCont = component.get("v.selectedListCont");
        var selectedListAvenant = component.get("v.selectedListAvenant");
        
        let isFaceFace = false;
        let isExclude = false;

        for (var k=0; k < selectedListAvenant.length; k++) {
            if (selectedListAvenant[k].PriceNextyearFromERP__c) {
                helper.toastdMessage("Ne pas augmenter", "Vous ne pouvez pas augmenter un avenant dont le tarif provient de l'ERP.", "warning");
                return;
                }
        }

        //check if contract are faceface
        selectedListCont.forEach(function (cont) {
            if (cont.Face_Face__c == true) {
                console.log(cont);
                isFaceFace = true;
            }
            if (cont.TECH_Excluded__c == true) {
                isExclude = true;
            }
        });
        if (isExclude) {
            helper.toastdMessage("Ne pas augmenter", "Vous avez sélectionné un avenant exclu et ne pouvez donc pas l'augmenter. Veuillez d'abord réinitialiser le contrat.", "warning");
        } else if (selectedListAvenant.length > 0) {
            let isCompletedAmendent = false;
            selectedListAvenant.forEach(function (amend) {
                if(amend.CompletedAmadement__c == true){
                    isCompletedAmendent = true;
                }
            });
            if(isCompletedAmendent){
                helper.toastdMessage("Ne pas augmenter", "Vous avez sélectionné un avenant traité. Veuillez d'abord le réinitialiser", "warning");
            } else {
                //Dynamic creation of lightningModalChild component and appending its markup in a div
                $A.createComponent('c:LC_InputModalForm_AugTarif', {
                    'headerText': headerText,
                    'action': action,
                    'selectedListAvenant': selectedListAvenant
                    
                },
                                   function (modalComponent, status, errorMessage) {
                                       if (status === "SUCCESS") {
                                           //Appending the newly created component in div
                                           var body = component.find('showInputForm').get("v.body");
                                           body.push(modalComponent);
                                           component.find('showInputForm').set("v.body", body);
                                       } else if (status === "INCOMPLETE") {
                                           console.log('Server issue or client is offline.');
                                       } else if (status === "ERROR") {
                                           console.log('error');
                                       }
                                   }
                                  );
            }
            
        } else {
            helper.toastdMessage("Ne pas augmenter", "Veuillez sélectionner au moins un avenant.", "warning");
        }
        
    },
    
    showResetForm: function (component, event, helper, action, headerText) {
        var selectedListAvenant = component.get("v.selectedListAvenant");
        var selectedListCont = component.get("v.selectedListCont");
        let isPriceNextYearSelected = false
        let warningText = ''

        for (var k=0; k < selectedListAvenant.length; k++) {
            if (selectedListAvenant[k].PriceNextyearFromERP__c) {
                helper.toastdMessage("Réinitaliser", "Vous ne pouvez pas réinitialiser un avenant dont le tarif provient de l'ERP.", "warning");
                return;
                }
        }

        var action2 = component.get("c.doAmendmentsHaveNextYearPrice");
        action2.setParams({
            listAmendments: selectedListAvenant
        });
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if(state == 'SUCCESS') {
                isPriceNextYearSelected = response.getReturnValue()
                if(isPriceNextYearSelected) {
                    warningText = "Un ou plusieurs avenant(s) sélectionné(s) contiennent un tarif N+1"
                }
                
                if (selectedListAvenant.length > 0) {
                    let isFaceFace = false;
                    let isExclude = false;
                    let IsUnderContract = false;
                    selectedListCont.forEach(function (cont) {
                       
                        var avenant= [] ;
                        avenant = cont.Avenants__r;
                        if(cont.TreatedContract__c){
                            for(var i= 0; i <avenant.length; i++){
                                if(avenant[i].IsUnderContract__c == true ){
                                    IsUnderContract = true;
                                }
                            }
                        }

                        if (cont.Face_Face__c == true) {
                            console.log(cont);
                            isFaceFace = true;
                        }
                        if (cont.TECH_Excluded__c == true) {
                            isExclude = true;
                        }
                    });
                    
                    if (isFaceFace) {
                        helper.toastdMessage("Réinitialiser", "Vous avez sélectionné un avenant en Face à Face. Pour le réinitialiser, veuillez réinitialiser le contrat.", "warning");
                    }else if (IsUnderContract) {
                        helper.toastdMessage("Réinitialiser", "Vous avez sélectionné un avenant sous contrat. Pour le réinitialiser, veuillez réinitialiser le contrat.", "warning");
                    } else if (isExclude) {
                        helper.toastdMessage("Réinitialiser", "Vous avez sélectionné un avenant exclu. Pour le réinitialiser, veuillez réinitialiser le contrat.", "warning");
                    } else {
                        $A.createComponent('c:LC_InputModalForm_AugTarif', {
                            'headerText': headerText,
                            'action': action,
                            'selectedListAvenant': selectedListAvenant,
                            'warningText': warningText,
                            'endMessage': 'avenants'
                        },
                                           function (modalComponent, status, errorMessage) {
                                               if (status === "SUCCESS") {
                                                    //Appending the newly created component in div
                                                    var body = component.find('showInputForm').get("v.body");
                                                    body.push(modalComponent);
                                                    component.find('showInputForm').set("v.body", body);
                                                } else if (status === "INCOMPLETE") {
                                                   console.log('Server issue or client is offline.');
                                               } else if (status === "ERROR") {
                                                   console.log('error');
                                               }
                                           }
                                          );
                    }
                } else {
                    helper.toastdMessage("Réinitialiser", "Veuillez sélectionner au moins un avenant.", "warning");
                }
            }
        });
        $A.enqueueAction(action2)       
    },
    showExcludeForm: function (component, event, helper, action, headerText) {
        // 
        var selectedListAvenant = component.get("v.selectedListAvenant");
        var selectedListCont = component.get("v.selectedListCont");
        
        if (selectedListAvenant.length > 0) {
            let isFaceFace = false;
            let isCompletedAmendent = false;
            
            // selectedListCont.forEach(function (cont) {
            //     if (cont.Face_Face__c == true) {
            //         console.log(cont);
            //         isFaceFace = true;
            //     }
            // });
            
            selectedListAvenant.forEach(function (amend) {
                console.log('amend.Contrat2__r.Face_Face__c',amend.Contrat2__c)
                var hasContNoFF = selectedListCont.find(e => e.Id == amend.Contrat2__c);
                console.log('hasContNoFF',hasContNoFF.Face_Face__c)
                if(amend.CompletedAmadement__c && !hasContNoFF.Face_Face__c){
                    isCompletedAmendent = true;
                }
            });
            
            if (isFaceFace) {
                helper.toastdMessage("Exclure", "Vous avez sélectionné un avenant en Face à Face. Pour l'exclure, veuillez exclure le contrat.", "warning");
            } else if (isCompletedAmendent) {
                helper.toastdMessage("Exclure", "Vous avez sélectionné un avenant traité et hors FaF. Veuillez d'abord le réinitialiser.", "warning");
            } else {
                $A.createComponent('c:LC_InputModalForm_AugTarif', {
                    'headerText': headerText,
                    'action': action,
                    'selectedListAvenant': selectedListAvenant
                },
                                   function (modalComponent, status, errorMessage) {
                                       if (status === "SUCCESS") {
                                           //Appending the newly created component in div
                                           var body = component.find('showInputForm').get("v.body");
                                           body.push(modalComponent);
                                           component.find('showInputForm').set("v.body", body);
                                       } else if (status === "INCOMPLETE") {
                                           console.log('Server issue or client is offline.');
                                       } else if (status === "ERROR") {
                                           console.log('error');
                                       }
                                   }
                                  );
            }
            
        } else {
            helper.toastdMessage("Exclure", "Veuillez sélectionner au moins un avenant.", "warning");
        }
        
    },
    
    /*showFaceToFaceForm: function (component, event, helper, action, headerText) {
        // 
        var selectedListCont = component.get("v.selectedListCont");
        var selectedListAvenant = component.get("v.selectedListAvenant");


        if (selectedListAvenant.length > 0) {
            //Dynamic creation of lightningModalChild component and appending its markup in a div
            $A.createComponent('c:LC_InputModalForm_AugTarif', {
                'headerText': headerText,
                'action': action,
                'selectedListCont': selectedListCont,
                'selectedListAvenant': selectedListAvenant

            },
                function (modalComponent, status, errorMessage) {
                    if (status === "SUCCESS") {
                        //Appending the newly created component in div
                        var body = component.find('showInputForm').get("v.body");
                        body.push(modalComponent);
                        component.find('showInputForm').set("v.body", body);
                    } else if (status === "INCOMPLETE") {
                        console.log('Server issue or client is offline.');
                    } else if (status === "ERROR") {
                        console.log('error');
                    }
                }
            );
        } else {
            helper.toastdMessage("Augmenter en face à face", "Veuillez sélectionner au moins un avenant.", "warning");
        }

    },*/
    
    showValidationForm: function (component, event, helper, action, headerText) {
        
        var selectedListCont = component.get("v.selectedListCont");
        var selectedListAvenant = component.get("v.selectedListAvenant");
        
        var count = 0;
        var countTemplate = 0;
        var avenantName;
        
        let isFaceFace = false;
        let isExclude = false;
        
        //check if contract are faceface
        selectedListCont.forEach(function (cont) {
            if (cont.Face_Face__c == true) {
                console.log(cont);
                isFaceFace = true;
            }
            if (cont.TECH_Excluded__c == true) {
                isExclude = true;
            }
        });
        
        if (isExclude) {
            helper.toastdMessage(" Augmentation par pourcentage", "Vous avez sélectionné un avenant exclu et ne pouvez donc pas l'augmenter. Veuillez d'abord réinitialiser le contrat.", "warning");
        } else if (selectedListAvenant.length > 0) {
            let isCompletedAmendent = false;
            selectedListAvenant.forEach(function (amend) {
                if(amend.CompletedAmadement__c == true){
                    isCompletedAmendent = true;
                }
            });
            if(isCompletedAmendent){
                helper.toastdMessage(" Augmentation par pourcentage", "Vous avez sélectionné un avenant traité. Veuillez d'abord le réinitialiser.", "warning");
            }else{
                // 
                //Check if all amendement are ok for validation
                for (var i = 0; i < selectedListAvenant.length; i++) {
                    console.log("selectedListAvenant >> ", selectedListAvenant[i].Name);
                    
                    if (!selectedListAvenant[i].CompletedAmadement__c) {
                        count++;
                        //Vous avez toujours des avenant
                        //
                    } else if (selectedListAvenant[i].Contrat2__r.Type_de_convention__c == null) {
                        countTemplate++;
                        avenantName = selectedListAvenant[i].Contrat2__r.Name;
                    }
                    
                }
                
                if (count > 0) { // Nombre d'Avenants à traiter restant
                    helper.toastdMessage("Soumettre pour validation", "Vous avez toujours " + count + " avenants non traités. Veuillez les traiter avant de soumettre pour approbation.", "warning");
                } else if (countTemplate > 0) {
                    helper.toastdMessage("Soumettre pour validation", "Le modèle de convention n\'est pas choisi pour le Contrat de l'\avenant " + avenantName, "warning");
                } else {
                    //Dynamic creation of lightningModalChild component and appending its markup in a div
                    $A.createComponent('c:LC_InputModalForm_AugTarif', {
                        'headerText': headerText,
                        'action': action,
                        'selectedListCont': selectedListCont,
                        'selectedListAvenant': selectedListAvenant
                        
                    },
                                       function (modalComponent, status, errorMessage) {
                                           if (status === "SUCCESS") {
                                               //Appending the newly created component in div
                                               var body = component.find('showInputForm').get("v.body");
                                               body.push(modalComponent);
                                               component.find('showInputForm').set("v.body", body);
                                           } else if (status === "INCOMPLETE") {
                                               console.log('Server issue or client is offline.');
                                           } else if (status === "ERROR") {
                                               console.log('error');
                                           }
                                       }
                                      );
                }
            }
        } else {
            helper.toastdMessage("Soumettre pour validation", "Veuillez sélectionner au moins un avenant.", "warning");
        }
        
    },
    
    //Set the current preference for the key filiale+filiere
    setColumnsDefs: function (component, event, helper, filiale, filiere, DEFINITIONS, COLUMNS) {
        // 
        console.log('LC_ContractsList_AugTarif---> Start setColumnsDefs..');
        /*const filiales = ["Séché Eco Industries Changé", "Tredi Hombourg", "Sotrefi", "Triadis", "Tredi S"];
        const filieres = ["Stockage DD", "Physico", "Plateforme de transit", "Stockage DND", "Quai de transfert", "Plateforme de transit", "Incinération DD"];
        component.set("v.isDefaultDisplayColumns", true);*/
        var columnDefs = [];
        
        columnDefs.push({
            targets: 0,
            data: null,
            defaultContent: "",
            orderable: false,
            width: '8px',
            className: "select-checkbox",
            checkboxes: {
                selectRow: true,
                stateSave: false
            }
        });
        
        function preDefined(field, index) {
            return {
                data: field,
                defaultContent: "",
                targets: index,
                orderable: true
            }
        }
        
        console.log('Column => '+JSON.stringify(COLUMNS));
        
        COLUMNS.forEach(e => {
            let index = COLUMNS.indexOf(e)+1;
            let constructed = {};
                        let param = DEFINITIONS.find(d => (e == 'Traité') ? d.QualifiedApiName == 'Trait_avenant' : ((e == 'Contrat') ? d.QualifiedApiName == 'Contrat_avenant' : d.Label == e));
        
        // it will predefin the node
        constructed = preDefined(param.Data__c, index);
        
        // checking if there is a function nested
        if(param.Function__c == 'Yes' && param.On__c) {
            
            constructed[param.On__c] = function(data) { let t = data; let ret = eval(param.evalCode__c); return ret; }
            
        } else if(param.Function__c == 'No' && param.On__c) {
            console.log('func NO => '+param.evalCode__c);
            if(param.evalCode__c.length > 0) {
                console.log(eval(param.evalCode__c));
                constructed[param.On__c] = eval(param.evalCode__c);
            }
        }
        
        constructed.className = (param.ClassName__c) ? param.ClassName__c : '';
        
        console.log('constructed => '+JSON.stringify(constructed));
        
        columnDefs.push(constructed);
    });
    
    /*if (filieres[1] === filiere) {//if filiere = physico
            component.set("v.isColumnsDisplay", 0);
            component.set("v.isDefaultDisplayColumns", false);

            columnDefs = [
                {
                    "visible": true,
                    "targets": 2
                },

                {
                    targets: 0,
                    data: null,
                    defaultContent: "",
                    orderable: false,
                    className: "select-checkbox",
                    checkboxes: {
                        selectRow: true,
                        stateSave: false
                    }
                },

                {
                    data: "Name",
                    defaultContent: "",
                    targets: 1,
                    className: "details-avenant",
                    orderable: true,
                    render: function (data) {
                        return '<i class="dec fa fa-plus-square" aria-hidden="true">' + data + '</i>';
                    }
                },

                {
                    data: "Contrat2__r.Name",
                    defaultContent: "",
                    targets: 2,
                    orderable: true
                },

                {
                    data: "ThirdPartyProducerName__c",
                    defaultContent: "",
                    targets: 3,
                    orderable: true
                },

                {
                    data: "WasteLabel__c",
                    defaultContent: "",
                    targets: 4,
                    orderable: true
                },

                {
                    data: "SheetNumber__c",
                    defaultContent: "",
                    targets: 5,
                    orderable: true
                },

                {
                    data: "Conditionnement__c",
                    defaultContent: "",
                    targets: 6,
                    orderable: true
                },

                {
                    data: "ArticleCode__c",
                    defaultContent: "",
                    targets: 7,
                    orderable: true
                },

                {
                    data: "PriceCurrentYear__c",
                    defaultContent: "",
                    targets: 8,
                    orderable: true
                },

                {
                    data: "AugmentationRatio__c",
                    defaultContent: "",
                    targets: 9,
                    orderable: true
                },

                {
                    data: "PriceNextYear__c",
                    defaultContent: "",
                    targets: 10,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(2);
                        }
                    }
                },

                {
                    data: "TreatmentQuantityCurrentYear__c",
                    defaultContent: "",
                    targets: 11,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(3);
                        }
                    }
                },

                {
                    data: "TreatmentQuantityNextYear__c",
                    defaultContent: "",
                    targets: 12,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(3);
                        }
                    }
                },

                {
                    data: "TreatmentRange__c",
                    defaultContent: "",
                    targets: 13,
                    orderable: true
                },


                {
                    data: "AnalyticSection__c",
                    defaultContent: "",
                    targets: 14,
                    orderable: true
                },

                {
                    data: "Nature__c",
                    defaultContent: "",
                    targets: 15,
                    orderable: true,
                },

                {
                    data: "CompletedAmadement__c",
                    defaultContent: "",
                    targets: 16,
                    orderable: true,
                    render: function (data) {
                        if (data == true) {
                            return 'Oui';
                        } else {
                            return 'Non';
                        }

                    },
                }
            ];

        } else if (filieres[3] === filiere || filieres[4] === filiere) {//if filiere =  Stockage DND / Quai de transfert
            component.set("v.isColumnsDisplay", 1);
            component.set("v.isDefaultDisplayColumns", false);

            columnDefs = [
                {
                    "visible": true,
                    "targets": 2
                },

                {
                    targets: 0,
                    data: null,
                    defaultContent: "",
                    orderable: false,
                    className: "select-checkbox",
                    checkboxes: {
                        selectRow: true,
                        stateSave: false
                    }
                },

                {
                    data: "Name",
                    defaultContent: "",
                    targets: 1,
                    className: "details-avenant",
                    orderable: true,
                    render: function (data) {
                        return '<i class="dec fa fa-plus-square" aria-hidden="true">' + data + '</i>';
                    }
                },

                {
                    data: "Contrat2__r.Name",
                    defaultContent: "",
                    targets: 2,
                    orderable: true
                },

                {
                    data: "ThirdPartyProducerName__c",
                    defaultContent: "",
                    targets: 3,
                    orderable: true
                },

                {
                    data: "WasteLabel__c",
                    defaultContent: "",
                    targets: 4,
                    orderable: true
                },

                {
                    data: "ArticleCode__c",
                    defaultContent: "",
                    targets: 5,
                    orderable: true
                },

                {
                    data: "AnalyticSection__c",
                    defaultContent: "",
                    targets: 6,
                    orderable: true
                },

                {
                    data: "PriceCurrentYear__c",
                    defaultContent: "",
                    targets: 7,
                    orderable: true
                },

                {
                    data: "PriceNextYear__c",
                    defaultContent: "",
                    targets: 8,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(2);
                        }
                    }
                },

                {
                    data: "CompletedAmadement__c",
                    defaultContent: "",
                    targets: 9,
                    orderable: true,
                    render: function (data) {
                        if (data == true) {
                            return 'Oui';
                        } else {
                            return 'Non';
                        }

                    },
                },

                {
                    data: "AugmentationRatio__c",
                    defaultContent: "",
                    targets: 10,
                    orderable: true
                },

                {
                    data: "TreatmentQuantityCurrentYear__c",
                    defaultContent: "",
                    targets: 11,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(3);
                        }
                    }
                },

                {
                    data: "TreatmentQuantityNextYear__c",
                    defaultContent: "",
                    targets: 12,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(3);
                        }
                    }
                },

                {
                    data: "Planned_next_year__c",
                    defaultContent: "",
                    targets: 13,
                    orderable: true
                }

            ];

        } else if (filiale.includes(filiales[3])) {//Filiale :Triadis (tout ce qui contient "Triadis")
            component.set("v.isColumnsDisplay", 2);
            component.set("v.isDefaultDisplayColumns", false);

            columnDefs = [
                {
                    "visible": true,
                    "targets": 2
                },

                {
                    targets: 0,
                    data: null,
                    defaultContent: "",
                    orderable: false,
                    className: "select-checkbox",
                    checkboxes: {
                        selectRow: true,
                        stateSave: false
                    }
                },

                {
                    data: "ThirdPartyProducerName__c",
                    defaultContent: "",
                    targets: 1,
                    orderable: true
                },

                {
                    data: "Contrat2__r.Name",
                    defaultContent: "",
                    targets: 2,
                    orderable: true
                },

                {
                    data: "WasteLabel__c",
                    defaultContent: "",
                    targets: 3,
                    orderable: true
                },

                {
                    data: "Name",
                    defaultContent: "",
                    targets: 4,
                    className: "details-avenant",
                    orderable: true,
                    render: function (data) {
                        return '<i class="dec fa fa-plus-square" aria-hidden="true">' + data + '</i>';
                    }
                },

                {
                    data: "PriceCurrentYear__c",
                    defaultContent: "",
                    targets: 5,
                    orderable: true
                },

                {
                    data: "Conditionnement__c",
                    defaultContent: "",
                    targets: 6,
                    orderable: true
                },

                {
                    data: "PriceNextYear__c",
                    defaultContent: "",
                    targets: 7,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(2);
                        }
                    }
                },

                {
                    data: "AugmentationRatio__c",
                    defaultContent: "",
                    targets: 8,
                    orderable: true
                },

                {
                    data: "CompletedAmadement__c",
                    defaultContent: "",
                    targets: 9,
                    orderable: true,
                    render: function (data) {
                        if (data == true) {
                            return 'Oui';
                        } else {
                            return 'Non';
                        }

                    },
                },

                {
                    data: "Nature__c",
                    defaultContent: "",
                    targets: 10,
                    orderable: true,
                    visible: false
                },

                {
                    data: "TreatmentQuantityCurrentYear__c",
                    defaultContent: "",
                    targets: 11,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(3);
                        }
                    }
                },

                {
                    data: "ArticleCode__c",
                    defaultContent: "",
                    targets: 12,
                    orderable: true
                },

                {
                    data: "TreatmentQuantityNextYear__c",
                    defaultContent: "",
                    targets: 13,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(3);
                        }
                    }
                },

                {
                    data: "Planned_next_year__c",
                    defaultContent: "",
                    targets: 14,
                    orderable: true
                },

                {
                    data: "TreatmentRange__c",
                    defaultContent: "",
                    targets: 15,
                    orderable: true
                },

                {
                    data: "AnalyticSection__c",
                    defaultContent: "",
                    targets: 16,
                    orderable: true
                }


            ];


        } else if (filiales[0].includes(filiale) && filieres[0].includes(filiere)) {   //Filiale :SEI Changé, filiere : Stockage DD
            component.set("v.isColumnsDisplay", 3);
            component.set("v.isDefaultDisplayColumns", false);

            columnDefs = [
                {
                    "visible": true,
                    "targets": 2
                },

                {
                    targets: 0,
                    data: null,
                    defaultContent: "",
                    orderable: false,
                    className: "select-checkbox",
                    checkboxes: {
                        selectRow: true,
                        stateSave: false
                    }
                },

                {
                    data: "Name",
                    defaultContent: "",
                    targets: 1,
                    className: "details-avenant",
                    orderable: true,
                    render: function (data) {
                        return '<i class="dec fa fa-plus-square" aria-hidden="true">' + data + '</i>';
                    }
                },

                {
                    data: "Contrat2__r.Name",
                    defaultContent: "",
                    targets: 2,
                    orderable: true
                },

                {
                    data: "ThirdPartyProducerName__c",
                    defaultContent: "",
                    targets: 3,
                    orderable: true
                },

                {
                    data: "WasteLabel__c",
                    defaultContent: "",
                    targets: 4,
                    orderable: true
                },

                {
                    data: "ActifAcceptationContrat__c",
                    defaultContent: "",
                    targets: 5,
                    orderable: true
                },

                {
                    data: "Conditionnement__c",
                    defaultContent: "",
                    targets: 6,
                    orderable: true
                },

                {
                    data: "ArticleCode__c",
                    defaultContent: "",
                    targets: 7,
                    orderable: true
                },

                {
                    data: "PriceCurrentYear__c",
                    defaultContent: "",
                    targets: 8,
                    orderable: true
                },

                {
                    data: "AugmentationRatio__c",
                    defaultContent: "",
                    targets: 9,
                    orderable: true
                },

                {
                    data: "PriceNextYear__c",
                    defaultContent: "",
                    targets: 10,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(2);
                        }
                    }
                },

                {
                    data: "TreatmentQuantityCurrentYear__c",
                    defaultContent: "",
                    targets: 11,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(3);
                        }
                    }
                },

                {
                    data: "TreatmentQuantityNextYear__c",
                    defaultContent: "",
                    targets: 12,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(3);
                        }
                    }
                },

                {
                    data: "Planned_next_year__c",
                    defaultContent: "",
                    targets: 13,
                    orderable: true
                },

                {
                    data: "TreatmentRange__c",
                    defaultContent: "",
                    targets: 14,
                    orderable: true
                },

                {
                    data: "AnalyticSection__c",
                    defaultContent: "",
                    targets: 15,
                    orderable: true
                },

                {
                    data: "CompletedAmadement__c",
                    defaultContent: "",
                    targets: 16,
                    orderable: true,
                    render: function (data) {
                        if (data == true) {
                            return 'Oui';
                        } else {
                            return 'Non';
                        }

                    },
                }
            ];

        } else if ((filiales[1].includes(filiale) || filiales[2].includes(filiale)) && filieres[2].includes(filiere)) {//filiale : Tredi Hombourg / Sotrefi filiere : Plateforme de transit
            component.set("v.isColumnsDisplay", 4);
            component.set("v.isDefaultDisplayColumns", false);

            columnDefs = columnDefs = [
                {
                    "visible": true,
                    "targets": 2
                },

                {
                    targets: 0,
                    data: null,
                    defaultContent: "",
                    orderable: false,
                    className: "select-checkbox",
                    checkboxes: {
                        selectRow: true,
                        stateSave: false
                    }
                },

                {
                    data: "Name",
                    defaultContent: "",
                    targets: 1,
                    className: "details-avenant",
                    orderable: true,
                    render: function (data) {
                        return '<i class="dec fa fa-plus-square" aria-hidden="true">' + data + '</i>';
                    }
                },

                {
                    data: "Contrat2__r.Name",
                    defaultContent: "",
                    targets: 2,
                    orderable: true
                },

                {
                    data: "ThirdPartyProducerName__c",
                    defaultContent: "",
                    targets: 3,
                    orderable: true
                },

                {
                    data: "WasteLabel__c",
                    defaultContent: "",
                    targets: 4,
                    orderable: true
                },

                {
                    data: "ActifAcceptationContrat__c",
                    defaultContent: "",
                    targets: 5,
                    orderable: true
                },

                {
                    data: "Conditionnement__c",
                    defaultContent: "",
                    targets: 6,
                    orderable: true
                },

                {
                    data: "ArticleCode__c",
                    defaultContent: "",
                    targets: 7,
                    orderable: true
                },

                {
                    data: "PriceCurrentYear__c",
                    defaultContent: "",
                    targets: 8,
                    orderable: true
                },

                {
                    data: "AugmentationRatio__c",
                    defaultContent: "",
                    targets: 9,
                    orderable: true
                },

                {
                    data: "PriceNextYear__c",
                    defaultContent: "",
                    targets: 10,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(2);
                        }
                    }
                },

                {
                    data: "TreatmentQuantityCurrentYear__c",
                    defaultContent: "",
                    targets: 11,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(3);
                        }
                    }
                },

                {
                    data: "TreatmentQuantityNextYear__c",
                    defaultContent: "",
                    targets: 12,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(3);
                        }
                    }
                },

                {
                    data: "SheetNumber__c",
                    defaultContent: "",
                    targets: 13,
                    orderable: true
                },

                {
                    data: "LabelArticleCode__c",
                    defaultContent: "",
                    targets: 14,
                    orderable: true
                },

                {
                    data: "Nature__c",
                    defaultContent: "",
                    targets: 15,
                    orderable: true
                },

                {
                    data: "CompletedAmadement__c",
                    defaultContent: "",
                    targets: 16,
                    orderable: true,
                    render: function (data) {
                        if (data == true) {
                            return 'Oui';
                        } else {
                            return 'Non';
                        }

                    },
                }
            ];


        } else if (filiale.includes(filiales[4])) {//Filiale :  tout ce qui contient "Tredi S"
            component.set("v.isColumnsDisplay", 5);
            component.set("v.isDefaultDisplayColumns", false);
            columnDefs = [
                {
                    "visible": true,
                    "targets": 2
                },

                {
                    targets: 0,
                    data: null,
                    defaultContent: "",
                    orderable: false,
                    className: "select-checkbox",
                    checkboxes: {
                        selectRow: true,
                        stateSave: false
                    }
                },

                {
                    data: "Name",
                    defaultContent: "",
                    targets: 1,
                    className: "details-avenant",
                    orderable: true,
                    render: function (data) {
                        return '<i class="dec fa fa-plus-square" aria-hidden="true">' + data + '</i>';
                    }
                },

                
                {
                    data: "Contrat2__r.Name",
                    defaultContent: "",
                    targets: 2,
                    orderable: true
                },

                {
                    data: "ThirdPartyProducerName__c",
                    defaultContent: "",
                    targets:3,
                    orderable: true
                },

                {
                    data: "WasteLabel__c",
                    defaultContent: "",
                    targets: 4,
                    orderable: true
                },

                {
                    data: "Conditionnement__c",
                    defaultContent: "",
                    targets: 5,
                    orderable: true
                },

                {
                    data: "ArticleCode__c",
                    defaultContent: "",
                    targets: 6,
                    orderable: true
                },
               

                {
                    data: "PriceCurrentYear__c",
                    defaultContent: "",
                    targets: 7,
                    orderable: true
                },

               

                {
                    data: "PriceNextYear__c",
                    defaultContent: "",
                    targets: 8,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(2);
                        }
                    }
                },

                {
                    data: "CompletedAmadement__c",
                    defaultContent: "",
                    targets: 9,
                    orderable: true,
                    render: function (data) {
                        if (data == true) {
                            return 'Oui';
                        } else {
                            return 'Non';
                        }

                    },
                },

                {
                    data: "TreatmentQuantityCurrentYear__c",
                    defaultContent: "",
                    targets: 10,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(3);
                        }
                    }
                },

                {
                    data: "TreatmentQuantityNextYear__c",
                    defaultContent: "",
                    targets: 11,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(3);
                        }
                    }
                },

                {
                    data: "AnalyticSection__c",
                    defaultContent: "",
                    targets: 12,
                    orderable: true
                },

                {
                    data: "AugmentationRatio__c",
                    defaultContent: "",
                    targets: 13,
                    orderable: true
                }          


            ];


        } else {
            //with default columns def
            component.set("v.isDefaultDisplayColumns", true);
            columnDefs = [
                {
                    "visible": true,
                    "targets": 2
                },

                {
                    targets: 0,
                    data: null,
                    defaultContent: "",
                    orderable: false,
                    className: "select-checkbox",
                    checkboxes: {
                        selectRow: true,
                        stateSave: false
                    }
                },

                {
                    data: "Name",
                    defaultContent: "",
                    targets: 1,
                    className: "details-avenant",
                    orderable: true,
                    render: function (data) {
                        return '<i class="dec fa fa-plus-square" aria-hidden="true">' + data + '</i>';
                    }
                },

                {
                    data: "Contrat2__r.Name",
                    defaultContent: "",
                    targets: 2,
                    orderable: true
                },

                {
                    data: "ThirdPartyProducerName__c",
                    defaultContent: "",
                    targets: 3,
                    orderable: true
                },

                {
                    data: "WasteLabel__c",
                    defaultContent: "",
                    targets: 4,
                    orderable: true
                },

                {
                    data: "Conditionnement__c",
                    defaultContent: "",
                    targets: 5,
                    orderable: true
                },

                {
                    data: "ArticleCode__c",
                    defaultContent: "",
                    targets: 6,
                    orderable: true
                },

                {
                    data: "PriceCurrentYear__c",
                    defaultContent: "",
                    targets: 7,
                    orderable: true
                },

                {
                    data: "AugmentationRatio__c",
                    defaultContent: "",
                    targets: 8,
                    orderable: true
                },

                {
                    data: "PriceNextYear__c",
                    defaultContent: "",
                    targets: 9,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(2);
                        }
                    }
                },

                {
                    data: "TreatmentQuantityCurrentYear__c",
                    defaultContent: "",
                    targets: 10,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(3);
                        }
                    }
                },

                {
                    data: "TreatmentQuantityNextYear__c",
                    defaultContent: "",
                    targets: 11,
                    orderable: true,
                    render: function (data) {
                        if (data != null) {
                            return data.toFixed(3);
                        }
                    }
                },

                {
                    data: "Planned_next_year__c",
                    defaultContent: "",
                    targets: 12,
                    orderable: true
                },

                {
                    data: "TreatmentRange__c",
                    defaultContent: "",
                    targets: 13,
                    orderable: true
                },

                {
                    data: "AnalyticSection__c",
                    defaultContent: "",
                    targets: 14,
                    orderable: true
                },

                {
                    data: "CompletedAmadement__c",
                    defaultContent: "",
                    targets: 15,
                    orderable: true,
                    render: function (data) {
                        if (data == true) {
                            return 'Oui';
                        } else {
                            return 'Non';
                        }

                    },
                },

                {
                    data: "Nature__c",
                    defaultContent: "",
                    targets: 16,
                    orderable: true
                },
          
            ];
        }*/
    
    console.log('LC_ContractsList_AugTarif.columnDefs---> ', columnDefs);
    
    component.set("v.columnDefs", columnDefs);
    
    return columnDefs;
},
 
 handleDisplayOptionsEvent: function (component, event) {
    var displayOption = event.getParam("displayOption");       
    component.set("v.displayOption", displayOption);
    localStorage.clear(); //reset status 
    console.log('ContractList.displayOption>> ', component.get("v.displayOption"));
    
    
}

})