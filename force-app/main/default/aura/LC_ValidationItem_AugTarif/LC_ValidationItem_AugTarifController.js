({
    init: function(component, event, helper) {


        // fromFaf will be true if the user just approved or rejected a faf Account and will be redirected to the same tab
        if(localStorage.getItem('fromFaf')) {
            component.set('v.tabId', 'three')
            localStorage.removeItem('fromFaf')
        } else if(localStorage.getItem('fromStandard')) {
            component.set('v.tabId', 'two')
            localStorage.removeItem('fromStandard')
        }

        var pageReference = component.get("v.pageReference");

        //apex ids return String, Brouillon//
           var pageReference = component.get("v.pageReference");
           var action = component.get("c.getStatusFaf");
           action.setParams({
               associatedSalesRep: pageReference.state.c__respCommId, 
               filiale : pageReference.state.c__filiale,
               filiere : pageReference.state.c__filiere,
           });
           
        component.set("v.objectName", pageReference.state.c__objectName);
        component.set("v.recordId", pageReference.state.c__Id);
        component.set("v.respCommId", pageReference.state.c__respCommId);
        component.set("v.respCommName", pageReference.state.c__respCommName);
        component.set("v.filiale", pageReference.state.c__filiale);
        component.set("v.filiere", pageReference.state.c__filiere);
        component.set("v.date", pageReference.state.c__date);
        component.set("v.datefaf", pageReference.state.c__datefaf);
        if(pageReference.state.c__disablebuttons != undefined)
        {console.log('setting button disable'); component.set("v.disableactions", pageReference.state['c__disablebuttons']);}
        component.set("v.status", pageReference.state.c__status);
        component.set("v.statusfaf", pageReference.state.c__statusfaf);
        helper.setMapContractCommentByAccountId(component,event,helper);
        
        //apex ids return String, Brouillon//
           var pageReference = component.get("v.pageReference");
           var action = component.get("c.getStatusFaf");
           action.setParams({
               associatedSalesRep: pageReference.state.c__respCommId, 
               filiale : pageReference.state.c__filiale,
               filiere : pageReference.state.c__filiere,
           });
           let statusFaf = pageReference.state.c__statusfaf;
           action.setCallback(this, function (response) {
               var state = response.getState();
               if (state === "SUCCESS") {
                   statusFaf = response.getReturnValue();
                   component.set("v.statusfaf", statusFaf);
                   helper.setMapContractCommentByAccountId(component,event,helper);
               }
           });
           $A.enqueueAction(action);
          
    },
    
    reInit : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    
    getListItemsValuesEvent : function(component, event, helper) {
        console.log('LC_ValidationItemIndic_AugTarif.getListItemsValuesEvent>>>>>>>>>>>Start ');
        helper.handleListItemsValues(component, event, helper) ;
    },
})