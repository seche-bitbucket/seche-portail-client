({
    //Method to set all needed contract info by Account Id
    setMapContractCommentByAccountId: function (component, event, helper) {
         var filiale = component.get("v.filiale");
         var filiere = component.get("v.filiere");
         var accountIds = component.get("v.listAllAccountIds");
        
         var action = component.get("c.fetchContractsByAccountIds");
         action.setParams({
             accountIds: accountIds,
             filiale: filiale,
             filiere: filiere
         });
 
         action.setCallback(this, function (response) {         
            var state = response.getState(); 
                
 
             if (state === "SUCCESS") {
                 var listContracts = response.getReturnValue();                
                 console.log('listContracts ====' + listContracts);
                 var mapTemp = new Map();
                 for (var j = 0; j < (listContracts.length); j++) {
                     //Map the AccountId with contract comment   
                     let comment;
                     if(listContracts[j].IncreaseComment__c != undefined && listContracts[j].IncreaseComment__c != ''){
                        comment = listContracts[j].IncreaseComment__c;
                        mapTemp.set(listContracts[j].Account__c+listContracts[j].Filiale__c+listContracts[j].Filiere__c+listContracts[j].AssociatedSalesRep__c+listContracts[j].Face_Face__c,comment);
                     }
                 }
                 component.set("v.mapAccountIdsComment",mapTemp);              
                 console.log('listContracts'+JSON.stringify(listContracts));
                 console.log(' mapAccountIdsComment-> ', JSON.stringify(component.get("v.mapAccountIdsComment")));
             }  else if (state === "INCOMPLETE") {
                console.log("Incomplite");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
             } else {
                 console.log('error calling action --> fetchContractsByAccountIds');
             }
         });
         $A.enqueueAction(action);
    },

    handleListItemsValues: function (component, event, helper) {
           
        var listAccountIds = event.getParam("listAccountIds");
        var listAllAccountIds = event.getParam("listAllAccountIds");

        // set the handler attributes based on event data
        component.set("v.listAccountIds", listAccountIds);
        component.set("v.listAllAccountIds", listAllAccountIds);

        console.log("LC_ValidationItem_AugTarif.listAccountIds>> ", component.get("v.listAccountIds"));
        console.log("LC_ValidationItem_AugTarif.listAllAccountIds >> ", component.get("v.listAllAccountIds"));
            
    },

    renderingDoneEvent : function(component, event, helper) {
        // Get the  event by using the   
        var appEvent = $A.get("e.c:LE_ValidationReturnFaceaFace");
        appEvent.setParams({
            "refreshListData": true,
            "returnToFaf": true
        });
        appEvent.fire();
    },
})