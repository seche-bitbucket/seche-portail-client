({
    
    doInit : function(component, event, helper) {
        // Get a reference to the getWeather() function defined in the Apex controller
        var action = component.get("c.getRSSReaderController");
        
        // Register the callback function
        action.setCallback(this, function(response) {
    
            component.set("v.newsFeed", response.getReturnValue());	//Set return value to Contact Map
           
			console.log(response.getReturnValue());
			var map = response.getReturnValue();
            //console.log(map);
            var tile='<ul class="slds-list--vertical slds-has-cards">';
             
            for (key in map){
                console.log(key);
                tile += '<li class="slds-list__item">';
                tile += '<article style="margin-top:20px; padding: 5px;">';
                tile += '<span style="text-align: left; font-style: italic;">Le '+map[key].datePub+'</span><br/>'
                tile += '<a target="_blank"  href="'+map[key].link+'">';
                tile += '<br/><h3 style="text-align: left;">'+ map[key].title +'</h3></a>';
                tile += '<img src="'+ map[key].image +'" alt="'+ map[key].title +'" style="width:120px;height:90px; margin: 0 0 10px 15px; float: right;"><br/>'
                tile += '<br/><p style="margin-bottom: 20px;">'+ decodeURIComponent(map[key].description);+'</p><br/>';
                tile += '</article>';
                tile += '</li>';
            }
            tile += '</ul>';
            document.getElementById("news").innerHTML = tile;
			
            
            // Set the component attributes using values returned by the API call
            /*if (data != null) {
                component.set("v.newsFeed", data);
            }else{
                alert('Stop');
            }*/
        });
        // Invoke the service
        $A.enqueueAction(action);
    }
    
    
})