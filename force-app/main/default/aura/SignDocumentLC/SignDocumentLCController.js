({
    openWindowUrl : function(component, event, helper) {
        var qId = component.get("v.recordId");
        var action = component.get("c.retrieveUrl");
        var url=null;
        action.setParams({"q": qId});
        action.setCallback(this, function(response) { 
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS"){
                url= response.getReturnValue();
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": url
                });
                urlEvent.fire();
            }else{alert('error');} 
        });
        $A.enqueueAction(action);
    }
    
})