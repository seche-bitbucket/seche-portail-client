({
	helperAccept : function(component) {
		
        
        var action = component.get("c.setAccepted");
        var recordID = component.get("v.RecordID");
        console.log(recordID);
      
        action.setParams({
            recordId : recordID
        });
        
        // Register the callback function
        action.setCallback(this, function(response) {
           	var state = response.getState();
            console.log(state);
            if(state === "SUCCESS"){
                var listID = component.get("v.ListID");
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "FIP",
                    "message": "Approuv\ée avec succ\ès"
                });
                toastEvent.fire();

                
                var FipEvent = $A.get("e.force:navigateToList");
                FipEvent.setParams({
                    "listViewName": null,
                    "listViewId": listID,
                    "scope": "FIP_FIP__c"
                });
                FipEvent.fire();
            }else{
                 toastEvent.setParams({
                    "title": "Erreur",
                    "message": "Une erreure est survenue merci de contacter votre administrateur"
                });
                toastEvent.fire();
            }
            
        });
        // Invoke the service
        $A.enqueueAction(action);
                
	},
    
    helperRefuse : function(component) {
		
        var action = component.get("c.setRefused");
        var recordID = component.get("v.RecordID");
        console.log(recordID);
       
        action.setParams({
            recordId : recordID
        });
        
        // Register the callback function
        action.setCallback(this, function(response) {
           var state = response.getState();
            console.log(state);
            if(state === "SUCCESS"){
                var listID = component.get("v.ListID");
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "FIP",
                    "message": "Rejet\ée avec succ\ès"
                });
                toastEvent.fire();

                
                var FipEvent = $A.get("e.force:navigateToList");
                FipEvent.setParams({
                    "listViewName": null,
                    "listViewId": listID,
                    "scope": "FIP_FIP__c"
                });
                FipEvent.fire();
            }else{
                 toastEvent.setParams({
                    "title": "Erreur",
                    "message": "Une erreure est survenue merci de contacter votre administrateur"
                });
                toastEvent.fire();
            }
            
        });
        // Invoke the service
        $A.enqueueAction(action);
                
	},

   
})