({
    doInit : function(component, event, helper) {
        helper.loadQuote(component, event, helper);
    },
    requestGenReport: function(component, event, helper) {
        helper.requestSignForQuote(component, event, helper);
    },
    cancelRequest: function(component, event, helper) {
        helper.cancelCall(component, event, helper);
    }
})