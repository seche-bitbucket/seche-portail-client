({
    loadQuote: function (component, event, helper) {
        //get Quote record Id
        var action = component.get("c.getQuote");
        action.setParams({ "quoteId": component.get("v.recordId") });

        console.log('recordIdrecordIdrecordId : ', component.get("v.recordId"));

        //configure action handler
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.quot", response.getReturnValue());
            } else {
                console.log('Impossible de charger Quote : ' + state);
            }
        });
        $A.enqueueAction(action);
    },

    requestSignForQuote: function (component, event, helper) {
        var createVisitReport = component.get("c.requestForSign");
        createVisitReport.setParams({
            "quot": component.get("v.quot")
        });
        //configure response handler for this action
        createVisitReport.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.objClassController", response.getReturnValue());
                var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        mode :'sticky' ,
                        type: response.getReturnValue().showToastMode,                       
                        message :  response.getReturnValue().message
                    });     
                    //Update the UI: closePanel, show toast, refresh page
                  $A.get('e.force:refreshView').fire();
                   resultsToast.fire(); 
                 
                    $A.get("e.force:closeQuickAction").fire();
                          
                            
            } else if (state === "ERROR") {                
                console.log('Problem Sending For Signing, response state ' + state);
            } else {
                console.log('Problème inconnu: ' + state);
            }
        });

        $A.enqueueAction(createVisitReport);

    },
    
    cancelCall: function (component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }

})