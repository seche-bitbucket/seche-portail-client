({
    handleClick : function (cmp, event, helper) {
        var qlikURL = cmp.get("v.simpleRecord.URL_Qlik__c");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": 'https://seche-qlik.groupe-seche.com/single/?appid=75db382b-b9fd-45c5-9d81-c8eb6fb6b15c&sheet=20b53475-8b71-4ae2-879b-a6a39f73d6d5&opt=noselections&select=_Key_Compte_SF,' + qlikURL
        });
        urlEvent.fire();
    }
});