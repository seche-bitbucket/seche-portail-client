({
    
    openModel: function(component, event, helper) {
        component.set("v.isOpen", true);
    },
    
    closeModel: function(component, event, helper) {
        component.set("v.isOpen", false);
    },
    
    likenClose: function(component, event, helper) {
        component.set("v.isOpen", false);
    },
    
    onclickSend : function(component, event, helper) { 
            var getEmail = component.get("v.email"); 
            helper.helperSend(component,getEmail);
            component.set("v.isOpen", false);

    },
       
    keyCheck: function(component, event, helper){
    	 if(event.which == 13) {
         console.log("Enter detected!"); 
         var getEmail = component.get("v.email"); 
         console.log("getEmail = "+getEmail); 

         helper.helperSend(component,getEmail);
         component.set("v.isOpen", false);
    	}
    },
    
    onclickCancel : function(component, event, helper) {
        component.set("v.isOpen", false);
        
    }
})