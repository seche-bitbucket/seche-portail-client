({
    helperOpenForm : function(component,email) { 
        window.open("https://seche--rodev42.lightning.force.com/c/SendFIPEmailAPP.app",
                    "mywindow","width=500,height=250 ");
        
    },
    
    helperSend : function(component,getEmail) {               
        // call the server side controller method 	
        var action = component.get("c.sendMailMethod");
        action.setParams({
            'mMail': getEmail,
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('SendFIPEmailHelper state  ' + state);
            var toastEvent = $A.get("e.force:showToast");
            
            if (state == "SUCCESS") {
                component.set("v.mailStatus", true);                
                toastEvent.setParams({
                    "type" : "success",
                    "title": "Réussie",
                    "message": "Votre FIP a bien été envoyé."
                });            
            }
            else
            {                
                toastEvent.setParams({
                    "type" : "Error",
                    "title": "Erreur",
                    "message": "Votre FIP n\'a pas été envoyé."
                });
                
            }
            toastEvent.fire();            
            
        });
        
        $A.enqueueAction(action);       
    },
})