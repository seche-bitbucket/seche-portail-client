({
    doInit : function(component, event, helper) {
        helper.loadFip(component, event, helper);
    },
    requestExploitation: function(component, event, helper) {
        helper.stateToExploitation(component, event, helper);
    },
    cancelRequest: function(component, event, helper) {
        helper.cancelCall(component, event, helper);
    }
})