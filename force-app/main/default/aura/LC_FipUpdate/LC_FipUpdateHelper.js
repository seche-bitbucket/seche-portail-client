({
    loadFip: function(component, event, helper) {
         //get FIP record Id
         var action = component.get("c.getFip");
         action.setParams({"fipId" : component.get("v.recordId")});
  
         //configure action handler
         action.setCallback(this, function(response){
             var state = response.getState();
             if(state === "SUCCESS"){
                 component.set("v.fip", response.getReturnValue());                
             }else{
                 console.log('Impossible de charger FIP : '+state);
             }
         });
         $A.enqueueAction(action);
    },

    stateToExploitation: function(component, event, helper) {        
       //prepare action for update inventory
       var updateState= component.get("c.setToExploitation");
       updateState.setParams({
           "fip" : component.get("v.fip")
       });
       //configure response handler for this action
       updateState.setCallback(this, function(response){
           var state = response.getState();
           if(state === "SUCCESS"){ 
             component.set("v.objClassController", response.getReturnValue());    
             console.log('SUCCESS : ',response.getReturnValue());     
               var resultsToast = $A.get("e.force:showToast");
               resultsToast.setParams({
                    mode: 'dismissible',
                    type: response.getReturnValue().showToastMode,
                    title: "",
                    message : response.getReturnValue().message
               });

               //Update the UI: closePanel, show toast, refresh page
               $A.get("e.force:closeQuickAction").fire();
               resultsToast.fire();
               //$A.get("e.force:refreshView").fire();
               helper.gotoList (component, event, helper);
           }else if(state === "ERROR"){
               console.log('Problem updating FIP, response state '+state);
           }else{
               console.log('Problème inconnu: '+state);
           }
       });
       //send the request to updateCall
       $A.enqueueAction(updateState);
 
    },

    gotoList : function (component, event, helper) {        
        var action = component.get("c.getListViewByName");
        action.setParams({"listViewDeveloperName" : component.get("v.listViewDeveloperName")});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var listview = response.getReturnValue();
                var navEvent = $A.get("e.force:navigateToList");
                navEvent.setParams({
                    "listViewId": listview.Id,
                    "listViewName": null,
                    "scope": "FIP_FIP__c"
                });
                navEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },

    cancelCall: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }

})