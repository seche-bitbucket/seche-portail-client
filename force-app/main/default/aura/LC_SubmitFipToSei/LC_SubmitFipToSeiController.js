({
    doInit : function(component, event, helper) {
        helper.loadFip(component, event, helper);
    },
    requestToSei: function(component, event, helper) {
        helper.submitToSei(component, event, helper);
    },
    cancelRequest: function(component, event, helper) {
        helper.cancelCall(component, event, helper);
    }
})