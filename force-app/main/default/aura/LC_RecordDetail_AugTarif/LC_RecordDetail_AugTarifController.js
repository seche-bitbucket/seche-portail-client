({
    onSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
           // "title": "Success!",
            "message": "Modification enrengistrée avec succès.",
            "type" : "success"
        });
        toastEvent.fire();
    },
    onSubmit : function(component, event, helper) {
    },
    onLoad : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Loaded!",
            "message": "The record has been Loaded successfully ."
        });
        toastEvent.fire();
    },
    onError : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error!",
            "message": "Error.",
            "type" : "error"
        });
        toastEvent.fire();
    },

    closeModal : function(component, event, helper) {
        // when a component is dynamically created in lightning, we use destroy() method to destroy it.
        var sObjectName = component.get('v.sObjectName');//get Object to fire the specific event (event for contracts or Amendements)       
        debugger;
        if(sObjectName ==='Amendment__c'){         
            var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");   
            appEvent.setParams({
                "isCancel": false                
            });
            //we must refresh contract List too to refresh related Amendements :(
                var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");  
                appEventCont.setParams({
                   "isCancel": false,
                   "isAvenantUpdate" : true            
               });   
               appEventCont.fire();

        }else if(sObjectName ==='Contract__c'){
            var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");  
            appEvent.setParams({
                "isCancel": false,
                "isAvenantUpdate" : false                      
           });              
        }
        appEvent.fire();

        component.destroy();
    }

    
    
})