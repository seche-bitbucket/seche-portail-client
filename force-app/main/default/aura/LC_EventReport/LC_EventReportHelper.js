({
    loadEvent: function (component, event, helper) {
        //get FIP record Id
        var action = component.get("c.getEvent");
        action.setParams({ "eventId": component.get("v.recordId") });

        console.log('recordId----- : ', component.get("v.recordId"));

        //configure action handler
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.evt", response.getReturnValue());
            } else {
                console.log('Impossible de charger FIP : ' + state);
            }
        });
        $A.enqueueAction(action);
    },

    requestVisitReport: function (component, event, helper) {
        console.log('requestVisitReport Start-- : ', component.get("v.recordId"));
        var createVisitReport = component.get("c.generateVisitReport");
        createVisitReport.setParams({
            "evt": component.get("v.evt")
        });
        //configure response handler for this action
        createVisitReport.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {               
                console.log('response----- : ' ,response.getReturnValue());
                component.set("v.objClassController", response.getReturnValue());
                if(response.getReturnValue().includes('error')== false){
                    //redirect to report
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": response.getReturnValue(),
                    "slideDevName": "related"
                });
                navEvt.fire(); 
                }else{
                    var resultsToast = $A.get("e.force:showToast");
                    let message ;
                    if(response.getReturnValue() === 'error1'){
                        message ='L\'évènement est déjà lié à un compte rendu'
                    }else{
                        message ='L\'évènement doit être lié à un compte et un contact pour générer un CR'
                    }
                    resultsToast.setParams({
                        mode: 'sticky',
                        type : 'warning',                        
                        message :  message
                    });
     
                    //Update the UI: closePanel, show toast, refresh page
                    //$A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    //$A.get("e.force:refreshView").fire();
    
                }             
                            
            } else if (state === "ERROR") {
                console.log('Problem requestVisitReport, response state ' + state);
            } else {
                console.log('Problème inconnu: ' + state);
            }
        });

        $A.enqueueAction(createVisitReport);

    },

    cancelCall: function (component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }

})