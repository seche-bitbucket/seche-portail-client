({
    doInit : function(component, event, helper) {
        helper.loadEvent(component, event, helper);
    },
    requestGenReport: function(component, event, helper) {
        helper.requestVisitReport(component, event, helper);
    },
    cancelRequest: function(component, event, helper) {
        helper.cancelCall(component, event, helper);
    }
})