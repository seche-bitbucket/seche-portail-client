({
    //Method to genarate a item Chart
    getReportData: function (component, event, helper, InputData) {         //
        //debugger;
        // var reportResultData = JSON.parse(InputData);

        var reportData = [];
        var groupingName = [];
        var columnLabels = [];

        if (InputData.groupingsDown.groupings) {
            for (var i = 0; i < (InputData.groupingsDown.groupings.length); i++) {

                //Iterate and prepare the list of Labels for the chart
                var groupingName = InputData.groupingsDown.groupings[i].label;

                var keyTemp = InputData.groupingsDown.groupings[i].key;
                //Prepeare the chart data to be plotted.
                for (var j = 0; j < (InputData.factMap[keyTemp + "!T"].aggregates.length); j++) {
                    var valueTemp = InputData.factMap[keyTemp + "!T"].aggregates[j].value;
                    var columnLabelTemp = InputData.reportMetadata.aggregates[j].split(".")[1] != undefined ? InputData.reportMetadata.aggregates[j].split(".")[1] : InputData.reportMetadata.aggregates[j];
                    columnLabels.push(columnLabelTemp);
                    reportData.push(valueTemp);
                }

            }
        }
        //                       
        console.log('Columns labels', columnLabels);

        //initialize object with the report
        var vObjet = {               
            caPreviousYear: reportData[0]> 0 ? reportData[0].toFixed(2) : 0,
            caN: reportData[1] > 0 ? reportData[1].toFixed(2) : 0,
            forecastedCaN: reportData[2] > 0 ? reportData[2].toFixed(2) : 0,
            quantityPreviousYear: reportData[3] > 0 ? reportData[3].toFixed(2) : 0,
            quantityCurrentYear: reportData[4] > 0 ? reportData[4].toFixed(2) : 0,
            estimateTurnoverNextYear: reportData[5] > 0 ? reportData[5].toFixed(2) : 0,
            forecastedCaNextYear: reportData[6] > 0 ? reportData[6].toFixed(2) : 0,
            formulaEuros: reportData[7] > 0 ? reportData[7] : 0,
            formula:  reportData[8] > 0 ? reportData[8] : 0,
            rowCount: reportData[9],
        }

        return vObjet;

       
    },

    getAllReportsData: function (component, event, helper) {
        var associatedSalesRepId = component.get("v.userId");
        var associatedSalesRepName = component.get("v.userName");
        var filiale = component.get("v.filiale");
        var filiere = component.get("v.filiere");

        console.log(associatedSalesRepName+' '+filiale+' '+filiere);

        //Set report's API names to display
        var lstReportName = ["Approb_01_Tout_Hors_FaF_9Bz", "Approb_02_Tout_FaF_HXD", "Approb_03_Traitement_Hors_FaF_fxN", "Approb_04_Traitement_FaF_sZE", "Approb_05_Prestation_Hors_FaF_JkV", "Approb_06_Prestation_FaF_9he", "Approb_07_Conditionnement_Hors_FaF_23n", "Approb_08_Conditionnement_FaF_7ZW", "Approb_14_Tout_Sous_Contrat_Nqh", "Approb_15_Traitement_Sous_Contrat_Uiu", "Approb_16_Prestation_Sous_Contrat_WAe", "Approb_17_Conditionnement_Sous_Contrat_4U5", "Approb_18_Tout", "Approb_19_Traitement_Total_N7M", "Approb_20_Prestation_Total_6m5", "Approb_21_Conditionnement_Total_Das"];
        var action = component.get("c.getReportData_chart");
        action.setParams({
            lstReportName: lstReportName,
            associatedSalesRep: associatedSalesRepName,
            filiale: filiale,
            filiere: filiere
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            var errors = response.getError(); 

            if (state === "SUCCESS") {
                var reportsResultDatas = response.getReturnValue();               
                console.log('reportsResultDatas ====' + reportsResultDatas);
                // debugger;
                reportsResultDatas.forEach(function (rep) {
                    var chartInputData = JSON.parse(rep);
                    var reportDevName = chartInputData.reportMetadata.developerName;
                    console.log('reportDevName => '+reportDevName);
                    var reportData = helper.getReportData(component, event, helper, chartInputData);
                    console.log('chartInputData => '+JSON.stringify(chartInputData));
                    switch (reportDevName) {
                        case "Approb_01_Tout_Hors_FaF_9Bz":
                            component.set("v.allData", reportData);
                            break;
                        case "Approb_02_Tout_FaF_HXD":
                            component.set("v.allDataFaf", reportData);
                            break;
                        case "Approb_03_Traitement_Hors_FaF_fxN":
                            component.set("v.traitData", reportData);
                            break;
                        case "Approb_04_Traitement_FaF_sZE":
                            component.set("v.traitDataFaf", reportData);
                            break;
                        case "Approb_05_Prestation_Hors_FaF_JkV":
                            component.set("v.prestData", reportData);
                            break;
                        case "Approb_06_Prestation_FaF_9he":
                            component.set("v.prestDataFaf", reportData);
                            break;
                        case "Approb_07_Conditionnement_Hors_FaF_23n":
                            component.set("v.condiData", reportData);
                            break;
                        case "Approb_08_Conditionnement_FaF_7ZW":
                            component.set("v.condiDataFaf", reportData);
                            break;
                        case "Approb_14_Tout_Sous_Contrat_Nqh":
                            component.set("v.allDataUndCon", reportData)
                            break
                        case "Approb_15_Traitement_Sous_Contrat_Uiu":
                            component.set("v.traitDataUndCon", reportData)
                            break
                        case "Approb_16_Prestation_Sous_Contrat_WAe":
                            component.set("v.prestDataUndCon", reportData)
                            break
                        case "Approb_17_Conditionnement_Sous_Contrat_4U5":
                            component.set("v.condiDataUndCon", reportData)
                            break
                        case "Approb_18_Tout":
                            component.set("v.allDataTotal", reportData)
                            break
                        case "Approb_19_Traitement_Total_N7M":
                            component.set("v.traitDataTotal", reportData)
                            break
                        case "Approb_20_Prestation_Total_6m5":
                            component.set("v.prestDataTotal", reportData)
                            break
                        case "Approb_21_Conditionnement_Total_Das":
                            component.set("v.condiDataTotal", reportData)
                            break

                    }


                });
            } else {
                console.log('error calling action --> getReportData');
            }
        });
        $A.enqueueAction(action);
    }
})