({
    doInit : function(component, event, helper) {
        helper.doInit(component, event);
    },
    
    saveLead : function(component, event, helper) {
        // Validate form fields
        // Pass form data to a helper function
        helper.saveLead(component, event);
    }
})