({
    doInit : function(cmp, ev) {
        var url = window.location.href;        
        var action = cmp.get("c.find_LeadById");
        action.setParams({
            URL: url
        });
        
        action.setCallback(this, function(response) {
            //console.log(response.getReturnValue());
            var state = response.getState();
            var result = response.getReturnValue();
            console.log(result);
            if (state === "SUCCESS" && result != null) {
                cmp.set('v.lea', response.getReturnValue());
            }
            else {
                alert('Error : ' + JSON.stringify(errors));
            }
        });
        $A.enqueueAction(action);
    },
    
    saveLead : function(cmp, ev) {
        var action = cmp.get("c.saveContents");
        var jsonLead = cmp.get("v.lea");
        var stringlead = JSON.stringify(jsonLead);
        
        action.setParams({
            "lfcJson" : stringlead
        });
         
        action.setCallback(this, function(response) {
           
            var state = response.getReturnValue();
            console.log(state);
            if (state === "SUCCESS") {
                alert('Vos informations ont bien été enregistrées');
            }
            else if (state === "ERROR") {
                alert('Une erreur s\'est produite');
            }
        });
        $A.enqueueAction(action);
    }
    
 
 })