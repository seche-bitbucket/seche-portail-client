({
    doInit : function(component, event, helper) { 
        // get the fields API name and pass it to helper function  
        var controllingFieldAPI = component.get("v.controllingFieldAPI");
        var dependingFieldAPI = component.get("v.dependingFieldAPI");
        var objDetails = component.get("v.objDetail");
      
        helper.fetchPicklistValues(component,objDetails,controllingFieldAPI, dependingFieldAPI);
        helper.fetchCustomerNaturePicklistValues(component,event, helper);
    },
    
    handleFieldChange: function(component, event, helper) {        
        helper.onControllerFieldChange(component, event, helper);      
    },
    
    fireEvent : function(component, event, helper) {
        helper.fireTheHeaderEvent(component,event,helper);
      },

    handleChange: function (component, event, helper) {
        var changeValue = event.getParam("value");
        helper.fireDisplayOption(component,event,helper,changeValue);
    }
})