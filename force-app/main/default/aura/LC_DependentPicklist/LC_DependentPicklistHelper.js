({
    fetchPicklistValues: function(component,objDetails,controllerField, dependentField) {
       
        // call the server side function  
        var action = component.get("c.getDependentMap");
        // pass paramerters [object definition , contrller field name ,dependent field name] -
        // to server side function 
        action.setParams({
            'objDetail' : objDetails,
            'contrfieldApiName': controllerField,
            'depfieldApiName': dependentField 
        });
        //set callback   
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                //store the return response from server (map<string,List<string>>)  
                var StoreResponse = response.getReturnValue();
                
                // once set #StoreResponse to depnedentFieldMap attribute 
                component.set("v.depnedentFieldMap",StoreResponse);
                
                // create a empty array for store map keys(@@--->which is controller picklist values) 
                var listOfkeys = []; // for store all map keys (controller picklist values)
                var ControllerField = []; // for store controller picklist value to set on lightning:select. 
                
                // play a for loop on Return map 
                // and fill the all map key on listOfkeys variable.
                for (var singlekey in StoreResponse) {
                    listOfkeys.push(singlekey);
                }
                
                //set the controller field value for lightning:select
                if (listOfkeys != undefined && listOfkeys.length > 0) {
                    ControllerField.push('--- None ---');
                }
                
                for (var i = 0; i < listOfkeys.length; i++) {
                    ControllerField.push(listOfkeys[i]);
                }  
                // set the ControllerField variable values to country(controller picklist field)
                component.set("v.listControllingValues", ControllerField);
            }else{
                alert('Something went wrong..');
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchDepValues: function(component, ListOfDependentFields) {
        // create a empty array var for store dependent picklist values for controller field  
        var dependentFields = [];
        dependentFields.push('--- None ---');
        for (var i = 0; i < ListOfDependentFields.length; i++) {
            dependentFields.push(ListOfDependentFields[i]);
        }
        // set the dependentFields variable values to store(dependent picklist field) on lightning:select
        component.set("v.listDependingValues", dependentFields);
        
    },
    fireTheHeaderEvent : function(component, event, helper) {
        // Get the  event by using the
        // name value from aura:registerEvent
        //debugger;
        var appEvent = $A.get("e.c:LE_DependentPicklist");
        var filiale =  component.find("controllerFld").get("v.value");  
        var filiere =  filiale == '--- None ---'? filiale: component.find("dependentFld").get("v.value");
        var natureCompte =  component.find("customerNatureListId").get("v.value");
        var isShowContent = false ;

        if(filiale != '--- None ---' && filiere != '--- None ---'){
             isShowContent = true;
         }
         
        appEvent.setParams({
            "filiale" : filiale ,
            "filiere" : filiere,
            "natureCompte" : natureCompte,
            "isShowContent" : isShowContent,
            });
            appEvent.fire();
    },

    fireDisplayOption : function(component, event, helper, displayOption) {  
        debugger;
        var appEvent = $A.get("e.c:LE_ScreenDisplayOptions");      
        appEvent.setParams({
            "displayOption" : displayOption
            });
            appEvent.fire();
    },

    onControllerFieldChange: function(component, event, helper) {    
       //debugger;        
        var controllerValueKey = event.getSource().get("v.value"); // get selected controller field value            
        
        if (controllerValueKey != '--- None ---') {
            var depnedentFieldMap = component.get("v.depnedentFieldMap"); 
            var ListOfDependentFields = depnedentFieldMap[controllerValueKey];
            
            if(ListOfDependentFields.length > 0){
                component.set("v.bDisabledDependentFld" , false);  
                helper.fetchDepValues(component, ListOfDependentFields);
                helper.fireTheHeaderEvent(component,event,helper);    
            }else{
                component.set("v.bDisabledDependentFld" , true); 
                component.set("v.listDependingValues", ['--- None ---']);               
            }  
            
        } else {
            component.set("v.listDependingValues", ['--- None ---']);
            component.set("v.bDisabledDependentFld" , true);
           
        }

       helper.fireTheHeaderEvent(component, event, helper); 
      

        
    },

    fetchCustomerNaturePicklistValues: function(component, event, helper) {
        var action = component.get("c.getListCustomerNature");
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var result = response.getReturnValue();
                // once set #StoreResponse to depnedentFieldMap attribute 
                var CustomerNatureList = [];
                for(var key in result){
                    CustomerNatureList.push({key: key, value: result[key]});
                }
                component.set("v.customerNatureList",CustomerNatureList);
            
            }
            else {
                console.log("Failed with CustomerNatureList ");    
           }
        });
      $A.enqueueAction(action);
    }

    
})