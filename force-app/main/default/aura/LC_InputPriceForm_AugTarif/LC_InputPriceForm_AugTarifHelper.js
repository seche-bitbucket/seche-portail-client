({
    //Use this method to call back the incPerOnContract method when we split the amendments to process
    incPerOnContractCallBack: function (component, event, helper, selectedListAvenant) {
        //debugger;
        var selectedListAvenant = selectedListAvenant;
        var inputAll = component.get("v.defaultValue") != "" ? component.get("v.defaultValue") : 0.0;
        var inputTrait = component.get("v.defaultValueTrait") != "" ? component.get("v.defaultValueTrait") : 0.0;
        var inputCondi = component.get("v.defaultValueCondi") != "" ? component.get("v.defaultValueCondi") : 0.0;
        var inputPresta = component.get("v.defaultValuePresta") != "" ? component.get("v.defaultValuePresta") : 0.0;
        var RoundingType;
        var RoundingTypeTrait;
        var RoundingTypePrest;
        var RoundingTypeCondi;
        var isNotArround = component.get("v.isNotArround");
        var checkCmpAuto = component.find("checkboxAuto");
        var isAutoArround = checkCmpAuto.get("v.value");
        var checkCmpSupp = component.find("checkboxSupp");
        var isSuppArround = checkCmpSupp.get("v.value");
        
        //Trait
        var isNotArroundTrait = component.get("v.isNotArroundTrait");
        var checkCmpAutoTrait = component.find("checkboxAutoTrait");
        if(checkCmpAutoTrait){
            var isAutoArroundTrait = checkCmpAutoTrait.get("v.value");
        }        
        if(checkCmpAutoTrait){
            var checkCmpSuppTrait = component.find("checkboxSuppTrait");
        }       
        if(checkCmpSuppTrait){
            var isSuppArroundTrait =  checkCmpSuppTrait.get("v.value");
        }
        

        //Presta
        var isNotArroundPrest = component.get("v.isNotArroundPrest");
        var checkCmpAutoPrest = component.find("checkboxAutoPrest");
        if(checkCmpAutoPrest){
            var isAutoArroundPrest = checkCmpAutoPrest.get("v.value");
        }        
        var checkCmpSuppPrest = component.find("checkboxSuppPrest");
        if(checkCmpSuppPrest){
            var isSuppArroundPrest = checkCmpSuppPrest.get("v.value");
        }       

        //Condi
        var isNotArroundCondi= component.get("v.isNotArroundCondi");
        var checkCmpAutoCondi = component.find("checkboxAutoCondi");
        if(checkCmpAutoCondi){
            var isAutoArroundCondi = checkCmpAutoCondi.get("v.value");
        }       
        var checkCmpSuppCondi = component.find("checkboxSuppCondi");
        if(checkCmpSuppCondi){
            var isSuppArroundCondi = checkCmpSuppCondi.get("v.value");
        }

        var selectedListAvenant2;

        if (isSuppArround) {
            RoundingType = 'supp';
        } else if (isAutoArround) {
            RoundingType = 'auto';
        } else {
            RoundingType = 'noRounding';
        }

        if (isSuppArroundTrait) {
            RoundingTypeTrait = 'supp';
        } else if (isAutoArroundTrait) {
            RoundingTypeTrait = 'auto';
        } else {
            RoundingTypeTrait = 'noRounding';
        }

        if (isSuppArroundPrest) {
            RoundingTypePrest = 'supp';
        } else if (isAutoArroundPrest) {
            RoundingTypePrest = 'auto';
        } else {
            RoundingTypePrest = 'noRounding';
        }

        if (isSuppArroundCondi) {
            RoundingTypeCondi = 'supp';
        } else if (isAutoArroundCondi) {
            RoundingTypeCondi = 'auto';
        } else {
            RoundingTypeCondi = 'noRounding';
        }

        console.log('validate.selectedListAvenant = ', selectedListAvenant);
        console.log('validate.inputAll = ', inputAll);
        console.log('validate.inputTrait = ', inputTrait);
        console.log('validate.inputCondi = ', inputCondi);
        console.log('validate.inputPresta = ', inputPresta);

        //call Apex
        var action = component.get("c.increaseValues");
        action.setParams({
            lstAmend: selectedListAvenant,
            inputValue: inputAll,
            inputValueTraitement: inputTrait,
            inputValueTransport: inputPresta,
            inputValueConditionnement: inputCondi,
            RoundingType: RoundingType,
            RoundingTypeTrait : RoundingTypeTrait,
            RoundingTypePrest : RoundingTypePrest,
            RoundingTypeCondi : RoundingTypeCondi
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                if (response.getReturnValue() === "ok") {
                    helper.toastdMessage("Augmentation tarifaire", "Augmentations effectuées avec succès", "success");
                    //Fire event for update list 
                    var action = component.get('v.action');//get action to fire the specific event (event for contracts or Amendements)       
                    if (action === 'increasePercentAmend' || action === 'increasePercent' || action === 'incfixedPrice' || action === 'fixedPrice') {
                        var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                        appEvent.setParams({
                            "isCancel": false
                        });
                        //we must refresh contract List too to refresh related Amendements :(
                        var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                        appEventCont.setParams({
                            "isCancel": false,
                            "isAvenantUpdate": true,
                            "actionFromContrat": component.get('v.actionFromContrat')
                        });
                        appEventCont.fire();


                    } else {
                        var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                        appEvent.setParams({
                            "isCancel": false,
                            "actionFromContrat": component.get('v.actionFromContrat')
                        });
                    }
                    appEvent.fire();
                    //
                    component.destroy();
                } else {
                    helper.toastdMessage("Augmentation tarifaire", "Erreur rencontrée pendant l'augmentation, veuillez contacter l'administrateur", "error");
                    //Fire event for update list 
                    var action = component.get('v.action');//get action to fire the specific event (event for contracts or Amendements)       
                    if (action === 'increasePercentAmend' || action === 'increasePercent' || action === 'incfixedPrice' || action === 'fixedPrice') {
                        var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                        appEvent.setParams({
                            "isCancel": true
                        });

                    } else {
                        var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                        appEvent.setParams({
                            "isCancel": true,
                            "actionFromContrat": component.get('v.actionFromContrat')
                        });
                    }
                    appEvent.fire();
                    component.destroy();
                }

            } else {
                console.log('Erreur rencontrée pendant la mise à jours ' + state);
                helper.toastdMessage("Augmentation tarifaire", "Erreur rencontrée pendant l'augmentation, veuillez contacter l'administrateur", "error");
                component.destroy();
            }
        });
        $A.enqueueAction(action);

    },

    incPerOnContract: function (component, event, helper) {
        //debugger;
        var selectedListAvenant = component.get("v.selectedListAvenant");
        var inputAll = component.get("v.defaultValue") != "" ? component.get("v.defaultValue") : 0.0;
        var inputTrait = component.get("v.defaultValueTrait") != "" ? component.get("v.defaultValueTrait") : 0.0;
        var inputCondi = component.get("v.defaultValueCondi") != "" ? component.get("v.defaultValueCondi") : 0.0;
        var inputPresta = component.get("v.defaultValuePresta") != "" ? component.get("v.defaultValuePresta") : 0.0;
        var RoundingType;
        var RoundingTypeTrait;
        var RoundingTypePrest;
        var RoundingTypeCondi;
        var isNotArround = component.get("v.isNotArround");
        var checkCmpAuto = component.find("checkboxAuto");
        var isAutoArround = checkCmpAuto.get("v.value");
        var checkCmpSupp = component.find("checkboxSupp");
        var isSuppArround = checkCmpSupp.get("v.value");
        var comment = component.get("v.comment");
          
            //Trait
        var isNotArroundTrait = component.get("v.isNotArroundTrait");
        var checkCmpAutoTrait = component.find("checkboxAutoTrait");
        if(checkCmpAutoTrait){
            var isAutoArroundTrait = checkCmpAutoTrait.get("v.value");
        }        
        if(checkCmpAutoTrait){
            var checkCmpSuppTrait = component.find("checkboxSuppTrait");
        }       
        if(checkCmpSuppTrait){
            var isSuppArroundTrait =  checkCmpSuppTrait.get("v.value");
        }
        

        //Presta
        var isNotArroundPrest = component.get("v.isNotArroundPrest");
        var checkCmpAutoPrest = component.find("checkboxAutoPrest");
        if(checkCmpAutoPrest){
            var isAutoArroundPrest = checkCmpAutoPrest.get("v.value");
        }        
        var checkCmpSuppPrest = component.find("checkboxSuppPrest");
        if(checkCmpSuppPrest){
            var isSuppArroundPrest = checkCmpSuppPrest.get("v.value");
        }       

        //Condi
        var isNotArroundCondi= component.get("v.isNotArroundCondi");
        var checkCmpAutoCondi = component.find("checkboxAutoCondi");
        if(checkCmpAutoCondi){
            var isAutoArroundCondi = checkCmpAutoCondi.get("v.value");
        }       
        var checkCmpSuppCondi = component.find("checkboxSuppCondi");
        if(checkCmpSuppCondi){
            var isSuppArroundCondi = checkCmpSuppCondi.get("v.value");
        }
        
        var selectedListAvenant2;

        /*  if(isNotArround){
              RoundingType ='noRounding';
          } else */
        if (isSuppArround) {
            RoundingType = 'supp';
        } else if (isAutoArround) {
            RoundingType = 'auto';
        } else {
            RoundingType = 'noRounding';
        }

        if (isSuppArroundTrait) {
            RoundingTypeTrait = 'supp';
        } else if (isAutoArroundTrait) {
            RoundingTypeTrait = 'auto';
        } else {
            RoundingTypeTrait = 'noRounding';
        }

        if (isSuppArroundPrest) {
            RoundingTypePrest = 'supp';
        } else if (isAutoArroundPrest) {
            RoundingTypePrest = 'auto';
        } else {
            RoundingTypePrest = 'noRounding';
        }

        if (isSuppArroundCondi) {
            RoundingTypeCondi = 'supp';
        } else if (isAutoArroundCondi) {
            RoundingTypeCondi = 'auto';
        } else {
            RoundingTypeCondi = 'noRounding';
        }

        console.log('validate.selectedListAvenant = ', selectedListAvenant);
        console.log('validate.inputAll = ', inputAll);
        console.log('validate.inputTrait = ', inputTrait);
        console.log('validate.inputCondi = ', inputCondi);
        console.log('validate.inputPresta = ', inputPresta);

        if (selectedListAvenant.length > 2000) {  // divide the list of amendements  to avoid the CPU Limit(processe bulider & triggers are waiting somewhere ^^)       
            selectedListAvenant2 = selectedListAvenant.splice(0, Math.ceil(selectedListAvenant.length / 2));

            console.log('selectedListAvenant---', selectedListAvenant.length);
            console.log('selectedListAvenant2---', selectedListAvenant2.length);
        }
        //call Apex
        var action = component.get("c.increaseValues");
        action.setParams({
            lstAmend: selectedListAvenant,
            inputValue: inputAll,
            inputValueTraitement: inputTrait,
            inputValueTransport: inputPresta,
            inputValueConditionnement: inputCondi,
            RoundingType: RoundingType,
            RoundingTypeTrait : RoundingTypeTrait,
            RoundingTypePrest : RoundingTypePrest,
            RoundingTypeCondi : RoundingTypeCondi,
            comment : comment
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                if (response.getReturnValue() === "ok") {
                    if (selectedListAvenant2) {
                        helper.incPerOnContractCallBack(component, event, helper, selectedListAvenant2);
                    } else {
                        helper.toastdMessage("Augmentation tarifaire", "Augmentations effectuées avec succès", "success");
                        //Fire event for update list 
                        var action = component.get('v.action');//get action to fire the specific event (event for contracts or Amendements)       
                        if (action === 'increasePercentAmend' || action === 'increasePercent' || action === 'incfixedPrice' || action === 'fixedPrice') {
                            var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                            appEvent.setParams({
                                "isCancel": false
                            });
                            //we must refresh contract List too to refresh related Amendements :(
                            var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                            appEventCont.setParams({
                                "isCancel": false,
                                "isAvenantUpdate": true,
                                "actionFromContrat": component.get('v.actionFromContrat')
                            });
                            appEventCont.fire();


                        } else {
                            var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                            appEvent.setParams({
                                "isCancel": false,
                                "actionFromContrat": component.get('v.actionFromContrat')
                            });
                        }
                        appEvent.fire();
                        //
                        component.destroy();
                    }


                } else {
                    helper.toastdMessage("Augmentation tarifaire", "Erreur rencontrée pendant l'augmentation, veuillez contacter l'administrateur", "error");
                    //Fire event for update list 
                    var action = component.get('v.action');//get action to fire the specific event (event for contracts or Amendements)       
                    if (action === 'increasePercentAmend' || action === 'increasePercent' || action === 'incfixedPrice' || action === 'fixedPrice') {
                        var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                        appEvent.setParams({
                            "isCancel": true
                        });

                    } else {
                        var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                        appEvent.setParams({
                            "isCancel": true,
                            "actionFromContrat": component.get('v.actionFromContrat')
                        });
                    }
                    appEvent.fire();
                    component.destroy();
                }

            } else {
                console.log('Erreur rencontrée pendant la mise à jours ' + state);
                helper.toastdMessage("Augmentation tarifaire", "Erreur rencontrée pendant l'augmentation, veuillez contacter l'administrateur", "error");
                component.destroy();
            }
        });
        $A.enqueueAction(action);
    },

    incPerOnAmendementCallBack: function (component, event, helper, selectedListAvenant) {
        debugger;
        var selectedListAvenant = selectedListAvenant;
        var inputAll = component.get("v.defaultValue") != "" ? component.get("v.defaultValue") : 0.0;
        var inputTrait = component.get("v.defaultValueTrait") != "" ? component.get("v.defaultValueTrait") : 0.0;
        var inputCondi = component.get("v.defaultValueCondi") != "" ? component.get("v.defaultValueCondi") : 0.0;
        var inputPresta = component.get("v.defaultValuePresta") != "" ? component.get("v.defaultValuePresta") : 0.0;
        var RoundingType;
        var RoundingTypeTrait;
        var RoundingTypePrest;
        var RoundingTypeCondi;
        var isNotArround = component.get("v.isNotArround");
        var checkCmpAuto = component.find("checkboxAuto");
        var isAutoArround = checkCmpAuto.get("v.value");
        var checkCmpSupp = component.find("checkboxSupp");
        var isSuppArround = checkCmpSupp.get("v.value");
        var comment = component.get("v.comment");

          
        //Trait
        var isNotArroundTrait = component.get("v.isNotArroundTrait");
        var checkCmpAutoTrait = component.find("checkboxAutoTrait");
        if(checkCmpAutoTrait){
            var isAutoArroundTrait = checkCmpAutoTrait.get("v.value");
        }        
        if(checkCmpAutoTrait){
            var checkCmpSuppTrait = component.find("checkboxSuppTrait");
        }       
        if(checkCmpSuppTrait){
            var isSuppArroundTrait =  checkCmpSuppTrait.get("v.value");
        }
        

        //Presta
        var isNotArroundPrest = component.get("v.isNotArroundPrest");
        var checkCmpAutoPrest = component.find("checkboxAutoPrest");
        if(checkCmpAutoPrest){
            var isAutoArroundPrest = checkCmpAutoPrest.get("v.value");
        }        
        var checkCmpSuppPrest = component.find("checkboxSuppPrest");
        if(checkCmpSuppPrest){
            var isSuppArroundPrest = checkCmpSuppPrest.get("v.value");
        }       

        //Condi
        var isNotArroundCondi= component.get("v.isNotArroundCondi");
        var checkCmpAutoCondi = component.find("checkboxAutoCondi");
        if(checkCmpAutoCondi){
            var isAutoArroundCondi = checkCmpAutoCondi.get("v.value");
        }       
        var checkCmpSuppCondi = component.find("checkboxSuppCondi");
        if(checkCmpSuppCondi){
            var isSuppArroundCondi = checkCmpSuppCondi.get("v.value");
        }
        var selectedListAvenant2;

        /*  if(isNotArround){
              RoundingType ='noRounding';
          } else */
        if (isSuppArround) {
            RoundingType = 'supp';
        } else if (isAutoArround) {
            RoundingType = 'auto';
        } else {
            RoundingType = 'noRounding';
        }
        if (isSuppArroundTrait) {
            RoundingTypeTrait = 'supp';
        } else if (isAutoArroundTrait) {
            RoundingTypeTrait = 'auto';
        } else {
            RoundingTypeTrait = 'noRounding';
        }

        if (isSuppArroundPrest) {
            RoundingTypePrest = 'supp';
        } else if (isAutoArroundPrest) {
            RoundingTypePrest = 'auto';
        } else {
            RoundingTypePrest = 'noRounding';
        }

        if (isSuppArroundCondi) {
            RoundingTypeCondi = 'supp';
        } else if (isAutoArroundCondi) {
            RoundingTypeCondi = 'auto';
        } else {
            RoundingTypeCondi = 'noRounding';
        }

        console.log('validate.selectedListAvenant = ', selectedListAvenant);
        console.log('validate.inputAll = ', inputAll);
        console.log('validate.inputTrait = ', inputTrait);
        console.log('validate.inputCondi = ', inputCondi);
        console.log('validate.inputPresta = ', inputPresta);


        //call Apex
        var action = component.get("c.increaseValues");
        action.setParams({
            lstAmend: selectedListAvenant,
            inputValue: inputAll,
            inputValueTraitement: inputTrait,
            inputValueTransport: inputPresta,
            inputValueConditionnement: inputCondi,
            RoundingType: RoundingType,
            RoundingTypeTrait : RoundingTypeTrait,
            RoundingTypePrest : RoundingTypePrest,
            RoundingTypeCondi : RoundingTypeCondi,
            comment : comment
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                if (response.getReturnValue() === "ok") {
                    helper.toastdMessage("Augmentation tarifaire", "Augmentations effectuées avec succès", "success");
                    //Fire event for update list 
                    var action = component.get('v.action');//get action to fire the specific event (event for contracts or Amendements)       
                    if (action === 'increasePercentAmend' || action === 'increasePercent' || action === 'incfixedPrice' || action === 'fixedPrice') {
                        var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                        appEvent.setParams({
                            "isCancel": false
                        });
                        //we must refresh contract List too to refresh related Amendements :(
                        var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                        appEventCont.setParams({
                            "isCancel": false,
                            "isAvenantUpdate": true,
                            "actionFromContrat": component.get('v.actionFromContrat')
                        });
                        appEventCont.fire();


                    } else {
                        var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                        appEvent.setParams({
                            "isCancel": false,
                            "actionFromContrat": component.get('v.actionFromContrat')
                        });
                    }
                    appEvent.fire();

                    component.destroy();
                } else {
                    helper.toastdMessage("Augmentation tarifaire", "Erreur rencontrée pendant l'augmentation, veuillez contacter l'administrateur", "error");
                    //Fire event for update list 
                    var action = component.get('v.action');//get action to fire the specific event (event for contracts or Amendements)       
                    if (action === 'increasePercentAmend' || action === 'increasePercent' || action === 'incfixedPrice' || action === 'fixedPrice') {
                        var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                        appEvent.setParams({
                            "isCancel": true
                        });

                    } else {
                        var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                        appEvent.setParams({
                            "isCancel": true,
                            "actionFromContrat": component.get('v.actionFromContrat')
                        });
                    }
                    appEvent.fire();
                    component.destroy();
                }

            } else {
                console.log('Erreur rencontrée pendant la mise à jours ' + state);
                helper.toastdMessage("Augmentation tarifaire", "Erreur rencontrée pendant l'augmentation, veuillez contacter l'administrateur", "error");
                component.destroy();
            }
        });
        $A.enqueueAction(action);

    },

    incPerOnAmendement: function (component, event, helper) {
        debugger;
        var selectedListAvenant = component.get("v.selectedListAvenant");
        var inputAll = component.get("v.defaultValue") != "" ? component.get("v.defaultValue") : 0.0;
        var inputTrait = component.get("v.defaultValueTrait") != "" ? component.get("v.defaultValueTrait") : 0.0;
        var inputCondi = component.get("v.defaultValueCondi") != "" ? component.get("v.defaultValueCondi") : 0.0;
        var inputPresta = component.get("v.defaultValuePresta") != "" ? component.get("v.defaultValuePresta") : 0.0;
        var RoundingType;
        var RoundingTypeTrait;
        var RoundingTypePrest;
        var RoundingTypeCondi;
        var isNotArround = component.get("v.isNotArround");
        var checkCmpAuto = component.find("checkboxAuto");
        var isAutoArround = checkCmpAuto.get("v.value");
        var checkCmpSupp = component.find("checkboxSupp");
        var isSuppArround = checkCmpSupp.get("v.value");
        var comment = component.get("v.comment");

        

        //Trait
        var isNotArroundTrait = component.get("v.isNotArroundTrait");
        var checkCmpAutoTrait = component.find("checkboxAutoTrait");
        if(checkCmpAutoTrait){
            var isAutoArroundTrait = checkCmpAutoTrait.get("v.value");
        }        
        if(checkCmpAutoTrait){
            var checkCmpSuppTrait = component.find("checkboxSuppTrait");
        }       
        if(checkCmpSuppTrait){
            var isSuppArroundTrait =  checkCmpSuppTrait.get("v.value");
        }
        

        //Presta
        var isNotArroundPrest = component.get("v.isNotArroundPrest");
        var checkCmpAutoPrest = component.find("checkboxAutoPrest");
        if(checkCmpAutoPrest){
            var isAutoArroundPrest = checkCmpAutoPrest.get("v.value");
        }        
        var checkCmpSuppPrest = component.find("checkboxSuppPrest");
        if(checkCmpSuppPrest){
            var isSuppArroundPrest = checkCmpSuppPrest.get("v.value");
        }       

        //Condi
        var isNotArroundCondi= component.get("v.isNotArroundCondi");
        var checkCmpAutoCondi = component.find("checkboxAutoCondi");
        if(checkCmpAutoCondi){
            var isAutoArroundCondi = checkCmpAutoCondi.get("v.value");
        }       
        var checkCmpSuppCondi = component.find("checkboxSuppCondi");
        if(checkCmpSuppCondi){
            var isSuppArroundCondi = checkCmpSuppCondi.get("v.value");
        }
       

        var selectedListAvenant2;

       
        if (isSuppArround) {
            RoundingType = 'supp';
        } else if (isAutoArround) {
            RoundingType = 'auto';
        } else {
            RoundingType = 'noRounding';
        }

        if (isSuppArroundTrait) {
            RoundingTypeTrait = 'supp';
        } else if (isAutoArroundTrait) {
            RoundingTypeTrait = 'auto';
        } else {
            RoundingTypeTrait = 'noRounding';
        }

        if (isSuppArroundPrest) {
            RoundingTypePrest = 'supp';
        } else if (isAutoArroundPrest) {
            RoundingTypePrest = 'auto';
        } else {
            RoundingTypePrest = 'noRounding';
        }

        if (isSuppArroundCondi) {
            RoundingTypeCondi = 'supp';
        } else if (isAutoArroundCondi) {
            RoundingTypeCondi = 'auto';
        } else {
            RoundingTypeCondi = 'noRounding';
        }

        console.log('validate.selectedListAvenant = ', selectedListAvenant);
        console.log('validate.inputAll = ', inputAll);
        console.log('validate.inputTrait = ', inputTrait);
        console.log('validate.inputCondi = ', inputCondi);
        console.log('validate.inputPresta = ', inputPresta);

        if (selectedListAvenant.length > 2000) {  // divide the list of amendements  to avoid the CPU Limit(processe bulider & triggers are waiting somewhere ^^)       
            selectedListAvenant2 = selectedListAvenant.splice(0, Math.ceil(selectedListAvenant.length / 2));

            console.log('selectedListAvenant---', selectedListAvenant.length);
            console.log('selectedListAvenant2---', selectedListAvenant2.length);
        }

        //call Apex
        var action = component.get("c.increaseValues");
        action.setParams({
            lstAmend: selectedListAvenant,
            inputValue: inputAll,
            inputValueTraitement: inputTrait,
            inputValueTransport: inputPresta,
            inputValueConditionnement: inputCondi,
            RoundingType: RoundingType,
            RoundingTypeTrait : RoundingTypeTrait,
            RoundingTypePrest : RoundingTypePrest,
            RoundingTypeCondi : RoundingTypeCondi,
            comment : comment
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                if (response.getReturnValue() === "ok") {
                    if (selectedListAvenant2) {
                        helper.incPerOnAmendementCallBack(component, event, helper, selectedListAvenant2)
                    } else {
                        helper.toastdMessage("Augmentation tarifaire", "Augmentations effectuées avec succès", "success");
                        //Fire event for update list 
                        var action = component.get('v.action');//get action to fire the specific event (event for contracts or Amendements)       
                        if (action === 'increasePercentAmend' || action === 'increasePercent' || action === 'incfixedPrice' || action === 'fixedPrice') {
                            var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                            appEvent.setParams({
                                "isCancel": false
                            });
                            //we must refresh contract List too to refresh related Amendements :(
                            var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                            appEventCont.setParams({
                                "isCancel": false,
                                "isAvenantUpdate": true,
                                "actionFromContrat": component.get('v.actionFromContrat')
                            });
                            appEventCont.fire();


                        } else {
                            var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                            appEvent.setParams({
                                "isCancel": false,
                                "actionFromContrat": component.get('v.actionFromContrat')
                            });
                        }
                        appEvent.fire();

                        component.destroy();
                    }

                } else {
                    helper.toastdMessage("Augmentation tarifaire", "Augmentation non effectuée, veuillez saisir au moins un pourcentage supérieur à zéro", "warning");
                    //Fire event for update list 
                    var action = component.get('v.action');//get action to fire the specific event (event for contracts or Amendements)       
                    if (action === 'increasePercentAmend' || action === 'increasePercent' || action === 'incfixedPrice' || action === 'fixedPrice') {
                        var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                        appEvent.setParams({
                            "isCancel": true
                        });

                    } else {
                        var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                        appEvent.setParams({
                            "isCancel": true,
                            "actionFromContrat": component.get('v.actionFromContrat')
                        });
                    }
                    appEvent.fire();
                    component.destroy();
                }

            } else {
                console.log('Erreur rencontrée pendant la mise à jours ' + state);
                helper.toastdMessage("Augmentation tarifaire", "Erreur rencontrée pendant l'augmentation, veuillez contacter l'administrateur", "error");
                component.destroy();
            }
        });
        $A.enqueueAction(action);

    },

    incFixedPriceCallBack: function (component, event, helper, selectedListAvenant) {
        var selectedListAvenant = selectedListAvenant;
        var inputAll = component.get("v.defaultValue") != "" ? component.get("v.defaultValue") : 0.0;
        var inputTrait = component.get("v.defaultValueTrait") != "" ? component.get("v.defaultValueTrait") : 0.0;
        var inputCondi = component.get("v.defaultValueCondi") != "" ? component.get("v.defaultValueCondi") : 0.0;
        var inputPresta = component.get("v.defaultValuePresta") != "" ? component.get("v.defaultValuePresta") : 0.0;
        var checkBoxValue = component.get("v.value")[0];

        console.log('validate.selectedListAvenant = ', selectedListAvenant);
        console.log('validate.inputAll = ', inputAll);
        console.log('validate.inputTrait = ', inputTrait);
        console.log('validate.inputCondi = ', inputCondi);
        console.log('validate.inputPresta = ', inputPresta);
        console.log('validate.checkBoxValue = ', checkBoxValue);

        //call Apex
        var action = component.get("c.increaseAMEByValue");
        action.setParams({
            amendmentList: selectedListAvenant,
            inputValue: inputAll,
            inputValueTraitement: inputTrait,
            inputValueTransport: inputPresta,
            inputValueConditionnement: inputCondi
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.toastdMessage("Augmentation tarifaire", "Augmentations effectuées avec succès", "success");
                //Fire event for update list 
                var action = component.get('v.action');//get action to fire the specific event (event for contracts or Amendements)       
                if (action === 'increasePercentAmend' || action === 'increasePercent' || action === 'incfixedPrice' || action === 'fixedPrice') {
                    var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                    appEvent.setParams({
                        "isCancel": false
                    });
                    //we must refresh contract List too to refresh related Amendements :(
                    var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                    appEventCont.setParams({
                        "isCancel": false,
                        "isAvenantUpdate": true,
                        "actionFromContrat": component.get('v.actionFromContrat')
                    });
                    appEventCont.fire();


                } else {
                    var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                    appEvent.setParams({
                        "isCancel": false,
                        "actionFromContrat": component.get('v.actionFromContrat')
                    });
                }
                appEvent.fire();

                component.destroy();
            } else {
                console.log('Erreur rencontrée pendant la mise à jours ' + state);
                helper.toastdMessage("Augmentation tarifaire", "Erreur rencontrée pendant l'augmentation, veuillez contacter l'administrateur", "error");
                component.destroy();
            }
        });
        $A.enqueueAction(action);
    },

    incFixedPrice: function (component, event, helper) {
        var selectedListAvenant = component.get("v.selectedListAvenant");
        var inputAll = component.get("v.defaultValue") != "" ? component.get("v.defaultValue") : 0.0;
        var inputTrait = component.get("v.defaultValueTrait") != "" ? component.get("v.defaultValueTrait") : 0.0;
        var inputCondi = component.get("v.defaultValueCondi") != "" ? component.get("v.defaultValueCondi") : 0.0;
        var inputPresta = component.get("v.defaultValuePresta") != "" ? component.get("v.defaultValuePresta") : 0.0;
        var checkBoxValue = component.get("v.value")[0];
        var selectedListAvenant2;
        var comment = component.get("v.comment");

        console.log('validate.selectedListAvenant = ', selectedListAvenant);
        console.log('validate.inputAll = ', inputAll);
        console.log('validate.inputTrait = ', inputTrait);
        console.log('validate.inputCondi = ', inputCondi);
        console.log('validate.inputPresta = ', inputPresta);
        console.log('validate.checkBoxValue = ', checkBoxValue);

        if (selectedListAvenant.length > 2000) {  // divide the list of amendements  to avoid the CPU Limit(processe bulider & triggers are waiting somewhere ^^)       
            selectedListAvenant2 = selectedListAvenant.splice(0, Math.ceil(selectedListAvenant.length / 2));

            console.log('selectedListAvenant---', selectedListAvenant.length);
            console.log('selectedListAvenant2---', selectedListAvenant2.length);
        }

        //call Apex
        var action = component.get("c.increaseAMEByValue");
        action.setParams({
            amendmentList: selectedListAvenant,
            inputValue: inputAll,
            inputValueTraitement: inputTrait,
            inputValueTransport: inputPresta,
            inputValueConditionnement: inputCondi,
            comment : comment
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                if (selectedListAvenant2) {
                    helper.incFixedPriceCallBack(component, event, helper, selectedListAvenant2);
                } else {
                    helper.toastdMessage("Augmentation tarifaire", "Augmentations effectuées avec succès", "success");
                    //Fire event for update list 
                    var action = component.get('v.action');//get action to fire the specific event (event for contracts or Amendements)       
                    if (action === 'increasePercentAmend' || action === 'increasePercent' || action === 'incfixedPrice' || action === 'fixedPrice') {
                        var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                        appEvent.setParams({
                            "isCancel": false
                        });
                        //we must refresh contract List too to refresh related Amendements :(
                        var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                        appEventCont.setParams({
                            "isCancel": false,
                            "isAvenantUpdate": true,
                            "actionFromContrat": component.get('v.actionFromContrat')
                        });
                        appEventCont.fire();


                    } else {
                        var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                        appEvent.setParams({
                            "isCancel": false,
                            "actionFromContrat": component.get('v.actionFromContrat')
                        });
                    }
                    appEvent.fire();

                    component.destroy();
                }
            } else {
                console.log('Erreur rencontrée pendant la mise à jours ' + state);
                helper.toastdMessage("Augmentation tarifaire", "Erreur rencontrée pendant l'augmentation, veuillez contacter l'administrateur", "error");
                component.destroy();
            }
        });
        $A.enqueueAction(action);
    },

    fixedPriceCallBack: function (component, event, helper, selectedListAvenant) {
        // debugger;
        var selectedListAvenant = selectedListAvenant;
        var inputAll = component.get("v.defaultValue") != "" ? component.get("v.defaultValue") : 0.0;
        var inputTrait = component.get("v.defaultValueTrait") != "" ? component.get("v.defaultValueTrait") : 0.0;
        var inputCondi = component.get("v.defaultValueCondi") != "" ? component.get("v.defaultValueCondi") : 0.0;
        var inputPresta = component.get("v.defaultValuePresta") != "" ? component.get("v.defaultValuePresta") : 0.0;
        var checkBoxValue = component.get("v.value")[0];

        console.log('validate.selectedListAvenant = ', selectedListAvenant);
        console.log('validate.inputAll = ', inputAll);
        console.log('validate.inputTrait = ', inputTrait);
        console.log('validate.inputCondi = ', inputCondi);
        console.log('validate.inputPresta = ', inputPresta);
        console.log('validate.checkBoxValue = ', checkBoxValue);

        //call Apex
        var action = component.get("c.increaseAMEFixedPrice");
        action.setParams({
            amendmentList: selectedListAvenant,
            inputValue: inputAll,
            inputValueTraitement: inputTrait,
            inputValueTransport: inputPresta,
            inputValueConditionnement: inputCondi
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.toastdMessage("Augmentation tarifaire", "Augmentations effectuées avec succès", "success");
                //Fire event for update list 
                var action = component.get('v.action');//get action to fire the specific event (event for contracts or Amendements)       
                if (action === 'increasePercentAmend' || action === 'increasePercent' || action === 'incfixedPrice' || action === 'fixedPrice') {
                    var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                    appEvent.setParams({
                        "isCancel": false
                    });
                    //we must refresh contract List too to refresh related Amendements :(
                    var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                    appEventCont.setParams({
                        "isCancel": false,
                        "isAvenantUpdate": true,
                        "actionFromContrat": component.get('v.actionFromContrat')
                    });
                    appEventCont.fire();


                } else {
                    var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                    appEvent.setParams({
                        "isCancel": false,
                        "actionFromContrat": component.get('v.actionFromContrat')
                    });
                }
                appEvent.fire();
                component.destroy();
            } else {
                console.log('Erreur rencontrée pendant la mise à jours ' + state);
                helper.toastdMessage("Augmentation tarifaire", "Erreur rencontrée pendant l'augmentation, veuillez contacter l'administrateur", "error");
                component.destroy();
            }
        });
        $A.enqueueAction(action);
    },

    fixedPrice: function (component, event, helper) {
        // debugger;
        var selectedListAvenant = component.get("v.selectedListAvenant");
        var inputAll = component.get("v.defaultValue") != "" ? component.get("v.defaultValue") : 0.0;
        var inputTrait = component.get("v.defaultValueTrait") != "" ? component.get("v.defaultValueTrait") : 0.0;
        var inputCondi = component.get("v.defaultValueCondi") != "" ? component.get("v.defaultValueCondi") : 0.0;
        var inputPresta = component.get("v.defaultValuePresta") != "" ? component.get("v.defaultValuePresta") : 0.0;
        var checkBoxValue = component.get("v.value")[0];
        var comment = component.get("v.comment");
        var selectedListAvenant2;

        console.log('validate.selectedListAvenant = ', selectedListAvenant);
        console.log('validate.inputAll = ', inputAll);
        console.log('validate.inputTrait = ', inputTrait);
        console.log('validate.inputCondi = ', inputCondi);
        console.log('validate.inputPresta = ', inputPresta);
        console.log('validate.checkBoxValue = ', checkBoxValue);

        if (selectedListAvenant.length > 2000) {  // divide the list of amendements  to avoid the CPU Limit(processe bulider & triggers are waiting somewhere ^^)       
            selectedListAvenant2 = selectedListAvenant.splice(0, Math.ceil(selectedListAvenant.length / 2));

            console.log('selectedListAvenant---', selectedListAvenant.length);
            console.log('selectedListAvenant2---', selectedListAvenant2.length);
        }

        //call Apex
        var action = component.get("c.increaseAMEFixedPrice");
        action.setParams({
            amendmentList: selectedListAvenant,
            inputValue: inputAll,
            inputValueTraitement: inputTrait,
            inputValueTransport: inputPresta,
            inputValueConditionnement: inputCondi,
            comment : comment
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                if (selectedListAvenant2) {
                    helper.fixedPriceCallBack(component, event, helper, selectedListAvenant2);
                } else {
                    helper.toastdMessage("Augmentation tarifaire", "Augmentations effectuées avec succès", "success");
                    //Fire event for update list 
                    var action = component.get('v.action');//get action to fire the specific event (event for contracts or Amendements)       
                    if (action === 'increasePercentAmend' || action === 'increasePercent' || action === 'incfixedPrice' || action === 'fixedPrice') {
                        var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                        appEvent.setParams({
                            "isCancel": false
                        });
                        //we must refresh contract List too to refresh related Amendements :(
                        var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                        appEventCont.setParams({
                            "isCancel": false,
                            "isAvenantUpdate": true,
                            "actionFromContrat": component.get('v.actionFromContrat')
                        });
                        appEventCont.fire();


                    } else {
                        var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                        appEvent.setParams({
                            "isCancel": false,
                            "actionFromContrat": component.get('v.actionFromContrat')
                        });
                    }
                    appEvent.fire();
                    component.destroy();
                }

            } else {
                console.log('Erreur rencontrée pendant la mise à jours ' + state);
                helper.toastdMessage("Augmentation tarifaire", "Erreur rencontrée pendant l'augmentation, veuillez contacter l'administrateur", "error");
                component.destroy();
            }
        });
        $A.enqueueAction(action);
    },

    toastdMessage: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": 'dismissible',
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    }
})