({

    doInit : function(component, event, helper) {
        console.log('LC_InputForm loaded..'); 
        console.log('Filiale..: ',component.get("v.filiale")); 
        console.log('selectedListCont..: ',component.get("v.selectedListCont")); 
        console.log('Filiale..: ',component.get("v.filiale")); 
    },

    submitForm: function (component, event, helper) {
        debugger;
        var action = component.get("v.action");
        switch (action) {
            case "increasePercent":               
                helper.incPerOnContract(component, event, helper);
                break;
            case "increasePercentAmend":               
                helper.incPerOnAmendement(component, event, helper);
                break;
            case "incfixedPrice":
                helper.incFixedPrice(component, event, helper); 
                break;
            case "fixedPrice":
                helper.fixedPrice(component, event, helper);
                break;           
           
        }    
    },
    
    fireInputFormEvent : function(component, event, helper) {
        var compEvent = component.getEvent("InputFormEvent");        
        compEvent.setParams({"defaultValue" : component.get("v.defaultValue") });
        compEvent.fire();
    },

    handleInputFormEvent : function(component, event, helper) {
        component.set("v.defaultValueTrait",component.get("v.defaultValue"));
        component.set("v.defaultValueCondi",component.get("v.defaultValue"));
        component.set("v.defaultValuePresta",component.get("v.defaultValue"));
        if (component.get("v.defaultValue") !== "") {
            component.find("inputTrait").set("v.disabled", true);
            component.find("inputPresta").set("v.disabled", true);
            component.find("inputCondi").set("v.disabled", true);
          } else {
            component.find("inputTrait").set("v.disabled", false);
            component.find("inputPresta").set("v.disabled", false);
            component.find("inputCondi").set("v.disabled", false);
          }
     },
 
     closeModal : function(component, event, helper) {
         // when a component is dynamically created in lightning, we use destroy() method to destroy it.
         var action = component.get('v.action');//get action to fire the specific event (event for contracts or Amendements)       
         if(action ==='increasePercentAmend' || action ==='increasePercent'||action ==='incfixedPrice' || action ==='fixedPrice'){         
             var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");   
             appEvent.setParams({
                 "isCancel": true
                 
             });
 
         }else{
             var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");  
             appEvent.setParams({
                "isCancel": true,
                "actionFromContrat": component.get('v.actionFromContrat')
            });              
         }
         appEvent.fire();
         component.destroy();
     },

     fireRefreshEvent : function(component, event, helper) {
        // Get the  event by using the
        // name value from aura:registerEvent
        //debugger;
        var action = component.get('v.action');//get action to fire the specific event (event for contracts or Amendements)

        if(action ==='increasePercentAmend' || action ==='increasePercent'||action ==='incfixedPrice' || action ==='fixedPrice'){         
            var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");   
            appEvent.setParams({
                "isCancel": false               
            });

        }else{
            var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
            appEvent.setParams({
                "isCancel": true,
                "actionFromContrat": component.get('v.actionFromContrat')
            });             
        }

         appEvent.fire();
       
    },

    onCheckSupp: function(component, event) {
        var checkCmp = component.find("checkboxSupp");      
       //alert(checkCmp.get("v.value"));
        component.set("v.isSuppArround", ""+checkCmp.get("v.value"));
        component.set("v.isNotArround", ""+ !checkCmp.get("v.value"));      
        
    },

    onCheckAuto: function(component, event) {
        var checkCmp = component.find("checkboxAuto");       
        //alert(checkCmp.get("v.value"));
        component.set("v.isSuppAuto", ""+checkCmp.get("v.value"));
        component.set("v.isNotArround", ""+ !checkCmp.get("v.value"));
      
    },

    onCheckSuppTrait: function(component, event) {
        var checkCmp = component.find("checkboxSuppTrait");      
       
        component.set("v.isSuppArroundTrait", ""+checkCmp.get("v.value"));
        component.set("v.isNotArroundTrait", ""+ !checkCmp.get("v.value"));
     
        
    },

    onCheckAutoTrait: function(component, event) {
        var checkCmp = component.find("checkboxAutoTrait");       
        
        component.set("v.isSuppAutoTrait", ""+checkCmp.get("v.value"));
        component.set("v.isNotArroundTrait", ""+ !checkCmp.get("v.value"));
      
    },

    onCheckSuppPrest: function(component, event) {
        var checkCmp = component.find("checkboxSuppPrest");      
       
        component.set("v.isSuppArroundPrest", ""+checkCmp.get("v.value"));
        component.set("v.isNotArroundPrest", ""+ !checkCmp.get("v.value"));
      
        
    },

    onCheckAutoPrest: function(component, event) {
        var checkCmp = component.find("checkboxAutoPrest");       
        
        component.set("v.isSuppAutoPrest", ""+checkCmp.get("v.value"));
        component.set("v.isNotArroundPrest", ""+ !checkCmp.get("v.value"));
      
    },

    onCheckSuppCondi: function(component, event) {
        var checkCmp = component.find("checkboxSuppCondi");      
       
        component.set("v.isSuppArroundCondi", ""+checkCmp.get("v.value"));
        component.set("v.isNotArroundCondi", ""+ !checkCmp.get("v.value"));
      
        
    },

    onCheckAutoCondi: function(component, event) {
        debugger;
        var checkCmp = component.find("checkboxAutoCondi");       
        
        component.set("v.isSuppAutoCondi", ""+checkCmp.get("v.value"));
        component.set("v.isNotArroundCondi", ""+ !checkCmp.get("v.value"));
      
    }
    
})