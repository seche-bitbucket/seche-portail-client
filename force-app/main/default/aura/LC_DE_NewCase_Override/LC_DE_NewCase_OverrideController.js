({
    handleSuccess : function(component, event, helper) {

        try{

            component.find('notifLib').showToast({
                "variant": "success",
                "title": "Account Created",
                "message": "Record ID: " + event.getParam("id")
            });

            let url = $A.get("$Label.c.deUrl");
            
            $A.get("e.force:closeQuickAction").fire();
            window.location.href = url+event.getParam("id");

        } catch(e) {
            console.log(e);
        }

    },

    handleSubmit : function(component, event, helper) {
        event.preventDefault(); // stop form submission
        var eventFields = event.getParam("fields");
        eventFields["RecordTypeId"] = $A.get("$Label.c.deCaseRecordType");
        component.find('createRequest').submit(eventFields);
    },

    handelCancel : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})