({
    doInit : function(component, event, helper) {       

        var action = component.get("c.getQuoteRecordsReminder");       
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("callback state: " + state);

            debugger;

            if (component.isValid() && state === "SUCCESS") {
                var context = response.getReturnValue();
                console.log("context: " + context.toString());                
               
                component.set("v.dataRecords",context);

                var mapFilterValues = new Map();

                  // when response successfully return from server then apply jQuery dataTable after 500 milisecond
                  setTimeout(function () {
                    var table;
                    //if table exists destroy it
                    if ($.fn.dataTable.isDataTable("#tableMassSendReminderId")) {
                        table = $("#tableMassSendReminderId").DataTable();
                        table.destroy();
                    }

                    // filter setup

                    // Begin Filters ----- Setup - add a text input to each header cell -----------//
                    // $('#tableId thead tr').clone(true).appendTo( '#tableId thead' );		

                    $('#tableMassSendReminderId thead tr:eq(1) th').each(function (i) {
                        //;
                        var title = '';
                        var currentIndex = $(this).context.cellIndex;

                         //Get saved filter input value  
                         if(component.get("v.filterValues")){
                            var mapVal = component.get("v.filterValues");                       
                            if (mapVal && mapVal.get(currentIndex)) {
                                title = mapVal.get($(this).context.cellIndex);
                            }   
                         }                 
                                                 
                                     
                        if (i > 0) {
                            //$(this).html( '<input type="text" value="'+title+'" class="column_search"/>' );
                            $(this).html('<input  id="' + $(this).context.cellIndex + '"  type="text" value="' + title + '" size="6" class="column_search" />');
                        }
                        //Event to handle filter requests
                        $(".column_search", this).on('keyup change', function () {
                           // ;
                            if(component.get("v.filterValues")){
                                mapFilterValues = component.get("v.filterValues");
                            }
                            
                            mapFilterValues.set($(this).parent().index(), $(this).context.value);
                            if (table.column(i).search() !== this.value) {
                                table
                                    .column(i)
                                    .search(this.value)
                                    .draw();
                            } else if (component.get("v.isFilterSet") && table.column(i).search()) {
                                mapFilterValues.forEach(function (values, key) {
                                    table
                                        .column(key)
                                        .search(values)
                                        .draw();
                                });                              
                            }
                            //Save user inputs filter in the Component in map attribut 
                            component.set("v.filterValues", mapFilterValues);                           
                        });

                    });

                    table = $("#tableMassSendReminderId").DataTable({
                        processing: true,
                        // serverSide: true,
                        "scrollX": true,
                        responsive: true,
                        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tout"]], 
                        iDisplayLength: 25,
                        fixedHeader: true,
                        scrollCollapse: true,
                        orderCellsTop: true,               
                        language: {
                            sProcessing: "Traitement en cours...",
                            sSearch: "Rechercher&nbsp;:",
                            sLengthMenu: "Afficher _MENU_ &nbsp;",
                            sInfo:
                                "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments&nbsp;",
                            sInfoEmpty:
                                "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                            sInfoFiltered:
                                "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                            sInfoPostFix: "",
                            sLoadingRecords: "Chargement en cours...",
                            sZeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                            sEmptyTable: "Aucun devis à envoyer trouvé",
                            oPaginate: {
                                sFirst: "Premier",
                                sPrevious: "Pr&eacute;c&eacute;dent",
                                sNext: "Suivant",
                                sLast: "Dernier"
                            },
                            oAria: {
                                sSortAscending:
                                    ": activer pour trier la colonne par ordre croissant",
                                sSortDescending:
                                    ": activer pour trier la colonne par ordre d&eacute;croissant"
                            },
                            select: {
                                rows: {
                                    _: "&nbsp;%d",
                                    0: "&nbsp;",
                                    1: "&nbsp;"
                                }
                            }
                        },
                        data:  component.get("v.dataRecords"),     
                                     
                        columnDefs: [
                            {
                                targets: 0,
                                data: null,
                                defaultContent: "",
                                orderable: false,
                                className: "select-checkbox",
                                checkboxes: {
                                    selectRow: true
                                }
                            },
                           
                            {
                                data: "Name",
                                targets: 1,
                                orderable: true
                            },

                            {
                                data: "Account.Name",
                                defaultContent: "",
                                targets: 2,
                                orderable: true
                            },

                            {
                                data: "Contact.Email",
                                defaultContent: "",
                                targets: 3,
                                orderable: true
                            },

                            {
                                data: "Contact.Name",
                                defaultContent: "",
                                targets: 4,
                                orderable: true
                            },

                            {
                                data: "Commercial__c",
                                defaultContent: "",
                                targets: 5,
                                orderable: true
                            },

                            {
                                data: "TotalPrice",
                                defaultContent: "",
                                targets: 6,
                                orderable: true
                            },

                            {
                                data: "Status",
                                defaultContent: "",
                                targets: 7,
                                orderable: true
                            },

                            {
                                data: "NbSendReminder__c",
                                defaultContent: "",
                                targets: 8,
                                orderable: true
                            },

                        ],

                        select: {
                            style: "multi",
                            selector: 'td:first-child'
                        },
                        order: [[3, "asc"]],
                      //  "destroy":true

                    });

                    //Init actions
                     //Init all event when reload data on the table
                     $('#tableMassSendReminderId_wrapper tbody').off('click');
                     $('#tableMassSendReminderId_wrapper tbody').off('change');

                       // Add event listener AllselectCheckbox forget selected contract                       
                        $('#tableMassSendReminderId_wrapper  tr th:first-child').change(function () {
                            var data = table.rows({ selected: true }).data();
                            let numSelected = data.length;
                            if (numSelected > 0) {
                            helper.getSelectedQuote(component, data);
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                duration: '5000',
                                key: 'info_alt',
                                type: 'info',
                                mode: 'pester',
                                message: "Vous avez sélectionné tous les " + numSelected + " devis à envoyer " 
                            });
                            toastEvent.fire();
                            }
                        });

                    // Add event listener checkbox forget selected contract
                    $("#tableMassSendReminderId_wrapper tbody").on("change", "td.select-checkbox", function () {
                        //debugger;
                        var data = table.rows({ selected: true }).data();
                        helper.getSelectedQuote(component, data);

                    });

                    component.set('v.spinner', 'display: none;');
                    component.set('v.table', 'display: block;');

                    table.columns.adjust();

                }, 800);
            }
        });
        
        $A.enqueueAction(action);
    },

    sendReminders : function(component, event, helper) {
        var selectedListQuote = component.get("v.selectedListQuote");     
        //debugger; 
        if (selectedListQuote.length > 0) {
            var action = component.get("c.SendReminders");    
            action.setParams({
                lstQuote : selectedListQuote });
    
            action.setCallback(this, function(response){
                var state = response.getState();
                console.log("callback state: " + state);
                console.log("callback message: " + state);
                if (component.isValid() && state === "SUCCESS") {
                    var response = response.getReturnValue();
                    console.log("response: " + response);
                    $A.get("e.force:closeQuickAction").fire();
                    helper.toastdMessage("Envoi de rappel", response, "success");              
                   $A.get("e.force:refreshView").fire();
                } else if(state == "ERROR"){
                    var errors = response.getError();                       
                    console.log("callback message: " +errors[0].message);  
                }
            });            
            $A.enqueueAction(action);
         }else{
            helper.toastdMessage("Envoi pour signature", "Veuillez sélectionner au moins un devis.", "warning");
         }            
       
    }
})