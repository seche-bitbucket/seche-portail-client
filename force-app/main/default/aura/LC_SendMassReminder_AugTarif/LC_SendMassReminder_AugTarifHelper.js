({
    toastdMessage: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": 'sticky',
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },

    getSelectedQuote: function (component, data) {
        var data = data;
        var newarrayQuote = [];
        for (var i = 0; i < data.length; i++) {
            console.log("data >> ", data[i].Name);
            newarrayQuote.push(data[i]);
        }
        component.set("v.selectedListQuote", newarrayQuote);
        console.log(
            "td.select-checkbox.selectedListQuote >> ",
            component.get("v.selectedListQuote")
        );
    }
})