({
    loadQuote: function (component, event, helper) {
        //get Quote record Id
        var action = component.get("c.getQuote");
        action.setParams({ "quoteId": component.get("v.recordId") });

        console.log('recordIdrecordIdrecordId : ', component.get("v.recordId"));

        //configure action handler
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.quot", response.getReturnValue());
            } else {
                console.log('Impossible de charger le devis : ' + state);
            }
        });
        $A.enqueueAction(action);
    },

    requestToSigned: function (component, event, helper) {
        var action = component.get("c.setStatus");
        console.log(component.get("v.selected"));
        action.setParams({
            "quot": component.get("v.quot"),
            "status": component.get("v.selected")
        });
        //configure response handler for this action
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.objClassController", response.getReturnValue());

                var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        mode :'pester' ,
                        type: response.getReturnValue().showToastMode,                       
                        message :  response.getReturnValue().message
                    });     
                    //Update the UI: closePanel, show toast, refresh page
                  $A.get('e.force:refreshView').fire();                                
                    $A.get("e.force:closeQuickAction").fire();   
                    resultsToast.fire();                         
                            
            } else if (state === "ERROR") {                
                console.log('Problem Sending For Signing, response state ' + state);
            } else {
                console.log('Problème inconnu: ' + state);
            }
        });

        $A.enqueueAction(action);

    },

    handleChange: function (component, event, helper) {

        console.log(event.getParam("value").toString());
        let t =event.getParam("value").toString();
        component.set("v.selected", t);
        console.log(component.get("v.selected"));

    },

    cancelCall: function (component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }

})