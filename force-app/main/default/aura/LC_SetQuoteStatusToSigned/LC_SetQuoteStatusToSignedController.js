({
    doInit : function(component, event, helper) {
        helper.loadQuote(component, event, helper);
    },
    requestToSigned: function(component, event, helper) {
        component.set('v.loaded', !component.get('v.loaded'));
        helper.requestToSigned(component, event, helper);
    },
    cancelRequest: function(component, event, helper) {
        helper.cancelCall(component, event, helper);
    },
    handleChange: function (component, event, helper) {
        // This will contain the string of the "value" attribute of the selected option
        helper.handleChange(component, event, helper);
    }

    
})