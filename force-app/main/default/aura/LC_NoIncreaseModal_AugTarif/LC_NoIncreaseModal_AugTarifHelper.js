({
    toastdMessage: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },
    processCallBack: function (component, event, helper, selectedListAvenant) {
        //debugger;
        var selectedListAvenant = selectedListAvenant;
        var comment = component.get("v.comment");

        console.log('noIncrease.selectedListAvenant = ', selectedListAvenant);
        //call Apex
        var action = component.get("c.dontIncreaseAME");
        action.setParams({
            amendmentList: selectedListAvenant,
            comment: comment
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.toastdMessage("Ne pas augmenter", response.getReturnValue().message, response.getReturnValue().toastMode);
                var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");   
                appEvent.setParams({
                    "isCancel": false                            
                });
                appEvent.fire();

                //we must refresh contract List too to refresh related Amendements :(
                    var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");  
                    appEventCont.setParams({
                    "isCancel": false,
                    "isAvenantUpdate" : true,
                    "actionFromContrat": component.get('v.actionFromContrat')          
                });   
                appEventCont.fire();
        
                component.destroy();

            } else {
                console.log('Erreur rencontrée pendant la mise à jours veuillez contacter l\'administrateur' + state);
            }
        });
        $A.enqueueAction(action);

    }
})