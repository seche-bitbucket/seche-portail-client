({  
    closeModal: function (component, event, helper) {
        // when a component is dynamically created in lightning, we use destroy() method to destroy it.
        var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");   
        appEvent.setParams({
            "isCancel": true                            
        });
        appEvent.fire();

        component.destroy();
    },   

    validation: function (component, event, helper) {
        //debugger;
        var selectedListAvenant = component.get("v.selectedListAvenant");
        var selectedListAvenant2;
        var comment = component.get("v.comment");

        console.log('noIncrease.selectedListAvenant = ', selectedListAvenant);

        if(selectedListAvenant.length> 2000){  // divide the list of amendements  to avoid the CPU Limit(processe bulider & triggers are waiting somewhere ^^)       
            selectedListAvenant2 = selectedListAvenant.splice(0, Math.ceil(selectedListAvenant.length / 2));

           console.log('selectedListAvenant---',selectedListAvenant.length);
           console.log('selectedListAvenant2---',selectedListAvenant2.length);
       }

        //call Apex
        var action = component.get("c.dontIncreaseAME");
        action.setParams({
            amendmentList: selectedListAvenant,
            comment: comment
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                if(selectedListAvenant2){
                    helper.processCallBack (component, event, helper, selectedListAvenant2);
                }else{
                    helper.toastdMessage("Ne pas augmenter", response.getReturnValue().message, response.getReturnValue().toastMode);
                    var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");   
                    appEvent.setParams({
                        "isCancel": false                            
                    });
                    appEvent.fire();
    
                    //we must refresh contract List too to refresh related Amendements :(
                        var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");  
                        appEventCont.setParams({
                        "isCancel": false,
                        "isAvenantUpdate" : true,
                        "actionFromContrat": component.get('v.actionFromContrat')        
                    });   
                    appEventCont.fire();
            
                    component.destroy();
                }               

            } else {
                console.log('Erreur rencontrée pendant la mise à jours veuillez contacter l\'administrateur' + state);
            }
        });
        $A.enqueueAction(action);

    },

    fireRefreshEvent : function(component, event, helper) {
        // Get the  event by using the
        // name value from aura:registerEvent
        //debugger;
        var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif"); 

         appEvent.fire();
       
    }
})