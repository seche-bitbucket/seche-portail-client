({
    //Set the current preference for the key filiale+filiere
    setColumnsDefs: function (component, event, helper, filiale, filiere, DEFINITIONS, COLUMNS) {
        //;

        console.log('LC_ContractsList_AugTarif---> Start setColumnsDefs..');
        //get columns visibility preference
        var mapColumnVisibilty = helper.setColumnsVisibility(component, event, helper);
        //const filiales = ["Séché Eco Industries Changé", "Tredi Hombourg", "Sotrefi", "Triadis", "Tredi S"];
        //const filieres = ["Stockage DD", "Physico", "Plateforme de transit", "Stockage DND", "Quai de transfert", "Plateforme de transit", "Incinération DD"];
        //component.set("v.isDefaultDisplayColumns", true);
        var columnDefs = [];

        columnDefs.push({
            targets: 0,
            data: null,
            defaultContent: "",
            orderable: false,
            width: '8px',
            className: "select-checkbox",
            checkboxes: {
                selectRow: true,
                stateSave: false
            }
        });

        function preDefined(field, index) {
            return {
                data: field,
                defaultContent: "",
                targets: index,
                orderable: true
            }
        }

        console.log('Column => '+COLUMNS);

        COLUMNS.forEach(e => {
            console.log('e => '+e);
            let index = COLUMNS.indexOf(e)+1;
            console.log('index => '+index);
            let constructed = {};
            let param = DEFINITIONS.find(d => (e == 'Traité') ? d.QualifiedApiName == 'Trait' : ((e == 'Contrat') ? d.QualifiedApiName == e : d.Label == e));
            
            console.log('key => '+DEFINITIONS.find(d => d.Label == e));
            console.log('param => '+JSON.stringify(param)+' typeOf => '+typeof param);
            
            // it will predefin the node
            constructed = preDefined(param.Data__c, index);

            // checking if there is a function nested
            if(param.Function__c == 'Yes' && param.On__c) {
                
                constructed[param.On__c] = function(data) { let t = data; let ret = eval(param.evalCode__c); return ret; }

                console.log('eval => '+JSON.stringify(constructed[param.On__c]));

            } else if(param.Function__c == 'No' && param.On__c) {
                console.log('func NO => '+param.evalCode__c);
                if(param.evalCode__c.length > 0) {
                    console.log(eval(param.evalCode__c));
                    constructed[param.On__c] = eval(param.evalCode__c);
                }
            }

            constructed.className = (param.ClassName__c) ? param.ClassName__c : '';

            console.log('constructed => '+JSON.stringify(constructed));

            columnDefs.push(constructed);
        });

        console.log('LC_ContractsList_AugTarif.columnDefs---> ', columnDefs);

        component.set("v.columnDefs", columnDefs);

        return columnDefs;
    },

    //Set the columns according user profile
    setColumnsVisibility: function (component, event, helper) {
        
        var mapColumnVisibilty = {};
        //Visible for only Assists users
        var currentUser = component.get("v.currentUser");
        console.log('currentUser'+currentUser.Profile.Name);
        if (currentUser) {
            if (currentUser.Profile.Name.includes('Assistante') || currentUser.Profile.Name == 'System Administrator') {
                mapColumnVisibilty[5] = true;
            }
        }
        console.log('LC_ContractsList_AugTarif.mapColumnVisibilty---> ', mapColumnVisibilty);
        return mapColumnVisibilty;
    },

    getContracts: function (component, event, helper) {

        return new Promise((resolve, reject) => {
            component.set("v.lstFaFCont", []);
            component.set("v.lstStandardCont", []);
    
            const filiale = component.get("v.filiale");
            const filiere = component.get("v.filiere");
            const natureCompte = component.get("v.natureCompte");

            if (natureCompte != '') {
                component.set("v.showButtonStandard", true);
            } else {
                component.set("v.showButtonStandard", false);
            }
    
            if(filiere == '--- None ---') {
                return;
            } else {
     
                let act = component.get('c.getMetadata');
                act.setParams({
                    filiale: filiale,
                    filiere: filiere,
                    natureCompte: natureCompte,
                    status: "Brouillon"
                });
    
                let columns;
        
                act.setCallback(this, function(response){
                    
                    //store state of response
                    var state = response.getState();
                    let tst = response.getReturnValue();
                    if (state === "SUCCESS") {
    
                        console.log('stat => '+response.getState());
                        console.log('val => '+JSON.stringify(response.getReturnValue()));
    
                        
                        let rsps = response.getReturnValue();
                        let settings = rsps[0];
                        let rsp;
    
                        let definitions = rsps[2];
    
                        console.log(filiale+' '+filiere);
                        console.log('settings => '+JSON.stringify(settings));
    
                        settings.forEach(e => {
                            console.log(e.Filiere__c);
                            console.log(e.Filiale__c);
                        })
    
                        if(filiale != '--- None ---' && filiere != '--- None ---') {
                            console.log('couple => '+settings.find(function(e) {
                                                                                    if(e.Filiere__c != undefined && e.Filiale__c != undefined) {
                                                                                        return e.Filiere__c.includes(filiere) && e.Filiale__c.includes(filiale);
                                                                                    }
                                                                                }));
                            if(settings.find(function(e) {
                                                            if(e.Filiere__c != undefined && e.Filiale__c != undefined) {
                                                                return e.Filiere__c.includes(filiere) && e.Filiale__c.includes(filiale);
                                                            }
                                                        })) {
                                                            
                                rsp = settings.find(function(e) {
                                                                    if(e.Filiere__c != undefined && e.Filiale__c != undefined) {
                                                                        return e.Filiere__c.includes(filiere) && e.Filiale__c.includes(filiale);
                                                                    }
                                                                });
    
                            }else{
                                console.log('one => '+settings.find(function(e) {
                                                                                    console.log(e);
                                                                                    if(e.Filiere__c != undefined) {
                                                                                        return e.Filiere__c.includes(filiere);
                                                                                    }else if(e.Filiale__c != undefined) {
                                                                                        return e.Filiale__c.includes(filiale);
                                                                                    }
                                                                                }));
                                
                                if(settings.find(function(e) {
                                                                console.log(e);
                                                                /* if(e.Filiere__c != undefined) {
                                                                    return e.Filiere__c.includes(filiere);
                                                                }else if(e.Filiale__c != undefined) {
                                                                    return e.Filiale__c.includes(filiale);
                                                                } */
                                                                if(e.Filiale__c != undefined) {
                                                                    console.log('filiale tiph : ' + e.Filiale__c);
                                                                    return filiale.includes(e.Filiale__c);
                                                                } else if(e.Filiere__c != undefined) {
                                                                    return e.Filiere__c.includes(filiere);
                                                                }
                                                            })) {
                                        rsp = settings.find(function(e) {
                                                                            /* if(e.Filiere__c != undefined) {
                                                                                return e.Filiere__c.includes(filiere);
                                                                            }else if(e.Filiale__c != undefined) {
                                                                                return e.Filiale__c.includes(filiale);
                                                                            } */
                                                                            if(e.Filiale__c != undefined) {
                                                                                return filiale.includes(e.Filiale__c);
                                                                            } else if(e.Filiere__c != undefined) {
                                                                                return e.Filiere__c.includes(filiere);
                                                                            }
                                                                        });
                                    
                                }else{
                                    rsp = settings.find(function(e) {
                                                                        return e.Label === 'standard';
                                                                    });
                                }
                            }
                        }
    
                        console.log('rsps => ',rsps[1]);
        
                        if(rsp != undefined) {
                            let str = rsp.Columns_Contracts__c;
                            let tmp = str.toString();
                            columns = tmp.split(',');
                        }
                    // ;
                        //set response value in lstOpp attribute on component.
                        component.set("v.lstCont", rsps[1]);
    
                        console.log("v.lstCont >>> ", component.get("v.lstCont"));
                        var isAvenantUpdate = event.getParam("isAvenantUpdate");
    
                        if (!isAvenantUpdate) {
                            var emptyList = [];
                            component.set("v.selectedListCont", emptyList);
                        }
    
                        var mapFilterValues = new Map();
                        var selectedIndx = [];
                    
                        var varcolumnDefs;
                        // when response successfully return from server then apply jQuery dataTable after 500 milisecond
                        setTimeout(function () {
                            //Set the number of buttons to display in pagination tool
                            $.fn.DataTable.ext.pager.numbers_length = 4;
                            var table;
                            //if table exists destroy it
                            console.log('condition => '+$.fn.dataTable.isDataTable("#tableId"))
                            if ($.fn.dataTable.isDataTable("#tableId")) {
                                console.log('in');
                                table = $("#tableId").DataTable();
                                console.log('found => '+table);
                                table.destroy();
                                component.set('v.showColumn', false);
                                $("#tableId tbody").empty();
                                console.log('content => '+JSON.stringify($('#tableId').html()));
                            }
    
                            varcolumnDefs = helper.setColumnsDefs(component, event, helper, component.get("v.filiale"), component.get("v.filiere"), definitions, columns);
    
                            component.set('v.columnsLabels', columns);
                            component.set('v.showColumn', true);
                            console.log('columnsLabels => '+component.get('v.columnsLabels'));
    
                            console.log('varcolumnDefs => '+varcolumnDefs);
                            
                            //clear table State
                            var resetSaveStatus = component.get("v.resetSaveStatus");
                            //;
                            if(resetSaveStatus){
                                localStorage.clear(); //reset status                         
                            component.set("v.resetSaveStatus", false);                            
                            // component.set("v.filterValues", mapTemp);   
                            }else if(!component.get("v.filterValues") && !component.get("v.keepState")){                      
                            localStorage.clear(); //reset status 
                                component.set("v.resetSaveStatus", false);  
                            }
                            
                            // Begin Filters ----- Setup - add a text input to each header cell -----------//
                            // $('#tableId thead tr').clone(true).appendTo( '#tableId thead' );				   
    
                            $('#tableId thead tr:eq(1) th').each(function (i) {
                                //;
                                var title = '';
                                var currentIndex = $(this).context.cellIndex;
    
                                //Get saved filter input value  
                                if(!resetSaveStatus && component.get("v.filterValues")){
                                    var mapVal = component.get("v.filterValues");                       
                                    if (mapVal && mapVal.get(currentIndex)) {
                                        title = mapVal.get($(this).context.cellIndex);
                                    }   
                                }                 
    
                                if (i > 0) {
                                    //$(this).html( '<input type="text" value="'+title+'" class="column_search"/>' );
                                    //resize input field filter buy column
                                    if ($(this).context.cellIndex == 1 || $(this).context.cellIndex == 2 ) {
                                        $(this).html('<input  id="' + $(this).context.cellIndex + '"  type="text" value="' + title + '" size="6" class="column_search" />');
                                    } else if ($(this).context.cellIndex == 6) {
                                        $(this).html('<input  id="' + $(this).context.cellIndex + '"  type="text" value="' + title + '" size="3" class="column_search" />');
                                    } else {
                                        $(this).html('<input  id="' + $(this).context.cellIndex + '"  type="text" value="' + title + '" size="10" class="column_search" />');
                                    }
    
                                }
                                //Event to handle filter requests
                                $(".column_search", this).on('keyup change', function () {
                                // ;
                                    if(component.get("v.filterValues")){
                                        mapFilterValues = component.get("v.filterValues");
                                    }
                                    
                                    mapFilterValues.set($(this).parent().index(), $(this).context.value);
                                    if (table.column(i).search() !== this.value) {
                                        table
                                            .column(i)
                                            .search(this.value)
                                            .draw();
                                    } else if (component.get("v.isFilterSet") && table.column(i).search()) {
                                        mapFilterValues.forEach(function (values, key) {
                                            table
                                                .column(key)
                                                .search(values)
                                                .draw();
                                        });                              
                                    }
                                    //Save user inputs filter in the Component in map attribut 
                                    component.set("v.filterValues", mapFilterValues);                           
                                });
    
                            });
    
                            console.log('pass-stage1');
                        
                            table = $("#tableId").DataTable({
                                //This method is trigered  after table initilzation and data load are completed
                                /*"initComplete": function (settings, json) {                              
                                console.log('initComplete --> start');
                                    
                                }, */
    
                                processing: true,
                                scrollY: '90vh',
                                scrollX: true,
                                scrollCollapse: true,
                                orderCellsTop: true,
                                // deferRender: true,
                                // responsive: true,
                                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tout"]],
                                iDisplayLength: component.get("v.selectedLenght"),
                                pagingType: "full_numbers",
                                stateSave: true,
                                language: {
                                    sProcessing: "Traitement en cours...",
                                    sSearch: "",
                                    searchPlaceholder: "Rechercher",
                                    sLengthMenu: "&nbsp;Afficher _MENU_ &nbsp;",
                                    sInfo:
                                        "Enregistrement _START_ &agrave; _END_ sur _TOTAL_ enregistrements",
                                    sInfoEmpty:
                                        "Enregistrement 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                                    sInfoFiltered:
                                        "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                                    sInfoPostFix: "",
                                    sLoadingRecords: "Chargement en cours...",
                                    sZeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                                    sEmptyTable: "Aucun contrat en cours d'augmentation trouvé",
                                    oPaginate: {
                                        sFirst: "Premier",
                                        sPrevious: "Pr&eacute;c&eacute;dent",
                                        sNext: "Suivant",
                                        sLast: "Dernier"
                                    },
                                    oAria: {
                                        sSortAscending:
                                            ": activer pour trier la colonne par ordre croissant",
                                        sSortDescending:
                                            ": activer pour trier la colonne par ordre d&eacute;croissant"
                                    },
                                    select: {
                                        rows: {
                                            _: "&nbsp;",
                                            0: "&nbsp;",
                                            1: "&nbsp;"
                                        }
                                    }
                                },
                                data: component.get("v.lstCont"),
                                function() {
                                    console.log('before data => '+varcolumnDefs);
                                },
                                columnDefs: varcolumnDefs,
                                function() {
                                    console.log('After data => '+varcolumnDefs);
                                },
                                select: {
                                    rowId: 'extn',
                                    style: "multi",
                                    selector: 'input[type="checkbox"]'                            
                                },
                                order: [[3, "asc"]],
    
    
                                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                    //Dynamic row color (if Amendement is treated => set backGroundColor)
    
                                    console.log('aData-----> ', aData);
                                    console.log('aData----->isUnderContract__c ', aData.Avenants__r);
                                    console.log('aData.TreatedContract__c -----> ', aData.TreatedContract__c);
    
                                    // var IsUnderContract = false;
                                    // var avenant= [] ;
                                    // avenant = aData.Avenants__r;
                                    // if(aData.TreatedContract__c){
                                    //     for(var i= 0; i <avenant.length; i++){
                                    //         if(avenant[i].IsUnderContract__c == true ){
                                    //             IsUnderContract = true;
                                    //         }
                                    //     }
                                    //     if (IsUnderContract) { //== Rank off column : sous contrat                
                                    //         $('td', nRow).css('background-color', '#f36088');
                                    //     }
                                    // }
                            
                                    if (aData.IsUnderContract__c) { //== Rank off column : sous contrat                
                                        $('td', nRow).css('background-color', '#f36088');
                                    }
                                    else if (aData.Face_Face__c) { //== Rank off column : Face_Face__c                 
                                        $('td', nRow).css('background-color', '#ffb962');
                                    }
                                    else if (aData.TreatedContract__c && !aData.TECH_Excluded__c) {
                                        $('td', nRow).css('background-color', '#8ac78a');
                                    }
                                    else if (aData.TECH_Excluded__c) { //== Rank off column : Face_Face__c                 
                                        $('td', nRow).css('background-color', '#a3a3a3');
                                    }
                                    //Dynamic restore selected contract
    
                                    var selectedListContIds = component.get("v.selectedListContIds");
                                    if (selectedListContIds.includes(aData.Id)){     
                                        console.log('selected contract > aData.Id -----> ',aData.Id); 
                                        selectedIndx.push(nRow._DT_RowIndex);
    
                                    /*  var idx = nRow._DT_RowIndex;
                                        var checkbox =   $('td', nRow).find('input').prop('checked', true);
                                        var data = table.rows({ selected: true }).data();
                                        helper.getSelectedContract(component,data);  */
                                    // table.row(':eq('+idx+')').select();  
    
                                    } 
                                },
    
                                "drawCallback": function (settings) {
                                    //;                           
                                    var api = new $.fn.dataTable.Api(settings);                            
                                    // You might do something more useful with it!                           
                                    console.log(api.rows({ page: 'current' }).data());
                                },
    
                                "headerCallback": function (thead, data, start, end, display) {
                                    let recordSelectedCount = component.get("v.recordSelectedCount");
                                // $('#tableId_wrapper').find('th').eq(0).html( 'Page '+ component.get("v.recordSelectedCount") );                        
    
                                }
    
                            });
    
                            console.log('pass-stage2');
    
                            //Init all event when reload data on the table
                            $('#tableId_wrapper tbody').off('click');
                            $('#tableId_wrapper tbody').off('change');
    
    
                        /* table.buttons().container()
                                .appendTo('#tableId_wrapper .col-sm-6:eq(0)');*/
    
                            //fixed header
                            // new $.fn.dataTable.FixedHeader( table );
    
                            // $('.dataTables_length').addClass('bs-select');                   
    
                            $('#tableId_wrapper tr th:first-child').on("change", function () {
                                //;
                                
                                var checkValue = $(this).find('input[type="checkbox"]').prop('checked');
    
                                if (checkValue) {
                                    var data = table.rows('.selected').data();
                                    helper.getSelectedContract(component, data);
                                    //Alert when all contracts are selected                           
                                    $('#selectedRecordsNumb').text( data.length +' ligne(s) séléctionnée(s)');
                                    let numSelected = data.length;
                                    if (numSelected > 0) {
                                        let filiale = component.get("v.filiale");
                                        let filiere = component.get("v.filiere");
                                        //helper.toastdMessage("Augmentation tarifaire", "Vous avez sélectionné tous les "+numSelected+ " contrats de la filiale "+filiale+ " et filière "+filiere , "warning");
                                        var toastEvent = $A.get("e.force:showToast");
                                        toastEvent.setParams({
                                            duration: '5000',
                                            key: 'info_alt',
                                            type: 'info',
                                            mode: 'pester',
                                            message: "Vous avez sélectionné tous les " + numSelected + " contrats de la filière " + filiere
                                        });
                                        toastEvent.fire();
                                    }
    
                                } else {
                                    var emptyList = [];
                                    component.set("v.selectedListCont", emptyList);
                                    $('#selectedRecordsNumb').text( 0 +' ligne(s) séléctionnée(s)');                            
                                }
    
    
                            });
    
    
                            // Add event listener checkbox forget selected contract
                            // $("#tableId_wrapper tbody").on("change", "td.select-checkbox", function () {
                            //     var data = table.rows('.selected').data();
                            //     helper.getSelectedContract(component, data);                       
                            //     $('#selectedRecordsNumb').text( data.length +' ligne(s) séléctionnée(s)');
                            // });
                            
    
                            $("#tableId_wrapper tbody").on("click", "td.select-checkbox", function () {
                                let row = $(this).closest("tr")
                                let checkbox = $(this).find('input[type="checkbox"]')
                                let isChecked = checkbox.prop('checked')
                                setTimeout(function() {
                                    if(!isChecked) {
                                        row.removeClass('selected')
                                        checkbox.prop('checked', false)
                                    } else {
                                        row.addClass('selected')
                                        checkbox.prop('checked', true)
                                    }
                                    var data = table.rows('.selected').data();
                                    helper.getSelectedContract(component, data);                       
                                    $('#selectedRecordsNumb').text(data.length +' ligne(s) séléctionnée(s)');
                                }, 50)
                            })
    
                            // Add event listener for opening Edit pop up 
                            $("#editButton").on("click", function () {
                            
                                var data = table.rows('.selected').data();
                                var newarray = [];
                                for (var i = 0; i < data.length; i++) {
                                    console.log("data >> ", data[i].Name);
                                    newarray.push(data[i]);
                                }
                                component.set("v.selectedListCont", newarray);
                                if (component.get("v.selectedListCont").length > 0) {
                                    helper.openEditModal(component, event, helper, component.get("v.selectedListCont"));
                                } else {
                                    helper.toastdMessage(" Modification/Visualisation", "Veuillez sélectionner au moins un contrat.", "warning");
                                }
                            });
    
                            // Add event listener for opening and closing Pop updetails
                            $("#tableId_wrapper tbody").on("click", "td.details-popup", function () {
                            
                                var tr = $(this).closest("tr");
                                //var tdi = tr.find("i.fa");
                                var row = table.row(tr);
                                var rowDatas = [];
                                var d = row.data();
                                rowDatas.push(d);
                                helper.openEditModal(component, event, helper, rowDatas);
                            });
                            // Add event listener for opening and closing details
                            $("#tableId_wrapper tbody").on("click", "td.details-contract", function () {
                                //;
                                var tr = $(this).closest("tr");
                                var tdi = tr.find("i.dec");
                                var row = table.row(tr);
    
                                if (row.child.isShown()) {
                                    // This row is already open - close it
                                    row.child.hide();
                                    tr.removeClass("shown");
                                    tdi.first().removeClass("fa-minus-square");
                                    tdi.first().addClass("fa-plus-square");
                                } else {
                                    // Open this row
                                    row.child(helper.formatDetail(component, event, helper, row.data())).show();
                                    tr.addClass("shown");
                                    tdi.first().removeClass("fa-plus-square");
                                    tdi.first().addClass("fa-minus-square");
                                }
                            });
                            //get lenght showed elements  
                            $("select[name='tableId_length']").on("change", function () {
                                //;
                                let selectedLenght = $(this).children("option:selected").val();                       
                                if(selectedLenght == -1){
                                    var allRecords = component.get("v.lstCont").length;
                                    component.set("v.selectedLenght", allRecords); 
                                }else{
                                    component.set("v.selectedLenght", selectedLenght); 
                                }                                            
                            });
    
                            //Event to load saved state
                            $("#tableId_wrapper").on('stateLoadParams.dt', function (e, settings, data) {                       
        
                                //to trigger filter (simulate user filtering) 
                                    let valTemp = $('#1').val(); 
                                    $('#1').val(valTemp);                                                  
                            } );
        
                            
                        
                            //Event to save state 
                            $('#tableId_wrapper').on('stateSaveParams.dt', function (e, settings, data) {
                            // ;
                            data.filters = component.get("v.filterValues");                       
                                //data.search.search = "";                       
                            });
                                
                            component.set('v.spinner', false);

                            resolve();
    
    
                        }, 100);
    
                    } else {
                        reject('Augmentation Tarifaire : Internal Server Error')
                    }
                    
                });
                $A.enqueueAction(act);
    
            }
        })

    },

    /* Formatting function for row details - modify as you need */
    formatDetail: function (component, event, helper, d) {
        // `d` is the original data object for the row
        var accountName = d.Account__r == null ? '' : d.Account__r.Name;
        var isTreated = d.TreatedContract__c == true ? 'Oui' : 'Non';
        var contactRelatedName = d.ContactRelated__r == null ? '' : d.ContactRelated__r.Name;
        var customerNature = d.CustomerNature__c == null ? '' : d.CustomerNature__c;
        var filiale = d.Filiale__c == null ? '' : d.Filiale__c;
        var contractType = d.ContractType__c == null ? '' : d.ContractType__c;
        var filiere = d.Filiere__c == null ? '' : d.Filiere__c;
        var associatedSalesRepName = d.AssociatedSalesRep__r == null ? '' : d.AssociatedSalesRep__r.Name;
        var status = d.Status__c == null ? '' : d.Status__c;
        var opportunity = d.Opportunity__r == null ? '' : d.Opportunity__r.Name;
        var type_de_convention = d.Type_de_convention__c == null ? '' : d.Type_de_convention__c;
        /* var contractEndDate = d.ContractEndDate__c == null ? '' : d.ContractEndDate__c; */
        var contractEndDate = d.ContractEndDate__c == null ? '' : $A.localizationService.formatDate(d.ContractEndDate__c, "DD/MM/YYYY");
        var increaseComment = d.IncreaseComment__c == null ? '' : d.IncreaseComment__c;
        var increaseRatio = d.IncreaseRatio__c == null ? '' : d.IncreaseRatio__c;
        var filiale = d.Filiale__c == null ? '' : d.Filiale__c;
        var filiale = d.Filiale__c == null ? '' : d.Filiale__c;
        var filiale = d.Filiale__c == null ? '' : d.Filiale__c;
        var regroupmentIndex = d.RegroupmentIndex__c == null ? '' : d.RegroupmentIndex__c;

        return (
            '<div class="slds-box">' +
            '<Table border ="1"  class="tabWidth">' +
            '<tr>' +
            '<td colspan="4" style="background-color: rgb(248, 203, 173);">Identité du client et du producteur</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Compte :</td><td>' + accountName + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Contact associé :</td><td>' + contactRelatedName + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Nature du client :</td><td>' + customerNature + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td colspan="4" style="background-color: rgb(221, 235, 247);">Données X3</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Nom de Contrats :</td><td>' + d.Name + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Filiale :</td><td>' + filiale + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Type de Contrat :</td><td>' + contractType + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Filière :</td><td>' + filiere + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Total Avenants :</td><td>' + d.AmountOfAmandements__c + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Commercial Associé :</td><td>' + associatedSalesRepName + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td colspan="4" style="background-color: rgb(226, 239, 218);">Statistiques de livraison et facturation des exercices passés</td>' +
            '</tr>' +
            '<tr>' +
            '<td>CA N :</td><td>' + d.CA_N__c + '</td>' +
            '</tr>' +


            '<tr>' +
            '<td colspan="4" style="background-color: rgb(242, 150, 150);">L\'augmentation appliquée</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Contrat traité :</td><td>' + isTreated + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Avenants traité :</td><td>' + d.TreatedAmandements__c + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Statut :</td><td>' + status + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Opportunité :</td><td>' + opportunity + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Type convention :</td><td>' + type_de_convention + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Date fin contrat :</td><td>' + contractEndDate + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Commentaire d\'augmentation :</td><td>' + increaseComment + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>% augmentation :</td><td>' + increaseRatio + '</td>' +
            '</tr>' +
            '<tr">' +
            '<td>CA N+1 Estimatif :</td><td>' + d.EstimateTurnoverNextYear__c + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Marge développement :</td><td>' + d.AddedValue__c + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Indice regroupement :</td><td>' + regroupmentIndex + '</td>' +
            '</tr>' +

            '</Table>' +

            '</div>'

        );


    },

    updateContract: function (component, event, helper) {

        var data = component.get("v.selectedListCont");

        if (component.get("v.selectedListCont").length == 1) {
            helper.openEditModal(component, event, helper, component.get("v.selectedListCont"));
        } else if (component.get("v.selectedListCont").length > 1) {
            helper.toastdMessage(" Modification/Visualisation", "Veuillez utiliser l'onglet Contrats pour la modification en masse des contrats.", "warning");
        } else {
            helper.toastdMessage(" Modification/Visualisation", "Veuillez sélectionner un contrat.", "warning");
        }

    },

    getSelectedContract: function (component, data) {
        //;       
        var data = data;
        var newarrayCont = [];
        var newarrayAven = [];
        var selectedListContIds = [];
        for (var i = 0; i < data.length; i++) {
            console.log("data >> ", data[i].Name);
            newarrayCont.push(data[i]);
            //Save selected Ids
            selectedListContIds.push(data[i].Id);
            //Get realated avenants                            
            if (data[i] && data[i].Avenants__r) {
                for (var k = 0; k < data[i].Avenants__r.length; k++) {
                    console.log("data.Avenants__r.Nature__c >> ", data[i].Avenants__r[k].Nature__c);
                    newarrayAven.push(data[i].Avenants__r[k]);
                }

            }
        }

        component.set("v.selectedListCont", newarrayCont);
        component.set("v.selectedListAvenant", newarrayAven);
        component.set("v.selectedListContIds", selectedListContIds);
        console.log(
            "td.select-checkbox.selectedListCont >> ",
            component.get("v.selectedListCont")
        );

        console.log(
            "td.select-checkbox.selectedListAvenant >> ",
            component.get("v.selectedListAvenant")
        );

        console.log(
            "td.select-checkbox.selectedListContIds >> ",
            component.get("v.selectedListContIds")
        );

    },

    //method to clear datatable state
    initState: function (component, event) {        
        component.set("v.resetSaveStatus", true);     
    },

    handleHeaderPickListEvent: function (component, event, helper) {
        //Reset savedStatus of datatable
        helper.initState(component, event);      
        var filiale = event.getParam("filiale");
        var filiere = event.getParam("filiere");
        var natureCompte = event.getParam("natureCompte");
      
        // set the handler attributes based on event data
        component.set("v.filiale", filiale);
        component.set("v.filiere", filiere);
        component.set("v.natureCompte", natureCompte);

        console.log("ContratListComponent.filiale>> ", component.get("v.filiale"));
        console.log("ContratListComponent.filiere >> ", component.get("v.filiere"));
        console.log("ContratListComponent.natureCompte >> ", component.get("v.natureCompte"));
        $('#0').val();  //to trigger filter (simulate user writting)             
    },

    fireEvent: function (component, event, helper) {
        //;
        console.log("fireEvent--- contractList Event  fire..");
        // Get the  event by using the
        // name value from aura:registerEvent
        var appEvent = $A.get("e.c:LE_ContractsList_AugTarif");
        var selectedListCont = component.get("v.selectedListCont");
        console.log(
            "LE_ContractsList_AugTarif.selectedListCont :",
            selectedListCont
        );
        appEvent.setParams({
            selectedListCont: selectedListCont
        });
        appEvent.fire();
    },

    openEditForm: function (component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "mode": "dismissible",
            "message": "The record has been Saved successfully."
        });
        toastEvent.fire();
    },

    openEditModal: function (component, event, helper, selectedListCont) {
        //Dynamic creation of lightningModalChild component and appending its markup in a div
        $A.createComponent('c:LC_RecordDetail_AugTarif', {
            'headerText': 'Détails du contrat ',
            'selectedListSObject': selectedListCont,
            'sObjectName': 'Contract__c'
        },
            function (modalComponent, status, errorMessage) {
                if (status === "SUCCESS") {
                    //Appending the newly created component in div
                    var body = component.find('showEditModal').get("v.body");
                    body.push(modalComponent);
                    component.find('showEditModal').set("v.body", body);
                } else if (status === "INCOMPLETE") {
                    console.log('Server issue or client is offline.');
                } else if (status === "ERROR") {
                    console.log('error');
                }
            }
        );
    },

    toastdMessage: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": 'dismissible',
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },

    showIncreaseForm: function (component, event, helper, action, headerText) {

        var selectedListCont = component.get("v.selectedListCont");
        var selectedListAvenant = component.get("v.selectedListAvenant");
        var actionFromContrat = component.get("v.actionFromContrat");

        for (var k=0; k < selectedListCont.length; k++) {
            if (selectedListCont[k].PriceNextyearFromERP__c) {
                helper.toastdMessage("Augmenter par pourcentage", "Vous ne pouvez pas augmenter un contrat dont le tarif provient de l'ERP.", "warning");
                return;
                }
        } 

        if (selectedListCont.length > 0) {
            let isExclude = false;
            let isCompleted = false;

            selectedListCont.forEach(function (cont) {
                if (cont.TECH_Excluded__c == true) {
                    isExclude = true;
                }
                if (cont.TreatedContract__c == true){
                    isCompleted = true;
                }
            });

           if (isExclude) {
                helper.toastdMessage("Augmenter par pourcentage", "Vous avez sélectionné un contrat exclu et ne pouvez donc pas l'augmenter. Veuillez d'abord le réinitialiser.", "warning");
            } else if(isCompleted) {
                helper.toastdMessage("Augmenter par pourcentage", "Vous avez sélectionné un contrat déjà traité. Veuillez d'abord le réinitialiser.", "warning");
            } else {
                if (selectedListAvenant.length > 0) {
                    let isPriceCurrentYearZero = false;
                    let amendmentNamePriceZero = '';
                    //check if amendement PriceCurrentYear is zero
                    selectedListAvenant.forEach(function (amend) {
                        if (amend.PriceCurrentYear__c == 0 && !amend.CompletedAmadement__c) {
                            console.log(amend);
                            amendmentNamePriceZero = amend.Name;
                            isPriceCurrentYearZero = true;
                        }
                    });
                    if (isPriceCurrentYearZero) {
                        helper.toastdMessage("Augmentation par pourcentage", "Le prix N+1 de l'avenant " + amendmentNamePriceZero + " sera nul car le prix N est lui aussi nul ; veuillez saisir un prix fixe pour cette ligne.", "warning");
                    } else {
                        //Dynamic creation of lightningModalChild component and appending its markup in a div
                        $A.createComponent('c:LC_InputModalForm_AugTarif', {
                            'headerText': headerText,
                            'selectedListAvenant': selectedListAvenant,
                            'action': action,
                            'actionFromContrat': component.get('v.actionFromContrat')
                        },
                            function (modalComponent, status, errorMessage) {
                                if (status === "SUCCESS") {
                                    //Appending the newly created component in div
                                    var body = component.find('showInputForm').get("v.body");
                                    body.push(modalComponent);
                                    component.find('showInputForm').set("v.body", body);
                                } else if (status === "INCOMPLETE") {
                                    console.log('Server issue or client is offline.');
                                } else if (status === "ERROR") {
                                    console.log('error');
                                }
                            }
                        );
                    }

                } else {
                    helper.toastdMessage("Augmentation par pourcentage", "Aucun avenant rattaché au contrat.", "warning");
                }

            }
        } else {
            helper.toastdMessage("Augmentation par pourcentage", "Veuillez sélectionner au moins un contrat.", "warning");
        }

    },

    showUpdateForm: function (component, event, helper, action, headerText) {
        //;
        var selectedListCont = component.get("v.selectedListCont");
        var filiale = component.get("v.filiale");

        if (selectedListCont.length > 0) {
            var lstContractsId = [];
            selectedListCont.forEach(function (cont) {
                lstContractsId.push(cont.Id);
            });
            //Dynamic creation of lightningModalChild component and appending its markup in a div
            $A.createComponent('c:LC_InputModalForm_AugTarif', {
                'headerText': headerText,
                'lstContractsId': lstContractsId.toString(),
                'action': action,
                'filiale': filiale,
                'selectedListCont': selectedListCont,
                'actionFromContrat': component.get('v.actionFromContrat')

            },
                function (modalComponent, status, errorMessage) {
                    if (status === "SUCCESS") {
                        //Appending the newly created component in div
                        var body = component.find('showInputForm').get("v.body");
                        body.push(modalComponent);
                        component.find('showInputForm').set("v.body", body);
                    } else if (status === "INCOMPLETE") {
                        console.log('Server issue or client is offline.');
                    } else if (status === "ERROR") {
                        console.log('error');
                    }
                }
            );
        } else {
            helper.toastdMessage("Modification type convention", "Veuillez sélectionner au moins un contrat.", "warning");
        }

    },

    showAssociateForm: function (component, event, helper, action, headerText) {
        //;
        var selectedListCont = component.get("v.selectedListCont");

       

        if (selectedListCont.length > 1) {
            //check if contract are faceface
            let isFaceFace = [];
            let isExclude = false;
            let isCompleted = false;
            let isAssociated = false;
            isUnderContract = false;
            let contactName = '';
            selectedListCont.forEach(function (cont) {
                if (cont.Face_Face__c == true) {
                     console.log(cont);
                     isFaceFace.push("true");
                     contactName += cont.Name+" et "; 
                }else{
                    isFaceFace.push('false');
                }

                if (cont.TECH_Excluded__c == true) {
                    isExclude = true;
                }
                if (cont.TreatedContract__c == true){
                    isCompleted = true;
                }
                if(cont.RegroupmentIndex__c != null){
                    isAssociated = true;
                }
                if (cont.IsUnderContract__c  == true) {
                     console.log(cont);
                     isUnderContract = true;
                 }
            });
            contactName = contactName.slice(0, -3);

            if (isFaceFace.includes('true') && isFaceFace.includes('false')) {
                helper.toastdMessage("Regroupement", contactName+" est un face à face. Or, un contrat face à face ne peut être regroupé qu'avec un ou des contrat(s) face à face. ", "warning");
            } else 
            if(isUnderContract){
                helper.toastdMessage("Regroupement", "Au moins un des contrats sélectionnés est sous contrat et ce type de contrat ne peut pas être regroupé", "warning");
            }else if (isExclude) {
                helper.toastdMessage("Regroupement", "Vous avez sélectionné un contrat en exclue et ne pouvez donc pas l'augmenter. Veuillez d'abord le réinitialiser.", "warning");
            } else if(isAssociated) {
                helper.toastdMessage("Regroupement", "Vous avez sélectionné un contrat déjà regroupé. Veuillez d'abord le réinitialiser.", "warning");
            } else {
                //Dynamic creation of lightningModalChild component and appending its markup in a div
                $A.createComponent('c:LC_InputModalForm_AugTarif', {
                    'headerText': headerText,
                    'action': action,
                    'selectedListCont': selectedListCont,
                    'actionFromContrat': component.get('v.actionFromContrat')

                },
                    function (modalComponent, status, errorMessage) {
                        if (status === "SUCCESS") {
                            //Appending the newly created component in div
                            var body = component.find('showInputForm').get("v.body");
                            body.push(modalComponent);
                            component.find('showInputForm').set("v.body", body);
                        } else if (status === "INCOMPLETE") {
                            console.log('Server issue or client is offline.');
                        } else if (status === "ERROR") {
                            console.log('error');
                        }
                    }
                );
            }
        } else {
            helper.toastdMessage("Regroupement", "Veuillez sélectionner au moins deux contrats.", "warning");
        }

    },

    showValidationForm: function (component, event, helper, action, headerText) {

        var selectedListCont = component.get("v.selectedListCont");
        var lstCont = component.get("v.lstCont");
        var count = 0;
        var countTemplate = 0;
        var countContact = 0;
        var contratName;
        var filiale = component.get("v.filiale");
        var filiere = component.get("v.filiere");

        var lstStandardCont = component.get("v.lstStandardCont");

        var contactDeactivated = false;
        var contactDeactivated_ContractName;

        var lst = [];

        //Check if all amendement are ok for validation
        for (var i = 0; i < lstCont.length; i++) {
            console.log("lstCont >> ", lstCont[i]);
            //Get related avenants    
            
            if (!lstCont[i].Face_Face__c && (lstCont[i].TreatedContract__c || lstCont[i].TECH_Excluded__c) ){
                if(!lst.includes(lstCont[i])) lst.push(lstCont[i]);
                // créer une liste contenant les contrats standards (pas faf)
            }

            if (lstCont[i].ContactRelated__c != null && !lstCont[i].TECH_Excluded__c && !lstCont[i].ContactRelated__r.Actif__c && !contactDeactivated) {
                contactDeactivated = true;
                contactDeactivated_ContractName = lstCont[i].Name;
            }

            console.log('lst => ', lst);

            if (lstCont[i] && lstCont[i].Avenants__r) {
                for (var k = 0; k < lstCont[i].Avenants__r.length; k++) {
                    console.log(' IsUnderContract ==§§ '+lstCont[i].IsUnderContract__c);
                    console.log(' Name ==§§ '+lstCont[i].Name);
                    console.log("lstCont.Avenants__r.Nature__c >> ", lstCont[i].Avenants__r[k].Nature__c);
                    if (lstCont[i].Type_de_convention__c == null && ((!lstCont[i].Face_Face__c &&  !lstCont[i].TECH_Excluded__c)) && !lstCont[i].IsUnderContract__c) {
                        countTemplate++;
                        contratName = lstCont[i].Name;
                        // Contrat marqué ni faf ni exclu ni sous contrat ou ayant pas de type de convention 
                    } else if(!lstCont[i].Face_Face__c &&  !lstCont[i].Avenants__r[k].CompletedAmadement__c) {
                        count++;
                    } else if (lstCont[i].ContactRelated__c == null && !lstCont[i].TECH_Excluded__c && !lstCont[i].IsUnderContract__c) {
                        countContact++;
                        // Contrat pas marqué exclu ou n'ayant pas de contact relié
                    }
                    
                }

            }
        }

        console.log('lst.length', lst.length);

        if(lst.length < 1) {
            helper.toastdMessage("Soumettre pour validation", "Il n'y a actuellemment aucun contrat standard disponible pour validation", "warning");
            return;
        }

        var currentUser = component.get("v.currentUser");
        console.log('currentUser'+currentUser.Profile.Name);
        if (currentUser) {
            if (currentUser.Profile.Name.includes('Assistante')) {
                helper.toastdMessage("Soumettre pour validation", "Vous ne pouvez pas soumettre de contrat pour approbation.", "warning");
                return;
            }
        }

        if (count > 0) { // Nombre d'Avenants à traiter restant
            helper.toastdMessage("Soumettre pour validation", "Vous avez toujours " + count + " avenants non traités. Veuillez les traiter avant de soumettre pour approbation.", "warning");
        } else if (countTemplate > 0) {
            helper.toastdMessage("Soumettre pour validation", "Le modèle de convention n\'est pas choisi pour le Contrat " + contratName, "warning");
        } else if (countContact > 0) {
            helper.toastdMessage("Soumettre pour validation", "Veuillez choisir un contact associé à chaque contrat.", "warning");
        } else if (contactDeactivated) {
            helper.toastdMessage("Soumettre pour validation", "Contact inactif sur le contrat suivant : "+contactDeactivated_ContractName, "warning");
        } else {
            //Dynamic creation of lightningModalChild component and appending its markup in a div                    
            $A.createComponent('c:LC_InputModalForm_AugTarif', {
                'headerText': headerText,
                'action': action,
                'selectedListCont': lst, // soumettre les contrats standards
                'filiale': filiale,
                'filiere': filiere,
                'actionFromContrat': component.get('v.actionFromContrat')
            },
                function (modalComponent, status, errorMessage) {
                    if (status === "SUCCESS") {
                        //Appending the newly created component in div
                        var body = component.find('showInputForm').get("v.body");
                        body.push(modalComponent);
                        component.find('showInputForm').set("v.body", body);
                    } else if (status === "INCOMPLETE") {
                        console.log('Server issue or client is offline.');
                    } else if (status === "ERROR") {
                        console.log('error');
                    }
                }
            );

        }

    },

    showValidationFaFForm: function (component, event, helper, action, headerText){

        try{

            var selectedListCont = component.get("v.selectedListCont");
            var lstCont = component.get("v.lstCont");
            var count = 0;
            var countTemplate = 0;
            var countContact = 0;
            var contratName;
            var filiale = component.get("v.filiale");
            var filiere = component.get("v.filiere");

            var lstStandardCont = component.get("v.lstStandardCont");
            var lstFaFCont = component.get("v.lstFaFCont");

            var standardValide = true;
            
            var lst = [];
            
            console.log("lstConts >> ", lstCont);

            var contractsByAccount = {};
            var allContractsByAccount = {};

            var contactDeactivated = false;
            var contactDeactivated_ContractName;

            var noFaf = true;
            var notTreated = true;

            console.log('lstCont : ' + JSON.stringify(lstCont));

            for (var i = 0; i < lstCont.length; i++) {
                if(allContractsByAccount[lstCont[i].Account__c] != undefined) {
                    allContractsByAccount[lstCont[i].Account__c].push(lstCont[i]);
                } else {
                    allContractsByAccount[lstCont[i].Account__c] = [lstCont[i]];
                }
            }
            //Check if all amendement are ok for validation

            if (selectedListCont != undefined && selectedListCont.length > 0 ) {

                for (var i = 0; i < selectedListCont.length; i++) {

                    if(contractsByAccount[selectedListCont[i].Account__c] != undefined) {
                        contractsByAccount[selectedListCont[i].Account__c].push(selectedListCont[i]);
                    } else {
                        contractsByAccount[selectedListCont[i].Account__c] = [selectedListCont[i]];
                    }

                    console.log("selectedListCont >> ", selectedListCont[i].Face_Face__c);
                    //Get related avenants                            
                    if (selectedListCont[i] && selectedListCont[i].Avenants__r) {

                        if (selectedListCont[i].TreatedContract__c && selectedListCont[i].ContactRelated__c != null && !selectedListCont[i].TECH_Excluded__c && !selectedListCont[i].ContactRelated__r.Actif__c && !contactDeactivated) {
                            contactDeactivated = true;
                            contactDeactivated_ContractName = selectedListCont[i].Name;
                        }
                        
                        for (var k = 0; k < selectedListCont[i].Avenants__r.length; k++) {
                            console.log("selectedListCont.Avenants__r.Nature__c >> ", selectedListCont[i].Avenants__r[k].Nature__c);
                            if(!selectedListCont[i].Face_Face__c) standardValide = false;

                            if (selectedListCont[i].Type_de_convention__c == null && (!selectedListCont[i].Face_Face__c &&  !selectedListCont[i].TECH_Excluded__c)) {
                                countTemplate++;
                                contratName = lstCont[i].Name;
                                // Contrat marqué ni faf ni exclu ou ayant pas de type de convention 
                            } else if (selectedListCont[i].ContactRelated__c == null && !selectedListCont[i].TECH_Excluded__c) {
                                countContact++;
                                // Contrat pas marqué exclu ou n'ayant pas de contact relié
                            }
                        }

                    }

                    if (selectedListCont[i].Face_Face__c && selectedListCont[i].TreatedContract__c) {
                        allContractsByAccount[selectedListCont[i].Account__c].forEach(c => {
                            if(!lst.includes(c)) lst.push(c);
                        })

                    }
                }

            } else {
                for (var i = 0; i < lstCont.length; i++) {

                    /* if(contractsByAccount[lstCont[i].Account__c] != undefined) {
                        contractsByAccount[lstCont[i].Account__c].push(lstCont[i]);
                    } else {
                        contractsByAccount[lstCont[i].Account__c] = [lstCont[i]];
                    } */

                    /* if(allContractsByAccount[lstCont[i].Account__c] != undefined) {
                        allContractsByAccount[lstCont[i].Account__c].push(lstCont[i]);
                    } else {
                        allContractsByAccount[lstCont[i].Account__c] = [lstCont[i]];
                    } */

                    console.log("lstCont >> ", lstCont[i].Face_Face__c);
                    //Get related avenants                            
                    if (lstCont[i] && lstCont[i].Avenants__r) {

                        if (lstCont[i].TreatedContract__c && lstCont[i].ContactRelated__c != null && !lstCont[i].TECH_Excluded__c && !lstCont[i].ContactRelated__r.Actif__c && !contactDeactivated) {
                            contactDeactivated = true;
                            contactDeactivated_ContractName = lstCont[i].Name;
                        }
                        
                        for (var k = 0; k < lstCont[i].Avenants__r.length; k++) {
                            console.log("lstCont.Avenants__r.Nature__c >> ", lstCont[i].Avenants__r[k].Nature__c);
                            if(!lstCont[i].Face_Face__c) standardValide = false;

                            if (lstCont[i].Type_de_convention__c == null && (!lstCont[i].Face_Face__c &&  !lstCont[i].TECH_Excluded__c)) {
                                countTemplate++;
                                contratName = lstCont[i].Name;
                                // Contrat marqué ni faf ni exclu ou ayant pas de type de convention 
                            } else if (lstCont[i].ContactRelated__c == null && !lstCont[i].TECH_Excluded__c) {
                                countContact++;
                                // Contrat pas marqué exclu ou n'ayant pas de contact relié
                            }
                        }

                    }

                    /* if (lstCont[i].Face_Face__c && lstCont[i].TreatedContract__c) {
                        if(!lst.includes(lstCont[i])) lst.push(lstCont[i]);
                    } */
                }

            }

            console.log('allContractsByAccount.length : ' + Object.keys(allContractsByAccount).length);
            console.log('selectedListCont == undefined : ' + selectedListCont);
            console.log('selectedListCont.length == 0 : ' + selectedListCont);
            //if (allContractsByAccount.length > 0 && (selectedListCont == undefined || selectedListCont.length == 0)) {
            if (Object.keys(allContractsByAccount).length > 0 && (selectedListCont == undefined || selectedListCont.length == 0)) {
                console.log('test sans selection');

                for(let key in allContractsByAccount) {
                
                    var traite = [];
                    var nonTraite = [];
                    var accountName = allContractsByAccount[key][0].Account__r.Name;

                    allContractsByAccount[key].forEach(c => {
                        if(!c.TreatedContract__c) {
                            nonTraite.push(c);
                        } else {
                            traite.push(c);
                        }
                    })

                    if (nonTraite.length == 0 && traite.length > 0) {
                        //contractsByAccount.push(allContractsByAccount[key]);
                        contractsByAccount[key] = allContractsByAccount[key];
                        allContractsByAccount[key].forEach(c => {
                            lst.push(c);
                        })
                    }


                }
            }

            var currentUser = component.get("v.currentUser");
            console.log('currentUser'+currentUser.Profile.Name);
            if (currentUser) {
                if (currentUser.Profile.Name.includes('Assistante')) {
                    helper.toastdMessage("Soumettre pour validation", "Vous ne pouvez pas soumettre de contrat pour approbation.", "warning");
                    return;
                }
            }

            var currentUser = component.get("v.currentUser");
            console.log('currentUser'+currentUser.Profile.Name);
            if (currentUser) {
                if (currentUser.Profile.Name.includes('Assistante')) {
                    helper.toastdMessage("Soumettre pour validation", "Vous ne pouvez pas soumettre de contrat pour approbation.", "warning");
                    return;
                }
            }

            console.log("contractsByAccount >> ", contractsByAccount);
              if(!standardValide) {
                helper.toastdMessage("Soumettre pour validation", "Veuillez valider les contrats standards avant de valider les contrats face à face.", "warning");             
                return;
            }


            if (lst.length == 0 || contractsByAccount.length == 0) { // Pas de faf a soumettre
                helper.toastdMessage("Soumettre pour validation", "Il n'y a aucun contrat FaF disponible pour validation", "info");
                return;
            }
            
            
            //A VERIFIER AU RETOUR D'AMINE :
            console.log('ici contractsByAccount : ' + JSON.stringify(contractsByAccount));
            console.log('ici allcontractsByAccount : ' + JSON.stringify(contractsByAccount));
            for(let key in contractsByAccount) {
                var traite = [];
                var nonTraite = [];
                var accountName = contractsByAccount[key][0].Account__r.Name;

                contractsByAccount[key].forEach(c => {
                    for (let keyAll in allContractsByAccount) {
                        allContractsByAccount[keyAll].forEach(ct => {
                            if (ct == c) {
                                allContractsByAccount[keyAll].forEach(sct => {
                                    if(!sct.TreatedContract__c) {
                                        nonTraite.push(sct);
                                        console.log('non traite');
                                    } else {
                                        traite.push(sct);
                                        console.log('traité');
                                    }
                                })
                            }
                        })
                    }
                    /* if(!c.TreatedContract__c) {
                    nonTraite.push(c);
                } else {
                    traite.push(c);
            	} */
                })            

                if(traite.length > 0 && nonTraite.length > 0) {
                    helper.toastdMessage("Soumettre pour validation", "Un compte ne peut pas être soumis partiellement. Veuillez traiter tous les contrats de "+accountName, "warning");
                    return;
                }

            }

            if (countTemplate > 0) {
                helper.toastdMessage("Soumettre pour validation", "Le modèle de convention n\'est pas choisi pour le Contrat " + contratName, "warning");
            } else if (countContact > 0) {
                helper.toastdMessage("Soumettre pour validation", "Veuillez choisir un contact associé à chaque contrat.", "warning");
            } else if (contactDeactivated) {
                helper.toastdMessage("Soumettre pour validation", "Contact inactif sur le contrat suivant : "+contactDeactivated_ContractName, "warning");
            } else {

                //Dynamic creation of lightningModalChild component and appending its markup in a div                    
                $A.createComponent('c:LC_InputModalForm_AugTarif', {
                    'headerText': headerText,
                    'action': action,
                    'selectedListCont': lst, // soumettre les contrats standards
                    'filiale': filiale,
                    'filiere': filiere,
                    'actionFromContrat': component.get('v.actionFromContrat')
                },
                    function (modalComponent, status, errorMessage) {
                        if (status === "SUCCESS") {
                            //Appending the newly created component in div
                            var body = component.find('showInputForm').get("v.body");
                            body.push(modalComponent);
                            component.find('showInputForm').set("v.body", body);
                        } else if (status === "INCOMPLETE") {
                            console.log('Server issue or client is offline.');
                        } else if (status === "ERROR") {
                            console.log('error');
                        }
                    }
                );

            }
        } catch(e) {
            console.log(e);
        }

    },

    showFaceToFaceForm: function (component, event, helper, action, headerText) {

        var selectedListAvenant = component.get("v.lstFaFCont");
        var selectedListCont = component.get("v.selectedListCont");
        var listCont  =  component.get("v.lstCont");

        for (var k=0; k < selectedListCont.length; k++) {
            if (selectedListCont[k].PriceNextyearFromERP__c) {
                helper.toastdMessage("Augmenter en face à face", "Vous ne pouvez pas augmenter en face à face un contrat dont le tarif provient de l'ERP.", "warning");
                return;
                }
        } 
    
        let isExclude = false;
        let isCompleted = false;
        let isAssociated = false;
        let associatContract ='';
        let tab = [] ;
        selectedListCont.forEach(function (cont) {
            tab.push(cont.Id);
        });

        selectedListCont.forEach(function (cont) {
           
            if (cont.TECH_Excluded__c == true) {
                isExclude = true;
            }
            if (cont.TreatedContract__c == true) {
                isCompleted = true;
            }

            if(cont.RegroupmentIndex__c != null){
                
               
                listCont.forEach(function (lstCont) {

                    if(lstCont.RegroupmentIndex__c && lstCont.RegroupmentIndex__c == cont.RegroupmentIndex__c && lstCont.Id != cont.Id  && !tab.includes(lstCont.Id)){
                        associatContract += lstCont.Name+" et " ;
                        isAssociated = true;
                    }
                });
                associatContract = associatContract.slice(0, -3);
            }
        });
        console.log("tab>>>>>>"+tab);
    
        if (isExclude) {
            helper.toastdMessage("Augmenter en face à face", "Vous avez sélectionné un contrat exclu et ne pouvez donc pas l'augmenter. Veuillez d'abord le réinitialiser.", "warning");
        } else if(isCompleted) {
            helper.toastdMessage("Augmenter en face à face", "Vous avez sélectionné un contrat déjà traité. Veuillez d'abord le réinitialiser.", "warning");
        }  else if(isAssociated){
            helper.toastdMessage("Augmenter en face à face", "Ce contrat est regroupé avec "+associatContract+".  Or, un contrat face à face ne peut être regroupé qu'avec un ou des contrat(s) face à face.", "warning");

        }else if (selectedListCont.length > 0) {
            //Dynamic creation of lightningModalChild component and appending its markup in a div
            $A.createComponent('c:LC_InputModalForm_AugTarif', {
                'headerText': headerText,
                'action': action,
                'selectedListCont': selectedListCont,
                'selectedListAvenant': selectedListAvenant,
                'actionFromContrat': component.get('v.actionFromContrat')
            },
                function (modalComponent, status, errorMessage) {
                    if (status === "SUCCESS") {
                        //Appending the newly created component in div
                        var body = component.find('showInputForm').get("v.body");
                        body.push(modalComponent);
                        component.find('showInputForm').set("v.body", body);
                    } else if (status === "INCOMPLETE") {
                        console.log('Server issue or client is offline.');
                    } else if (status === "ERROR") {
                        console.log('error');
                    }
                }
            );
        } else {
            helper.toastdMessage("Augmenter en face à face", "Veuillez sélectionner au moins un contrat.", "warning");
        }

    }, 

    showUnderContractForm: function (component, event, helper, action, headerText) {
        var selectedListAvenant = component.get("v.selectedListAvenant");
        var selectedListCont = component.get("v.selectedListCont");
        var listCont  =  component.get("v.lstCont");

        let isExclude = false;
        let isCompleted = false;
        let isAssociated = false;
        let associatContract ='';
        selectedListCont.forEach(function (cont) {
            if (cont.TECH_Excluded__c == true) {
                isExclude = true;
            }
            if (cont.TreatedContract__c == true) {
                isCompleted = true;
            }
            if(cont.RegroupmentIndex__c != null){
                isAssociated = true;
                listCont.forEach(function (lstCont) {

                    if(lstCont.RegroupmentIndex__c && lstCont.RegroupmentIndex__c == cont.RegroupmentIndex__c && lstCont.Id != cont.Id ){
                        associatContract += lstCont.Name+" et " ;
                    }
                });
                associatContract = associatContract.slice(0, -3);
            }
        });
         if(isCompleted) {
            helper.toastdMessage("Sous contrat", "Vous avez sélectionné un contrat déjà traité. Veuillez d'abord le réinitialiser.", "warning");
        } else if(isAssociated){
            helper.toastdMessage("Sous contrat", "Vous ne pouvez pas qualifier de sous contrat un ou des contrat(s) regroupé(s).", "warning");
        }
         else if (selectedListCont.length > 0) {
            //Dynamic creation of lightningModalChild component and appending its markup in a div
            $A.createComponent('c:LC_InputModalForm_AugTarif', {
                'headerText': headerText,
                'action': action,
                'selectedListCont': selectedListCont,
                'selectedListAvenant': selectedListAvenant,
                'actionFromContrat': component.get('v.actionFromContrat')
            },
                function (modalComponent, status, errorMessage) {
                    if (status === "SUCCESS") {
                        //Appending the newly created component in div
                        var body = component.find('showInputForm').get("v.body");
                        body.push(modalComponent);
                        component.find('showInputForm').set("v.body", body);
                    } else if (status === "INCOMPLETE") {
                        console.log('Server issue or client is offline.');
                    } else if (status === "ERROR") {
                        console.log('error');
                    }
                }
            );
        } else {
            helper.toastdMessage("Sous contrat", "Veuillez sélectionner au moins un contrat.", "warning");
        }

    }, 

    showFixedPriceForm: function (component, event, helper, action, headerText) {
        // ;
        var selectedListAvenant = component.get("v.selectedListAvenant");
        var selectedListCont = component.get("v.selectedListCont");
        var actionFromContrat = component.get("v.actionFromContrat");

        for (var k=0; k < selectedListCont.length; k++) {
            if (selectedListCont[k].PriceNextyearFromERP__c) {
                helper.toastdMessage("Augmenter prix fixe", "Vous ne pouvez pas augmenter un contrat dont le tarif provient de l'ERP.", "warning");
                return;
                }
        } 

        if (selectedListAvenant.length > 0) {
            let isExclude = false;
            let isCompleted = false;
            selectedListCont.forEach(function (cont) {
                if (cont.TECH_Excluded__c == true) {
                    isExclude = true;
                }
                if (cont.TreatedContract__c == true){
                    isCompleted = true;
                }
            });
            if (isExclude) {
                helper.toastdMessage("Augmenter prix fixe", "Vous avez sélectionné un contrat exclu et ne pouvez donc pas l'augmenter. Veuillez d'abord le réinitialiser.", "warning");
            } else if (isCompleted) {
                helper.toastdMessage("Augmenter prix fixe", "Vous avez sélectionné un contrat déjà traité. Veuillez d'abord le réinitialiser.", "warning");
            } else {
                //Dynamic creation of lightningModalChild component and appending its markup in a div
                $A.createComponent('c:LC_InputModalForm_AugTarif', {
                    'headerText': headerText,
                    'action': action,
                    'selectedListAvenant': selectedListAvenant,
                    'actionFromContrat': component.get('v.actionFromContrat')
                },
                    function (modalComponent, status, errorMessage) {
                        if (status === "SUCCESS") {
                            //Appending the newly created component in div
                            var body = component.find('showInputForm').get("v.body");
                            body.push(modalComponent);
                            component.find('showInputForm').set("v.body", body);
                        } else if (status === "INCOMPLETE") {
                            console.log('Server issue or client is offline.');
                        } else if (status === "ERROR") {
                            console.log('error');
                        }
                    }
                );
            }

        } else {
            helper.toastdMessage("Augmenter prix fixe", "Veuillez sélectionner au moins un contrat.", "warning");
        }

    },

    showNoIncreaseForm: function (component, event, helper, action, headerText) {
        // ;
        var selectedListAvenant = component.get("v.selectedListAvenant");
        var selectedListCont = component.get("v.selectedListCont");
        var actionFromContrat = component.get("v.actionFromContrat");

        for (var k=0; k < selectedListCont.length; k++) {
            if (selectedListCont[k].PriceNextyearFromERP__c) {
                helper.toastdMessage("Ne pas augmenter", "Vous ne pouvez pas augmenter un contrat dont le tarif provient de l'ERP.", "warning");
                return;
                }
        } 

        if (selectedListAvenant.length > 0) {
            let isExclude = false;
            let isCompleted = false;
            selectedListCont.forEach(function (cont) {
                if (cont.TECH_Excluded__c == true) {
                    isExclude = true;
                }
                if (cont.TreatedContract__c == true){
                    isCompleted = true;
                }
            });
            if (isExclude) {
                helper.toastdMessage("Ne pas augmenter", "Vous avez sélectionné un contrat exclu et ne pouvez donc pas l'augmenter. Veuillez d'abord le réinitialiser.", "warning");
            } else if (isCompleted) {
                helper.toastdMessage("Ne pas augmenter", "Vous avez sélectionné un contrat déjà traité. Veuillez d'abord le réinitialiser.", "warning");
            } else {
                //Dynamic creation of lightningModalChild component and appending its markup in a div
                $A.createComponent('c:LC_InputModalForm_AugTarif', {
                    'headerText': headerText,
                    'action': action,
                    'selectedListAvenant': selectedListAvenant,
                    'actionFromContrat': component.get('v.actionFromContrat')

                },
                    function (modalComponent, status, errorMessage) {
                        if (status === "SUCCESS") {
                            //Appending the newly created component in div
                            var body = component.find('showInputForm').get("v.body");
                            body.push(modalComponent);
                            component.find('showInputForm').set("v.body", body);
                        } else if (status === "INCOMPLETE") {
                            console.log('Server issue or client is offline.');
                        } else if (status === "ERROR") {
                            console.log('error');
                        }
                    }
                );
            }
        } else {
            helper.toastdMessage("Ne pas augmenter", "Veuillez sélectionner au moins un contrat.", "warning");
        }

    },

    showResetForm: function (component, event, helper, action, headerText) {
        var selectedListCont = component.get("v.selectedListCont");
        var selectedListAvenant = component.get("v.selectedListAvenant");
        var lstCont = component.get("v.lstCont");
        const filiale = component.get("v.filiale");
        const filiere = component.get("v.filiere");
        //let AreStandardContractSubmitted = false;

        for (var k=0; k < selectedListCont.length; k++) {
            if (selectedListCont[k].PriceNextyearFromERP__c) {
                helper.toastdMessage("Réinitaliser", "Vous ne pouvez pas réinitialiser un contrat dont le tarif provient de l'ERP.", "warning");
                return;
                }
        } 

        let StandardContractSubmitted = component.get('c.AreStandardContractsSubmitted');
        StandardContractSubmitted.setParams({
                    filiale: filiale,
                    filiere: filiere,
                });
        

        StandardContractSubmitted.setCallback(this, function(response) {

            var AreStandardContractSubmitted = response.getReturnValue();

            console.log('StandardContractSubmitted value : ' + AreStandardContractSubmitted);

            if (AreStandardContractSubmitted == true) {
                for (var k=0; k < selectedListCont.length; k++) {
                    if (selectedListCont[k].Face_Face__c) {
                        helper.toastdMessage("Réinitaliser", "Vous ne pouvez pas réinitialiser un contrat face à face après avoir soumis pour approbation les contrats standards.", "warning");
                        return;
                        }
                    }        
            }

            if (selectedListCont.length > 0) {
                $A.createComponent('c:LC_InputModalForm_AugTarif', {
                    'headerText': headerText,
                    'action': action,
                    'selectedListCont': selectedListCont,
                    'selectedListAvenant': selectedListAvenant,
                    'endMessage': 'contrats',
                    'actionFromContrat': component.get('v.actionFromContrat')
                },
                    function (modalComponent, status, errorMessage) {
                        if (status === "SUCCESS") {
                            //Appending the newly created component in div
                            var body = component.find('showInputForm').get("v.body");
                            body.push(modalComponent);
                            component.find('showInputForm').set("v.body", body);
                        } else if (status === "INCOMPLETE") {
                            console.log('Server issue or client is offline.');
                        } else if (status === "ERROR") {
                            console.log('error');
                        }
                    }
                );
            } else {
                helper.toastdMessage("Réinitaliser", "Veuillez sélectionner au moins un contrat.", "warning");
            }
            
        });
        $A.enqueueAction(StandardContractSubmitted);

    },

    showExcludeForm: function (component, event, helper, action, headerText) {
        // ;
        var selectedListCont = component.get("v.selectedListCont");
        var selectedListAvenant = component.get("v.selectedListAvenant");

        if (selectedListCont.length > 0) {

            let isCompleted = false;

            selectedListCont.forEach(function (cont) {
                if (cont.TreatedContract__c == true && !cont.Face_Face__c){
                    isCompleted = true;
                }
            });

            if(isCompleted) {
                helper.toastdMessage("Exclusion", "Vous avez sélectionné un contrat traité et hors FaF. Veuillez d'abord le réinitialiser.", "warning");
            } else {
                $A.createComponent('c:LC_InputModalForm_AugTarif', {
                    'headerText': headerText,
                    'action': action,
                    'selectedListCont': selectedListCont,
                    'selectedListAvenant': selectedListAvenant,
                    'actionFromContrat': component.get('v.actionFromContrat')
                },
                    function (modalComponent, status, errorMessage) {
                        if (status === "SUCCESS") {
                            //Appending the newly created component in div
                            var body = component.find('showInputForm').get("v.body");
                            body.push(modalComponent);
                            component.find('showInputForm').set("v.body", body);
                        } else if (status === "INCOMPLETE") {
                            console.log('Server issue or client is offline.');
                        } else if (status === "ERROR") {
                            console.log('error');
                        }
                    }
                );
            }
        } else {
            helper.toastdMessage("Exclure", "Veuillez sélectionner au moins un contrat.", "warning");
        }

    },

    handleDisplayOptionsEvent: function (component, event) {
        var displayOption = event.getParam("displayOption");       
        component.set("v.displayOption", displayOption);
        localStorage.clear(); //reset status 
        console.log('ContractList.displayOption>> ', component.get("v.displayOption"));
      

    },

    showUnderContract: function (component, event, helper, action, headerText) {

        var selectedListAvenant = component.get("v.lstFaFCont");
        var selectedListCont = component.get("v.selectedListCont");

        let isExclude = false;
        let isCompleted = false;
        selectedListCont.forEach(function (cont) {
            if (cont.TECH_Excluded__c == true) {
                isExclude = true;
            }
            if (cont.TreatedContract__c == true) {
                isCompleted = true;
            }
        });
        if (isExclude) {
            helper.toastdMessage("Sous contrat", "Vous avez sélectionné un contrat exclu et ne pouvez donc pas l'augmenter. Veuillez d'abord le réinitialiser.", "warning");
        } else if(isCompleted) {
            helper.toastdMessage("Sous contrat", "Vous avez sélectionné un contrat déjà traité. Veuillez d'abord le réinitialiser.", "warning");
        } else if (selectedListCont.length > 0) {
            //Dynamic creation of lightningModalChild component and appending its markup in a div
            $A.createComponent('c:LC_InputModalForm_AugTarif', {
                'headerText': headerText,
                'action': action,
                'selectedListCont': selectedListCont,
                'selectedListAvenant': selectedListAvenant,
                'actionFromContrat': component.get('v.actionFromContrat')
            },
                function (modalComponent, status, errorMessage) {
                    if (status === "SUCCESS") {
                        //Appending the newly created component in div
                        var body = component.find('showInputForm').get("v.body");
                        body.push(modalComponent);
                        component.find('showInputForm').set("v.body", body);
                    } else if (status === "INCOMPLETE") {
                        console.log('Server issue or client is offline.');
                    } else if (status === "ERROR") {
                        console.log('error');
                    }
                }
            );
        } else {
            helper.toastdMessage("Sous contrat", "Veuillez sélectionner au moins un contrat.", "warning");
        }

        /* var action = component.get("c.setAmendmentsUnderContract");
        action.setParams({
            listAmendments: component.get("v.selectedListAvenant"),
            enableUnderContract: true
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == 'SUCCESS') {
                helper.setRowBackgroundColor('IsUnderContract', component.get("v.selectedRows"))
                helper.updateSelectedAvenant(component, event, helper)
                helper.refreshContracts(component, event, helper)
            }
        });
        $A.enqueueAction(action); */

    }, 

});