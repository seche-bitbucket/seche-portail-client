({
    scriptsLoaded : function(component, event, helper) {
        console.log('LC_ContractsList_AugTarif---> Start Script loaded..'); 
                    
    },

    // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },
    
    doInit : function(component,event,helper){   
        console.log('LC_ContractsList_AugTarif---> doInit..');    
        console.log('LC_ContractsList_AugTarif.filiale>> ', component.get("v.filiale"));
        console.log('MainComponent.filiere >> ', component.get("v.filiere"));
        console.log('LC_ContractsList_AugTarif.isShowContent >> ', component.get("v.isShowContent"))     
        helper.getContracts(component,event,helper);
    },

    doInitRefresh : function(component,event,helper){  
        // 
        console.log('LC_ContractsList_AugTarif---> doInitRefresh..');    
        console.log('LC_ContractsList_AugTarif.filiale>> ', component.get("v.filiale"));
        console.log('MainComponent.filiere >> ', component.get("v.filiere"));
        console.log('LC_ContractsList_AugTarif.isShowContent >> ', component.get("v.isShowContent"));         
        var isAvenantUpdate = event.getParam("isAvenantUpdate");
        var isCancel = event.getParam("isCancel");
        var actionFromContrat = event.getParam("actionFromContrat");
        component.set("v.keepState", true);
        if(!isCancel){ 
            var selectedData = component.get("v.selectedListCont")
            
            helper.getContracts(component,event,helper).then(() => {
                var newestData = []
                { 
                    var table = $("#tableId").DataTable()
                    table.rows().every(
                        function() {
                            var data = this.data();
                            console.log('Amine Data', data)
                            selectedData.forEach(sD => {
                                console.log('Amine sD', sD)
                                if(sD.Id == data.Id) {
                                    newestData.push(data)
                                    var node = this.node()
                                    console.log('Amine Id Equals')
                                    if(!actionFromContrat) {
                                        $(node).find('input[type="checkbox"]').prop('checked', true) 
                                        $(node).addClass("selected")
                                    } else {
                                        newestData = []
                                        $(node).find('input[type="checkbox"]').prop('checked', false) 
                                        $(node).removeClass("selected")
                                    }
                                }
                            })
                        }
                        )
                        console.log('executed Amine')      
                        component.set("v.selectedListCont", [])
                        $('#selectedRecordsNumb').text( newestData.length +' ligne(s) séléctionnée(s)');
                        if(!actionFromContrat) {
                            component.set("v.selectedListCont", newestData)
                        }              
                    }
                })
            }
    },

    
    initState : function(component, event, helper) {
        console.log('ContratListComponent.getHeaderEvent>>>>>>>>>>>Start ');
        helper.initState(component, event, helper) ;
    },

    fireEvent : function(component, event, helper) {
        helper.fireTheContratListEvent(component,event,helper);
    },

    getHeaderPicklistEvent : function(component, event, helper) {
        console.log('ContratListComponent.getHeaderEvent>>>>>>>>>>>Start ');
        helper.handleHeaderPickListEvent(component, event, helper) ;
    },

    toggleVisibility : function(component, event, helper) {
        var ddDiv = component.find('ddId');
        $A.util.toggleClass(ddDiv,'slds-is-open');
    },
        
    handlefireEvent : function(component, event, helper) {
        helper.fireEvent(component,event,helper);
    },

    onSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The record has been Saved successfully."
        });
        toastEvent.fire();
    },

    updateContract : function(component, event, helper) {            
        helper.updateContract(component, event, helper);
    },

    validation : function(component, event, helper) {
        var action ="validation";
        var headerText ="Soumettre pour validation"
        helper.showValidationForm(component, event, helper,action,headerText);
    },

    validateFaF : function(component, event, helper) {
        var action = "validation";
        var headerText = "Soumettre pour validation face à face";
        helper.showValidationFaFForm(component, event, helper, action, headerText);
    },

    increasePercent : function(component, event, helper) {
        var action ="increasePercent";
        var headerText ="Augmentation par pourcentage"
        helper.showIncreaseForm(component, event, helper,action,headerText);
    },

    incfixedPrice : function(component, event, helper) {
        var action ="incfixedPrice";
        var headerText ="Augmentation en prix fixe";
        helper.showFixedPriceForm(component, event, helper, action, headerText);
    },

    fixedPrice : function(component, event, helper) {
        var action ="fixedPrice";
        var headerText ="Prix fixe";                    
        helper.showFixedPriceForm(component, event, helper, action, headerText);
    },

    noIncrease : function(component, event, helper) {
        var action ="noIncrease";
        var headerText ="Ne pas augmenter";
        helper.showNoIncreaseForm(component, event, helper, action, headerText);
    },

    reset : function(component, event, helper) {
        var action ="reset";
        var headerText ="Réinitialiser";                    
        helper.showResetForm(component, event, helper, action, headerText);
    },

    exclude : function(component, event, helper) {
        var action ="exclude";
        var headerText ="Exclusion";                    
        helper.showExcludeForm(component, event, helper, action, headerText);
    },

    validation : function(component, event, helper) {
        var action ="validation";
        var headerText ="Soumettre pour validation";
        helper.showValidationForm(component, event, helper,action,headerText);
    },

    validationFaF: function(component, event , helper) {
        //component.set("v.openModal", true);
    },

    cancelFaF: function(component, event, helper) {
        component.set("v.openModal", false);
    },
    
    increaseFaceToFace: function (component, event, helper) {
        var action = "increaseFaceToFace";
        var headerText = "Augmentation en face à face";
        helper.showFaceToFaceForm(component, event, helper, action, headerText);
    },

    
    associate : function(component, event, helper) {
        var action ="associate";
        var headerText ="Regrouper les contrats"
        helper.showAssociateForm(component, event, helper,action,headerText);
    },


    update : function(component, event, helper) {
        var action ="update";
        var headerText ="Modification type de convention"
        helper.showUpdateForm(component, event, helper,action,headerText);
    },


    showEditModal : function(component, event, helper) {
        //Dynamic creation of lightningModalChild component and appending its markup in a div
        helper.openEditModal(component, event, helper);
    },

    getDisplayOptions : function(component, event, helper) {
        helper.handleDisplayOptionsEvent(component, event) ;
    },

    setUnderContract: function (component, event, helper) {
        var action = "setUnderContract";
        var headerText = "Sous Contrat";
        helper.showUnderContractForm(component, event, helper, action, headerText);
    } 
        
    /*handleSelect: function (component , event, helper) {
        // This will contain the string of the "value" attribute of the selected
        // lightning:menuItem
        var selectedMenuItemValue = event.getParam("value");
        // alert("Menu item selected with value: " + selectedMenuItemValue);
        if("increasePercent"===selectedMenuItemValue){
            var action ="increasePercent";
            var headerText ="Augmentation par pourcentage";
            helper.showIncreaseForm(component, event, helper,action,headerText);
        }
        if("increaseFaceToFace"===selectedMenuItemValue) {
            var action = "increaseFaceToFace";
            var headerText = "Augmentation en face à face";
            helper.showFaceToFaceForm(component, event, helper, action, headerText);
        }
        if("associate"===selectedMenuItemValue){ 
            var action ="associate";
            var headerText ="Regrouper les contrats";
            helper.showAssociateForm(component, event, helper,action,headerText);
        }

        if("update"===selectedMenuItemValue){ 
            var action ="update";
            var headerText ="Modification type de convention";                    
            helper.showUpdateForm(component, event, helper,action,headerText);
        }

        if("validation"===selectedMenuItemValue){
            var action ="validation";
            var headerText ="Soumettre pour validation";
            helper.showValidationForm(component, event, helper,action,headerText);
        }

        if("incfixedPrice"===selectedMenuItemValue){ 
            var action ="incfixedPrice";
            var headerText ="Augmentation en prix fixe";
            helper.showFixedPriceForm(component, event, helper, action, headerText);
        }

        if("fixedPrice"===selectedMenuItemValue){ 
            var action ="fixedPrice";
            var headerText ="Prix fixe";                    
            helper.showFixedPriceForm(component, event, helper, action, headerText);
        }

        if("noIncrease"===selectedMenuItemValue){
            var action ="noIncrease";
            var headerText ="Ne pas augmenter";
            helper.showNoIncreaseForm(component, event, helper, action, headerText);
        }
        
        if("reset"===selectedMenuItemValue){ 
            var action ="reset";
            var headerText ="Réinitialiser";                    
            helper.showResetForm(component, event, helper, action, headerText);
        }

        if("exclude"===selectedMenuItemValue){ 
            var action ="exclude";
            var headerText ="Exclusion";                    
            helper.showExcludeForm(component, event, helper, action, headerText);
        }
        // alert("Menu item selected with value: " + selectedMenuItemValue);
    }*/
       
})