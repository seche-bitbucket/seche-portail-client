({
    navigateToUrl: function( sobjectId ) {
        console.log( 'navigating to url', sobjectId );

        if ( !$A.util.isEmpty(sobjectId) ) {
            sforce.one.navigateToURL('/one/one.app#/sObject/'+sobjectId+'/view',false); 
        }

    }
})