({
    openModal : function(component, event, helper) {
        //Dynamic creation of lightningModalChild component and appending its markup in a div
		$A.createComponent( 'c:LC_ShowIModalIncators_AugTarif', {
                'headerText' : 'Liste des indicateurs '
            },
            function(modalComponent, status, errorMessage) {
                if (status === "SUCCESS") {
                    //Appending the newly created component in div
                    var body = component.find( 'showChildModal' ).get("v.body");
                    body.push(modalComponent);
                    component.find( 'showChildModal' ).set("v.body", body);
                } else if (status === "INCOMPLETE") {
                	console.log('Server issue or client is offline.');
                } else if (status === "ERROR") {
                	console.log('error');
                }
            }
        );
    },
    helperShow : function(component,event,secId) {
        var acc = component.find(secId);
              for(var cmp in acc) {
              $A.util.toggleClass(acc[cmp], 'slds-show');  
              $A.util.toggleClass(acc[cmp], 'slds-hide');  
         }
      },
      handleTheHeaderEvent : function(component, event) {
          //debugger;
        var filiale = event.getParam("filiale");
        var filiere = event.getParam("filiere");
        var natureCompte = event.getParam("natureCompte");
        var isShowContent = event.getParam("isShowContent");

        // set the handler attributes based on event data
        component.set("v.filiale", filiale);
        component.set("v.filiere", filiere);  
        component.set("v.natureCompte", natureCompte);
        component.set("v.isShowContent", isShowContent);

        console.log('LC_Header_AugTarif natureCompte >> ', natureCompte)        
        console.log('LC_Header_AugTarif.event.getParam("filiale") >> ',component.get("v.filiale"));
        console.log('LC_Header_AugTarif.event.getParam("filiere") >> ',component.get("v.filiere"));
        console.log('LC_Header_AugTarif.event.getParam("Nature du compte") >> ',component.get("v.natureCompte"));
        console.log('LC_Header_AugTarif.isShowContent >> ',component.get("v.isShowContent"));     

    },

    toastdMessage: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },

    fireChartEvent : function(component, event, helper, menuItemValue) {
        // Get the  event by using the
        // name value from aura:registerEvent
        //debugger;
        var appEvent = $A.get("e.c:LE_ChartEvt_AugTarif");       
        appEvent.setParams({
            "menuItem" : menuItemValue            
            });
            appEvent.fire();
    },


     
})