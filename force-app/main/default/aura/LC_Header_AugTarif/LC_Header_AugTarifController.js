({
  doInit: function (component, event, helper) {
    helper.doInit(component, event, helper);
  },

  openModal: function (component, event, helper) {
    //Dynamic creation of lightningModalChild component and appending its markup in a div
    helper.openModal(component, event, helper);
  },

  sectionOne: function (component, event, helper) {
    helper.helperShow(component, event, 'articleOne');
  },

  sectionTwo: function (component, event, helper) {
    helper.helperShow(component, event, 'articleTwo');
  },

  getHeaderEvent: function (component, event, helper) {
    helper.handleTheHeaderEvent(component, event, helper);
  },

  handleSelect: function (component, event, helper) {
    // This will contain the string of the "value" attribute of the selected
    // lightning:menuItem           
    var selectedMenu = event.detail.menuItem.get("v.value");
    console.log('selectedMenu-', selectedMenu);
    switch (selectedMenu) {
      case "getTauxAugmentation":
        component.set("v.incatorTitle", 'Indicateurs de simulation de l’augmentation')
        helper.fireChartEvent(component, event, helper, 'getTauxAugmentation');
        break;
      case "getAugmentationCA":
        component.set("v.incatorTitle", 'Indicateurs de simulation de l’augmentation')
        helper.fireChartEvent(component, event, helper, 'getAugmentationCA');
        break;
    }
  },

  showProgress: function (component, event, helper) {
    helper.fireChartEvent(component, event, helper, 'getAvancementAugmentation');
    // helper.toastdMessage('', 'Affichage KPI Pourcentage augmentation global sur portefeuille', 'success');
  },

  getTauxAugmentation: function (component, event, helper) {
    helper.fireChartEvent(component, event, helper, 'getTauxAugmentation');
  },

  getAugmentationCA: function (component, event, helper) {
    helper.fireChartEvent(component, event, helper, 'getAugmentationCA');
  },

  initIncators: function (component, event, helper) {
    helper.initIncators(component, event, helper);
  }

})