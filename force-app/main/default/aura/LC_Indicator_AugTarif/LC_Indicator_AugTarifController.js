({
    afterScriptsLoaded : function(component, event, helper) 
    {
        helper.getAvancementAugmentation(component,event,helper);
      
    },

    refreshData : function(component, event, helper) 
    {
        component.set("v.menuItem", "getAvancementAugmentation");
        helper.getAvancementAugmentation(component,event,helper);
      
    },
    
    handleChartEvent : function(component, event, helper) 
    {
        helper.handleChartEvent(component,event,helper);
    } 
    
})