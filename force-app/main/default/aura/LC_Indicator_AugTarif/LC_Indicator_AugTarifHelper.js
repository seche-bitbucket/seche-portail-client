({
    //Method to genarate a item Chart
    generateChart: function (component, event, helper, chartInputData, chartId, chartTitle, xAxisTilte, yAxisTilte, seriesName, chartType, chartUnite, displayTotal) {         //
        
        // var reportResultData = JSON.parse(chartInputData);

        var listSign = [];
        var chartData = [];
        var chartDataN1 = [];
        var chartLabels = [];
        
        if(chartType == 'columnStacked'){
            chartData =  helper.generateChartSlackData(chartInputData);
        } else {
            if (chartInputData.groupingsDown.groupings) {
                for (var i = 0; i < (chartInputData.groupingsDown.groupings.length); i++) {
                    //Reset sign
                    var sign = '';//use to add negatif values
                    //Iterate and prepare the list of Labels for the chart
                    var labelItem = chartInputData.groupingsDown.groupings[i].label;
                    if(chartId == 'chart11'){
                        if (labelItem == 'true' || labelItem == 'vrai') {
                            labelItem = 'Face à Face'
                        } else if (labelItem == 'false' || labelItem == 'faux') {
                            labelItem = 'Non Face à Face'
                        }
                    } else {
                        if (labelItem == 'true' || labelItem == 'vrai') {
                            labelItem = 'Traités'
                        } else if (labelItem == 'false' || labelItem == 'faux') {
                            labelItem = 'Non traités'
                        }
                    }
                    chartLabels.push(labelItem);
                    var keyTemp = chartInputData.groupingsDown.groupings[i].key;
                    //Prepeare the chart data to be plotted.
                    var valueTemp = chartInputData.factMap[keyTemp + "!T"].aggregates[0].value;
    
                    //check if value is positive or negative
                    if (Math.sign(valueTemp) == -1) {//if negative
                        sign = '-';
                        //Get abs of the value
                        valueTemp = Math.abs(valueTemp);
                    }
                    listSign.push(sign);
                    chartData.push(valueTemp);
    
                    if (chartType == 'column' && chartInputData.factMap[keyTemp + "!T"] != null) {
                        var valueTemp = chartInputData.factMap[keyTemp + "!T"].aggregates[1].value;
                        chartDataN1.push(valueTemp);
                        //chartLabels.push(labelItem);
                    }
                }
            }
        }

        //                       
        console.log('====', chartData);
        //Set params
        var chartId = chartId;
        var chartTitle = chartTitle;
        var chartSubTitle = '';
        var chartType = chartType;
        var seriesName = seriesName;
        var xAxisTilte = xAxisTilte;
        var yAxisTilte = yAxisTilte;
        //helper.donutchart(component, event, helper, chartId, chartType, chartTitle, chartSubTitle, chartData, chartLabels, xAxisTilte, yAxisTilte, seriesName);
        if (chartType == 'column') {
            helper.columnchart(component, event, helper, chartId, chartType, chartTitle, chartSubTitle, chartData, chartDataN1, chartLabels, xAxisTilte, yAxisTilte, seriesName);
        } else if (chartType == 'columnStacked') {
            helper.columnchartSlack(component, event, helper, chartId, chartType, chartTitle, chartSubTitle, chartData, chartDataN1, chartLabels, xAxisTilte, yAxisTilte, seriesName);
        }else {
            helper.piechart(component, event, helper, chartId, chartType, chartTitle, chartSubTitle, chartData, chartLabels, xAxisTilte, yAxisTilte, seriesName, chartUnite, listSign, displayTotal);
        }
    },

     //Method to genarate a SLack items for Chart column from reportData
     generateChartSlackData: function (reportData) { 
         
        var chartData = [];  

        if (reportData.groupingsDown.groupings) {
            for (var i = 0; i < (reportData.groupingsDown.groupings.length); i++) {
                var chartDataTemp = [];
                //get nature
                var natureTemp = reportData.groupingsDown.groupings[i].label;

                for(var j = 0; j < (reportData.groupingsDown.groupings[i].groupings.length); j++){  
                var currentGroupingRows =  reportData.groupingsDown.groupings[i].groupings.length;               
                //Iterate and prepare the list of Labels for the chart
                var labelItem = reportData.groupingsDown.groupings[i].groupings[j].label;
                var keyTemp = reportData.groupingsDown.groupings[i].groupings[j].key;
                 //Prepeare the chart data to be plotted.
                var valueTempN = reportData.factMap[keyTemp + "!T"].aggregates[0].value;
                var valueTempN1 = reportData.factMap[keyTemp + "!T"].aggregates[1].value;

                //get row Item grouping by hors FaF/FaF
                var rowItemTemp = {                   
                    faf: labelItem,
                    can: valueTempN,
                    can1: valueTempN1 } 
                                  
                    //
                    if(currentGroupingRows == 1) {//if we have only hors Faf/Faf
                        if(labelItem == "false" || labelItem == "faux"){//if it hors Faf initialize faf with zero (create face à face record with zero)
                            var rowItemInit = {                   
                                faf: "true",
                                can: 0,
                                can1: 0 } 
                                //insert rows (index 0 = hors face)
                                chartDataTemp.push(rowItemTemp);  
                                chartDataTemp.push(rowItemInit);
                        }else {
                            var rowItemInit = {                   
                                faf: "false",
                                can: 0,
                                can1: 0 } 
                                  //insert rows (index 0 = hors face)
                                chartDataTemp.push(rowItemInit);
                                chartDataTemp.push(rowItemTemp);  
                        }
                    } else{
                         //save item in list   
                        chartDataTemp.push(rowItemTemp); 
                    }                                 
                } 
                //create report Row Data
                var rowTemp = {
                    nature: natureTemp,
                    data: chartDataTemp} 

                chartData.push(rowTemp);             
            }
        }
        return chartData;
    },
    
    getAvancementAugmentation: function (component, event, helper) {
        var filiale = component.get("v.filiale");
        var filiere = component.get("v.filiere");
        var natureCompte = component.get("v.natureCompte");
        //Set report's API names to display
        var lstReportName = ["Avct_nbre_avenants_6SN", "Avct_nbre_contrats_RXu", "Aug_en_CA_augmente_86L"];
        //var lstReportName = ["Taux_aug_par_filiale_5zN"];
        var action = component.get("c.getReportData");
        action.setParams({
            lstReportName: lstReportName,
            filiale: filiale,
            filiere: filiere,
            natureCompte: natureCompte,
        });

        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                var reportsResultDatas = response.getReturnValue();
                console.log('reportsResultDatas ====' + reportsResultDatas);
                // 
                reportsResultDatas.forEach(function (rep) {
                    var chartInputData = JSON.parse(rep);
                    var chartId;
                    var reportDevName = chartInputData.reportMetadata.developerName;
                    var chartTitle = chartInputData.reportMetadata.name;
                    var xAxisTilte;
                    var yAxisTilte;
                    var seriesName;
                    var chartType = 'pie';
                    var chartUnite = '';
                    var displayTotal;

                    switch (reportDevName) {
                        case "Avct_nbre_contrats_RXu":
                            xAxisTilte = ""
                            yAxisTilte = "Avancement Augmentation - Globale";
                            seriesName = "Nombre contrats";
                            displayTotal = true;
                            //Aura:Id to use in the component
                            chartId = 'chart0'
                            break;
                        case "Avct_nbre_avenants_6SN":
                            xAxisTilte = "";
                            yAxisTilte = "s";
                            seriesName = "Nombre avenants";
                            displayTotal = true;
                            //Aura:Id to use in the component
                            chartId = 'chart1'
                            break;
                        case "Aug_en_CA_augmente_86L":
                            xAxisTilte = "";
                            yAxisTilte = "Avancement Augmentation- Globale";
                            seriesName = "Aug CA  ";
                            //Aura:Id to use in the component
                            chartId = 'chart2'
                            break;
                    }

                    helper.generateChart(component, event, helper, chartInputData, chartId, chartTitle, xAxisTilte, yAxisTilte, seriesName, chartType, chartUnite, displayTotal);
                });
            } else {
                console.log('error calling action --> getDataChartForglobIncrease');
            }
        });
        $A.enqueueAction(action);
    },

    getTauxAugmentation: function (component, event, helper) {
        var filiale = component.get("v.filiale");
        var filiere = component.get("v.filiere");
        //Set report's API names to display       
        var lstReportName = ["Taux_aug_par_filiale_0IQ", "Taux_aug_par_filiale_SANA_pYO", "Taux_aug_par_filiale_REGION_mJc", "Taux_aug_par_filiale_COMMERCIAL_dps", "Taux_aug_par_filiale_TYPO_CLIENTS_4Vf"];
        var action = component.get("c.getReportData");
        action.setParams({
            lstReportName: lstReportName,
            filiale: filiale,
            filiere: filiere
        });

        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                var reportsResultDatas = response.getReturnValue();
                console.log('reportsResultDatas ====' + reportsResultDatas);
                // 
                reportsResultDatas.forEach(function (rep) {
                    var chartInputData = JSON.parse(rep);
                    var chartId;
                    var reportDevName = chartInputData.reportMetadata.developerName;
                    var chartTitle = chartInputData.reportMetadata.name;
                    var xAxisTilte;
                    var yAxisTilte;
                    var seriesName;
                    var chartType = 'pie';
                    var chartUnite = '%';
                    var displayTotal;


                    switch (reportDevName) {
                        case "Taux_aug_par_filiale_0IQ":
                            xAxisTilte = ""
                            yAxisTilte = "Avancement Augmentation - Globale";
                            seriesName = "Bilan taux";
                            chartUnite = '%';
                            //Aura:Id to use in the component
                            chartId = 'chart3'
                            break;
                        case "Taux_aug_par_filiale_SANA_pYO":
                            xAxisTilte = "";
                            yAxisTilte = "Taux Augmentation - Globale";
                            seriesName = "Bilan taux";
                            //Aura:Id to use in the component
                            chartId = 'chart4'
                            break;
                        case "Taux_aug_par_filiale_REGION_mJc":
                            xAxisTilte = ""
                            yAxisTilte = "Taux Augmentation - Globale";
                            seriesName = "Bilan taux";
                            //Aura:Id to use in the component
                            chartId = 'chart5'
                            break;
                        case "Taux_aug_par_filiale_COMMERCIAL_dps":
                            xAxisTilte = "";
                            yAxisTilte = "Taux Augmentation - Globale";
                            seriesName = "Bilan taux";
                            //Aura:Id to use in the component
                            chartId = 'chart6'
                            break;
                        case "Taux_aug_par_filiale_TYPO_CLIENTS_4Vf":
                            xAxisTilte = ""
                            yAxisTilte = "Taux Augmentation - Globale";
                            seriesName = "Bilan taux";
                            //Aura:Id to use in the component
                            chartId = 'chart7'
                            break;
                    }

                    helper.generateChart(component, event, helper, chartInputData, chartId, chartTitle, xAxisTilte, yAxisTilte, seriesName, chartType, chartUnite, displayTotal);
                });
            } else {
                console.log('error calling action --> getDataChartForglobIncrease');
            }
        });
        $A.enqueueAction(action);
    },

    getAugmentationCA: function (component, event, helper) {
        var filiale = component.get("v.filiale");
        var filiere = component.get("v.filiere");
        //Set report's API names to display       
        var lstReportName = ["CA_non_soumis_FF_NtW", "Aug_en_CA_aug_VS_non_soumis_BGA","X8Montant_CA_N_N1_proportion_FaF_QNy"];
        var action = component.get("c.getReportData");
        action.setParams({
            lstReportName: lstReportName,
            filiale: filiale,
            filiere: filiere
        });

        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                var reportsResultDatas = response.getReturnValue();
                console.log('reportsResultDatas ====' + reportsResultDatas);
                // 
                reportsResultDatas.forEach(function (rep) {
                    var chartInputData = JSON.parse(rep);
                    var chartId;
                    var reportDevName = chartInputData.reportMetadata.developerName;
                    var chartTitle = chartInputData.reportMetadata.name;
                    var xAxisTilte;
                    var yAxisTilte;
                    var seriesName;
                    var chartType;
                    var chartUnite = '';
                    var displayTotal;
                    switch (reportDevName) {
                        case "CA_non_soumis_FF_NtW":
                            xAxisTilte = "";
                            yAxisTilte = "";
                            seriesName = "CA N+1 Estimatif";
                            chartType = "pie";
                            displayTotal = true;
                            //Aura:Id to use in the component
                            chartId = 'chart8';
                            break;
                        case "Aug_en_CA_aug_VS_non_soumis_BGA":
                            xAxisTilte = "";
                            yAxisTilte = "";
                            seriesName = "CA N+1 Estimatif";
                            chartType = "pie";
                            displayTotal = true;
                            //Aura:Id to use in the component
                            chartId = 'chart9';
                            break;                     
                            case "X8Montant_CA_N_N1_proportion_FaF_QNy":
                                xAxisTilte = "";
                                yAxisTilte = "";
                                seriesName = "";
                                chartType = "columnStacked";
                                //Aura:Id to use in the component
                                chartId = 'chart10';                              
                                break;                            

                    }                    

                   helper.generateChart(component, event, helper, chartInputData, chartId, chartTitle, xAxisTilte, yAxisTilte, seriesName, chartType, chartUnite, displayTotal);
                });
            } else {
                console.log('error calling action --> getDataChartForglobIncrease');
            }
        });
        $A.enqueueAction(action);
    },


    handleChartEvent: function (component, event, helper) {
        //
        var menuItem = event.getParam("menuItem");

        // set the handler attributes based on event data
        component.set("v.menuItem", menuItem);

        console.log('LE_ChartEvt_AugTarif.menuItem >> ', component.get("v.menuItem"));

        //Get the report to display
        
        switch (menuItem) {
            case "getAvancementAugmentation":
                helper.getAvancementAugmentation(component, event, helper);
                break;
            case "getTauxAugmentation":
                helper.getTauxAugmentation(component, event, helper);
                break;
            case "getAugmentationCA":
                helper.getAugmentationCA(component, event, helper);
                break;
        }
    },
    // Celui qui est utlisé par défaut
    piechart: function (component, event, helper, chartId, chartType, chartTitle, chartSubTitle, chartData, chartLabels, xAxisTilte, yAxisTilte, seriesName, chartUnite, listSign, displayTotal) {
        var data = [];
        chartData.forEach(function (dat) {
            data.push(dat);
        });
        console.log("data =====> "+data);

        var labs = [];
        chartLabels.forEach(function (lab) {
            labs.push(lab);
        });

        new Highcharts.Chart({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                renderTo: component.find(chartId).getElement(),
                type: chartType,
                events: { //Load the total
                    load: function (event) {
                        if (displayTotal) {
                            var total = 0;
                            for (var i = 0, len = this.series[0].yData.length; i < len; i++) {
                                total += this.series[0].yData[i];
                            }
                            var text = this.renderer.text(
                                '<b>Total: ' + total + '</b>',
                                this.plotLeft,
                                this.plotTop
                            ).attr({
                                zIndex: 5
                            }).add()
                        }
                        var chart = this;
                        if (this.series[0] && this.series[0].yData == 0) {//If no data to display in chart
                            var r = Math.min(chart.plotWidth / 2, chart.plotHeight / 2),
                                y = chart.plotHeight / 2 + chart.plotTop,
                                x = chart.plotWidth / 2 + chart.plotLeft;
                            /*   chart.pieOutline = chart.renderer.circle(x, y, r).attr({
                                   fill: '#ffffff',
                                   stroke: '#7cb5ec',
                                       'stroke-width': 1,
                                       
                               }).add(); */
                            chart.renderer.text('Aucune donnée à afficher ', 110, y)
                                .css({
                                    color: '#4572A7',
                                    fontSize: '12pt'
                                }).add();
                        }
                    }
                }
            },
            title: {
                text: ''//chartTitle
            },
            subtitle: {
                text: chartTitle
            },
            xAxis: {
                categories: labs,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title:
                {
                    text: yAxisTilte
                }
            },
            legend: {
                enabled: true,
                labelFormatter: function () {
                    var legendIndex = this.index;
                    var legendName = this.series.chart.axes[0].categories[legendIndex];
                    return legendName;
                }
            },
            tooltip: {
                //pointFormat: '{series.name}: <b>'+moins+' {point.y}</b>'
                formatter: function () {
                    var legendIndex = this.point.index;
                    var legendName = this.series.chart.axes[0].categories[legendIndex];
                    var s = this.series.name + ': <b>' + listSign[legendIndex] + ' ' + this.point.y + '</b>';

                    return s;
                }
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        formatter: function () {
                            var sliceIndex = this.point.index;
                            var sliceName = this.series.chart.axes[0].categories[sliceIndex];
                            if (chartUnite == '%') {
                                return '' + listSign[sliceIndex] + ' ' + this.y + ' ' + chartUnite;
                            } else {
                                return '' + listSign[sliceIndex] + ' ' + this.y + '';
                            }

                        }
                    },
                    showInLegend: true
                },
            },
            series: [{
                name: seriesName,
                data: data
            }]

        });

    },

    columncharts: function (component, event, helper, chartId, chartType, chartTitle, chartSubTitle, chartDataAutre, chartDataGroupSeche, chartLabels, xAxisTilte, yAxisTilte, seriesName) {

        var labs = [];
        chartLabels.forEach(function (lab) {
            labs.push(lab);
        });

        
        new Highcharts.Chart({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                renderTo: component.find(chartId).getElement(),
                type: chartType,
                styledMode: true
            },
            title: {
                text: ''//chartTitle
            },
            subtitle: {
                text: chartTitle
            },
            xAxis: {
                title:
                {
                    text: xAxisTilte
                },
                categories: labs,
                crosshair: true
            },
            yAxis: {
                // type: 'linear',
                min: 0,
                title:
                {
                    text: yAxisTilte
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        //fontWeight: 'bold',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'gray'
                    }
                }
            },
            legend: {
                align: 'center',
                x: -30,
                verticalAlign: 'top',
                y: 25,
                floating: true,
                backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || 'white',
                borderColor: '#CCC',
                borderWidth: 0,
                shadow: false
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            series: [{
                name: 'Autres',
                data: chartDataAutre
            }, {
                name: 'Groupe Séché',
                data: chartDataGroupSeche
            }]

        });

    },

    columnchart: function (component, event, helper, chartId, chartType, chartTitle, chartSubTitle, chartData, chartDataN1, chartLabels, xAxisTilte, yAxisTilte, seriesName) {

        var data = [];
        chartData.forEach(function (dat) {
            data.push(dat);
        });

        var labs = [];
        chartLabels.forEach(function (lab) {
            labs.push(lab);
        });

        
        new Highcharts.Chart({
            chart: {
                renderTo: component.find(chartId).getElement(),
                type: chartType,
                styledMode: true
            },
            title: {
                text: ''//chartTitle
            },
            subtitle: {
                text: chartTitle
            },
            xAxis: {
                title:
                {
                    text: xAxisTilte
                },
                categories: labs,
                crosshair: true
            },
            yAxis: {
                // type: 'linear',
                min: 0,
                title:
                {
                    text: yAxisTilte
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        //fontWeight: 'bold',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'gray'
                    }
                }
            },
            legend: {
                align: 'center',
                x: -20,
                verticalAlign: 'bottom',
                y: 20,
                floating: true,
                backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || 'white',
                borderColor: '#CCC',
                borderWidth: 0,
                shadow: false
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.2f} €</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.1,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Somme CA N',
                data: chartData
            }, {
                name: 'Somme CA N+1',
                data: chartDataN1
            }]

        });

    },

    columnchartSlack: function (component, event, helper, chartId, chartType, chartTitle, chartSubTitle, chartData, chartDataN1, chartLabels, xAxisTilte, yAxisTilte, seriesName) {

        //prepare dataSlack format
       var categories = [];
       var dataCanHorsFaf = [];
       var dataCanFaf = [];
       var dataCan1HorsFaf = [];
       var dataCan1Faf = [];
      
        chartData.forEach(function (dat) {
            categories.push(dat.nature);
            for(var i = 0; i< dat.data.length; i++){
                if(i == 0){//we are on CA N Satack column
                    dataCanHorsFaf.push(dat.data[0] != undefined ? dat.data[0].can : 0);
                    dataCanFaf.push(dat.data[1] != undefined ? dat.data[1].can : 0);
                }else {//we are on CA N+1 Satack column
                    dataCan1HorsFaf.push(dat.data[0] != undefined ? dat.data[0].can1 : 0);
                    dataCan1Faf.push(dat.data[1] != undefined ? dat.data[1].can1: 0);
                }             
            }
           
        });

        var labs = [];
        chartLabels.forEach(function (lab) {
            labs.push(lab);
        }); 

        
          //
          Highcharts.chart({
            chart: {
                renderTo: component.find(chartId).getElement(),
                type: 'column',
                styledMode: true
            },
        
            title: {
                text: ''
            },
            subtitle: {
                text: chartTitle
            },        
            xAxis: {
                labels: {
                    format: '<div style="text-align:center;">&nbsp; CA N &nbsp; CA N+1<br/><b/>{value}</div>',
                    useHTML: true
                  },
                categories: categories
            },
        
            yAxis: {
                allowDecimals: false,
                min: 0,
                title: {
                    text: 'CA'
                }
            },        
            tooltip: {
                formatter: function () {
                    return '<b>' + this.x + '</b><br/>' +
                        this.series.name + ': ' + this.y + '<br/>' +
                        'Total: ' + this.point.stackTotal;
                }
            },
        
            plotOptions: {
                column: {
                    stacking: 'normal'
                }
            },
        
            series: [{ 
                color:'#7cb5ec',
                name: 'CA N hors FaF',
                data: dataCanHorsFaf,
                stack: 'CAN'
              }, {
                color:'#f7a35c',
                name: 'CA N FaF',
                data: dataCanFaf,
                stack: 'CAN'
              }, {
                color:'#434348',
                name: 'CA N+1 hors FaF',
                data: dataCan1HorsFaf,
                stack: 'CAN+1'
              }, {
                 color:'#f7a35c',
                name: 'CA N+1 FaF',
                data: dataCan1Faf,
                stack: 'CAN+1'
              }]
            });

    },

    donutchart: function (component, event, helper, chartId, chartType, chartTitle, chartSubTitle, chartData, chartLabels, xAxisTilte, yAxisTilte, seriesName) {
        var data = [];
        chartData.forEach(function (dat) {
            data.push(dat);
        });

        var labs = [];
        chartLabels.forEach(function (lab) {
            labs.push(lab);
        });

        new Highcharts.Chart({
            chart: {
                renderTo: component.find(chartId).getElement(),
                type: 'pie'
            },
            title: {
                text: ''//chartTitle
            },
            subtitle: {
                text: chartTitle
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    innerSize: 100,
                    depth: 45
                }
            },
            series: [{
                type: 'pie',
                name: seriesName,
                data: data
            }]

        });

    },

    toastdMessage: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    }

})