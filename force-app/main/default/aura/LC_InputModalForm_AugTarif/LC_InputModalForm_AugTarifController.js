({
    doInit : function(component, event, helper) {
        console.log('LC_InputFormModal loaded..'); 
        console.log('Filiale..: ',component.get("v.filiale")); 
        console.log('selectedListCont..: ',component.get("v.selectedListCont")); 
        console.log('Filiale..: ',component.get("v.filiale")); 
    },

    toastdMessage : function(title,message,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": 'dismissible',
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    },

    fireInputFormEvent : function(component, event, helper) {
        var compEvent = component.getEvent("InputFormEvent");        
        compEvent.setParams({"defaultValue" : component.get("v.defaultValue") });
        compEvent.fire();
    },

    handleInputFormEvent : function(component, event, helper) {
       component.set("v.defaultValueTrait",component.get("v.defaultValue"));
       component.set("v.defaultValueCondi",component.get("v.defaultValue"));
       component.set("v.defaultValuePresta",component.get("v.defaultValue"));
    },

    closeModal : function(component, event, helper) {
        // when a component is dynamically created in lightning, we use destroy() method to destroy it.
        component.destroy();
    }, 

    submitForm: function (component, event, helper) {
        //debugger;
        var selectedListAvenant = component.get("v.selectedListAvenant");
        var inputAll = component.get("v.defaultValue")!=""?component.get("v.defaultValue"):0.0;
        var inputTrait = component.get("v.defaultValueTrait")!=""?component.get("v.defaultValueTrait"):0.0;
        var inputCondi = component.get("v.defaultValueCondi")!=""?component.get("v.defaultValueCondi"):0.0;
        var inputPresta = component.get("v.defaultValuePresta")!=""?component.get("v.defaultValuePresta"):0.0;
        var checkBoxValue =  component.get("v.value")[0];     
       
        console.log('validate.selectedListAvenant = ',selectedListAvenant);
        console.log('validate.inputAll = ',inputAll);
        console.log('validate.inputTrait = ',inputTrait);
        console.log('validate.inputCondi = ',inputCondi);
        console.log('validate.inputPresta = ',inputPresta);
        console.log('validate.checkBoxValue = ',checkBoxValue); 
        
         //call Apex
         var action = component.get("c.percentAugmentation");
         action.setParams({
             lstAmend:  selectedListAvenant,
             inputValue:inputAll,
             inputValueTraitement: inputTrait,
             inputValueTransport: inputPresta,
             inputValueConditionnement: inputCondi,
             RoundingType: checkBoxValue}); 
  
         //configure action handler
         action.setCallback(this, function(response){
            // debugger;
             var state = response.getState();          
             if(state === "SUCCESS"){
                 if(response.getReturnValue() === "ok"){
                    helper.toastdMessage("Augmentation tarifaire","Augmentations effectuées avec succès","success");
                    component.destroy();
                 }else{
                    helper.toastdMessage("Augmentation tarifaire","Erreur rencontrée pendant l'augmentation, veuillez contacter l'administrateur","error");
                    component.destroy();
                 }
                             
             }else{
                 console.log('Erreur rencontrée pendant la mise à jours '+state);
             }
         });
         $A.enqueueAction(action);
       
    },

    handleChange: function (component, event) {
        console.log(event.getParam('value'));
      // alert(component.get("v.value"));        
       component.set("v.checkBoxValue",component.get("v.value") );      
    }
})