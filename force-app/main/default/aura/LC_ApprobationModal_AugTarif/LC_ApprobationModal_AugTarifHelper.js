({
    processOnContractCallBack: function (component, event, helper,selectedListCont) {
        //debugger;
        var selectedListCont = selectedListCont;
        var objectType = component.get("v.objectType");
        console.log('associate.selectedListCont = ', selectedListCont);
        var filiale = component.get("v.filiale");
        var filiere = component.get("v.filiere");

        //call Apex
        var action = component.get("c.submitAMEForValidation");
        action.setParams({
            contractsList: selectedListCont, 
            filiale : filiale,
            filiere : filiere           
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.toastdMessage("Soumettre pour validation", response.getReturnValue().message,  response.getReturnValue().toastMode);
                //Fire Event to update List
                var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
                appEvent.setParams({
                    "isCancel": false                            
                });
                appEvent.fire();
                
                component.destroy();

            } else {
                console.log('Erreur rencontrée pendant la mise à jours veuillez contacter l\'administrateur' + state);
            }
        });
        $A.enqueueAction(action);

    },

    processOnContract: function (component, event, helper) {
        //debugger;
        var selectedListCont = component.get("v.selectedListCont");
        var objectType = component.get("v.objectType");
        console.log('associate.selectedListCont = ', selectedListCont);
        var filiale = component.get("v.filiale");
        var filiere = component.get("v.filiere");
        var selectedListCont2;      

        if(selectedListCont.length> 100){  // divide the list of amendements  to avoid the CPU Limit(processe bulider & triggers are waiting somewhere ^^)       
            selectedListCont2 = selectedListCont.splice(0, Math.ceil(selectedListCont.length / 2));

           console.log('selectedListCont---',selectedListCont.length);
           console.log('selectedListCont2---',selectedListCont2.length);
       }

        //call Apex
        var action = component.get("c.submitAMEForValidation");
        action.setParams({
            contractsList: selectedListCont, 
            filiale : filiale,
            filiere : filiere           
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            console.log(JSON.stringify(response));
            if (state === "SUCCESS") {
                if(selectedListCont2){
                    helper.processOnContractCallBack(component, event, helper,selectedListCont2);
                }else{
                    helper.toastdMessage("Soumettre pour validation", response.getReturnValue().message,  response.getReturnValue().toastMode);
                    //Fire Event to update List
                    var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
                    appEvent.setParams({
                        "isCancel": false                            
                });
                appEvent.fire();
                
                component.destroy();
                }
                

            } else {
                console.log('Erreur rencontrée pendant la mise à jours veuillez contacter l\'administrateur' + state);
            }
        });
        $A.enqueueAction(action);

    },

    processOnAmendement: function (component, event, helper) {
        //debugger;
        var selectedListCont = component.get("v.selectedListCont");
        var objectType = component.get("v.objectType");
        console.log('associate.selectedListCont = ', selectedListCont);

        //call Apex
        var action = component.get("c.submitAMEForValidation");
        var filiale = component.get("v.filiale");
        var filiere = component.get("v.filiere");
        action.setParams({
            contractsList: selectedListCont,
            filiale : filiale,
            filiere : filiere
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.toastdMessage("Soumettre pour validation", response.getReturnValue().message,  response.getReturnValue().toastMode);
                //fire evnt to update list
                var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");   
                appEvent.setParams({
                    "isCancel": false                            
                });
                appEvent.fire();
        
                component.destroy();

            } else {
                console.log('Erreur rencontrée pendant la mise à jours veuillez contacter l\'administrateur' + state);
            }
        });
        $A.enqueueAction(action);

    },
    
    toastdMessage: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": 'dismissible',
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    }
})