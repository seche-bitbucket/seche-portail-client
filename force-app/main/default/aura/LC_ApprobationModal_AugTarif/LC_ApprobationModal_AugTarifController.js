({  
    closeModal: function (component, event, helper) {
        // when a component is dynamically created in lightning, we use destroy() method to destroy it.
        var objectType = component.get("v.objectType");      
        if(objectType === 'contract'){         
            var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
            appEvent.setParams({
                "isCancel": false                            
            });
        }else{           
            var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");   
            appEvent.setParams({
                "isCancel": false                            
            });               
        }
         appEvent.fire();

        component.destroy();
    },

    process: function (component, event, helper) {
       var objectType = component.get("v.objectType");
       if(objectType === 'contract'){
           helper.processOnContract(component, event, helper);
       }else{
        helper.processOnAmendement(component, event, helper);
       }

    },

    fireRefreshEvent : function(component, event, helper) {
        var objectType = component.get("v.objectType");      
        if(objectType === 'contract'){         
            var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
            appEvent.setParams({
                "isCancel": false                            
            });
        }else{           
            var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");   
            appEvent.setParams({
                "isCancel": false                            
            });               
        }
         appEvent.fire();

    }
    
})