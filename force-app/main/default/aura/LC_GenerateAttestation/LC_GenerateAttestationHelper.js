({
    init: function (component, event, helper) {
       
        var recordId = component.get("v.recordId");

        //call Apex
        var action = component.get("c.getAttestation");
        action.setParams({
            attestationId: recordId                    
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
               component.set("v.attestation",response.getReturnValue());

            } else {
                console.log('getAttestation --> Erreur rencontrée pendant la récupération de l\'attestation' + state);
            }
        });
        $A.enqueueAction(action);

    },

    processGeneration: function (component, event, helper) {
        debugger;
        //check attestation values before call Apex
        var attestation = component.get("v.attestation");
        var attestation_generate_status = component.get("v.attestation_generate_status");
        var attestation_notconcerned_status = component.get("v.attestation_notconcerned_status");
        var sorted = attestation.Sorted__c;
        var mixed = attestation.Mixed__c;
        var papercardboard = attestation.PaperCardboard__c;
        var plastic = attestation.Plastic__c;
        var metal = attestation.Metal__c;
        var glass = attestation.Glass__c;
        var wood = attestation.Wood__c;
        var plaster = attestation.Plaster__c;
        var mineralFractions = attestation.MineralFractions__c;
        var status = attestation.Status__c;
        var numberFiles = attestation.ContentDocumentLinks ? attestation.ContentDocumentLinks.length:0 ;

        if(status==attestation_notconcerned_status){
            helper.toastdMessage("Génération attestation","Impossible à générer car "+attestation_notconcerned_status,  "warning");               
            $A.get("e.force:closeQuickAction").fire();
        }else if(status==attestation_generate_status || numberFiles > 0){
            helper.toastdMessage("Génération attestation","Attestation déjà générée.",  "warning");               
            $A.get("e.force:closeQuickAction").fire();
        }else if(!mixed && !sorted){
            helper.toastdMessage("Génération attestation","Veuillez choisir entre 'Triés' ou 'En mélange'",  "warning");               
            $A.get("e.force:closeQuickAction").fire();
        } else if(!papercardboard && !plastic && !metal && !glass && !wood && !plaster && !mineralFractions){
            helper.toastdMessage("Génération attestation","Veuillez choisir un déchet si 'Triés', ou plusieurs si 'En mélange'",  "warning");               
            $A.get("e.force:closeQuickAction").fire();
        } else{
            var recordId = component.get("v.recordId");

            //call Apex
            var action = component.get("c.updateAttestation");
            action.setParams({
                attestationId: recordId                    
            });
    
            //configure action handler
            action.setCallback(this, function (response) {
                // debugger;
                var state = response.getState();
                if (state === "SUCCESS") {
                    helper.toastdMessage("Génération attestation", response.getReturnValue().message,  response.getReturnValue().toastMode);               
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
    
                } else {
                    console.log('Erreur rencontrée pendant la mise à jours veuillez contacter l\'administrateur' + state);
                }
            });
            $A.enqueueAction(action);
        }    
       
    },

    toastdMessage: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": 'sticky',
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    }

})