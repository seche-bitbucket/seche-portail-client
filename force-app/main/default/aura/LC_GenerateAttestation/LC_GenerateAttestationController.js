({
    doInit: function (component, event, helper) {
        helper.init(component, event, helper);
     },

    process: function (component, event, helper) {
        helper.processGeneration(component, event, helper);
     },

     closeModal: function (component, event, helper) {      
        $A.get("e.force:closeQuickAction").fire();

    }

})