({  
    init: function(component, event, helper) {
        var selectedAv = component.get('v.selectedListAvenant');
        var containsCompleted = false;

        console.log('selectedAv', selectedAv);

        selectedAv.forEach(av => {
            if(av.CompletedAmadement__c) {
                containsCompleted = true;
            }
        })

        console.log('init', containsCompleted);
        component.set('v.warning', containsCompleted);
    },

    closeModal: function (component, event, helper) {
        // when a component is dynamically created in lightning, we use destroy() method to destroy it.
        var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
        appEvent.setParams({
           "isCancel": true,
           "actionFromContrat": component.get('v.actionFromContrat')                            
        });
 
          appEvent.fire();
        component.destroy();
    },

    exclude : function(component, event, helper) {
        var selectedListCont = component.get("v.selectedListCont");
        var selectedListAvenant = component.get("v.selectedListAvenant");
        var reason = component.find("inputReason").get("v.value"); 
        //helper.processCallBack(component, event, helper,selectedListCont,selectedListAvenant,reason);
		var selectedListAvenant = component.get("v.selectedListAvenant");
        var amendPartionLists = selectedListAvenant.length > 1500 ? helper.doListPartition(selectedListAvenant, 1500) : helper.doListPartition(selectedListAvenant, selectedListAvenant.length);
        helper.excluded_am(component, event, helper, amendPartionLists, 0, reason);
    },

    fireRefreshEvent : function(component, event, helper) {
        // Get the  event by using the
        // name value from aura:registerEvent
        //debugger;
      //  var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif"); 
      var appEvent = $A.get("e.c:LE_ContractsList_AugTarif");   
       appEvent.setParams({
          "isCancel": false                            
       });

         appEvent.fire();
       
    }

})