({
    toastdMessage: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },
    processCallBack: function (component, event, helper,selectedListCont,selectedListAvenant,reason) {
        debugger;
        var selectedListCont = selectedListCont;
        var selectedListAvenant = selectedListAvenant;
        var reason = reason;

        console.log('increaseContractFaf.selectedListCont = ', selectedListCont);
        console.log('increaseContractFaf.selectedListAvenant = ', selectedListAvenant);
        
        //call Apex
        var action = component.get("c.excluded");
        action.setParams({
            contractList: selectedListCont,
            amendmentList: selectedListAvenant,
            reason: reason
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.toastdMessage("Exclusion", response.getReturnValue().message, response.getReturnValue().toastMode);
                //Fire event to update the list
                //we must refresh contract List too to refresh related Amendements :(
                var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                appEvent.setParams({
                    "isCancel": false
                });
                //we must refresh contract List too to refresh related Amendements :(
                var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                appEventCont.setParams({
                    "isCancel": false,
                    "isAvenantUpdate": true,
                    "actionFromContrat": component.get('v.actionFromContrat')
                });
                appEvent.fire();
                appEventCont.fire();
                component.destroy();           
            } else {
                console.log('Erreur rencontrée pendant la mise à jours veuillez contacter l\'administrateur' + state);
            }
        });
        $A.enqueueAction(action);

    },
    
    excluded_am: function(component, event, helper, list, position, reason) {
        //call Apex
        
        component.set("v.displayProgress", true);
        component.set("v.message", "Mise à jour des avenants en cours...");
        if(list.length > 1) {
            component.set('v.progressValue', parseInt(((50/(list.length-1))*position)));
        } else {
            component.set('v.progressValue', parseInt(0));
        }
        
        var action = component.get("c.excluded_am");
        action.setParams({
            reason: reason,
            amendmentList: list[position]        
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                if(position+1 <= list.length-1) {
                    console.log('excludeAm', position)
                    helper.excluded_am(component, event, helper, list, position+1, reason);
                } else {
                    console.log('contract')
                    var selectedListCont = component.get("v.selectedListCont");

    				var contractPartionLists = selectedListCont.length > 100 ? helper.doListPartition(selectedListCont, 100) : helper.doListPartition(selectedListCont, selectedListCont.length);
                    console.log(contractPartionLists)
                    helper.excluded_c(component, event, helper, contractPartionLists, 0, reason);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    excluded_c: function(component, event, helper, list, position, reason) {
        //call Apex
        
        component.set("v.message", "Mise à jour des contrats en cours...");
        if(list.length > 1) {
            component.set('v.progressValue', parseInt(((50/(list.length-1))*position)+50));
        } else {
            component.set('v.progressValue', parseInt(50));
        }
        
        var action = component.get("c.excluded_c");
        action.setParams({
            reason: reason,
            contractList: list[position]        
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                if(position+1 <= list.length-1) {
                    console.log('excludeC', position)
                    helper.excluded_c(component, event, helper, list, position+1, reason);
                } else {
                    component.set('v.progressValue', parseInt(100));
                    console.log('done')
                   helper.toastdMessage("Exclusion", response.getReturnValue().message, response.getReturnValue().toastMode);
                    //Fire event to update the list
                    //we must refresh contract List too to refresh related Amendements :(
                    var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                    appEvent.setParams({
                        "isCancel": false
                    });
                    //we must refresh contract List too to refresh related Amendements :(
                    var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                    appEventCont.setParams({
                        "isCancel": false,
                        "isAvenantUpdate": true,
                        "actionFromContrat": component.get('v.actionFromContrat')
                    });
                    appEvent.fire();
                    appEventCont.fire();
                    component.destroy();     
                }
            } else {
                let errorMsg = response.getError()[0];
                
                console.log('response.getError()', response.getError());
                console.log('Erreur rencontrée pendant la mise à jours veuillez contacter l\'administrateur. Erreur :' + errorMsg);
            }
        });
        $A.enqueueAction(action);
    },
    
        /**
 * Returns an array with arrays of the given size.
 *
 * @param myArray {Array} array to split
 * @param chunk_size {Integer} Size of every group
 */
    doListPartition : function (myArray, chunk_size){    
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];
    
    for (index = 0; index < arrayLength; index += chunk_size) {
       let  myChunk = myArray.slice(index, index+chunk_size);
        // Do something if you want with the group
        tempArray.push(myChunk);
    }

    return tempArray;
}
})