({
    doInit : function(component, event) {
        // the function that reads the url parameters
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
                debugger;
            
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var UID = getUrlParameter('UID');
        var ID = getUrlParameter('ID');
        var setId=component.set("v.Id",ID);
        var setuId=component.set("v.Uid",UID);
        console.log(component.get("v.Id"));
        console.log(component.get("v.Uid"));
        var action = component.get("c.getQuoteId");
        action.setParams({
            Id: ID,
            Uid: UID
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === "SUCCESS" && result != null) {
                var ssc = component.set('v.ssc', response.getReturnValue());
                var res= component.get("v.ssc.RequestResult");
                if(res=='OK'){
                    component.set("v.OkTosend","true");
                    console.log(res);
                }else if(res=='UIDNOTOK'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Erreur",
                        "type": "Error",
                        "message": "le paramètre sur le lien envoyé a été modifié ou n\'existe pas, veuillez réessayer avec le lien original ",
                    });
                    toastEvent.fire();
                }else if(res=='TOKENNOTOK'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Erreur",
                        "type": "Error",
                        "message": "le document ne peut pas être envoyé pour signature à cause d\'erreurs inconnues",
                    });
                    toastEvent.fire();
                }else if(res=='IDNULL'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Erreur",
                        "type": "Error",
                        "message": "le paramètre sur le lien envoyé a été modifié ou n\'existe pas",
                    });
                    toastEvent.fire();
                }else if(res=='ALREADYSENT'){
                    var form = component.find('entireForm');
                    $A.util.addClass(form,'slds-hide'); 
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Info",
                        "type": "Info",
                        "message": "le document est déjà envoyé pour signature",
                    });
                    toastEvent.fire();
                }
            }
            else {
                alert('Error : ' + JSON.stringify(errors));
            }
        });
        $A.enqueueAction(action);
    },
    
    sendForSignature : function(component, event) {
        var OkTosend=component.get("v.OkTosend");
        if(OkTosend=="true"){
            var action = component.get("c.SendForSignature");
            var FirstName = component.get("v.ssc.FirstName");
            var LastName = component.get("v.ssc.LastName");
            var Email = component.get("v.ssc.Email");
            var Phone = component.get("v.ssc.Phone");
            var ID = component.get("v.Id");
            var ContactId= component.get("v.ssc.ContactId");
            var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  
            if(FirstName!=null && LastName!=null && Email!=null){
                if(Email.match(regExpEmailformat)){
                    action.setParams({
                        Id : ID,
                        FirstName : FirstName,
                        LastName : LastName,
                        Email : Email,
                        Phone : Phone,
                        ContactId : ContactId
                    });
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        var result = response.getReturnValue();
                        console.log(state);
                        if (state === "SUCCESS" ) {
                            if(result=='OK'){
                                var form = component.find('entireForm');
                                $A.util.addClass(form,'slds-hide'); 
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "Succès",
                                    "type": "success",
                                    "message": "Votre document est envoyé pour signature",
                                });
                                toastEvent.fire();
                            }else if(result=='AlreadySent'){
                                var form = component.find('entireForm');
                                $A.util.addClass(form,'slds-hide');  
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "Info",
                                    "type": "Info",
                                    "message": "Votre document est déjà envoyé pour signature",
                                });
                                toastEvent.fire();
                            }else{
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "Erreur",
                                    "type": "Error",
                                    "message": "Une erreur s\'est produite:"+ result,
                                });
                                toastEvent.fire();
                            }
                        }
                        else if (state === "ERROR") {
                            alert('Une erreur s\'est produite:'+JSON.stringify(errors)+'.' );
                        }
                    });
                    $A.enqueueAction(action);
                }else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Info",
                        "type": "Info",
                        "message": "Veuillez saisir un mail valide",
                    });
                    toastEvent.fire();
                }
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Info",
                    "type": "Info",
                    "message": "Veuillez remplir le formulaire avant de le saisir",
                });
                toastEvent.fire();
            }
        }
    },
    
    UpdateField : function(component, event, helper) {
        var ssc = component.set("v.ssc.ContactId", "Unknown");
    },
    UpdateInformation : function(component, event, helper) {
    }
})