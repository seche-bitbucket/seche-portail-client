({
    doInit : function(component, event, helper) {
        helper.doInit(component, event);
    },
    
    sendForSignature : function(component, event, helper) {
        helper.sendForSignature(component, event);
    },
    UpdateField : function(component, event, helper) {
        helper.UpdateField(component, event);
    },
    UpdateInformation : function(component, event, helper) {
        helper.UpdateInformation(component, event);
    }
})