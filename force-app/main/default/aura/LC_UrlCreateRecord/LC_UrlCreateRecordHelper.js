({
    handleShowCreateForm: function( component ) {

        /*
         * Supported URL parameters:
         *   objectName - API name of a standard or custom object (e.g. Account or Project__c)
         *   recordTypeId - which record type to use, if not specified then the user's default record type is assumed
         *
         * All other key=value URL parameters are
         * assumed as default field values for the record form.
         *
         * URL parameter names must match the API field names exactly (case-sensitive).
         * For example, "phone" and "PHONE" will not pre-populate the standard Contact "Phone" field.
         *
         * Example Custom Button URL:
         *     "/lightning/cmp/c__URL_CreateRecordCmp?objectName=Contact&FirstName=Astro&LastName=Nomical&AccountId={!Account.Id}"
         */

       let helper = this;
       let pageRef = component.get('v.pageReference');    
        console.log( 'pageRef------ ', pageRef );
        // Retrieve specific parameters from the URL.
        // For case-insensitivity, the properties are lowercase.
        let urlParamMap = {
            'c__ObjectName' : '',      // object whose create form to display
            'c__RecordTypeId' : '',    // record type for new record (optional)
            'c__RecordId' : ''  ,       // id of record where button was clicked
            'c__AccountId' : '' ,
            'c__OpportuniteMereIntra' : '' ,
            'c__CloseDate' : '16/04/2019',
            'c__Nature' : '',
            'c__ContactName' : '' ,
            'c__StageName' : '' 
            
        };

        for ( let key in pageRef.state ) {
            //let lowerKey = key.toLowerCase();
            
            if ( urlParamMap.hasOwnProperty( key ) ) {
                urlParamMap[key] = pageRef.state[key];
            }
        }
        console.log( 'urlParamMap', urlParamMap );
        
        //Load Opp
      // helper.loadOpp(component,event, helper);

        Promise.resolve()
            .then( function() {
                if ( !$A.util.isEmpty( urlParamMap.c__RecordId ) ) {
                    // workaround for not being able to customize the cancel
                    // behavior of the force:createRecord event. instead of
                    // the user seeing a blank page, instead load in the background
                    // the very record the user is viewing so when they click cancel
                    // they are still on the same record.
                    helper.navigateToUrl( '/' + urlParamMap.c__RecordId );
                    // give the page some time to load the new url
                    // otherwise we end up firing the show create form
                    // event too early and the page navigation happens
                    // afterward, causing the quick action modal to disappear.
                    return new Promise( function( resolve, reject ) {
                        setTimeout( resolve, 1000 );
                    });
                }
            })
            .then( function() {
                helper.showCreateForm( component, urlParamMap, pageRef );
            });

    },

    //-----------------------------------------------------------------
  /*  loadOpp: function(component, event, helper) {    
        //debugger;    
        var action = component.get("c.getOpportunity");
        action.setParams({"oppId" :  urlParamMap.c__RecordId});         
        action.setCallback(this, function(response){             
            var state = response.getState();
            if(state === "SUCCESS"){
                console.log('SUCCESS: ', response.getState());
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + errors[0].message);                                 
                    }
                } else {
                    console.log('Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
   },  */

    // -----------------------------------------------------------------

    showCreateForm: function( component, urlParamMap, pageRef ) {

        let helper = this;

        helper.enqueueAction( component, 'c.getFieldDescribeMap', {

            'ObjectName' : urlParamMap.c__ObjectName

        }).then( $A.getCallback( function( fieldDescribeMap ) {

            console.log( 'fieldDescribeMap', fieldDescribeMap );
 
            let eventParamMap = {
                'defaultFieldValues' : {}
            };

            if ( !$A.util.isEmpty( urlParamMap.c__ObjectName ) ) {
                eventParamMap['entityApiName'] = urlParamMap.c__ObjectName;
            }

            if ( !$A.util.isEmpty(urlParamMap.c__RecordTypeId )) {
                eventParamMap['recordtypeid'] = urlParamMap.c__RecordTypeId;
            }

            // ensure only fields the current user has permission to create are set
            // otherwise upon attempt to save will get component error
            var customFields = ['ContactName', 'OpportuniteMereIntra', 'Nature'];
            let customFieldsMap = {
                'ContactName' : 'ContactName__c',     
                'OpportuniteMereIntra' : 'OpportuniteMereIntra__c',   
                'Nature' : 'Nature__c'
            };
            for ( let fieldName in pageRef.state ) {
                //delete namespace from params fields  
                
                var fieldLabel = fieldName.substring(3, fieldName.length);
                if(fieldLabel === "ContactName" ){
                   fieldLabel = customFieldsMap.ContactName;                 
                }else  if(fieldLabel === "OpportuniteMereIntra" ){
                    fieldLabel = customFieldsMap.OpportuniteMereIntra;                 
                 }else  if(fieldLabel === "Nature" ){
                    fieldLabel = customFieldsMap.Nature;                 
                 }

                console.log('fieldLabel--- :', fieldLabel );              
                if ( fieldDescribeMap.hasOwnProperty( fieldLabel ) && fieldDescribeMap[fieldLabel].createable ) {
                    // avoid setting lookup fields to undefined, get Error ID: 1429293140-211986 (-590823013), assign to null instead
                    eventParamMap.defaultFieldValues[fieldLabel] = pageRef.state[fieldName] || null;
                }
            }           
            
            return eventParamMap;

        })).then( $A.getCallback( function( eventParamMap ) {

            console.log( 'eventParamMap', eventParamMap );
            
            $A.get( 'e.force:createRecord' ).setParams( eventParamMap ).fire();

        })).catch( $A.getCallback( function( err ) {

            helper.logActionErrors( err );

        }));

    },

    navigateToUrl: function( url ) {

        console.log( 'navigating to url', url );

        if ( !$A.util.isEmpty( url ) ) {
            $A.get( 'e.force:navigateToURL' ).setParams({ 'url': url }).fire();
        }

    },

    enqueueAction: function( component, actionName, params, options ) {

        let helper = this;

        return new Promise( function( resolve, reject ) {

            component.set( 'v.showSpinner', true );

            let action = component.get( actionName );

            if ( params ) {
                action.setParams( params );
            }

            if ( options ) {
                if ( options.background ) { action.setBackground(); }
                if ( options.storable )   { action.setStorable(); }
            }

            action.setCallback( helper, function( response ) {

                component.set( 'v.showSpinner', false );

                if ( component.isValid() && response.getState() === 'SUCCESS' ) {

                    resolve( response.getReturnValue() );

                } else {

                    console.error( 'Error calling action "' + actionName + '" with state: ' + response.getState() );

                    helper.logActionErrors( response.getError() );

                    reject( response.getError() );

                }
            });

            $A.enqueueAction( action );

        });
    },

    logActionErrors : function( errors ) {
        if ( errors ) {
            if ( errors.length > 0 ) {
                for ( let i = 0; i < errors.length; i++ ) {
                    console.error( 'Error: ' + errors[i].message );
                }
            } else {
                console.error( 'Error: ' + errors );
            }
        } else {
            console.error( 'Unknown error' );
        }
    }
})