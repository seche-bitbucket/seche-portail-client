({
    
	CheckStatus : function(component, event, helper){
        
        var action = component.get("c.isEditableServiceDemand");
        action.setParams({
            "serviceDemandID" : component.get("v.recordId")
        });
        // set call back 
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                // if response is true, table is editable, else it's read only 
                component.set("v.isEditable", response.getReturnValue());
                console.log(response.getReturnValue());
            }else{
                var errors = response.getError();
                alert(''+errors[0].message);
            }
        });
        // enqueue the server side action  
        $A.enqueueAction(action);
    },    

	// function call on component Load
    getAllQuotelines : function(component, event, helper) {

        // Get a reference to the getPriceBookEntries() function defined in the Apex controller
        var action = component.get("c.getPriceBookEntries");
        action.setParams({
            "serviceDemandID": component.get("v.recordId")
        });
        // Register the callback function
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.PBEntries", response.getReturnValue());
            }
        });
        // Invoke the service
        $A.enqueueAction(action);
        
    },
    
    CheckQuantity : function(component, event, helper){
        
        var qls = component.get("v.PBEntries");
        var quantity = 0;
        
        for(var key in qls){
            var ql = qls[key];
            if(ql.Quantity__c != undefined){
                quantity += ql.Quantity__c
            }
            
        }
        return quantity;
    },
    
    SaveLines : function(component, event, helper){
        
        var qls = JSON.stringify(component.get("v.PBEntries"));
       
        var action = component.get("c.saveQuoteLines");
        action.setParams({
            "qls" : qls,
            "serviceDemandID" : component.get("v.recordId")
        });
        // set call back 
        action.setCallback(this, function(response) {
            console.log('Fire');
            var state = response.getState();
            if (state === "SUCCESS") {
                // if response if success then reset/blank the 'contactList' Attribute 
                // and call the common helper method for create a default Object Data to Contact List 
                component.set("v.PBEntries", response.getReturnValue());
                component.set("v.AddedQuoteLines", []);
                alert('Elément(s) à éliminer ajouté(s)');
            }else{
                var errors = response.getError();
                alert(''+errors[0].message);
            }
        });
        // enqueue the server side action  
        $A.enqueueAction(action);
    }
    
})