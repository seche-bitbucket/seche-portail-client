({
    
    // function call on component Load
    doInit: function(component, event, helper) {
        
        helper.CheckStatus(component, event, helper);
        helper.getAllQuotelines(component, event, helper);
       
        
    },
    
    // function for save the Records 
    Save: function(component, event, helper) {
        
        var TotalQuantity = helper.CheckQuantity(component, event, helper);
               
        if(TotalQuantity > 9){
           helper.SaveLines(component, event, helper);
        }else{
            alert('La quantité des éléments doit être au minimum de 10');
        }
        
        
    },
    
    
    
    
    
})