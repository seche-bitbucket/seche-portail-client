({
  closeModal: function (component, event, helper) {
    // when a component is dynamically created in lightning, we use destroy() method to destroy it.
    var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");
    appEvent.setParams({
      "isCancel": true,
      "actionFromContrat": component.get('v.actionFromContrat')
    });

    appEvent.fire();
    component.destroy();
  },

  reset: function (component, event, helper) {

  
    var selectedListCont = component.get("v.selectedListCont");
    var selectedListAvenant = component.get("v.selectedListAvenant");

    var contractPartionLists = selectedListCont.length > 500 ? helper.doListPartition(selectedListCont, 500) : helper.doListPartition(selectedListCont, selectedListCont.length);
    component.set("v.contractPartionLists", contractPartionLists);
    var amendPartionLists = selectedListAvenant.length > 1500 ? helper.doListPartition(selectedListAvenant, 1500) : helper.doListPartition(selectedListAvenant, selectedListAvenant.length);
      
      helper.recursive_am(component, event, helper, amendPartionLists, 0);

    /*for (let i = 0; i < amendPartionLists.length; i++) {
      helper.processAmendmentReset(component, event, helper, amendPartionLists[i]);
      console.log('amendPartionLists current processed item lengnth ', amendPartionLists[i].length);
    }*/

  },

  fireRefreshEvent: function (component, event, helper) {
    // Get the  event by using the
    // name value from aura:registerEvent
    //debugger;
    //  var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif"); 
    var appEvent = $A.get("e.c:LE_ContractsList_AugTarif");
    appEvent.setParams({
      "isCancel": false
    });

    appEvent.fire();

  },

  // this function automatic call by aura:waiting event  
  showSpinner: function (component, event, helper) {
    // make Spinner attribute true for display loading spinner 
    component.set("v.Spinner", true);
  },

  // this function automatic call by aura:doneWaiting event 
  hideSpinner: function (component, event, helper) {
    // make Spinner attribute to false for hide loading spinner    
    component.set("v.Spinner", false);
  }


})