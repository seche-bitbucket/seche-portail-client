({
    toastdMessage: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },

    showSystemError: function(cmp, event) {
        // Handle system error
        $A.log(cmp);
        $A.log(event); 
        console.log('showSystemError.reset event --> ', event);
        console.log('showSystemError.reset cmp --> ', cmp);
     },

     processContractReset: function (component, event, helper,selectedListCont) {
        //debugger;
        var selectedListCont = selectedListCont;       

        console.log('processContractReset.selectedListCont = ', selectedListCont);
        
        //call Apex
        var action = component.get("c.resetContractIncrease");
        action.setParams({
            contractList: selectedListCont        
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.toastdMessage("Réinitialiser", response.getReturnValue().message, response.getReturnValue().toastMode);
                //Fire event to update the list
                //we must refresh contract List too to refresh related Amendements :(
                    var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");  
                    appEventCont.setParams({
                    "isCancel": false,
                    "actionFromContrat": component.get('v.actionFromContrat')     
                });
                appEventCont.fire();
                component.destroy();             
            } else {
                let errorMsg = response.getError()[0];
                console.log('Erreur rencontrée pendant la mise à jours veuillez contacter l\'administrateur. Erreur :' + errorMsg);
            }
        });
        $A.enqueueAction(action);

    },

    processAmendmentReset: function (component, event, helper,selectedListAvenant) {
        //debugger;
        var selectedListAvenant = selectedListAvenant;       

        console.log('processAmendmentReset.selectedListAvenant = ', selectedListAvenant);
        
        //call Apex
        var action = component.get("c.resetAmendmentIncrease");
        action.setParams({
            amendmentList: selectedListAvenant        
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                //update contract
                let contractPartionLists = component.get("v.contractPartionLists");
                for (let i = 0; i < contractPartionLists.length; i++) {
                    helper.processContractReset(component, event, helper, contractPartionLists[i]);
                    console.log('contractPartionLists current processed item lengnth ', contractPartionLists[i].length);
                  } 
                helper.toastdMessage("Réinitialiser",'La mise à jour a bien été effectuée', response.getReturnValue().toastMode);
                var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                appEvent.setParams({
                    "isCancel": false
                });
                
                 appEvent.fire();
                //we must refresh contract List too to refresh related Amendements :(
                var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                appEventCont.setParams({
                    "isCancel": false,
                    "isAvenantUpdate": true,
                    "actionFromContrat": component.get('v.actionFromContrat')
                });
                appEventCont.fire();
                component.destroy();             
            } else {
                let errorMsg = response.getError()[0];
                
                console.log('response.getError()', response.getError());
                console.log('Erreur rencontrée pendant la mise à jours veuillez contacter l\'administrateur. Erreur :' + errorMsg);
            }
        });
        $A.enqueueAction(action);

    },

    processCallBack: function (component, event, helper,selectedListCont,selectedListAvenant) {
        //debugger;
        var selectedListCont = selectedListCont;
        var selectedListAvenant = selectedListAvenant;

        console.log('increaseContractFaf.selectedListCont = ', selectedListCont);
        
        //call Apex
        var action = component.get("c.resetIncrease");
        action.setParams({
            contractList: selectedListCont,
            amendmentList: selectedListAvenant
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.toastdMessage("Réinitialiser", response.getReturnValue().message, response.getReturnValue().toastMode);
                //Fire event to update the list
                //we must refresh contract List too to refresh related Amendements :(
                    var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");  
                    appEventCont.setParams({
                    "isCancel": false,
                    "actionFromContrat": component.get('v.actionFromContrat')
                });
                appEventCont.fire();
                component.destroy();             
            } else {
                let errorMsg = response.getError()[0];
                console.log('Erreur rencontrée pendant la mise à jours veuillez contacter l\'administrateur. Erreur :' + errorMsg);
            }
        });
        $A.enqueueAction(action);

    },
    
    recursive_am: function(component, event, helper, list, position) {
        //call Apex
        // limited to 50%, 
        component.set("v.displayProgress", true);
        component.set("v.message", "Mise à jour des avenants en cours...");
        if(list.length > 0) {
            component.set('v.progressValue', parseInt(((50/(list.length-1))*position)));
        } else {
            component.set('v.progressValue', parseInt(0));
        }
        
        var action = component.get("c.resetAmendmentIncrease");
        action.setParams({
            amendmentList: list[position]        
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                if(position+1 <= list.length-1) {
                    helper.recursive_am(component, event, helper, list, position+1);
                } else {
                    console.log('contract')
                    var selectedListCont = component.get("v.selectedListCont");

    				var contractPartionLists = selectedListCont.length > 100 ? helper.doListPartition(selectedListCont, 100) : helper.doListPartition(selectedListCont, selectedListCont.length);
                    console.log(contractPartionLists)
                    helper.recursive_c(component, event, helper, contractPartionLists, 0);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    recursive_c: function(component, event, helper, list, position) {
        //call Apex
        
        component.set("v.message", "Mise à jour des contrats en cours...");
        if(list.length > 1) {
            component.set('v.progressValue', parseInt(((50/(list.length-1))*position)+50));
        } else {
            component.set('v.progressValue', parseInt(50));
        }
        
        //
        var action = component.get("c.resetContractIncrease");
        action.setParams({
            contractList: list[position]        
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                if(position+1 <= list.length-1) {
                    console.log('resC', position)
                    helper.recursive_c(component, event, helper, list, position+1);
                } else {
                    component.set('v.progressValue', parseInt(100));
                    console.log('done')
                   helper.toastdMessage("Réinitialiser",'La mise à jour a bien été effectuée', response.getReturnValue().toastMode);
                    var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                    appEvent.setParams({
                        "isCancel": false
                    });
                    
                     appEvent.fire();
                    //we must refresh contract List too to refresh related Amendements :(
                    var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                    appEventCont.setParams({
                        "isCancel": false,
                        "isAvenantUpdate": true,
                        "actionFromContrat": component.get('v.actionFromContrat')
                    });
                    appEventCont.fire();
                    component.destroy();     
                }
            } else {
                let errorMsg = response.getError()[0];
                
                console.log('response.getError()', response.getError());
                console.log('Erreur rencontrée pendant la mise à jours veuillez contacter l\'administrateur. Erreur :' + errorMsg);
            }
        });
        $A.enqueueAction(action);
    },

    /**
 * Returns an array with arrays of the given size.
 *
 * @param myArray {Array} array to split
 * @param chunk_size {Integer} Size of every group
 */
    doListPartition : function (myArray, chunk_size){    
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];
    
    for (index = 0; index < arrayLength; index += chunk_size) {
       let  myChunk = myArray.slice(index, index+chunk_size);
        // Do something if you want with the group
        tempArray.push(myChunk);
    }

    return tempArray;
}
})