({
    toastdMessage: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },

    faceFaceProcess: function (component, event, helper,selectedListCont,selectedListAvenant,reason) {
        var selectedListCont = selectedListCont;
        var selectedListAvenant = selectedListAvenant;
        var reason = reason; 

        console.log('increaseContractFaf.selectedListCont = ', selectedListCont);
        console.log('increaseContractFaf.reason = ', reason);

        //call Apex
        var action = component.get("c.increaseContractFaf");
        action.setParams({
            contractList: selectedListCont,
            reason: reason,
            avenantList: selectedListAvenant
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.toastdMessage("Augmentation face à face", response.getReturnValue().message, response.getReturnValue().toastMode);
                //Fire event to update the list
                //we must refresh contract List too to refresh related Amendements :(
                var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif");
                appEvent.setParams({
                    "isCancel": false
                });
                //we must refresh contract List too to refresh related Amendements :(
                var appEventCont = $A.get("e.c:LE_ComponentRefresh_AugTarif");
                appEventCont.setParams({
                    "isCancel": false,
                    "isAvenantUpdate": true,
                    "actionFromContrat": component.get('v.actionFromContrat')
                });
                appEvent.fire();
                appEventCont.fire();
                component.destroy();             
            } else {
                console.log('Erreur rencontrée pendant la mise à jours veuillez contacter l\'administrateur' + state);
            }
        });
        $A.enqueueAction(action);

    }
})