({  
    closeModal: function (component, event, helper) {
        // when a component is dynamically created in lightning, we use destroy() method to destroy it.
        var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
        appEvent.setParams({
           "isCancel": true,
           "actionFromContrat": component.get('v.actionFromContrat')                            
        });
 
          appEvent.fire();
        component.destroy();
    },
    
    faceFace : function(component, event, helper) {
        var selectedListCont = component.get("v.selectedListCont");
        var selectedListAvenant = component.get("v.selectedListAvenant");
        var reason = component.find("inputReason").get("v.value"); 
        helper.faceFaceProcess(component, event, helper,selectedListCont,selectedListAvenant,reason);
    },

    fireRefreshEvent : function(component, event, helper) {
        // Get the  event by using the
        // name value from aura:registerEvent
        //debugger;
      //  var appEvent = $A.get("e.c:LE_AvenantListUpdate_AugTarif"); 
      var appEvent = $A.get("e.c:LE_ContractsList_AugTarif");   
       appEvent.setParams({
          "isCancel": false                            
       });

         appEvent.fire();
       
    }
})