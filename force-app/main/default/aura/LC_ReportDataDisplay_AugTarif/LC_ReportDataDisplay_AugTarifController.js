({
    doInit : function(component, event, helper) {
        console.log('Do Init')
    },

    afterScriptsLoaded : function(component, event, helper) {
        console.log('Component loaded');
    },

    generateChart : function(component, event, helper) {        
        let chartId = 'chart0';
        let chartType = 'column';
        let chartTitle ='';
        let chartDataN =  [parseInt(component.get("v.data.forecastedCaN"))];
        let chartDataN1 =  [parseInt(component.get("v.data.forecastedCaNextYear"))];
        let chartLabels = ['', ''];
        let xAxisTilte ='';
        let yAxisTilte ='';
        setTimeout(function() {
            helper.columnchart(component, chartId, chartType, chartTitle, chartDataN, chartDataN1, chartLabels, xAxisTilte, yAxisTilte)
        }, 1000);
    },

     // function automatic called by aura:waiting event  
     showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    }
})