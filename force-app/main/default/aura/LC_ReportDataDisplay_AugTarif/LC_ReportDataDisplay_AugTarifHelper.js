({
    columnchart: function (component,chartId, chartType, chartTitle, chartDataN, chartDataN1, chartLabels, xAxisTilte, yAxisTilte) {
      
        try{
            var labs = [];
            chartLabels.forEach(function (lab) {
                labs.push(lab);
            });

            //debugger;
            console.log('cmp dmp', component.find(chartId))
            new Highcharts.Chart({
                chart: {
                    renderTo: component.find(chartId).getElement(),
                    type: chartType,
                    styledMode: true,
                    height: 150,
                    width : 300
                },
                events: { //Load the total
                    load: function (event) {
                        if (displayTotal) {
                            var total = 0;
                            for (var i = 0, len = this.series[0].yData.length; i < len; i++) {
                                total += this.series[0].yData[i];
                            }
                            var text = this.renderer.text(
                                '<b>Total: ' + total + '</b>',
                                this.plotLeft,
                                this.plotTop
                            ).attr({
                                zIndex: 5
                            }).add()
                        }
                        var chart = this;
                        if (this.series[0] && this.series[0].yData == 0) {//If no data to display in chart
                            var r = Math.min(chart.plotWidth / 2, chart.plotHeight / 2),
                                y = chart.plotHeight / 2 + chart.plotTop,
                                x = chart.plotWidth / 2 + chart.plotLeft;
                            /*   chart.pieOutline = chart.renderer.circle(x, y, r).attr({
                                fill: '#ffffff',
                                stroke: '#7cb5ec',
                                    'stroke-width': 1,
                                    
                            }).add(); */
                            chart.renderer.text('Aucune donnée à afficher ', 110, y)
                                .css({
                                    color: '#4572A7',
                                    fontSize: '12pt'
                                }).add();
                        }
                    }
                },
                
                title: {
                    text: ''//chartTitle
                },
                subtitle: {
                    text: chartTitle
                },
                xAxis: {             
                    categories: labs,
                    crosshair: true
                },
                yAxis: {
                    // type: 'linear',
                    min: 0,
                    title:
                    {
                        text: yAxisTilte
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            //fontWeight: 'bold',
                            color: ( // theme
                                Highcharts.defaultOptions.title.style &&
                                Highcharts.defaultOptions.title.style.color
                            ) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: 0,
                    verticalAlign: 'bottom',
                    y: 10,
                    floating: true,
                    backgroundColor:
                        Highcharts.defaultOptions.legend.backgroundColor || 'white',
                    borderColor: '#CCC',
                    borderWidth: 0,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.2f} €</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.1,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'CA N projeté',
                    data: chartDataN,
                    maxPointWidth: 40
                }, {
                    name: 'CA N+1 projeté',
                    data: chartDataN1,
                    maxPointWidth: 40
                }]

            });
        } catch(e) {
            console.log(e)
        }

    },
})