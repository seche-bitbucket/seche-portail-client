({
       closeModal: function (component, event, helper) {
        // when a component is dynamically created in lightning, we use destroy() method to destroy it.
          //Fire Event to update List Contracts
          var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
          appEvent.setParams({
              "isCancel": true,
              "actionFromContrat": component.get('v.actionFromContrat')                 
          });
          appEvent.fire();

        component.destroy();
    },

    associate: function (component, event, helper) {
        //debugger;
        var selectedListCont = component.get("v.selectedListCont");

        console.log('associate.selectedListCont = ', selectedListCont);

        //call Apex
        var action = component.get("c.regroupContracts");
        action.setParams({
            contractList: selectedListCont
        });

        //configure action handler
        action.setCallback(this, function (response) {
            // debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.toastdMessage("Augmentation tarifaire", response.getReturnValue().message,  response.getReturnValue().toastMode);
                //Fire Event to update List Contracts
                var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");   
                appEvent.setParams({
                    "isCancel": false,
                    "actionFromContrat": component.get('v.actionFromContrat')                           
                });
                appEvent.fire();

                component.destroy();

            } else {
                console.log('Erreur rencontrée pendant la mise à jours veuillez contacter l\'administrateur' + state);
            }
        });
        $A.enqueueAction(action);

    },

    fireRefreshEvent : function(component, event, helper) {
        // Get the  event by using the
        // name value from aura:registerEvent
        //debugger;
        var appEvent = $A.get("e.c:LE_ComponentRefresh_AugTarif");       
        appEvent.setParams({
            "actionFromContrat": component.get('v.actionFromContrat')                           
        });
        appEvent.fire();
    }
    
})