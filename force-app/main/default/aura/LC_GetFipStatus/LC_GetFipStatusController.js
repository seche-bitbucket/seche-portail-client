({
    doInit : function(component, event, helper) {
        helper.loadFip(component, event, helper);
    },
    requestGetStatus: function(component, event, helper) {
        helper.getStatus(component, event, helper);
    },
    cancelRequest: function(component, event, helper) {
        helper.cancelCall(component, event, helper);
    }
})