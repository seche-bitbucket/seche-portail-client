({
    
    // function call on component Load
    doInit: function(component, event, helper) {
        
        // Get a reference to the getPriceBookEntries() function defined in the Apex controller
        var action = component.get("c.getPriceBookEntries");
        action.setParams({
            "wasteInstanceId": component.get("v.recordId")
        });
        // Register the callback function
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.PBEntries", response.getReturnValue());
            }
        });
        // Invoke the service
        $A.enqueueAction(action);
        
    },
    
    // function for save the Records 
    Save: function(component, event, helper) {
        // first call the helper function in if block which will return true or false.
        // this helper function check the "first Name" will not be blank on each row.
        /* if (helper.validateRequired(component, event)) {
            // call the apex class method for save the Contact List
            // with pass the contact List attribute to method param.  
            var action = component.get("c.saveContacts");
            action.setParams({
                "ListContact": component.get("v.contactList")
            });
            // set call back 
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    // if response if success then reset/blank the 'contactList' Attribute 
                    // and call the common helper method for create a default Object Data to Contact List 
                    component.set("v.contactList", []);
                    helper.createObjectData(component, event);
                    alert('record Save');
                }
            });
            // enqueue the server side action  
            $A.enqueueAction(action);
        }*/
        var qls = JSON.stringify(component.get("v.PBEntries"));
        var genericQLS = JSON.stringify(component.get("v.AddedQuoteLines"));
        console.log('On sauvegarde'+ qls);
        var action = component.get("c.saveQuoteLines");
        action.setParams({
            "qls" : qls,
            "genericQls" : genericQLS,
            "wasteInstanceId" : component.get("v.recordId")
        });
        // set call back 
        action.setCallback(this, function(response) {
            console.log('Fire');
            var state = response.getState();
            if (state === "SUCCESS") {
                // if response if success then reset/blank the 'contactList' Attribute 
                // and call the common helper method for create a default Object Data to Contact List 
                component.set("v.PBEntries", response.getReturnValue());
                component.set("v.AddedQuoteLines", []);
                alert('Réparations misent à jour');
            }else{
                var errors = response.getError();
                alert(''+errors[0].message);
            }
        });
        // enqueue the server side action  
        $A.enqueueAction(action);
    },
    
    AddNewRow : function(component, event, helper){
        // fire the AddNewRowEvt Lightning Event 
        //helper.createObjectData(component, event);
    },
    
    
    // function for delete the row 
    removeRow: function(component, event, helper) {
        console.log('Called : ');
        // get the selected row Index for delete, from Lightning Event Attribute  
        //var selectedRow = event.currentTarget;
        
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.record;
        console.log('Index : '+index);
        // get the all List (contactList attribute) and remove the Object Element Using splice method    
        var AllRowsList = component.get("v.AddedQuoteLines");
        AllRowsList.splice(index, 1);
        // set the contactList after remove selected row element  
        component.set("v.AddedQuoteLines", AllRowsList);
    },
    
})