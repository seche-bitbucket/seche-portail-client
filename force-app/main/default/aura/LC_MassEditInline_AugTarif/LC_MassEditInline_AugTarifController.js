({
    doInit: function (component, event, helper) {        
        var sObjectName = component.get("v.sObjectName");
        console.log('LC_MassEditInline_AugTarif.doInit.sObjectName---- = ',sObjectName)
        if(sObjectName == 'Contract__c'){
            component.set('v.columns', [
                {label: 'Id', fieldName: 'Id', type: 'text' , editable: true},
                {label: 'Name', fieldName: 'Name', type: 'text' ,editable: true},
                {label: 'Type de contrat', fieldName: 'ContractType__c', type: 'texte' ,editable: true},
                {label: 'Type de convention', fieldName: 'Type_de_convention__c', type: 'text' ,editable: true},
                {label: 'Statut', fieldName: 'Status__c', type: 'texte' ,editable: true},
                {label: 'Filiale', fieldName: 'Filiale__c', type: 'text' ,editable: true},
                {label: 'Filiere', fieldName: 'Filiere__c', type: 'text' ,editable: true},
                {label: 'Vendeur associé', fieldName: 'AssociatedSalesRep__c', type: 'texte' ,editable: true } 
            ]);   
        }else if(sObjectName == 'Amendment__c'){
            component.set('v.columns', [
                {label: 'Id', fieldName: 'Id', type: 'text' , editable: true},
                {label: 'Name', fieldName: 'Name', type: 'text' ,editable: true},
                {label: 'Contrat', fieldName: 'Contrat2__r.Name', type: 'texte' ,editable: true},
                {label: 'Nature', fieldName: 'Nature__c', type: 'text' ,editable: true},
                {label: 'Tarif N', fieldName: 'PriceCurrentYear__c', type: 'texte' ,editable: true},
                {label: 'Tarif N+1', fieldName: 'PriceNextYear__c', type: 'text' ,editable: true},
                {label: 'Filiale', fieldName: 'Filiale__c', type: 'text' ,editable: true},
                {label: 'Filiere', fieldName: 'Filiere__c', type: 'text' ,editable: true},
                {label: 'Commercial associé', fieldName: 'Commercial_Associe__c', type: 'texte' ,editable: true } 
            ]);   
        }else{
            component.set('v.columns', [
                {label: 'Id', fieldName: 'Id', type: 'text' , editable: true},
                {label: 'Name', fieldName: 'Name', type: 'text' ,editable: true}                
            ]);   
        }
    },
    handleSaveEdition: function (component, event, helper) {
        var draftValues = event.getParam('draftValues');
        console.log(draftValues);
        var action = component.get("c.updateAccount");
        action.setParams({"acc" : draftValues});
        action.setCallback(this, function(response) {
            var state = response.getState();
            $A.get('e.force:refreshView').fire();
            
        });
        $A.enqueueAction(action);
        
    },

    handleSaveEdition: function (component, event, helper) {
        var draftValues = event.getParam('draftValues');
        console.log('handleSaveEdition.draftValues >>>', draftValues);
        var action = component.get("c.updateContract");
        action.setParams({"lstCont" : draftValues});
        action.setCallback(this, function(response) {
            var state = response.getState();
            $A.get('e.force:refreshView').fire();
            
        });
        $A.enqueueAction(action);
        
    }
})