({
    helperSend : function(component,recordId) {       
        // call the server side controller method 	
        var action = component.get("c.setChangeStatut");
        action.setParams({
            'recordId': recordId,
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();            
            var toastEvent = $A.get("e.force:showToast");            
            if (state === "SUCCESS" || state === "Created") {
                toastEvent.setParams({
                    "type" : "success",
                    "title": "Réussie",
                    "message": "Votre FIP a bien été envoyé."
                });
            }
            else if (state === "ERROR") {
                toastEvent.setParams({
                    "type" : "Error",
                    "title": "Erreur",
                    "message": "Votre FIP n\'a pas été envoyé. "
                });        
            }
            toastEvent.fire();
        });
        $A.enqueueAction(action);        
    },
})