({

    doInit: function(component, event, helper) {
        helper.setContent(component, event, helper);
    },
    
    sendMail: function(component, event, helper) {
        helper.sendHelper(component, event, helper);
    }, 

    sendPkg: function(component, event, helper) {
        helper.sendHelper(component, event, helper);
    }, 
 
    closeModal : function(component, event, helper) {
        // when a component is dynamically created in lightning, we use destroy() method to destroy it.
        $A.get("e.force:closeQuickAction").fire();
      
    },

    onChange : function(component, event, helper) {
        helper.handleOnChange(component, event, helper);
    },

    handleUploadFinished : function(component, event, helper) {
 
        // Get the list of uploaded files
        var uploadedFiles = event.getParam("files");
        console.log(uploadedFiles);

        let lst = component.get('v.notToSendViaConnective');

        uploadedFiles.forEach(file => {
            console.log(file.documentId);
            lst.push(file.documentId);
        });

        component.set('v.notToSendViaConnective', lst);
 
    },

    deleteFile: function(component, event, helper) {
        helper.deleteFileById(component, event, helper, event.getSource().get('v.name'));
    }
    
})