({

    setContent: function (component, event,helper) {

        var recordId = component.get('v.recordId');
        console.log('id => '+recordId);
        // call the server side controller method 	
        var action = component.get("c.genericGetEmailPrev");
        action.setParams({
            'objId': recordId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            
            console.log(state);
            
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();   
                
                console.log(storeResponse.fileId);
                
                if(storeResponse.isEmailAddressBounced === 'isEmailAddressBounced'){
                    //reset the quote status
                    component.set("v.initialStatus", storeResponse.initialStatus);
                   // helper.resetHelper(component, event, helper);
                   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "mode": 'sticky',
                        "title": 'Envoi pour signature',
                        "message": storeResponse.message,
                        "type": 'warning'
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();

                }else{
                    component.set("v.message", storeResponse.message);

                    component.set("v.isShowForm", storeResponse.isShowForm);
                    
                    if (storeResponse.isShowForm === 'true') {
                        component.set("v.contactId", storeResponse.contactId);
                        component.set("v.contactName", storeResponse.contactName);
                        component.set("v.toAddresses", storeResponse.toAddresses);
                        component.set("v.fromAddresses", storeResponse.replyTo);
                        component.set("v.cciEmail", storeResponse.ccaddresses);
                        component.set("v.senderDisplayName", storeResponse.senderDisplayName);
                        component.set("v.subject", storeResponse.subject);
                        component.set("v.body", storeResponse.body);
                        component.set("v.files", storeResponse.fileId);
                        component.set("v.initialStatus", storeResponse.status);
    
                    } else {
                        $A.get("e.force:closeQuickAction").fire();
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "mode": 'sticky',
                            "title": 'Envoi pour signature',
                            "message": storeResponse.message,
                            "type": 'warning'
                        });
                        toastEvent.fire();
    
                    }
                }
                // if state of server response is comes "SUCCESS",
                // display the success message box by set mailStatus attribute to true         
              
            } else {
                console.log('error ---> setContent');
            }

        });
        $A.enqueueAction(action);
    },

    sendHelper: function (component, event, helper) {
        //set spinner    
        component.set('v.loaded', !component.get('v.loaded'));

        component.set('v.isCloseModal', false);
        var loaded = component.get('v.loaded');
        var recordId = component.get('v.recordId');
        var contactId = component.get('v.contactId');
        var contactName = component.get('v.contactName');
        var fromAddresses = component.find('fromAddresses').get("v.value");
        var toAddresses = component.find('toAddresses').get("v.value");
        var cciEmail = component.find('cciEmail').get("v.value");
        var senderDisplayName = component.get("v.senderDisplayName");
        var subject = component.find('subject').get("v.value");
        var body = component.find('body').get("v.value");
        var files = component.get('v.selectedFiles');
        var filesToLink = component.get('v.notToSendViaConnective');

        // call the server side controller method 	
        var action = component.get("c.genericCreatePackage");
		
        var cciAddressValid = component.find('cciEmail').get('v.validity').valid;
        if (cciAddressValid) {
        
            // set the 3 params to sendMailMethod method   
            action.setParams({
                'signerId': contactId,
                'contactName': contactName,
                'fromAddresses': fromAddresses,
                'toAddresses': toAddresses,
                'cciEmail': cciEmail,
                'senderDisplayName': senderDisplayName,
                'subject': subject,
                'body': body,
                'objId': recordId,
                'filesIds': (files.length > 0) ? fiels : component.get('v.files')[0],
                'filesToAttach': filesToLink
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    component.set('v.loaded', !component.get('v.loaded'));
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "mode": 'sticky',
                        "title": 'Envoi pour signature',
                        "message": storeResponse.message,
                        "type": storeResponse.showToastMode
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get("e.force:refreshView").fire();
                } else {
                    console.log('error ---> sendHelper');
                }
    
            });
            $A.enqueueAction(action);
            
            
        } else {
        	component.set('v.loaded', !component.get('v.loaded'));
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "mode": 'sticky',
                "title": $A.get("$Label.c.Warning_invalid_fields_title"),
                "message": $A.get("$Label.c.Warning_invalid_fields"),
                "type": 'warning'
            });
            toastEvent.fire();
		}
    },

    handleOnChange: function(component, event, helper) {
        const inputValue = event.getSource().get('v.value');
        const state = event.getSource().get('v.checked');
        console.log(inputValue+': ', state);
        let lstIds = component.get('v.selectedFiles');
        console.log('before: ', lstIds);
        if(lstIds.includes(inputValue) && !state) {
            lstIds = lstIds.filter(function(item) {
                return item !== inputValue
            });
        } else if(!lstIds.includes(inputValue) && state) {
            lstIds.push(inputValue);
        }
        console.log('after: ', lstIds);
        component.set('v.selectedFiles', lstIds);
    },

    deleteFileById: function(component, event, helper, id) {
        // call the server side controller method 	
        var action = component.get("c.deleteDocument");
        action.setParams({
            'Id': id
        });
        action.setCallback(this, function (res) {
            var state = res.getState();
            
            if (state === "SUCCESS") {
                let response = res.getReturnValue();
                if(response.deleted == 'OK')  {
                    let lst = component.get('v.notToSendViaConnective');
                    lst = lst.filter(function(item) {
                        return item !== id
                    });
                    component.set('v.notToSendViaConnective', lst);
                } else {
                    console.log('error ---> not dlelted');
                }
        
            } else {
                console.log('error ---> setContent');
            }

        });
        $A.enqueueAction(action);
    }

})