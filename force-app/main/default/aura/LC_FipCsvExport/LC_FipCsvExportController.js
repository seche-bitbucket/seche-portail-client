({
    // ## function call on component load  
    loadFipList: function(component, event, helper){
       helper.onLoad(component, event);
    },
    
    
    // ## function call on Click on the "Download As CSV" Button. 
    downloadCsv : function(component,event,helper){
        
        // get the Records [contact] list from 'ListOfContact' attribute 
        var stockData = component.get("v.ListOfFip");
        console.log('SUCCESS stockData: ',stockData);  
        
        // call the helper function which "return" the CSV data as a String   
        var csv = helper.convertArrayOfObjectsToCSV(component,stockData);        
       
        if ( csv == null){return;} 

        //Date time 
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date+' '+time;
        
        // ####--code for create a temp. <a> html tag [link tag] for download the CSV file--####     
       
	     var hiddenElement = document.createElement('a');
          hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
          hiddenElement.target = '_blank'; // 
          hiddenElement.download ='FIP - '+dateTime+'.csv'; // CSV file Name* you can change it.[only name not .csv]            
          document.body.appendChild(hiddenElement); // Required for FireFox browser
    	  hiddenElement.click(); // using click() js function to download csv file
        }, 

        backToList: function(component, event, helper){
          helper.gotoList(component, event);
       }
 })