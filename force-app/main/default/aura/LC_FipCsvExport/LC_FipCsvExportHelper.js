({
    onLoad: function(component, event) {
       //call apex class method
      
       //Convert the list passed from the visualforce to remove the [  ]
       var ids =  component.get("v.ListOfFip").replace(/\[/g, "").replace(/\]/g, "").split(', ');
       console.log('onLoad start ids: ', ids);  
       var action = component.get('c.fetchFip');
       action.setParams({       
        selectedFipIds : ids
    });
       action.setCallback(this, function(response){
          //store state of response
          var state = response.getState();
          if (state === "SUCCESS") {
             //set response value in ListOfContact attribute on component.
             component.set('v.ListOfFip', response.getReturnValue());
             console.log('SUCCESS ListOfFip: ',response.getReturnValue());  
          }
       });
       $A.enqueueAction(action);
    },
      
    convertToCsv: function(component, event) {
        //call apex class method
        //var ids =  component.get("v.ListOfFip").replace(/\[/g, "").replace(/\]/g, "").split(', ');
       console.log('convertToCsv start : ');  
       component.set( 'v.showSpinner', true );
        //Convert the list passed from the visualforce to remove the [  ]
        var fips =  component.get("v.ListOfFip");      
        var action = component.get('c.generateCSV');
        action.setParams({       
            fips : fips
     });
        action.setCallback(this, function(response){
            component.set( 'v.showSpinner', false );
           //store state of response
           var state = response.getState();
           if (state === "SUCCESS") {
              //set response value in ListOfContact attribute on component.             
              console.log('SUCCESS convertToCsv ListOfFip: ',response.getReturnValue()); 
              component.set('v.fipCsv', response.getReturnValue()); 
              console.log('SUCCESS fipCsv--: ',component.get('v.fipCsv')); 
           }
        });
        $A.enqueueAction(action);
     },
       
    convertArrayOfObjectsToCSV : function(component,objectRecords){
        
         // declare variables
         var csvStringResult, counter, keys, columnDivider, lineDivider;
        
         // check if "objectRecords" parameter is null, then return from function
         if (objectRecords == null || !objectRecords.length) {
             return null;
          }
         // store ,[comma] in columnDivider variabel for sparate CSV values and 
         // for start next line use '\n' [new line] in lineDivider varaible  
         columnDivider = ',';
         lineDivider =  '\n';
  
         // in the keys valirable store fields API Names as a key 
         // this labels use in CSV file header         
         keys = ['Id', 'FIPType__c', 'PreviousPACNumber__c', 'ContractRoot__c', 'ChargedX3Code__c', 'AdressCode__c', 'WasteName__c', 'WasteNamingCode__c', 'AnnualQuantity__c', 'AsbestosType__c', 'PackagingType__c', 'UNCode__c', 'Class__c', 'PackagingGroup__c','Collector__r.Name', 'Collector__r.BillingStreet', 'Collector__r.BillingPostalCode', 'Collector__r.BillingCity', 'CreatedBy.FirstName', 'CreatedBy.LastName', 'CreatedBy.Phone', 'Collector__r.SIRET_Number__c', 'ProductorBusinessName__c', 'ProductorCompanyIdentificationSystem__c', 'ProductorAdress1__c', 'ProductorAdress2__c', 'ProductorAdress3__c', 'ProductorPostalCode__c', 'ProductorCity__c', 'ConstructionSiteAdress__c', 'FrenchTaxeID__c', 'ChargedX3Code__c', 'ChargedX3AddressCode__c', 'ProductorX3Code__c', 'ProductorX3AddressCode__c', 'AskerCodeX3__c', 'AskerX3AddressCode__c', 'TECH_Subsidiary_Key__c', 'AcceptedBy__r.Trigramme__c', 'PlateformeTransit__c', 'Exutoire__c', 'ContractNumber__c'];
        
         csvStringResult = '';
         csvStringResult += keys.join(columnDivider);
         csvStringResult += lineDivider;
         
         for(var i=0; i < objectRecords.length; i++){   
             counter = 0;
             var currentLine = objectRecords[i] ; 
             let rec ;
              for(var sTempkey in keys) {
                 var skey = keys[sTempkey] ;  //exple :Id
                 
               // add , [comma] after every String value,. [except first]
                   if(counter > 0){ 
                       csvStringResult += columnDivider; 
                    }                     
                   
                    if(skey.includes('__r')){//Handle relation fields
                     var fields = skey.split('.');
                        if(currentLine[fields[0]]){
                           rec =currentLine[fields[0]][fields[1]];
                        }else{
                           rec = undefined;
                        }
                                               
                    }else{
                         rec = currentLine[skey];
                    }
                    fields = [];
                    if(rec===undefined){
                     rec = null;
                    }   
                csvStringResult += '"'+rec+'"'; 
                
                counter++;
  
             } // inner for loop close 
              csvStringResult += lineDivider;
           }// outer main for loop close 
        
        // return the CSV formate String 
         return csvStringResult;        
     },

     gotoList : function (component, event, helper) {
      var action = component.get("c.getListViews");
      action.setCallback(this, function(response){
          var state = response.getState();
          if (state === "SUCCESS") {
              var listviews = response.getReturnValue();
              var navEvent = $A.get("e.force:navigateToList");
             /* navEvent.setParams({
                  "listViewId": listviews[0].Id,
                  "listViewName": null,
                  "scope": "FIP_FIP__c"
              });
              navEvent.fire(); */
              window.location = "/lightning/o/FIP_FIP__c/list?filterName=" + listviews[0].Id
          }
      });
      $A.enqueueAction(action);
      
  }
 })