@isTest
public class SendSignatureQueueableTest {
    @isTest static void sendCreatedFlowTest(){
        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '01000';
        a.NAF_Number__c = '1234A';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        //c.Salesman__c = director.Id;
        c.Email = 'aaa@groupe-seche.com';
        c.Phone = '03 00 00 00 00';
        insert c;
        Opportunity opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        Quote q = new Quote();
        q.OpportunityId = opp.ID;
        q.Name = opp.Name;
        q.DateDevis__c = Date.today();
        q.QuoteCode__c=opp.Name;
        q.ContactId = opp.ContactName__c;
        q.Email = opp.ContactName__r.Email;
        q.Phone = opp.ContactName__r.Phone;
        q.Fax = opp.ContactName__r.Fax;
        q.TECH_UID__c='9001';
        q.BillingStreet = opp.Account.BillingStreet;
        q.BillingCity = opp.Account.BillingCity;
        q.BillingState = opp.Account.BillingState;
        q.BillingPostalCode = opp.Account.BillingPostalCode;
        q.BillingCountry = opp.Account.BillingCountry;
        q.TECH_UnknownSignerInformation__c='1=2=3=4=5';
        q.TECH_SendForSignature__c=true;
        insert q;
        Attachment att = new Attachment();
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        att.body=bodyBlob;
        att.Name = q.Name+'- Devis.Docx';	
        att.ParentId = q.Id;             
        insert att; 
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WS001_ConnectiveMock()); 
        WS001_ConnectiveMock.externalId=q.id;
        SendSignatureQueueable sss= new SendSignatureQueueable(q);
        sss.sendCreatedFlow(q.Id);
        q.TECH_UnknownSignerInformation__c=c.Id;
        update q;
        sss.sendCreatedFlow(q.Id);
        ID jobID = System.enqueueJob(sss);
        Test.stopTest(); 
        
    }
}