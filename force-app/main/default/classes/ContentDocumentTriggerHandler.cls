/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ContentVersionTriggerHandler
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ContentVersionTriggerHandler trigger
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 23-03-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
public with sharing class ContentDocumentTriggerHandler {
    public static void onBeforeDelete(Map<Id,ContentDocument> mapContentDocument,Map<Id,ContentDocument> mapOldContentDocument){
        blockContentDocumentQuote(mapOldContentDocument);  
    }

    /**
    * Returns Void
    * 
    * This method block the delete of a file for the Quote Locked in a approval process or when it is in the status final 
    *
    * @param  mapContentDocument Map<Id,ContentDocument>
    * @return void
    */
    public static void blockContentDocumentQuote(Map<Id,ContentDocument> mapContentDocument){
        List<ContentDocumentLink> lstCdl = [SELECT ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId IN:mapContentDocument.keySet()];
        if(lstCdl.size()>0){
            Map<Id,ContentDocument> mapIdQuoteContentDocument = new Map<Id,ContentDocument>();
            for(ContentDocumentLink cv : lstCdl){
                mapIdQuoteContentDocument.put(cv.LinkedEntityId,mapContentDocument.get(cv.ContentDocumentId));
            }
            Map<Id,Quote> mapQuote = new Map<Id,Quote>([SELECT Id, TECH_IsLocked__c FROM Quote WHERE TECH_IsLocked__c=true AND Id IN:mapIdQuoteContentDocument.keySet()]);
            if(mapQuote.size()>0){
                List<ContentDocument> listContentDocumentError = new List<ContentDocument>();
                for(Quote q : mapQuote.values()){
                    if(q!=null && q.TECH_IsLocked__c){
                        listContentDocumentError.add(mapIdQuoteContentDocument.get(q.Id));
                    }
                }
                if(listContentDocumentError.size()>0){
                    for(ContentDocument cv : listContentDocumentError){
                        cv.addError(Constants.STR_CVE_ERRORMESSAGE_QUOTE_LOCKED);
                    }
                }
            }
        }
    }
}