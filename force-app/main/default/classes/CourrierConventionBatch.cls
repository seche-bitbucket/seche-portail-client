global class CourrierConventionBatch implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {	
        //String quoteName = '%Convention '+ (System.Today().year() +1)+'%';
        String Nextyear = System.Label.YearN1;
        String quoteName = '%Convention '+Nextyear+'%';
        String query = 'SELECT Id, Name,CongaConventionID__c, CreatedDate,OpportunityId,Status,(Select Id From Attachments), (SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLinks) FROM Quote WHERE NAME Like \''+quoteName+'\' AND NAME Like \'%V1%\' AND Status NOT IN(\'Présenté\', \'Envoyé pour signature\', \'Signé\', \'Refusé\', \'Approuvé\')';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Quote> scope)
    {          
        
        Set<Id> quotesIds = new Set<Id>();
        List<ID> OpportunitiesIds=new List<Id>();
        for(Quote q : scope){
            quotesIds.add(q.Id);
            if(q.ContentDocumentLinks.size()<1){
                OpportunitiesIDs.add(q.OpportunityID);
            }
        }
        System.debug('OpportunitiesIDs : '+OpportunitiesIDs);   
        List<ConventionTemplateMapping__mdt> ConventionMapping =[SELECT Id, FilialeAndTypeConvention__c, TemplateName__c FROM ConventionTemplateMapping__mdt];
        Map<String,String> TemplateByFiliale=new Map<String,String>();
        System.debug('ConventionMapping : '+ConventionMapping);      

      
        for(ConventionTemplateMapping__mdt CTM:ConventionMapping){
            TemplateByFiliale.put(CTM.FilialeAndTypeConvention__c,CTM.TemplateName__c);
        }
        System.debug('TemplateByFiliale : '+TemplateByFiliale);   

        List<Contract__c> ContractsTogenerate=[SELECT Filiale__c,Status__c,Type_de_convention__c,Name,Account__c, Id,Opportunity__c,Account__r.Name
                                                   FROM Contract__c Where Opportunity__c IN: OpportunitiesIDs ORDER BY Account__c];
        Map<Id,String> OpportunityAndTemplateName=new Map<Id,String>();
        System.debug('ContractsToGenerate : '+ContractsTogenerate);       
        
        for(Contract__c c:ContractsTogenerate){
            String FilialeAndTemplate=c.Filiale__c+c.Type_de_convention__c;
           
            for(String t:TemplateByFiliale.keyset()){
                if(FilialeAndTemplate==t){
                    OpportunityAndTemplateName.put(c.Opportunity__c,TemplateByFiliale.get(t));
                }
            }
        }
        System.debug('OpportunityAndTemplateName :'+OpportunityAndTemplateName.Values() );


        List<APXTConga4__Conga_Template__c> Templates=[SELECT APXTConga4__Name__c, Id FROM APXTConga4__Conga_Template__c Where APXTConga4__Name__c IN:OpportunityAndTemplateName.Values()];
        System.debug('Templates :'+Templates );

        Map<ID,String> OppIdTemplateName=new Map<ID,String>();
        for(Id OppId:OpportunityAndTemplateName.keyset()){
            for(APXTConga4__Conga_Template__c template:Templates){
                if(OpportunityAndTemplateName.get(OppId)==template.APXTConga4__Name__c){
                    OppIdTemplateName.put(OppId,template.Id);
                }
            }
        }
        
        List<Quote> Quotes=[Select Id,OpportunityID,CongaConventionID__c FROM Quote WHERE OpportunityId IN:OppIdTemplateName.keyset() AND Id IN :quotesIds];
        for(Quote q:Quotes){
            for(ID oppid:OppIdTemplateName.keyset()){
                if(oppid==q.OpportunityID){
                    q.CongaConventionID__c=OppIdTemplateName.get(oppId);
                }
            }
        }
        Update Quotes;
        for(Quote q : Quotes){
            q.TECH_CongaAutomatedSending__c=true;
        }
        Update Quotes;
        /*Integer year=System.today().Year()+1; 
        string inVar='Courrier '+year;
        string tempInput = '%' + inVar + '%';
        List<Quote> DevisCourrier=[Select Id, Name, CreatedDate,OpportunityId,Status,(Select Id From Attachments) 
                                   FROM Quote WHERE NAME Like :tempInput and status!='Présenté'];
        List<ID> OpportunitiesIds=new List<Id>();
        for(Quote q : DevisCourrier){
            if(q.Attachments.size()<1){
                q.TECH_CongaAutomatedSending__c=true;
            }
        }
		Update DevisCourrier;
		*/  
   
    }
    global void finish(Database.BatchableContext BC)
    {   
    }
}