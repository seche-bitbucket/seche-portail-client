@isTest
private class GenerateVisitReportFromEventTest {
    
    static testMethod void testGenerateVR()
    {        
        User u = [Select Id From User Limit 1];
        //RecordType rt = [Select Id From RecordType Where IsActive = true AND SobjectType = 'Account' Limit 1];
        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        //a.RecordTypeId = rt.Id;
        a.Producteur__c = false;
        a.BillingPostalCode = '01000';
        a.NAF_Number__c = '1234A';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Salesman__c = u.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '03 00 00 00 00';
        insert c;
        
        Event e = new Event();
        e.WhatId = a.Id;
        e.WhoId = c.Id;
        e.Subject = 'test title';
        e.OwnerId = u.Id;
        e.StartDateTime = System.today();
        e.EndDateTime = System.today();
        e.ActivityDate = System.today();
        e.ActivityDateTime = System.today();
        e.Compte__c = a.Id;
        insert e;
        
        // Cas nominal
        System.assert(!GenerateVisitReportFromEvent.requestVisitReport(e.id).equals('error1') && !GenerateVisitReportFromEvent.requestVisitReport(e.id).equals('error2'));
        
        // Cas d'erreur où l'évévement est déjà lié à un CR
        VisitReport__c vr = new VisitReport__c();
        vr.Name = 'test';
        vr.Account__c = a.Id;
        vr.Contact__c = c.Id;
        insert vr;
        e.Compte_rendu__c = vr.Id;
        update e;
        System.assert(GenerateVisitReportFromEvent.requestVisitReport(e.id).equals('error1'));
        
        // Cas d'erreur où le contact ou compte ne sont pas renseignés
        e.Compte_rendu__c = null;
        e.Compte__c = null;
        e.WhoId = null;
        update e;
        System.assert(GenerateVisitReportFromEvent.requestVisitReport(e.id).equals('error2'));
    }
}