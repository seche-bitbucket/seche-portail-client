/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-07-04
* @systemLayer    Utility         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Controller class for button that increases by percentage the amendments linked to the contracts selected
2018-07-04  Added changed so that the page has 4 inputs , 3 for every nature of amendments and one to update all types of amendments
2018-07-16  Fixed a small error with updating the amendments
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class VFC_ContractIncreaseByPercentage {
    public List<Contract__c> ContractList {get;set;}
    public Double inputValue{get;set;}
    public Double inputValueTraitement{get;set;}
    public Double inputValueTransport{get;set;}
    public Double inputValueConditionnement{get;set;}
    public Boolean NoValue{get;set;}
    public Boolean EntierHigh{get;set;}
    public Boolean EntierAuto{get;set;}
    public VFC_ContractIncreaseByPercentage(ApexPages.StandardSetController controller){
        this.EntierAuto=false;
        this.EntierHigh=false;
        // retrieve selected contracts and check if they are locked or not
        if(controller.getSelected().size()>=1){
            this.ContractList=[SELECT Id,RecordLocked__c FROM Contract__c WHERE ID IN:controller.getSelected()];
            this.NoValue=false;
            Boolean Locked=false;
            for(Contract__c c:this.ContractList){
                if(c.RecordLocked__c==true){
                    Locked=true;
                }
            }
            if(locked){
                this.Novalue=true;
                ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Le contrat sélectionné est soumis ou validé et ne peut pas être modifié');
                ApexPages.addMessage(errorMessage);
            }
        }else{
            this.Novalue=true;
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Veuillez sélectionner un contrat');
            ApexPages.addMessage(errorMessage);
        } 
    }
    public void changeAllInput(){
        if(this.inputValue!=null && this.inputValue!=0.0){
            this.inputValueTraitement=this.inputValue;
            this.inputValueTransport=this.inputValue;
            this.inputValueConditionnement=this.inputValue;
        }else{
            this.inputValue=null;
        }
    }
    public PageReference save(){
        // save the changes (if there is any) and adding the % value to the next year price in the amendments linked to the contracts selected
        PageReference returnPage=ApexPages.currentPage();
        List<Amendment__c> AmendmentToUpdate=[SELECT Nature__c,PriceCurrentYear__c,PriceNextYear__c FROM Amendment__c WHERE Contrat2__c IN:this.ContractList];
        List<Amendment__c> AmendmentToUpdate2=new List<Amendment__c>();
        if(this.inputValue!=null && this.inputValue>0){
            for(Amendment__c am: AmendmentToUpdate){
                if(am.PriceCurrentYear__c!=null){
                    am=addpercentage(am,this.inputValue);
                }
                am.ByPassContract__c=false;
                AmendmentToUpdate2.add(am);
            }
        }else{
            if((this.inputValueTraitement!=null && this.inputValueTraitement>0) 
               || (this.inputValueTransport!=null && this.inputValueTransport>0)
               || (this.inputValueConditionnement!=null && this.inputValueConditionnement>0) ){
                   for(Amendment__c am: AmendmentToUpdate){
                       if(am.Nature__c=='Traitement' && (this.inputValueTraitement!=null && this.inputValueTraitement>0)){
                           am=addpercentage(am,this.inputValueTraitement);
                           am.ByPassContract__c=false;
                       }
                       if(am.Nature__c=='Prestation' && (this.inputValueTransport!=null && this.inputValueTransport>0)){
                           am=addpercentage(am,this.inputValueTransport);
                           am.ByPassContract__c=false;
                       }
                       if(am.Nature__c=='Conditionnement' && (this.inputValueConditionnement!=null && this.inputValueConditionnement>0)){
                           am=addpercentage(am,this.inputValueConditionnement);
                           am.ByPassContract__c=false;
                       }
                       AmendmentToUpdate2.add(am);
                   }
               }else{
                   ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.WARNING,'Veuillez saisir une valeur positive');
                   ApexPages.addMessage(errorMessage);
               }
        }
        If(AmendmentToUpdate2.size()>0){
            Update AmendmentToUpdate2;
            ReturnPage= new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        }
        return ReturnPage;
    }
    public Amendment__c addpercentage(Amendment__c a,Double Value){
        a.PriceNextYear__c=a.PriceCurrentYear__c+a.PriceCurrentYear__c*Value*0.01;
        if(this.EntierHigh==true){
            a.PriceNextYear__c=a.PriceNextYear__c.round(System.RoundingMode.CEILING);
        }else if(this.EntierAuto==true){
            a.PriceNextYear__c=a.PriceNextYear__c.round();
        }
        return a;
    }
}