public class WST_ManageOpportunityElimController {
    
    private static ApexPages.StandardSetController ctrl;
    
    //List of WST selected, with opportunities, without opporuntunites
    private static List<WST_Waste_Statement__c> LinkedOppToUpdate = new List<WST_Waste_Statement__c>();
    private static List<WST_Waste_Statement__c> LinkedOppToCreate = new List<WST_Waste_Statement__c>();
    private static List<WST_Waste_Statement__c> wsts = new List<WST_Waste_Statement__c>();
    private static List<PriceBookEntry> pbeList = new List<PriceBookEntry>();
    
    //List of Opporunities to create or update
    //private static List<Opportunity> oppToCreate = new List<Opportunity>();
    private static List<Opportunity> oppToUpdate = new List<Opportunity>();
    
    //list of QuoteLine linked to selected WST
    private static Map<ID, List<QuoteLine__c>> qls = new Map<ID, List<QuoteLine__c>>();
    
    //Boolean to Control if everything is OK
    private static Boolean check = true;
    private static Exception exep;
    
    public WST_ManageOpportunityElimController(ApexPages.StandardSetController controller){
        ctrl = controller;
        wsts = [Select ID, Name, Opportunity__c, TotalWeight__c, MetallicWeight__c, DielectricWeight__c, Brand__c,
                KVA__c, PCBContent__c, Type__c, ServiceDemande__r.PriceBook__c,
                ServiceDemande__c, ServiceDemande__r.ApplicantCenter__c, ServiceDemande__r.ApplicantCenter__r.Name, ServiceDemande__r.ApplicantCenter__r.OwnerID,
                ServiceDemande__r.ApplicantCenter__r.BillingStreet, ServiceDemande__r.ApplicantCenter__r.BillingCity, ServiceDemande__r.ApplicantCenter__r.BillingState,
                ServiceDemande__r.ApplicantCenter__r.BillingPostalCode, ServiceDemande__r.ApplicantCenter__r.BillingCountry,
                ServiceDemande__r.ResponsibleName__c, ServiceDemande__r.ResponsibleName__r.Email, ServiceDemande__r.ResponsibleName__r.Phone, ServiceDemande__r.ResponsibleName__r.Fax
                FROM WST_Waste_Statement__c WHERE ID in :ctrl.getSelected()];
        
    }
    
    public static PageReference manageOpportunites(){
        
        Id SDID = apexPages.CurrentPage().getParameters().get('id');
        PageReference retPage = new PageReference('/'); 
        if(SDID != null){
            retPage  = new PageReference('/'+SDID); 
        }else{
             retPage  = new PageReference('/a1B'); 
        }
        
        if(wsts.size() > 0){ 
            for(WST_Waste_Statement__c wst : wsts){
                if(wst.Opportunity__c != null){
                    LinkedOppToUpdate.add(wst);
                    
                }else{
                    LinkedOppToCreate.add(wst);
                }
            }
            Opportunity oppToCreate = createOpportunity(LinkedOppToCreate);
            Quote qs = createQuote(LinkedOppToCreate, oppToCreate);
            createOpportunityLineItems(oppToCreate, LinkedOppToCreate);
            if(check){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Offre(s) créée(nt) avec succès !'));
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Une erreur est survenue, contacter votre administrateur.'+ exep));
                
            }
        }else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Veuillez sélectionner un ou plusieurs transformateur(s) avant de procéder'));   
        }
        
        
        return retPage;
        
        
    }
    
    public static Opportunity createOpportunity(List<WST_Waste_Statement__c> wstWithoutOpportunites){
        Map<ID, WST_Waste_Statement__c> mapWST = new Map<ID, WST_Waste_Statement__c>();
        List<WST_Waste_Statement__c> wstList = new List<WST_Waste_Statement__c>();
        
        //Generate one opportunity to link with all the WST
        WST_Waste_Statement__c wst = wstWithoutOpportunites.get(0);
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = Label.Opp_RT_IndusStd;
        opp.Name = wst.ServiceDemande__r.ApplicantCenter__r.Name+' - Destruction '+(Date.today()).format();
        opp.AccountId = wst.ServiceDemande__r.ApplicantCenter__c;
        opp.ContactName__c = wst.ServiceDemande__r.ResponsibleName__c;
        opp.Pricebook2Id = wst.ServiceDemande__r.PriceBook__c;
        opp.ServiceDemand__c = wst.ServiceDemande__c;
        opp.Salesman__c = wst.ServiceDemande__r.ApplicantCenter__r.OwnerID;
        opp.Nature__c = 'Développement récurrent';
        opp.CloseDate = system.today();
        opp.StageName = 'Proposition Commercial';
        opp.Amount = 1;
        opp.Filiale__c = 'Tredi Saint Vulbas';
        opp.Filiere__c = 'Transformateurs';
        
        
        try{
            insert opp;
        }catch(Exception e){
            exep = e;
            check = false;
        }
        
        
        //update WST with the linked Opportunity
        for(WST_Waste_Statement__c wstToUpdate : wstWithoutOpportunites){
            wstToUpdate.Opportunity__c = opp.ID;
            wst.Status__c = 'Devis en preparation';
            wstList.add(wstToUpdate);
        }
        
        try{
            update wstList;
        }catch(Exception e){
            exep = e;
            check = false;
        }
        
        return opp;
        
    }
    
     @AuraEnabled 
    public static Quote createQuote(List<WST_Waste_Statement__c> wsts, Opportunity opp){
        
        Opportunity oppTmpToUpdate = new Opportunity();
        Map<ID, Quote> manageOppUpdate = new Map<Id, Quote>();
        
        WST_Waste_Statement__c wst = wsts.get(0);
        Quote q = new Quote();
        
        q.OpportunityId = wst.Opportunity__c;
        q.Name = opp.Name +' V1';
        
        q.ContactId = wst.ServiceDemande__r.ResponsibleName__c;
        q.Email = wst.ServiceDemande__r.ResponsibleName__r.Email;
        q.Phone = wst.ServiceDemande__r.ResponsibleName__r.Phone;
        q.Fax = wst.ServiceDemande__r.ResponsibleName__r.Fax;
        q.BillingStreet = wst.ServiceDemande__r.ApplicantCenter__r.BillingStreet;
        q.BillingCity = wst.ServiceDemande__r.ApplicantCenter__r.BillingCity;
        q.BillingState = wst.ServiceDemande__r.ApplicantCenter__r.BillingState;
        q.BillingPostalCode = wst.ServiceDemande__r.ApplicantCenter__r.BillingPostalCode;
        q.BillingCountry = wst.ServiceDemande__r.ApplicantCenter__r.BillingCountry; 
        
        q.ShippingStreet = wst.ServiceDemande__r.ApplicantCenter__r.BillingStreet;
        q.ShippingCity = wst.ServiceDemande__r.ApplicantCenter__r.BillingCity;
        q.ShippingState = wst.ServiceDemande__r.ApplicantCenter__r.BillingState;
        q.ShippingPostalCode = wst.ServiceDemande__r.ApplicantCenter__r.BillingPostalCode;
        q.ShippingCountry = wst.ServiceDemande__r.ApplicantCenter__r.BillingCountry;
        q.Pricebook2Id = wst.ServiceDemande__r.PriceBook__c;
        q.DateDevis__c = getQuoteEndDate(q.Pricebook2Id);
        
     
        
        try{
            insert q;
        }catch(Exception e){
            exep = e;
            check = false;
        }
        
      
            opp.SyncedQuoteId = q.ID;
               
        try{
            update opp;
        }catch(Exception e){
            exep = e;
            check = false;
        }
        
        return q;
        
    }
    
    
    
    public static Boolean createOpportunityLineItems(Opportunity opp, List<WST_Waste_Statement__c> wsts){
        List<OpportunityLineItem> olis = new List<OpportunityLineItem>();
        
        pbeList = getAllPriceBookEntries(opp.Pricebook2ID);
        Map<ID, OpportunityLineItem> mapOLI = new Map<ID, OpportunityLineItem>();
        
        if(wsts != null){
            for(WST_Waste_Statement__c wst : wsts){
                OpportunityLineItem oli = getOpportunityLineItemTransfo(wst, opp.Pricebook2Id);
                
                if(oli.PricebookEntryId == null){
                    oli = getOpportunityLineItemWaste(wst, opp.Pricebook2Id);
                    
                }
                System.debug('OLI : '+oli);
                if(mapOli.get(oli.PricebookEntryId) != null){
                    OpportunityLineItem tmpOLI = mapOli.get(oli.PricebookEntryId);
                    oli.Quantity = tmpOLI.Quantity + oli.Quantity;
                }
                oli.OpportunityId = wst.Opportunity__c;
                mapOLI.put(oli.PricebookEntryId, oli);
                
            }
            OpportunityLineItem oliTransport = getOpportunityLineItemTransport(wsts);
            System.debug('----------------OLI Transport : '+oliTransport);
            if(oliTransport.PricebookEntryId != null){
                
                mapOLI.put(oliTransport.PricebookEntryId, oliTransport);
            }
            
        }
        
        for(OpportunityLineItem oli : mapOli.values()){
            System.debug('OLI : '+oli);
            olis.add(oli);
        }
        
        try{
            insert olis;
        }catch(Exception e){
            exep = e;
            check = false;
        }
        
        return true;
    }

    public static List<PriceBookEntry> getAllPriceBookEntries(ID PricebookID){
        
        List<PricebookEntry> pbes = [SELECT Id, Product2.ID, UnitPrice, Product2.Name, Product2.Unit__c, Product2.Param_1_Num_Min__c, 
                                     Product2.Param_1_Num_Max__c, Product2.Param_2_Num_Min__c, Product2.Param_2_Num_Max__c, 
                                     Product2.Param_3_Text__c, Product2.Param_4_Text__c, Product2.Family FROM PricebookEntry WHERE Pricebook2ID = : PricebookID AND isActive = true];
        
        return pbes;
    }
    
    
    public static OpportunityLineItem getOpportunityLineItemTransfo(WST_Waste_Statement__c wst, ID pbID){
        
        OpportunityLineItem oli = new OpportunityLineItem();
        System.debug('GETOLI TRANSFO');
        System.debug('WST NUMBER : '+wst.name);
        for(PriceBookEntry pbe : pbeList){
           
            if((pbe.Product2.Param_1_Num_Min__c <  wst.KVA__c &&
                pbe.Product2.Param_1_Num_Max__c > wst.KVA__c) &&
               (pbe.Product2.Param_2_Num_Min__c < wst.PCBContent__c &&
                pbe.Product2.Param_2_Num_Max__c >  wst.PCBContent__c) &&
               pbe.Product2.Param_3_Text__c ==  wst.Type__c && 
               pbe.Product2.Family == 'Traitement'){
                   oli.PricebookEntryId = pbe.ID;
                   oli.Unit__c = pbe.Product2.Unit__c;
                   oli.UnitPrice = pbe.UnitPrice;
                   oli.Quantity = 1;
                   
                   return oli;
               }
        }
        
        return new OpportunityLineItem();
    }
    
    
    public static OpportunityLineItem getOpportunityLineItemWaste(WST_Waste_Statement__c wst, ID pbID){
        System.debug('GETOLI TRANSFO');
        System.debug('WST NUMBER : '+wst.name);
        System.debug('WST TotalWeight__c : '+wst.TotalWeight__c);
        
        OpportunityLineItem oli = new OpportunityLineItem();
        String brand = 'null';
		Decimal quantity = 1;
        if(wst.Brand__c != null){
            brand = (wst.Brand__c).ToUpperCase();
        }
        
        if( wst.TotalWeight__c > 0 ||  wst.TotalWeight__c != null){
            Decimal tmp = wst.TotalWeight__c/1000;
            quantity  = tmp.setScale(2);
        }

        for(PriceBookEntry pbe : pbeList){
            String keyword ='';
            if(pbe.Product2.Param_4_Text__c != null){
                keyword = pbe.Product2.Param_4_Text__c.ToUpperCase();
               
                if((pbe.Product2.Param_2_Num_Min__c < wst.PCBContent__c &&
                    pbe.Product2.Param_2_Num_Max__c >  wst.PCBContent__c) && pbe.Product2.Family == 'Traitement' &&
                   (pbe.Product2.Param_3_Text__c ==  wst.Type__c ||  (keyword).contains(brand))){
                       oli.PricebookEntryId = pbe.ID;
                       oli.Unit__c = pbe.Product2.Unit__c;
                       oli.UnitPrice = pbe.UnitPrice;
                       oli.Quantity = quantity;
                       
                       return oli;
                   }
            }
        }
        
        
        if(pbID == 	Label.RCT_PB_Elimination_N){
            oli.PricebookEntryId = Label.RCT_PBE_DEFAUT_Elimination_N; 
            
        }else{
            oli.PricebookEntryId = Label.RCT_PBE_DEFAUT_Elimination_S; 
        }
        
        oli.Unit__c = 'Unité';
        oli.UnitPrice = 0;
        oli.Quantity = 1;
        
        return oli;
        
    }
    
    public static OpportunityLineItem getOpportunityLineItemTransport(List<WST_Waste_Statement__c> wsts){
        Decimal totalWeight = 0;
        Decimal pcb = 0;
        ID OpportunityId;
        for(WST_Waste_Statement__c wst : wsts){
            
            //Sum total weight of WST
            if(wst.TotalWeight__c != null){
                totalWeight += wst.TotalWeight__c;
            }else{
                totalWeight += wst.DielectricWeight__c;
                totalWeight += wst.MetallicWeight__c;
            }
            
            //Get max PCB of WST
            if(wst.PCBContent__c != null && wst.PCBContent__c > pcb){
                pcb = wst.PCBContent__c;
            }else if(wst.PCBAnalyzeResult__c != null && wst.PCBAnalyzeResult__c > pcb){
                pcb = wst.PCBAnalyzeResult__c;
            }
            OpportunityID = wst.Opportunity__c;
        }
        System.debug('PCB :'+pcb);
        System.debug('totalWeight :'+totalWeight);
        OpportunityLineItem oli = new OpportunityLineItem();
       
        
        for(PriceBookEntry pbe : pbeList){
            if((pbe.Product2.Param_1_Num_Min__c <  totalWeight &&
               pbe.Product2.Param_1_Num_Max__c > totalWeight) &&
               (pbe.Product2.Param_2_Num_Min__c < pcb &&
               pbe.Product2.Param_2_Num_Max__c >  pcb) && 
               pbe.Product2.Family == 'Prestation'){
                   oli.OpportunityId = OpportunityID;
                   oli.PricebookEntryId = pbe.ID;
                   oli.Unit__c = pbe.Product2.Unit__c;
                   oli.UnitPrice = pbe.UnitPrice;
                   oli.Quantity = 1;
                   
                   return oli;
               }
        }
		      
        
        return new OpportunityLineItem();
        
    }
    
  

    public static Date getQuoteEndDate(ID PriceBookID){
        
        Date d;
        
        
        if(PriceBookID == 	Label.RCT_PB_Reparation){
            
            d = date.parse(Label.RCT_REPA_END_DATE_CONTRACT);
            
        }else if(PriceBookID == Label.RCT_PB_Depollution){
            
            d = date.parse(Label.RCT_DEPOL_END_DATE_CONTRACT);
            
        }else if(PriceBookID == Label.RCT_PB_Elimination_N || PriceBookID == Label.RCT_PB_Elimination_S){
            
            d = date.parse(Label.RCT_ELIM_END_DATE_CONTRACT);
            
        }else if(PriceBookID == Label.RCT_PB_Elimination_SF6){
            
            d = date.parse(Label.RCT_SF6_END_DATE_CONTRACT);
            
        }else {
            d = Date.today();
        }
        
        
        return d;
        
    }
    
}