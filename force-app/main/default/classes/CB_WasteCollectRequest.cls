/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Mohammed Amine IDRISSI TAGHKI <mohammed-amine.idrissi-taghki@capgemini.com>
 * @version        1.0
 * @created        2021-09-14
 * @systemLayer    Extend Base Model
 *
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Desciption: Child class extends base model
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

 public with sharing class CB_WasteCollectRequest extends PolyMorphic_BaseClass{

    override
    public Object VInit(Id recordId) {

        List<WasteCollectRequest__c> records = (List<WasteCollectRequest__c>)VGetRecord(recordId);
        WasteCollectRequest__c record = records[0];

        String myUrl = URL.getSalesforceBaseUrl().toExternalForm()+PageReference.forResource('DDE_Icons').getUrl();
        if(myUrl.contains('?')) {
            myUrl = myUrl.subString(0, myUrl.indexOf('?'));
        }

        Site site = [SELECT Id FROM Site WHERE UrlPathPrefix = 'DemandesEnlevement' LIMIT 1];

        String communityUrl = [SELECT SecureURL FROM SiteDetail WHERE DurableId =: site.Id].SecureUrl;

        myUrl = myUrl.substring(myUrl.indexOf('/resource'), myUrl.length());

        communityUrl += myUrl; 

        String setupImg;

        if(record.RequestType__c == 'Gaz') {
            setupImg = '28';
        } else {
            setupImg = '48';
        }

        Map<String, Object> config = new Map<String, Object>();

        //custom condition = lock record // modify disabled in js
        String lockBadge = (record.Statut_demande__c == 'Annulé' || record.Statut_demande__c == 'Approuvé' || record.Statut_demande__c == 'Soumis pour approbation') ?
                            '<div><div style="padding: 0px 10px; background-color: #fe9339; border-radius: 20px;">Verrouillé</div></div>': null;

        // disable inputs
        Boolean disabled = (record.Statut_demande__c == 'Annulé' || record.Statut_demande__c == 'Approuvé' || record.Statut_demande__c == 'Soumis pour approbation') ? true : false;

        // additional html
        List<String> htmlAddons = new List<String>();
        htmlAddons.add('<div>Adresse d\'enlèvement : <span style="font-family: \'fontKarbon-SemiBold\'; color: #1E4477;">'+record.Account__r.BillingStreet+', '+record.Account__r.BillingPostalCode+' '+record.Account__r.BillingCity+'</span></div>');
        htmlAddons.add('<div>Type déchets : <img src='+communityUrl+'/'+record.RequestType__c+'.png'+' width='+setupImg+' height='+setupImg+' align="middle"></div>');

        List<Object> lstCustoms = new List<Object>();

        //button one
        Map<String, Object> subConfigApprov = new Map<String, Object>();

        subConfigApprov.put('label', 'Soumettre pour approbation');
        subConfigApprov.put('class', 'slds-button slds-button_brand slds-align_absolute-center');
        subConfigApprov.put('action', 'submitForApproval');
        subConfigApprov.put('popup', true);
        subConfigApprov.put('popupTitle', 'Envoi pour approbation');
        subConfigApprov.put('popupBody', '<p>Vous êtes sur le point de soumettre cette demande pour approbation.<br>Toute demande d\'approbation verrouille l\'enregistrement et aucune modification ne vous sera permise.<br>Pour confirmer votre demande d\'approbation, veuillez cocher la case ci-dessous</p>');
        subConfigApprov.put('popupField', JSON.serialize(new Map<String, Object>{
            'label' => 'Par la présente, je certifie l’exactitude des renseignements et informations fournis.',
            'name' => 'TECH_Signed__c',
            'required' => true,
            'style' => 'display: flex; flex-direction: row;',
            'variant' => 'label-hidden',
            'buttonLabel' => 'Soumettre pour approbation'
        }));

        //button two
        Map<String, Object> subConfigCancel = new Map<String, Object>();
        
        subConfigCancel.put('label', 'Annuler la demande');
        subConfigCancel.put('class', 'slds-button slds-button_destructive slds-align_absolute-center');
        subConfigCancel.put('action', 'cancelRequest');
        subConfigCancel.put('popup', true);
        subConfigCancel.put('popupTitle', 'Annuler la demande');
        subConfigCancel.put('popupBody', '<p>Veuiller préciser la raison de l\'annulation de la demande.</p>');
        subConfigCancel.put('popupField', JSON.serialize(new Map<String, Object>{
            'label' => null,
            'name' => 'CancellationComment__c',
            'required' => false,
            'style' => '',
            'variant' => '',
            'buttonLabel' => 'Annuler la demande'
        }));

        //add to list
        if(!disabled) {
            lstCustoms.add(subConfigApprov);
            lstCustoms.add(subConfigCancel);
        }

        config.put('htmlAddons', htmlAddons);
        config.put('disabled', disabled);
        config.put('additionalDetail', new List<String>{lockBadge});
        config.put('customButtons', lstCustoms);
        config.put('record.Statut_demande__c', record.Statut_demande__c);
        config.put('headerSize', (lstCustoms.size() > 0) ? 'slds-col slds-size_6-of-12' : 'slds-col slds-size_10-of-12');

        return config;

    }
    
    override
    public Object VGetRecord(String recordId) {

        System.debug('Id: '+recordId);
        return [SELECT ID, Name, Account__r.Name, OutOfMarket__c, Statut_demande__c, Date_Enlevement__c, RequestType__c, Account__r.ShippingAddress, Account__r.BillingCity, Account__r.BillingPostalCode, Account__r.BillingStreet, Special_Instruction__c FROM WasteCollectRequest__c WHERE ID = :recordId];
    
    }

    override
    public Object VCustomAction(String recordJSON, String action) {

        Map<String, Object> objMap = (Map<String, Object>)JSON.deserializeUntyped(recordJSON);
        objMap.remove('accountObj');
        WasteCollectRequest__c record = (WasteCollectRequest__c)JSON.deserialize(JSON.serialize(objMap), WasteCollectRequest__c.class);

        if(action == 'submitForApproval') {

            record.SignedBy__c = UserInfo.getUserId();
            upsert record;

            return sendForApproval(record);

        } else if(action == 'cancelRequest') {

            return cancelRequest(record);

        } else if(action == 'lwcCancelRequest') {

            return lwcCancelRequest(recordJSON);

        }

        return null;
        
    }

    private Object cancelRequest(SObject record) {

        record.put('TECH_ByPass__c', true);
        record.put('Statut_demande__c', 'Annulé');
        upsert record;

        record.put('TECH_ByPass__c', false);
        upsert record;

        return new Map<String, Object>{'reload' => true,'title' => 'Annuler la demande', 'message' => 'La demande a bien été annulée', 'type' => 'success'};

    }

    private Object sendForApproval(SObject record) {

        Boolean empty = true;
        Boolean empty_outMarket = true;
        Boolean dateBefore = false;

        WasteCollectRequest__c wrc = (WasteCollectRequest__c)record;

        system.debug(wrc.OutOfMarket__c);

        List<WasteCollectRequestItem__c> lstWCRI_outMarket;
        if(wrc.OutOfMarket__c){
            lstWCRI_outMarket = [SELECT Quantite__c FROM WasteCollectRequestItem__c WHERE Demande_Enlevement__c = :(String)record.get('id') AND Product__r.Name LIKE '%Autre Produit%'];
        }

        system.debug(lstWCRI_outMarket);

        List<WasteCollectRequestItem__c> lstWCRI = [SELECT Quantite__c FROM WasteCollectRequestItem__c WHERE Demande_Enlevement__c = :(String)record.get('id') AND Product__r.Name != 'Autre Produit'];

        if(wrc.Date_Enlevement__c < System.Date.today()) {
            dateBefore = true;
        }

        for(WasteCollectRequestItem__c wrci: lstWCRI) {
            if(wrci.Quantite__c != null) {
                empty = false;
                break;
            }
        }

        if(lstWCRI_outMarket != null && lstWCRI_outMarket.size() > 0){
            for(WasteCollectRequestItem__c wrci: lstWCRI_outMarket) {
                if(wrci.Quantite__c != null && wrci.Quantite__c > 0) {
                    empty_outMarket = false;
                    break;
                }
            }
        } else {
            empty_outMarket = false;
        }

        if(dateBefore) {
            return new Map<String, Object>{'reload' => false, 'title' => 'Envoi pour approbation', 'message' => 'Motif: la date de cette demande est antérieure à la date d\'aujourd\'hui', 'type' => 'warning'};
        } else if(!empty && !empty_outMarket) {
            // create the new approval request to submit
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setObjectId(wrc.Id);
            // submit the approval request for processing
            Approval.ProcessResult result = Approval.Process(req);
            // display if the reqeust was successful
            System.debug('Submitted for approval successfully: '+result.isSuccess());
            if(result.isSuccess()) {
                return new Map<String, Object>{'reload' => true,'title' => 'Envoi pour approbation', 'message' => 'La demande d\'enlèvement a bien été envoyée pour approbation', 'type' => 'success'};
            } else {
                return new Map<String, Object>{'reload' => false,'title' => 'Envoi pour approbation', 'message' => 'Erreur lors de l\'envoi pour approbation', 'type' => 'error'};
            }
        } else if(empty){
            return new Map<String, Object>{'reload' => false,'title' => 'Envoi pour approbation', 'message' => 'Veuillez renseigner au moins un déchet avant l\'envoi pour approbation', 'type' => 'warning'};
        } else if(empty_outMarket && wrc.OutOfMarket__c) {
            return new Map<String, Object>{'reload' => false,'title' => 'Envoi pour approbation', 'message' => 'Veuillez renseigner au moins un déchet hors marché avant l\'envoi pour approbation', 'type' => 'warning'};
        }

        return null;

    }

    @AuraEnabled
    public static string lwcCancelRequest(String jsonSobject){
        try {
            WCR_User_Controller subclass = new WCR_User_Controller();
            return subclass.cancelRequest(jsonSobject);
        } catch (Exception e) {
            System.debug(e.getMessage());
            return 'NOK';
        }
    }

    public without sharing class WCR_User_Controller {
        public string cancelRequest(String jsonSobject){
            try {
                System.debug(jsonSobject);
                SObject obj = (SObject)JSON.deserialize(jsonSobject, WasteCollectRequest__c.class);
                update obj;

                obj.put('TECH_ByPass__c', false);
                upsert obj;

                return 'OK';
            } catch (Exception e) {
                System.debug(e.getMessage());
                return 'NOK';
            }
        }
    }

}