global without sharing class GenGuidContactFacturation {

    global class FormulaireId {
        @InvocableVariable
        global ID formulaireId;
        @InvocableVariable
        global Attestation__c[] prefips;
    }


     @InvocableMethod(label='Set GUID ContactFacturation' description='Set a GUID passcode for Contact Facturation' category='Contact Facturation')
     public static void genGuidContactFacturation(List<FormulaireId> request) {
       
        System.debug('requestttt'+request);
        String contactFacturationUrl = [SELECT SITE_ContactFacturation__c FROM URL__c].SITE_ContactFacturation__c;
        List<ID> listId      = new List<ID>{}; 
        for(Integer i = 0; i < request.size(); i++) {
            listId.add (request[i].FormulaireId);
        }
        List<Contact> lstCts = [SELECT Contact.GUID__c,Contact.BillingContactUpdateLink__c FROM Contact WHERE ID IN : listId ]; 
        for (Contact c : lstCts){
            Blob b = Crypto.GenerateAESKey(128);
            String h = EncodingUtil.ConvertTohex(b);
            String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
            String url = ''+contactFacturationUrl+'/s/?recordId='+guid;
            c.GUID__c = guid;
            c.BillingContactUpdateLink__c = url;
        }
        upsert lstCts;
        return;
    }
}