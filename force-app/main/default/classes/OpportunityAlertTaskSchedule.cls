global class OpportunityAlertTaskSchedule implements Schedulable {
    
    global void execute(SchedulableContext sc) {
        System.debug('.....CA MARCHE');
        OpportunityAlertTaskBatch b = new OpportunityAlertTaskBatch('Select Id, OwnerId From Opportunity Where CloseDate = YESTERDAY AND IsClosed = false'); 
        Database.executebatch(b);
    }
}