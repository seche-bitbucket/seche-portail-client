public with sharing class MercurialeController {
    
    @AuraEnabled(cacheable=true)
    public static Map<Id, IndexValue__c> getMercuriale(String selectedSource, Date selectedDate) {
        
        Map<Id, IndexValue__c> indexValueMap = new Map<Id, IndexValue__c>();
        for (IndexValue__c indexValue : getIndexValue(selectedSource, selectedDate)) {
            indexValueMap.put(indexValue.Indices__c, indexValue);
        }
        
        Map<Id, IndexValue__c> indexValues = new Map<Id, IndexValue__c>();
        for (Index__c index : getProduct(selectedSource)) {
            IndexValue__c indexValue = indexValueMap.get(index.Id);
            IndexValue__c indexValueToUpsert = new IndexValue__c();
            indexValueToUpsert.Indices__c = index.Id;
            indexValueToUpsert.Source__c = selectedSource;
            indexValueToUpsert.Tech_ProductName__c = index.Product__r.Name;
            System.debug('family :' + index.Product__r.ProductFamilly__c);
            System.debug('sub family :' + index.Product__r.ProductSubFamily__c);
            indexValueToUpsert.ProductFamily__c = index.Product__r.ProductFamilly__c;
            indexValueToUpsert.ProductSubFamily__c = index.Product__r.ProductSubFamily__c;
            if (indexValue!=null) {
                indexValueToUpsert.Id = indexValue.Id;
                indexValueToUpsert.Value__c = indexValue.Value__c;
                indexValueToUpsert.Date__c = indexValue.Date__c;
            }
            indexValues.put(index.Id, indexValueToUpsert);
        }

        return indexValues;
    }

    public static List<Index__c> getProduct(String selectedSource) {
        return [
            SELECT Id, Source__c, Product__c, Product__r.Name, ProductFamily__c, ProductSubFamily__c, Product__r.ProductFamilly__c, Product__r.ProductSubFamily__c
            FROM Index__c 
            WHERE Source__c =: selectedSource ORDER BY 
            ProductFamily__c, ProductSubFamily__c
        ];
    }
   
    public static List<IndexValue__c> getIndexValue(String selectedSource, Date selectedDate) {
        return [
            SELECT Id, Indices__c, Source__c, Date__c, Value__c
            FROM IndexValue__c 
            WHERE Source__c =: selectedSource AND Date__c =: selectedDate
        ];
    }

    @AuraEnabled
    public static String getSFDomain() {
        return [SELECT Org_URL__c FROM URL__c LIMIT 1].Org_URL__c;
    }
}