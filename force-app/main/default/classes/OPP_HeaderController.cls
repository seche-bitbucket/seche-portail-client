public class OPP_HeaderController {
    
    Opportunity opp =  new Opportunity();

    public OPP_HeaderController(ApexPages.StandardController acon) {
        
        
        opp = [Select ID, Name, RecordtypeID, Recordtype.Name, OpportuniteMereIntra__r.recordtypeID FROM Opportunity WHERE ID =: Apexpages.currentPage().getParameters().get('Id')];
    }
    
    public String getOppRTName() {
        return opp.RecordType.Name;
    
    }
    
    public String getOppBackgroundColor() {
        //Opport Intermediaire
        if(opp.RecordTypeID =='01258000000Ab0Y' || 
           opp.RecordTypeID =='01258000000Ab0R' || opp.RecordTypeID =='01258000000Ab0S' || 
           opp.RecordTypeID =='01258000000Ab0T' || opp.RecordTypeID =='01258000000Ab0U' || 
           opp.RecordTypeID =='01258000000Ab0V' || opp.RecordTypeID =='01258000000Ab0W'){
               return '#006C51';
        
             
        //Opport Standard
        }else if(opp.RecordTypeID =='01258000000Tt7p' || opp.RecordTypeID =='01258000000U0z5' || 
                 opp.RecordTypeID =='01258000000Tfhm' || opp.RecordTypeID =='01258000000U0z6' || 
                 opp.RecordTypeID =='01258000000U0z8' || opp.RecordTypeID =='01258000000U0z7' ){
            return '#DB7312';
        //Opport Intra
        }else{
           //Opport Intra Intermediaire
        if(opp.OpportuniteMereIntra__r.recordtypeID =='01258000000Ab0Y' || 
           opp.OpportuniteMereIntra__r.recordtypeID =='01258000000Ab0R' || opp.OpportuniteMereIntra__r.recordtypeID =='01258000000Ab0S' || 
           opp.OpportuniteMereIntra__r.recordtypeID =='01258000000Ab0T' || opp.OpportuniteMereIntra__r.recordtypeID =='01258000000Ab0U' || 
           opp.OpportuniteMereIntra__r.recordtypeID =='01258000000Ab0V' || opp.OpportuniteMereIntra__r.recordtypeID =='01258000000Ab0W'){
               return '#006C51';
               //Opport Intra Standard
           }else{
               return '#DB7312';
           }
        }
        
        
    }
    
    public String getOppFontColor() {
       
            return 'white';
    }
}