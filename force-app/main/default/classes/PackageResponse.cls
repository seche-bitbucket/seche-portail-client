/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : PackageResponse
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : PackageResponse
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 25-05-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
public class PackageResponse {

    public String PackageId {get;set;}
    public String PackageName {get;set;}
    public String DocumentGroupCode {get;set;}
    public String Initiator {get;set;}
    public String ExpiryTimestamp {get;set;}
    public String ExternalPackageReference {get;set;}
    public String F2FSigningUrl {get;set;}
    public String PackageStatus {get;set;}
    public List<ResponsePackageDocument> PackageDocuments {get;set;}
    public List<ResponseStakeholder> Stakeholders {get;set;}
    public String CreationTimestamp {get;set;}
    public List<String> Warnings {get;set;}

    public class ResponsePackageDocument {
        public String DocumentId {get;set;}
        public String DocumentType {get;set;}
        public String ExternalDocumentReference {get;set;}
        public String DocumentName {get;set;} 
        public List<ResponseLocation> Locations {get;set;}
    }

    public class ResponseLocation {
        public String Id {get;set;}
        public String Label {get;set;}
        public Integer PageNumber {get;set;}
    }

    public class ResponseStakeholder {
        public String Type {get;set;}
        public String EmailAddress {get;set;}
        public String ContactGroupCode {get;set;}
        public String ExternalStakeholderReference {get;set;}
        public String StakeholderId {get;set;}
        public List<ResponseActor> Actors {get;set;}
    }

    public class ResponseActor {
        public String Type {get;set;}
        public String Reason {get;set;}
        public String CompletedBy {get;set;}
        public String CompletedTimestamp {get;set;}
        public List<ResponseLocationActor> Locations {get;set;}
        public String ActorId {get;set;}
        public String ActionUrl {get;set;}
        //public List<String> ActionUrls {get;set;}
        public String ActorStatus {get;set;}
    }

    public class ResponseLocationActor {
        public String Id {get;set;}
        public String UsedSigningType {get;set;}
    }

    /* BEGIN Region Classes use in get method : ConnectiveApiManager getBatch50Package */
    public class ResponseGetListPackage {
        public String ContinuationToken {get;set;}
        public List<ResponseItemPackage> Items {get;set;}
        public Integer MaxQuantity {get;set;}
        public Integer Total {get;set;}
    }

    public class ResponseItemPackage {
        public String PackageId {get;set;}
        public String PackageStatus {get;set;}
        public String ExternalPackageReference {get;set;}
    }

    public static ResponseGetListPackage deserializeResponseGetListPackagejson(String json){
        return (ResponseGetListPackage) System.JSON.deserialize(json, ResponseGetListPackage.class);
    }
    /* END Region Classes use in get method :  ConnectiveApiManager getBatch50Package */

    /* BEGIN Region Classes use in get method : ConnectiveApiManager getStatusByConnectivePackageId */
    public class ResponseGetPackageStatus {
        public String PackageName {get;set;}
        public String CreationTimestamp {get;set;}
        public String Initiator {get;set;}
        public String ExpiryTimestamp {get;set;}
        public String ExternalPackageReference {get;set;}
        public String F2FSigningUrl {get;set;}
        public String PackageStatus {get;set;}
        public List<ResponsePackageDocument> PackageDocuments {get;set;}
        public List<ResponseStakeholder> Stakeholders {get;set;}
    }

    public static ResponseGetPackageStatus deserializeResponseGetPackageStatusjson(String json){
        return (ResponseGetPackageStatus) System.JSON.deserialize(json, ResponseGetPackageStatus.class);
    }
    /* END Region Classes use in get method : ConnectiveApiManager getStatusByConnectivePackageId */

    public static PackageResponse deserializeResponsejson(String json){
        return (PackageResponse) System.JSON.deserialize(json, PackageResponse.class);
    }

    public static ResponsePackageDocument deserializeResponsePackageDocumentjson(String json){
        return (ResponsePackageDocument) System.JSON.deserialize(json, ResponsePackageDocument.class);
    }

    public static String getFirstSignerId(PackageResponse resp){
        String signerId = '';
        for(ResponseStakeholder st : resp.Stakeholders){
            if(st.Type == 'Person'){
                signerId = st.ExternalStakeholderReference;
                break;
            }
        }
        return signerId;
    }

    public static String getFirstDocumentIdSalesforce(PackageResponse resp){
        String docId = '';
        for(ResponsePackageDocument doc : resp.PackageDocuments){
            docId = doc.ExternalDocumentReference;
            break;
        }
        return docId;
    }

    public static String getFirstDocumentId(PackageResponse resp){
        String docId = '';
        for(ResponsePackageDocument doc : resp.PackageDocuments){
            docId = doc.DocumentId;
            break;
        }
        return docId;
    }

    public static String getFirstDocumentId(ResponseGetPackageStatus resp){
        String docId = '';
        for(ResponsePackageDocument doc : resp.PackageDocuments){
            docId = doc.DocumentId;
            break;
        }
        return docId;
    }

    public static String getFirstDocumentName(PackageResponse resp){
        String docName = '';
        for(ResponsePackageDocument doc : resp.PackageDocuments){
            docName = doc.DocumentName;
            break;
        }
        return docName;
    }
}