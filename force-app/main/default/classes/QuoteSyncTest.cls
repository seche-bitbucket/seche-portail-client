@isTest(SeeAllData=false)
public class QuoteSyncTest {

    @isTest
    public static void test() {
        
        TestFactory.createSettings();
        
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        //Quote Section
        Opportunity opp = TestFactory.generateOpport(acc, null);
        
        Quote qte = TestFactory.generateQuote(ct, acc, opp);
        
        Product2 p = new Product2(Name='test product', IsActive=true, TECH_ExternalID__c='zExternalID_Capgemini');             
        insert(p);                            
        
        PricebookEntry currPbe = new PricebookEntry(Pricebook2Id=qte.Pricebook2Id, Product2Id=p.Id, IsActive=true, UnitPrice=10);
        insert(currPbe);
        
        opp.SyncedQuoteId = qte.Id;
        update opp;
        
        qte.OpportunityId = opp.Id;
        update qte;
        
        Test.startTest();
        
        qte.ReferenceWasteOwner__c = 'Test';
        update qte;
        
        Test.stopTest();
        
        opp = [SELECT ReferenceWasteOwner__c FROM Opportunity WHERE ID = :opp.Id];
        
        System.assertEquals(qte.ReferenceWasteOwner__c, opp.ReferenceWasteOwner__c);
        
    }
    
    @isTest
    public static void test2() {
        
        TestFactory.createSettings();
        
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        //Quote Section
        Opportunity opp = TestFactory.generateOpport(acc, null);
        
        Quote qte = TestFactory.generateQuote(ct, acc, opp);
        
        opp.SyncedQuoteId = qte.Id;
        update opp;
        
        qte.OpportunityId = opp.Id;
        update qte;
        
        Test.startTest();
        	
        opp.FuelPriceIndexation__c = true;
        update opp;
        
        Test.stopTest();
        
        qte = [SELECT FuelPriceIndexation__c FROM Quote WHERE ID = :qte.Id];
        
        System.assertEquals(opp.FuelPriceIndexation__c, qte.FuelPriceIndexation__c);
        
    }
    
    @isTest
    public static void test3() {
        
        TestFactory.createSettings();
        
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        //Quote Section
        Opportunity opp = TestFactory.generateOpport(acc, null);
        
        Quote qte = TestFactory.generateQuote(ct, acc, opp);
        
        opp.SyncedQuoteId = qte.Id;
        update opp;
        
        Product2 p = new Product2(Name='test product', IsActive=true, TECH_ExternalID__c='zExternalID_Capgemini');             
        insert(p);                            
        
        PricebookEntry currPbe = new PricebookEntry(Pricebook2Id=qte.Pricebook2Id, Product2Id=p.Id, IsActive=true, UnitPrice=10);
        insert(currPbe);
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.Id, UnitPrice=10, Quantity=1, Discount=1, ServiceDate=System.today(), PricebookEntryId=currPbe.Id);
        insert oli;
        
        QuoteLineItem qteLine = new QuoteLineItem(QuoteId=qte.Id, UnitPrice=10, Quantity=1, Discount=1, ServiceDate=System.today(), PricebookEntryId=currPbe.Id, OpportunityLineItemId=oli.Id);
        insert qteLine;
        
        System.debug([select id from quotelineitem where opportunitylineitemid =: oli.id]);
        
        Test.startTest();
        
        oli.Classe__c = 'Test';
        update oli;
        
        Test.stopTest();
                
        qteLine = [SELECT Classe__c FROM QuoteLineItem WHERE ID = :qteLine.Id];
        System.assertEquals(oli.Classe__c, qteLine.Classe__c);
        
    }
    
    @isTest
    public static void test4() {
        
        TestFactory.createSettings();
        
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        //Quote Section
        Opportunity opp = TestFactory.generateOpport(acc, null);
        
        Quote qte = TestFactory.generateQuote(ct, acc, opp);
        
        opp.SyncedQuoteId = qte.Id;
        update opp;
        
        Product2 p = new Product2(Name='test product', IsActive=true, TECH_ExternalID__c='zExternalID_Capgemini');             
        insert(p);                            
        
        PricebookEntry currPbe = new PricebookEntry(Pricebook2Id=qte.Pricebook2Id, Product2Id=p.Id, IsActive=true, UnitPrice=10);
        insert(currPbe);
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.Id, UnitPrice=10, Quantity=1, Discount=1, ServiceDate=System.today(), PricebookEntryId=currPbe.Id);
        insert oli;
        
        SyncHandler.runTest();
        
        QuoteLineItem qteLine = new QuoteLineItem(QuoteId=qte.Id, UnitPrice=10, Quantity=1, Discount=1, ServiceDate=System.today(), PricebookEntryId=currPbe.Id, OpportunityLineItemId=oli.Id);
        insert qteLine;
        
        QteLineItemSyncTriggerV2Handler.unstop();
		                
        Test.startTest();
        
        qteLine.Features__c = 'Features';
        update qteLine;
        
        Test.stopTest();
        
        oli = [SELECT Features__c FROM OpportunityLineItem WHERE ID = :oli.Id];
        System.assertEquals(oli.Features__c, qteLine.Features__c);
        
    }
    
}