/**
 * @description       : 
 * @author            : MAIT - @moidriss
 * @group             : 
 * @last modified on  : 11-13-2023
 * @last modified by  : MAIT - @moidriss
**/
public class Connective_CallBackTriggerHandler {
    
    public static void UpdateAfterSignature(List<Connective_CallBack__e> triggerNew) {

        System.debug(UserInfo.getUserId());
        
        Map<String, String> mapPkgToStatus = new Map<String, String>();
        Map<String, String> mapPkgToReject = new Map<String, String>();
        Map<String, String> mapPkgToURL = new Map<String, String>();

        Map<Id, String> mapQuoteTosalesmanEmail = new Map<Id, String>();
        Map<Id, String> mapQuoteTosalesAssistantEmail = new Map<Id, String>();
        Map<Id, String> mapDocTosalesmanEmail = new Map<Id, String>();
        Map<Id, String> mapDocTosalesAssistantEmail = new Map<Id, String>();
        
        for(Connective_CallBack__e ev: triggerNew) {
            Map<String, Object> mapJson = (Map<String, Object>) JSON.deserializeUntyped(ev.JSON_Body__c);
          	mapPkgToStatus.put((String)mapJson.get('Id'), (String)mapJson.get('Status'));
            
            List<Map<String, Object>> lstActor = new List<Map<String, Object>>();
            for(Object sth : (List<Object>)mapJson.get('Stakeholders')){
                Map<String, Object> mapSth = (Map<String, Object>)sth;
                List<Object> lstTmp = (List<Object>)mapSth.get('Actors');
                for(Object obj: lstTmp) {
                    lstActor.add((Map<String, Object>)obj);
                }
            }
            
            for(Map<String, Object> act : lstActor){
                if((String)act.get('Type') == 'Receiver'){
                    List<Object> urls = (List<Object>)act.get('Links');
                    if(urls.size() > 0) {
                        mapPkgToURL.put((String)mapJson.get('Id'), (String)urls[0]);
                    }
                }
                if((String)act.get('Type') == 'Signer'){
                    Map<String, Object> mapResult = (Map<String, Object>)act.get('Result');
                    if(mapPkgToReject.containsKey((String)mapJson.get('Id'))) {
                        break;
                    }
                    mapPkgToReject.put((String)mapJson.get('Id'), (mapResult.get('RejectReason') != null) ? (String)mapResult.get('RejectReason') : '');
                }
            }
            
        }
        
        system.debug(mapPkgToStatus);
        
        Map<String, SObject> lstSignedObj = new Map<String, SObject>();
        Map<String, SObject> lstRejectedObj = new Map<String, SObject>();
        List<DocumentsForSignature__c> lstDocSigned = [SELECT Id, TECH_ExternalId__c, Package__c, Package__r.ConnectivePackageStatus__c, Package__r.ConnectivePackageId__c, Attestation__c, T5F_PreFip__r.Lead__r.OwnerId, Status__c, Quote__c, FIP_FIP__c, Lead__c, CAP__c FROM DocumentsForSignature__c WHERE Package__r.ConnectivePackageId__c IN :mapPkgToStatus.keySet()];

        Map<String, Attestation__c> mapDocToPreFip = new Map<String, Attestation__c>();
        Map<String, Quote> mapDocToQuote = new Map<String, Quote>();
        Map<String, User> mapDocToUser = new Map<String, User>();
        Map<String, Lead> mapDocToLead = new Map<String, Lead>();
        Map<String, CAP__c> mapDocToCap = new Map<String, CAP__c>();
        
        List<Id> userIds = new List<Id>();
        List<Id> prefipIds = new List<Id>();
        List<Id> quoteIds = new List<Id>();
        List<Id> leadIds = new List<Id>();
        List<Id> capIds = new List<Id>();
        
        for(DocumentsForSignature__c doc: lstDocSigned) {
            prefipIds.add(doc.Attestation__c);
            quoteIds.add(doc.Quote__c);
            leadIds.add(doc.Lead__c);
            capIds.add(doc.CAP__c);
        }

        List<String> pfipFields = new List<String>(Attestation__c.SObjectType.getDescribe().fields.getMap().keySet());
        
        if(prefipIds.size() > 0) {
            for(Attestation__c prefip: Database.query('SELECT '+String.join(pfipFields, ',')+', Contact__r.Name, Account__r.Name, Form__r.BusinessCase__c, AssociatedSalesRep__r.Email FROM Attestation__c WHERE ID IN :prefipIds')) {
                for(DocumentsForSignature__c doc: lstDocSigned) {
                    if(doc.Attestation__c == prefip.Id) {
                        mapDocToPreFip.put(doc.Id, prefip);
                    }
                }
            }
        }
        
        List<String> qtFields = new List<String>(Quote.SObjectType.getDescribe().fields.getMap().keySet());
        
        if(quoteIds.size() > 0) {
            for(Quote qt: Database.query('SELECT '+String.join(qtFields, ',')+', Opportunity.Salesman__r.Email, Opportunity.Salesman__r.Assistant__r.Email FROM Quote WHERE ID IN :quoteIds')) {
                for(DocumentsForSignature__c doc: lstDocSigned) {
                    if(doc.Quote__c == qt.Id) {
                        mapDocToQuote.put(doc.Id, qt);
                    }
                }
            }
        }

        List<String> leadFields = new List<String>(Lead.SObjectType.getDescribe().fields.getMap().keySet());
        
        if(leadIds.size() > 0) {
            for(Lead lead: Database.query('SELECT '+String.join(leadFields, ',')+' FROM Lead WHERE ID IN :leadIds')) {
                for(DocumentsForSignature__c doc: lstDocSigned) {
                    if(doc.Lead__c == lead.Id) {
                        mapDocToLead.put(doc.Id, lead);
                    }
                }
            }
        }
        
        List<String> capFields = new List<String>(CAP__c.SObjectType.getDescribe().fields.getMap().keySet());
        if(capIds.size() > 0) {
            for(CAP__c cap: Database.query('SELECT '+String.join(capFields, ',')+' FROM CAP__c WHERE ID IN :capIds')) {
                for(DocumentsForSignature__c doc: lstDocSigned) {
                    if(doc.CAP__c == cap.Id) {
                        mapDocToCap.put(doc.Id, cap);
                    }
                }
            }
        }

        Map<Id, Id> mapOppToQuote = new Map<Id, Id>();
        System.debug(quoteIds);
        if(quoteIds.size() > 0) {
            List<Quote> qts = [SELECT id, OpportunityId FROM Quote WHERE Id = :quoteIds];
            System.debug(qts);
            List<Id> oppIds = new List<Id>();
            for(Quote q: qts) {
                mapOppToQuote.put(q.OpportunityId, q.Id);
                oppIds.add(q.OpportunityId);
            }
            System.debug(oppIds);
        
            for(Opportunity opp: [SELECT Id, SyncedQuoteId, Salesman__r.Email, Assistant__r.Email FROM Opportunity WHERE Id IN :oppIds]) {
                mapQuoteTosalesmanEmail.put(mapOppToQuote.get(opp.Id), opp.Salesman__r.Email);
                mapQuoteTosalesAssistantEmail.put(mapOppToQuote.get(opp.Id), opp.Assistant__r.Email);
            }
        }
        System.debug(mapQuoteTosalesmanEmail);
        System.debug(mapQuoteTosalesAssistantEmail);

        for(DocumentsForSignature__c doc: lstDocSigned) {
            if(doc.Quote__c != null) {
                System.debug(mapQuoteTosalesmanEmail.get(doc.Quote__c));
                mapDocTosalesmanEmail.put(doc.Id, mapQuoteTosalesmanEmail.get(doc.Quote__c));
                System.debug(mapQuoteTosalesAssistantEmail.get(doc.Quote__c));
                mapQuoteTosalesAssistantEmail.put(doc.Id, mapQuoteTosalesAssistantEmail.get(doc.Quote__c));
            }
        }
        
        Map<String, List<Messaging.SingleEmailMessage>> mapMsg = new Map<String, List<Messaging.SingleEmailMessage>>();
        
        List<DocumentsForSignature__c> docToDownload = new List<DocumentsForSignature__c>();
        List<Id> capsToReset = new List<Id>();
		        
        for(DocumentsForSignature__c doc: lstDocSigned) {

            if(mapDocToCap.size() > 0 && mapDocToCap.get(doc.Id) != null && new List<String>{'REJECTED', 'Rejected', 'rejected'}.contains(mapPkgToStatus.get(doc.Package__r.ConnectivePackageId__c))) {
                // RESET PFAS
                capsToReset.add(mapDocToCap.get(doc.Id).Id);
            }

            Map<String, Object> mapUpdate = ConnectiveV4ApiUtils.updateDocumentForSigTrigger(
                doc, 
                mapPkgToStatus.get(doc.Package__r.ConnectivePackageId__c),
                mapPkgToReject.get(doc.Package__r.ConnectivePackageId__c), 
                mapPkgToURL.get(doc.Package__r.ConnectivePackageId__c),
                (mapDocToQuote.size() > 0) ? mapDocToQuote.get(doc.Id) : null,
                (mapDocToPreFip.size() > 0) ? mapDocToPreFip.get(doc.Id) : null,
                mapDocToUser.get(doc.Id),
                (mapDocTosalesmanEmail.size() > 0) ? mapDocTosalesmanEmail.get(doc.Id) : null,
                (mapQuoteTosalesAssistantEmail.size() > 0) ? mapQuoteTosalesAssistantEmail.get(doc.id) : null,
                (mapDocToLead.size() > 0) ? mapDocToLead.get(doc.Id) : null,
                (mapDocToCap.size() > 0) ? mapDocToCap.get(doc.Id) : null
            );

            doc = (DocumentsForSignature__c) mapUpdate.get('doc');
            SObject obj = (SObject)mapUpdate.get('object');
            if(doc.Status__c == 'FINISHED') {
                docToDownload.add(doc);
                if(obj != null) lstSignedObj.put((String)obj.get('Id'), obj);
            } else if(doc.Status__c == 'REJECTED') {
                if(obj != null) lstRejectedObj.put((String)obj.get('Id'), obj);
            }
            // Using map instead of list to prevent sending duplicate emails for packages with multiple documents
            // Check if the map has already an email for the package
            if(!mapMsg.containsKey(doc.Package__r.ConnectivePackageId__c)) {
            	mapMsg.put(doc.Package__r.ConnectivePackageId__c, (List<Messaging.SingleEmailMessage>)mapUpdate.get('lstMsg')); 
            }
        }
        
        if(docToDownload.size()>0){
            Batch_Connective_Get_Document batch = new Batch_Connective_Get_Document(docToDownload, new List<SObject>(lstSignedObj.values()));
            system.enqueueJob(batch);
        }
        
        if(lstRejectedObj.size() > 0) {
            update new List<SObject>(lstRejectedObj.values());
        }
        
        update lstDocSigned;

        if(!capsToReset.isEmpty()) {
            List<Id> cdlsIds = new List<Id>();
            List<CAP__c> caps = new List<CAP__c>();

            for(ContentDocumentLink cdl: [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN :capsToReset]) {
                cdlsIds.add(cdl.ContentDocumentId);
            }

            // Delete Files
            delete [SELECT Id FROM ContentDocument WHERE Id IN :cdlsIds];

            // Delete Docs Sent
            delete [SELECT Id FROM DocumentsForSignature__c WHERE CAP__c IN :capsToReset];

            // Reset each CAP__c
            for(Id capId: capsToReset) {
                caps.add(new CAP__c(Id = capId, TECH_SendWithConnective__c = false, TECH_GenerateCongaDoc__c = false));
            }

            update caps;
        }

        try{
            List<Messaging.SingleEmailMessage> lstMsg = new List<Messaging.SingleEmailMessage>();
            for(List<Messaging.SingleEmailMessage> msgs : mapMsg.values()) {
                lstMsg.addAll(msgs);
            }
        	Messaging.sendEmail(lstMsg);
        } catch(Exception e) {
            system.debug(e.getLineNumber() + ' ' + e.getMessage() + ' '+e.getCause());
        }

    }
}