@isTest
private class FIP_CSVExportControllerTest {
    
    @isTest static void testAllAccountList() {
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.BillingPostalCode = '21000';
        acc.BillingStreet = '1 rue du test';
        insert acc;
        
        Contact ctc = new Contact();
        ctc.lastName = 'Test';
        ctc.AccountId = acc.ID;
        insert ctc;
        
        List<FIP_FIP__c> testFips = new List<FIP_FIP__c>();
        for(integer i=0; i<100 ; i++){
            FIP_FIP__c fip = new FIP_FIP__c();
            fip.Collector__c = acc.ID;
            fip.Contact__c = ctc.ID;
            
            testFips.add(fip);
        }
        insert testFips;
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        PageReference pageRef = Page.FIP_exportCSV;
        Test.setCurrentPage(pageRef);
        FIP_CSVExportController listviewAPIctrl = new FIP_CSVExportController();
        pageRef.getParameters().put('listid', 'ert322144');
        pageRef.getParameters().put('Object', 'FIP_FIP__c');
        pageRef.getParameters().put('listName', 'All Fip');
        listviewAPIctrl.fetchListviewRecords();
        Test.StopTest();
        system.assertEquals(listviewAPIctrl.fileName,'FIP_FIP__c_All_Fip_'+Datetime.now().format());
        //system.assertEquals(listviewAPIctrl.columnName[0],'Account Name');
        
    }

    @isTest static void testGenerateCsv() {
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.BillingPostalCode = '21000';
        acc.BillingStreet = '1 rue du test';
        insert acc;
        
        Contact ctc = new Contact();
        ctc.lastName = 'Test';
        ctc.AccountId = acc.ID;
        insert ctc;
        
        List<FIP_FIP__c> testFips = new List<FIP_FIP__c>();
        for(integer i=0; i<100 ; i++){
            FIP_FIP__c fip = new FIP_FIP__c();
            fip.Collector__c = acc.ID;
            fip.Contact__c = ctc.ID;
            
            testFips.add(fip);
        }
        insert testFips;
        Test.StartTest();        
        PageReference pageRef = Page.FIP_exportCSV;
        Test.setCurrentPage(pageRef);
        FIP_CSVExportController listviewAPIctrl = new FIP_CSVExportController();        
        String csv= listviewAPIctrl.generateCSV(testFips);
        Test.StopTest();
        system.assertNotEquals(csv, '');
        //system.assertEquals(listviewAPIctrl.columnName[0],'Account Name');
        
    }
    
     @isTest static void testUpdateFip() {
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.BillingPostalCode = '21000';
        acc.BillingStreet = '1 rue du test';
        insert acc;
        
        Contact ctc = new Contact();
        ctc.lastName = 'Test';
        ctc.AccountId = acc.ID;
        insert ctc;
        
        List<FIP_FIP__c> testFips = new List<FIP_FIP__c>();
        for(integer i=0; i<100 ; i++){
            FIP_FIP__c fip = new FIP_FIP__c();
            fip.Collector__c = acc.ID;
            fip.Contact__c = ctc.ID;
            
            testFips.add(fip);
        }
        insert testFips;
        Test.StartTest();        
        PageReference pageRef = Page.FIP_exportCSV;
        Test.setCurrentPage(pageRef);
        FIP_CSVExportController listviewAPIctrl = new FIP_CSVExportController();        
        listviewAPIctrl.updateFIPList(testFips);
        Test.StopTest();
       
        
    }

    @isTest static void testAllAccountList_error() {
        
        List<Account> testAccounts = new List<Account>();
        for(integer i=0; i<100 ; i++){
            Account acc = new Account();
            acc.Name = 'Test Account'+ i;
            acc.BillingPostalCode = '21000';
            testAccounts.add(acc);
        }
        insert testAccounts;
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        PageReference pageRef = Page.FIP_exportCSV;
        Test.setCurrentPage(pageRef);
        FIP_CSVExportController listviewAPIctrl = new FIP_CSVExportController();
        pageRef.getParameters().put('listid', '');
        pageRef.getParameters().put('Object', '');
        listviewAPIctrl.fetchListviewRecords();
        Test.StopTest();
        system.assertNotEquals(listviewAPIctrl.fileName,'Account_All_Account_'+Datetime.now().format());
        
    }
}