public with sharing class VFP_QuoteRelatedItemsController {
ApexPages.StandardSetController stdSetController;
public List <QuoteLineItem> lineItemsSelected = New List <QuoteLineItem>();
List<QuoteLineItem> itemsToUp = new List<QuoteLineItem>();
List<SObject> QuoteLineItems = new List<SObject>();
public Boolean isNoSelecteItem { get; set; }
public Integer inputValue { get; set; }
public QuoteLineItem qli { get; set; }

/*public Integer inputValueTraitement { get; set; }
public Integer inputValueTransport { get; set; }
public Integer inputValueConditionnement { get; set; }
public String RoundingType { get; set; }
public String RoundingTypeTrait { get; set; }
public String RoundingTypePrest { get; set; }
public String RoundingTypeCondi { get; set; } */

public VFP_QuoteRelatedItemsController(ApexPages.StandardSetController controller) {
	System.debug('Start VFP_QuoteRelatedItemsController----: ');
	isNoSelecteItem = false;
	stdSetController = controller;
	lineItemsSelected = [select id, UnitPrice, BuyingPrice__c  from QuoteLineItem where id IN:stdSetController.getSelected()];
}

public void init(){

	System.debug('List Records : '+lineItemsSelected);
	this.qli = new QuoteLineItem();
	this.qli.UnitPrice = 0;

    for(QuoteLineItem it : lineItemsSelected){
        if(it.BuyingPrice__c == null){
            isNoSelecteItem = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Prix d\'achat non renseigné ,veuillez renseigner le prix d\'achat'));
        }
    }
	If (lineItemsSelected.Size() == 0)
	{
        isNoSelecteItem = true;
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Veuillez sélectionner un enregistrement avant de procéder à l\'augmentation !'));
	}	

}

private QuoteLineItem addpercentage(QuoteLineItem a,Double Value, String RoundingType){
	a.UnitPrice=a.BuyingPrice__c*(Value*0.01 + 1);	
	/* if(RoundingType.equals('supp')) {
		a.UnitPrice=a.UnitPrice.round(System.RoundingMode.CEILING);
	}else if(RoundingType.equals('auto')) {
		a.UnitPrice=a.UnitPrice.round();
	} */
	return a;
}


public List<QuoteLineItem> increaseValues(List<QuoteLineItem> lstItem, Double inputValue){
	System.debug('VFP_QuoteRelatedItemsController.increaseValues --> Start');
	
	List<QuoteLineItem> itemsToUpdate= new  List<QuoteLineItem>();	
    String RoundingType = 'no';

	//Use the same value to update all record
	if(inputValue!=null && inputValue>0) {
		for(QuoteLineItem it: lstItem) {
            it=addpercentage(it,inputValue,RoundingType);
            itemsToUpdate.add(it);
		}
	}	
	System.debug('LC_ContractsList_AugTarifCtrl.increaseValues --> End ');
	return itemsToUpdate;
}


public pageReference save(){
    System.debug('VFP_QuoteRelatedItemsController.save -->qli.UnitPrice  Start ');	
	try{
		Double inputValue = qli.UnitPrice;	
		System.debug('VFP_QuoteRelatedItemsController.save -->qli.UnitPrice  '+qli.UnitPrice);	
        List<QuoteLineItem> itemsToUp = increaseValues(lineItemsSelected, inputValue) ;
		if (itemsToUp.size()>0) {
            System.debug('VFP_QuoteRelatedItemsController.save.itemsToUp --> Update '+itemsToUp);	
			update itemsToUp;
		}	
      
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Enregistrement(s) mise(nt) à jour avec succès !'));
	}
	catch(Exception e) {
		System.debug('Error : '+e);
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Une erreur est survenue, contacter votre administrateur.'+ e));
	}
	return stdSetController.cancel();

}

public pageReference returnBack()
{
	return stdSetController.cancel();
}




}