/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-07-02
* @systemLayer    Controller         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Controller class for button that copies the price from last year
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class VFC_DontIncrease {
    public List<Amendment__c> AmendmentList {get;set;}
    public VFC_DontIncrease(ApexPages.StandardSetController controller){
        // retrieve selected amendments
        if(controller.getSelected().size()>=1){
            this.AmendmentList=[SELECT PriceCurrentYear__c,ByPassContract__c,PriceNextYear__c,Contrat2__r.RecordLocked__c FROM Amendment__c WHERE ID IN:controller.getSelected()];
        }else{
            this.AmendmentList = new List<Amendment__c>();
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Veuillez selectionner un avenant');
            ApexPages.addMessage(errorMessage);
        }
    }
    // checks if amendments are not locked and sets the price of last year to this year. 
    public PageReference dontIncrease(){
        Boolean locked=false;
        PageReference returnPage=ApexPages.currentPage();
        List<Amendment__c> AmendmentToUpdate=new List<Amendment__c>();
        If(this.amendmentList!=null && this.amendmentList.size()>0){
            for(Amendment__c am: this.amendmentList){
                if(am.PriceCurrentYear__c!=null && am.Contrat2__r.RecordLocked__c==false){
                    am.PriceNextYear__c=am.PriceCurrentYear__c;
                    am.ByPassContract__c=true;
                    AmendmentToUpdate.add(am);
                }else{
                    if(am.Contrat2__r.RecordLocked__c){
                        locked=true;
                    }
                }
            }
        }
        If(AmendmentToUpdate.size()>0 && !locked){
            Update AmendmentToUpdate;
            ReturnPage= new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        }else{
            if(locked) {
                ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'L\'avenant sélectionné est soumis ou validé et ne peut pas être modifié');
                ApexPages.addMessage(errorMessage);
            }
            ReturnPage=null;
        }
        return ReturnPage;
    }
}