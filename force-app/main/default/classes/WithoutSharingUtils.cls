public without sharing class WithoutSharingUtils {
    public static Decimal getIndexMax(){
        AggregateResult[] result=[SELECT MAX (RegroupmentIndex__c) regroupmentIndexMax FROM Contract__c];
        return (Decimal)result[0].get('regroupmentIndexMax');
    }
}