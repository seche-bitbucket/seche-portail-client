@isTest
public class WST_ManageEliminationControllerTest {
    
    //FINISHED
    @isTest
    public static void WST_ManageEliminationControllerTest(){

         Test.startTest();
        //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        
        //insert WST_Waste_Statement
        insert vWst1;
       
		        
        //create controller
        Test.setCurrentPage(Page.WST_ManageEliminationOpportunity);
        ApexPages.StandardController stdController = new ApexPages.StandardController(vWst1);

        
        //call the method
        WST_ManageEliminationController vResult = new WST_ManageEliminationController(stdController);
        System.assertNotEquals(null, vResult);
        Test.stopTest(); 
    }
    
    
    //FINISHED
    @isTest
    public static void manageWSTToEliminateTest(){

         Test.startTest();
        //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        
        //insert WST_Waste_Statement
        insert vWst1;
       
		        
        //create controller
        Test.setCurrentPage(Page.WST_ManageEliminationOpportunity);
        ApexPages.StandardController stdController = new ApexPages.StandardController(vWst1);

        
        //call the method
        WST_ManageEliminationController vResult = new WST_ManageEliminationController(stdController);
        PageReference PageRef  =  WST_ManageEliminationController.manageWSTToEliminate();
        System.debug('manageWSTToEliminateTest.vResult =' + vResult);  
        System.debug('manageWSTToEliminateTest.PageRef =' + PageRef.getRedirect());  
        System.assertNotEquals(null, vResult);
        System.assertNotEquals(null, PageRef);
        System.assertEquals(true, PageRef.getRedirect());
        Test.stopTest(); 
    }
    
}