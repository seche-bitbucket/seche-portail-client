/*
 * ------------------------------------------------------------------------------------------------------------------
 * -- - Name : VFC_UpdateCONTypeDeConventionTest
 * -- - Author : NLE
 * -- - Company : Capgemini
 * -- - Purpose : Test class for VFC_UpdateCONTypeDeConvention class
 * --
 * -- Maintenance History:
 * --
 * -- Date 	Name Version Remarks
 * -- ----------- ---- ------- -------------------------------------------------------------------------------------
 * -- 06-08-2019 NLE 1.0 Initial version 
 * --------------------------------------------------------------------------------------------------------------------
 */

@isTest
public with sharing class VFC_UpdateCONTypeDeConventionTest {
	@isTest
    public static void testGetTypeDeConventionValues(){
       	 List<Contract__c> listC = new List<Contract__c>();
         ApexPages.StandardSetController sc = new ApexPages.standardSetController(listC);        
         VFC_UpdateCONTypeDeConvention updateConv = new VFC_UpdateCONTypeDeConvention(sc);    
		 List<Schema.PicklistEntry>  typeDeConventionPicklist = Contract__c.Type_de_convention__c.getDescribe().getPicklistValues();
		 System.assertEquals( typeDeConventionPicklist[0].getValue(), updateConv.getTypeDeConventionValues().substringBefore(','));     	        
    }
    @isTest
    public static void testGetContractsId(){
        Contract__c c1 = new Contract__c();
        Contract__c c2 = new Contract__c();
        List<Contract__c> listC = new List<Contract__c>();
        listC.add(c1);
        listC.add(c2);
        insert listC ; 
        ApexPages.StandardSetController sc = new ApexPages.standardSetController(listC);        
        VFC_UpdateCONTypeDeConvention updateConv = new VFC_UpdateCONTypeDeConvention(sc);        
        updateConv.contractsList = listC ;
        
        updateConv.getContractsId();
     	System.assertEquals( ((List<Id>)JSON.deserialize(updateConv.getContractsId(), List<Id>.class))[0], listC[0].Id);        
    }
    
    @isTest
    public static void testUpdateCheck(){
        Contract__c c1 = new Contract__c();
        Contract__c c2 = new Contract__c();
        List<Contract__c> listC = new List<Contract__c>();
        listC.add(c1);
        listC.add(c2);
        insert listC ; 
        ApexPages.StandardSetController sc = new ApexPages.standardSetController(listC);        
        VFC_UpdateCONTypeDeConvention updateConv = new VFC_UpdateCONTypeDeConvention(sc);        
        updateConv.contractsList = listC ;
        System.assertEquals(true, updateConv.isUpdatable);
    }
    
   @isTest
   public static void testUpdateCheckErrorFiliale(){
        Contract__c c1 = new Contract__c();
        Contract__c c2 = new Contract__c();
        c1.Filiale__c = String_Helper.CON_Filiale_Alcea;
        c2.Filiale__c = String_Helper.CON_Filiale_Drimm;
        List<Contract__c> listC = new List<Contract__c>();
        listC.add(c1);
        listC.add(c2);
        insert listC ; 
        ApexPages.StandardSetController sc = new ApexPages.standardSetController(listC);        
        VFC_UpdateCONTypeDeConvention updateConv = new VFC_UpdateCONTypeDeConvention(sc);        
        updateConv.contractsList = listC ;       
       	updateConv.updateCheck();
        System.assertEquals(false, updateConv.isUpdatable);
    }
    
   @isTest
   public static void testUpdateCheckErrorEmptyContractsList(){      	
        List<Contract__c> listC = new List<Contract__c>();       
        ApexPages.StandardSetController sc = new ApexPages.standardSetController(listC);        
        VFC_UpdateCONTypeDeConvention updateConv = new VFC_UpdateCONTypeDeConvention(sc);              
       	updateConv.updateCheck();
        System.assertEquals(false, updateConv.isUpdatable);
    }
}