public with sharing class VFC_GenericSendReminderController {
    ApexPages.StandardSetController stdSetController;
    //List <Opportunity> opprt {get;set;}
    private Id recordId;
    private FIP_FIP__c fip;
    Boolean isNoSelectedOpp;
    Boolean isQuote;
    String status;
    String syncedQuoteId; 
    Quote syncedQuote;
    String prefix;
    
    public Boolean getIsNoSelectedOpp()
    {
        return isNoSelectedOpp;
    }
    
    public VFC_GenericSendReminderController(ApexPages.StandardSetController controller) {
        System.debug('Start VFC_GenericSendReminderController----: ');	
        recordId = ApexPages.currentPage().getParameters().get('id'); 
        if(recordId.getSObjectType() == FIP_FIP__c.SObjectType){
            this.fip = [SELECT Id, Name FROM FIP_FIP__c WHERE Id = :recordId]; 
        }
        if(recordId.getSObjectType() == Quote.SObjectType) {
            this.status = [SELECT Status FROM Quote WHERE Id = :recordId][0].Status;
            this.syncedQuoteId = [SELECT Id, Opportunity.SyncedQuoteId FROM Quote WHERE Id = :recordId][0].Opportunity.SyncedQuoteId;
            this.syncedQuote = [SELECT Id, Name, Status FROM Quote WHERE Id = :syncedQuoteId];
        }
        if(recordId.getSObjectType() == Quote.SObjectType) {
            this.status = [SELECT Status FROM Quote WHERE Id = :recordId][0].Status;
            this.syncedQuoteId = [SELECT Id, Opportunity.SyncedQuoteId FROM Quote WHERE Id = :recordId][0].Opportunity.SyncedQuoteId;
            this.syncedQuote = [SELECT Id, Name, Status FROM Quote WHERE Id = :syncedQuoteId];
            this.prefix = 'Ce devis';
        }
        if(recordId.getSObjectType() == Attestation__c.SObjectType) {
            this.status = [SELECT Status__c FROM Attestation__c WHERE Id = :recordId][0].Status__c;
            this.prefix = 'Cette attestation';
        }
        System.debug('Start VFC_GenericSendReminderController----: '+ this.status);	
        stdSetController = controller;
        isNoSelectedOpp = false;
    }
    
    public pageReference init(){   
        //variable isTestRunnig have not the same function with Test.isRunningTest()
        System.debug(status);
        
        if(this.status == 'Signé' || this.status == 'Refusé') {
            isNoSelectedOpp = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,' '+this.prefix+' a déjà été '+ this.status.toLowerCase()+', l\'envoi de rappel est desactivé'));
            return null;
        }

        if(this.syncedQuote.Status == 'Signé') {
            isNoSelectedOpp = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,' Le devis synchronisé est déjà signé' ));
            return null;
        }

        if(recordId.getSObjectType() == CAP__c.SObjectType) {
            CAP__c cap = [SELECT NbSendReminder__c, LastReminderDate__c FROM CAP__c WHERE Id = :recordId];
            Date date3Days = System.today() - 3;
            if (cap.LastReminderDate__c > date3Days) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Un rappel a déjà été envoyé pour ce CAP il y a moins de 3 jours.' ));
                return null; 
            }
        }

        if(stdSetController.getRecords().Size()>0){
            String result = '';
            result = ConnectiveV4ApiManager.sendRedminder(recordId);	
            if(result =='OK') {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Rappel envoyé avec succès'));
                return goback();
            }else if (result =='KO') {
                isNoSelectedOpp = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Un rappel a déjà été envoyé, ou le document est déjà signé, ou le service Connective n\'est pas disponible'));
                return null;
            }			  
        }
        else{
            isNoSelectedOpp = true;		
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Veuillez d\'abord créer le document!'));
            return null;
        }
        return null;
    }
    
    public pageReference goback()
    {
        return stdSetController.cancel();
    }
    
}