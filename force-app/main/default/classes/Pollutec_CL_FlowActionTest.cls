@isTest(SeeAllData=false)
public class Pollutec_CL_FlowActionTest {
    
    @isTest
    public static void test() {
        
        Lead ld = TestFactory.generateLead();
        
        Pollutec_CL_FlowAction.Inputs inp = new Pollutec_CL_FlowAction.Inputs();
        inp.recordId = ld.Id;
        
        System.assertEquals(Pollutec_CL_FlowAction.detect(new List<Pollutec_CL_FlowAction.Inputs>{inp})[0], 'Lead');
        
    }

}