/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-07-02
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Controller class for button that puts the contracts in Augmentation face à face status
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class VFC_IncreaseFaceToFace {
    public List<Amendment__c> AmendmentList {get;set;}
    public VFC_IncreaseFaceToFace(ApexPages.StandardSetController controller){
        // retrieve the amendments selected
        if(controller.getSelected().size()>=1){
            this.AmendmentList=[SELECT PriceCurrentYear__c,ByPassContract__c,PriceNextYear__c,Contrat2__r.RecordLocked__c  FROM Amendment__c WHERE ID IN:controller.getSelected()];
        }else{
            this.AmendmentList = new List<Amendment__c>();
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Veuillez selectionner un avenant');
            ApexPages.addMessage(errorMessage);
        }
    }
    // if records are not locked , Set BypassContract to true so that the contract is regarded as Face To Face type Contract
    public PageReference increase(){
        Boolean locked=false;
        PageReference returnPage=ApexPages.currentPage();
        List<Amendment__c> AmendmentToUpdate=new List<Amendment__c>();
        If(this.amendmentList!=null && this.amendmentList.size()>0){
            for(Amendment__c am: this.amendmentList){
                if(am.Contrat2__r.RecordLocked__c==false){
                    am.PriceNextYear__c=null;
                    am.ByPassContract__c=true;
                    AmendmentToUpdate.add(am);
                }else{
                    if(am.Contrat2__r.RecordLocked__c){
                        locked=true;
                    }
                }
            }
        }
        If(AmendmentToUpdate.size()>0 && !locked){
            Update AmendmentToUpdate;
            ReturnPage= new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        }else{
            if(locked) {
                ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'L\'avenant sélectionné est soumis ou validé et ne peut pas être modifié');
                ApexPages.addMessage(errorMessage);
            }
            ReturnPage=null;
        }
        return ReturnPage;
        
    }
}