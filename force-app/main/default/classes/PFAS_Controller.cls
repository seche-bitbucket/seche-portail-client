/**
 * @description       : 
 * @author            : MAIT - @moidriss
 * @group             : 
 * @last modified on  : 12-05-2023
 * @last modified by  : MAIT - @moidriss
**/
public without sharing class PFAS_Controller {

    @AuraEnabled
    public static List<CAP__c> getCapByContactId(Id contactId){
        try {
            return [SELECT Id, Name, HasPfas__c, WasteName__c, ComponentStatus__c, Producteur__c, PolymeresList__c, (SELECT Id, Name, ComponentType__c, CasNumber__c, MaximumPercentage__c, MinimumPercentage__c, PfasType__c FROM Constituants__r) FROM CAP__c WHERE Contact__c = :contactId AND (ComponentStatus__c = 'Brouillon' OR ComponentStatus__c = 'Refusé')];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Boolean checkIfSentForSig(List<Id> capsIds){
        try {
            return [SELECT Id FROM DocumentsForSignature__c WHERE CAP__c IN :capsIds].size() > 0;
        } catch (Exception e) {
            return false;
        }
    }

    @AuraEnabled
    public static List<Map<String, String>> getSubstances(){
        try {
            List<Map<String, String>> componentTypeValues = new List<Map<String, String>>();
            for(PicklistEntry picklistEntry: Component__c.ComponentType__c.getDescribe().getPicklistValues()) {
                componentTypeValues.add(new Map<String, String> {
                    'label' => picklistEntry.getLabel(),
                    'value' => picklistEntry.getValue()
                });
            }

            return componentTypeValues;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Object sendData(String processName, String jsonData){
        try {
            Map<String, Object> flowArgs = new Map<String, Object>();
            flowArgs.put('process', processName);
            switch on processName {
                when 'updateCAP' {
                    flowArgs.put('CAPRecords', JSON.deserialize(jsonData, List<CAP__c>.Class));
                    return callFlow(flowArgs, 'CAPRecords');
                }

                when 'updateFIP' {
                    flowArgs.put('FIPRecord', JSON.deserialize(jsonData, FIP_FIP__c.Class));
                    return callFlow(flowArgs, 'FIPRecord');
                }
                
                when 'insertComponents', 'updateComponents' {
                    flowArgs.put('ComponentsRecords', JSON.deserialize(jsonData, List<Component__c>.Class));
                    return callFlow(flowArgs, 'ComponentsRecords');
                }

                when 'deleteComponents' {
                    flowArgs.put('ComponentsIds', JSON.deserialize(jsonData, List<Id>.Class));
                    return callFlow(flowArgs, null);
                }

                when else {
                    throw new AuraHandledException('Unhandled process ' + processName);
                }
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    private static Object callFlow(Map<String, Object> params, String outputArg) {
        Flow.Interview.PFAS_Flow_Controller pfasControllerFlow = new Flow.Interview.PFAS_Flow_Controller(params);
        pfasControllerFlow.start();

        return (outputArg != null) ? pfasControllerFlow.getVariableValue(outputArg) : null;
    }

    public class PFAS_ARGS {
        @InvocableVariable
        public ID contactId;
        @InvocableVariable
        public List<Id> capsIds;
    }

    @InvocableMethod(label='Send CAP with Connective' category='PFAS')
    public static void SendCAPToSign (List<PFAS_ARGS> args) {
        send(args[0].contactId, args[0].capsIds);
    }
    
    @future(callout=true)
    private static void send(Id contactId, List<Id> capIds) {
        Boolean runFinally = false;
        try {
            ConnectiveV4ApiManager.createPackage(ConnectiveV4ApiUtils.createPackage(contactId, contactId), contactId);
            runFinally = true;
        } catch(Exception e) {
            runFinally = false;
            return;
        } finally {
            // Update
            if(runFinally && !capIds.isEmpty()) {
                List<CAP__c> caps = new List<CAP__c>();

                for(Id capId: capIds) {
                    caps.add(new CAP__c(Id = capId, ComponentStatus__c = 'Envoyé pour signature'));
                }

                update caps;
            }
        }
    }

}