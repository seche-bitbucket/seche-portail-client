public with sharing class VFC_GenericGetStatusController {
    ApexPages.StandardSetController stdSetController;
    private Id id;
    Boolean isNoSelectedOpp;
    String syncedQuoteId; 
    Quote syncedQuote;
    
    public Boolean getIsNoSelectedOpp()
    {
        return isNoSelectedOpp;
    }
    
    public VFC_GenericGetStatusController(ApexPages.StandardSetController controller) {
        System.debug('Start VFC_GenericGetStatusController----: ');
        id = ApexPages.currentPage().getParameters().get('id');	
        if(id.getSObjectType() == Quote.SObjectType) {
            this.syncedQuoteId = [SELECT Id, Opportunity.SyncedQuoteId FROM Quote WHERE Id = :id][0].Opportunity.SyncedQuoteId;
            this.syncedQuote = [SELECT Id, Name, Status FROM Quote WHERE Id = :syncedQuoteId];
        }
        stdSetController = controller;
        isNoSelectedOpp = false;
    }
    
    public pageReference init(){
        System.debug('List Records : '+stdSetController.getSelected());
        
        if(this.syncedQuote.Status == 'Signé') {
            isNoSelectedOpp = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,' Le devis synchronisé est déjà signé' ));
            return null;
        }

        if(stdSetController.getRecords().Size()>0){
            String result = '';
            result = ConnectiveV4ApiManager.updateStatus(id);
            
            if(String.isNotBlank(result) && result == 'OK') {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Status mis à jour'));
                return goback();
            }
            else{
                isNoSelectedOpp = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Erreur lors de la récupération du statut, veuillez  vérifier le service Connective'));
            }
            return null;
        }
        else{
            isNoSelectedOpp = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Veuillez d\'abord créer le document!'));
        }
        return null;
    }
    
    public pageReference goback()
    {
        return stdSetController.cancel();
    }
}