/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ConnectiveMockSendReminder
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ConnectiveMockSendReminder
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 16-06-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
@isTest
global class ConnectiveMockSendReminder implements  HTTPCalloutMock{
	global HTTPResponse respond(HTTPRequest req){
        HttpResponse res = new HTTPResponse();
        res.setHeader('Content-Type', 'application/JSON');
        res.setBody('');
        req.setMethod('POST');
        res.setStatusCode(201);
        return res;
    }
}