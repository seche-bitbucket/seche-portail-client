public class FlowStatusChange {
    public String FlowId{get;set;}
    public String Action{get;set;}
    public Boolean SendNotification{get;set;}
    public static FlowStatusChange deserializejson(String json){
        return (FlowStatusChange) System.JSON.deserialize(json, FlowStatusChange.class);
    }
        public static String populatejson(String FlId,String Action,Boolean SendNot){
		FlowStatusChange Fsg=new FlowStatusChange();
        Fsg.FlowId= FlId;
        Fsg.Action= Action;
        Fsg.SendNotification=SendNot;
        String jsonBody = json.serialize(Fsg);
        return jsonBody;
    }
}