@isTest (SeeAllData=true)
public class OpportunityTriggerTest {
    
    @isTest
    public static void recursive() {
        checkRecursive.overrideVals();
        checkRecursive.runforRights();
        checkRecursive.runOnce();
        checkRecursive.runOnceAfter();
        checkRecursive.runOnceBefore();
        checkRecursive.runOnceTriggerLoads();
        checkRecursive.setFalse();
        checkRecursive.setTrue();
    }
        
    public static testMethod void TestOpportunityTrigger() {
      
      //Get a profile to create User
        Profile p = [select id from profile where name = :'Chargé(e) d\'affaires' limit 1];
        
        String testemail = 'director_-_User_test@test.com';
        
        User director = new User(profileId = p.id, username = testemail, email = testemail,
                                 SubsidiaryCreationRight__c = 'Alcea',
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        insert director;
    
        String testemail2 = 'assistant_-_User_test@test.com';
        String testemail3 = 'assistant2_-_User_test@test.com';
        
        User assistant = new User(profileId = p.id, username = testemail2, email = testemail2,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname', IsActive=true);
        insert assistant;

        User assistant2 = new User(profileId = p.id, username = testemail3, email = testemail3,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname', IsActive=true);
        insert assistant2;
        
        Equipe__c e = new Equipe__c();
        e.Directeur__c = director.ID;
        e.Assistant__c = assistant.ID;
        e.Filiere__c = 'Physico';
        
        insert e;
        
        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Salesman__c = director.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '03 00 00 00 00';
        insert c;
       
        
        Opportunity opp = new Opportunity();
        opp.Name = 'test';
        opp.Salesman__c = director.Id;
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        opp.Assistant__c = assistant.Id;
        
        Test.startTest();
        try {
            insert opp;
        }
        catch (Exception z) {
            System.assert(z.getMessage().contains('Vous ne pouvez pas choisir cet assistant car il n\'est pas rattaché à la filiale porteuse de l\'offre'));
        }

        assistant.SubsidiaryCreationRight__c = 'Tredi Hombourg';
        update assistant;
        insert opp;

        opp.Assistant__c = assistant2.Id;
        try {
            update opp;
        }
        catch (Exception z) {
            System.assert(z.getMessage().contains('Vous ne pouvez pas choisir cet assistant car il n\'est pas rattaché à la filiale porteuse de l\'offre'));
        }
        
        //Test all recordType "intermediaire"
        List<RecordType> lstRT = [Select Id, DeveloperName From RecordType Where DeveloperName LIKE '%Direct' ];
        
               
        Opportunity oppF = new Opportunity();
        oppF.Name = 'TestFille';
        oppF.Salesman__c = director.Id;
        oppF.StageName = 'Contractualisation';
        oppF.CloseDate = System.today().addDays(10);
        oppF.AccountId = a.Id;
        oppF.TonnageN__c = 0;
        oppF.Filiale__c = 'Tredi Hombourg';
        oppF.Filiere__c = 'Physico';
        oppF.ContactName__c = c.Id;
        oppF.Opportunit_mere__c = opp.ID;
        insert oppF;
        
        for(OpportunityTeamMember otm : [select UserId from OpportunityTeamMember where OpportunityId = : opp.ID]){
            
            if(otm.UserID == director.ID){
                System.assertEquals(otm.UserID, director.ID);
            }else if(otm.UserID == assistant.ID){
                System.assertEquals(otm.UserID, assistant.ID);
            }
            
        }
        
        for(RecordType rt : lstRt){
            oppF.RecordTypeId = rt.ID;
            update oppF;
        }
        
        Test.stopTest();
        
    }
    
    public static testMethod void TestSubOppRename() {
        
              
        //Get a profile to create User
        Profile p = [select id from profile where name IN ('Utilisateur standard', 'Standard User')  limit 1];
        String testemail = 'director_-_User_test@test.com';
        
        User salesman = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname', IsActive=true);
        
        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '03 00 00 00 00';
        insert c;
       
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Std Opp';
        opp.Salesman__c = salesman.ID;
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        
      
        insert opp;
        
        
        
        //Retrive Sub Opp RT
        RecordType RT = [Select Id, DeveloperName From RecordType Where DeveloperName = 'OPP_OpportuniteIntraGroupe' ];
        Test.startTest();
        
        Opportunity oppF2 = new Opportunity();
        oppF2.Name = 'monOppSub3';
        oppF2.RecordTypeId = RT.ID;
        oppF2.StageName = 'Contractualisation';
        oppF2.CloseDate = System.today().addDays(10);
        oppF2.AccountId = a.Id;
        oppF2.TonnageN__c = 0;
        oppF2.Filiale__c = 'Tredi Hombourg';
        oppF2.Filiere__c = 'Physico';
        oppF2.ContactName__c = c.Id;
        oppF2.OpportuniteMereIntra__c = opp.ID;
        insert oppF2;
        oppF2 = [Select Id, Name From Opportunity Where Id = : OppF2.ID];
        
        //System.assertEquals('ST - monOppSous3', oppF2.name);        
               
        Opportunity oppF = new Opportunity();
        oppF.Name = 'OppSUBTest1';
        oppF.RecordTypeId = RT.ID;
        oppF.StageName = 'Contractualisation';
        oppF.CloseDate = System.today().addDays(10);
        oppF.AccountId = a.Id;
        oppF.TonnageN__c = 0;
        oppF.Filiale__c = 'Tredi Hombourg';
        oppF.Filiere__c = 'Physico';
        oppF.ContactName__c = c.Id;
        oppF.OpportuniteMereIntra__c = opp.ID;
        insert oppF;
		 oppF = [Select Id, Name From Opportunity Where Id = : OppF.ID];
                
        //System.assertEquals('ST - OppSusTest1', oppF.name);

        
        Test.stopTest();
        
    }
    
    
    public static testMethod void TestOpportunitySubscription() {
        
         //Get a profile to create User
        Profile p = [select id from profile where name IN ('Utilisateur standard', 'Standard User') limit 1];
        //Getting Opp RT
		RecordType RT = [Select Id, DeveloperName From RecordType Where DeveloperName = 'OPP_06_AutreOffreSecteurPriveStandard' ];        
        String testemail = 'director_-_User_test@test.com';
        
        User salesman = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname', IsActive=true);
        insert salesman;
    
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User assistant = new User(profileId = p.id, username = testemail2, email = testemail2,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname', IsActive=true);
        insert assistant;
        

        
        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '03 00 00 00 00';
        insert c;
       
                    
            
            Opportunity opp = new Opportunity();
            opp.Name = 'test';
            opp.Salesman__c = salesman.Id;
            opp.StageName = 'Contractualisation';
            opp.CloseDate = System.today().addDays(10);
            opp.AccountId = a.Id;
            opp.TonnageN__c = 0;
            opp.RecordTypeId = RT.ID;
            opp.Filiale__c = 'Tredi Hombourg';
            opp.Filiere__c = 'Physico';
            opp.ContactName__c = c.Id;
            
            Test.startTest();
            insert opp;
            
			List<EntitySubscription> lstSE = [Select ID, SubscriberId From EntitySubscription Where ParentID = : opp.ID];
            
            System.assertEquals(2, lstSE.size());
            
            Test.stopTest();
        
    }

    public static testMethod void TestOpportunityWithQuote() {
        
        //Get a profile to create User
       Profile p = [select id from profile where name IN ('Utilisateur standard', 'Standard User') limit 1];
       //Getting Opp RT
       RecordType RT = [Select Id, DeveloperName From RecordType Where DeveloperName = 'OPP_06_AutreOffreSecteurPriveStandard' ];        
       String testemail = 'director_-_User_test@test.com';
       
       User salesman = new User(profileId = p.id, username = testemail, email = testemail,
                                   emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                   languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                   alias='cspu', lastname='lastname', IsActive=true);
       insert salesman;
   
       String testemail2 = 'assistant_-_User_test@test.com';
       
       User assistant = new User(profileId = p.id, username = testemail2, email = testemail2,
                                   emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                   languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                   alias='cspu', lastname='lastname', IsActive=true);
       insert assistant;
       

       
       Account a = new Account();
       a.Name = 'testAccName';
       a.CustomerNature__c = 'Administration';
       a.Industry = 'Trader';
       a.CurrencyIsoCode = 'EUR';
       a.Producteur__c = false;
       a.BillingPostalCode = '00000';
       a.NAF_Number__c = '1234A';
       insert a;
       
       Contact c = new Contact();
       c.LastName = 'testName';
       c.AccountId = a.Id;
       c.Email = 'aaa@yopmail.com';
       c.Phone = '03 00 00 00 00';
       insert c;
      
                   
           
           Opportunity opp = new Opportunity();
           opp.Name = 'test';
           opp.Salesman__c = salesman.Id;
           opp.StageName = 'Contractualisation';
           opp.CloseDate = System.today().addDays(10);
           opp.AccountId = a.Id;
           opp.TonnageN__c = 0;
           opp.RecordTypeId = RT.ID;
           opp.Filiale__c = 'Tredi Hombourg';
           opp.Filiere__c = 'Physico';
           opp.ContactName__c = c.Id;
           
           Test.startTest();
           insert opp;

           Quote q = new Quote();
           q.OpportunityId = opp.ID;
           q.Name = opp.Name;
           q.DateDevis__c = Date.today();
           q.QuoteCode__c=opp.Name;
           q.ContactId = opp.ContactName__c;
           q.Email = 'secheemail@gmail.com';
           q.Phone = opp.ContactName__r.Phone;
           q.Fax = opp.ContactName__r.Fax;
           q.TECH_UID__c='9001';
           q.BillingStreet = opp.Account.BillingStreet;
           q.BillingCity = opp.Account.BillingCity;
           q.BillingState = opp.Account.BillingState;
           q.BillingPostalCode = opp.Account.BillingPostalCode;
           q.BillingCountry = opp.Account.BillingCountry;
           insert q;

          // after inserting the quote we need to update the opportunity
        opp.SyncedQuoteId = q.Id;
        
        checkRecursive.overrideVals();
        
        update opp;
           
           List<EntitySubscription> lstSE = [Select ID, SubscriberId From EntitySubscription Where ParentID = : opp.ID];
           
           System.assertEquals(2, lstSE.size());
           
           Test.stopTest();
       
   }
    
    @IsTest(SeeAllData=true) 
    public static void TestOpportunityCreationCheckTrigger(){
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Assistante commerciale' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname');
        
        System.RunAs(currentUser) { 
            
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aucun', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try{
                insert ac;
            }catch(Exception e){
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '0606060606');
            
            try{
                insert contactTest;
            }catch(Exception e1){
            }
            
            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];
            
            //Create Opportunity
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
            Test.startTest();
            try{
                insert opportTest;
            }catch(Exception e2){
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }     
            
            List<GroupMember> grpMember = [Select ID, userOrGroupID  From GroupMember Where GroupID = : [Select ID, Name From Group Where name = :opportTest.Filiale__c] AND userOrGroupID =:opportTest.OwnerId ];
            System.assertEquals(0, grpMember.size());
            Test.stopTest();
        }
    }

    @IsTest(SeeAllData=true) 
    public static void TestOpportunityCreationDiffGroup(){
        
        //Get a profile to create User
        Profile p = [select id from profile where name = 'Solution Manager' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname', SubsidiaryCreationRight__c='Drimm');
        
        System.RunAs(currentUser) { 
            
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aucun', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try{
                insert ac;
            }catch(Exception e){
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '0606060606');
            
            try{
                insert contactTest;
            }catch(Exception e1){
            }
            
            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];
            
            //Create Opportunity
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234);
            Test.startTest();
            try{
                insert opportTest;
            }catch(Exception e2){
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }     
            
            List<GroupMember> grpMember = [Select ID, userOrGroupID  From GroupMember Where GroupID = : [Select ID, Name From Group Where name = :opportTest.Filiale__c] AND userOrGroupID =:opportTest.OwnerId ];
            System.assertEquals(0, grpMember.size());
            Test.stopTest();
        }
    }

    @IsTest(SeeAllData=true) 
    public static void TestOpportunityCreationForAdmin(){
        
        //Get a profile to create User
        Profile p = [select id from profile where name = 'Solution Manager' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname', SubsidiaryCreationRight__c='Drimm');
        
        System.RunAs(currentUser) { 
            
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aucun', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try{
                insert ac;
            }catch(Exception e){
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '0606060606');
            
            try{
                insert contactTest;
            }catch(Exception e1){
            }
            
            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_OpportuniteIntraGroupe' LIMIT 1];
            
            //Create Opportunity
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234);
            Test.startTest();
            try{
                insert opportTest;
            }catch(Exception e2){
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }     
            
            List<GroupMember> grpMember = [Select ID, userOrGroupID  From GroupMember Where GroupID = : [Select ID, Name From Group Where name = :opportTest.Filiale__c] AND userOrGroupID =:opportTest.OwnerId ];
            System.assertEquals(0, grpMember.size());
            Test.stopTest();
        }
    }


    @IsTest(SeeAllData=true) 
    public static void TestOpportunityUpdate(){
        
        //Get a profile to create User
        Profile p = [select id from profile where name ='System Administrator' or name ='Administrateur système' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname', SubsidiaryCreationRight__c='Alcea');
        
        System.RunAs(currentUser) { 
            
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aucun', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try{
                insert ac;
            }catch(Exception e){
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '0606060606');
            
            try{
                insert contactTest;
            }catch(Exception e1){
            }
            
            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_OpportuniteIntraGroupe' LIMIT 1];
            
            //Create Opportunity
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234);
            insert opportTest;
            Test.startTest();
            try{               
                opportTest.Filiale__c = 'Alcea';
                update opportTest;
            }catch(Exception e2){
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }     
            
            List<GroupMember> grpMember = [Select ID, userOrGroupID  From GroupMember Where GroupID = : [Select ID, Name From Group Where name = :opportTest.Filiale__c] AND userOrGroupID =:opportTest.OwnerId ];
            System.assertEquals(0, grpMember.size());
            Test.stopTest();
        }
    }

    @IsTest(SeeAllData=true) 
    public static void TestOpportunityAdminWithIntraGroup(){
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Assistante commerciale' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname');
        
        System.RunAs(currentUser) { 
            
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aucun', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try{
                insert ac;
            }catch(Exception e){
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '0606060606');
            
            try{
                insert contactTest;
            }catch(Exception e1){
            }
            
            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_OpportuniteIntraGroupe' LIMIT 1];
            
            //Create Opportunity
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
            Test.startTest();
            try{
                insert opportTest;
            }catch(Exception e2){
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }     
            
            List<GroupMember> grpMember = [Select ID, userOrGroupID  From GroupMember Where GroupID = : [Select ID, Name From Group Where name = :opportTest.Filiale__c] AND userOrGroupID =:opportTest.OwnerId ];
            System.assertEquals(0, grpMember.size());
            Test.stopTest();
        }
    }


    @IsTest(SeeAllData=true) 
    public static void TestOpportunityForForDirCo(){
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Directeur commercial' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@capg.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname', SubsidiaryCreationRight__c='Alcea');
        
        System.RunAs(currentUser) { 
            
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aucun', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try{
                insert ac;
            }catch(Exception e){
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '0606060606');
            
            try{
                insert contactTest;
            }catch(Exception e1){
            }
            
            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];
            
            //Create Opportunity
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
            Test.startTest();
            try{
                insert opportTest;
            }catch(Exception e2){
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }     
            
            List<GroupMember> grpMember = [Select ID, userOrGroupID  From GroupMember Where GroupID = : [Select ID, Name From Group Where name = :opportTest.Filiale__c] AND userOrGroupID =:opportTest.OwnerId ];
            System.assertEquals(0, grpMember.size());
            Test.stopTest();
        }
    }

}