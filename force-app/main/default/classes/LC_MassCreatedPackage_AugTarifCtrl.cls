/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : LC_MassCreatedPackage_AugTarifCtrl
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : LC_MassCreatedPackage_AugTarifCtrl
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 29-05-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
public class LC_MassCreatedPackage_AugTarifCtrl implements Queueable, Database.AllowsCallouts  {

    public Map<Id,Quote> quotes = new Map<Id,Quote>();
    public List<Quote> quotesProcessed = new List<Quote>();
    

    public LC_MassCreatedPackage_AugTarifCtrl(List<Quote> quotes,List<Quote> quotesProcessed){
        this.quotes = new Map<Id,Quote>(quotes);
        if(quotesProcessed == null){
            this.quotesProcessed = new List<Quote>();
        }
        else{
            this.quotesProcessed = quotesProcessed;
        }
    }

    public void execute(QueueableContext context) {
        try{
            for(Id qId : quotes.keySet()){
                String result = '';
                if(Test.isRunningTest()){
                    result = 'OK';
                }
                else{
                    //TODO update with multiple send
                    // String packId = ConnectiveApiUtils.setupPackFromQuoteId(qId);
                    // Boolean successStatus = ConnectiveApiUtils.setPackagePending(packId); //set status so signers gets a notification
                    // if (successStatus){ //if(resultCallPackage == 'OK'){
                    //     //TST
                    //     String bodyStatus = ConnectiveApiManager.getStatusByConnectivePackageId(packId);
                    //     PackageResponse bodyRes = PackageResponse.deserializeResponsejson(bodyStatus);
                    //     Package__c pkg = new Package__c(
                    //         ConnectivePackageId__c=packId,
                    //         ConnectivePackageStatus__c=bodyRes.PackageStatus
                    //     );
                    //     insert pkg;
                    //     //add all docs
                    //     List<DocumentsForSignature__c> docLs = ConnectiveApiUtils.parseStatusRespToDocList(bodyRes, pkg.Id, qId);
                    //     insert docLs;
                    //     result = 'OK';
                    //     //END TST
                    // }
                    PackageV4Wrapper pkg = ConnectiveV4ApiUtils.createPackage(qId, quotes.get(qId).ContactId);
                    String httpResp = ConnectiveV4ApiManager.createPackage(pkg, qId);
                
                    System.debug('v4: '+httpResp);
                    result=httpResp;
                    //result = ConnectiveApiManager.createInstantPackage(qId);
                }
                if(result == 'OK'){
                    Quote qt = quotes.get(qId);
                    this.quotesProcessed.add(qt);
                }
                quotes.remove(qId);
                if(quotes.size()>0){
                    System.enqueueJob(new LC_MassCreatedPackage_AugTarifCtrl(quotes.values(),this.quotesProcessed));
                }
                else{
                    SignatureUtils.sendEmailGroup(this.quotesProcessed,false);
                    updaterecordTypeQuote(this.quotesProcessed);
                }
                break;
            }
        }
        catch(Exception ex){
            Log__c error = new Log__c(
                ApexJob__c= LC_MassCreatedPackage_AugTarifCtrl.class.getName(),
                ErrorMessage__c=ex.getMessage(),
                ExceptionType__c='APEX'
            );
            insert error;
        }
    }

    private void updaterecordTypeQuote(List<Quote> quoteToUpdate){
        List<Quote> quoteUpdated = new List<Quote>();
        RecordType rt=[SELECT Id, Name, DeveloperName, Description FROM RecordType Where DeveloperName='Devis_approuv_Read_Only' LIMIT 1];
        for(Quote q : quoteToUpdate){
            q.Tech_UID__c=GuidUtil.NewGuid();
            q.TECH_isApprouved__c=true;
            q.Status='Envoyé pour signature';
            q.RecordTypeId=rt.Id;
            quoteUpdated.add(q);		
        }
		//Update quotes
		if(quoteUpdated.size() > 0) {
			updateLockedRecords updateQuoteRecords = new updateLockedRecords();
			updateQuoteRecords.updateRecords(quoteUpdated);			
		}	
    }

    // Class for updating locked record (without sharing to by pass locked record)
    without sharing class updateLockedRecords {	  
        public void updateRecords(List<Quote> qt) { 		
            update qt; 
        }
    }

}