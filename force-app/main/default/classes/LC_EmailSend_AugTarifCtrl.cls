public with sharing class LC_EmailSend_AugTarifCtrl {

public static Task createEmailActivity(String subject,String description, String contactId, String quoteId) {

	Task t = new Task(
		Subject = subject,
		Description = description.stripHtmlTags(),
		WhoId = contactId,
		WhatId = quoteId,
		TaskSubtype = 'Email',
		ActivityDate = Date.today(),
		Status = 'Completed');
		return t;
}

public static ContentDocumentLink linkAttToTask(String taskId, ContentVersion cversion) {
	// Create Related file
		ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = cversion.ContentDocumentId;
        cdl.LinkedEntityId = taskId;
        cdl.ShareType = 'V';     
		return cdl;
}

// @AuraEnabled
// public static void resetSendEmail (String quotId, String initialStatus){
// 	Quote q = new Quote();
// 	q.Id = quotId;
// 	q.Status = initialStatus;
// 	q.TECH_UID__c = '';
// 	update q;
// }

@AuraEnabled
public static RequestResponse getEmailValues(String quotId, String TECH_UID){
	String userTheme = UserInfo.getUiThemeDisplayed();
	LC_EmailSend_AugTarifCtrl.RequestResponse resp = new LC_EmailSend_AugTarifCtrl.RequestResponse();
	resp.message = 'Contact manquant ou nombre de pièces-jointes non conforme sur le devis.';
	resp.showToastMode = 'warning';
	resp.mailStatus = false;
	resp.isShowForm = 'false';
	String returnValue='Wait';

	Quote q=[SELECT id, Name,ContactId,Opportunity.Salesman__c,Opportunity.OwnerId,Opportunity.Owner.Email, Quote_Type__c,
	         Opportunity.Owner.Name,Email, QuoteCode__c,TECH_UID__c,Opportunity.Filiale__c,Account.Name FROM Quote WHERE Id=:quotId];
	RecordType rt=[SELECT Id, Name, DeveloperName, Description FROM RecordType Where DeveloperName='Devis_approuv_Read_Only' LIMIT 1];
	List<Contact> contact=[SELECT ID,Name,FirstName,LastName,Email,Phone From Contact WHERE Id=:q.ContactId];
	List<DocumentsForSignature__c> document = new List<DocumentsForSignature__c>();
	document = [SELECT Id, Status__c, FlowId__c,SigningUrl__c,Package__r.ConnectivePackageId__c FROM DocumentsForSignature__c WHERE TECH_ExternalId__c= :quotId Limit 1];

	List<Attachment> att=[SELECT id, Name, body, ContentType,ParentId FROM Attachment WHERE ParentId=:q.Id];
	List<ContentDocumentLink> files  = [SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLink where LinkedEntityId =:q.Id ];
	Messaging.Emailfileattachment fileAttachment = new Messaging.Emailfileattachment();
	String contactGreeting;
	String CommercialText;
	String secheIndustryText;
	String ConnectiveText;
	String greeting;
	String notContact;
	if(files != null && files.size()>0) {
		if(files.size()>0 && contact.size()>0) {
			if(files.size()<2) {
				if(document.isEmpty()==false) {
					resp.message = 'Document déja envoyé pour signature';
					resp.showToastMode = 'warning';
					resp.isShowForm = 'false';
				}else if(files[0].ContentDocument.title.length() > 75){
					resp.message = 'Le nom du fichier est trop long (maximum 75 caractères).';
					resp.showToastMode = 'warning';
					resp.isShowForm = 'false';
				}else{
					/*q.Tech_UID__c=GuidUtil.NewGuid();
					q.TECH_isApprouved__c=true;
					q.Status='Envoyé pour signature';
					q.RecordTypeId=rt.Id;
					update q; */
					User u=[SELECT Id, Name,Signature,email FROM User WHERE Id = :q.Opportunity.Salesman__c];
					User Assistante=[SELECT Id, Name,email FROM User WHERE Id = :UserInfo.getUserId()]; //User connecté
					String userSignature;
					if(u.Signature!=Null) {
						userSignature ='<pre>'+u.Signature+'</pre>';
					}else{
						userSignature=' ';
					}
					//Integer NextYear = System.today().year()+1;
					String userName=u.Name;
					ContentVersion cversion =  [SELECT title, PathOnClient, FileType, versiondata, FileExtension, CreatedDate FROM contentversion
												WHERE ContentDocumentId =: files[0].ContentDocumentId ORDER BY CreatedDate DESC LIMIT 1];
					Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
					String convOrDevis = 'Devis';
					if(q.Quote_Type__c == 'Convention') {
						convOrDevis = 'Convention';
					}
					contactGreeting ='Cher client,<br><br>';
					CommercialText= u.Name+' vous prie de trouver ci-joint votre '+convOrDevis.toLowerCase()+'.';//+NextYear+'. ';
					ConnectiveText='<br><br>'+q.Opportunity.Filiale__c+' vous invite, via Connective, à signer en 3 étapes votre '+convOrDevis+' en cliquant sur le lien que vous allez recevoir dans un autre mail.<br>';
					notContact='<br><br><i> Séché Environnement collecte et traite des données à caractère personnel dans le cadre de la gestion des contrats et des obligations légales et réglementaires s’y rattachant. Elles sont uniquement destinées à nos services internes et à nos filiales habilitées. Conformément au Règlement Général sur la Protection des Données, le Groupe met à disposition des droits d’accès, de rectification et d’opposition sur les données à caractère personnel. Il est possible d\'exercer ces droits en contactant notre Délégué à la Protection des Données par courrier électronique adressé à <a href="dp@groupe-seche.com">dp@groupe-seche.com</a> , qui traitera votre demande. Pour plus de détails sur l’engagement du Groupe :  <a href="https://www.groupe-seche.com/fr/mentions-legales">RGPD</a>';
					greeting= '<br>Cordialement,';
					//Create Email content
					resp.contactId = contact[0].Id;
					resp.contactName = contact[0].Name;
					resp.toAddresses = new String[] { contact[0].Email };
					resp.ccaddresses = new String[] { Assistante.Email };
					resp.bccaddresses = new String[] {u.Email};
					resp.replyTo = new String[] {u.Email};
					resp.senderDisplayName = u.Name;
					resp.subject = convOrDevis + ' - '+q.Opportunity.Filiale__c+' - '+q.Account.Name;
					resp.htmlBody = contactGreeting+CommercialText+ConnectiveText+greeting;//+userSignature; //+ notContact;
					resp.atts = new List<Messaging.Emailfileattachment>();
					resp.atts.add(fileAttachment);
					resp.isShowForm = 'true';
					resp.fileId = files[0].ContentDocumentId;
					resp.cversions = new List<ContentVersion>();
					resp.cversions.add(cversion);
				}
			}else{
				//TODO ?
				returnValue='TooManyAtts';
			}
		}
	}
	resp.objectQuote = q;
	return resp;
}



@AuraEnabled
public static Map<String,String> getPreviewEmail(String quotId){	
	Map<String,String> result = new Map<String,String>();
	Boolean isEmailAddressBounced = false;
	Quote quote = [SELECT Id, Status, Contact.Name, Contact.Email, Contact.IsEmailBounced FROM Quote WHERE Id=:quotId];
	String status = quote.Status;
	//Check if Email address is bounced
	if(quote.Contact.IsEmailBounced ==true) {
		isEmailAddressBounced = true;
		result.put('message',' L\'adresse mail suivante est incorrecte : '+quote.Contact.Email+' ('+quote.Contact.Name+')');
		result.put('isEmailAddressBounced', 'isEmailAddressBounced');
		result.put('initialStatus', Status);
		}else if(status == 'Envoyé pour signature'){
			result.put('message',' Le devis a déjà été envoyé pour signature ');
		}else{
			LC_EmailSend_AugTarifCtrl.RequestResponse resp = new LC_EmailSend_AugTarifCtrl.RequestResponse();
			//generate the Tech_UID
			String TECH_UID = GuidUtil.NewGuid();
			resp = getEmailValues(quotId,TECH_UID);

			result.put('message', resp.message !=null ? resp.message : '');
			result.put('contactId', resp.contactId);
			result.put('contactName', resp.contactName);
			result.put('toAddresses', resp.toAddresses!=null ? resp.toAddresses[0] : '');
			result.put('ccaddresses', resp.ccaddresses!=null ? resp.ccaddresses[0] : '');
			result.put('bccaddresses', resp.bccaddresses!=null ? resp.bccaddresses[0] : '');
			result.put('replyTo', resp.replyTo!=null ? resp.replyTo[0] : '');
			result.put('senderDisplayName', resp.senderDisplayName!=null ? resp.senderDisplayName : '');
			result.put('subject', resp.subject!=null ? resp.subject : '');
			result.put('fileId', resp.fileId!=null ? resp.fileId : '');
			result.put('body', resp.htmlBody!=null ? resp.htmlBody : '');
			result.put('isShowForm', resp.isShowForm);
			result.put('status', status);
			result.put('tech_uid', TECH_UID);
	}

	
	return result;
}

/* @AuraEnabled
public static RequestResponse getPreFipEmailValues(T5F_PreFip__c preFip){
	String userTheme = UserInfo.getUiThemeDisplayed();
	String preFipId = preFip.Id;
	LC_EmailSend_AugTarifCtrl.RequestResponse resp = new LC_EmailSend_AugTarifCtrl.RequestResponse();
	resp.message = 'Contact manquant ou nombre de pièces-jointes non conforme sur le devis.';
	resp.showToastMode = 'warning';
	resp.mailStatus = false;
	resp.isShowForm = 'false';
	String returnValue='Wait';
		
	List<DocumentsForSignature__c> document = new List<DocumentsForSignature__c>();
	document = [SELECT Id, Status__c, FlowId__c,SigningUrl__c,Package__r.ConnectivePackageId__c FROM DocumentsForSignature__c WHERE TECH_ExternalId__c= :preFipId Limit 1];

	List<Attachment> att=[SELECT id, Name, body, ContentType,ParentId FROM Attachment WHERE ParentId=:preFipId];
	List<ContentDocumentLink> files  = [SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLink where ContentDocument.title like '%Final%' AND LinkedEntityId =:preFipId];
	Messaging.Emailfileattachment fileAttachment = new Messaging.Emailfileattachment();
	String contactGreeting;
	String CommercialText;
	String secheIndustryText;
	String ConnectiveText;
	String greeting;
	String notContact;
	if(files != null && files.size()>0) {
		if(files.size()>0) {			
				if(document.isEmpty()==false) {
					resp.message = 'Document déja envoyé pour signature';
					resp.showToastMode = 'warning';
					resp.isShowForm = 'false';
				}else if(files[0].ContentDocument.title.length() > 75){
					resp.message = 'Le nom du fichier est trop long (maximum 75 caractères).';
					resp.showToastMode = 'warning';
					resp.isShowForm = 'false';
				}else{					
				
					User currentUser=[SELECT Id, Name, email, Assistant__r.Email, Signature FROM User WHERE Id = :preFip.Lead__r.OwnerId]; //User connecté
					String userSignature;
					if(currentUser.Signature!=Null) {
						userSignature ='<pre>'+currentUser.Signature+'</pre>';
					}else{
						userSignature=' ';
					}
					//Integer NextYear = System.today().year()+1;
					String userName=currentUser.Name;
					ContentVersion cversion =  [SELECT title, PathOnClient, FileType, versiondata, FileExtension FROM contentversion WHERE ContentDocumentId =: files[0].ContentDocumentId ORDER BY CreatedDate DESC LIMIT 1];
					Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
					String docType = 'PreFip';
				
					contactGreeting ='Cher client,<br><br>';
					CommercialText= currentUser.Name+' vous prie de trouver ci-joint votre '+docType.toLowerCase()+'.';//+NextYear+'. ';
					ConnectiveText='<br><br> Séché Environnement vous invite, via Connective, à signer en 3 étapes votre '+docType+' en cliquant sur le lien que vous allez recevoir dans un autre mail.<br>';
					notContact='<br><br><i> Séché Environnement collecte et traite des données à caractère personnel dans le cadre de la gestion des contrats et des obligations légales et réglementaires s’y rattachant. Elles sont uniquement destinées à nos services internes et à nos filiales habilitées. Conformément au Règlement Général sur la Protection des Données, le Groupe met à disposition des droits d’accès, de rectification et d’opposition sur les données à caractère personnel. Il est possible d\'exercer ces droits en contactant notre Délégué à la Protection des Données par courrier électronique adressé à <a href="dp@groupe-seche.com">dp@groupe-seche.com</a> , qui traitera votre demande. Pour plus de détails sur l’engagement du Groupe :  <a href="https://www.groupe-seche.com/fr/mentions-legales">RGPD</a>';
					greeting= '<br>Cordialement,';
					//Create Email content					
					resp.ccaddresses = new String[] { currentUser.Email, currentUser.Assistant__r.Email };					
					resp.replyTo = new String[] {currentUser.Email};
					resp.senderDisplayName = currentUser.Name;
					resp.subject = preFip.Name+' - '+preFip.Lead__r.Name;
					resp.htmlBody = contactGreeting+CommercialText+ConnectiveText+greeting;//+userSignature; //+ notContact;
					resp.atts = new List<Messaging.Emailfileattachment>();
					resp.atts.add(fileAttachment);
					resp.isShowForm = 'true';
					resp.fileId = files[0].ContentDocumentId;
					resp.cversions = new List<ContentVersion>();
					resp.cversions.add(cversion);
				}
			
		}
	}
	resp.objectPreFip = preFip;
	return resp;
}

@AuraEnabled
public static Map<String,String> getPreviewPreFipEmail(String preFipId){	
	Map<String,String> result = new Map<String,String>();
	Boolean isEmailAddressBounced = false;
	T5F_PreFip__c preFip = [SELECT Id, Name, Account__r.Name, Lead__c, Lead__r.OwnerId, Lead__r.Email, Lead__r.Name, Contact__c, Contact__r.Name, Contact__r.Email, Contact__r.IsEmailBounced, Status__c FROM T5F_PreFip__c WHERE Id=:preFipId];
	String status = preFip.Status__c;
	//Check if Email address is bounced
	if(preFip.Contact__r.IsEmailBounced ==true) {
		isEmailAddressBounced = true;
		result.put('message',' L\'adresse mail suivante est incorrecte : '+preFip.Contact__r.Email+' ('+preFip.Contact__r.Name+')');
		result.put('isEmailAddressBounced', 'isEmailAddressBounced');
		result.put('initialStatus', Status);
		}else if(status == 'Envoyé pour signature'){
			result.put('message',' Le document a déjà été envoyé pour signature ');
		}else{
			LC_EmailSend_AugTarifCtrl.RequestResponse resp = new LC_EmailSend_AugTarifCtrl.RequestResponse();
			
			//get The email needed infos
			resp = getPreFipEmailValues(preFip);

			System.debug(preFip.Lead__c);

			result.put('message', resp.message !=null ? resp.message : '');
			result.put('leadId', preFip.Lead__c);
			result.put('contactName', preFip.Lead__r.Name);
			result.put('toAddresses', preFip.Lead__r.Email);
			result.put('ccaddresses', resp.ccaddresses!=null ? String.join(resp.ccaddresses, ' ; ') : '');
			result.put('bccaddresses', resp.bccaddresses!=null ? resp.bccaddresses[0] : '');
			result.put('replyTo', resp.replyTo!=null ? resp.replyTo[0] : '');
			result.put('senderDisplayName', resp.senderDisplayName!=null ? resp.senderDisplayName : '');
			result.put('subject', resp.subject!=null ? resp.subject : '');
			result.put('fileId', resp.fileId!=null ? resp.fileId : '');
			result.put('body', resp.htmlBody!=null ? resp.htmlBody : '');
			result.put('isShowForm', resp.isShowForm);
			result.put('status', status);
			
	}
	
	return result;
}*/

@AuraEnabled
public static Map<String,String> createPackage(String contactId, String contactName, String fromAddresses, String toAddresses, String cciEmail, String senderDisplayName, String subject, String body, String quoteId){
	Map<String,String> result = new Map<String,String>();
	System.debug('########## BEGIN instancePackage');
	RecordType rt=[SELECT Id, Name, DeveloperName, Description FROM RecordType Where DeveloperName='Devis_approuv_Read_Only' LIMIT 1];
	//String resultCallPackage = ConnectiveApiManager.createInstantPackage(quoteId);
	
	//TST
	String packId = ConnectiveApiUtils.setupPackFromQuoteId(quoteId);
	Boolean successStatus = ConnectiveApiUtils.setPackagePending(packId); //set status so signers gets a notification
	//END TST

	if (successStatus){ //if(resultCallPackage == 'OK'){
		//TST
		String bodyStatus = ConnectiveApiManager.getStatusByConnectivePackageId(packId);
		PackageResponse bodyRes = PackageResponse.deserializeResponsejson(bodyStatus);
		Package__c pkg = new Package__c(
			ConnectivePackageId__c=packId,
			ConnectivePackageStatus__c=bodyRes.PackageStatus
		);
		insert pkg;
		//add all docs
		
		List<DocumentsForSignature__c> docLs = ConnectiveApiUtils.parseStatusRespToDocList(bodyRes, pkg.Id, quoteId);
		insert docLs;
		//END TST

		try{
			result = sendMailMethod(contactId, contactName, fromAddresses, toAddresses, cciEmail, senderDisplayName, subject, body, quoteId);
			Quote qt = new Quote();
			//Boolean islock = Approval.isLocked(quoteId);
			qt.Id = quoteId;	
			qt.TECH_isApprouved__c=true;
			qt.Status='Envoyé pour signature';
			qt.RecordTypeId=rt.Id;
			//update qt;
			updateLockedRecord updateLockedRecord = new updateLockedRecord();
		  	updateLockedRecord.updateRecord(qt);
		}
		catch(exception ex){
			System.debug('####### Error : ' + ex.getMessage());
		}
	}
	else{
		System.debug('####### Error');
	}
	System.debug('########## END instancePackage');
	return result;
}

public static Map<String,String> sendMailMethod(String contactId, String contactName, String fromAddresses, String toAddresses, String cciEmail, String senderDisplayName, String subject, String body, String quoteId){

	Map<String,String> result = new Map<String,String>();

	Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();

	List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
	Messaging.Emailfileattachment fileAttachment = new Messaging.Emailfileattachment();

	ContentVersion cversion = new ContentVersion ();

	if(quoteId != null) {
		List<Attachment> att=[SELECT id, Name, body, ContentType,ParentId FROM Attachment WHERE ParentId=:quoteId];
		List<ContentDocumentLink> files  = [SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLink where LinkedEntityId =:quoteId];
		if(files != null && files.size()>0) {//Salesforce Lightning UI running
			cversion =  [SELECT title, PathOnClient, FileType, versiondata, FileExtension, ContentDocumentId, CreatedDate FROM contentversion
						WHERE ContentDocumentId =: files[0].ContentDocumentId ORDER BY CreatedDate DESC LIMIT 1];
			String title = (cversion.title.lastIndexOf('.') != -1) ? cversion.title : cversion.title +'.'+ cversion.FileExtension;
			fileAttachment.setFileName(title);
			fileAttachment.setBody(cversion.versiondata);

		} else if(att !=null && att.size()>0) { //Salesforce Classic UI running
			fileAttachment.setFileName(att[0].Name);
			fileAttachment.setBody(att[0].Body);
		}

		List<Messaging.Emailfileattachment> fileLs = new List<Messaging.Emailfileattachment>{fileAttachment};
		//add the CGV file to the mail if not Convention
		Quote qte = [SELECT Quote_Type__c, Opportunity_Filiale__c FROM Quote WHERE Id = :quoteId];
		if (qte.Quote_Type__c == 'Proposition commerciale'){
			String fileName = ConnectiveApiUtils.getStaticResourceNameFromFiliale(qte.Opportunity_Filiale__c);
			StaticResource sr = [SELECT Id, Name, Body FROM StaticResource WHERE Name=:fileName];
			Messaging.Emailfileattachment srAttachment = new Messaging.Emailfileattachment();
			srAttachment.setFileName(sr.Name + '.pdf');
			srAttachment.setBody(sr.Body);
			fileLs.add(srAttachment);
		}

		// Add to attachment file list
		message.setFileAttachments(fileLs);
		message.setReplyTo(fromAddresses);
		message.toAddresses = new String[] {toAddresses};
		message.ccaddresses = cciEmail.trim().split(';');
		message.bccaddresses = new String[] {fromAddresses};
		message.subject = subject;
		message.setSenderDisplayName(senderDisplayName);
		message.htmlBody = body;
		message.setTargetObjectId(contactId);
        message.setSaveAsActivity(true);

		mails.add(message);
		Messaging.sendEmail(mails);
		//Create activity
		Task t = createEmailActivity( subject, body, contactId, quoteId);
		insert t;
		//Add the attachment on task
		insert linkAttToTask(t.Id, cversion);

	}

	result.put('message', 'Envoyé pour signature');
	result.put('showToastMode', 'success');
	return result;
}

//------------------PreFip sepecific methods---------------------------------------------
/*@AuraEnabled
public static Map<String,String> createPackagePreFip(String leadId, String contactName, String fromAddresses, String toAddresses, String cciEmail, String senderDisplayName, String subject, String body, String preFipId){
	Map<String,String> result = new Map<String,String>();
	System.debug('########## BEGIN createPackagePreFip'+leadId);
	
	//TST
	Lead lead = [SELECT Id,FirstName, LastName, Email From Lead WHERE Id =: leadId];
	//T5F_PreFip__c preFip = [SELECT Id, Name FROM T5F_PreFip__c WHERE Id =: preFipId];
	String packId = ConnectiveApiUtils.setupPackFromPreFip(preFipId, lead);
	Boolean successStatus = ConnectiveApiUtils.setPackagePending(packId); //set status so signers gets a notification
	//END TST

	if (successStatus){ //if(resultCallPackage == 'OK'){
		//TST
		String bodyStatus = ConnectiveApiManager.getStatusByConnectivePackageId(packId);
		PackageResponse bodyRes = PackageResponse.deserializeResponsejson(bodyStatus);
		Package__c pkg = new Package__c(
			ConnectivePackageId__c=packId,
			ConnectivePackageStatus__c=bodyRes.PackageStatus
		);
		insert pkg;
		//add all docs
		
		List<DocumentsForSignature__c> docLs = ConnectiveApiUtils.parsePreFipStatusRespToDocList(bodyRes, pkg.Id, preFipId);
		insert docLs;
		//END TST

		try{
			result = sendMailPreFip(leadId, contactName, fromAddresses, toAddresses, cciEmail, senderDisplayName, subject, body, preFipId);
			 T5F_PreFip__c fip = new T5F_PreFip__c();		
			fip.Id = preFipId;			
			fip.Status__c='Envoyé pour Signature';			
			update fip;			
		}
		catch(exception ex){
			System.debug('####### Error : ' + ex.getMessage());
		}
	}
	else{
		System.debug('####### Error');
	}
	System.debug('########## END createPackagePreFip');
	return result;
}


public static Map<String,String> sendMailPreFip(String contactId, String contactName, String fromAddresses, String toAddresses, String cciEmail, String senderDisplayName, String subject, String body, String preFipId){
	
	Map<String,String> result = new Map<String,String>();

	Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();

	List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
	Messaging.Emailfileattachment fileAttachment = new Messaging.Emailfileattachment();

	ContentVersion cversion = new ContentVersion ();

	if(preFipId != null) {
		List<Attachment> att=[SELECT id, Name, body, ContentType,ParentId FROM Attachment WHERE ParentId=:preFipId];
		List<ContentDocumentLink> files  = [SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLink where ContentDocument.title like '%Final%' AND LinkedEntityId =:preFipId];
		System.debug(files);
		if(files != null && files.size()>0) {//Salesforce Lightning UI running
			cversion =  [SELECT title, PathOnClient, FileType, versiondata, FileExtension, ContentDocumentId FROM contentversion WHERE ContentDocumentId =: files[0].ContentDocumentId];
			System.debug(cversion);
			fileAttachment.setFileName(cversion.title);
			fileAttachment.setBody(cversion.versiondata);

		} else if(att !=null && att.size()>0) { //Salesforce Classic UI running
			fileAttachment.setFileName(att[0].Name);
			fileAttachment.setBody(att[0].Body);
		}

		List<Messaging.Emailfileattachment> fileLs = new List<Messaging.Emailfileattachment>{fileAttachment};		

		// Add to attachment file list
		message.setEntityAttachments(new List<ID> {cversion.Id});
		message.setReplyTo(fromAddresses);
		message.toAddresses = new String[] {toAddresses};
		message.ccaddresses = cciEmail.trim().split(';');
		message.bccaddresses = new String[] {fromAddresses};
		message.subject = subject;
		message.setSenderDisplayName(senderDisplayName);
		message.htmlBody = body;
		message.setTargetObjectId(contactId);
        message.setSaveAsActivity(true);

		mails.add(message);
		Messaging.sendEmail(mails);
		//Create activity
		Task t = createEmailActivity( subject, body, null, preFipId);
		insert t;
		//Add the attachment on task
		insert linkAttToTask(t.Id, cversion);

	}

	result.put('message', 'Envoyé pour signature');
	result.put('showToastMode', 'success');
	return result;
}*/

//-----------End Prefip methods ----------------------------------
class RequestResponse {
@AuraEnabled
public String message {get; set;}
@AuraEnabled
public String showToastMode {get; set;}
@AuraEnabled
public Quote objectQuote {get; set;}
/*@AuraEnabled
public T5F_PreFip__c objectPreFip {get; set;}*/
@AuraEnabled
List<String> toAddresses {get; set;}
@AuraEnabled
List<String> ccaddresses {get; set;}
@AuraEnabled
List<String> bccaddresses {get; set;}
@AuraEnabled
List<String> replyTo {get; set;}
@AuraEnabled
String senderDisplayName {get; set;}
@AuraEnabled
public String subject {get; set;}
@AuraEnabled
public String contactName {get; set;}
@AuraEnabled
public String contactId {get; set;}
@AuraEnabled
String htmlBody {get; set;}
@AuraEnabled
public Boolean mailStatus {get; set;}
@AuraEnabled
public String isShowForm {get; set;}
@AuraEnabled
public List<ContentVersion> cversions {get; set;}
@AuraEnabled
public List<Messaging.Emailfileattachment> atts {get; set;}
@AuraEnabled
public String fileId {get; set;}
@AuraEnabled
public String status {get; set;}

}

//-------------------------------Mass Email methods---------------------

@auraEnabled
public static List<Quote> getQuoteRecords() {//Initialize SendMassEmailList
	system.debug('getQuoteRecords::Start: ');
	Integer thisYear = System.today().year();
	//Integer nextYear = thisYear + 1;
	Integer nextYear = 2021;
	
	return [SELECT Name,Quote_Type__c,Contact.Email,OpportunityId,Opportunity.Salesman__c,Opportunity.Filiale__c,AccountId,Account.Name,
			Commercial__c,ContactId,Contact.Name,TotalPrice,Status, Opportunity.Id, Opportunity.Convention_face_face__c,IsSyncing
			FROM QUOTE WHERE Status IN ('Présenté','Approuvé') AND IsSyncing = true
			AND Name like: '%Convention '+nextYear+'%' AND  Opportunity.Convention_face_face__c=false
			AND Opportunity.StageName!='Abandonnée' AND ( (CALENDAR_YEAR(CreatedDate) = 2020 AND CALENDAR_MONTH(CreatedDate) >= 9) OR (CALENDAR_YEAR(CreatedDate) = 2021))];
}

@auraEnabled
public static String SendEmailRecords(List<Quote> lstQuote) {
	system.debug('SendEmailRecords --- > Start ');
	for(Quote q:lstQuote) {
		q.TECH_MassSignature__c = true;
		q.TECH_Assistante__c=UserInfo.getUserId();
	}	
	LC_MassCreatedPackage_AugTarifCtrl sendMassMailJob = new LC_MassCreatedPackage_AugTarifCtrl(lstQuote,null);
	// enqueue the job for processing
	ID jobID = System.enqueueJob(sendMassMailJob);
	System.debug('jobID : '+jobID);

	return 'L’envoi pour signature a été exécuté avec succès';
}

@auraEnabled
public static String  checkAndSendQuoteRecords( List<Quote> lstQuote) {
	String resp = '';
	Boolean isAnError = false;
	Boolean isAlreadySent = false;
	Boolean isEmailAddressBounced = false;
	Boolean isFileNameTooLong = false;
	List<Quote> quotesToSend = new List<Quote>();
	List<Quote> quotesWithError = new List<Quote>();
	List<Quote> quotesSent = new List<Quote>();
	List<Quote> quotesEmailBounced = new List<Quote>();
	List<ContentDocumentLink> docsFileNameTooLong = new List<ContentDocumentLink>();
	Set<Id> ids = new Set<Id>();

	for(Quote q : lstQuote) {
		ids.add(q.Id);
	}
//Get Quotes with related docs
	List<Quote> quotes = [SELECT Name,Quote_Type__c,TECH_UID__c,QuoteCode__c,Email,OpportunityId, Opportunity.Filiale__c,AccountId,Account.Name,Commercial__c,ContactId,Contact.Name,Contact.Email,Contact.IsEmailBounced,TotalPrice,Status,Opportunity.Salesman__c, IsSyncing,(SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLinks),(SELECT ID,Name FROM Attachments) FROM Quote Where ID IN :ids];
	for(quote q:Quotes) {
		Integer docSize = q.ContentDocumentLinks.size();
		List<ContentDocumentLink> lstDocs = q.ContentDocumentLinks;
		List<Attachment> lstAtts = q.Attachments;
		if(lstDocs.size()==1 && q.Opportunity.Salesman__c!=Null && q.ContactId!=Null && q.Contact.Email!=Null) {
			if(lstDocs[0].ContentDocument.title.length() > 75){
				docsFileNameTooLong.add(lstDocs[0]);
				isFileNameTooLong = true;
			}
			QuotesTosend.add(q);
		}else{
			QuotesWithError.add(q);
			isAnError=true;
		}
		if(q.TECH_UID__c!=null) {
			QuotesSent.add(q);
			isAlreadySent=true;
		}

		if(q.Contact.IsEmailBounced ==true) {
			quotesEmailBounced.add(q);
			isEmailAddressBounced=true;
		}
	}

	if(!isAnError && !isAlreadySent && !isEmailAddressBounced && !isFileNameTooLong) {
		resp =  SendEmailRecords(QuotesTosend);
		//resp = 'Success !';
	}else{
		if(isAlreadySent) {
			resp = 'Vous avez sélectionné un devis déjà envoyé pour signature';
		}else if(isAnError) {
			resp = 'Erreur pendant l\'envoi pour signature. Pas de convention rattachée au dévis, ou adresse email manquante sur le contact.';
		}else if(isEmailAddressBounced) {			
			for(Quote qt : quotesEmailBounced){				
				resp +='- '+qt.Contact.Email + ' ('+qt.Contact.Name +')  ';
				
			}
		
		}else if(isFileNameTooLong) {
			resp = 'Les noms de fichier suivants sont trop longs (maximum 75 caractères) : \n\n';
			for(ContentDocumentLink doc : docsFileNameTooLong){
				resp += '- ' + doc.ContentDocument.title + '\n';
			}
		}
	}
	System.debug('resp :' +resp );
	return resp;
}

// Class for updating locked record (without sharing to by pass locked record)
  without sharing class updateLockedRecord {	  
    public void updateRecord(Quote qt) { 		
		update qt; 
	}
  }

//-------------------------------Mass Reminder methods---------------------

	@auraEnabled
	public static List<Quote> getQuoteRecordsReminder() {//Initialize SendMassEmailList
		//Integer nextYear = System.today().year()+1;
		Integer nextYear = 2021;
		
		return [SELECT Name,Quote_Type__c,NbSendReminder__c,Contact.Email,OpportunityId,Opportunity.Salesman__c,Opportunity.Filiale__c,AccountId,Account.Name,Commercial__c,ContactId,Contact.Name,TotalPrice,Status, Opportunity.Id, Opportunity.Convention_face_face__c,IsSyncing FROM QUOTE
				WHERE Status IN ('Envoyé pour signature') AND IsSyncing = true
				AND Name like: '%Convention '+nextYear+'%' AND  Opportunity.Convention_face_face__c=false AND Opportunity.StageName!='Abandonnée'];
			
	}

  	@auraEnabled
	public static String SendReminders(List<Quote> lstQuote) {
		system.debug('SendEmailRecords --- > Start ');
		ConnectiveAsyncSendReminder sendMassMailJob = new ConnectiveAsyncSendReminder(lstQuote,lstQuote);
		// enqueue the job for processing
		ID jobID = System.enqueueJob(sendMassMailJob);
		System.debug('jobID : '+jobID);
		return 'L’envoi de rappel a été exécuté avec succès';
	}

}