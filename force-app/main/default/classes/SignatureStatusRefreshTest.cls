@IsTest
public class SignatureStatusRefreshTest {
    @Istest static void getAllStatus(){
        Account a;
        Contact c;
        Opportunity opp;
        
        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        Quote q = new Quote();
        q.OpportunityId = opp.ID;
        q.Name = opp.Name;
        q.DateDevis__c = Date.today();
        q.QuoteCode__c=opp.Name;
        q.ContactId = opp.ContactName__c;
        q.Email = opp.ContactName__r.Email;
        q.Phone = opp.ContactName__r.Phone;
        q.Fax = opp.ContactName__r.Fax;
        q.BillingStreet = opp.Account.BillingStreet;
        q.BillingCity = opp.Account.BillingCity;
        q.BillingState = opp.Account.BillingState;
        q.BillingPostalCode = opp.Account.BillingPostalCode;
        q.BillingCountry = opp.Account.BillingCountry; 
        insert q;
        DocumentsForSignature__c doc=new DocumentsForSignature__c();
        doc.Name=q.Name+'.pdf';
        doc.FlowId__c='a02c9f7a-cfd0-46d2-8ba3-465a5bd79fce';
        doc.Document__c='https://i.pinimg.com/736x/ba/03/23/ba03237a6d6499f0e2633314826e1526--cutest-animals-baby-animals.jpg';
        doc.Status__c='PENDINGSIGNING';
        doc.Quote__c=q.id;
        doc.TECH_ExternalId__c=q.id;
        insert doc;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WS002_ConnectiveMock());
        WS002_ConnectiveMock.externalId=q.id;
        WS002_ConnectiveMock.status='SIGNED';
        SignatureStatusRefresh tsc = new SignatureStatusRefresh();
        String chron = '0 0 23 * * ?';        
        system.schedule('Test Sched', chron, tsc);
        SignatureStatusMassRetrieve  batch = new SignatureStatusMassRetrieve(); 
        Id batchId = Database.executeBatch(batch);
        Test.stopTest();
        
    }
}