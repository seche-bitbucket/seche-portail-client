public with sharing class Constants {

    //Begin------------Constants using to handle Attestions 
    public static final String ATTESTATION_GENERATE_STATUS ='Générée';   
    public static final String ATTESTATION_SEND_STATUS ='Envoyée';  
    public static final String ATTESTATION_DRAFT_STATUS ='Brouillon';
    public static final String ATTESTATION_NOTCONCERNED_STATUS ='Non concernée';
    public static final String Email_Subject ='Brouillon'; 
    public static final Integer yearOfAttestations = Date.today().year() - 1;

    public static String massEmailSubject(Integer year){
        return 'Attestation de valorisation déchets';
    }  

     public static String massEmailBody(Integer year){
         return 'Cher client,<br><br>' + 
         'Vous nous avez confié au cours de l’année ' + year +' vos déchets valorisables et vous en remercions. <br><br>'+
         'Conformément à l’arrêté du 18 juillet 2018 relatif à l’attestation mentionnée à l’article D. 543-284 du code de l’environnement, nous vous joignons une attestation relative à chaque flux pris en charge par notre société.<br><br>'+
         'Nous restons à votre disposition pour tout renseignement complémentaire.<br><br>';
     }
    //End------------Constants using to handle Attestions 
    
    //Begin------------Recordtype Name Constants 
    public static final String STR_LED_RET_TRIADIS_AMIANTE = 'LED_WEB_TO_LEAD_TRIADIS_AMIANTE';
    public static final String STR_LED_RET_STANDARD = 'LED_Standard';
    public static final String STR_LED_RET_TRIADIS_DEPOTAGE = 'LED_TRIADIS_DEPOTAGE';
    public static final String STR_LED_RET_DND = 'LED_WEB_TO_LEAD_DND';
    public static final String STR_LED_RET_TENDER = 'LED_TENDER';
    public static final String STR_LED_RET_MISCELLANEOUS = 'LED_WEB_TO_LEAD_MISCELLANEOUS';
    public static final String STR_LED_RET_SHC_DASRI = 'LED_WEB_TO_LEAD_SHC_DASRI';
    public static final String STR_LED_RET_SUI = 'LED_WEB_TO_LEAD_SUI';
    public static final String STR_LED_RET_PLOMB = 'LED_WEB_TO_LEAD_PLOMB';
    public static final String STR_LED_RET_SALON = 'LED_PISTES_SALON';
    //End------------Recordtype Name Constants 
    
    //Begin------------Filiere Constants 
    public static final String STR_LED_FILIERE_DASRI = 'DASRI';
    //End------------ Filiere Constants 

    //Begin------------Filiale Constants 
    public static final String STR_LED_FILIALE_SECHE_ECO_INDUS_CHANGE = 'Séché Eco Industries Changé';
    //End------------ Filiale Constants 

    //Begin------------PostalCode Constants 
    public static final String STR_LED_PSTCODE_DEFAULT = '00';
    public static final String STR_LED_PSTCODE_97 = '97';
    public static final String STR_LED_PSTCODE_98 = '98';
    //End------------ PostalCode Constants 

    //Begin------------Product Constants 
    public static final String STR_PRD_TECH_EXTERNALID_DEFAULT = 'DEFAULT';
    public static final String STR_PRD_FAMILY_TRAITEMENT = 'Traitement';
    public static final String STR_PRD_FAMILY_CONDITIONNEMENT = 'Conditionnement';
    public static final String STR_PRD_FAMILY_PRESTATION = 'Prestation';
    public static final String STR_PRD_PRODUCTCODE_DEFAULT = 'SERV_DEFAUT';
    //End------------ Product Constants

    //Begin------------PriceBook Constants 
    public static final String STR_PRB_NAME_PLATEFORME = 'Plateforme';
    public static final String STR_PRB_NAME_STOCKAGE_DD = 'Stockage DD';
    public static final String STR_PRB_NAME_SHC = 'SHC';
    public static final String STR_PRB_NAME_GESTION_DND = 'Gestion des DND';
    //End------------ PriceBook Constants

    //Begin------------Unite Constants 
    public static final String STR_UNIT_TONNE = 'Tonne';
    public static final String STR_UNIT_UNITE = 'Unité';
    public static final String STR_UNIT_FORFAIT = 'Forfait';
    public static final String STR_UNIT_JOUR = 'Jour';
    public static final String STR_UNIT_UNITE_MOIS = 'Unité/mois';
    //End------------ Unite Constants 

    //Begin------------Duration Constants 
    public static final String STR_DURATION_0_15 = '0-15j';
    public static final String STR_DURATION_15_30 = '15-30j';
    public static final String STR_DURATION_USAGE_UNIQUE= 'A usage unique';
    //End------------ Duration Constants 

    //Begin------------Nature Constants 
    public static final String STR_NATURE_TRAITEMENT = 'Traitement';
    //End------------ Nature Constants

    //Begin------------OpportunityLineItem Constants 
    public static final String STR_OPPLINEITEM_DESC_TRAITEMENT = 'Traitement';
    //End------------ OpportunityLineItem Constants

    //Begin------------Opportunity Constants 
    public static final String STR_OPP_STAGENAME_PREPA_AO = 'Préparation de l\'AO';
    //End------------ Opportunity Constants
    
    //Begin------------ContentVerion Constants 
    public static final String STR_CVE_ERRORMESSAGE_QUOTE_LOCKED = 'Vous ne pouvez pas modifier ou supprimer des fichiers car le devis est verrouillé.';
    public static final String STR_CVE_ERRORMESSAGE_SIZE_TITLE = 'Le fichier sélectionné a un titre de trop grande taille (supérieur à 75 caractères)';
    public static final String STR_CVE_SIGNE = 'Signée';
    //End------------ ContentVerion Constants
}