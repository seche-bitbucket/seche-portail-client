@isTest 
public class QuoteTriggerTest {
    
    @isTest
    static void QuoteTriggerTest(){
        Account a;
        Contact c;
        Opportunity opp;
        
        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

            //Get a profile to create User
            Profile p = [select id from profile where name in:profiLisList limit 1];
        
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        insert director;

        System.runAs(director) {
            Quote quote = new Quote();
            quote.OpportunityId=opp.Id;
            quote.Name = 'test';
            quote.QuoteNumber__c = 111;
            insert quote;

            
            quote.Status = 'Signé';
            update quote;
        }
    }

    @isTest
    static void QuoteTriggerTestSignature() {

        ByPassUtils.ByPass('QuoteTrigger');
            
        QteLineItemSyncTriggerV2Handler.stop();
        OppLineItemSyncTriggerV2Handler.stop();
        QuoteSyncTriggerV2Handler.stop();
        OppSyncTriggerV2Handler.stop();

        Account a;
        Contact c;
        Opportunity opp;
        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        Quote q = new Quote();
        q.OpportunityId=opp.Id;
        q.Name = 'test';
        q.QuoteNumber__c = 111;
        q.Quote_Type__c = 'Convention';
        insert q;

        opp.SyncedQuoteId = q.Id;
        update opp;

        try {
            q.Status = 'Signé';
            update q;
        } catch (Exception e) {
            //System.assert(e.getMessage().contains('Veuillez d\'abord ajouter un document signé dans la liste des Fichiers'));
        }

        ContentVersion contentVersion = new ContentVersion(
                    Title          = 'Test - Signé',
                    PathOnClient   = 'Pic.jpg',
                    VersionData    = Blob.valueOf('Test Content'),
                    IsMajorVersion = true);
        insert contentVersion;

        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = q.Id;
        cdl.ContentDocumentId = documents[0].Id;
        insert cdl;
        q.Status = 'Signé';
        update q;
    }

      @isTest
    static void QuoteTriggerTestAtt(){
         Account a;
        Contact c;
        Opportunity opp;
        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        Quote q = new Quote();
        q.OpportunityId=opp.Id;
        q.Name = 'test';
        q.QuoteNumber__c = 111;
        q.Quote_Type__c = 'Convention';
        insert q;
        
        q=[Select ID, Name, QuoteCode__c,TECH_UnknownSignerInformation__c, TECH_SendForSignature__c, Quote_Type__c,
                   NameCustomer__c,ReferenceWasteOwner__c, QuoteNumber, OpportunityId, Status,Contact.FirstName,
                   Contact.LastName,Contact.Email,Contact.Phone FROM Quote WHERE ID = :q.Id];

       Attachment att = new Attachment();
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        att.body=bodyBlob;
        att.Name = q.QuoteCode__c +' - Convention - '+q.NameCustomer__c+' - '+q.ReferenceWasteOwner__c+'.pdf';	
        att.ParentId = q.Id;             
        insert att;  
             
        
        //Insert ContentVersion
        ContentVersion cVersion = new ContentVersion();
        cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
        cVersion.PathOnClient = q.Name;//File name with extention
        cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.      
        cVersion.Title = q.QuoteCode__c +' - Convention - '+q.NameCustomer__c+' - '+q.ReferenceWasteOwner__c+'.pdf';//Name of the file
        cVersion.VersionData = bodyBlob;//File content
        Insert cVersion;
        
        //After saved the Content Verison, get the ContentDocumentId
        Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
        
        //Insert ContentDocumentLink
        ContentDocumentLink cDocLink = new ContentDocumentLink();
        cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
        cDocLink.LinkedEntityId = q.Id;//Add attachment parentId
        cDocLink.ShareType = 'I';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
        cDocLink.Visibility = 'InternalUsers';//AllUsers, InternalUsers, SharedUsers
        Insert cDocLink;
        q.Status ='Révision nécessaire';
        update q;    
    }

    @isTest
    static void QuoteTriggerTestAttWithoutTypeConv(){
         Account a;
        Contact c;
        Opportunity opp;
        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;

        Quote qt = new Quote();
        qt.OpportunityId=opp.Id;
        qt.Name = 'test';
        qt.QuoteNumber__c = 121;
        insert qt;
        
        qt=[Select ID, QuoteCode__c,TECH_UnknownSignerInformation__c, TECH_SendForSignature__c, Quote_Type__c,
                   NameCustomer__c,ReferenceWasteOwner__c, QuoteNumber, OpportunityId, Status,Contact.FirstName,
                   Contact.LastName,Contact.Email,Contact.Phone FROM Quote WHERE ID = :qt.Id];

        Attachment at = new Attachment();
        Blob bodyBlobb=Blob.valueOf('Unit Test Attachment Body');
        at.body=bodyBlobb;
        at.Name = qt.QuoteCode__c +' - '+qt.Quote_Type__c+' - '+qt.NameCustomer__c+' - '+qt.ReferenceWasteOwner__c+'.pdf';	
        at.ParentId = qt.Id;             
        insert at;  

        qt.Status ='Révision nécessaire';
        update qt;      
    }
}