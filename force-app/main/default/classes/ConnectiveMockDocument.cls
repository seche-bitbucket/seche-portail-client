/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ConnectiveMockDocument
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ConnectiveMockDocument
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 22-06-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
@isTest
global class ConnectiveMockDocument implements  HTTPCalloutMock{
	global HTTPResponse respond(HTTPRequest req){
        
        HttpResponse res = new HTTPResponse();
        res.setHeader('Content-Type', 'application/JSON');
        res.setBody('');
        req.setMethod('GET');
        res.setStatusCode(200);
        return res;
    }
}