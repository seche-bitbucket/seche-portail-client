/*
* ------------------------------------------------------------------------------------------------------------------
* -- Name : ConnectiveUpdateStatusBatchV4
* -- Author : PMB
* -- Company : Capgemini
* -- Purpose : ConnectiveUpdateStatusBatchV4
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 27-05-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
global class Batch_CheckForDuplicates implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful{

    global List<Contact> lstCts {get; set;}
    
    global Database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(
            'SELECT Id, Account.Id, Account.TECH_IsDuplicate__c, Account.TECH_NotToMerge__c, Account.TECH_IsPassed__c, Account.SIRET_Number__c, Account.ContractRoot__c FROM Contact WHERE Account.TECH_IsPassed__c = false AND Account.SIRET_Number__c != NULL AND (Account.SIRET_Number__c LIKE \'______________\') AND (NOT Account.CustomerNature__c IN (\'Exutoire\',\'Particulier\', \'Groupe Séché\'))'
        );
    }

    global void execute(Database.BatchableContext BC, List<Contact> scope) {
       
        this.lstCts = scope;
        System.debug(this.lstCts.size());
        
        List<Id> lstCtsIds = new List<Id>();
        
        if(lstCts.size() > 0) {
            for(Contact ct: lstCts) {
                lstCtsIds.add(ct.Id);
            }
        }

        if(lstCtsIds.size() > 0) {
            
            List<AccountContactRelation> lstRela = Database.Query('SELECT AccountId, ContactId FROM AccountContactRelation WHERE ContactId IN :lstCtsIds');
            
            Map<String, List<Account>> mapAccsWithSamesSiret = new Map<String, List<Account>>();
            Set<Account> notToMerge = new Set<Account>();
            
            for(Contact ct: lstCts) {
                if(!mapAccsWithSamesSiret.containsKey(ct.Account.SIRET_Number__c)) {
                    mapAccsWithSamesSiret.put(ct.Account.SIRET_Number__c, new List<Account>{ct.Account});
                } else {
                    List<Account> accs = new List<Account>();
                    accs = mapAccsWithSamesSiret.get(ct.Account.SIRET_Number__c);
                    if(!accs.contains(ct.Account)) {
                        accs.add(ct.Account);
                        mapAccsWithSamesSiret.put(ct.Account.SIRET_Number__c, accs);
                    }
                }
            }
            
            for(Contact c: lstCts) {
                if(c.Account.ContractRoot__c != null) {
                    notToMerge.addAll(mapAccsWithSamesSiret.get(c.Account.SIRET_Number__c));
                }
            
                for(Contact ct: lstCts) {
                    if(ct.AccountId == c.Account.Id) {
            
                        List<AccountContactRelation> lstRelations = new List<AccountContactRelation>();
            
                        for(AccountContactRelation rela: lstRela) {
                            if(rela.ContactId == ct.Id) {
                                lstRelations.add(rela);
                            }
                        }
            
                        if(lstRelations.size() > 1) {
                            notToMerge.addAll(mapAccsWithSamesSiret.get(c.Account.SIRET_Number__c));
                        }
            
                    }
                }
            }
            
            List<Account> updateAccs = new List<Account>();
            
            for(String siret: mapAccsWithSamesSiret.keySet()) {
                if(mapAccsWithSamesSiret.get(siret).size() >= 2)
                    updateAccs.addAll(mapAccsWithSamesSiret.get(siret));
            }
            
            for(Account acc: updateAccs) {
                for(Account a: notToMerge) {
                    if(acc.Id == a.Id) {
                        acc.TECH_NotToMerge__c = true;
                    }
                }
                acc.TECH_IsDuplicate__c = true;
                acc.TECH_IsPassed__c = true;
            }
            
            update updateAccs;
            
        }
            
    }

    global void finish(Database.BatchableContext BC){ }
}