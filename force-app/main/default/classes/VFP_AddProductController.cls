public with sharing class VFP_AddProductController {

private String currentRecordId;
private final Opportunity oppt;

public VFP_AddProductController(ApexPages.StandardController controller) {
	System.debug('VF_AddProductController Start ---- : ');
	currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
	this.oppt = [SELECT Name, TECH_PriceBookID__c FROM Opportunity WHERE Id = :currentRecordId ];
}


public pageReference init(){
	System.debug('init Start ---> currentRecordId: ');
	If (String.isBlank(oppt.TECH_PriceBookID__c)){
		pageReference pr = Page.OPP_ChoosePriceBook;
		pr.getParameters().put('id', oppt.Id);
		pr.setRedirect(true);
		return pr;
	}
	else{
		pageReference pr = Page.OLI_ProductSearch;
		pr.getParameters().put('OppId', oppt.Id);
		pr.getParameters().put('OppName', oppt.Name);
		pr.setRedirect(true);
		return pr;
	}

}
}