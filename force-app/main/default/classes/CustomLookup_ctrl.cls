public with sharing class CustomLookup_ctrl {      
    public String fieldTypeForSelectedField {get;set;}     
    public list<EmailNumberDetail> listOfLookupsElement {get;set;}
    public String objectName {get;set;}
    public String fieldName {get;set;}
    public String searchByName{get;set;}
    public String searchByEmail{get;set;}
    public boolean isChecked {get;set;}
    public String emailNumberPassToTextBox {get;set;}
    public String emailNamePassToHdn {get;set;}
    public boolean isAllSelectCheckBox {get;set;}    
    
    
    /*Controller*/
    public CustomLookup_ctrl (){
        listOfLookupsElement = new list<EmailNumberDetail>();        
        objectName='Contact';
		fieldName ='Email';
        isAllSelectCheckBox = false;
        searchByEmail = '';
        searchByName ='';
        emailNumberPassToTextBox = '';
        emailNamePassToHdn  = '';
        getlistOfLookupsElement();
   }
    
    
    
   public void getlistOfLookupsElement(){
        try{ 
            listOfLookupsElement = new list<EmailNumberDetail>();
            String query = 'SELECT Name,'+fieldName+' FROM '+objectName +' order by name limit 999';
            system.debug(' query = ' +query);
            list<SObject> s = Database.query(query);
            for(sObject sObj:s){ 
                if(sObj.get(fieldName) != null && sObj.get('Name') != null){
                    listOfLookupsElement.add(new EmailNumberDetail(String.valueOf(sObj.get('Name')),String.valueOf(sObj.get(fieldName)),false));                     
                }
            } 
        }catch(Exception ex){
             apexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Le paremetre est non défini, merci de contacter votre administrateur!'));
        }       
    }
    
    
    
    public void onSearch(){
      //  emailNumberPassToTextBox = '';
      //  emailNamePassToHdn  = '';
        isAllSelectCheckBox = false;
        listOfLookupsElement = new list<EmailNumberDetail>();
        String byName = '\'%'+String.escapeSingleQuotes(searchByName) + '%\'';
        String byEmail = '\'%'+String.escapeSingleQuotes(searchByEmail) + '%\'';
        try{
            String query ='SELECT Name,'+fieldName+' FROM '+objectName +' WHERE  name Like '+byName +' AND  '+fieldName + ' Like '+byEmail + 'limit 999';
                        system.debug(' query2 = ' +query);

            list<SObject> s = Database.query(query); 
            for(sObject sObj:s){
                if(sObj.get(fieldName) != null && sObj.get('Name') != null){
                    listOfLookupsElement.add(new EmailNumberDetail(String.valueOf(sObj.get('Name')),String.valueOf(sObj.get(fieldName)),false));                     
                }
            } 
        }catch(Exception ex){
            apexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Parametre de recherche non valide'));
        }       
    }
    
    
    
    
    public void onCheck(){
     //   emailNumberPassToTextBox = '';
       // emailNamePassToHdn  = '';
        for(EmailNumberDetail pn:listOfLookupsElement){
            if(pn.isChecked){
                    emailNumberPassToTextBox += pn.sObjectEmail+','; 
                    emailNamePassToHdn += pn.sObjectNameField+',';                    

       			 }
   		 }
    }
    
    
    
    
    public void onSelectAll(){
        emailNumberPassToTextBox = '';
        emailNamePassToHdn  = '';
        for(EmailNumberDetail pn:listOfLookupsElement){
            if(isAllSelectCheckBox){
                pn.isChecked = true;
            }else{
                pn.isChecked = false;
            }           
            if(pn.isChecked){         
                    emailNumberPassToTextBox += pn.sObjectEmail+','; 
                    emailNamePassToHdn += pn.sObjectNameField+',';                    
 
            }
        }
    }
    
    
    
   public class EmailNumberDetail{
        public boolean isChecked {get;set;}
        public string sObjectNameField {get;set;}
        public string sObjectEmail {get;set;}
        public EmailNumberDetail(string sObjectNameField,string sObjectEmail,boolean isChecked ){
            this.sObjectNameField = sObjectNameField;
            this.sObjectEmail = sObjectEmail ;
            this.isChecked = isChecked ;
        }        
    }     

}