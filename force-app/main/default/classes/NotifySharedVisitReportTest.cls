@isTest (seeAllData=true)
private class NotifySharedVisitReportTest {
    static testMethod void validateSharingUser() {
        
         //Get a profile to create User
        Profile p = [select id from profile where name = :'Directeur commercial' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User userTest = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname');
        
        insert userTest;
        User u = [Select Id From User Where IsActive =  true LIMIT 1];
        
        Account a = new Account();
        a.Name = 'testAccName1';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c ='1234A';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName1';
        c.AccountId = a.Id;
        c.Salesman__c = u.Id;
        c.Email = 'aaaa@yopmail.com';
        c.Phone = '03 00 00 00 00';
        insert c;
        
        VisitReport__c vr = new VisitReport__c();
        vr.Account__c = a.Id;
        vr.Contact__c = c.id;
        insert vr;
        
        VisitReport__Share vrs = new VisitReport__Share();
        vrs.ParentId = vr.Id;
        vrs.UserOrGroupId = userTest.Id;
        vrs.AccessLevel = 'Read';
        insert vrs;
        
        Group grp = new Group();
        grp.Name = 'testGrp';
        grp.DoesIncludeBosses = false;
        grp.DeveloperName = 'ABC';
        insert grp;
        
        System.RunAs(new User(id = UserInfo.getUserId()))
        {
            GroupMember gm1 = new GroupMember();
            gm1.GroupId = grp.id;
            gm1.UserOrGroupId = u.id;
            insert gm1;
        }
        
        VisitReport__Share vrs2 = new VisitReport__Share();
        vrs2.ParentId = vr.Id;
        vrs2.UserOrGroupId = grp.id;
        vrs2.AccessLevel = 'Read';
        insert vrs2;
        
        Test.startTest();
        NotifySharedVisitReport n = new NotifySharedVisitReport();
        Database.executeBatch(n);
        Test.stopTest();
    }
}