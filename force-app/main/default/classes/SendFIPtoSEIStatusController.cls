public with sharing class SendFIPtoSEIStatusController {
    
    @AuraEnabled
    public static Boolean setChangeStatut(Id recordId){
        System.debug('setChangeStatut START') ;
        System.debug('recordId = '+recordId);
        FIP_FIP__c fip = [Select ID, State__c, contact__c  FROM  FIP_FIP__c WHERE ID =: recordId];
        System.debug('fip.get(0).contact__c = ' + fip.contact__c);
        
        System.debug('fip.get(0) = ' + fip);
        System.debug('fip.State__c ' + fip.State__c );
        if(fip.State__c =='En attente qualification par le collecteur')
        {
            fip.State__c = 'Soumise a SEI';
            
            try{
                update fip;
                System.debug('setChangeStatut END1') ;
                return true;
            }catch(Exception e){
                System.debug('Exception = '+ e) ;
                System.debug('setChangeStatut END2') ;
                return false;
            }
        } 
        return false;   
    }
}