@isTest
public class SendFIPtoSEIStatusControllerTEST {
    @isTest static void test(){
        System.debug('FIPFormControllerTest START' );
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.BillingPostalCode = '21000';
        acc.BillingStreet = '1 rue du test';
        acc.TECH_UID__c = '21000';
        
        insert acc;
        
        Contact ctc = new Contact();
        ctc.lastName = 'Test';
        ctc.AccountId = acc.ID;
        insert ctc;
        
        
        
        List<FIP_FIP__c> testFips = new List<FIP_FIP__c>();
        for(integer i=0; i<1 ; i++){
            FIP_FIP__c fip = new FIP_FIP__c();
            fip.Collector__c = acc.ID;
            fip.Contact__c = ctc.ID;
            testFips.add(fip);
        }
        
        insert testFips;
        System.debug('testFips =' + testFips );
        
        Boolean sendFIPs = SendFIPtoSEIStatusController.setChangeStatut(testFips.get(0).id);
        System.debug('Result =' + sendFIPs );
        
        System.debug('FIPFormControllerTest END' );
        
    }
    
}