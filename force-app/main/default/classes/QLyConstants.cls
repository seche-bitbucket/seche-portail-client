public with sharing class QLyConstants {
    public static final String PREFIX_NAME = 'Questionnaire Lycee';
    public static final Integer CURRENT_YEAR = Date.Today().Year();
    public static final String NORMAL = 'Normal';
    public static final String QUANTITY_PHOTO = 'Quantite_Photos';
    public static final String QUANTITY_LIST_PHOTO ='Quantite_Liste_Photos';
    public static final String PEROXYDE_CLASSIFICATION_QUANTITY_PHOTO = 'Peroxyde_Classification_Quantite_Photos';
    public static final String BOUCHON_DEGAGEUR ='Bouchon dégazeur pour bidon BID5, BID10 et BID20';
}