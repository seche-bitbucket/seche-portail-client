global inherited sharing class OpportunityImportProductLine {
    public ApexPages.StandardSetController stdSetController;
    public List<SObject> opps = new List<SObject>();
    public String currentRecordId {get;set;}
    public Boolean saveButDisp {get;set;}
    public Boolean retButDisp {get;set;}
    public Boolean isShowConfirmation {get;set;}
    public Boolean  isNoSelectedOpp {get;set;}
    private List<Id> ids;

    public OpportunityImportProductLine (ApexPages.StandardSetController controller) {  
       this.stdSetController = controller;
       currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
       ids = new List<Id>();
       isNoSelectedOpp = false;
       System.debug('stdController.currentRecordIdcurrentRecordId : '+currentRecordId);
        //oppSelected = [select id,Attributaire__c,DateFinEngagement__c,Name,account.name,ContactName__c,Filiale__c,Filiere__c,Amount,TonnageN__c from Opportunity where id IN:stdSetController.getSelected()] ;
    }

    public void init(){        
        System.debug('List Records : '+stdSetController.getSelected());
        
        If (stdSetController.getSelected().Size()>0)
        {   
             saveButDisp = true;
             retButDisp  = true;  
             isShowConfirmation = true;                   
            for(sObject SObj : stdSetController.getSelected()){
                opps.add(SObj);
                ids.add((ID)SObj.get('Id'));              
            }           
        }
        else
        {
            isNoSelectedOpp = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Veuillez sélectionner au moins une opportunité dans la liste des Opportunités Sous-traitance avant de procéder !'));   
        }      
        
    }
    
    WebService static string ImportOppLineItemsFromDaughters (List<Id> oppIntraIds, Id oppId) {
        
        List <Opportunity> oppIntraGroup = new List<Opportunity>();
        List <OpportunityLineItem> oppLineItemMere = new List<OpportunityLineItem>();
        List <OpportunityLineItem> oppLineItemIntraGroup = new List<OpportunityLineItem>();
        List <OpportunityLineItem> productAutre = new List<OpportunityLineItem>(); 
        List <PriceBookEntry> priceBookEntry1 = new List<PriceBookEntry>(); 
        List <product2> productOther = new List<product2>(); 
        Boolean existe = false;
        Opportunity oppPriceBook = [select pricebook2Id from opportunity where id =:oppId ];
        
        //Nous récupérons les oppLineItems de l'opportunité mére
        oppLineItemMere = [select Id,Name,Product2Id,PriceBookEntryId,Quantity,TotalPrice,Description, Packaging__c, IncludedTGAP__c, 
                           Treatment__c, TransportType__c, Unit__c, Nature__c, Label__c, TECH_ExternalID__c, Classe__c, EuropeanCode__c,
                           GE__c, UNCode__c, Features__c, Filiale__c, AdditionalInformation__c, TECH_ProductName__c
                           from OpportunityLineItem where OpportunityId =:oppId];
        //Nous récupérons les oppLineItems des opportunités filles
        oppLineItemIntraGroup = [select Id,Name,Product2Id,PriceBookEntryId,Quantity,TotalPrice, UnitPrice,Nature__c,Description,Sector__c,Packaging__c,UNCode__c,Classe__c,Label__c,GE__c,EuropeanCode__c,TECH_ProductName__c,TECH_ExternalID__c, IncludedTGAP__c, Treatment__c, TransportType__c, Unit__c, Features__c, Filiale__c from OpportunityLineItem where OpportunityId IN:oppIntraIds];
        productOther = [select name from product2 where TECH_ExternalID__c = 'DEFAUT'  ];
        priceBookEntry1 = [select Id,Name from PriceBookEntry where name =: productOther[0].name and pricebook2Id =: oppPriceBook.pricebook2Id];
        if (priceBookEntry1.size()==1)
        {
            //Boucle pour tester si l'oppLineItem fille n'a pas été déjà importé
            For (OpportunityLineItem oppLIF : oppLineItemIntraGroup){
                existe = false;
                
                For (OpportunityLineItem oppLIM : oppLineItemMere ){
                    
                    if ((oppLIF.TECH_ProductName__c == oppLIM.Description)&&(oppLIF.Quantity == oppLIM.Quantity)&&(oppLIM.PricebookEntryId ==priceBookEntry1[0].Id)&&(oppLIF.Unit__c == oppLIM.Unit__c)){
                        
                        existe = true;
                    }
                }
                
                if (existe == false)
                {
                    
                    OpportunityLineItem oppLineItem = New OpportunityLineItem(PriceBookEntryId = priceBookEntry1[0].Id, 
                                                                              OpportunityId = oppId ,
                                                                              Description = oppLIF.TECH_ProductName__c, 
                                                                              Quantity = oppLIF.Quantity, 
                                                                              TotalPrice =0,
                                                                              Sector__c =oppLIF.Sector__c,
                                                                              UNCode__c=oppLIF.UNCode__c,
                                                                              Classe__c=oppLIF.Classe__c,
                                                                              Label__c=oppLIF.Label__c,
                                                                              GE__c=oppLIF.GE__c,
                                                                              EuropeanCode__c=oppLIF.EuropeanCode__c,
                                                                              Packaging__c=oppLIF.Packaging__c ,
                                                                              Nature__c=oppLIF.Nature__c,
                                                                              IncludedTGAP__c = oppLIF.IncludedTGAP__c,
                                                                              Treatment__c = oppLIF.Treatment__c,
                                                                              TransportType__c = oppLIF.TransportType__c,
                                                                              Unit__c = oppLIF.Unit__c,
                                                                              Features__c = oppLIF.Features__c,
                                                                              Filiale__c = oppLIF.Filiale__c,
                                                                              BuyingPrice__c = oppLIF.UnitPrice,
                                                                              TECH_ExternalIDIntraGroup__c = oppLIF.TECH_ExternalID__c
                                                                              );
                    productAutre.Add(oppLineItem);
                }
            }
            
            System.debug('///////Existe : '+existe);            
            
            insert productAutre;
            update productAutre;
            
            if (productAutre.size()>0)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM ,'Les lignes de produits ont été bien importées !'));
                return ('Les lignes de produits ont été bien importées');          
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING ,'Aucune ligne de produit n\'a été importée, ligne déja importée'));     
                return ('Aucune ligne de produit n\'a été importée'); 
                
            }
        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING ,'Vous devez sélectionner un catalogue de prix avant d\'importer les lignes d\'opportunité de sous-traitance'));
            return ('Vous devez sélectionner un catalogue de prix avant d\'importer les lignes d\'opportunité de sous-traitance');
        }
        
        
    }

   
     public  static void processImportOppLineItemsFromDaughters (List<Id> oppIntraIds, Id oppId) {
         System.debug('processImportOppLineItemsFromDaughters Start -----');
         ImportOppLineItemsFromDaughters(oppIntraIds, oppId);              
    }

     public void save()
    {
        try{
            isNoSelectedOpp = true;
             processImportOppLineItemsFromDaughters (ids, currentRecordId);     
              
        }
        catch(Exception e){
            System.debug('Error : '+e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Une erreur est survenue, contacter votre administrateur.'+ e));            
            
        }         
        
    }

    public Boolean getIsNoSelectedOpp()
        {
            return isNoSelectedOpp;
        }

    public  pageReference returnBack()
        {       
            return stdSetController.cancel();
        }

    public  List<Opportunity> getOpps()
        {       
            return opps;
        }
}