@isTest(SeeAllData=true)
public class TestDataFactory_Test {

    /************************
     * Account
     * *********************/
    
    @isTest
    public static void testCreateAccount(){
        
        String name = 'MonCompteCapgemini';
        
        Test.startTest();
        
        Account acc = TestDataFactory.createAccount(name);
        insert acc;
        System.assertEquals(Name, acc.Name);
        Test.stopTest();
    }
    
    @isTest
    public static void testCreateAccountWithInsert(){
        
        String name = 'MonCompteCapgemini';
        
        Test.startTest();
        
        Account acc = TestDataFactory.createAccountWithInsert(name);
      
        System.assertNotEquals(null, acc.ID);
        Test.stopTest();
    }
    
     @isTest
    public static void testCreateAccountList(){
        
        String name = 'MaListedeCompteCapgemini';
        
        Test.startTest();
        
        List<Account> accs = TestDataFactory.createAccountList(name, 20);
        insert accs;
        System.assertEquals(20, accs.size());
        Test.stopTest();
    }
    
     /************************
     * Contact
     * *********************/
    
    @isTest
    public static void testCreateContact(){
        
        String name = 'MonCompteCapgemini';
        
        Test.startTest();
        
        Account acc = TestDataFactory.createAccount(name);
        insert acc;
        Contact ctc = TestDataFactory.createContact(acc);
        insert ctc;
        
        System.assertNotEquals(null, ctc.LastName);
        Test.stopTest();
    }
    
    @isTest
    public static void testCreateContactWithInsert(){
        
        String name = 'MonCompteCapgemini';
        
        Test.startTest();
        
        Contact ctc = TestDataFactory.createContactWithInsert(TestDataFactory.createAccountWithInsert(name));
        
        System.assertNotEquals(null,  ctc.ID);
        Test.stopTest();
    }
    
    @isTest
    public static void testCreateContactList(){
        
        String name = 'MonCompteCapgemini';
        
        Test.startTest();
        
        Account acc = TestDataFactory.createAccount(name);
        insert acc;
        List<Contact> ctcs = TestDataFactory.createContactList(acc, 10);
        
        System.assertEquals(10, ctcs.size());
        Test.stopTest();
    }
    
    
    /************************
     * Service Demand
     * *********************/
    
    @isTest
    public static void testCreateServiceDemand(){
        // Type : Repairs, Remediation, Elimination, EliminationSF6, Waste
        String name = 'MonCompteCapgemini';
        Account acc = TestDataFactory.createAccountWithInsert(name);
        Test.startTest();
        SD_ServiceDemand__c sd = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Repairs');
        insert sd;
        
        System.assertEquals(sd.ApplicantCenter__c, acc.Id);
        Test.stopTest();
    }
    
    /************************
     * Waste Instance
     * *********************/
    
    @isTest
    public static void testCreateWasteInstance(){
        
        String name = 'MonCompteCapgemini';
        Account acc = TestDataFactory.createAccountWithInsert(name);
        SD_ServiceDemand__c sd = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Repairs');
        insert sd;
        Test.startTest();
        WST_Waste_Statement__c wst = TestDataFactory.createWasteStatement(sd);
        insert wst;
        
        System.assertEquals(wst.ServiceDemande__c, sd.Id);
        Test.stopTest();
    }
    
    
    @isTest
    public static void testCreateWasteInstanceList(){
        
        String name = 'MonCompteCapgemini';
        Account acc = TestDataFactory.createAccountWithInsert(name);
        SD_ServiceDemand__c sd = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Repairs');
        insert sd;
        Test.startTest();
        List<WST_Waste_Statement__c> wsts = TestDataFactory.createWasteStatementList(sd, 100);
        insert wsts;
        
        System.assertEquals(100, wsts.size());
        Test.stopTest();
    }
    
    @isTest
    public static void testcreateCustomQuoteLineList_WST_Parent(){
        
        String name = 'MonCompteCapgemini';
        Account acc = TestDataFactory.createAccountWithInsert(name);
        SD_ServiceDemand__c sd = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Repairs');
        insert sd;
        Test.startTest();
        WST_Waste_Statement__c wst = TestDataFactory.createWasteStatement(sd);
        insert wst;
        PricebookEntry pbe = TestDataFactory.getPriceBookEntry();
        List<QuoteLine__c> qls = TestDataFactory.createCustomQuoteLineList_WST_Parent(wst, 55, pbe);
        
        System.assertEquals(55, qls.size());
        Test.stopTest();
    }
    
}