/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : PackageWrapper
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : PackageWrapper
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 13-05-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
public class PackageWrapper {

    public String Document{get;set;}		
    public String DocumentLanguage{get;set;}
    public String DocumentName{get;set;}
    public String PackageName{get;set;}
    public String Initiator{get;set;}
    public List<Stakeholder> Stakeholders{get;set;}
    public String ExternalDocumentReference{get;set;}
    public Boolean ReassignEnabled{get;set;}
    public List<SigningField> SigningFields{get;set;}
    public String TargetType {get;set;}
    public String PdfErrorHandling {get;set;}
    public String PackageId{get;set;} // use to sendReminder

    public class Stakeholder {
        public String FirstName{get;set;}
        public String LastName{get;set;}
        public String EmailAddress{get;set;}
        public String Language{get;set;}
        public String Type{get;set;}
        public String ExternalStakeholderReference{get;set;}
        public List<Actor> Actors{get;set;}
        public Boolean SendNotifications{get;set;}
    }
    
    public class Actor {
        public String Type{get;set;}
        public String RedirectURL{get;set;}
        public String RedirectType{get;set;}
        public Integer OrderIndex{get;set;}
        public List<SigningType> SigningTypes{get;set;}
        public List<SigningField> SigningFields{get;set;}
        public String Phonenumber{get;set;}
        public String[] LocationIds{get;set;}
        public Boolean SendNotifications{get;set;}
    }

    public class SigningType {
        public String SigningType{get;set;}
    }
    
    public class SigningField {
        public String Width{get;set;}
        public String Height{get;set;}
        public Integer PageNumber{get;set;}
        public String Left{get;set;}
        public String Top{get;set;}
        public String MarkerOrFieldId{get;set;}
        public String Label{get;set;}
        public SigningField(){}
    }
    
    public static sObject retrieveContact(Id objId){
        sObject returnObj = null;
        String sobjectType = objId.getSObjectType().getDescribe().getName();                 
        Schema.SObjectType convertType = Schema.getGlobalDescribe().get(sobjectType);
        if (convertType == Quote.sObjectType) {
            returnObj = [SELECT LastName, FirstName, Phone, Email, Title, Id FROM Contact c WHERE c.Id in (Select ContactId FROM Quote WHERE ID =: objId ) LIMIT 1];
        } else if (convertType == FIP_FIP__c.sObjectType){
            returnObj = [SELECT LastName, FirstName, Phone, Email, Title, Id FROM Contact c WHERE c.Id in (Select Contact__c FROM FIP_FIP__c WHERE ID =: objId ) LIMIT 1];
        }  else if(convertType == Contact.sObjectType){
            returnObj = [SELECT LastName, FirstName, Phone, Email, Title, Id FROM Contact c WHERE c.Id =:objId LIMIT 1];
        } else {
            System.debug('ERROR OBJECT NOT HANDLED BY CALLOUT');
        } 
        return returnObj;
    }
    
    public static ContentVersion retrieveDocument(Id objId){
        try{
            String sobjectType = objId.getSObjectType().getDescribe().getName();                 
            Schema.SObjectType convertType = Schema.getGlobalDescribe().get(sobjectType);
            if(convertType != ContentVersion.sObjectType){
                ContentDocumentLink cdl=[SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: objId Limit 1];
                ContentVersion cv = [select PathOnClient, ContentLocation, Origin, OwnerId, Title, VersionData, ContentDocumentId from ContentVersion where ContentDocumentId =: cdl.ContentDocumentId Limit 1];
                System.debug(cv);
                return cv;
            }
            else{
                ContentVersion cv = [select PathOnClient, ContentLocation, Origin, OwnerId, Title, VersionData, ContentDocumentId from ContentVersion where Id =:objId Limit 1];
                return cv;
            }
        } catch(Exception ex){
            System.debug('error no file found');
            return null;
        }
    }

    public static User retrieveSalesman(Id objId){
        try{
            String sobjectType = objId.getSObjectType().getDescribe().getName();                 
            Schema.SObjectType convertType = Schema.getGlobalDescribe().get(sobjectType);
            if(convertType == Quote.sObjectType){
                Quote q = [SELECT Id,Opportunity.Salesman__c FROM Quote WHERE Id =: objId Limit 1];
                if(q != null){
                    User us = [SELECT Id,FirstName, Lastname, Email from User where id =: q.Opportunity.Salesman__c Limit 1];
                    return us;
                }
            }
            return null;
        } catch(Exception ex){
            System.debug('error no file found');
            return null;
        }
    }

    public static String populatejson(Id objId){
        /*Field class use to build json output*/
        PackageWrapper pk=new PackageWrapper();
        Stakeholder sthClient = new Stakeholder();
        Stakeholder sthSF = new Stakeholder();
        Stakeholder sthSalesman = new Stakeholder();
        Actor acClient = new Actor();
        Actor acSF = new Actor();
        SigningType st = new SigningType();
        SigningField sf = new SigningField();
        List<Stakeholder> lstStakeholders = new List<Stakeholder>();
        List<Actor> lstActorsClient = new List<Actor>();
        List<Actor> lstActorsSF = new List<Actor>();
        List<SigningField> lstSigningFields = new List<SigningField>();
        List<SigningType> lstSigningTypes = new List<SigningType>();
        /* Field use into transform*/
        String FirstName = '';
        String phoneNumber = '';
        /* Field output*/
        String jsonBody='';
        /* Get attachment to send */
        ContentVersion cv=retrieveDocument(objId);
        /* Get contact link to attachment */
        Contact c=(Contact)retrieveContact(objId);
        User salesman = retrieveSalesman(objId);
        if(cv!=null){
            String documentBase64 = EncodingUtil.base64Encode(cv.VersionData);
            pk.Document = documentBase64;
            pk.DocumentLanguage = 'fr';
            pk.DocumentName = cv.Title.substringBefore('.');
            pk.Initiator = System.Label.Connective_Initiator;
            pk.ReassignEnabled = true;
            pk.TargetType = 'PdfA2A';
	        pk.PdfErrorHandling = 'DetectFixFail';

            pk.ExternalDocumentReference = cv.Id;
            
            /********* BEGIN Manage Stakeholder Client*********/
            /*Manage FirstName*/
            if(c.FirstName==null){
                FirstName='DefaultFirstName';
            }else{
                FirstName=c.FirstName; 
            }
            
            sthClient.FirstName = FirstName;
            sthClient.LastName = c.LastName;
            sthClient.EmailAddress = c.Email;
            
            sthClient.Language = 'fr';
            sthClient.Type = 'Person';
            sthClient.ExternalStakeholderReference = c.Id;

            /*Manage actor field*/
            acClient.Type = 'Signer';
            acClient.OrderIndex = 1;
            acClient.Phonenumber = phoneNumber;
            acClient.SendNotifications = true;
            acClient.RedirectURL = 'https://www.groupe-seche.com/fr';
            acClient.RedirectType = 'AfterCompletion';


            sf.MarkerOrFieldId = Label.ConnectiveSigningFieldId;
            /*sf.Width = '112';
            sf.Height = '70';
            sf.PageNumber = 1;
            sf.Left = '39';
            sf.Top = '741';*/
            st.SigningType = 'manual';

            /*affect list*/
            lstSigningFields.add(sf);
            lstSigningTypes.add(st);
            acClient.SigningFields = lstSigningFields;
            acClient.SigningTypes = lstSigningTypes;
            lstActorsClient.add(acClient);
            sthClient.Actors = lstActorsClient;
            /********* END Manage Stakeholder Client*********/
            /********* BEGIN Manage Stakeholder Salesforce*********/
            sthSF.FirstName = 'Salesforce';
            sthSF.LastName = 'Salesforce';
            sthSF.EmailAddress = 'sechecrm69@gmail.com';
            
            sthSF.Language = 'fr';
            sthSF.Type = 'Person';

            /*Manage actor field*/
            acSF.Type = 'Receiver';
            acSF.SendNotifications = false;
            lstActorsSF.add(acSF);
            sthSF.Actors = lstActorsSF;
            /********* END Manage Stakeholder Salesforce*********/
            /********* BEGIN Manage Stakeholder Salesforce*********/
            if (salesman != null){
                sthSF.FirstName = salesman.FirstName;
                sthSF.LastName = salesman.LastName;
                sthSF.EmailAddress = salesman.Email ;
                
                sthSF.Language = 'fr';
                sthSF.Type = 'Person';

                /*Manage actor field*/
                acSF.Type = 'Receiver';
                acSF.SendNotifications = true;
                lstActorsSF.add(acSF);
                sthSF.Actors = lstActorsSF;
            }
            /********* END Manage Stakeholder Salesforce*********/
            
            lstStakeholders.add(sthClient);
            lstStakeholders.add(sthSF);
            pk.Stakeholders = lstStakeholders;

            jsonBody = json.serialize(pk);
            System.debug(jsonBody);
        }else{
            jsonBody= '';
        }      
        return jsonBody;           
    }

    public static String populatejsonSendReminders(String pkgId){
        /*Field class use to build json output*/
        PackageWrapper pk=new PackageWrapper();
        /* Field output*/
        String jsonBody='';
        pk.PackageId = pkgId;
        jsonBody = json.serialize(pk);
        return jsonBody;
    }

    public static PackageWrapper deserializejson(String json){
        return (PackageWrapper) System.JSON.deserialize(json, PackageWrapper.class);
    }
}