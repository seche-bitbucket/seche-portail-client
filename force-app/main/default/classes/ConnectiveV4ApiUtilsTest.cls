/**
 * @description       : 
 * @author            : MAIT - @moidriss
 * @group             : 
 * @last modified on  : 11-21-2023
 * @last modified by  : MAIT - @moidriss
**/
@isTest
public class ConnectiveV4ApiUtilsTest {

    @testSetup
    public static void makeData() {
        ConnectiveData__c data = new ConnectiveData__c();
        data.SigningUrl__c = 'https://sechegroup-dev.connective.eu/esig/public/actioninit?';
        insert data;
    }
    
    @isTest
    public static void testFip() {

        ByPassUtils.ByPass('QuoteTrigger');
            
        QteLineItemSyncTriggerV2Handler.stop();
        OppLineItemSyncTriggerV2Handler.stop();
        QuoteSyncTriggerV2Handler.stop();
        OppSyncTriggerV2Handler.stop();
        
        Lead ld = TestFactory.generateLead();
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        ld.OwnerId = UserInfo.getUserId();
        update ld;

        Opportunity opp = TestFactory.generateOpport(acc, null);
        opp.Salesman__c = UserInfo.getUserId();
        opp.Assistant__c = UserInfo.getUserId();
        update opp;
        Quote qte = TestFactory.generateQuote(ct, acc, opp);
        
        FIP_FIP__c fip = TestFactory.generateFip(ct, acc);
        
        ContentDocumentLink cdl = TestFactory.linkDoc(fip.Id, 'testFip - packageOK');
        
        PackageV4Wrapper pkg = ConnectiveV4ApiUtils.createPackage(fip.Id, null);
        
        System.assertEquals('testFip - packageOK', pkg.Name);
        
        Id rtId = [SELECT Id FROM RecordType WHERE DeveloperName LIKE '%Attestation_Producteur_DND%'][0].Id;
        Attestation__c prefip = new Attestation__c(RecordTypeId=rtId, Filiale__c='Opale Environnement', ProducerEmail__c = 'test@test.com', ProducerFirstName__c = 'PTest', ProducerLastName__c = 'TEST', AssociatedSalesRep__c = [SELECT Email, Id FROM User WHERE Id = :UserInfo.getUserId()].Id, Contact__c = ct.Id, Account__c = acc.Id);
        insert prefip;

        Attachment att1 = TestFactory.linkAttachement(prefip, 'ATTPROD_testPrefip - packageOK');
        ContentDocumentLink cdl1 = TestFactory.linkDoc(prefip.Id, 'ATTPROD_testPrefip Final - packageOK');
        
        PackageV4Wrapper pkg1 = ConnectiveV4ApiUtils.createPackage(prefip.Id, ld.Id);
        
        System.assertEquals('ATTPROD_testPrefip Final - packageOK', pkg1.Name);
        
        //----Insert docs
        
        String body = TestFactory.getResponsePackage(ct, ConnectiveV4ApiUtils.retrieveDocument(prefip.Id).Id);
        
        testGenerateContentEmail(prefip.Id, qte, null);
        
        List<DocumentsForSignature__c> docs = ConnectiveV4ApiUtils.insertDocuments(body, fip.Id);
        List<DocumentsForSignature__c> docs1 = ConnectiveV4ApiUtils.insertDocuments(body, prefip.Id);
        
        System.assertEquals(1, docs.size());
        System.assertEquals(1, docs1.size());
        
        List<DocumentsForSignature__c> dcs = new List<DocumentsForSignature__c>();
        
        dcs.addAll(docs);
        dcs.addAll(docs1);
        
        Map<Package__c,List<DocumentsForSignature__c>> mapDocs = ConnectiveV4ApiUtils.getPackageDoc(dcs);
        
        System.assertEquals(2, mapDocs.size());
        
        String bodySigned = TestFactory.getResponseStatus('Signed', 'Finished', null, ConnectiveV4ApiUtils.retrieveDocument(prefip.Id).Id);
        String bodyRejected = TestFactory.getResponseStatus('Rejected', 'Rejected', 'Testing', ConnectiveV4ApiUtils.retrieveDocument(prefip.Id).Id);
        
        Package__c pkgSign = new Package__c(ConnectivePackageId__c = 'packageOK');
        Package__c pkgRejected = new Package__c(ConnectivePackageId__c = 'Rejected');
    	
        Insert new List<Package__c>{pkgRejected, pkgSign};
        
        DocumentsForSignature__c docSign = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign.Id, TECH_ExternalId__c='Signed', Attestation__c=prefip.Id);
        DocumentsForSignature__c docRejected = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgRejected.Id, TECH_ExternalId__c='Rejected', Quote__c=qte.Id);
        
        Insert new List<DocumentsForSignature__c>{docSign, docRejected};
        
        ConnectiveV4ApiUtils.updateDocumentForSig((Map<String, Object>)JSON.deserializeUntyped(bodySigned), 'packageOK');
        ConnectiveV4ApiUtils.updateDocumentForSig((Map<String, Object>)JSON.deserializeUntyped(bodyRejected), 'Rejected');
        
        List<ID> lstIds = new List<ID>{docSign.Id, docRejected.Id};
        
        List<DocumentsForSignature__c> docSigns = [SELECT Id, Status__c FROM DocumentsForSignature__c WHERE ID IN :lstIds];
        
        System.assertEquals('FINISHED', docSigns[0].Status__c);
        System.assertEquals('REJECTED', docSigns[1].Status__c);

        Attestation__c prefip2 = new Attestation__c(Filiale__c='Séché Eco Industries La Dominelais', Producer__c =true, Mixed__c = false, Sorted__c = true, ProducerEmail__c = 'test@test.com', ProducerFirstName__c = 'PTest', ProducerLastName__c = 'TEST', AssociatedSalesRep__c = [SELECT Email, Id FROM User WHERE Id = :UserInfo.getUserId()].Id, Contact__c = ct.Id, Account__c = acc.Id);
        insert prefip2;
        
        DocumentsForSignature__c docSignForTrigger = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign.Id, TECH_ExternalId__c='Signed', Attestation__c=prefip2.Id);
        DocumentsForSignature__c docSignForTrigger2 = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign.Id, TECH_ExternalId__c='Signed', Quote__c=qte.Id);

        insert new List<DocumentsForSignature__c>{docSignForTrigger, docSignForTrigger2};
            
        User user = [SELECT ID, Email, Assistant__r.Email, Assistant__c FROM User WHERE ID = :UserInfo.getUserId()];
        
        Map<String, Object> mp1 = ConnectiveV4ApiUtils.updateDocumentForSigTrigger(docSignForTrigger, 'Finished', null, 'url', null, prefip, user, user.Email, 'test@testcap.com', null, null);
        Map<String, Object> mp2 = ConnectiveV4ApiUtils.updateDocumentForSigTrigger(docSignForTrigger2, 'Rejected', 'Test', 'url', null, prefip, user, user.Email, 'test@testcap.com', null, null);
        
        List<ID> lstIds2 = new List<ID>{docSignForTrigger.Id, docSignForTrigger2.Id};
        
        List<DocumentsForSignature__c> docSignsTrigger = [SELECT Id, Status__c FROM DocumentsForSignature__c WHERE ID IN :lstIds2];        
    }
    
    @isTest
    public static void testQte() {
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        Opportunity opp = TestFactory.generateOpport(acc, null);
        Quote qte = TestFactory.generateQuote(ct, acc, opp);
        Attachment att2 = TestFactory.linkAttachement(qte, 'testQuote - packageOK');
        ContentDocumentLink cdl2 = TestFactory.linkDoc(qte.Id, 'testQuote - packageOK');

        ContentVersion doc = ConnectiveV4ApiUtils.retrieveDocument(qte.Id);
        
        PackageV4Wrapper pkg2 = ConnectiveV4ApiUtils.createPackage(qte.Id, ct.Id);
        
        Contact cont = (Contact) ConnectiveV4ApiUtils.retrieveContact(qte.Id);
        testGenerateContentEmail(qte.Id, qte, null);
        
        //System.assertEquals('testQuote - packageOK', pkg2.Name);
        
    }
    
    public static void testGenerateContentEmail(Id objId, Quote quote, Attestation__c att){
        User user = [SELECT Id, user.Email, user.Assistant__r.Email FROM User WHERE Id =: UserInfo.getUserId()];
        DocumentsForSignature__c doc = new DocumentsForSignature__c(
            Package__r = new Package__c(ConnectivePackageId__c = 'Package123'), 
            TECH_ExternalId__c='Signed', 
            Status__c = 'PENDING', 
            CAP__c = TestFactory.generateCap(null, null).Id
        );
        String status = 'Signé';
        ConnectiveV4ApiUtils.generateContentEmail(objId, status, doc, null, att);
        ConnectiveV4ApiUtils.generateContentEmail(objId, status, doc, user, att);

        if(quote!=null){
            Id quoteId = quote.Id;
        	List<String> fields = new List<String>(quoteId.getSObjectType().getDescribe().fields.getMap().keySet());
        	quote = Database.query('SELECT '+ String.join(fields, ',') + ' FROM Quote WHERE Id = :quoteId');
            ConnectiveV4ApiUtils.generateContentEmailTrigger(doc, status, quote, user, att, 'test01@cap.com', 'test02@cap.com'); 
        }
    }
    
    @isTest
    public static void testGetActorIdAndgetUrl(){
        Id signerId = UserInfo.getUserId();
        String stakeholder = '{"Id": "ActorId123","Type": "Signer" , "Links": ["https://sechegroup-dev.connective.eu"], "Elements" : [{"ActorId": "ActorId123", "Type": "SigningField"}]}';
        Map<String, Object> stakeholders = (Map<String, Object>)System.JSON.deserializeUntyped('{"Id": "Stakeholder123","ExternalReference": "'+signerId+'", "Actors": ['+stakeholder+']}');
        List<Object> actors = (List<Object>)stakeholders.get('Actors');
        Map<String, Object> actor = (Map<String, Object>) actors[0];
        String actorId = (String)actor.get('Id');
        
        List<Object> elements = (List<Object>) actor.get('Elements');
        ConnectiveV4ApiUtils.getUrlFromActorId(actors, actorId);
        ConnectiveV4ApiUtils.getActorIdFromSigningField(elements);
        ConnectiveV4ApiUtils.getSignerId(new List<Map<String, Object>> {stakeholders});
    }
    
    @isTest
    public static void testCap() {
        Contact con = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        CAP__c cap = TestFactory.generateCap(acc.Id, con.Id);
        cap.TECH_SendWithConnective__c = true;
        update cap;
        
        ContentDocumentLink cdl = TestFactory.linkDoc(cap.Id, 'testCap');
        ContentVersion doc = ConnectiveV4ApiUtils.retrieveDocument(cap.Id);
        
        PackageV4Wrapper pkg = ConnectiveV4ApiUtils.createPackage(con.Id, con.Id);
        Contact c = (Contact) ConnectiveV4ApiUtils.retrieveContact(con.Id);
        
        List<DocumentsForSignature__c> docs1 = ConnectiveV4ApiUtils.insertDocuments(TestFactory.getResponsePackage(doc.Id), con.Id);
        List<DocumentsForSignature__c> docs2 = testGetPackageDoc(doc.Id, cap.Id);
        
        User user = [SELECT Id, Email FROM User WHERE Id = :UserInfo.getUserId()];
        ConnectiveV4ApiUtils.updateDocumentForSigTrigger(docs2[0], 'Finished', null, 'url', null, null, user, user.Email, 'test@testcap.com', null, cap);
    }
    
    private static List<DocumentsForSignature__c> testGetPackageDoc(Id docId, Id capId){
        Map<String, Object> pkgResp = (Map<String, Object>) System.JSON.deserializeUntyped(TestFactory.getResponsePackage(docId));
        
        Package__c pkg = new Package__c();
        pkg.ConnectivePackageId__c = (String)pkgResp.get('Id');
        pkg.ConnectivePackageStatus__c = (String)pkgResp.get('Status');
        insert pkg;
        
        List<Object> docsResp = (List<Object>)pkgResp.get('Documents');
        Map<String, Object> docResp = (Map<String, Object>) docsResp[0];
        
        List<DocumentsForSignature__c> docs = new List<DocumentsForSignature__c>();
        DocumentsForSignature__c doc = new DocumentsForSignature__c();
        doc.TECH_ExternalId__c = (String)docResp.get('ExternalReference');
        doc.CAP__c = capId;
        DocumentsForSignature__c doc2 = doc.clone(false, false, false);
        doc2.Package__c = pkg.Id;
        docs.add(doc);
        docs.add(doc2);
        insert docs;
        
        ConnectiveV4ApiUtils.getPackageDoc(docs);
        
        ConnectiveV4ApiUtils.updateDocumentForSig((Map<String, Object>)System.JSON.deserializeUntyped(TestFactory.getResponseStatus(pkg.ConnectivePackageId__c, 'Finished', '', docId)), pkg.ConnectivePackageId__c);
        
        ConnectiveV4ApiUtils.updateDocumentForSig((Map<String, Object>)System.JSON.deserializeUntyped(TestFactory.getResponseStatus(pkg.ConnectivePackageId__c, 'Rejected', 'TEST', docId)), pkg.ConnectivePackageId__c);
        
        return docs;
    }
    
    @isTest 
    public static void testLead() {

        Lead leadUnderContract = new Lead (LastName = 'Test Last Name', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Urgence Assainissement', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),'LED_WEB_TO_LEAD_ASSAINISSEMENT'),PostalCode = '69009',
                                 Quantity__c = 200, Filiale__c = 'Séché Eco Industries Changé',
                                 CustomerNature__c = 'Industriel', Industry = 'Chimie', Email = 'Test@Test.com', IsUnderContract__c = true);

        insert leadUnderContract;

        Attachment att2 = TestFactory.linkAttachement(leadUnderContract, 'testLead');
        ContentDocumentLink cdl2 = TestFactory.linkDoc(leadUnderContract.Id, 'testLead');
        Lead lead = (Lead) ConnectiveV4ApiUtils.retrieveContact(leadUnderContract.Id);
        
        List<Id> leadIds = new List<Id>();
        leadIds.add(leadUnderContract.Id);

        SendLeadConnective.SendLeadToSign(leadIds);
        testGenerateContentEmail(leadUnderContract.Id, null, null);
    }

}