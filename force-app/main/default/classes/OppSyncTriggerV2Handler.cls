public class OppSyncTriggerV2Handler {
    
    private static Boolean stop = false;
    
    public static void stop() {
        stop = true;
    }
    
    Public static void execute(List<Opportunity> newLst, Map<Id, Opportunity> oldMap) {
        if(!stop) {
            SyncHandler.SyncHandler(newLst, oldMap, 'Opportunity');
        }
    }

}