@isTest(SeeAllData=true)
public class TRANSFO_ManageSF6PriceBookTest {
    
    //FINISHED
    @isTest
    public static void  getPriceBookEntriesTest(){
        Test.startTest(); 
        
        //Create Service Demande
        String name = 'AccountName1';
        Account acc = TestDataFactory.createAccountWithInsert(name);
        SD_ServiceDemand__c vServiceD = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Repairs');
        
        //insert Service Demande
        insert vServiceD;
       
        //call method
        List<QuoteLine__c> vResults  = TRANSFO_ManageSF6PriceBook.getPriceBookEntries(vServiceD.Id);
        
        //check the result
        System.debug('getPriceBookEntriesTest.vResults' + vResults);  
        System.assertNotEquals(null, vResults);
        
		Test.stopTest(); 
       
    }
    
    
    //NOT FINISHED   - il faut serialiser 
    @isTest
    public static void  saveQuoteLinesTest(){
        Test.startTest(); 
        
        //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        
        //insert Waste_Statement
        insert vWst1;
        
        
        //Create Service Demande
        String name = 'AccountName1';
        Account acc = TestDataFactory.createAccountWithInsert(name);

        SD_ServiceDemand__c vServiceD = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Repairs');
        
        //insert Service Demande
        insert vServiceD;
        
        
        List<QuoteLine__c> vLquoteLine = new List<QuoteLine__c>();
        
         //Create QuoteLine  
        QuoteLine__c vQuoteline = new QuoteLine__c();
 
         
        //link QuoteLine to ServiceDemande
        vQuoteline.ServiceDemand__c = vServiceD.Id;
          
        //link QuoteLine to TECH_PriceBookEntryID__c  
        vQuoteline.TECH_PriceBookEntryID__c='01u9E00000A0sGcQAJ';
          
        
        insert vQuoteline;
        
        vLquoteLine.add(vQuoteline);
        update vLquoteLine;
        
        //call method
        List<QuoteLine__c> vResult = TRANSFO_ManageSF6PriceBook.saveQuoteLines(JSON.serialize(vLquoteLine),vServiceD.Id);
            
        //check the result
        System.debug('saveQuoteLinesTest.vResult = ' + vResult);
        System.assertNotEquals(null, vResult);
        
        Test.stopTest();
    }
    
    
    
    
    //FINISHED  ServiceDemande = 'En Contruction' , result return true 
      @isTest
    public static void  isEditableServiceDemandTest(){
        Test.startTest(); 
        
        //Create Service Demande
        String name = 'AccountName1';
        Account acc = TestDataFactory.createAccountWithInsert(name);

        SD_ServiceDemand__c vServiceD = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Repairs');
        vServiceD.Status__c ='En construction';
        
        //insert Service Demande
        insert vServiceD;
        
        //call method
		Boolean vResult = TRANSFO_ManageSF6PriceBook.isEditableServiceDemand(vServiceD.Id);
        
         //check the result
         System.debug('isEditableServiceDemandTest.vResult' + vResult);  
        System.assertEquals(true, vResult);

        Test.stopTest();
    }
    
    
        //FINISHED  ServiceDemande != 'En Contruction' , result return false 
      @isTest
      public static void  isEditableServiceDemandTest2(){
        Test.startTest(); 
        
        //Create Service Demande
        String name = 'AccountName1';
        Account acc = TestDataFactory.createAccountWithInsert(name);

        SD_ServiceDemand__c vServiceD = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Repairs');
        vServiceD.Status__c ='Logistique';
        
        //insert Service Demande
        insert vServiceD;
        
        //call method
		Boolean vResult = TRANSFO_ManageSF6PriceBook.isEditableServiceDemand(vServiceD.Id);
        
         //check the result
        System.debug('isEditableServiceDemandTest2.vResult' + vResult);  
        System.assertEquals(false, vResult);

        Test.stopTest();
    }
    
    
    //FINISHED at 90%
    @isTest
      public static void  getQuoteLinesTest(){
        Test.startTest();
        //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        
        //insert Waste_Statement
        insert vWst1;
          
          
		//Create Service Demande
        String name = 'AccountName1';
        Account acc = TestDataFactory.createAccountWithInsert(name);

        SD_ServiceDemand__c vServiceD = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Repairs');
        
        //insert Service Demande
        insert vServiceD;
          
        //Create QuoteLine  
        QuoteLine__c vQuoteline = new QuoteLine__c();
 		System.debug('getQuoteLinesTest.vQuoteline' + vQuoteline); 
        
                   
        //link QuoteLine to ServiceDemande
        vQuoteline.ServiceDemand__c = vServiceD.Id;
          
         //link QuoteLine to TECH_PriceBookEntryID__c  
        vQuoteline.TECH_PriceBookEntryID__c='01u9E00000A0sGcQAJ';
          
         //TODO to complete
        insert vQuoteline;
          
        
        //call method
		Map<ID, QuoteLine__c> vResult = new Map<ID, QuoteLine__c>();
        vResult = TRANSFO_ManageSF6PriceBook.getQuoteLines(vServiceD.Id);
        System.debug('getQuoteLinesTest.vResult' + vResult); 
        System.assertNotEquals(null, vResult);
        
        Test.stopTest();
      }
    
    
    
}