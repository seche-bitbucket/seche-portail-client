public class FIPStatusController {

    @AuraEnabled
    public static Boolean setAccepted(Id recordId){
        System.debug('Rec1'+recordId);
        FIP_FIP__c fip = [Select ID, State__c FROM  FIP_FIP__c WHERE ID =: recordID];
        
        fip.State__c = 'Approuvée';
        fip.AcceptedBy__c = UserInfo.getUserId();
        
        try{
            update fip;
            return true;
        }catch(Exception e){
            return false;
        }
       
    }
    
     @AuraEnabled
    public static Boolean setRefused(Id recordId){
        System.debug('Rec2'+recordId);
        FIP_FIP__c fip = [Select ID, State__c FROM  FIP_FIP__c WHERE ID =: recordID];
        
        fip.State__c = 'Refusée';
        
        try{
            update fip;
            return true;
        }catch(Exception e){
            return false;
        }
       
    }
    
    @AuraEnabled
    public static ListView getListView() {
                System.debug('getListView');

        ListView listview = 
            [SELECT Id, Name FROM ListView WHERE SobjectType = 'FIP_FIP__c' AND Name ='FIPToApprouve'];
        
        // Perform isAccessible() check here
        return listview;
    }
    
}