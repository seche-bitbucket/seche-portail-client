/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Mohammed Amine IDRISSI TAGHKI <mohammed-amine.idrissi-taghki@capgemini.com>
 * @version        1.0
 * @created        2021-09-14
 * @systemLayer    Extend Base Model
 *
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Desciption: Child class extends base model
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

public without sharing class CB_Case extends PolyMorphic_BaseClass {

    override
    public Object VInsert(String data, String apiName){
        try {    
            SObject sobj = (SObject)JSON.deserialize(data, Type.forName('Schema.'+apiName));
            Id arDDE = [SELECT Id FROM AssignmentRule WHERE Name = 'Case Assignment Rule'].Id;

            System.debug('arDDE '+arDDE);
            
            Database.DMLOptions dmar = new Database.DMLOptions();
            dmar.AssignmentRuleHeader.assignmentRuleId = arDDE;
            
            if(!Test.isRunningTest()) {
                Database.insert(sobj, dmar);
            } else {
                insert sobj;
            }

            System.debug('sobj '+sobj);

            return (String)sobj.get('Id');

        } catch (Exception e) {
            Map<String, Object> errorMap = new Map<String, Object>();
            errorMap.put('message', e.getMessage());
            errorMap.put('line', e.getLineNumber());
            errorMap.put('source', 'CB_Case');
            errorMap.put('inputs', new Map<String, String>{'data' => data,
                                                           'apiName' => apiName});
            return errorMap;
        }
    }

}