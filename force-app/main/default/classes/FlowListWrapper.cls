public class FlowListWrapper {
    public String ContinuationToken{get;set;}
    public Integer MaxQuantity{get;set;}
    public Integer Total{get;set;}
    public List <Item> Items{get;set;} 
    public class Item{
        public String id{get;set;}
        public String FlowStatus{get;set;}
        public String ExternalReference{get;set;}
    }
        public static FlowListWrapper deserializejson(String json){
        return (FlowListWrapper) System.JSON.deserialize(json, FlowListWrapper.class);
    }
}