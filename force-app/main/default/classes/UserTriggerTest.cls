@isTest
public class UserTriggerTest {
    @isTest(seeAllData=true)
    static void testUser(){
        

        List<Profile> prof = [select id, name from Profile where  name = 'Customer Community Amiante'];
        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        Contact con = new Contact(LastName ='testCon',AccountId = a.Id);
        insert con;  

        String orgId=UserInfo.getOrganizationId(); 
        String dateString=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;

        Integer RandomId=Integer.valueOf(Math.rint(Math.random()*1000000));        
        String uniqueName=orgId+dateString+RandomId; 

       Test.startTest();

        User u = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileId = prof[0].Id, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username=uniqueName + '@test' + orgId + '.org');
       

        insert u;  
       
        u.firstName = 'test11';       
        Test.stopTest();
        //update u;
        Account vAccount = [SELECT Id,TECH_UID__c FROM Account WHERE id=:con.accountid];    
          System.assertNotEquals(null,vAccount.TECH_UID__c);
    }
}