public class RegionTriggerBeforInsertUpdateHandler {
    
    public static boolean stop = false;
    
    public static void handler(List<Account> lst) {
        
        if(!stop) {
        
            Map<Account, String> accs = new Map<Account, String>();
            String pc;
            RecordType r = [SELECT  Id FROM RecordType WHERE SobjectType='Account' AND Name='France'];
            for(Account a : lst){
                //Filling the Map with accounts as keys and the two first letters billingPostalCode
                if(a.BillingPostalCode != NULL && a.BillingPostalCode.length()>=2 && a.RecordTypeId == r.Id){
                    pc = a.BillingPostalCode.substring(0,2);
                    if (pc == '97' || pc == '98'){
                        pc = a.BillingPostalCode.substring(0,3);
                    }
                    accs.put(a,pc);           
                }                      
            }
            //getting the regions with code__c in values of billingsPostalCode
            LIST<Regions__c> re =[SELECT Id,Code__c, Name, Country__c FROM Regions__c WHERE Code__c in:accs.values()];
            
            // Fill the fields of accounts with those of regions
            for(Account Acc : accs.keySet()) {
                for(Regions__c reg : re){
                    if(reg.Code__c == accs.get(Acc)){
                        Acc.BillingState = reg.Name;
                        Acc.BillingCountry = reg.Country__c;
                    }
                }
            } 
            
        }
        
    }

}