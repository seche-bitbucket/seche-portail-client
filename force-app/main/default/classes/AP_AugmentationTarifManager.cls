/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Baïla TOURE <baila.toure@capgemini.com>
 * @version        1.0
 * @created        2018-08-16
 * @systemLayer    Manager
 * @Update :17/03/2020 , author Baïla TOURE, Modif: optimize SOQL
 * @Update :16/04/2020 , author PM BARRAL, Modif: increaseAMEFaf
 * @Update :07/05/2020, author Maxime GRENIER, Modif: regroupContracts
 * @Update :28/04/2023, author Tiphaine VIANA, Modif: AreUserStandardContractsSubmitted
 * 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Desciption: This class contains methods to handle the increase price of contracts and amendements
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

public with sharing class AP_AugmentationTarifManager {


	//Method to fetch the current user Contracts and their related amendments
	public static List<Contract__c> retrieveUserContracts(String userId, String filiale, String filiere, String natureCompte, String status){
	
		System.debug('AP_AugmentationTarifManager.retrieveUserContracts---> Start :parms filiale ='+filiale+' filière ='+filiere+' natureCompte ='+natureCompte);
		Id profileId = UserInfo.getProfileId();
		String profileName =[Select Id, Name from Profile where Id=:profileId].Name;
		
		List<Contract__c> contracts = new List<Contract__c> ();
		
		if (profileName=='Responsable commercial'||profileName=='Directeur commercial'||profileName=='Chargé(e) d\'affaires') {
			if(natureCompte!=''){
				contracts = [SELECT Id,Name,ContractType__c,Type_de_convention__c,ContractCode__c,Status__c,Filiale__c, Filiere__c, RecordLocked__c,Account__c, Account__r.Name, Account__r.TECH_X3SECHEID__c, Account__r.ChargedX3Code__c, Contrat_face_face__c,IncreaseRatioNumber__c,
											IncreaseRatio__c,CustomerNature__c,ContactRelated__r.Name,ContactRelated__r.Actif__c,ContactRelated__r.Email,ContactRelated__c,AmountOfAmandements__c,CA_N__c,TreatedContract__c,AddedValue__c,TreatedAmandements__c,Opportunity__r.Name,
											ContractEndDate__c,EstimateTurnoverNextYear__c,RegroupmentIndex__c,AssociatedSalesRep__c,AssociatedSalesRep__r.Name,Face_Face__c,TECH_Excluded__c,IncreaseComment__c,
											IsUnderContract__c, PriceNextyearFromERP__c, (SELECT Id, Name,Contrat2__r.RecordLocked__c,IsUnderContract__c,Augmentation__c,ThirdPartyProducerCode__c,CompletedAmadement__c,AugmentationRatio__c,MinimumTonnage__c,ThirdPartyProducerName__c,WasteLabel__c,Conditionnement__c, ByPassContract__c, Nature__c,PriceCurrentYear__c,PriceNextYear__c,Contrat2__r.Id,Contrat2__r.Name, Contrat2__r.Status__c,
											Contrat2__r.Type_de_convention__c,Contrat2__r.Filiale__c,Filiale__c, Filiere__c, Account__c, ContactRelated__c, CustomerNature__c, Commercial_Associe__c, Face_face__c, UnitNextYear__c, Account__r.Name,
											Recurrence__c, LabelArticleCode__c, ArticleCode__c, TechnicInfo__c, IncludedTGAP__c, CEECode__c, OnuCode__c, Label__c, HeaderContractX3__c, FooterContractX3__c,
											Keywords__c, AnalyticSection__c, ActifAcceptationContrat__c, TreatmentRange__c, PackagingGroup__c, Class__c, TT_X3__c, TextSalesX3__c, CommentaryAmendementsX3__c,
											QuantityPreviousYear__c, QuantityCurrentYear__c, QuantityNextYear__c, Planned_next_year__c, PricePreviousYear__c, UnitPreviousYear__c, UnitCurrentYear__c, QuantityInvoiced2018__c,
											Chiffre_d_affaires_N_du_Contrat__c, CA_N__c, State__c, Indexation_gasoil__c, Communal_tax__c,TECH_Excluded__c,ExclusionComment__c, SheetNumber__c,
											TreatmentQuantityPreviousYear__c, TreatmentQuantityNextYear__c, TreatmentQuantityCurrentYear__c, SpecialContribution__c, GoLitrePrice__c, FuelPart__c, PriceNextyearFromERP__c FROM Avenants__r)
											FROM Contract__c WHERE AssociatedSalesRep__c=:userId AND Filiale__c =:filiale AND Filiere__c =:filiere   AND CustomerNature__c =:natureCompte  AND Status__c IN ('Brouillon','Rejete')];/*TO DO replace by CreatedDate = THIS_YEAR */
			}else{
				contracts = [SELECT Id,Name,ContractType__c,Type_de_convention__c,ContractCode__c,Status__c,Filiale__c, Filiere__c, RecordLocked__c,Account__c, Account__r.Name, Account__r.TECH_X3SECHEID__c, Account__r.ChargedX3Code__c, Contrat_face_face__c,IncreaseRatioNumber__c,
											IncreaseRatio__c,CustomerNature__c,ContactRelated__r.Name,ContactRelated__r.Actif__c,ContactRelated__r.Email,ContactRelated__c,AmountOfAmandements__c,CA_N__c,TreatedContract__c,AddedValue__c,TreatedAmandements__c,Opportunity__r.Name,
											ContractEndDate__c,EstimateTurnoverNextYear__c,RegroupmentIndex__c,AssociatedSalesRep__c,AssociatedSalesRep__r.Name,Face_Face__c,TECH_Excluded__c,IncreaseComment__c,
											IsUnderContract__c, PriceNextyearFromERP__c, (SELECT Id, Name,Contrat2__r.RecordLocked__c,IsUnderContract__c,Augmentation__c,ThirdPartyProducerCode__c,CompletedAmadement__c,AugmentationRatio__c,MinimumTonnage__c,ThirdPartyProducerName__c,WasteLabel__c,Conditionnement__c, ByPassContract__c, Nature__c,PriceCurrentYear__c,PriceNextYear__c,Contrat2__r.Id,Contrat2__r.Name, Contrat2__r.Status__c,
											Contrat2__r.Type_de_convention__c,Contrat2__r.Filiale__c,Filiale__c, Filiere__c, Account__c, ContactRelated__c, CustomerNature__c, Commercial_Associe__c, Face_face__c, UnitNextYear__c, Account__r.Name,
											Recurrence__c, LabelArticleCode__c, ArticleCode__c, TechnicInfo__c, IncludedTGAP__c, CEECode__c, OnuCode__c, Label__c, HeaderContractX3__c, FooterContractX3__c,
											Keywords__c, AnalyticSection__c, ActifAcceptationContrat__c, TreatmentRange__c, PackagingGroup__c, Class__c, TT_X3__c, TextSalesX3__c, CommentaryAmendementsX3__c,
											QuantityPreviousYear__c, QuantityCurrentYear__c, QuantityNextYear__c, Planned_next_year__c, PricePreviousYear__c, UnitPreviousYear__c, UnitCurrentYear__c, QuantityInvoiced2018__c,
											Chiffre_d_affaires_N_du_Contrat__c, CA_N__c, State__c, Indexation_gasoil__c, Communal_tax__c,TECH_Excluded__c,ExclusionComment__c, SheetNumber__c,
											TreatmentQuantityPreviousYear__c, TreatmentQuantityNextYear__c, TreatmentQuantityCurrentYear__c, SpecialContribution__c, GoLitrePrice__c, FuelPart__c, PriceNextyearFromERP__c FROM Avenants__r)
											FROM Contract__c WHERE AssociatedSalesRep__c=:userId AND Filiale__c =:filiale AND Filiere__c =:filiere  AND Status__c IN ('Brouillon','Rejete')];/*TO DO replace by CreatedDate = THIS_YEAR */
			}
		} else { // when else block, optional
			if(natureCompte!=''){
				contracts = [SELECT Id,Name,ContractType__c,Type_de_convention__c,ContractCode__c,Status__c,Filiale__c, Filiere__c, RecordLocked__c,Account__c,Account__r.Name, Account__r.TECH_X3SECHEID__c, Contrat_face_face__c, Account__r.ChargedX3Code__c, IncreaseRatioNumber__c,
											IncreaseRatio__c,CustomerNature__c,ContactRelated__r.Name,ContactRelated__r.Actif__c,ContactRelated__r.Email,ContactRelated__c,AmountOfAmandements__c,CA_N__c,TreatedContract__c,AddedValue__c,TreatedAmandements__c,Opportunity__r.Name,
											ContractEndDate__c,EstimateTurnoverNextYear__c,RegroupmentIndex__c,AssociatedSalesRep__c,AssociatedSalesRep__r.Name,Face_Face__c,TECH_Excluded__c,IncreaseComment__c,
											IsUnderContract__c, PriceNextyearFromERP__c, (SELECT Id, Name,Contrat2__r.RecordLocked__c,IsUnderContract__c,Augmentation__c,ThirdPartyProducerCode__c,CompletedAmadement__c,AugmentationRatio__c,MinimumTonnage__c,ThirdPartyProducerName__c,WasteLabel__c,Conditionnement__c, ByPassContract__c, Nature__c,PriceCurrentYear__c,PriceNextYear__c,Contrat2__r.Id,Contrat2__r.Name, Contrat2__r.Status__c,
											Contrat2__r.Type_de_convention__c,Contrat2__r.Filiale__c, Filiale__c, Filiere__c, Account__c, ContactRelated__c, CustomerNature__c, Commercial_Associe__c, Face_face__c, UnitNextYear__c, Account__r.Name,
											Recurrence__c, LabelArticleCode__c, ArticleCode__c, TechnicInfo__c, IncludedTGAP__c, CEECode__c, OnuCode__c, Label__c, HeaderContractX3__c, FooterContractX3__c,
											Keywords__c, AnalyticSection__c, ActifAcceptationContrat__c, TreatmentRange__c, PackagingGroup__c, Class__c, TT_X3__c, TextSalesX3__c, CommentaryAmendementsX3__c,
											QuantityPreviousYear__c, QuantityCurrentYear__c, QuantityNextYear__c, Planned_next_year__c, PricePreviousYear__c, UnitPreviousYear__c, UnitCurrentYear__c, QuantityInvoiced2018__c,
											Chiffre_d_affaires_N_du_Contrat__c, CA_N__c, State__c, Indexation_gasoil__c, Communal_tax__c,TECH_Excluded__c,ExclusionComment__c, SheetNumber__c,
											TreatmentQuantityPreviousYear__c, TreatmentQuantityNextYear__c, TreatmentQuantityCurrentYear__c, SpecialContribution__c,GoLitrePrice__c, FuelPart__c, PriceNextyearFromERP__c FROM Avenants__r)
											FROM Contract__c WHERE  Filiale__c =:filiale AND Filiere__c =:filiere AND CustomerNature__c =:natureCompte AND Status__c IN ('Brouillon','Rejete')]; /*TO DO replace by CreatedDate = THIS_YEAR */
			//  WHERE  Filiale__c =:filiale AND Filiere__c =:filiere ];
			}else{
				contracts = [SELECT Id,Name,ContractType__c,Type_de_convention__c,ContractCode__c,Status__c,Filiale__c, Filiere__c, RecordLocked__c,Account__c,Account__r.Name, Account__r.TECH_X3SECHEID__c, Contrat_face_face__c, Account__r.ChargedX3Code__c, IncreaseRatioNumber__c,
											IncreaseRatio__c,CustomerNature__c,ContactRelated__r.Name,ContactRelated__r.Actif__c,ContactRelated__r.Email,ContactRelated__c,AmountOfAmandements__c,CA_N__c,TreatedContract__c,AddedValue__c,TreatedAmandements__c,Opportunity__r.Name,
											ContractEndDate__c,EstimateTurnoverNextYear__c,RegroupmentIndex__c,AssociatedSalesRep__c,AssociatedSalesRep__r.Name,Face_Face__c,TECH_Excluded__c,IncreaseComment__c,
											IsUnderContract__c, PriceNextyearFromERP__c, (SELECT Id, Name,Contrat2__r.RecordLocked__c,IsUnderContract__c,Augmentation__c,ThirdPartyProducerCode__c,CompletedAmadement__c,AugmentationRatio__c,MinimumTonnage__c,ThirdPartyProducerName__c,WasteLabel__c,Conditionnement__c, ByPassContract__c, Nature__c,PriceCurrentYear__c,PriceNextYear__c,Contrat2__r.Id,Contrat2__r.Name, Contrat2__r.Status__c,
											Contrat2__r.Type_de_convention__c,Contrat2__r.Filiale__c, Filiale__c, Filiere__c, Account__c, ContactRelated__c, CustomerNature__c, Commercial_Associe__c, Face_face__c, UnitNextYear__c, Account__r.Name,
											Recurrence__c, LabelArticleCode__c, ArticleCode__c, TechnicInfo__c, IncludedTGAP__c, CEECode__c, OnuCode__c, Label__c, HeaderContractX3__c, FooterContractX3__c,
											Keywords__c, AnalyticSection__c, ActifAcceptationContrat__c, TreatmentRange__c, PackagingGroup__c, Class__c, TT_X3__c, TextSalesX3__c, CommentaryAmendementsX3__c,
											QuantityPreviousYear__c, QuantityCurrentYear__c, QuantityNextYear__c, Planned_next_year__c, PricePreviousYear__c, UnitPreviousYear__c, UnitCurrentYear__c, QuantityInvoiced2018__c,
											Chiffre_d_affaires_N_du_Contrat__c, CA_N__c, State__c, Indexation_gasoil__c, Communal_tax__c,TECH_Excluded__c,ExclusionComment__c, SheetNumber__c,
											TreatmentQuantityPreviousYear__c, TreatmentQuantityNextYear__c, TreatmentQuantityCurrentYear__c, SpecialContribution__c, GoLitrePrice__c, FuelPart__c, PriceNextyearFromERP__c FROM Avenants__r)
											FROM Contract__c WHERE  Filiale__c =:filiale AND Filiere__c =:filiere AND Status__c IN ('Brouillon','Rejete')]; /*TO DO replace by CreatedDate = THIS_YEAR */
			//  WHERE  Filiale__c =:filiale AND Filiere__c =:filiere ];
			}
		}
		
	
	 System.debug('AP_AugmentationTarifManager.retrieveUserContracts---> End');
		return contracts;
	}
	
	//Method to fetch the current user Contracts and theirs related amendements
	public static List<Amendment__c> getAvenantsById(List<String> listIds){
		List<Amendment__c> Amendments = [SELECT Id, Name,Contrat2__r.RecordLocked__c,Augmentation__c,ThirdPartyProducerCode__c,CompletedAmadement__c,AugmentationRatio__c,MinimumTonnage__c,ThirdPartyProducerName__c,WasteLabel__c,Conditionnement__c, ByPassContract__c, Nature__c,PriceCurrentYear__c,PriceNextYear__c,Contrat2__r.Id,Contrat2__r.Name, Contrat2__r.Status__c,
											Contrat2__r.Type_de_convention__c,Contrat2__r.Filiale__c,IsUnderContract__c,Filiale__c, Filiere__c, Account__c, ContactRelated__c, CustomerNature__c, Commercial_Associe__c, Face_face__c, UnitNextYear__c, Account__r.Name,
											Recurrence__c, LabelArticleCode__c, ArticleCode__c, TechnicInfo__c, IncludedTGAP__c, CEECode__c, OnuCode__c, Label__c, HeaderContractX3__c, FooterContractX3__c,
											Keywords__c, AnalyticSection__c, ActifAcceptationContrat__c, TreatmentRange__c, PackagingGroup__c, Class__c, TT_X3__c, TextSalesX3__c, CommentaryAmendementsX3__c,
											QuantityPreviousYear__c, QuantityCurrentYear__c, QuantityNextYear__c, PricePreviousYear__c, Planned_next_year__c, UnitPreviousYear__c, UnitCurrentYear__c, QuantityInvoiced2018__c,
											Chiffre_d_affaires_N_du_Contrat__c, CA_N__c, State__c, Indexation_gasoil__c, Communal_tax__c,TECH_Face_Face__c,TECH_Excluded__c,ExclusionComment__c, SheetNumber__c,
											TreatmentQuantityPreviousYear__c, TreatmentQuantityNextYear__c, TreatmentQuantityCurrentYear__c, GoLitrePrice__c, FuelPart__c, PriceNextyearFromERP__c  FROM Amendment__c WHERE id IN :listIds];
	
		return Amendments;
	}

    // Method to set amendments passed in arguments under contract
	@AuraEnabled
    public static RequestResponse setContractsUnderContract(List<Contract__c> listContract) {

		List<Id> idsList = new List<Id>();
		for(Contract__c a : listContract) {
			idsList.add(a.Id);
		}
		List<Amendment__c> listAmendments = [SELECT isUnderContract__c, PriceNextYear__c, PriceCurrentYear__c  FROM Amendment__c WHERE Contrat2__c IN :idsList];
		for(Amendment__c a : listAmendments) {
			a.isUnderContract__c = true;
			if(a.PriceNextYear__c == null) {
				a.PriceNextYear__c = a.PriceCurrentYear__c;
			}
		}
		

		RequestResponse response = new RequestResponse();
		/*List<Amendment__c> listAmendments = new List<Amendment__c>(); 
		for(Contract__c c : listContract) {
			for(Amendment__c amendement : c.Avenants__r){
				amendement.isUnderContract__c = true;
				listAmendments.add(amendement);
			}
        }*/
		if(listAmendments.size()>0){
			//by pass the contract Trigger
			ByPassUtils.ByPass('ContractTrigger');
			Update listAmendments;
		
			response.message = 'Les contracts sélectionnés ont été modifiés';
			response.toastMode = Label.ToastMode_Success;
			//reset by pass the contract Trigger
			ByPassUtils.UndoByPass('ContractTrigger');
		}

		return response;

    }


	// Method to set amendments passed in arguments under contract

    public static List<Amendment__c> setAmendmentsUnderContract(List<Amendment__c> listAmendments, Boolean enableUnderContract) {
        for(Amendment__c a : listAmendments) {
            if(enableUnderContract) {
                a.isUnderContract__c = true;
                if(a.PriceNextYear__c == null) {
                    a.PriceNextYear__c = a.PriceCurrentYear__c;
                }
            } else {
                a.isUnderContract__c = false;
                a.PriceNextYear__c = null;
            }
        }
        update listAmendments;
        return listAmendments;
    }

	public static Boolean doAmendmentsHaveNextYearPrice(List<Amendment__c> listAmendments) {
		List<Id> idsList = new List<Id>();
		for(Amendment__c a : listAmendments) {
			idsList.add(a.Id);
		}
		List<Amendment__c> newListAmendments = [SELECT PriceNextYear__c FROM Amendment__c WHERE ID in :idsList];
		for(Amendment__c a : newListAmendments) {
			if(a.PriceNextYear__c != null) {
				return true;
			}
		}
		return false;
	}
	
	
	//Method to update a list of contracts
	public static void updateContract(List<Contract__c> lstCont ){
		//by pass the contract Trigger
		ByPassUtils.ByPass('ContractTrigger');
		update lstCont;
		//reset by pass the contract Trigger
		ByPassUtils.UndoByPass('ContractTrigger');
	}
	
	public static Amendment__c addpercentage(Amendment__c a,Double Value, String RoundingType){
		a.PriceNextYear__c=a.PriceCurrentYear__c+a.PriceCurrentYear__c*Value*0.01;
		/*	if(RoundingType.equals('noRounding')) { 
			a.PriceNextYear__c=a.PriceCurrentYear__c+a.PriceCurrentYear__c*Value*0.01;
		}else */
		 if(RoundingType.equals('supp')) {
			a.PriceNextYear__c=a.PriceNextYear__c.round(System.RoundingMode.CEILING);
		}else if(RoundingType.equals('auto')) {
			a.PriceNextYear__c=a.PriceNextYear__c.round();
		}
		return a;
	}
	
	
	@AuraEnabled
	public static String increaseValues(List<Amendment__c> lstAmend, Double inputValue,Double inputValueTraitement,
										Double inputValueTransport,Double inputValueConditionnement, String RoundingType, String RoundingTypeTrait, String RoundingTypePrest, String RoundingTypeCondi, String comment){
		System.debug('LC_ContractsList_AugTarifCtrl.increaseValues --> Start');
		// save the changes (if there is any) and adding the % value to the next year price in the amendments linked to the contracts selected
		List<String> lstNatureOptions = AP_AugmentationTarifUtils.getselectOptions(new Amendment__c(), 'Nature__c');
		List<Amendment__c> AmendmentToUpdate= new  List<Amendment__c>();
		Map<Id,Contract__c> ContratToUpdate= new  Map<Id,Contract__c>();
		
		//add comment on all contracts
		Boolean addComment = String.isNotBlank(comment);
        if (addComment){
			for(Amendment__c am: lstAmend) {
				if(!ContratToUpdate.containsKey(am.Contrat2__c)){
					ContratToUpdate.put(am.Contrat2__c,new Contract__c(Id=am.Contrat2__c,IncreaseComment__c=comment));
				}
			}
        }
		//Use the same value to update all record
		if(inputValue!=null && inputValue>0) {
			for(Amendment__c am: lstAmend) {
				if(!am.TECH_Excluded__c && !am.CompletedAmadement__c){
					if(am.PriceCurrentYear__c!=null) {
						am=addpercentage(am,inputValue,RoundingType);
					}
					am.ByPassContract__c=false;
					AmendmentToUpdate.add(am);
				}
			}
		}else{
			if((inputValueTraitement!=null && inputValueTraitement>0)
			   || (inputValueTransport!=null && inputValueTransport>0)
			   || (inputValueConditionnement!=null && inputValueConditionnement>0)) {
				for(Amendment__c am: lstAmend) {
					if(!am.TECH_Excluded__c && !am.CompletedAmadement__c){
						if(am.Nature__c=='Traitement' && (inputValueTraitement!=null && inputValueTraitement>0)) {
							am=addpercentage(am,inputValueTraitement,RoundingTypeTrait);
							am.ByPassContract__c=false;
						}
						if(am.Nature__c=='Prestation' && (inputValueTransport!=null && inputValueTransport>0)) {
							am=addpercentage(am,inputValueTransport,RoundingTypePrest);
							am.ByPassContract__c=false;
						}
						if(am.Nature__c=='Conditionnement' && (inputValueConditionnement!=null && inputValueConditionnement>0)) {
							am=addpercentage(am,inputValueConditionnement,RoundingTypeCondi);
							am.ByPassContract__c=false;
						}
						AmendmentToUpdate.add(am);
					}
				}
			}else{
				System.debug('LC_ContractsList_AugTarifCtrl.increaseValues --> End ko');
				return 'ko';
			}
		}
		If(AmendmentToUpdate.size()>0){
			//by pass the contract Trigger
			ByPassUtils.ByPass('ContractTrigger');
			update AmendmentToUpdate;		
			System.debug('LC_ContractsList_AugTarifCtrl.increaseValues --> End success');
			if(ContratToUpdate.size()>0){
				update ContratToUpdate.values();
			}
	
			//reset by pass the contract Trigger
			ByPassUtils.UndoByPass('ContractTrigger');
			return 'ok';
		}
		System.debug('LC_ContractsList_AugTarifCtrl.increaseValues --> End ko');
		return 'ko';
	}
	
	//Update a list of contract's typeDeConvention
	public static void updateContractsA(String contractsId, String typeDeConvention){
        List<Id> ids = new List<Id>();
        if(Test.isRunningTest()){
           ids.add(contractsId);
        }else{
            ids = (List<Id>)JSON.deserialize(contractsId, List<Id>.class);
        }
		List<Contract__c> contractsToUpdate = [select Id, Type_de_convention__c from Contract__c where Id in :ids];
		if(contractsToUpdate.size() >0) {
			for(Contract__c c: contractsToUpdate) {
				c.Type_de_convention__c = typeDeConvention;
			}
			//by pass the contract Trigger
			ByPassUtils.ByPass('ContractTrigger');
			Update contractsToUpdate;
			//reset by pass the contract Trigger
			ByPassUtils.UndoByPass('ContractTrigger');
		}
	
	}
	
	//Update a list of contract's typeDeConvention
	@AuraEnabled
	public static void updateContractsConv(List<Contract__c> lstCont, String typeDeConvention){
	
		for(Contract__c c: lstCont) {
			c.Type_de_convention__c = typeDeConvention;
		}
		//by pass the contract Trigger
		ByPassUtils.ByPass('ContractTrigger');
		Update lstCont;
		//reset by pass the contract Trigger
		ByPassUtils.UndoByPass('ContractTrigger');
	
	
	}
	
	
	public static Map<String,List<String> >  getTypeValues(String ctrField, String depField) {
		Map<String,List<String> > Lis = AP_AugmentationTarifUtils.getDependentMap(new Contract__c(),ctrField, depField);
		return Lis;
	}
	
	//Submit Amendments for validation
	@AuraEnabled
	public static RequestResponse submitAMEForValidation(list<Contract__c> contractsList, String filiale, String filiere){
		System.debug('submitAMEForValidation --> start params filiale & filiere '+filiale + ' '+filiere);
		RequestResponse response = new RequestResponse();
			  if(AP_AugmentationTarifUtils.submit(contractsList,filiale,filiere)) {
					response.message = Label.ContractsSubmittedForValidation;
					response.toastMode = Label.ToastMode_Success;
				}
				else{
					response.message = Label.ErrorContactAdmin;
					response.toastMode = Label.ToastMode_Error;
			 }
	
		/*if(contractsList.size() < 250 ){
				if(AP_AugmentationTarifUtils.submit(contractsList,filiale,filiere)) {
					response.message = Label.ContractsSubmittedForValidation;
					response.toastMode = Label.ToastMode_Success;
				}
				else{
					response.message = Label.ErrorContactAdmin;
					response.toastMode = Label.ToastMode_Error;
				}
			}else{//asynchronous sending
				LC_SendValidationQueue_AugTarif sendValidationJob = new LC_SendValidationQueue_AugTarif(contractsList, filiale, filiere);
				// enqueue the job for processing
				ID jobID = System.enqueueJob(sendValidationJob);
					response.message = 'Demande prise en compte. Vos contrats sont en cours d\'envoi pour validation via le job :'+jobID;
					response.toastMode = Label.ToastMode_Success;	
			}*/
			
	
		return response;
	}
	
	@AuraEnabled
	public static RequestResponse increaseContractFaf(list<Contract__c> contractList, String reason,List<Amendment__c> avenantList){
		RequestResponse response = new RequestResponse();
		List<Contract__c> contractToUpdate=new List<Contract__c>();
		List<Amendment__c> amToUpdate=new List<Amendment__c>();
		if(contractList!=null && contractList.size()>0){
			for(Contract__c ct: contractList) {
				ct.Face_Face__c = true;
				ct.IncreaseComment__c =reason;
				contractToUpdate.add(ct);
			}
		}
		if(avenantList.size()>0){
			for(Amendment__c am: avenantList) {
				if(!am.TECH_Excluded__c){
					am.TECH_Face_Face__c =true;
					am.PriceNextYear__c=null;
					am.TECH_Excluded__c =false;
					amToUpdate.add(am);
				}
			}
		}
		if(contractToUpdate.size()>0){
			//by pass the contract Trigger
			ByPassUtils.ByPass('ContractTrigger');
			Update contractToUpdate;
			if(amToUpdate.size()>0){
				Update amToUpdate;
			}
			response.message = 'Les contracts sélectionnés ont été modifiés';
			response.toastMode = Label.ToastMode_Success;
			//reset by pass the contract Trigger
			ByPassUtils.UndoByPass('ContractTrigger');
		}
		return response;
	}
	
	@AuraEnabled
	public static RequestResponse increaseAMEFixedPrice(list<Amendment__c> amendmentList, Double inputValue,Double inputValueTraitement,
														Double inputValueTransport,Double inputValueConditionnement,String comment){
		RequestResponse response = new RequestResponse();
		Map<Id,Contract__c> ContratToUpdate= new  Map<Id,Contract__c>();
		
		Boolean addComment = String.isNotBlank(comment);
		for(Amendment__c am: amendmentList) {
			if(addComment && !ContratToUpdate.containsKey(am.Contrat2__c)){
				ContratToUpdate.put(am.Contrat2__c,new Contract__c(Id=am.Contrat2__c,IncreaseComment__c=comment));
			}
		}
	
		List<Amendment__c> AmendmentToUpdate=new List<Amendment__c>();
		// Add the value of price current year to the price next year
		if(inputValue!=null && inputValue>0) {
			for(Amendment__c am: amendmentList) {
				if(!am.TECH_Excluded__c && !am.CompletedAmadement__c){
					am.PriceNextYear__c= inputValue;
					am.ByPassContract__c=false;
					AmendmentToUpdate.add(am);
				}
			}
		}else{
			if((inputValueTraitement!=null && inputValueTraitement>0)
			   || (inputValueTransport!=null && inputValueTransport>0)
			   || (inputValueConditionnement!=null && inputValueConditionnement>0) ) {
				for(Amendment__c am: amendmentList) {
					if(!am.TECH_Excluded__c && !am.CompletedAmadement__c){
						if(am.Nature__c=='Traitement' && (inputValueTraitement!=null && inputValueTraitement>0)) {
							am.PriceNextYear__c= inputValueTraitement;
							am.ByPassContract__c=false;
						}
						if(am.Nature__c=='Prestation' && (inputValueTransport!=null && inputValueTransport>0)) {
							am.PriceNextYear__c= inputValueTransport;
							am.ByPassContract__c=false;
						}
						if(am.Nature__c=='Conditionnement' && (inputValueConditionnement!=null && inputValueConditionnement>0)) {
							am.PriceNextYear__c= inputValueConditionnement;
							am.ByPassContract__c=false;
		
						}
						AmendmentToUpdate.add(am);
					}
					
				}
			}else{
				response.message = 'Veuillez saisir une valeur positive';
				response.toastMode = Label.ToastMode_Warning;
			}
		}
	
		If(AmendmentToUpdate.size()>0){
			//by pass the contract Trigger
			ByPassUtils.ByPass('ContractTrigger');
			Update AmendmentToUpdate;
			if(ContratToUpdate.size()>0){
				update ContratToUpdate.values();
			}
			response.message = 'Les avenants sélectionnés ont été modifiés';
			response.toastMode = Label.ToastMode_Success;
			//reset by pass the contract Trigger
			ByPassUtils.UndoByPass('ContractTrigger');
		}
	
		return response;
	}

	
	@AuraEnabled
	public static RequestResponse increaseAMEByValue(list<Amendment__c> amendmentList, Double inputValue,Double inputValueTraitement,
													 Double inputValueTransport,Double inputValueConditionnement,String comment){
		RequestResponse response = new RequestResponse();
		Map<Id,Contract__c> ContratToUpdate= new  Map<Id,Contract__c>();
		
		Boolean addComment = String.isNotBlank(comment);
		for(Amendment__c am: amendmentList) {
			if(addComment && !ContratToUpdate.containsKey(am.Contrat2__c)){
				ContratToUpdate.put(am.Contrat2__c,new Contract__c(Id=am.Contrat2__c,IncreaseComment__c=comment));
			}
		}
	
		List<Amendment__c> AmendmentToUpdate=new List<Amendment__c>();
		// Add the value of price current year to the price next year
		if(inputValue!=null && inputValue>0) {
			for(Amendment__c am: amendmentList) {
				if(!am.TECH_Excluded__c && !am.CompletedAmadement__c){
					am.PriceNextYear__c=am.PriceCurrentYear__c+inputValue;
					am.ByPassContract__c=false;
					AmendmentToUpdate.add(am);
				}
			}
		}else{
			if((inputValueTraitement!=null && inputValueTraitement>0)
			   || (inputValueTransport!=null && inputValueTransport>0)
			   || (inputValueConditionnement!=null && inputValueConditionnement>0) ) {
				for(Amendment__c am: amendmentList) {
					if(!am.TECH_Excluded__c && !am.CompletedAmadement__c){
						if(am.Nature__c=='Traitement' && (inputValueTraitement!=null && inputValueTraitement>0)) {
							am.PriceNextYear__c=am.PriceCurrentYear__c+inputValueTraitement;
							am.ByPassContract__c=false;
						}
						if(am.Nature__c=='Prestation' && (inputValueTransport!=null && inputValueTransport>0)) {
							am.PriceNextYear__c=am.PriceCurrentYear__c+inputValueTransport;
							am.ByPassContract__c=false;
						}
						if(am.Nature__c=='Conditionnement' && (inputValueConditionnement!=null && inputValueConditionnement>0)) {
							am.PriceNextYear__c=am.PriceCurrentYear__c+inputValueConditionnement;
							am.ByPassContract__c=false;
	
						}
						AmendmentToUpdate.add(am);
					}
				}
			}else{
				response.message = 'Veuillez saisir une valeur positive';
				response.toastMode = Label.ToastMode_Warning;
			}
		}
	
		If(AmendmentToUpdate.size()>0){
			//by pass the contract Trigger
			ByPassUtils.ByPass('ContractTrigger');
			Update AmendmentToUpdate;
			if(ContratToUpdate.size()>0){
				update ContratToUpdate.values();
			}
			response.message = 'Les avenants sélectionnés ont été modifiés';
			response.toastMode = Label.ToastMode_Success;
			//reset by pass the contract Trigger
			ByPassUtils.UndoByPass('ContractTrigger');
		}
		return response;
	}
	
	@AuraEnabled
	public static RequestResponse dontIncreaseAME(list<Amendment__c> amendmentList,String comment){
		RequestResponse response = new RequestResponse();
		List<Amendment__c> AmendmentToUpdate=new List<Amendment__c>();
		Map<Id,Contract__c> ContratToUpdate= new  Map<Id,Contract__c>();
		Boolean addComment = String.isNotBlank(comment);
	
		If(amendmentList!=null && amendmentList.size()>0){
			for(Amendment__c am: amendmentList) {
				if(!am.TECH_Excluded__c && !am.CompletedAmadement__c){
					if(am.PriceCurrentYear__c!=null) {
						am.PriceNextYear__c=am.PriceCurrentYear__c;
						//ligne commentée en 2019, car le fait de "Ne pas augmenter" ne doit pas passer l'avenant en Face à Face :
						//am.ByPassContract__c=true;
						am.ByPassContract__c=false;
						AmendmentToUpdate.add(am);
					}
					if(addComment && !ContratToUpdate.containsKey(am.Contrat2__c)){
						ContratToUpdate.put(am.Contrat2__c,new Contract__c(Id=am.Contrat2__c,IncreaseComment__c=comment));
					}
				}
			}
		}
		If(AmendmentToUpdate.size()>0){
			//by pass the contract Trigger
			ByPassUtils.ByPass('ContractTrigger');
			Update AmendmentToUpdate;
			if(ContratToUpdate.size()>0){
				update ContratToUpdate.values();
			}
			response.message = 'Les avenants sélectionnés ont été modifiés';
			response.toastMode = Label.ToastMode_Success;
			//reset by pass the contract Trigger
			ByPassUtils.UndoByPass('ContractTrigger');
		}
		return response;
	}
	
	@AuraEnabled
	public static RequestResponse resetContractIncrease(list<Contract__c> contractList){
		RequestResponse response = new RequestResponse();
		Boolean differentAccount = false;
		List<Contract__c> contractListToUpdate  = new List<Contract__c>();
		
		if(contractList != null && contractList.size()>0){
			for(Contract__c c : contractList){
				Contract__c cont = new Contract__c();
				cont.Id = c.Id;
				cont.Face_Face__c = false;
				cont.IncreaseComment__c ='';
				cont.RegroupmentIndex__c = null;
				contractListToUpdate.add(cont);			
			}
		}
		try{
			//by pass the contract Trigger
			ByPassUtils.ByPass('ContractTrigger');	
			update contractListToUpdate;
			response.message = 'La mise à jour des contrats a bien été effectuée';
			response.toastMode = Label.ToastMode_Success;
			//reset by pass the contract Trigger
			ByPassUtils.UndoByPass('ContractTrigger');
		}
		catch(Exception ex){
			response.message = 'resetIncrease --> Une erreur est survenue pendant la mise à jour des contrats, merci de contacter votre administrateur. Erreur : '+ex;
			response.toastMode = Label.ToastMode_Warning;
			throw new AuraHandledException(String.valueOf(ex.getLineNumber()));
		}
		return response;
	}
	
	@AuraEnabled
	public static RequestResponse resetAmendmentIncrease(list<Amendment__c> amendmentList){
		RequestResponse response = new RequestResponse();
		Boolean differentAccount = false;
		List<Amendment__c> amendmentListToUpdate = new List<Amendment__c>();
		List<Contract__c> contractListToUpdate  = new List<Contract__c>();	
	
		if(amendmentList != null && amendmentList.size()>0){
			for(Amendment__c am : amendmentList){
				//we use new object to reduce fields
				Amendment__c upAm = new Amendment__c();
				upAm.Id = am.Id;
				upAm.PriceNextYear__c=null;
				upAm.ByPassContract__c=false;
				upAm.TECH_Face_Face__c =false;
				upAm.TECH_Excluded__c = false;
				upAm.ExclusionComment__c = '';
				upAm.IsUnderContract__c = false;
				amendmentListToUpdate.add(upAm);
			}
		}
		
		try{
			//by pass the contract Trigger
			ByPassUtils.ByPass('ContractTrigger');
			update amendmentListToUpdate;	
			response.message = 'La mise à jour des avenants a bien été effectuée';
			response.toastMode = Label.ToastMode_Success;
			//reset by pass the contract Trigger
			ByPassUtils.UndoByPass('ContractTrigger');
		}
		catch(Exception ex){
			response.message = 'resetAmendmentIncrease --> Une erreur est survenue pendant la mise à jour des avenants, merci de contacter votre administrateur. Erreur : '+ex;
			response.toastMode = Label.ToastMode_Warning;
			throw new AuraHandledException(ex.getMessage());
		}
		return response;
	}
    
    @AuraEnabled
    public static RequestResponse excluded_am(list<Amendment__c> amendmentList,String reason){
		RequestResponse response = new RequestResponse();
		Boolean differentAccount = false;
		List<Amendment__c> amendmentListToUpdate = new List<Amendment__c>();
	
		try{
            if(amendmentList != null && amendmentList.size()>0){
                for(Amendment__c am : amendmentList){
                    am.PriceNextYear__c=null;
                    am.ByPassContract__c=false;
                    am.TECH_Face_Face__c =false;
                    am.ExclusionComment__c = reason;
                    am.TECH_Excluded__c = true;
                    amendmentListToUpdate.add(am);
                }
            }
            
			//by pass the contract Trigger
			ByPassUtils.ByPass('ContractTrigger');
			update amendmentListToUpdate;
			response.message = 'La mise à jour a bien été effectué';
			response.toastMode = Label.ToastMode_Success;
	
			//reset by pass the contract Trigger
			ByPassUtils.UndoByPass('ContractTrigger');
		}
		catch(Exception ex){
			response.message = 'Une erreur est survenu, merci de contacter votre administrateur';
			response.toastMode = Label.ToastMode_Warning;
		}
		return response;
	}
	
	@AuraEnabled
	public static RequestResponse excluded_c(list<Contract__c> contractList,String reason){
		RequestResponse response = new RequestResponse();
		Boolean differentAccount = false;
		List<Contract__c> contractListToUpdate  = new List<Contract__c>();
	
		try{

            if(contractList != null && contractList.size()>0){
                for(Contract__c c : contractList){
                    c.Face_Face__c = false;
                    c.IncreaseComment__c ='';
                    contractListToUpdate.add(c);
                }
            }
			//by pass the contract Trigger
			ByPassUtils.ByPass('ContractTrigger');
			update contractListToUpdate;
			response.message = 'La mise à jour a bien été effectuée';
			response.toastMode = Label.ToastMode_Success;
	
			//reset by pass the contract Trigger
			ByPassUtils.UndoByPass('ContractTrigger');
		}
		catch(Exception ex){
			response.message = 'Une erreur est survenu, merci de contacter votre administrateur';
			response.toastMode = Label.ToastMode_Warning;
		}
		return response;
	}
	
	@AuraEnabled
	public static RequestResponse regroupContracts(list<Contract__c> contractList){
		RequestResponse response = new RequestResponse();
		Boolean faceAFace = false;
		Boolean augmentationTarf = false;
		Boolean differentAccount = false;
		Boolean differentContact=false;
		Boolean differentConventionType=false;
	
		for(Contract__c c: contractList) {
			if(c.Account__c!= contractList[0].Account__c) {
				differentAccount=true;
			}
			if(c.Contrat_face_face__c) {
				faceAFace = true;
			}
			if(c.IncreaseRatioNumber__c>=0 || c.IncreaseRatioNumber__c != null ) {
				augmentationTarf = true;
			}
			if(c.ContactRelated__c!=contractList[0].ContactRelated__c){
				differentContact = true;
			}
			if(c.Type_de_convention__c!=contractList[0].Type_de_convention__c){
				differentConventionType = true;
			}
		}
	
		if(differentAccount) {
			response.message = 'Les contrats liés aux avenants sélectionnés proviennent de deux comptes différents et ne peuvent pas être fusionnés';
			response.toastMode = Label.ToastMode_Warning;
		}else if(differentContact){
			response.message = 'Les contrats sélectionnés n\'ont pas le même Contact associé et ne peuvent donc pas être fusionnés';
			response.toastMode = Label.ToastMode_Warning;
		}else if(differentConventionType){
			response.message = 'Les contrats sélectionnés n\'ont pas le même Type de convention et ne peuvent donc pas être fusionnés';
			response.toastMode = Label.ToastMode_Warning;
		}else if(faceAFace && augmentationTarf) {
			response.message ='Les contrats en Face à Face ne peuvent pas être fusionnés avec ceux en augmentation tarifaire.';
			response.toastMode = Label.ToastMode_Warning;
		}else{
			Decimal RegroupmentValue = WithoutSharingUtils.getIndexMax();
			if(RegroupmentValue==null) {
				RegroupmentValue=0;
			}
			Set<Contract__c> ContractsToUpdate=new Set<Contract__c>();
			If(contractList!=null && contractList.size()>0){
				for(Contract__c c: contractList) {
					c.RegroupmentIndex__c=RegroupmentValue+1;
					ContractsToUpdate.add(c);
				}
			}
	
			If(ContractsToUpdate.size()>0){
				List<Contract__c> ContractsListUpdate= new List<Contract__c>(ContractsToUpdate);
				//by pass the contract Trigger
				ByPassUtils.ByPass('ContractTrigger');
				Update ContractsListUpdate;
				//reset by pass the contract Trigger
				ByPassUtils.UndoByPass('ContractTrigger');
				response.message = 'Les contrats sélectionnés ont été groupés';
				response.toastMode = Label.ToastMode_Success;
	
			}else{
				response.message = 'Le contrat sélectionné est soumis ou validé et ne peut pas être groupé';
				response.toastMode = Label.ToastMode_Warning;
			}
		}
		return response;
	}
	
	public class RequestResponse {
	@AuraEnabled
	public string message {get; set;}
	@AuraEnabled
	public string toastMode {get; set;}
	}

	public static Boolean AreUserStandardContractsSubmitted(String userId, String filiale, String filiere){

		System.debug('AP_AugmentationTarifManager.retrieveUserStandardSubmittedContracts---> Start :parms filiale ='+filiale+' filière ='+filiere);
		
		List<Contract__c> contracts = new List<Contract__c> ();
		
		contracts = [SELECT Id,Name,Status__c,Filiale__c, Filiere__c, Contrat_face_face__c, Face_Face__c,TECH_Excluded__c,IsUnderContract__c
		FROM Contract__c WHERE AssociatedSalesRep__c=:userId AND Filiale__c =:filiale AND Filiere__c =:filiere AND Status__c = 'Soumis' 
		AND Face_Face__c = false AND TECH_Excluded__c = false AND IsUnderContract__c = false];

		System.debug('AreUserStandardContractsSubmitted contracts : ' + contracts);

		if (contracts != null && contracts.size() > 0) return true;
		return false;

	}

}