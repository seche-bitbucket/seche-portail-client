@IsTest
public class FIPTriggerTest {

    
    @IsTest
    public static void testFIPTrigger(){
        
        Test.startTest();
        
        Account a = new Account();
        a.Name = 'TESTFIPAMIANTE';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingStreet = '12 rue du test';
        a.BillingPostalCode = '39000';
        a.BillingCity = 'Lyon';
        a.NAF_Number__c ='1234A';
        a.ChargedX3Code__c = 'FR972139821';
        a.X3ChargedAddressCode__c = 'GEN';
        a.FavoriteSubsidiaryAsbestos__c = 'Tredi Salaise 1';
        a.SIRET_Number__c = '09218301381222';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName1';
        c.AccountId = a.Id;
        c.Email = 'aaaa@groupe-seche.com';
        c.Phone = '03 00 00 00 00';
        insert c;
        
        FIP_FIP__c fip = new FIP_FIP__c();
        system.debug('Collector__c = '+ a.ID);
        fip.Collector__c = a.ID;
        system.debug('Contact__c = '+ c.ID);
        fip.Contact__c = c.ID;
        insert fip;
        
        fip = [Select ID, ChargedBusinessName__c, ProductorBusinessName__c FROM FIP_FIP__c WHERE ID = : fip.ID];
        
        System.assertEquals(a.Name, fip.ChargedBusinessName__c);
        System.assertEquals(a.Name, fip.ProductorBusinessName__c);
        
        Test.stopTest();
        
    }
}