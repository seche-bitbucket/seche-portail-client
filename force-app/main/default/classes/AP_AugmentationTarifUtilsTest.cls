@isTest
public class AP_AugmentationTarifUtilsTest {
    
    @isTest
    public static void testGetDependentMap(){
        
        Map<String, List<String>> dependentMap = AP_AugmentationTarifUtils.getDependentMap(new Contract__c(), 'Filiale__c','Type_de_convention__c');
        List<String> filiale = dependentMap.get(String_Helper.CON_Filiale_Drimm);
      //  System.assertEquals(filiale.contains(String_Helper.CON_TypeDeConvention_DndTraitement), true);
        
    }
    
    @isTest
    public static void testDecimalToBinary(){
        
        String bits = AP_AugmentationTarifUtils.decimalToBinary(10);       
        System.assertEquals(bits, '1010');        
        
        
    }
    
    @isTest
    public static void testbase64ToBits(){
        
        String bits = AP_AugmentationTarifUtils.base64ToBits('test');       
        System.assertEquals(bits, '101101011110101100101101');                
        
    }
    
    @IsTest(SeeAllData=true)
    public static void testSubmit(){
        AP_AugmentationTarifUtils ctrl = new AP_AugmentationTarifUtils();
        List<Contract__c> listC = new  List<Contract__c>();
                
        Contract__c c1 = new Contract__c();   
        c1.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
        c1.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport;   
        c1.Status__c = String_Helper.CON_Status_Brouillon;
        c1.AssociatedSalesRep__c =  UserInfo.getUserId();
        
        Contract__c c2 = new Contract__c();   
        c2.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c2.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport;
        c2.Status__c = String_Helper.CON_Status_Brouillon;
        c2.AssociatedSalesRep__c =  UserInfo.getUserId();

        Contract__c c3 = new Contract__c();   
        c3.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c3.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c3.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 
        c3.Status__c = String_Helper.CON_Status_Brouillon;
        c3.AssociatedSalesRep__c =  UserInfo.getUserId();

       	listC.add(c1); listC.add(c2);  listC.add(c3);
       	insert listC;			

        String filiale = 'Drimm';
        String filiere = 'Stockage DND'; 
        
        Boolean response = AP_AugmentationTarifUtils.Submit(listC,filiale,filiere);       
        listC = [Select Id , Status__c from Contract__c];
               
        
        
    }
    @isTest(SeeAllData=true) 
    public  static void testGetReportData(){
        List<String> result = new List<String>();        
        result.add('Amiante_Stockage_DD');        
        result = AP_AugmentationTarifUtils.getReportData(result , String_Helper.CON_Filiale_Drimm, String_Helper.CON_Filiere_CentreDeTriDAE); 
        
        system.assertEquals(String.isEmpty(result[0]),false);
        
    }

    @isTest
    static void testgetReportDataValidation(){
       
        List<String> lstReportName = new List<String>{'Approb_01_Tout_Hors_FaF_9Bz', 'Approb_01_Tout_Hors_FaF_9B', 'Approb_01_Tout_Hors_FaF_9Bz'};


        User associatedSalesRep = [SELECT Id FROM User WHERE profileId = :[select id from profile where name = :'Responsable commercial' Limit 1].id Limit 1] ;
       
        List <String> datas = AP_AugmentationTarifUtils.getReportDataValidation(lstReportName,  associatedSalesRep.Id,  String_Helper.CON_Filiale_Drimm,  String_Helper.CON_Filiere_CentreDeTriDAE, false, false);
        System.assertEquals(false, datas == null);
    }

    @isTest
    static void testgetselectOption(){
        Contract__c cont = new Contract__c();
        List < String > filiales = AP_AugmentationTarifUtils.getselectOptions(cont, 'Filiale__c');
        System.assertEquals(true, filiales.size() > 0);
    }
    
    
	
}