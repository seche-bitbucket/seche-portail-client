@isTest
global class WS005_ConnectiveMock implements  HTTPCalloutMock{
    global Static String externalId;
	global HTTPResponse respond(HTTPRequest req){
        HttpResponse res = new HTTPResponse();
        res.setHeader('Content-Type', 'application/JSON');
        res.setBody('{"ExternalReference":"'+externalId+'","ErrorLog":[]}');
        req.setMethod('GET');
        res.setStatusCode(200);
        return res;
    }
}