global with sharing class SignatureMassFilesRetrieve implements Database.Batchable<sObject>, Database.AllowsCallouts{
     global Database.QueryLocator start(Database.BatchableContext BC)
    {	// retrieve contracts that don't have an opportunity linked to them
        String query = 'SELECT Id, Status__c, FlowId__c,TECH_ExternalId__c,OwnerId,Quote__r.OwnerId , Quote__r.QuoteCode__c, FIP_FIP__r.Name FROM DocumentsForSignature__c WHERE Quote__r.Name Like \'Convention 2020%\' And (Status__c= \'SIGNED\' OR Status__c=\'\')';
        return Database.getQueryLocator(query);
    }  
    
    global void execute(Database.BatchableContext BC, List<DocumentsForSignature__c> scope)
    {	
        ConnectiveCallout.retrieveMassFlowStatus(scope);
    }
    global void finish(Database.BatchableContext BC)
    {  
        
    }
}