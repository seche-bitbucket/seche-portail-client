global with sharing class CustomLookUpController {
    
    public Opportunity opportunity {get;set;} // new account to create
    public List<Opportunity> results{get;set;} // search results
    public string searchString{get;set;} // search keyword
    public string idOppMere{get;set;} // Opp Mere ID
    
    
    public CustomLookUpController(ApexPages.StandardController controller) {
        // get the current search string
        idOppMere = (string)Apexpages.currentPage().getParameters().get('id');
        searchString = System.currentPageReference().getParameters().get('lksrch');
        runSearch(); 
    }
    
    public CustomLookUpController(ApexPages.StandardSetController controller) {
        // get the current search string
        idOppMere = (string)Apexpages.currentPage().getParameters().get('id');
        searchString = System.currentPageReference().getParameters().get('lksrch');
        runSearch();   
    }
    
    
    
    // performs the keyword search
    public PageReference search() {
        runSearch();
        return null;
    }
    
    // prepare the query and issue the search command
    private void runSearch() {
        // TODO prepare query string for complex serarches & prevent injections
        results = performSearch(searchString);               
    } 
    
    // run the search and return the records found. 
    private List<Opportunity> performSearch(string searchString) {
        
        
        ID rtIntra = [Select Id From RecordType Where DeveloperName = 'OPP_OpportuniteIntraGroupe'].Id;
        
        Set<String> oppID = new Set<String>{idOppMere};
        Set<ID> rt = new Set<ID>{rtIntra};
            
        String soql = 'select id, name from Opportunity Where Opportunit_mere__c = null AND ID not in:oppID AND RecordTypeID not in:rt ';
        if(searchString != '' && searchString != null)
            soql = soql +  ' AND name LIKE \'%' + searchString +'%\'';
        soql = soql + ' limit 25';
        return database.query(soql); 
        
    }
    
    
    // used by the visualforce page to send the link to the right dom element
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
    
    // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }

    public PageReference redirectOppMere() {
        
       
        PageReference redirect = new PageReference('/'+idOppMere);
        redirect.setRedirect(true);
        return redirect;
    }
    
}