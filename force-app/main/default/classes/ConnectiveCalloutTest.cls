@isTest
public class ConnectiveCalloutTest {    
    static Quote q;
    static Attachment Att;
    static Attachment Att2;
    static FIP_FIP__c fip;
    static DocumentsForSignature__c doc;
    static DocumentsForSignature__c doc2;
    
       public static void initAttachement(){
        Account a;
        Contact c;
        Opportunity opp;
        
        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        q = new Quote();
        //q.id='0Q09E0000000AhESAU';
        q.OpportunityId = opp.ID;
        q.Name = opp.Name;
        q.DateDevis__c = Date.today();
        q.QuoteCode__c=opp.Name;
        q.ContactId = opp.ContactName__c;
        q.Email = opp.ContactName__r.Email;
        q.Phone = opp.ContactName__r.Phone;
        q.Fax = opp.ContactName__r.Fax;
        q.BillingStreet = opp.Account.BillingStreet;
        q.BillingCity = opp.Account.BillingCity;
        q.BillingState = opp.Account.BillingState;
        q.BillingPostalCode = opp.Account.BillingPostalCode;
        q.BillingCountry = opp.Account.BillingCountry; 
        insert q;
        
        att = new Attachment();
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        att.body=bodyBlob;
        att.Name = q.Name;	
        att.ParentId = q.Id;             
        insert att;    
        fip = new FIP_FIP__c();
        fip.Collector__c = a.ID;
        fip.Contact__c = c.ID;
        insert fip;       
  
    }

  public static void initFiles(){
        Account a;
        Contact c;
        Opportunity opp;
        
        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        q = new Quote();
        //q.id='0Q09E0000000AhESAU';
        q.OpportunityId = opp.ID;
        q.Name = opp.Name;
        q.DateDevis__c = Date.today();
        q.QuoteCode__c=opp.Name;
        q.ContactId = opp.ContactName__c;
        q.Email = opp.ContactName__r.Email;
        q.Phone = opp.ContactName__r.Phone;
        q.Fax = opp.ContactName__r.Fax;
        q.BillingStreet = opp.Account.BillingStreet;
        q.BillingCity = opp.Account.BillingCity;
        q.BillingState = opp.Account.BillingState;
        q.BillingPostalCode = opp.Account.BillingPostalCode;
        q.BillingCountry = opp.Account.BillingCountry; 
        insert q;        
       
        fip = new FIP_FIP__c();
        fip.Collector__c = a.ID;
        fip.Contact__c = c.ID;
        insert fip;       
        
        //Insert ContentVersion
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        ContentVersion cVersion = new ContentVersion();
        cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
        cVersion.PathOnClient = q.Name;//File name with extention
        cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.      
        cVersion.Title = q.Name;//Name of the file
        cVersion.VersionData = bodyBlob;//File content
        Insert cVersion;
        
        //After saved the Content Verison, get the ContentDocumentId
        Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
        
        //Insert ContentDocumentLink
        ContentDocumentLink cDocLink = new ContentDocumentLink();
        cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
        cDocLink.LinkedEntityId = q.Id;//Add attachment parentId
        cDocLink.ShareType = 'I';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
        cDocLink.Visibility = 'InternalUsers';//AllUsers, InternalUsers, SharedUsers
        Insert cDocLink;
    }
    
    static testMethod void testSendCreatedFlow(){
        initAttachement();
        string pbefore = 'Testing base 64 encode';            
        Blob beforeblob = Blob.valueOf(pbefore);
        
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=fip.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WS001_ConnectiveMock()); 
        WS001_ConnectiveMock.externalId=fip.id;
        ConnectiveCallout.sendCreatedFlow(fip.id);
        Test.stopTest(); 
    }  


    
    /*static testMethod void testSendCreatedFlowWS(){
        initAttachement();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WS001_ConnectiveMock()); 
        WS001_ConnectiveMock.externalId=q.id;
        ConnectiveCallout.sendCreatedFlowWS(q.id);
        Test.stopTest();
        
    }*/
        static testMethod void testSendCreatedFlowUnknownSigner(){
        initAttachement();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WS001_ConnectiveMock()); 
        WS001_ConnectiveMock.externalId=q.id;
        ConnectiveCallout.sendCreatedFlowWSUnknownSigner(q.id,'First Name', 'Lastname', '09912818121' , 'email@email.com');
        Test.stopTest();
        
    }

    
        static testMethod void testLexSendCreatedFlowUnknownSigner(){
        initFiles();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WS001_ConnectiveMock()); 
        WS001_ConnectiveMock.externalId=q.id;
        ConnectiveCallout.sendCreatedFlowWSUnknownSigner(q.id,'First Name', 'Lastname', '09912818121' , 'email@email.com');
        Test.stopTest();
        
    }
    static testMethod void testretrieveFlowStatus(){
        initAttachement();
        doc=new DocumentsForSignature__c();
        doc.Name=q.Name+'.pdf';
        doc.FlowId__c='a02c9f7a-cfd0-46d2-8ba3-465a5bd79fce';
        doc.Quote__c=q.id;
        doc.TECH_ExternalId__c=q.id;
        insert doc;
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new WS002_ConnectiveMock());
        WS002_ConnectiveMock.externalId=q.id;
        WS002_ConnectiveMock.status='REVOKED';
        ConnectiveCallout.retrieveFlowStatus(q.id);        
        Test.stopTest();
    }  

     static testMethod void testretrieveFlowStatusMass(){
        initAttachement();
        List<DocumentsForSignature__c> listDocs = new List<DocumentsForSignature__c>();
        doc=new DocumentsForSignature__c();
        doc.Name=q.Name+'.pdf';
        doc.FlowId__c='a02c9f7a-cfd0-46d2-8ba3-465a5bd79fce';
        doc.Quote__c=q.id;
        doc.TECH_ExternalId__c=q.id;
        insert doc;

        DocumentsForSignature__c docu =new DocumentsForSignature__c();
        docu.Name=fip.Name+'.pdf';
        docu.FlowId__c='a02c9f7a-cfd0-46d2-8ba3-465a5bd79fc';
        docu.FIP_FIP__c=fip.id;
        docu.TECH_ExternalId__c=fip.id;
        insert docu;

        listDocs.add(doc);
        listDocs.add(docu);
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new WS002_ConnectiveMock());
        //WS002_ConnectiveMock.externalId=q.id;
        WS002_ConnectiveMock.status='REVOKED';
        ConnectiveCallout.retrieveMassFlowStatus(listDocs);        
        Test.stopTest();
    }  



    
    static testMethod void testretrieveStatus(){
        initAttachement();
        doc=new DocumentsForSignature__c();
        doc.Name=q.Name+'.pdf';
        doc.FlowId__c='a02c9f7a-cfd0-46d2-8ba3-465a5bd79fce';
        doc.Quote__c=q.id;
        doc.TECH_ExternalId__c=q.id;
        insert doc;
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new WS002_ConnectiveMock());
        WS002_ConnectiveMock.externalId=q.id;
        WS002_ConnectiveMock.status='REVOKED';        
        ConnectiveCallout.retrieveStatus(q.id); 
        Test.stopTest();
    }  

      static testMethod void testretrieveSignedStatus(){
        initAttachement();
        doc=new DocumentsForSignature__c();
        doc.Name=q.Name+'-  Signée.pdf';
        doc.FlowId__c='a02c9f7a-cfd0-46d2-8ba3-465a5bd79fce';
        doc.Quote__c=q.id;
        doc.TECH_ExternalId__c=q.id;
        insert doc;
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new WS002_ConnectiveMock());
        WS002_ConnectiveMock.externalId=q.id;
        WS002_ConnectiveMock.status='SIGNED';        
        ConnectiveCallout.retrieveStatus(q.id); 
        Test.stopTest();
    }  
    
    static testMethod void testdeleteFlow(){
        initAttachement();
        doc=new DocumentsForSignature__c();
        doc.Name=q.Name+'.pdf';
        doc.FlowId__c='a02c9f7a-cfd0-46d2-8ba3-465a5bd79fce';
        doc.Quote__c=q.id;
        doc.TECH_ExternalId__c=q.id;
        insert doc;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WS004_ConnectiveMock());
        WS004_ConnectiveMock.externalId=q.id;
        ConnectiveCallout.deleteFlow(doc.FlowId__c,'REVOKE',FALSE);
        Test.stopTest();
    }
    
    static testMethod void testretrieveAllFlowsRejected(){
        /*initAttachement();
        doc=new DocumentsForSignature__c();
        doc.Name=q.Name+'.pdf';
        doc.FlowId__c='a02c9f7a-cfd0-46d2-8ba3-465a5bd79fce';
        doc.Document__c='https://i.pinimg.com/736x/ba/03/23/ba03237a6d6499f0e2633314826e1526--cutest-animals-baby-animals.jpg';
        doc.Quote__c=q.id;
        doc.TECH_ExternalId__c=q.id;
        insert doc;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WS003_ConnectiveMock());
        WS003_ConnectiveMock.externalId=q.id;
        WS003_ConnectiveMock.status='REJECTED';
        ConnectiveCallout.retrieveAllFlows();
        Test.stopTest();*/
    }
    
    
    static testMethod void testsendReminderFlow(){
        initAttachement();
        doc=new DocumentsForSignature__c();
        doc.Name=q.Name+'.pdf';
        doc.FlowId__c='a02c9f7a-cfd0-46d2-8ba3-465a5bd79fce';
        doc.Quote__c=q.id;
        doc.TECH_ExternalId__c=q.id;
        insert doc;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WS005_ConnectiveMock());
        WS005_ConnectiveMock.externalId=q.id;
        String t=ConnectiveCallout.sendReminderFlow(q.id);
        System.assertEquals('OK',t);
        Test.stopTest();
    }
    
    static testMethod void testsendReminderFlowFIP(){
        initAttachement();
        doc=new DocumentsForSignature__c();
        doc.Name=q.Name+'.pdf';
        doc.FlowId__c='a02c9f7a-cfd0-46d2-8ba3-465a5bd79fce';
        doc.Quote__c=null;
        doc.FIP_FIP__c=fip.id;
        doc.TECH_ExternalId__c=fip.id;
        insert doc;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WS005_ConnectiveMock());
        WS005_ConnectiveMock.externalId=fip.id;
        String t=ConnectiveCallout.sendReminderFlowFIP(fip.id);
        String tt=ConnectiveCallout.sendReminderFIP(fip.id);
        System.assertEquals('OK',t);
        Test.stopTest();
    }
    static testMethod void testFlowstatusResponse(){
        
        FlowStatusResponse fsr =new FlowStatusResponse();
        String body='{"ExternalReference":"FREFEFEFEF","Status":"Signed","DownloadURL":"https://google.com","F2FSigningURL":"https://portaldemo3.connective.eu/signinit?id=a02c9f7a-cfd0-46d2-8ba3-465a5bd79fce","Signers":[{"SignerId":"eef3c466-ce15-487d-9883-3bfb19209ba0","ExternalReference":"0039E00000LBf6yQAD","EmailAddress":"sechecrm69@gmail.com","Uri":"https://portaldemo3.connective.eu/signinit?id=eef3c466-ce15-487d-9883-3bfb19209ba0","Status":"PENDINGSIGNING","RejectReason":null,"SignatureTimestamp":null,"SignatureSigner":null,"RedirectURL":""}],"ExpiryTimestamp":null,"ErrorLog":[]}';
        fsr=FlowStatusResponse.deserializejson(body);
        FlowStatusResponse.Signer s= new FlowStatusResponse.Signer();
        s.SignerId='123';
        s.ExternalReference='123';
        s.EmailAddress='123@123.123';
        s.Url='123';
        s.Status='123';
        s.RejectReason='123';
        s.SignatureTimestamp='123';
        s.SignatureSigner='123';
        fsr.Signers.add(s);
        fsr.DownloadURL='123';
        fsr.ExternalReference='123';
        fsr.Status='SIGNED';
    }

    
    static testMethod void testsendReminderFlowQuote(){
        initAttachement();
        doc=new DocumentsForSignature__c();
        doc.Name=q.Name+'.pdf';
        doc.FlowId__c='a02c9f7a-cfd0-46d2-8ba3-465a5bd79fce';
        doc.Quote__c=q.id;
        doc.FIP_FIP__c=null;
        doc.TECH_ExternalId__c=q.id;
        insert doc;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WS005_ConnectiveMock());
        WS005_ConnectiveMock.externalId=q.id;
        String t=ConnectiveCallout.sendReminder(q);
        System.assertEquals('OK',t);
        Test.stopTest();
    }
}