public with sharing class OpportunityListViewButtonController{

	ApexPages.StandardSetController stdSetController;
	public List <Opportunity> oppSelected = New List <Opportunity>();
	//List <Opportunity> opprt {get;set;}
	//public String alertMessage {get;set;}
	List<Opportunity> oppToUpdIndir = new List<Opportunity>();
	List<Opportunity> oppToUpdDir = new List<Opportunity>();
	List<SObject> opps = new List<SObject>();
	Boolean saveButDisp;
	Boolean retButDisp;
	Boolean isNoSelectedOpp;
	Boolean isNotDisplayTextHelp;
	
	public OpportunityListViewButtonController(ApexPages.StandardSetController controller) {
		System.debug('Start OpportunityListViewButtonController----: ');
		isNoSelectedOpp = false;
		isNotDisplayTextHelp = false;
		stdSetController = controller;
		oppSelected = [select id,Attributaire__c,DateFinEngagement__c,Name,account.name,ContactName__c,Filiale__c,Filiere__c,Amount,TonnageN__c from Opportunity where id IN:stdSetController.getSelected()];
	}
	
	public void setToWon(){
	
		System.debug('List Records : '+oppSelected);
	
		If (oppSelected.Size()>0)
		{
			for(SObject o : oppSelected ) {
				o.put(Schema.Opportunity.StageName, 'Clôturée / Gagnée');
				opps.add(o);
				System.debug('Opps: '+ opps);
	
			}
		}
		else
		{
			isNoSelectedOpp = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,Label.OPP_Select));
		}
	
	}
	/*public void setToLost(){
	
		System.debug('Start List Records ----: '+oppSelected);
	
		if (oppSelected.Size()>0)
		{
			for (Opportunity op : oppSelected)
			{
				if(op.Attributaire__c != null && op.DateFinEngagement__c != null)
				{
					op.StageName = 'Clôturée / Perdue';
					oppToUpdDir.add(op);
					retButDisp = true;
				}
				else
				{
					op.StageName = 'Clôturée / Perdue';
					oppToUpdIndir.add(op);
					saveButDisp = True;
					retButDisp = true;
				}
			}
	
			if (oppToUpdDir.size()>0 && oppToUpdIndir.size() == 0 )
			{
				save();
			}
		}
		else
		{
			retButDisp = true;
			saveButDisp = false;
			isNotDisplayTextHelp = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Veuillez sélectionner une opportunité avant de procéder !'));
		}
	}*/
	
	public void setToLost(){
		setStageName('Clôturée / Perdue');
	}
	
	public void setToCancel(){
		setStageName('Abandonnée');
	}
	
	public void setStageName(String stageName){
		System.debug('Start List Records ----: '+oppSelected);
	
		if (oppSelected.Size()>0)
		{
			for (Opportunity op : oppSelected)
			{
				if(op.Attributaire__c != null && op.DateFinEngagement__c != null)
				{
					op.StageName = stageName;
					oppToUpdDir.add(op);
					retButDisp = true;
				}
				else
				{
					op.StageName = stageName;
					oppToUpdIndir.add(op);
					saveButDisp = True;
					retButDisp = true;
				}
			}
	
			if (oppToUpdDir.size()>0 && oppToUpdIndir.size() == 0 )
			{
				save();
			}
		}
		else
		{
			retButDisp = true;
			saveButDisp = false;
			isNotDisplayTextHelp = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,Label.OPP_Select));
		}
	}
	
	public pageReference save()
	{
		try{
			if (oppToUpdDir.size()>0) {
				update oppToUpdDir;
			}
	
			if (oppToUpdIndir.size()>0) {
				update oppToUpdIndir;
			}
	
			if (opps.size()>0) {
				update opps;
			}
	
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,Label.OPP_Success));
		}
		catch(DmlException eDml) {
			System.debug('Error : '+eDml);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,eDml.getDmlMessage(0)));
			return null; //return null so the page won't go to cancel
		}
		catch(Exception e) {
			System.debug('Error : '+e);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.ErrorContactAdmin + ' ' + e));
			return null; //return null so the page won't go to cancel
		}
		return stdSetController.cancel();
	
	}
	
	public pageReference returnBack()
	{
		return stdSetController.cancel();
	}
	
	
	Public List<Opportunity> getOpprt()
	{
		return oppToUpdIndir;
	
	}
	public Boolean getSaveButDisp()
	{
		return saveButDisp;
	
	}
	public Boolean getRetButDisp()
	{
		return retButDisp;
	}
	
	public Boolean getIsNoSelectedOpp()
	{
		return isNoSelectedOpp;
	}
	
	public Boolean getIsNotDisplayTextHelp()
	{
		return isNotDisplayTextHelp;
	}
	
	/*
	   public void setToLoose(){
	
	   List<SObject> opps = new List<SObject>();
	   System.debug('List Records : '+stdSetController.getSelected());
	
	   for(SObject o : stdSetController.getSelected() ){
	   o.put(Schema.Opportunity.StageName, 'Clôturée / perdue');
	   opps.add(o);
	   System.debug('Opps: '+ opps);
	
	   }
	   try{
	   update opps;
	   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Opportunité(s) mise(nt) à jour avec succès !'));
	   }catch(Exception e){
	   System.debug('Error : '+e);
	   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Une erreur est survenue, contacter votre administrateur.'+ e));
	
	   }
	
	   }*/
	
	
	}