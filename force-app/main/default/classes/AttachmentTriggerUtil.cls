public class AttachmentTriggerUtil {

    
    public static void checkApprouvedQuoteDeletion(List<Attachment> atts){
        Set<Id> qteID = new Set<ID>();
        List<Attachment> attError = new List<Attachment>();
        for(Attachment att : atts){
			qteID.add(att.ParentID);            
        }
        
        List<Quote> quotes = [Select ID, Name, Status, RecordTypeId FROM QUOTE WHERE ID =: qteID];
        if(quotes.size() != null){
            for(Quote qte : quotes){
                if(qte.Status == 'Approuvé' && qte.RecordTypeId == Label.QUO_RT_ApprouvedConga){
                    attError.add(getAttchmentByParentID(qte.ID, atts));
                }
            }
            
            for(Attachment att : attError){
                att.addError('Vous ne pouvez pas supprimer un devis qui a été validé');          
            }            
        }
       
        
        
        
    }
    
    
    private static Attachment getAttchmentByParentID(ID parentID, List<Attachment> atts){
        
       for(Attachment att : atts){
           if(att.ParentID == parentID){
               return att;
           }       
        }
        
        return new Attachment();
    }
}