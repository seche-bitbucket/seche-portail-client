@isTest
public class OpportunityCreationCheckTriggerTest {
    
    @IsTest(SeeAllData=true) 
    public static void TestOpportunityCreationCheckTrigger(){
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Assistante commerciale' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname');
        
        System.RunAs(currentUser) { 
            
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aéronautique', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try{
                insert ac;
            }catch(Exception e){
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '0606060606');
            
            try{
                insert contactTest;
            }catch(Exception e1){
            }
            
            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];
            
            //Create Opportunity
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
            Test.startTest();
            try{
                insert opportTest;
            }catch(Exception e2){
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }     
            
            List<GroupMember> grpMember = [Select ID, userOrGroupID  From GroupMember Where GroupID = : [Select ID, Name From Group Where name = :opportTest.Filiale__c] AND userOrGroupID =:opportTest.OwnerId ];
            System.assertEquals(0, grpMember.size());
            Test.stopTest();
        }
    }

    @IsTest(SeeAllData=true) 
    public static void TestOpportunityCreationDiffGroup(){
        
        //Get a profile to create User
        Profile p = [select id from profile where name = 'Solution Manager' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname', SubsidiaryCreationRight__c='Drimm');
        
        System.RunAs(currentUser) { 
            
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aéronautique', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try{
                insert ac;
            }catch(Exception e){
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '0606060606');
            
            try{
                insert contactTest;
            }catch(Exception e1){
            }
            
            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];
            
            //Create Opportunity
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234);
            Test.startTest();
            try{
                insert opportTest;
            }catch(Exception e2){
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }     
            
            List<GroupMember> grpMember = [Select ID, userOrGroupID  From GroupMember Where GroupID = : [Select ID, Name From Group Where name = :opportTest.Filiale__c] AND userOrGroupID =:opportTest.OwnerId ];
            System.assertEquals(0, grpMember.size());
            Test.stopTest();
        }
    }

    @IsTest(SeeAllData=true) 
    public static void TestOpportunityCreationForAdmin(){
        
        //Get a profile to create User
        Profile p = [select id from profile where name = 'Solution Manager' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname', SubsidiaryCreationRight__c='Drimm');
        
        System.RunAs(currentUser) { 
            
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aéronautique', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try{
                insert ac;
            }catch(Exception e){
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '0606060606');
            
            try{
                insert contactTest;
            }catch(Exception e1){
            }
            
            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_OpportuniteIntraGroupe' LIMIT 1];
            
            //Create Opportunity
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234);
            Test.startTest();
            try{
                insert opportTest;
            }catch(Exception e2){
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }     
            
            List<GroupMember> grpMember = [Select ID, userOrGroupID  From GroupMember Where GroupID = : [Select ID, Name From Group Where name = :opportTest.Filiale__c] AND userOrGroupID =:opportTest.OwnerId ];
            System.assertEquals(0, grpMember.size());
            Test.stopTest();
        }
    }


    @IsTest(SeeAllData=true) 
    public static void TestOpportunityUpdate(){
        
        //Get a profile to create User
        Profile p = [select id from profile where name ='System Administrator' or name ='Administrateur système' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname', SubsidiaryCreationRight__c='Alcea');
        
        System.RunAs(currentUser) { 
            
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aéronautique', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try{
                insert ac;
            }catch(Exception e){
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '0606060606');
            
            try{
                insert contactTest;
            }catch(Exception e1){
            }
            
            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_OpportuniteIntraGroupe' LIMIT 1];
            
            //Create Opportunity
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234);
            insert opportTest;
            Test.startTest();
            try{               
                opportTest.Filiale__c = 'Alcea';
                update opportTest;
            }catch(Exception e2){
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }     
            
            List<GroupMember> grpMember = [Select ID, userOrGroupID  From GroupMember Where GroupID = : [Select ID, Name From Group Where name = :opportTest.Filiale__c] AND userOrGroupID =:opportTest.OwnerId ];
            System.assertEquals(0, grpMember.size());
            Test.stopTest();
        }
    }

    @IsTest(SeeAllData=true) 
    public static void TestOpportunityAdminWithIntraGroup(){
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Assistante commerciale' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname');
        
        System.RunAs(currentUser) { 
            
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aéronautique', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try{
                insert ac;
            }catch(Exception e){
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '0606060606');
            
            try{
                insert contactTest;
            }catch(Exception e1){
            }
            
            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_OpportuniteIntraGroupe' LIMIT 1];
            
            //Create Opportunity
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
            Test.startTest();
            try{
                insert opportTest;
            }catch(Exception e2){
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }     
            
            List<GroupMember> grpMember = [Select ID, userOrGroupID  From GroupMember Where GroupID = : [Select ID, Name From Group Where name = :opportTest.Filiale__c] AND userOrGroupID =:opportTest.OwnerId ];
            System.assertEquals(0, grpMember.size());
            Test.stopTest();
        }
    }


    @IsTest(SeeAllData=true) 
    public static void TestOpportunityForForDirCo(){
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Directeur commercial' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@capg.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname', SubsidiaryCreationRight__c='Alcea');
        
        System.RunAs(currentUser) { 
            
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aéronautique', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try{
                insert ac;
            }catch(Exception e){
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '0606060606');
            
            try{
                insert contactTest;
            }catch(Exception e1){
            }
            
            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];
            
            //Create Opportunity
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
            Test.startTest();
            try{
                insert opportTest;
            }catch(Exception e2){
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }     
            
            List<GroupMember> grpMember = [Select ID, userOrGroupID  From GroupMember Where GroupID = : [Select ID, Name From Group Where name = :opportTest.Filiale__c] AND userOrGroupID =:opportTest.OwnerId ];
            System.assertEquals(0, grpMember.size());
            Test.stopTest();
        }
    }
    
    
}