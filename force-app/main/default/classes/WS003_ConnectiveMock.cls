@isTest
global class WS003_ConnectiveMock implements  HTTPCalloutMock{
    global Static String externalId;
    global Static String status;
	global HTTPResponse respond(HTTPRequest req){
        HttpResponse res = new HTTPResponse();
        res.setHeader('Content-Type', 'application/JSON');
        res.setBody('{"ContinuationToken":"1","Items":[{"Id":"a02c9f7a-cfd0-46d2-8ba3-465a5bd79fce","FlowStatus":"'+status+'","ExternalReference":"'+externalId+'"},{"Id":"eda3f2a1-ee75-49c6-bf0c-78da693251fb","FlowStatus":"PENDINGSIGNING","ExternalReference":"Test Reference Digital"},{"Id":"c1acae65-a85c-4fc9-aec5-4d474b3bcc86","FlowStatus":"SIGNED","ExternalReference":"Test Reference Manual"}],"MaxQuantity":50,"Total":3}');
        req.setMethod('GET');
        res.setStatusCode(200);
        return res;
    }
}