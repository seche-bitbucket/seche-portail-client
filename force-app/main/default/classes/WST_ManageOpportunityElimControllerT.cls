@isTest(SeeAllData=true)
public class WST_ManageOpportunityElimControllerT {
  
    //FINISHED
    @isTest
    public static void WST_ManagerOpportunityElimControllerTest(){
		
        Test.startTest();
        
         //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        
        //insert WST_Waste_Statement
        insert vWst1;
        
       
        //create List of WST_Waste_Statement
		List<WST_Waste_Statement__c> vLwsts = new List<WST_Waste_Statement__c>();
        vLwsts.add(vWst1);
		         
        //create controller
        Test.setCurrentPage(Page.WST_ManageEliminationOpportunity);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(vLwsts);
        stdSetController.setSelected(vLwsts);
        
        //call the method
        WST_ManageOpportunityElimController sdGenerateCSVController = new WST_ManageOpportunityElimController(stdSetController);
        
        Test.stopTest();
    }
    
   
    //FINISHED
    @isTest
    public static void createOpportunityTest(){
    
         Test.startTest();
        
        //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        
        //insert WST_Waste_Statement
        insert vWst1;
        
        //create a list WST_Waste_Statement
        List<WST_Waste_Statement__c> vLwsts = new List<WST_Waste_Statement__c>();
        vLwsts.add(vWst1);
        
 
        //call method
        Opportunity vResult = WST_ManageOpportunityElimController.createOpportunity(vLwsts);
        
        //check the result
        System.debug('createOpportunityTest.vResult =' + vResult);  
        System.assertNotEquals(null, vResult);
        System.assertEquals('Transformateurs', vResult.Filiere__c);

        Test.stopTest();
		}

    
    


	//FINISHED
    @isTest
    public static void getQuoteEndDateTest(){
    
      Test.startTest();
        String month = '';
        //TEST1
		//create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        
        //insert 
        insert vWst1;
 
        //call method
        Date vResult =  WST_ManageOpportunityElimController.getQuoteEndDate(vWst1.Id);
        
        //check the result
		System.debug('getQuoteEndDateTest.vResult1 =' + vResult); 
        System.assertEquals(system.today(), vResult);
        
        //TEST2
        //call method
        Date vResult2 = WST_ManageOpportunityElimController.getQuoteEndDate(Label.RCT_PB_Reparation);
        //check the result
		System.debug('getQuoteEndDateTest.vResult2 =' + vResult2); 
		if(vResult2.month() < 10){
            month = '0'+vResult2.month();
        }else{
            month = vResult2.month()+'';
        }
        //Check the result
        System.assertEquals(Label.RCT_REPA_END_DATE_CONTRACT, vResult2.day()+'/'+month+'/'+vResult2.year());



    	//TEST3
        //call method
        Date vResult3 = WST_ManageOpportunityElimController.getQuoteEndDate(Label.RCT_PB_Depollution);
        //check the result
		System.debug('getQuoteEndDateTest.vResult3 =' + vResult3); 

        if(vResult3.month() < 10){
            month = '0'+vResult3.month();
        }else{
            month = vResult3.month()+'';
        }
        //Check the result
        System.assertEquals(Label.RCT_DEPOL_END_DATE_CONTRACT, vResult3.day()+'/'+month+'/'+vResult3.year());
		                        
        //TEST4
        //call method
        Date vResult4 = WST_ManageOpportunityElimController.getQuoteEndDate(Label.RCT_PB_Elimination_N);
        //check the result
		System.debug('getQuoteEndDateTest.vResult4 =' + vResult4); 
       if(vResult4.month() < 10){
            month = '0'+vResult4.month();
        }else{
            month = vResult4.month()+'';
        }
        //Check the result
        System.assertEquals(Label.RCT_ELIM_END_DATE_CONTRACT, vResult4.day()+'/'+month+'/'+vResult4.year());

		

		//TEST5
        //call method
        Date vResult5 = WST_ManageOpportunityElimController.getQuoteEndDate(Label.RCT_PB_Elimination_SF6);
        //check the result
		System.debug('getQuoteEndDateTest.vResult5 =' + vResult5);  
        if(vResult5.month() < 10){
            month = '0'+vResult5.month();
        }else{
            month = vResult5.month()+'';
        }
        //Check the result
        System.assertEquals(Label.RCT_SF6_END_DATE_CONTRACT, vResult5.day()+'/'+month+'/'+vResult5.year());

        Test.stopTest();
		}

    
    
    //FINISHED
    @isTest
    public static void manageOpportunitesTest(){
        
         Test.startTest();
        
        //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        
        //insert WST_Waste_Statement
        insert vWst1;
        
        //create a list WST_Waste_Statement
        List<WST_Waste_Statement__c> vLwsts = new List<WST_Waste_Statement__c>();
        vLwsts.add(vWst1);
 
        //call method
        PageReference vResult = WST_ManageOpportunityElimController.manageOpportunites();
        
        //check the result
        System.debug('manageOpportunitesTest.vResult =' + vResult);  
        System.assertNotEquals(null, vResult);

        Test.stopTest();
  
    }
    
    
    
    
    
      //FINISHED
    @isTest
    public static void createOpportunityLineItemsTest(){
        
         Test.startTest();
        
        //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        vWst1.Type__c = 'Cabine';
        vWst1.TotalWeight__c = 400; 
        //insert WST_Waste_Statement
        insert vWst1;
        
        //Create opportunity
	     Opportunity vOpportunity = TestDataFactory.createOpportunity('OpportunityName1');    
         
        //insert opportunity
        insert vOpportunity;
         
        //link Waste_Statement to opportunity
        vWst1.Opportunity__c = vOpportunity.Id;
        
        
        
        //create a list WST_Waste_Statement
        List<WST_Waste_Statement__c> vLwsts = new List<WST_Waste_Statement__c>();
        vLwsts.add(vWst1);
 
        //call method
        Boolean vResult= WST_ManageOpportunityElimController.createOpportunityLineItems(vOpportunity,vLwsts);
        
       //check the result
       System.debug('createOpportunityLineItemsTest.vResult =' + vResult);  
       System.assertNotEquals(null, vResult);
       System.assertEquals(true, vResult);

       Test.stopTest();
  
    }
    
    
    //FINISHED
    @isTest
    public static void createQuoteTest(){
        
         Test.startTest();
        
        //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        
        //insert WST_Waste_Statement
        insert vWst1;
        
        //Create opportunity
	     Opportunity vOpportunity = TestDataFactory.createOpportunity('OpportunityName1');    
         
        //insert opportunity
        insert vOpportunity;
         
        //link Waste_Statement to opportunity
        vWst1.Opportunity__c = vOpportunity.Id;
        
        
        //create a list WST_Waste_Statement
        List<WST_Waste_Statement__c> vLwsts = new List<WST_Waste_Statement__c>();
        vLwsts.add(vWst1);
 
        //call method
        Quote vResult= WST_ManageOpportunityElimController.createQuote(vLwsts,vOpportunity);
        
       //check the result
       System.debug('createOpportunityLineItemsTest.vResult =' + vResult);  
       System.assertNotEquals(null, vResult);

       Test.stopTest();
  
    }
    
    
    
    


    
}