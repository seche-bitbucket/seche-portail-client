global with sharing class INV_SendEmail {
    
    private class CustomException extends Exception {}

    global class EmailParameters {

        @InvocableVariable(required=true)
        global ID emailTemplateId;

        @InvocableVariable(required=true)
        global Id toId;

        @InvocableVariable
        global String[] ccAdresses;

        @InvocableVariable(required=true)
        global Id owa;

        @InvocableVariable(required=true)
        global Id whatId;

        @InvocableVariable
        global Id[] attachementsId;

    }
    
    @InvocableMethod(label='Send Email' description='Send Email using Email Template' category='System Utils')
    public static void sendEmail(List<EmailParameters> request) {
        
        try{
            
            if(Test.isRunningTest() && request[0].ccAdresses.contains('Error')) {
                throw new CustomException('Test');
            }

            String owa = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = 'Séché Connect'].Id;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setOrgWideEmailAddressId(request[0].owa);
            if(request[0].ccAdresses != null && request[0].ccAdresses.size() > 0 ) {
                mail.setCcAddresses(request[0].ccAdresses);
            }
            if(request[0].attachementsId != null && request[0].attachementsId.size() > 0 ) {
                List<Id> cvsId = new List<Id>();
                String signedFile = [SELECT Id, Title FROM ContentVersion WHERE (ContentDocumentId IN :request[0].attachementsId) AND Title LIKE '%Signée' Order By CreatedDate DESC LIMIT 1].Title;
                signedFile = signedFile.substring(0, signedFile.indexoF(' - Signée'));
                for(ContentVersion cv: [SELECT Id, Title FROM ContentVersion WHERE ContentDocumentId IN :request[0].attachementsId]) {
                    if(!(cv.Title.contains(signedFile) && !cv.Title.contains('Signé'))) {
                        cvsId.add(cv.Id);
                    }
                }
                mail.setEntityAttachments(cvsId);
            }
            mail.setTargetObjectId(request[0].toId);
            mail.setWhatId(request[0].whatId);
            mail.setTemplateId(request[0].emailTemplateId);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            // create task

        }catch(Exception ex){
            System.debug(ex.getMessage()+' '+ex.getLineNumber());
            return;
        }   
        return;

    }
    
}