/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-06-29
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for Batch launches the Convention generation by conga workflow
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
private class Test_CourrierConventionBatch {
    
    @isTest static void testBatchExecute(){
        
        //String quoteName = '%Convention '+ (System.Today().year() +1)+'%';
        String quoteName = '%Convention '+System.Label.YearN1+'%';
        String query = 'SELECT Id, Name,CongaConventionID__c, CreatedDate,OpportunityId,Status,(Select Id From Attachments), (SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLinks) FROM Quote WHERE NAME Like \''+quoteName+'\' AND NAME Like \'%V1%\' AND Status NOT IN(\'Présenté\', \'Envoyé pour signature\', \'Signé\', \'Refusé\') AND Opportunity.Convention_face_face__c = false';
        Account acc = new Account(Name= 'TestAcc');
        insert acc;
        acc = [Select Id from Account Limit 1];        
        Opportunity opp=new Opportunity(Name='TestOpp', AccountId= acc.Id, StageName = 'Proposition commerciale' , CloseDate = date.newInstance(System.today().year()+1, 1, 1) );               
        insert opp;
        opp = [Select Id from Opportunity Limit 1];
        //Quote quo = new Quote(Name='Convention '+ (System.Today().year() +1)+'%V1%' , OpportunityId = opp.Id , Status = 'Brouillon');
        Quote quo = new Quote(Name='Convention '+System.Label.YearN1+' %V1%' , OpportunityId = opp.Id , Status = 'Brouillon');
        insert quo;   
        
        Contract__c con = new Contract__c(Filiale__c = 'Drimm' ,Status__c ='Brouillon' ,Type_de_convention__c = 'DND TRAITEMENT TRANSPORT' ,Name = 'testCon',Account__c = acc.Id , Opportunity__c = opp.Id);
        insert con;
        
        
        Test.startTest();
        CourrierConventionBatch batch = new CourrierConventionBatch(); 
        Database.executeBatch(batch);
        Test.stopTest();
    }
}