@isTest(seeAllData=true)
public class VisitReportTest {
    /** * @author Thomas Prouvot * @date Creation 19.09.2016 * @description VisitReportTest – TriggerTestClass */
	
    static testMethod void testVisitReport(){
        //TODO test trigger to be bulkified
         Account ac = new Account(Name='TestAccount',
                                       Producteur__c = false,
                                       BillingPostalCode='69800',
                                       TVA_Number__c = '2434234',
                                  		NAF_Number__c = '1234B');
                                       //SIRET_Number__c = 345436);
     	insert ac;
        
        Contact cont = new Contact(FirstName='Test',
						LastName='Test',
						Accountid= ac.id);
        insert cont;
        User commercial = new User(Alias = 'standt',
                          Email='standarduser@testorg.com', 
      					  EmailEncodingKey='UTF-8',
                          LastName='Testing',
                          LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US',
                          ProfileId = UserInfo.getProfileId(), 
      					  TimeZoneSidKey='America/Los_Angeles',
                          UserName='monuser@montest.org');   
        insert commercial;
        Test.startTest();
        VisitReport__c vr = new VisitReport__c(
            			  Commercial_associe__c = commercial.Id,
                          Account__c=ac.Id,
                          Contact__c=cont.Id,
            			  Name='Description du compte rendu');
        insert vr;
        
        //reload to get case id
        vr = [SELECT Id, OwnerId FROM VisitReport__c WHERE Id=:vr.Id];
        System.assertEquals(commercial.Id , vr.OwnerId);
        
        VisitReport__Share vrs = [Select Id, ParentId, UserOrGroupId, AccessLevel FROM VisitReport__Share WHERE ParentId=: vr.Id AND AccessLevel='Edit'];
        System.assertEquals(vrs.UserOrGroupId , UserInfo.getUserId());        
        
  		Test.stopTest();   
    }
    
    static testMethod void testVisitReportOpportunity(){
        //TODO test trigger to be bulkified
         Account ac = new Account(Name='TestAccount',
                                       Producteur__c = false,
                                       BillingPostalCode='69800',
                                       TVA_Number__c = '2434234',
                                  		NAF_Number__c = '1234B');
                                       //SIRET_Number__c = 345436);
     	insert ac;
        
        Contact cont = new Contact(FirstName='Test',
						LastName='Test',
						Accountid= ac.id);
        insert cont;
        User commercial = new User(Alias = 'standt',
                          Email='standarduser@testorg.com', 
      					  EmailEncodingKey='UTF-8',
                          LastName='Testing',
                          LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US',
                          ProfileId = UserInfo.getProfileId(), 
      					  TimeZoneSidKey='America/Los_Angeles',
                          UserName='monuser@montest.org');   
        insert commercial;
        
        User commercial2 = new User(Alias = 'standt2',
                          Email='standarduser2@testorg.com', 
      					  EmailEncodingKey='UTF-8',
                          LastName='TestingT',
                          LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US',
                          ProfileId = UserInfo.getProfileId(), 
      					  TimeZoneSidKey='America/Los_Angeles',
                          UserName='monuser2@montest.org');   
        insert commercial2;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'test';
        opp.Salesman__c = commercial2.Id;
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = ac.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = cont.Id;
        insert opp;
        
        Test.startTest();
        VisitReport__c vr = new VisitReport__c(
            			  Commercial_associe__c = commercial.Id,
                          Account__c=ac.Id,
                          Contact__c=cont.Id,
            			  Opportunity__c = opp.ID,
            			  Name='Description du compte rendu');
        insert vr;
       
        List<VisitReport__Share> vrs = [Select Id, ParentId, UserOrGroupId, AccessLevel FROM VisitReport__Share WHERE ParentId=: vr.Id AND AccessLevel='Edit'];
        System.assertEquals(2 , vrs.size());        
        
  		Test.stopTest();   
    }
}