@isTest
private without sharing class NotifySharedOpportunityTest {
    static testMethod void validateSharingUser() {
        
       //Get a profile to create User
        Profile p = [select id from profile where name = :'Administrateur système' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail, isactive = true,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname');
        
        insert currentUser;
        
       
        Account a = new Account();
        a.Name = 'testAccName1';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c ='1234A';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName1';
        c.AccountId = a.Id;
        c.Salesman__c = currentUser.Id;
        c.Email = 'aaaa@yopmail.com';
        c.Phone = '03 00 00 00 00';
        insert c;
        
        RecordType rt = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];
        Opportunity opp = new Opportunity();
        opp.AccountId = a.Id;
        opp.Name = 'TEST OPP';
        opp.ContactName__c = c.Id;
        opp.Nature__c = 'Spot';
        opp.Filiale__c = 'Alcea';
        opp.Amount = 100;
        //opp.TonnageN__c = 0;
        opp.CloseDate = System.today();
        opp.StageName = 'Proposition commerciale';
        opp.RecordTypeId = rt.Id;
        //opp.Salesman__c = u.Id;
        opp.Salesman__c = UserInfo.getUserId();
        insert opp;
        
        /*OpportunityShare os = new OpportunityShare();
        os.OpportunityId = opp.Id;
        os.UserOrGroupId = UserInfo.getUserId();
        os.OpportunityAccessLevel = 'Read';
        insert os;*/
        
        Group grp = new Group();
        grp.Name = 'testGrp';
        grp.DoesIncludeBosses = false;
        grp.DeveloperName = 'ABC';
        insert grp;
        
        System.RunAs(currentUser)
        {
            GroupMember gm1 = new GroupMember();
            gm1.GroupId = grp.id;
            gm1.UserOrGroupId = currentUser.id;
            insert gm1;
        }
        
        OpportunityShare os2 = new OpportunityShare();
        os2.OpportunityId = opp.Id;
        os2.UserOrGroupId = grp.id;
        os2.OpportunityAccessLevel = 'Read';
        insert os2;
        
        Test.startTest();
        NotifySharedOpportunity n = new NotifySharedOpportunity();
        Database.executeBatch(n);
        Test.stopTest();
    }
}