@isTest
public class QLI_ListViewTest {
    @isTest
    public static void listViewTest() {      
        ID  standardPb = Test.getStandardPricebookId();
        Product2 prd1 = new Product2 (Name='Test Product Entry 1',Description='Test Product Entry 1',productCode = 'ABC', isActive = true, TECH_ExternalID__c='zExternalID_Capgemini001');
        QuoteLineItem_Field__c f = new QuoteLineItem_Field__c(Name='INCOTERMS__c', OppLineSyncField__c='INCOTERMS__c');
        insert f;
        insert prd1;
        PricebookEntry pbe1 = new PricebookEntry (Product2ID=prd1.id,Pricebook2ID=standardPb ,UnitPrice=50, isActive=true);
        insert pbe1;
        Account acc = TestDataFactory.createAccount('Test Account');
        Opportunity opp = TestDataFactory.createOpportunity('Test Opp');
        insert acc;
        opp.AccountId = acc.Id;
        insert opp;
        OpportunityLineItem oli = new OpportunityLineItem (OpportunityID = opp.id, PriceBookEntryID=pbe1.id, quantity=4, UnitPrice=4);
        insert oli;
        Quote q = new Quote();
        q.OpportunityId = opp.Id;
        q.Name = opp.Name;
        q.DateDevis__c = Date.today();
        q.QuoteCode__c=opp.Name;
        insert q;


        List<QuoteLineItem> listQli = new List<QuoteLineItem>();
        QuoteLineItem qli = new QuoteLineItem(QuoteId = q.Id, PriceBookEntryID=pbe1.id, quantity=4, UnitPrice=10, OpportunityLineItemId = oli.Id);
        listQli.add(qli);
        insert listQli;
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(listQli); 
        stdSetController.setSelected(listQli);
        Test.StartTest();    
        QLI_ListView ext = new QLI_ListView(stdSetController);
        Test.StopTest();
    }
}