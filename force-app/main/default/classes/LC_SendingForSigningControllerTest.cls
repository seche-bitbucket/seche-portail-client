@isTest
public with sharing class LC_SendingForSigningControllerTest {
   @isTest
    public static void testrequestForSign(){
        Profile p = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];        
        String testemail2 = 'assistanttest@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        director.UserRoleId=[select Id from UserRole where Name='Direction Commerciale - Nicolas Rogeau'].Id;
        insert director;
        System.RunAs(director) {
            Account a;
            Contact c;
            Opportunity opp;
            Quote q;
            a = new Account();
            a.Name = 'testAccName';
            a.CustomerNature__c = 'Administration';
            a.Industry = 'Aéronautique';
            a.CurrencyIsoCode = 'EUR';
            a.Producteur__c = false;
            a.BillingPostalCode = '00000';
            a.NAF_Number__c = '1234A';
            a.BillingStreet = '12 rue Pré Gaudry';
            insert a;
            
            c = new Contact();
            c.LastName = 'testName';
            c.AccountId = a.Id;
            c.Email = 'sechetestsfdc@gmail.com';
            c.Phone = '000000000';
            insert c;
            
            opp = new Opportunity();
            opp.Name = 'Mon OPP Convention';
            opp.StageName = 'Contractualisation';
            opp.CloseDate = System.today().addDays(10);
            opp.AccountId = a.Id;
            opp.TonnageN__c = 0;
            opp.Filiale__c = 'Tredi Hombourg';
            opp.Filiere__c = 'Physico';
            opp.ContactName__c = c.Id;
            insert opp;
            
            q = new Quote();
            q.OpportunityId = opp.ID;
            q.Name = opp.Name;
            q.DateDevis__c = Date.today();
            q.QuoteCode__c=opp.Name+'test ';
            q.ContactId = opp.ContactName__c;
            q.Email = 'sechetestsfdc@gmail.com';
            q.Phone = opp.ContactName__r.Phone;
            q.Fax = opp.ContactName__r.Fax;
            q.BillingStreet = opp.Account.BillingStreet;
            q.BillingCity = opp.Account.BillingCity;
            q.BillingState = opp.Account.BillingState;
            q.BillingPostalCode = opp.Account.BillingPostalCode;
            q.BillingCountry = opp.Account.BillingCountry; 
            insert q;
            
            Attachment att = new Attachment();
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            att.body=bodyBlob;
            att.Name = 'Convention 2019'+q.QuoteCode__c+'.pdf';	
            att.ParentId = q.Id;             
            insert att; 
            List<Attachment> atts=[SELECT id, Name, body, ContentType,ParentId FROM Attachment WHERE ParentId=:q.Id];
            system.debug('attachements'+atts.size());
            test.startTest();
            sendQuoteCommunityLink.sendQuoteForSignature(q.Id);
        	test.stopTest();
            q.Name = 'Convention 2019'+opp.Name;
            Update q;
           LC_SendingForSigningController.requestForSign(q);
            Quote quoteUpdate=[Select Id,Tech_UID__c,TECH_isApprouved__c,RecordTypeId FROM Quote Where ID=:q.ID];
            RecordType rt=[SELECT Id, Name, DeveloperName, Description FROM RecordType Where DeveloperName='Devis_approuv_Read_Only' LIMIT 1];
            System.assert(quoteUpdate.TECH_UID__c!=Null);
        }
    }

 @isTest
    public static void testrequestForSignNotAtt(){
        Profile p = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];        
        String testemail2 = 'assistanttest@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        director.UserRoleId=[select Id from UserRole where Name='Direction Commerciale - Nicolas Rogeau'].Id;
        insert director;
        System.RunAs(director) {
            Account a;
            Contact c;
            Opportunity opp;
            Quote q;
            a = new Account();
            a.Name = 'testAccName';
            a.CustomerNature__c = 'Administration';
            a.Industry = 'Aéronautique';
            a.CurrencyIsoCode = 'EUR';
            a.Producteur__c = false;
            a.BillingPostalCode = '00000';
            a.NAF_Number__c = '1234A';
            a.BillingStreet = '12 rue Pré Gaudry';
            insert a;
            
            c = new Contact();
            c.LastName = 'testName';
            c.AccountId = a.Id;
            c.Email = 'sechetestsfdc@gmail.com';
            c.Phone = '000000000';
            insert c;
            
            opp = new Opportunity();
            opp.Name = 'Mon OPP Convention';
            opp.StageName = 'Contractualisation';
            opp.CloseDate = System.today().addDays(10);
            opp.AccountId = a.Id;
            opp.TonnageN__c = 0;
            opp.Filiale__c = 'Tredi Hombourg';
            opp.Filiere__c = 'Physico';
            opp.ContactName__c = c.Id;
            insert opp;
            
            q = new Quote();
            q.OpportunityId = opp.ID;
            q.Name = opp.Name;
            q.DateDevis__c = Date.today();
            q.QuoteCode__c=opp.Name+'test ';
            q.ContactId = opp.ContactName__c;
            q.Email = 'sechetestsfdc@gmail.com';
            q.Phone = opp.ContactName__r.Phone;
            q.Fax = opp.ContactName__r.Fax;
            q.BillingStreet = opp.Account.BillingStreet;
            q.BillingCity = opp.Account.BillingCity;
            q.BillingState = opp.Account.BillingState;
            q.BillingPostalCode = opp.Account.BillingPostalCode;
            q.BillingCountry = opp.Account.BillingCountry; 
            insert q;            
           
            test.startTest();
            sendQuoteCommunityLink.sendQuoteForSignature(q.Id);
        	test.stopTest();
            q.Name = 'Convention 2019'+opp.Name;
            Update q;
            LC_SendingForSigningController.requestForSign(q);
            Quote quoteUpdate=[Select Id,Tech_UID__c,TECH_isApprouved__c,RecordTypeId FROM Quote Where ID=:q.ID];
            RecordType rt=[SELECT Id, Name, DeveloperName, Description FROM RecordType Where DeveloperName='Devis_approuv_Read_Only' LIMIT 1];
            System.assert(quoteUpdate.TECH_UID__c==Null);
        }
    }

@isTest
     public static void testGetQuote(){
        Profile p = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];        
        String testemail2 = 'assistanttest@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        director.UserRoleId=[select Id from UserRole where Name='Direction Commerciale - Nicolas Rogeau'].Id;
        insert director;
        System.RunAs(director) {
            Account a;
            Contact c;
            Opportunity opp;
            Quote q;
            a = new Account();
            a.Name = 'testAccName';
            a.CustomerNature__c = 'Administration';
            a.Industry = 'Aéronautique';
            a.CurrencyIsoCode = 'EUR';
            a.Producteur__c = false;
            a.BillingPostalCode = '00000';
            a.NAF_Number__c = '1234A';
            a.BillingStreet = '12 rue Pré Gaudry';
            insert a;
            
            c = new Contact();
            c.LastName = 'testName';
            c.AccountId = a.Id;
            c.Email = 'sechetestsfdc@gmail.com';
            c.Phone = '000000000';
            insert c;
            
            opp = new Opportunity();
            opp.Name = 'Mon OPP Convention';
            opp.StageName = 'Contractualisation';
            opp.CloseDate = System.today().addDays(10);
            opp.AccountId = a.Id;
            opp.TonnageN__c = 0;
            opp.Filiale__c = 'Tredi Hombourg';
            opp.Filiere__c = 'Physico';
            opp.ContactName__c = c.Id;
            insert opp;
            
            q = new Quote();
            q.OpportunityId = opp.ID;
            q.Name = opp.Name;
            q.DateDevis__c = Date.today();
            q.QuoteCode__c=opp.Name+'test ';
            q.ContactId = opp.ContactName__c;
            q.Email = 'sechetestsfdc@gmail.com';
            q.Phone = opp.ContactName__r.Phone;
            q.Fax = opp.ContactName__r.Fax;
            q.BillingStreet = opp.Account.BillingStreet;
            q.BillingCity = opp.Account.BillingCity;
            q.BillingState = opp.Account.BillingState;
            q.BillingPostalCode = opp.Account.BillingPostalCode;
            q.BillingCountry = opp.Account.BillingCountry; 
            insert q;           
                     
            System.assertEquals(q.Id, LC_SendingForSigningController.getQuote(q.Id).Id);
           }
    }

    
}