/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ConnectiveMockGet50Status
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ConnectiveMockGet50Status
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 16-06-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
@isTest
global class ConnectiveMockGet50Status implements  HTTPCalloutMock{
	global HTTPResponse respond(HTTPRequest req){
        
        HttpResponse res = new HTTPResponse();
        res.setHeader('Content-Type', 'application/JSON');
        res.setBody('{"ContinuationToken": "1","Items": [{"PackageId": "3bb6a2d1-35db-4a3a-a434-868c01bcca9d","PackageStatus": "Finished","ExternalPackageReference": "0Q01i000000l0V2CAI"}],"MaxQuantity": 50,"Total": 21476}');
        req.setMethod('GET');
        res.setStatusCode(200);
        return res;
    }
}