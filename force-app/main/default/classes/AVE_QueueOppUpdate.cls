public with sharing class AVE_QueueOppUpdate implements Queueable {

    private List<Opportunity> oppsToUpdate;
    private List<QuoteLineItem> qlis;

    public AVE_QueueOppUpdate(List<Opportunity> opps, List<QuoteLineItem> qlis) {
        this.oppsToUpdate = opps;
        this.qlis = qlis;
    }

    public void execute(QueueableContext context) {
        try{
            ByPassUtils.ByPass('OpportunityTrigger');
            update oppsToUpdate;
            System.enqueueJob(new AVE_QueueOLIsUpdateSync(this.oppsToUpdate, this.qlis));
        } catch (Exception e) {
            Log__c error = new Log__c(ApexJob__c= AVE_QueueOppUpdate.class.getName(),
                                    ErrorMessage__c=e.getMessage()+' at '+e.getLineNumber(),
                                    ExceptionType__c='INTERNAL');
            insert error;
        }
    }

}