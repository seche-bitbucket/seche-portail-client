/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ConnectiveMockGetStatus
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ConnectiveMockGetStatus
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 16-06-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
@isTest
global class ConnectiveMockGetStatus implements  HTTPCalloutMock{
        global Static String externalIdContact;
        global Static String externalIdDocument;
        global Static String connectiveIdPackage;
	global HTTPResponse respond(HTTPRequest req){
        
        HttpResponse res = new HTTPResponse();
        res.setHeader('Content-Type', 'application/JSON');
        res.setBody('{"PackageName": "package-docu1.pdf","Initiator": "signer@gmail.com","ExpiryTimestamp": null,"ExternalPackageReference": "'+externalIdDocument+'","F2FSigningUrl": "http://myserver/signinit?packageSignId=6bc402eb-6cbd-423a-bf00-1157e8d68f37&f2f=True","PackageStatus": "Finished","PackageDocuments": [{"DocumentId": "dc2691d8-e3c0-470b-9715-e55b489ea493","DocumentType": "application/pdf","ExternalDocumentReference": "'+externalIdDocument+'","DocumentName": "docu1"}],"Stakeholders": [{"Type": "PersonGroup","EmailAddress": null,"ContactGroupCode": null,"ExternalStakeholderReference": "'+externalIdContact+'","StakeholderId": "6b2cda0c-ab81-4984-9e16-159fe20d983f","Actors": [{"Type": "Signer","Reason": null,"CompletedBy": null,"CompletedTimestamp": null,"Locations": [{"Id": "24ab070a-29e4-496e-9b79-e66c0edcced7","UsedSigningType": null}],"ActorId": "2806f94d-2a45-4168-8667-cbd4ce4ce090","ActionUrl": null,"ActionUrls": [{"EmailAddress": "john.smith@mail.com","Url": "https://MyURL.com"},{"EmailAddress": "jane.jefferson@mail.com","Url": "https://HerURL.com"}],"ActorStatus": "Available"},{"Type": "Receiver","ActorId": "e3da1877-fac9-43c4-9949-bacef76718fa","ActionUrl": null,"ActionUrls": [],"ActorStatus": ""}],"PersonGroupName": "APIGroup"},{"Type": "Person","EmailAddress": "signer@gmail.com","ContactGroupCode": null,"ExternalStakeholderReference": "3","StakeholderId": "490e242f-43c4-4bc0-a1b4-e2d67dc1fdac","Actors": [{"Type": "Receiver","ActorId": "0903c863-9197-4abf-93f4-694fddde9d99","ActionUrl": null,"ActionUrls": [],"ActorStatus": ""}],"PersonGroupName": null}],"CreationTimestamp": "2019-10-23T13:34:12Z"}');
        req.setMethod('GET');
        res.setStatusCode(200);
        return res;
    }
}