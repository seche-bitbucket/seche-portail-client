public with sharing class AP_AugmentationTarifUtils {

/* ***
 * Method to get depended fields list from a controller field on object(objDetail)
 * objDetail : the object
 * contrfieldApiName : the controller field
 * depfieldApiName : the depended field
 * Example : getDependentMap(new Contract__c(), 'Filiale__c','Type_de_convention__c')
 **/
public static Map<String, List<String> > getDependentMap(sObject objDetail, string contrfieldApiName,string depfieldApiName) {
	String controllingField = contrfieldApiName.toLowerCase();
	String dependentField = depfieldApiName.toLowerCase();

	Map<String,List<String> > objResults = new Map<String,List<String> >();

	Schema.sObjectType objType = objDetail.getSObjectType();
	if (objType==null) {
		return objResults;
	}

	Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();

	if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)) {
		return objResults;
	}

	Schema.SObjectField theField = objFieldMap.get(dependentField);
	Schema.SObjectField ctrlField = objFieldMap.get(controllingField);

	List<Schema.PicklistEntry> contrEntries = ctrlField.getDescribe().getPicklistValues();
	List<PicklistEntryWrapper> depEntries = wrapPicklistEntries(theField.getDescribe().getPicklistValues());
	List<String> controllingValues = new List<String>();

	for (Schema.PicklistEntry ple : contrEntries) {
		String label = ple.getLabel();
		objResults.put(label, new List<String>());
		controllingValues.add(label);
	}

	for (PicklistEntryWrapper plew : depEntries) {
		String label = plew.label;
		String validForBits = base64ToBits(plew.validFor);
		for (Integer i = 0; i < validForBits.length(); i++) {
			String bit = validForBits.mid(i, 1);
			if (bit == '1') {
				objResults.get(controllingValues.get(i)).add(label);
			}
		}
	}
	return objResults;
}

public static String decimalToBinary(Integer val) {
	String bits = '';
	while (val > 0) {
		Integer remainder = Math.mod(val, 2);
		val = Integer.valueOf(Math.floor(val / 2));
		bits = String.valueOf(remainder) + bits;
	}
	return bits;
}

public static String base64ToBits(String validFor) {
	if (String.isEmpty(validFor)) return '';

	String validForBits = '';

	for (Integer i = 0; i < validFor.length(); i++) {
		String thisChar = validFor.mid(i, 1);
		Integer val = base64Chars.indexOf(thisChar);
		String bits = decimalToBinary(val).leftPad(6, '0');
		validForBits += bits;
	}

	return validForBits;
}

private static final String base64Chars = '' +
                                          'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
                                          'abcdefghijklmnopqrstuvwxyz' +
                                          '0123456789+/';


private static List<PicklistEntryWrapper> wrapPicklistEntries(List<Schema.PicklistEntry> PLEs) {
	return (List<PicklistEntryWrapper>)
	       JSON.deserialize(JSON.serialize(PLEs), List<PicklistEntryWrapper>.class);
}

public class PicklistEntryWrapper {
public String active {get; set;}
public String defaultValue {get; set;}
public String label {get; set;}
public String value {get; set;}
public String validFor {get; set;}
public PicklistEntryWrapper(){
}

}

/*public static Map<Object,List<String> > getDependentPicklistValues( Schema.sObjectField dependToken )
{
	Schema.DescribeFieldResult depend = dependToken.getDescribe();
	Schema.sObjectField controlToken = depend.getController();
	if ( controlToken == null ) return null;
	Schema.DescribeFieldResult control = controlToken.getDescribe();
	List<Schema.PicklistEntry> controlEntries =
		(   control.getType() == Schema.DisplayType.Boolean
		    ?   null
		    :   control.getPicklistValues()
		);

	String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
	Map<Object,List<String> > dependentPicklistValues = new Map<Object,List<String> >();
	for ( Schema.PicklistEntry entry : depend.getPicklistValues() ) if ( entry.isActive() )
		{
			List<String> base64chars =
				String.valueOf
				        (   ((Map<String,Object>) JSON.deserializeUntyped( JSON.serialize( entry ) )).get( 'validFor' )
				        ).split( '' );
			for ( Integer index = 0; index < (controlEntries != null ? controlEntries.size() : 2); index++ )
			{
				Object controlValue =
					(   controlEntries == null
					    ?   (Object) (index == 1)
					    :   (Object) (controlEntries[ index ].isActive() ? controlEntries[ index ].getLabel() : null)
					);
				Integer bitIndex = index / 6, bitShift = 5 - Math.mod( index, 6 );
				if  (   controlValue == null
				        ||  (base64map.indexOf( base64chars[ bitIndex ] ) & (1 << bitShift)) == 0
				        ) continue;
				if ( !dependentPicklistValues.containsKey( controlValue ) )
				{
					dependentPicklistValues.put( controlValue, new List<String>() );
				}
				dependentPicklistValues.get( controlValue ).add( entry.getLabel() );
			}
		}
	return dependentPicklistValues;
} */

/*
 * Method to get an object pickList Values
 * Params :objObject the Object, fld the pickList Name
 */

public static List < String > getselectOptions(sObject objObject, string fld) {
	system.debug('objObject --->' + objObject);
	system.debug('fld --->' + fld);
	List < String > allOpts = new list < String > ();
	// Get the object type of the SObject.
	Schema.sObjectType objType = objObject.getSObjectType();

	// Describe the SObject using its object type.
	Schema.DescribeSObjectResult objDescribe = objType.getDescribe();

	// Get a map of fields for the SObject
	map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();

	// Get the list of picklist values for this field.
	list < Schema.PicklistEntry > values =
		fieldMap.get(fld).getDescribe().getPickListValues();

	// Add these values to the selectoption list.
	for (Schema.PicklistEntry a: values) {
		allOpts.add(a.getValue());
	}
	system.debug('allOpts ---->' + allOpts);
	allOpts.sort();
	return allOpts;
}

public static Approbation_user__mdt getApprouver(String filiale, String filiere){
	if(!Test.isRunningTest()){
		Approbation_user__mdt userEmail  = [SELECT ApproverName__c, Approbateur__c FROM Approbation_user__mdt WHERE Filiale__c =:filiale and Filiere__c =:filiere];
		return userEmail;
	} else {
		return new Approbation_user__mdt(ApproverName__c ='App', Approbateur__c='test@test.com');
	}
	
}


public static Boolean Submit( List<Contract__c> contractList, String filiale, String filiere){	

	Approbation_user__mdt filialeFiliereMdt  = [SELECT DeveloperName FROM Approbation_user__mdt WHERE Filiale__c =:filiale and Filiere__c =:filiere];
	
	String target = ' ';
	String replacement = '_';
	filiale = filiale.replace(target, replacement);
	filiere = filiere.replace(target, replacement);	
	System.debug('AP_AugmentationTarifUtils.filiale :'+filiale);
	System.debug('AP_AugmentationTarifUtils.filiere :'+filiere);
	
	if(contractList.size()>0) {
		List<Contract__c> ContractToUpdate=new List<Contract__c>();
		for(Contract__c c: ContractList) {
			c.Status__c='Soumis';
			c.RecordLocked__c=true;
			c.ChangedStatusDate__c = System.today();
			ContractToUpdate.add(c);
		}
		Approbation_user__mdt DC= getApprouver(contractList[0].Filiale__c, contractList[0].Filiere__c);

		try{
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			mail.setToAddresses(new String[] {DC.Approbateur__c});
			mail.setReplyTo(ContractList[0].AssociatedSalesRep__r.Email);
			mail.setSenderDisplayName(ContractList[0].AssociatedSalesRep__r.Name);
			mail.setSubject('Augmentations tarifaires pour '+ContractList[0].AssociatedSalesRep__r.Name);
			String Nextyear = System.Label.YearN1;
			// Integer Nextyear = 2021;
			String domainUrl=URL.getSalesforceBaseUrl().toExternalForm();
            system.debug('********domainUrl:'+domainUrl);			 
			
			
           // Old used for VFP -> String endpointUrl = domainUrl+'/apex/VFP_SalesDetails?userid='+ContractList[0].AssociatedSalesRep__c+'&name='+filialeFiliereMdt.DeveloperName+'&FilialeParam='+filiale+'&FiliereParam='+filiere;
		   String endpointUrl = domainUrl+'/lightning/n/Validation_augmentation';
			mail.setHtmlBody('Bonjour'+((DC.ApproverName__c != null) ? ' '+DC.ApproverName__c : '')+', <br/><br/><br/>       '
			                 +' Vous avez une demande de validation pour les augmentations tarifaires   '+Nextyear+' :'
			                 +' <br/>  <br/>                 '
							 +'Filiale : ' + contractList[0].Filiale__c + '<br/>Filière : ' + contractList[0].Filiere__c
							 +'<br/><br/> Veuillez cliquer sur le lien ci-dessous pour consulter les portefeuilles à approuver : <br/><br/>'
							 +endpointUrl);
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		}catch(Exception E) {
			System.debug(E);
			return false;
		}
		if(ContractToUpdate.size()>0) {
			update ContractToUpdate;
		}

	}
	return true;
}

//------------------------------------------------Methods to get data from Salesforce Reports--------------------------------

//Method to get report's data by the report Name
public static List<String>   getReportData(List<String> ReportNames, String filiale, String filiere) {

 List<String> lstResults = new List<String>();
	//00O1i000000JesjEAC
	// Get the report ID
	List <Report> reportList = [SELECT Id,DeveloperName FROM Report where
	                            DeveloperName IN: ReportNames];
	
	for(Report r : reportList){
		String reportId = r.Id;

		Reports.ReportDescribeResult describe = Reports.ReportManager.describeReport(reportId);
		Reports.ReportMetadata reportMd = describe.getReportMetadata();

	// Override filter and run report
		if( reportMd.getReportFilters() != null){
			reportMd.getReportFilters()[0].setValue(filiale);
			reportMd.getReportFilters()[1].setValue(filiere);
		
		} 
		// Run the report
		Reports.ReportResults results = Reports.ReportManager.runReport(reportId, reportMd);

		// Get the report metadata
		Reports.ReportMetadata rm = results.getReportMetadata();
		System.debug('Name: ' + rm.getName());
		System.debug('ID: ' + rm.getId());
		System.debug('Currency code: ' + rm.getCurrencyCode());
		System.debug('Developer name: ' + rm.getDeveloperName());
		
		// Get grouping info for first grouping
		Reports.GroupingInfo gInfo = rm.getGroupingsDown()[0];
		System.debug('Grouping name: ' + gInfo.getName());
		System.debug('Grouping sort order: ' + gInfo.getSortOrder());
		System.debug('Grouping date granularity: ' + gInfo.getDateGranularity());
		
	/*	Reports.Dimension dim = results.getGroupingsDown();
		Reports.GroupingValue groupingVal = dim.getGroupings()[0];
		System.debug('Key: ' + groupingVal.getKey());
		System.debug('Label: ' + groupingVal.getLabel());
		System.debug('Value: ' + groupingVal.getValue());
		

		// Get aggregates
		System.debug('First aggregate: ' + rm.getAggregates()[0]);
		System.debug('Second aggregate: ' + rm.getAggregates()[1]);

		// Get detail columns
		System.debug('Detail columns: ' + rm.getDetailColumns());

		// Get report format
		System.debug('Report format: ' + rm.getReportFormat());*/
				lstResults.add(JSON.serialize(results));

	}
	System.debug('getReportData.lstResults: ' + lstResults);
	return lstResults;
}


public static List<String> getReportDataValidation(List<String> ReportNames, String associatedSalesRep, String filiale, String filiere, Boolean faf, Boolean sousContrat) {
	
	List<String> lstResults = new List<String>();
	
	// Get the report ID
	List <Report> reportList = [SELECT Id,DeveloperName FROM Report where
	DeveloperName IN: ReportNames];
	
	for(Report r : reportList){
		String reportId = r.Id;
		
		Reports.ReportDescribeResult describe = Reports.ReportManager.describeReport(reportId);
		Reports.ReportMetadata reportMd = describe.getReportMetadata();

		
		// Override filter and run report
		if( reportMd.getReportFilters() != null){
			reportMd.getReportFilters()[0].setValue(filiale);
			reportMd.getReportFilters()[1].setValue(filiere);
			if(reportMd.getName().contains('18') || reportMd.getName().contains('19') || reportMd.getName().contains('20') || reportMd.getName().contains('21')) {
				reportMd.getReportFilters()[2].setValue(associatedSalesRep);
			} else {
				reportMd.getReportFilters()[3].setValue(associatedSalesRep);
			}
			if(faf != null) {
				system.debug('here');
				reportMd.getReportFilters()[5].setValue(String.valueOf(faf));
			}
			if(sousContrat != null) {
				system.debug('here');
				reportMd.getReportFilters()[7].setValue(String.valueOf(sousContrat));
			}
		}
		// Run the report
		Reports.ReportResults results = Reports.ReportManager.runReport(reportId, reportMd);
		
		// Get the report metadata
		Reports.ReportMetadata rm = results.getReportMetadata();
		System.debug('Name: ' + rm.getName());
		System.debug('ID: ' + rm.getId());
		System.debug('Currency code: ' + rm.getCurrencyCode());
		System.debug('Developer name: ' + rm.getDeveloperName());
		
		// Get grouping info for first grouping
		Reports.GroupingInfo gInfo = rm.getGroupingsDown()[0];
		System.debug('Grouping name: ' + gInfo.getName());
		System.debug('Grouping sort order: ' + gInfo.getSortOrder());
		System.debug('Grouping date granularity: ' + gInfo.getDateGranularity());

		Reports.ReportFactWithSummaries factSum = 
		(Reports.ReportFactWithSummaries)results.getFactMap().get('T!T');
		System.debug('Value for [0]: ' + factSum.getAggregates()[0].getLabel());
	
		// Get detail columns
		System.debug('Detail columns: ' + rm.getDetailColumns());
	
		lstResults.add(JSON.serialize(results));

	   }
	      System.debug('getReportData.lstResults: ' + lstResults);
	     return lstResults;
   }

}