public without sharing class Transfo_AddPriceBookEntriesController {
    
    public static  List<QuoteLine__c> genericQuoteLines = new  List<QuoteLine__c>();
    
    @AuraEnabled
    public static List<QuoteLine__c> getPriceBookEntries(ID wasteInstanceId){
        
        List<QuoteLine__c> results = new List<QuoteLine__c>();
        List<QuoteLine__c> resultsFirst = new List<QuoteLine__c>();
        WST_Waste_Statement__c wst = [Select ID, IsDiagnosticDone__c, PriceBook__c FROM WST_Waste_Statement__c WHERE ID = : wasteInstanceId];
        List<PricebookEntry> pbes = [Select Id, Name, ProductCode FROM PricebookEntry WHERE PriceBook2ID = : wst.PriceBook__c AND IsActive = true AND Product2.Family = 'Traitement' ORDER BY ProductCode DESC ];
        Map<ID, QuoteLine__c> qls = new Map<ID, QuoteLine__c>();
        qls = getQuoteLines(wasteInstanceId);
        
        for(PricebookEntry pbe : pbes){
            QuoteLine__c ql = new QuoteLine__c();
            if(pbe.ProductCode != 'DEFAUT' && pbe.ProductCode != 'FRL2A' && pbe.ProductCode != 'FRL2DBIS'){// Avoid to add Generic product in the list
                
                if(qls != null && qls.containsKey(pbe.ID)){
                    ql = qls.get(pbe.ID);
                }else{
                    //ql.ID = null;
                    ql.TECH_PriceBookEntryID__c = pbe.ID;
                    ql.Label__c = pbe.Name;
                    ql.Code__c = pbe.ProductCode;
                }
                results.add(ql);
            }else if(pbe.ProductCode != 'DEFAUT' && (pbe.ProductCode == 'FRL2A' || pbe.ProductCode == 'FRL2DBIS')){// Avoid to add Generic product in the list
                
                if(qls != null && qls.containsKey(pbe.ID)){
                    ql = qls.get(pbe.ID);
                }else{
                    //ql.ID = null;
                    ql.TECH_PriceBookEntryID__c = pbe.ID;
                    ql.Label__c = pbe.Name;
                    ql.Code__c = pbe.ProductCode;
                }
                resultsFirst.add(ql);
            }
            
        }
        System.debug('List First : '+resultsFirst);
        System.debug('results : '+results);
        resultsFirst.addAll(results);
        if(genericQuoteLines.size() > 0){
            
            resultsFirst.addAll(genericQuoteLines);
        }
        
        genericQuoteLines.clear();
      
        
        return resultsFirst;
    }
    
    @AuraEnabled
    public static List<QuoteLine__c> saveQuoteLines(String qls, String genericQls, ID wasteInstanceId){
        System.debug('Apex called');
        List<QuoteLine__c> qlTest = (List<QuoteLine__c>)JSON.deserialize(qls, List<QuoteLine__c>.class);
        //qlTest.addAll((List<QuoteLine__c>)JSON.deserialize(genericQls, List<QuoteLine__c>.class));
        List<QuoteLine__c> test = (List<QuoteLine__c>)JSON.deserialize(genericQls, List<QuoteLine__c>.class);
        System.debug('/////////'+test);
        qlTest.addAll(test);
        List<QuoteLine__c> qlToUpdate = new List<QuoteLine__c>();    
        List<QuoteLine__c> qlToRemove = new List<QuoteLine__c>();
        for(QuoteLine__c ql : qlTest){
            if(ql.Quantity__c != null && ql.Quantity__c != 0){
                if( ql.ID == null){
                    ql.Transformateur__c = wasteInstanceId;
                }if(ql.Code__c == ''){
                    ql.Code__c = 'DEFAUT';
                    ql.TECH_PriceBookEntryID__c = Label.PBE_GenericProductID;
                }                
                qlToUpdate.add(ql);
            }else if((ql.Quantity__c == null || ql.Quantity__c == 0) && ql.ID != null ){
                qlToRemove.add(ql);
            }
        }
        
        upsert qlToUpdate;
        delete qlToRemove;
        
        return getPriceBookEntries(wasteInstanceId);
    }
    
    public static Map<ID, QuoteLine__c> getQuoteLines(ID wasteInstanceId){
        Map<ID, QuoteLine__c> qls = new Map<ID, QuoteLine__c>();
        List<QuoteLine__c> existingQL = [SELECT ID, Code__c, Label__c, Quantity__c, TECh_PricebookEntryID__c, Transformateur__r.PriceBook__c FROM QuoteLine__c WHERE Transformateur__c = : wasteInstanceId];
        
        for(QuoteLine__c ql :existingQL){
            if(ql.Code__c != 'DEFAUT'){
                qls.put(ql.TECh_PricebookEntryID__c ,ql);
            }else{
                genericQuoteLines.add(ql);
            }
        }
        
        
        return qls;
        
    }
    
}