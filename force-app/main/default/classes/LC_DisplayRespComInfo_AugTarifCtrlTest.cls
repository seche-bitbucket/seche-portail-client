@IsTest
public with sharing class LC_DisplayRespComInfo_AugTarifCtrlTest {
    @testSetup static void setup() {
        User assistantCo = TestDataFactory.createAssistantCommercialUser();
        User respCom =  TestDataFactory.createResponsableCommercialUser(assistantCo);
        User respCom1 = TestDataFactory.createResponsableCommercialUser(assistantCo);
           
    }

    @isTest 
    static void testfetchUserDetail(){

        User us = [SELECT Id FROM User Limit 1];
        User usr = LC_DisplayRespComInfo_AugTarifCtrl.fetchUserDetail(us.Id);
        System.assertEquals(us.Id, usr.Id);
    }

}