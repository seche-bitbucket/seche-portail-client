public with sharing class VFC_GetStatusFIPController {
ApexPages.StandardSetController stdSetController;

private String fipId;
private FIP_FIP__c currentFip;
Boolean isNoSelectedOpp;

public Boolean getIsNoSelectedOpp()
{
	return isNoSelectedOpp;
}

Boolean isTestRunnig;//Used just when running class test for process else part in the init() method

public Boolean getIsTestRunnig()
{
	return isTestRunnig;
}

public void setIsTestRunnig(Boolean value)
{
	this.isTestRunnig = value;
}

public VFC_GetStatusFIPController(ApexPages.StandardSetController controller) {
	System.debug('Start VFC_GetStatusFIPController----: ');
	isNoSelectedOpp = false;
	isTestRunnig = false;
	this.fipId = ApexPages.currentPage().getParameters().get('id');
	stdSetController = controller;
}

public pageReference  init(){
	
	if(stdSetController.getRecords().Size()>0 && isTestRunnig == false){
		String result = ConnectiveApiManager.updateStatusByFipQuote(fipId);
		if(String.isNotBlank(result) && result == 'OK') {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Rappel envoyé avec succès'));
			return goback();
		}
		else{
			isNoSelectedOpp = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Erreur lors de la récupération du statut, veuillez  vérifier le service Connective'));
		}
		return null;
	}
	else{
		isNoSelectedOpp = true;
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Veuillez d\'abord créer le document!'));
	}
	return null;
}

public pageReference goback()
{
	return stdSetController.cancel();
}

}