public with sharing class FIP_LEXCSVExportController {
public List<String> columnName {get; set;}
public List<sObject> recordList;
public List<FIP_FIP__c> fipList;
public List<FIP_FIP__c> listFIP  {get; set;}
public List<FIP_FIP__c> listFipIds  {get; set;}
public String CSV {get; set;}
public String fileName {get; set;}
public ApexPages.StandardSetController stdSetController;
public Boolean isNoSelectedOpp {get; set;}
private String filterViewId;
private String listName;

public FIP_LEXCSVExportController(ApexPages.StandardSetController controller) {
	this.stdSetController = controller;
	this.filterViewId = stdSetController.getFilterId();
	isNoSelectedOpp = false;
	System.debug('stdController.filterViewId----- : '+filterViewId);

}

public void init(){            

	System.debug('exportFip Start---- selectedRecords size : '+stdSetController.getSelected().Size());
	

	If (stdSetController.getSelected().Size()==0)
	{
       isNoSelectedOpp = true;
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Veuillez sélectionner une FIP avant de procéder !'));
	}else{
		 listFipIds = (List<FIP_FIP__c>) stdSetController.getSelected();
         listFIP = [Select ID, Name FROM FIP_FIP__c WHERE id IN :listFipIds];
        System.debug('listFIP.----- : '+listFIP);
		System.debug('listFipIds.----- : '+listFipIds);
	}

}

public PageReference save(){
    PageReference exportCsvPage;      
    this.listName = [SELECT Id, Name FROM ListView WHERE id = : filterViewId].Name;
	System.debug('stdController.filterViewId.listName----- : '+listName);  

	If (stdSetController.getSelected().Size()>0){
        List<FIP_FIP__c> listFipIds = (List<FIP_FIP__c>) stdSetController.getSelected();
        List<FIP_FIP__c> listFIP = [Select ID, Name FROM FIP_FIP__c WHERE id IN :listFipIds];
        System.debug('listFIP.----- : '+listFIP);
		 System.debug('listFipIds.----- : '+listFipIds);
        
        
            CSV = generateCSV(getFIPList(listFIP));
              System.debug('CSV.----- : '+CSV);
            if(CSV != null){
                updateFIPList(listFIP);
                exportCsvPage = Page.FIP_FipCsvDisplay;
                exportCsvPage.getParameters().put('csv', CSV );               
                exportCsvPage.setRedirect(true); 
                return exportCsvPage;
            }  		
	}
    return stdSetController.cancel();	

}


public List<FIP_FIP__c> getFIPList(List<SObject> sobjs){

	List<FIP_FIP__c> fips = new List<FIP_FIP__c>();
	List<String> fipNames = new List<String>();
	for(Sobject sobj : sobjs) {
		fipNames.add(String.valueOf(sobj.get('Name')));
	}

	fips = [Select ID, Name, FIPType__c, Exutoire__c, PreviousPACNumber__c, ContractRoot__c, AdressCode__c, TECH_Subsidiary_Key__c, AcceptedBy__r.Trigramme__c, PlateformeTransit__c,
	        WasteName__c, WasteNamingCode__c, AnnualQuantity__c, AsbestosType__c, PackagingType__c, UNCode__c, Class__c, PackagingGroup__c,
	        Collector__r.Name, Collector__r.BillingStreet, Collector__r.BillingPostalCode, Collector__r.BillingCity, CreatedBy.FirstName, CreatedBy.LastName, CreatedBy.Phone, CreatedBy.Email, Collector__r.SIRET_Number__c,
	        ProductorBusinessName__c, ProductorCompanyIdentificationSystem__c, ProductorAdress1__c, ProductorAdress2__c, ProductorAdress3__c,ProductorPostalCode__c, ProductorCity__c, ConstructionSiteAdress__c,
	        ChargedBusinessName__c, ChargedCompanyIdentificationSystem__c, ChargedAdress__c, ChargedPostalCode__c, ChargedCity__c, FrenchTaxeID__c,
	        ProductorX3Code__c, ProductorX3AddressCode__c, ChargedX3Code__c, ChargedX3AddressCode__c, AskerCodeX3__c, AskerX3AddressCode__c, ContractNumber__c
	        FROM FIP_FIP__c WHERE NAME in : fipNames ]; //AND State__c = 'Approuvée'

	return fips;
}

public String generateCSV(List<FIP_FIP__c> fips){

	csv = '"FIP_ID";"Type_amiante";"Cap_renouv";"Racine_Contrat";"Code_tiers_X3";"Code_Adresse";"Appelation_usuelle";"Code_CEE";"Quantite_annuelle";"Type_amiante";"Type_condit";"Code_ONU";"Classe";"Groupe_emb";"FACT_raison_soc";"FACT_adresse";"FACT_CP";"FACT_Ville";"FACT_Prenom_Contact";"FACT_Nom_Contact";"FACT_Telephone";"FACT_Siret";"PROD_raison_soc";"PROD_Siret";"PROD_adresse1";"PROD_adresse2";"PROD_adresse3";"PROD_CP";"PROD_Ville";"Adresse_Chantier";"FACT_Id_TVA_FR";"FACT_CODEX3";"FACT_Code_Adresse";"PROD_CODEX3";"PROD_Code_Adresse";"DEM_CodeX3";"DEM_Code_Adresse";"Filiale_Fac";"APPROB_Exploit";"Plateforme";"Exutoire";"Contrat_PLT"';
	csv += '\n';
	for(FIP_FIP__c fip : fips) {

		String tmp = '"'+fip.ID+ '";"';
		tmp += fip.FIPType__c+ '";"';
		tmp += fip.PreviousPACNumber__c+ '";"';
		tmp += fip.ContractRoot__c+ '";"';
		tmp += fip.ChargedX3Code__c+ '";"';
		tmp += fip.AdressCode__c+ '";"';
		tmp += fip.WasteName__c+ '";"';
		if(fip.WasteNamingCode__c.replaceAll('\\s+', '').length() <= 8){
			tmp += fip.WasteNamingCode__c.replaceAll('\\s+', '')+ '";"'; //delete spaces
		}else{
			tmp += fip.WasteNamingCode__c.replaceAll('\\s+', '').substring(0,8)+ '";"'; //delete spaces and keep 8 first characters
		}
		tmp += fip.AnnualQuantity__c+ '";"';
		tmp += fip.AsbestosType__c+ '";"';
		tmp += fip.PackagingType__c+ '";"';
		tmp += fip.UNCode__c+ '";"';
		tmp += fip.Class__c+ '";"';
		tmp += fip.PackagingGroup__c+ '";"';
		tmp += checkReturnLine(fip.Collector__r.Name)+ '";"';
		tmp += checkReturnLine(fip.Collector__r.BillingStreet)+ '";"';
		tmp += fip.Collector__r.BillingPostalCode+ '";"';
		tmp += checkReturnLine(fip.Collector__r.BillingCity)+ '";"';
		tmp += fip.CreatedBy.FirstName+ '";"';
		tmp += fip.CreatedBy.LastName+ '";"';
		tmp += fip.CreatedBy.Phone+ '";"';
		tmp += fip.Collector__r.SIRET_Number__c+ '";"';
		tmp += checkReturnLine(fip.ProductorBusinessName__c)+ '";"';
		tmp += fip.ProductorCompanyIdentificationSystem__c+ '";"';
		tmp += checkReturnLine(fip.ProductorAdress1__c)+ '";"';
		tmp += checkReturnLine(fip.ProductorAdress2__c)+ '";"';
		tmp += checkReturnLine(fip.ProductorAdress3__c)+ '";"';
		tmp += fip.ProductorPostalCode__c+ '";"';
		tmp += fip.ProductorCity__c+ '";"';
		tmp += fip.ConstructionSiteAdress__c+ '";"';
		tmp += fip.FrenchTaxeID__c+ '";"';
		tmp += fip.ChargedX3Code__c+ '";"';
		tmp += fip.ChargedX3AddressCode__c+ '";"';
		tmp += fip.ProductorX3Code__c+ '";"';
		tmp += fip.ProductorX3AddressCode__c+ '";"';
		tmp += fip.AskerCodeX3__c+ '";"';
		tmp += fip.AskerX3AddressCode__c+ '";"';
		tmp += fip.TECH_Subsidiary_Key__c+ '";"';
		tmp += fip.AcceptedBy__r.Trigramme__c+ '";"';
		tmp += fip.PlateformeTransit__c+ '";"';
		tmp += fip.Exutoire__c+ '";"';
		tmp += fip.ContractNumber__c+ '";';
		tmp += '\n';

		csv += tmp;
	}

	return csv;
}

public void updateFIPList(List<FIP_FIP__c> fips){
	List<FIP_FIP__c> fipsToUpdate = new List<FIP_FIP__c>();

	for(FIP_FIP__c fip : fips) {
		fip.Is_Exported__c = true;
		fipsToUpdate.add(fip);
	}

	update fips;
}

public String checkReturnLine(String pStringToManage){
	String result = '';
	if(pStringToManage != null ) {
		if(pStringToManage.contains('\n')) {
			result = pStringToManage.replace('\n', ' ');
		}else if(pStringToManage.contains('\r\n')) {
			result = pStringToManage.replace('\r\n', ' ');
		}else{
			result = pStringToManage;
		}
	}


	return result;

}

}