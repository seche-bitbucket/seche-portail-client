global class sendQuoteCommunityLink {

webservice static String sendQuoteForSignature(Id qId){
	String userTheme = UserInfo.getUiThemeDisplayed();
	String returnValue='Wait';
	Quote q=[SELECT id, Name,ContactId,Opportunity.Salesman__c,Opportunity.OwnerId,Opportunity.Owner.Email,
	         Opportunity.Owner.Name,Email, QuoteCode__c,TECH_UID__c,Opportunity.Filiale__c,Account.Name FROM Quote WHERE Id=:qId];
	RecordType rt=[SELECT Id, Name, DeveloperName, Description FROM RecordType Where DeveloperName='Devis_approuv_Read_Only' LIMIT 1];
	List<Contact> contact=[SELECT ID,Name,FirstName,LastName,Email,Phone From Contact WHERE Id=:q.ContactId];
	List<DocumentsForSignature__c> document = new List<DocumentsForSignature__c>();
	document = [SELECT Id, Status__c, FlowId__c FROM DocumentsForSignature__c WHERE TECH_ExternalId__c= :qId Limit 1];

    List<Attachment> att=[SELECT id, Name, body, ContentType,ParentId FROM Attachment WHERE ParentId=:q.Id];
    List<ContentDocumentLink> files  = [SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLink where LinkedEntityId =:q.Id ];	
    
	if(att !=null && att.size()>0) { //Salesforce Classic UI running	
		if(att.size()>0 && contact.size()>0) {
			if(att.size()<2) {
				if(document.isEmpty()==false) {
					returnValue='KO';
				}else{
					if(q.TECH_UID__c!=null) {
						returnvalue='KO';
					}else{
						q.Tech_UID__c=GuidUtil.NewGuid();
						q.TECH_isApprouved__c=true;
						q.Status='Envoyé pour signature';
						q.RecordTypeId=rt.Id;
						update q;
						User u=[SELECT Id, Name,Signature,email FROM User WHERE Id = :q.Opportunity.Salesman__c];
						User Assistante=[SELECT Id, Name,email FROM User WHERE Id = :UserInfo.getUserId()];
						String userSignature;
						if(u.Signature!=Null) {
							userSignature ='<pre>'+u.Signature+'</pre>';
						}else{
							userSignature=' ';
						}
						Integer NextYear = System.today().year()+1;
						String userName=u.Name;
						if(q.Name.Contains('Convention')) {
							String contactGreeting ='Cher client,<br><br>';
							String CommercialText= u.Name+' vous prie de trouver ci-joint votre convention '+NextYear+'. ';
							String ConnectiveText='<br><br>'+q.Opportunity.Filiale__c+' vous invite, via Connective, à signer votre convention en cliquant sur le lien ci-dessous :<br>';
							String link='<a href="'+label.CommunityORGURL+'/SignatureDevis/s/sendsignatureform?UID='+q.TECH_UID__c+'&ID='+q.Id+'">';
							String secheConnect='<br>Ce lien vous redirige sur un formulaire Séché Connect pré-rempli avec vos éléments, si toutefois vous n\'êtes pas le signataire du devis vous pouvez remplacer vos coordonnées par celles dudit signataire.<br><br> Une fois ce formulaire rempli, vous recevrez un mail de la part de "no-reply@connective.eu", service de signature électronique sécurisée choisi par Séché Environnement dans le cadre de son programme de transformation digitale.';
							String LinkBody='Portail Signature de la Convention '+NextYear+' - '+q.QuoteCode__c+' </a>'+'<br>';
							String notContact='<br><br><i> Séché Environnement collecte et traite des données à caractère personnel dans le cadre de la gestion des contrats et des obligations légales et réglementaires s’y rattachant. Elles sont uniquement destinées à nos services internes et à nos filiales habilitées. Conformément au Règlement Général sur la Protection des Données, le Groupe met à disposition des droits d’accès, de rectification et d’opposition sur les données à caractère personnel. Il est possible d\'exercer ces droits en contactant notre Délégué à la Protection des Données par courrier électronique adressé à <a href="dp@groupe-seche.com">dp@groupe-seche.com</a> , qui traitera votre demande. Pour plus de détails sur l’engagement du Groupe :  <a href="https://www.groupe-seche.com/fr/mentions-legales">RGPD</a>';
							String greeting= '<br><br>Cordialement,';
							Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
							message.toAddresses = new String[] { contact[0].Email };
							message.ccaddresses=new String[] { Assistante.Email };
							message.bccaddresses=new String[] {u.Email};
							message.subject = 'Convention '+NextYear+' - '+q.Opportunity.Filiale__c+' - '+q.Account.Name;
							message.setReplyTo(u.Email);
							message.setSenderDisplayName(u.Name);
							message.htmlBody = contactGreeting+CommercialText+ConnectiveText+link+LinkBody+secheConnect+greeting+userSignature + notContact;
							Messaging.SingleEmailMessage[] EmailsToSend = new Messaging.SingleEmailMessage[] { message };
							// Add to attachment file list
							Messaging.Emailfileattachment fileAttachment = new Messaging.Emailfileattachment();
							fileAttachment.setFileName(att[0].Name);
							fileAttachment.setBody(att[0].Body);
							message.setFileAttachments(new Messaging.Emailfileattachment[] { fileAttachment });
							Messaging.sendEmail(EmailsToSend);
						}else{
							String contactGreeting ='Cher client,<br><br>';
							String CommercialText= u.Name+' vous prie de trouver ci-joint votre devis. ';
							String ConnectiveText='<br><br>'+q.Opportunity.Filiale__c+' vous invite, via Connective, à signer votre devis en cliquant sur le lien ci-dessous :<br>';
							String link='<a href="'+label.CommunityORGURL+'/SignatureDevis/s/sendsignatureform?UID='+q.TECH_UID__c+'&ID='+q.Id+'">';
							String LinkBody='Portail Signature du devis  - '+q.QuoteCode__c+' </a>'+'<br>';
							String secheConnect='<br>Ce lien vous redirige sur un formulaire Séché Connect pré-rempli avec vos éléments, si toutefois vous n\'êtes pas le signataire du devis vous pouvez remplacer vos coordonnées par celles dudit signataire.<br><br> Une fois ce formulaire rempli, vous recevrez un mail de la part de "no-reply@connective.eu", service de signature électronique sécurisée choisi par Séché Environnement dans le cadre de son programme de transformation digitale.';
							String greeting= '<br><br>Cordialement,';
							String notContact='<br><br><i> Séché Environnement collecte et traite des données à caractère personnel dans le cadre de la gestion des contrats et des obligations légales et réglementaires s’y rattachant. Elles sont uniquement destinées à nos services internes et à nos filiales habilitées. Conformément au Règlement Général sur la Protection des Données, le Groupe met à disposition des droits d’accès, de rectification et d’opposition sur les données à caractère personnel. Il est possible d\'exercer ces droits en contactant notre Délégué à la Protection des Données par courrier électronique adressé à <a href="dp@groupe-seche.com">dp@groupe-seche.com</a> , qui traitera votre demande. Pour plus de détails sur l’engagement du Groupe :  <a href="https://www.groupe-seche.com/fr/mentions-legales">RGPD</a>';
							Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
							message.toAddresses = new String[] { contact[0].Email };
							message.ccaddresses=new String[] { Assistante.Email };
							message.bccaddresses=new String[] {u.Email};
							message.subject = 'Devis '+System.today().year()+' - '+q.Opportunity.Filiale__c+' - '+q.Account.Name;
							message.setReplyTo(u.Email);
							message.setSenderDisplayName(u.Name);
							message.htmlBody = contactGreeting+CommercialText+ConnectiveText+link+LinkBody+secheConnect+greeting+userSignature + notContact;
							Messaging.SingleEmailMessage[] EmailsToSend = new Messaging.SingleEmailMessage[] { message };
							// Add to attachment file list
							Messaging.Emailfileattachment fileAttachment = new Messaging.Emailfileattachment();
							fileAttachment.setFileName(att[0].Name);
							fileAttachment.setBody(att[0].Body);
							message.setFileAttachments(new Messaging.Emailfileattachment[] { fileAttachment });
							Messaging.sendEmail(EmailsToSend);
						}
						returnValue='OK';
					}
				}
			}else{
				returnValue='TooManyAtts';
			}
		}
	}else if(files != null && files.size()>0){//Salesforce Lightning UI running
		
		if(files.size()>0 && contact.size()>0) {
			if(files.size()<2) {
				if(document.isEmpty()==false) {
					returnValue='KO';
				}else{
					if(q.TECH_UID__c!=null) {
						returnvalue='KO';
					}else{
						q.Tech_UID__c=GuidUtil.NewGuid();
						q.TECH_isApprouved__c=true;
						q.Status='Envoyé pour signature';
						q.RecordTypeId=rt.Id;
						update q;
						User u=[SELECT Id, Name,Signature,email FROM User WHERE Id = :q.Opportunity.Salesman__c];
						User Assistante=[SELECT Id, Name,email FROM User WHERE Id = :UserInfo.getUserId()];
						String userSignature;
						if(u.Signature!=Null) {
							userSignature ='<pre>'+u.Signature+'</pre>';
						}else{
							userSignature=' ';
						}
						Integer NextYear = System.today().year()+1;
						String userName=u.Name;
						ContentVersion cversion =  [SELECT title, PathOnClient, FileType, versiondata, FileExtension FROM contentversion WHERE ContentDocumentId =: files[0].ContentDocumentId];                        
						if(q.Name.Contains('Convention')) {
							String contactGreeting ='Cher client,<br><br>';
							String CommercialText= u.Name+' vous prie de trouver ci-joint votre convention '+NextYear+'. ';
							String ConnectiveText='<br><br>'+q.Opportunity.Filiale__c+' vous invite, via Connective, à signer votre convention en cliquant sur le lien ci-dessous :<br>';
							String link='<a href="'+label.CommunityORGURL+'/SignatureDevis/s/sendsignatureform?UID='+q.TECH_UID__c+'&ID='+q.Id+'">';
							String secheConnect='<br>Ce lien vous redirige sur un formulaire Séché Connect pré-rempli avec vos éléments, si toutefois vous n\'êtes pas le signataire du devis vous pouvez remplacer vos coordonnées par celles dudit signataire.<br><br> Une fois ce formulaire rempli, vous recevrez un mail de la part de "no-reply@connective.eu", service de signature électronique sécurisée choisi par Séché Environnement dans le cadre de son programme de transformation digitale.';
							String LinkBody='Portail Signature de la Convention '+NextYear+' - '+q.QuoteCode__c+' </a>'+'<br>';
							String notContact='<br><br><i> Séché Environnement collecte et traite des données à caractère personnel dans le cadre de la gestion des contrats et des obligations légales et réglementaires s’y rattachant. Elles sont uniquement destinées à nos services internes et à nos filiales habilitées. Conformément au Règlement Général sur la Protection des Données, le Groupe met à disposition des droits d’accès, de rectification et d’opposition sur les données à caractère personnel. Il est possible d\'exercer ces droits en contactant notre Délégué à la Protection des Données par courrier électronique adressé à <a href="dp@groupe-seche.com">dp@groupe-seche.com</a> , qui traitera votre demande. Pour plus de détails sur l’engagement du Groupe :  <a href="https://www.groupe-seche.com/fr/mentions-legales">RGPD</a>';
							String greeting= '<br><br>Cordialement,';
							Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
							message.toAddresses = new String[] { contact[0].Email };
							message.ccaddresses=new String[] { Assistante.Email };
							message.bccaddresses=new String[] {u.Email};
							message.subject = 'Convention '+NextYear+' - '+q.Opportunity.Filiale__c+' - '+q.Account.Name;
							message.setReplyTo(u.Email);
							message.setSenderDisplayName(u.Name);
							message.htmlBody = contactGreeting+CommercialText+ConnectiveText+link+LinkBody+secheConnect+greeting+userSignature + notContact;
							Messaging.SingleEmailMessage[] EmailsToSend = new Messaging.SingleEmailMessage[] { message };
							// Add to attachment file list
							Messaging.Emailfileattachment fileAttachment = new Messaging.Emailfileattachment();
							fileAttachment.setFileName(cversion.title+'.'+cversion.FileExtension);
							fileAttachment.setBody(cversion.versiondata);
							message.setFileAttachments(new Messaging.Emailfileattachment[] { fileAttachment });
							Messaging.sendEmail(EmailsToSend);
						}else{
							String contactGreeting ='Cher client,<br><br>';
							String CommercialText= u.Name+' vous prie de trouver ci-joint votre devis. ';
							String ConnectiveText='<br><br>'+q.Opportunity.Filiale__c+' vous invite, via Connective, à signer votre devis en cliquant sur le lien ci-dessous :<br>';
							String link='<a href="'+label.CommunityORGURL+'/SignatureDevis/s/sendsignatureform?UID='+q.TECH_UID__c+'&ID='+q.Id+'">';
							String LinkBody='Portail Signature du devis  - '+q.QuoteCode__c+' </a>'+'<br>';
							String secheConnect='<br>Ce lien vous redirige sur un formulaire Séché Connect pré-rempli avec vos éléments, si toutefois vous n\'êtes pas le signataire du devis vous pouvez remplacer vos coordonnées par celles dudit signataire.<br><br> Une fois ce formulaire rempli, vous recevrez un mail de la part de "no-reply@connective.eu", service de signature électronique sécurisée choisi par Séché Environnement dans le cadre de son programme de transformation digitale.';
							String greeting= '<br><br>Cordialement,';
							String notContact='<br><br><i> Séché Environnement collecte et traite des données à caractère personnel dans le cadre de la gestion des contrats et des obligations légales et réglementaires s’y rattachant. Elles sont uniquement destinées à nos services internes et à nos filiales habilitées. Conformément au Règlement Général sur la Protection des Données, le Groupe met à disposition des droits d’accès, de rectification et d’opposition sur les données à caractère personnel. Il est possible d\'exercer ces droits en contactant notre Délégué à la Protection des Données par courrier électronique adressé à <a href="dp@groupe-seche.com">dp@groupe-seche.com</a> , qui traitera votre demande. Pour plus de détails sur l’engagement du Groupe :  <a href="https://www.groupe-seche.com/fr/mentions-legales">RGPD</a>';
							Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
							message.toAddresses = new String[] { contact[0].Email };
							message.ccaddresses=new String[] { Assistante.Email };
							message.bccaddresses=new String[] {u.Email};
							message.subject = 'Devis '+NextYear+' - '+q.Opportunity.Filiale__c+' - '+q.Account.Name;
							message.setReplyTo(u.Email);
							message.setSenderDisplayName(u.Name);
							message.htmlBody = contactGreeting+CommercialText+ConnectiveText+link+LinkBody+secheConnect+greeting+userSignature + notContact;
							Messaging.SingleEmailMessage[] EmailsToSend = new Messaging.SingleEmailMessage[] { message };
							// Add to attachment file list
							Messaging.Emailfileattachment fileAttachment = new Messaging.Emailfileattachment();
							fileAttachment.setFileName(cversion.title+'.'+cversion.FileExtension);
							fileAttachment.setBody(cversion.versiondata);
							message.setFileAttachments(new Messaging.Emailfileattachment[] { fileAttachment });
							Messaging.sendEmail(EmailsToSend);
						}
						returnValue='OK';
					}
				}
			}else{
				returnValue='TooManyAtts';
			}
		}
	}

	return returnValue;
}

public static String requestSendQuoteForSignature(Quote quot){
	return sendQuoteForSignature(quot.Id);
}
}