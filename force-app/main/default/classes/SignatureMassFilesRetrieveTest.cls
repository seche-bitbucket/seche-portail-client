@isTest
public with sharing class SignatureMassFilesRetrieveTest {
   static Quote q;
    static Attachment Att;
    static Attachment Att2;
    static FIP_FIP__c fip;
    static DocumentsForSignature__c doc;
    static DocumentsForSignature__c doc2;
    
       public static void initAttachement(){
        Account a;
        Contact c;
        Opportunity opp;
        
        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        q = new Quote();
        //q.id='0Q09E0000000AhESAU';
        q.OpportunityId = opp.ID;
        q.Name = 'Convention 2020 '+opp.Name;
        q.DateDevis__c = Date.today();
        q.QuoteCode__c=opp.Name;
        q.ContactId = opp.ContactName__c;
        q.Email = opp.ContactName__r.Email;
        q.Phone = opp.ContactName__r.Phone;
        q.Fax = opp.ContactName__r.Fax;
        q.BillingStreet = opp.Account.BillingStreet;
        q.BillingCity = opp.Account.BillingCity;
        q.BillingState = opp.Account.BillingState;
        q.BillingPostalCode = opp.Account.BillingPostalCode;
        q.BillingCountry = opp.Account.BillingCountry; 
        insert q;
        
        att = new Attachment();
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        att.body=bodyBlob;
        att.Name = q.Name;	
        att.ParentId = q.Id;             
        insert att;    
        fip = new FIP_FIP__c();
        fip.Collector__c = a.ID;
        fip.Contact__c = c.ID;
        insert fip;       
  
    }
   static testMethod void myTestMethod() {      
        initAttachement();
        doc=new DocumentsForSignature__c();
        doc.Name=q.Name+'-  Signée.pdf';
        doc.FlowId__c='a02c9f7a-cfd0-46d2-8ba3-465a5bd79fce';
        doc.Quote__c=q.id;
        doc.TECH_ExternalId__c=q.id;
        doc.Status__c ='SIGNED';
        insert doc;       
        
        string pbefore = 'Testing base 64 encode';            
        Blob beforeblob = Blob.valueOf(pbefore);
        
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId= doc.TECH_ExternalId__c;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;

         test.starttest();
         Test.setMock(HttpCalloutMock.class, new WS002_ConnectiveMock());
        //WS002_ConnectiveMock.externalId=q.id;
        WS002_ConnectiveMock.status='SIGNED';
         SignatureMassFilesRetrieve myBatch = new SignatureMassFilesRetrieve ();   
         Id batchId = Database.executeBatch(myBatch); 
         test.stopTest();
    }
}