@isTest(SeeAllData=false)
public with sharing class LWC_CustomBannerCtrlTest {

    @TestSetup
    static void makeData(){

        Account acc = new Account(Name = 'Test Fact Acc');
        insert acc;

        Account acc_DDE = new Account(Name='SIREDOM');
        upsert acc_DDE;

        acc.ParentId = acc_DDE.Id;
        update acc;

        WasteCollectRequest__c dde = new WasteCollectRequest__c(RequestType__c='Classique',
                                                                Date_Enlevement__c=Date.today(),
                                                                CallDate__c=Date.today(),
                                                                Account__c=acc.Id);

        insert dde;

        Product2 proddef = new Product2(Name='Autre Produit',TECH_ExternalID__c='DEFAUT');
        insert proddef;
        
        Product2 prod = new Product2(Name='Test prod',TECH_ExternalID__c='TST1');
        insert prod;
        
        PriceBook2 pcb_classic = new PriceBook2(Name='Classique', isActive=true);
        insert pcb_classic;
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        
        PricebookEntry pbe_std = new PricebookEntry(UnitPrice=0,Product2Id=prod.id, PriceBook2Id=standardPricebook.Id);
        insert pbe_std;
        
        PricebookEntry pbe = new PricebookEntry(UnitPrice=0,Product2Id=prod.id, PriceBook2Id=pcb_classic.Id);
        insert pbe;

    }

    @isTest
    public static void testCase() {

        try {
            
            Id recortypeid =  [Select Name, Id From RecordType 
                  where DeveloperName='CASE_RT_WasteCollectRequest'].Id;

            Id accId = [SELECT Id FROM Account LIMIT 1].Id;
            Id ddeId = [SELECT Id FROM WasteCollectRequest__c LIMIT 1].id;

            Case rqst = new Case(Subject='TEST POLYMORPHIC',
                                 RecordTypeId=recortypeid,
                                    AccountId=accId,
                                    WasteCollectRequest__c=ddeId);

            System.debug(rqst);

            Test.startTest();

            Id rqstId = (Id)LWC_CustomBannerCtrl.customInsert('Case', JSON.serialize(rqst), 'Case');

            System.debug(rqstId);

            System.assertNotEquals(null, rqstId);

            System.assertEquals('Case', LWC_CustomBannerCtrl.getObjectApiName(rqstId));

            System.assertNotEquals(null, LWC_CustomBannerCtrl.getRecordTypeId('Requête Demande enlèvement'));

            System.assertNotEquals(null, LWC_CustomBannerCtrl.getRecordTypeId(null));

            System.assertNotEquals(null, LWC_CustomBannerCtrl.getUrl());

            System.assertEquals(null, LWC_CustomBannerCtrl.initInstance(null));

            Map<String, Object> err = (Map<String, Object>)LWC_CustomBannerCtrl.customInsert('Case', JSON.serialize(rqst), null);

            System.assertEquals(true, err.containsKey('message'));

            Object initObj = LWC_CustomBannerCtrl.init(rqstId, rqstId);

            System.assertEquals(null, initObj);

            Object save = LWC_CustomBannerCtrl.save(rqstId, JSON.serialize(new List<Case>{rqst}), 'Case');

            System.assertEquals(true, save instanceof Case);

            Map<String, Object> save_err = (Map<String, Object>)LWC_CustomBannerCtrl.save(rqstId, JSON.serialize(new List<Case>{rqst}), null);

            System.assertEquals(true, save_err.containsKey('message'));

            Object cstm = LWC_CustomBannerCtrl.custom(rqstId, null, null);

            System.assertEquals(null, cstm);

            List<Case> getrcd = (List<Case>)LWC_CustomBannerCtrl.getRecord(rqstId, rqstId);

            System.assertEquals(rqstId, getrcd[0].get('Id'));

            Map<String, Object> getrcd_err = (Map<String, Object>)LWC_CustomBannerCtrl.getRecord(rqstId, null);

            System.assertEquals(true, getrcd_err.containsKey('message'));

            List<Map<String,String>> pickVal = (List<Map<String,String>>)LWC_CustomBannerCtrl.getPickListValuesIntoList(rqstId, rqstId, 'Status');

            System.assertNotEquals(null, pickVal.size());

            List<Map<String,String>> pickVal2 = (List<Map<String,String>>)LWC_CustomBannerCtrl.getPickListValuesIntoList(rqstId, 'Case', 'Status');

            System.assertNotEquals(null, pickVal2.size());

            Map<String, Object> pickVal_err = (Map<String, Object>)LWC_CustomBannerCtrl.getPickListValuesIntoList(rqstId, rqstId, null);

            System.assertEquals(true, pickVal_err.containsKey('message'));

            List<Map<String, Object>> fields = (List<Map<String, Object>>)LWC_CustomBannerCtrl.getFieldsLabelAndValue(rqstId, rqstId, 'Status');

            System.assertNotEquals(null, fields.size());

            Map<String, Object> fields_err = (Map<String, Object>)LWC_CustomBannerCtrl.getFieldsLabelAndValue(rqstId, rqstId, null);

            System.assertEquals(true, fields_err.containsKey('message'));

            Test.stopTest();

        } catch(Exception e) {
            System.debug(e.getLineNumber()+' '+e.getMessage());
        }

    }
    
    @isTest
    public static void testDDE() {

        WasteCollectRequest__c dde = [SELECT Statut_demande__c, RequestType__c FROM WasteCollectRequest__c LIMIT 1];
        dde.Statut_demande__c = 'Annulé';
        update dde;

        System.debug(dde);

        Test.startTest();

        Object dde_save = LWC_CustomBannerCtrl.customInsert('WasteCollectRequest__c', JSON.serialize(dde), 'WasteCollectRequest__c');

        System.assertEquals(null, dde_save);

        Map<String, Object> initObj = (Map<String, Object>)LWC_CustomBannerCtrl.init(dde.Id, dde.id);

        System.assertEquals(true, initObj.get('disabled'));

        List<WasteCollectRequest__c> getrcd = (List<WasteCollectRequest__c>)LWC_CustomBannerCtrl.getRecord(dde.Id, dde.Id);

        System.assertEquals(dde.id, getrcd[0].get('Id'));

        List<WasteCollectRequest__c> getrcd_err = (List<WasteCollectRequest__c>)LWC_CustomBannerCtrl.getRecord(dde.Id, null);

        System.assertEquals(0, getrcd_err.size());

        String res = LWC_DDECtrl.copyProducts(dde.RequestType__c, dde.Id);

        System.assertEquals('OK', res);

        Map<String, Object> lstWcri_err = (Map<String, Object>)LWC_CustomBannerCtrl.getRecord('WasteCollectRequestItem__c', null);

        System.assertEquals(true, lstWcri_err.containsKey('message'));

        dde.Date_Enlevement__c = Date.today().addDays(-1);
        update dde;

        Map<String, Object> custom_err_date = (Map<String, Object>)LWC_CustomBannerCtrl.custom(dde.id, JSON.serialize(dde), 'submitForApproval');

        System.assertEquals('Motif: la date de cette demande est antérieure à la date d\'aujourd\'hui', (String)custom_err_date.get('message'));

        dde.Date_Enlevement__c = Date.today();
        update dde;

        Map<String, Object> custom_err_data = (Map<String, Object>)LWC_CustomBannerCtrl.custom(dde.id, JSON.serialize(dde), 'submitForApproval');

        System.assertEquals('Veuillez renseigner au moins un déchet avant l\'envoi pour approbation', (String)custom_err_data.get('message'));

        dde.OutOfMarket__c = true;
        update dde;

        Id defaultProd = LWC_DDECtrl.getDefaultProd();
        
        System.debug('defaultProd '+defaultProd);

        WasteCollectRequestItem__c ddeItem = new WasteCollectRequestItem__c(Product__c=defaultProd, Demande_Enlevement__c=dde.Id);
        insert ddeItem;
        
        WasteCollectRequestItem__c ddeItem2 = new WasteCollectRequestItem__c(Product__c=[SELECT Id FROM Product2 WHERE TECH_ExternalID__c = 'TST1'].Id, Demande_Enlevement__c=dde.Id, Quantite__c=1);
        insert ddeItem2;

        List<WasteCollectRequestItem__c> lstWcri = (List<WasteCollectRequestItem__c>)LWC_CustomBannerCtrl.getRecord('WasteCollectRequestItem__c', dde.Id);

        System.assertNotEquals(0, lstWcri.size());

        Map<String, Object> custom_err_data_OFM = (Map<String, Object>)LWC_CustomBannerCtrl.custom(dde.id, JSON.serialize(dde), 'submitForApproval');

        System.assertEquals('Veuillez renseigner au moins un déchet hors marché avant l\'envoi pour approbation', (String)custom_err_data_OFM.get('message'));

        dde.OutOfMarket__c = false;
        update dde;
        
        List<WasteCollectRequestItem__c> ids = new List<WasteCollectRequestItem__c>();

        for(WasteCollectRequestItem__c wcri: lstWcri) {

            ids.add(wcri);
            wcri.Quantite__c = 1;

        }
        
        List<WasteCollectRequestItem__c> lstWcri_save = (List<WasteCollectRequestItem__c>)LWC_CustomBannerCtrl.save('WasteCollectRequestItem__c', JSON.serialize(ids), 'WasteCollectRequestItem__c');

        System.assertNotEquals(0, lstWcri_save.size());

        Map<String, Object> custom_approval_done = (Map<String, Object>)LWC_CustomBannerCtrl.custom(dde.id, JSON.serialize(dde), 'submitForApproval');

        System.assertEquals('La demande d\'enlèvement a bien été envoyée pour approbation', (String)custom_approval_done.get('message'));

        Map<String, Object> custom_cancel = (Map<String, Object>)LWC_CustomBannerCtrl.custom(dde.id, JSON.serialize(dde), 'cancelRequest');

        System.assertEquals('La demande a bien été annulée', (String)custom_cancel.get('message'));
        
        List<id> allids = new List<Id>();
        
        for(WasteCollectRequestItem__c ww: ids) {
            allids.add(ww.id);
        }

        LWC_DDECtrl.deleteAll(String.join(allids, ', '));

        System.assertEquals(0, [SELECT ID FROM WasteCollectRequestItem__c WHERE ID IN :allids].size());

        Test.stopTest();

    }


}