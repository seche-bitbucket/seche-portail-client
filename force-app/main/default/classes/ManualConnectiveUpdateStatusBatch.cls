global with sharing class ManualConnectiveUpdateStatusBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful {
    //get quotes ids
    List<String> quoteIds = getQuoteIds.fecthIds();
    global Database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(
            'SELECT Id, Status__c, FlowId__c,TECH_ExternalId__c,OwnerId,Quote__c,Quote__r.OwnerId , Quote__r.QuoteCode__c, FIP_FIP__r.Name, Package__c, Package__r.ConnectivePackageId__c, Package__r.ConnectivePackageStatus__c'+
            ' FROM DocumentsForSignature__c WHERE Quote__c IN :quoteIds'
        );
    }

    global void execute(Database.BatchableContext BC, List<DocumentsForSignature__c> scope) {
        PackageResponse.ResponseGetListPackage listPkg = ConnectiveApiManager.getBatch50Package(true,null,null);
        Map<PackageResponse.ResponseItemPackage,Boolean> pkgConnectiveProcessed = new Map<PackageResponse.ResponseItemPackage,Boolean>();
        try{
            ConnectiveAsyncUpdateStatus updateStatus = new ConnectiveAsyncUpdateStatus(scope,listPkg);
            ID jobID = System.enqueueJob(updateStatus);
        }
        catch (Exception e){
            System.debug(LoggingLevel.ERROR, e.getMessage());
        }
    }

    global void finish(Database.BatchableContext BC){

        // Get the ID of the AsyncApexJob representing this batch job  
        // from Database.BatchableContext.    
        // Query the AsyncApexJob object to retrieve the current job's information.  

        AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors, JobItemsProcessed,
        TotalJobItems, CreatedBy.Email
        from AsyncApexJob where Id =:BC.getJobId()];   
       
        // Send an email to the Apex job's submitter notifying of job completion.  
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.ccaddresses = new String[] {'sechecrm69@gmail.com'};
            mail.setSubject('ManualConnectiveUpdateStatusBatch ' + a.Status);
            mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +
           ' batches with '+ a.NumberOfErrors + ' failures.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}