@isTest
public with sharing class ScanPollutecTest {
    
    @isTest
    public static void CodeValidationTest() {

        Datetime now = Datetime.now();

        Contacts_Invitation__c inv1 = new Contacts_Invitation__c(GUID__c = '1234-1234');
        Contacts_Invitation__c inv2 = new Contacts_Invitation__c(GUID__c = '5678-5678', Date_Time_Arrival_at_party__c = now);
        List<Contacts_Invitation__c> invList = new List<Contacts_Invitation__c>();
        invList.add(inv1);
        invList.add(inv2);
        insert invList;
        
        String GUIDinv1 = [SELECT Id, GUID__c FROM Contacts_Invitation__c WHERE Id = :inv1.Id].GUID__c;
        String paramInv1 = '{"type":"qr","value":"' + GUIDinv1 + '"}';

        String result1 = ScanPollutec.CodeValidation('{"type":"qr","value":"0000-0000"}');
        String result2 = ScanPollutec.CodeValidation(paramInv1);
        String result3 = ScanPollutec.CodeValidation('{"type":"qr","value":"5678-5678"}');

    }
}