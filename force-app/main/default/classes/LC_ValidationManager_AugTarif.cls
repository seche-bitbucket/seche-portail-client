public with sharing class LC_ValidationManager_AugTarif {
    
    //@AuraEnabled(cacheable=true)
    @AuraEnabled(cacheable=true)
    public static List<Object> fetchContracts() {   
        
        Id profileId = UserInfo.getProfileId();
        String userName = UserInfo.getUserName();
        if(userName.contains('.devcap') ){
            userName = userName.substringBefore('.devcap');
        }else if (userName.contains('.dev01')){
            userName = userName.substringBefore('.dev01');
        }else if (userName.contains('.preprodint')) {
            userName = userName.substringBefore('.preprodint');
        }

		String profileName =[Select Id, Name from Profile where Id=:profileId].Name;
        List<Approbation_user__mdt> Approbation = [SELECT Filiale__c , Filiere__c FROM Approbation_user__mdt  WHERE Approbateur__c =:userName ];
       
        Set<String> filialesList = new Set<String>();
        Set<String> filieresList = new Set<String>();
        for(Approbation_user__mdt a: Approbation) {
            filialesList.add(a.Filiale__c);
            filieresList.add(a.Filiere__c);
        }

        List<String> duplicateKeys = new List<String>();
        List <Contract__c> result = New List <Contract__c>();
        Map<String, Object> mapContractWithoutDuplicate = new Map<String, Object>();  

        Map<Id, Contract__c> mapContract_faf;
        Map<Id, Contract__c> mapContract;
       
        if (profileName=='directeur commercial' ){      

            mapContract = new Map<Id, Contract__c> ([SELECT Id,Name,ContractType__c,Type_de_convention__c,ContractCode__c,Status__c,Filiale__c, Filiere__c, RecordLocked__c,Account__c, Account__r.Name, Account__r.TECH_X3SECHEID__c, Account__r.ChargedX3Code__c, IncreaseRatioNumber__c,
            IncreaseRatio__c,CustomerNature__c,ContactRelated__r.Name,ContactRelated__r.Email,ContactRelated__c,AmountOfAmandements__c,CA_N__c,TreatedContract__c,AddedValue__c,TreatedAmandements__c,Opportunity__r.Name,
            ContractEndDate__c,EstimateTurnoverNextYear__c,RegroupmentIndex__c,AssociatedSalesRep__c,AssociatedSalesRep__r.Name,Face_Face__c, ChangedStatusDate__c, IconStatus__c
            FROM Contract__c WHERE (Status__c = 'Valide' OR Status__c = 'Rejete'  OR Status__c = 'Soumis') AND Face_Face__c = false AND TECH_Excluded__c = false AND IsUnderContract__c = false
            AND Filiale__c IN : filialesList AND Filiere__c IN : filieresList ]); 
            
            mapContract_faf = new Map<Id, Contract__c> ([SELECT Id,Name,ContractType__c,Type_de_convention__c,ContractCode__c,Status__c,Filiale__c, Filiere__c, RecordLocked__c,Account__c, Account__r.Name, Account__r.TECH_X3SECHEID__c, Account__r.ChargedX3Code__c, IncreaseRatioNumber__c,
            IncreaseRatio__c,CustomerNature__c,ContactRelated__r.Name,ContactRelated__r.Email,ContactRelated__c,AmountOfAmandements__c,CA_N__c,TreatedContract__c,AddedValue__c,TreatedAmandements__c,Opportunity__r.Name,
            ContractEndDate__c,EstimateTurnoverNextYear__c,RegroupmentIndex__c,AssociatedSalesRep__c,AssociatedSalesRep__r.Name,Face_Face__c, ChangedStatusDate__c, IconStatus__c
            FROM Contract__c WHERE (Status__c = 'Valide' OR Status__c = 'Rejete'  OR Status__c = 'Soumis') AND Face_Face__c = true AND TECH_Excluded__c = false AND IsUnderContract__c = false
            AND Filiale__c IN : filialesList AND Filiere__c IN : filieresList ]); 

        } else {    

            mapContract = new Map<Id, Contract__c> ([SELECT Id,Name,ContractType__c,Type_de_convention__c,ContractCode__c,Status__c,Filiale__c, Filiere__c, RecordLocked__c,Account__c, Account__r.Name, Account__r.TECH_X3SECHEID__c, Account__r.ChargedX3Code__c, IncreaseRatioNumber__c,
            IncreaseRatio__c,CustomerNature__c,ContactRelated__r.Name,ContactRelated__r.Email,ContactRelated__c,AmountOfAmandements__c,CA_N__c,TreatedContract__c,AddedValue__c,TreatedAmandements__c,Opportunity__r.Name,
            ContractEndDate__c,EstimateTurnoverNextYear__c,RegroupmentIndex__c,AssociatedSalesRep__c,AssociatedSalesRep__r.Name,Face_Face__c, ChangedStatusDate__c, IconStatus__c
            FROM Contract__c WHERE (Status__c = 'Valide' OR Status__c = 'Rejete'  OR Status__c = 'Soumis') AND Face_Face__c = false AND TECH_Excluded__c = false AND IsUnderContract__c = false ]); 
            
            mapContract_faf = new Map<Id, Contract__c> ([SELECT Id,Name,ContractType__c,Type_de_convention__c,ContractCode__c,Status__c,Filiale__c, Filiere__c, RecordLocked__c,Account__c, Account__r.Name, Account__r.TECH_X3SECHEID__c, Account__r.ChargedX3Code__c, IncreaseRatioNumber__c,
            IncreaseRatio__c,CustomerNature__c,ContactRelated__r.Name,ContactRelated__r.Email,ContactRelated__c,AmountOfAmandements__c,CA_N__c,TreatedContract__c,AddedValue__c,TreatedAmandements__c,Opportunity__r.Name,
            ContractEndDate__c,EstimateTurnoverNextYear__c,RegroupmentIndex__c,AssociatedSalesRep__c,AssociatedSalesRep__r.Name,Face_Face__c, ChangedStatusDate__c, IconStatus__c
            FROM Contract__c WHERE (Status__c = 'Valide' OR Status__c = 'Rejete'  OR Status__c = 'Soumis') AND Face_Face__c = true AND TECH_Excluded__c = false AND IsUnderContract__c = false ]); 

        } 

        if(mapContract.size() > 0 && mapContract != null) {

            for(Id cId :mapContract.keySet()){
                
                String status;
                Contract__c cont = mapContract.get(cId);
                String duplicateKey = cont.AssociatedSalesRep__c+'/'+cont.Filiale__c+'/'+cont.filiere__c;

                // if(!duplicateKeys.contains(duplicateKey)){
                //     duplicateKeys.add(duplicateKey);
                //     Object mp = new Map<String, Object>{'standard' => mapContract.get(cId)};
                //     mapContractWithoutDuplicate.put(duplicateKey,mp);
                // }

                if(!duplicateKeys.contains(duplicateKey)){
                    duplicateKeys.add(duplicateKey);
                    Map<String, Object> mp = (Map<String, Object>)mapContractWithoutDuplicate.get(duplicateKey);
                    if(mp == null) {
                        mp = new Map<String, Object>();
                    }

                    Object rcd = (Object)mapContract.get(cId);
                    
                    mp.put('standard', rcd);
                    mapContractWithoutDuplicate.put(duplicateKey, mp);
                } else {

                    Map<String, Object> mp = (Map<String, Object>)mapContractWithoutDuplicate.get(duplicateKey);

                    Contract__c rcdInBase = (Contract__c)mp.get('standard');
                    Contract__c rcd = mapContract.get(cId);

                    System.debug('rcd.Status__c '+ rcd.Status__c);
                    System.debug('rcdInBase.Status__c '+ rcdInBase.Status__c);

                    if(rcdInBase.Status__c != 'Soumis' &&
                        rcd.Status__c == 'Soumis') {
                            System.debug('Soumis condition');
                        mp.put('standard', rcd);
                    } else if(rcdInBase.Status__c != 'Soumis' && 
                            rcd.Status__c == 'Rejete') {
                            System.debug('Rejete condition');
                        mp.put('standard', rcd);
                    } else if(rcdInBase.Status__c != 'Soumis' && rcdInBase.Status__c != 'Rejete' &&
                                rcd.Status__c == 'Valide') {
                            System.debug('valide condition');
                        mp.put('standard', rcd);
                    }

                    Contract__c c = (Contract__c)mp.get('standard');
                    System.debug('c.Status__c '+ c.Status__c);

                    mapContractWithoutDuplicate.put(duplicateKey, mp);

                }

            }

        }

        duplicateKeys = new List<String>();

        if(mapContract_faf.size() > 0 && mapContract_faf != null) {

            for(Id cId :mapContract_faf.keySet()){
                String status;
                Contract__c cont = mapContract_faf.get(cId);
                String duplicateKey = cont.AssociatedSalesRep__c+'/'+cont.Filiale__c+'/'+cont.filiere__c;

                if(!duplicateKeys.contains(duplicateKey)){
                    duplicateKeys.add(duplicateKey);
                    Map<String, Object> mp = (Map<String, Object>)mapContractWithoutDuplicate.get(duplicateKey);
                    if(mp == null) {
                        mp = new Map<String, Object>();
                    }

                    Object rcd = (Object)mapContract_faf.get(cId);
                    
                    mp.put('faf', rcd);
                    mapContractWithoutDuplicate.put(duplicateKey, mp);
                } else {

                    Map<String, Object> mp = (Map<String, Object>)mapContractWithoutDuplicate.get(duplicateKey);

                    Contract__c rcdInBase = (Contract__c)mp.get('faf');
                    Contract__c rcd = mapContract_faf.get(cId);

                    System.debug('rcd.Status__c '+ rcd.Status__c);
                    System.debug('rcdInBase.Status__c '+ rcdInBase.Status__c);

                    if(rcdInBase.Status__c != 'Soumis' &&
                        rcd.Status__c == 'Soumis') {
                            System.debug('Soumis condition');
                        mp.put('faf', rcd);
                    } else if(rcdInBase.Status__c != 'Soumis' && 
                            rcd.Status__c == 'Rejete') {
                            System.debug('Rejete condition');
                        mp.put('faf', rcd);
                    } else if(rcdInBase.Status__c != 'Soumis' && rcdInBase.Status__c != 'Rejete' &&
                                rcd.Status__c == 'Valide') {
                            System.debug('valide condition');
                        mp.put('faf', rcd);
                    }

                    Contract__c c = (Contract__c)mp.get('faf');
                    System.debug('c.Status__c '+ c.Status__c);

                    mapContractWithoutDuplicate.put(duplicateKey, mp);

                }
            }

        }
            
        return  mapContractWithoutDuplicate.values();

    }

    @AuraEnabled(cacheable=true)
    public static List<Contract__c> getAccountIds() {    
        List<Contract__c> mapContract = [SELECT Id,Account__c FROM Contract__c WHERE  Status__c = 'Valide' OR Status__c = 'Rejete'  OR Status__c = 'Soumis'];  
        return  mapContract;
    }

    @AuraEnabled(cacheable=true)
    public static List<String> getReportData(List<String> lstReportName, String associatedSalesRep, String filiale, String filiere, Boolean faf, Boolean sousContrat){
        List<String>  listResult = new List<String> ();  
        if(Test.isRunningTest()){
            listResult = null;
            } else {
                listResult = AP_AugmentationTarifUtils.getReportDataValidation(lstReportName, associatedSalesRep, filiale, filiere, faf, sousContrat);
            }
      
        return listResult;
        
    } 

    @AuraEnabled(cacheable=true)
    public static List<String> getReportData_chart(List<String> lstReportName, String associatedSalesRep, String filiale, String filiere){
        List<String>  listResult = new List<String> ();  
        if(Test.isRunningTest()){
            listResult = null;
            } else {
                listResult.addAll(AP_AugmentationTarifUtils.getReportDataValidation(lstReportName, associatedSalesRep, filiale, filiere, null, null));
            }
      
        return listResult;
        
    } 

    @AuraEnabled(cacheable=true)
    public static List <Contract__c> fetchContractsByAccountIds(String accountIds,String filiale, String filiere) {  
        System.debug('Start-- fetchContractsByAccountIds');
        //Create new list because input list is wiht [array] style
        System.debug('temps accountIds'+ accountIds); 
        List<String> Ids = new List<String>();
        if (accountIds == null || String.isBlank(accountIds)){
            List<Contract__c> contractLs = getAccountIds();
            for (Contract__c contract: contractLs){
               Ids.add(contract.Account__c);
            }
        }
        else if(accountIds.contains(',')){
            Ids = accountIds.split(',');
        } else {
            Ids.add(accountIds);
        }
        System.debug('temps Ids'+ Ids);           
        List <Contract__c>  result=  [SELECT Id,Name,ContractType__c,Type_de_convention__c,ContractCode__c,Status__c,Filiale__c, Filiere__c, RecordLocked__c,Account__c, AssociatedSalesRep__c,AssociatedSalesRep__r.Name,Face_Face__c, ChangedStatusDate__c, IconStatus__c,IncreaseComment__c
        FROM Contract__c  WHERE Account__c IN :Ids  AND Filiale__c =:filiale AND filiere__c = :filiere ];
        return result;
    }

     // Approve the contracts and amendments , by changing the status to valide 
     @AuraEnabled
     public static String validate(String associatedSalesRep, String filiale, String filiere, Boolean faf, List<String> accountsIds){
        System.debug(accountsIds);
        List<Contract__c> contracts = new List<Contract__c>();
        if(faf){
            contracts = [SELECT Id, Name, AssociatedSalesRep__c, Account__c,Account__r.Name,RecordLocked__c, Filiale__c, Filiere__c, Status__c FROM Contract__c WHERE AssociatedSalesRep__c = :associatedSalesRep AND Filiale__c = :filiale AND Filiere__c = :filiere AND status__c='Soumis' AND Face_Face__c=true AND Account__c IN :accountsIds Order By Name]; 
        }else{
            contracts = [SELECT Id, Name, AssociatedSalesRep__c, Account__c,Account__r.Name,RecordLocked__c, Filiale__c, Filiere__c, Status__c FROM Contract__c WHERE AssociatedSalesRep__c = :associatedSalesRep AND Filiale__c = :filiale AND Filiere__c = :filiere AND status__c='Soumis' AND Face_Face__c=false Order By Name]; 
        }
        
        
        Set<String> listAcount = new Set<String>();

        //STATUT PORTEFEUILLE FAF
        String status = 'Valide';
        try{
            for(contract__c c: contracts ){
                c.status__c='Valide';
                listAcount.add('<li>'+c.Account__r.Name+'</li>');
                c.ChangedStatusDate__c = System.today();
                System.debug('Contract Valide : '+ c );
            }
           
            // Sending the email to the user who is associated to the contracts
            String userName = UserInfo.getUserName();
            User activeUser = [Select Email From User where Username = : userName limit 1];
            String userEmail = activeUser.Email;
             //Get SalesResp           
             User salesRespUser = [Select Name, Email From User where Id = : associatedSalesRep limit 1];
             String salesRespEmail = salesRespUser.Email;

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new String[] {salesRespEmail});
            mail.setCcAddresses(new String[] {userEmail});
            mail.setReplyTo(userEmail);
            mail.setSenderDisplayName('Salesforce Augmentation Tarifaire');
            mail.setSubject('Augmentations tarifaires validées');
            String Nextyear = System.Label.YearN1;
            // Integer Nextyear = 2021;
            if(!faf) {
                mail.setHtmlBody('Bonjour '+salesRespUser.Name+
                ', <br/><br/>Votre demande d\'augmentations tarifaires '+Nextyear
                +' a été validée pour les comptes que vous gérez sur '+filiale+' '+filiere
                +'. <br/><br/>Les opportunités et conventions associées viennent d’être créées.'
                +'<br/>Vous pourrez ainsi poursuivre le process d’augmentation par l’envoi des conventions.'
                +'<br/><br/>Cordialement, <br/><br/>'+UserInfo.getName());
            } else {
                mail.setHtmlBody('Bonjour '+salesRespUser.Name+
                ', <br/><br/>Votre demande d\'augmentations tarifaires '+Nextyear
                +' a été validée pour les comptes face à face que vous gérez sur '+filiale+' '+filiere
                +'. Il s\'agit des comptes :'
                +'<br><ul>'+String.join(new List<String>(listAcount), '')+'</ul>'
                +'<br/>Les opportunités et conventions associées viennent d’être créées.'
                +'<br/>Vous pourrez ainsi poursuivre le process d’augmentation par l’envoi des conventions.'
                +'<br/><br/>Cordialement, <br/><br/>'+UserInfo.getName());
            }
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }catch(Exception E){
            status ='ko';
            System.debug(E);
        }
        update contracts;
        
        //STATUT PORTEFEUILLE FAF
        Set<String> listStatus = new Set<String>();
        List<Contract__c> listcontracts = new List<Contract__c>();
        listcontracts = [SELECT Status__c FROM Contract__c WHERE AssociatedSalesRep__c = :associatedSalesRep AND Filiale__c = :filiale AND Filiere__c = :filiere  AND Face_Face__c=true]; 
        for(contract__c c: listcontracts ){
            listStatus.add(c.status__c);
        }

        System.debug('listStatus : '+ listStatus );
        if(listStatus.contains('Soumis')){
            status = 'Soumis';
        }else if(listStatus.contains('Rejete')){
            status = 'Rejete';
        }
       System.debug('Status : '+ status );

        return status;
    }

    @AuraEnabled
    public static List<Contract__c> getStatusById(List<Id> ids){
        List<Contract__c> contrats;
        try {
            contrats = [select Status__c, Face_Face__c from Contract__c where Id in :ids];
            System.debug(contrats);
        } catch (Exception e) {
            System.debug(e);
            contrats = null;
        }
        return contrats;
    }

     // refuse the contracts and send them back to be rechanged 
     @AuraEnabled
     public static String reject(String associatedSalesRep, String filiale, String filiere, List<List<String>> comments, Boolean faf, List<String> accountsIds){
        System.debug('Start ---> reject');
        System.debug(accountsIds);
        Boolean valide=false;
        List<Contract__c> contracts = new List<Contract__c>();
        if(faf){
            contracts = [SELECT Id, Name, AssociatedSalesRep__c, Account__c,RecordLocked__c, Filiale__c, Filiere__c, Status__c FROM Contract__c WHERE AssociatedSalesRep__c = :associatedSalesRep AND Filiale__c = :filiale AND Filiere__c = :filiere AND status__c='Soumis' AND Face_Face__c=true AND Account__c IN :accountsIds Order By Name]; 
        }else{
            contracts = [SELECT Id, Name, AssociatedSalesRep__c, Account__c,RecordLocked__c, Filiale__c, Filiere__c, Status__c FROM Contract__c WHERE AssociatedSalesRep__c = :associatedSalesRep AND Filiale__c = :filiale AND Filiere__c = :filiere AND status__c='Soumis' AND Face_Face__c=false Order By Name]; 
        }
         //STATUT PORTEFEUILLE FAF
        String status = 'Valide';
        for(Contract__c c:contracts){
            if(c.Status__c=='Valide'){
                valide=true;
            }
        }
        if(!valide){
            try{
                for(contract__c c:contracts ){
                    c.status__c='Rejete';
                    c.RecordLocked__c=false;
                    c.ChangedStatusDate__c = System.today();
                    c.IncreaseComment__c = null;
                }                
                String userName = UserInfo.getUserName();
                User activeUser = [Select Email From User where Username = : userName limit 1];
                String userEmail = activeUser.Email;
                 //Get SalesResp           
                User salesRespUser = [Select Name, Email From User where Id = : associatedSalesRep limit 1];
                //get comments
                String commentsToSend ='';
                for(List<String> com : comments){  
                    if(com[1] != null && !com[1].contains('undefined')) {
                        commentsToSend += '<br/> <b>- Compte '+com[0]+'</b> > '+com[1];
                    }           
                    
                }
 
                System.debug('Comment reject : '+commentsToSend);
              
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new String[] {salesRespUser.Email});
                mail.setCcAddresses(new String[] {userEmail});
                mail.setReplyTo(userEmail);
                mail.setSenderDisplayName('Salesforce Augmentation Tarifaire');
                mail.setSubject('Augmentations tarifaires refusées');
                String Nextyear = System.Label.YearN1;
                // Integer Nextyear = 2021;
                mail.setHtmlBody('Bonjour '+salesRespUser.Name+
                ', <br/><br/>Votre demande d\'augmentations tarifaires '+Nextyear
                +' a été rejetée sur '+filiale+' '+filiere
                +'. <br/><br/>Veuillez prendre connaissance des commentaires qui ont amené ce refus :'+commentsToSend
                +'<br/><br/> Cordialement, <br/><br/>'+UserInfo.getName());
                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

            }catch(Exception E){
                status='ko';
                System.debug(E);
            }
           
        } 
        update contracts;
        
        //STATUT PORTEFEUILLE FAF
        Set<String> listStatus = new Set<String>();
        List<Contract__c> listcontracts = new List<Contract__c>();
        listcontracts = [SELECT Status__c FROM Contract__c WHERE AssociatedSalesRep__c = :associatedSalesRep AND Filiale__c = :filiale AND Filiere__c = :filiere  AND Face_Face__c=true]; 
        for(contract__c c: listcontracts ){
            listStatus.add(c.status__c);
        }
        System.debug('listStatus : '+ listStatus );
        if(listStatus.contains('Soumis')){
            status = 'Soumis';
        }else if(listStatus.contains('Rejete')){
            status = 'Rejete';
        }
       System.debug('Status : '+ status );

        return status;
    }

    @AuraEnabled
    public static Id getAccountId(String name){
        return [Select Id, Name from Account Where Name=:name][0].Id;
    }

    // Approve the contracts and amendments , by changing the status to valide 
    @AuraEnabled
    public static String getStatusFaf(String associatedSalesRep, String filiale, String filiere){
       //STATUT PORTEFEUILLE FAF
       String status = 'Valide';
       Set<String> listStatus = new Set<String>();
       List<Contract__c> listcontracts = new List<Contract__c>();
       listcontracts = [SELECT Status__c FROM Contract__c WHERE AssociatedSalesRep__c = :associatedSalesRep AND Filiale__c = :filiale AND Filiere__c = :filiere  AND Face_Face__c=true]; 
       for(contract__c c: listcontracts ){
           listStatus.add(c.status__c);
       }
       System.debug('listStatus : '+ listStatus );
       if(listStatus.contains('Soumis')){
           status = 'Soumis';
       }else if(listStatus.contains('Rejete')){
           status = 'Rejete';
       }
      System.debug('Status : '+ status );

       return status;
    }

}