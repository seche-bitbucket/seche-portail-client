/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ConnectiveAsyncSendReminder
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ConnectiveAsyncSendReminder
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 17-08-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
public with sharing class ConnectiveV4AsyncSendReminder implements Queueable, Database.AllowsCallouts  {

    Map<Id,Quote> scope = new Map<Id,Quote>();
    List<Quote> scopeSendEmail = new List<Quote>();

    public ConnectiveV4AsyncSendReminder(List<Quote> scope, List<Quote> allScope){
        this.scope=new Map<Id,Quote>(scope);
        this.scopeSendEmail = allScope;
    }

    public void execute(QueueableContext context) {
        Id currentId;
        //Integer NextYear = System.today().year()+1;
        String Nextyear = System.Label.YearN1;
        try{
            for(Id qtId : scope.keySet()){
                currentId= qtId;
                String result = ConnectiveV4ApiManager.sendRedminder(qtId);
                scope.remove(qtId);
                
                break;
            }
        }
        catch (Exception e){
            System.debug(LoggingLevel.ERROR, e.getMessage());
            if(currentId != null){
                this.scope.remove(currentId);
            }
        }
        finally{
            if(!Test.isRunningTest() && scope.size()>0){
                ID jobDoc = System.enqueueJob(new ConnectiveV4AsyncSendReminder(this.scope.values(),scopeSendEmail));
            }
        }
    }
}