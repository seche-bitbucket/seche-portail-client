public Inherited sharing class LC_EventController {
@AuraEnabled
public String message {get; set;}
@AuraEnabled
public String showToastMode {get; set;}
@AuraEnabled
public VisitReport__c objectVisitReport {get; set;}


@AuraEnabled
public static Event getEvent(Id eventId){
     System.debug('getEvent-----Start- parm: event ID : '+eventId);
	//make your own SOQL here from your desired object where you want to place your lightnign action
	return [SELECT Id, Description, Compte_Rendu__c, Subject, Compte__c, StartDateTime, WhatId, What.Type, WhoId, Who.Type From Event Where Id =: eventId];
}


@AuraEnabled
public static String generateVisitReport(Event evt){
	String result =GenerateVisitReportFromEvent.requestVisitReport(evt.Id); 
	LC_EventController wrapper = new LC_EventController();
	if(!result.contains('error')){
		 wrapper.objectVisitReport = [SELECT ID FROM VisitReport__c WHERE Id = :result];
	} else if (result.equals('error1')){
		wrapper.message = 'L\'évènement est déjà lié à un compte rendu';
        wrapper.showToastMode = 'warning';
		return 'error1';
	}else if (result.equals('error2')){
		wrapper.message = 'L\'évènement doit être lié à un compte et un contact pour générer un CR';
        wrapper.showToastMode = 'warning';
		return 'error2';
	}
	return result;
}
}