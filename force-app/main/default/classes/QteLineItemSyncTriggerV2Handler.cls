public class QteLineItemSyncTriggerV2Handler {
    
    private static Boolean stop = false;
    
    public static void stop() {
        stop = true;
    }
    
    public static void unstop() {
        stop = false;
    }
    
    Public static void execute(List<QuoteLineItem> newLst, Map<Id, QuoteLineItem> oldMap) {
        if(!stop) {
            SyncHandler.SyncHandler(newLst, oldMap, 'QuoteLineItem');
        }
    }
    
    Public static void copy(List<QuoteLineItem> lst) {

        if(!stop) {
        
            List<ID> idsOLI = new List<ID>();
            
            for(QuoteLineItem qli: lst) {
                /*if(qli.OpportunityLineItemId != null)*/ idsOLI.add(qli.OpportunityLineItemId);
            }

            List<QuoteLineItem_Field__c> lstQuoteLineFields = [SELECT Name,OppLineSyncField__c FROM QuoteLineItem_Field__c];
            String fields;
            for(QuoteLineItem_Field__c qlif: lstQuoteLineFields) {
                if(fields == null) {
                    fields=qlif.Name;
                }else{
                    fields=fields+', '+qlif.Name;
                }
            }
            String query = 'SELECT '+fields+' FROM OpportunityLineItem WHERE Id IN: idsOLI';
            
            System.debug(idsOLI);
            System.debug(query);
            
            List<OpportunityLineItem> listOLI = Database.query(query); /*[SELECT Id, Description, Nature__c, PricebookEntryId, Quantity, Classe__c, EuropeanCode__c,  Features__c, 
                                                GE__c, IncludedTGAP__c, Sector__c, Label__c, Product2ID, Packaging__c, TransportType__c, Treatment__c, 
                                                UNCode__c, Unit__c, N_CAP__c, UnitPrice, Amendment__c, BuyingPrice__c, CommentaryAmendementsX3__c,
                                                FooterContractX3__c, HeaderContractX3__c, LabelArticleCode__c, Outlet__c, Planned_next_year__c,
                                                TECH_Communal_tax__c, TECH_Indexation_gasoil__c, TextSalesX3__c, TransportPrice__c, TreatmentPrice__c
                                                FROM OpportunityLineItem WHERE Id IN: idsOLI];*/
            
            System.debug('listOLI : '+listOLI);
            
            List<QuoteLineItem> lst2 = new List<QuoteLineItem>();
            
            for(QuoteLineItem qli : lst){
                for(OpportunityLineItem oli: listOLI) {
                    if(qli.OpportunityLineItemId == oli.Id) {

                        QuoteLineItem qli2 = new QuoteLineItem(Id=qli.Id);
                        for(QuoteLineItem_Field__c qlif: lstQuoteLineFields) {
                            
                            qli2.put(qlif.Name, oli.get(qlif.OppLineSyncField__c));
                            
                        }

                        lst2.add(qli2);
                    }
                }
                
            }
            
            QteLineItemSyncTriggerV2Handler.stop();
            
            System.debug('lst2 : '+lst2);
            
            update lst2;

        }
        
    }

}