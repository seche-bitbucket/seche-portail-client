/**
 * @description       : 
 * @author            : MAIT - @moidriss
 * @group             : 
 * @last modified on  : 11-22-2023
 * @last modified by  : MAIT - @moidriss
**/
@isTest
public class PFAS_ControllerTest {
    
    @isTest
    public static void allErrors() {
        PFAS_Controller.getCapByContactId(null);
        PFAS_Controller.checkIfSentForSig(null);
        PFAS_Controller.SendCAPToSign(new List<PFAS_Controller.PFAS_ARGS>{new PFAS_Controller.PFAS_ARGS()});
    }
    
    @isTest
    public static void testGetCapByContactId() {
        // Create test data and add documents
        Contact con = TestFactory.generateContact();
        CAP__c cap = TestFactory.generateCap(null, con.Id);
        ContentDocumentLink cdl = TestFactory.linkDoc(cap.Id, 'testCap');
    
        // Call methods
        Test.startTest();
        List<CAP__c> getCapByContactId = PFAS_Controller.getCapByContactId(con.Id);
        PFAS_Controller.getSubstances();
        Test.stopTest();
        
        // Assert the results
        System.assertEquals(1, getCapByContactId.size());
    }
    
    @isTest
    public static void testSendData() {
        // Create test data
        CAP__c testCAP = TestFactory.generateCap(null, null);
        Component__c testComponent = TestFactory.generateComponent(testCAP.Id);
        
        List<Id> componentsIds = new List<Id>();
        List<CAP__c> capRecords = new List<CAP__c>{ testCAP };
        List<Component__c> componentRecords = new List<Component__c>{ testComponent };

        Account a = new Account();
        a.Name = 'TESTFIPAMIANTE';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingStreet = '12 rue du test';
        a.BillingPostalCode = '39000';
        a.BillingCity = 'Lyon';
        a.NAF_Number__c ='1234A';
        a.ChargedX3Code__c = 'FR972139821';
        a.X3ChargedAddressCode__c = 'GEN';
        a.FavoriteSubsidiaryAsbestos__c = 'Tredi Salaise 1';
        a.SIRET_Number__c = '09218301381222';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName1';
        c.AccountId = a.Id;
        c.Email = 'aaaa@groupe-seche.com';
        c.Phone = '03 00 00 00 00';
        insert c;
        
        FIP_FIP__c fip = new FIP_FIP__c();
        system.debug('Collector__c = '+ a.ID);
        fip.Collector__c = a.ID;
        system.debug('Contact__c = '+ c.ID);
        fip.Contact__c = c.ID;
        insert fip;

        String jsonData1 = JSON.serialize(capRecords);
        String jsonData2 = JSON.serialize(componentRecords);
        String jsonDataFip = JSON.serialize(fip);
        
        // Call the sendData method from the controller
        Test.startTest();
        Object res1 = PFAS_Controller.sendData('updateCAP', jsonData1);
        Object resFip = PFAS_Controller.sendData('updateFIP', jsonDataFip);
        Map<Id, Component__c> res2 = new Map<Id, Component__c>((List<Component__c>)PFAS_Controller.sendData('insertComponents', jsonData2));

        String jsonData3 = JSON.serialize(res2.keySet());
        Object res3 = PFAS_Controller.sendData('deleteComponents', jsonData3);
        Test.stopTest();

        // Assert the results
        System.assertEquals(1, capRecords.size());
        System.assertNotEquals(null, res1);
    }
    
    @isTest
    public static void testSign() {
        Contact con = TestFactory.generateContact();
        CAP__c cap = TestFactory.generateCap(null, con.Id);
        List<Id> capIds = new List<Id>{ cap.Id };

        PFAS_Controller.PFAS_ARGS args = new PFAS_Controller.PFAS_ARGS();
        args.contactId = con.Id;
        args.capsIds = capIds;
        
        Test.startTest();
        PFAS_Controller.checkIfSentForSig(capIds);
        PFAS_Controller.SendCAPToSign(new List<PFAS_Controller.PFAS_ARGS>{args});
        Test.stopTest();
    }
}