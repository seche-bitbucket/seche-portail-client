public with sharing class DeleteContactInvitationsBatch implements Database.Batchable<sObject>  {
    
    public Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator('SELECT Id, TECH_ToDelete__c FROM Contacts_Invitation__c WHERE TECH_ToDelete__c = true');
    }

    public void execute(Database.BatchableContext context, List<Contacts_Invitation__c> scope) {
        System.debug('scope : ' + scope);
        List<Database.DeleteResult> deleteResult = Database.delete(scope, false);
    }

    public void finish(Database.BatchableContext context) {
    }
}