@isTest(SeeAllData=false)
public with sharing class FlowAction_QueriesTest {
    
    @isTest
    public static void test() {

        List<Account> accs = new List<Account>{new Account(Name = 'Test 1 Test 1'), new Account(Name='SET 1 SAT 1')};
        insert accs;

        FlowAction_Queries.Parameter param = new FlowAction_Queries.Parameter();
        param.objectApiName='Account';
        Map<Id, Account> mp = new Map<Id,Account>([Select Id FROM Account]);
        param.listValues=new List<Id>(mp.keySet());

        List<List<SObject>> lst = FlowAction_Queries.getRecords(new List<FlowAction_Queries.Parameter>{param});

        System.assertEquals(lst[0].size(), 2);

        param.fieldsToRetrieve='Id,Name';

        lst = FlowAction_Queries.getRecords(new List<FlowAction_Queries.Parameter>{param});

        System.assertEquals(lst[0].size(), 2);
        
    }
}