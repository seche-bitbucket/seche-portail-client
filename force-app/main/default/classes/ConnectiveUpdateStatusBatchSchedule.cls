/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ConnectiveUpdateStatusBatchSchedule
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ConnectiveUpdateStatusBatchSchedule
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 27-05-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
global class ConnectiveUpdateStatusBatchSchedule implements Schedulable{
    global void execute(SchedulableContext sc) {
        ConnectiveUpdateStatusBatch b1 = new ConnectiveUpdateStatusBatch();
        ID batchprocessid = Database.executeBatch(b1,200);           
    }
}