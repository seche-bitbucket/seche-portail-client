public class QuoteTriggerHandler {
    
    public static void updateQte(List<Quote> lst) {
        
        system.debug(lst);
        
        for (Quote quote: lst){
            Date today = system.today();
            
            //Set the year
            quote.QuoteCode__c = string.valueOf(today.year())+ '-';
            
            //Set the Month
            if(string.valueOf(today.month()).length()==1)
                quote.QuoteCode__c += '0' + today.month() + '-';
            else
                quote.QuoteCode__c += today.month() + '-';
            
            //set the day
            if(string.valueOf(today.day()).length()==1)
                quote.QuoteCode__c += '0' + today.day() + '-';
            else
                quote.QuoteCode__c += today.day()+ '-';
            
            
            quote.QuoteCode__c +=  quote.Opportunity.Salesman__r.Trigramme__c + '-';
            String qteNbe = String.valueOf(quote.QuoteNumber);
            if(!String.isEmpty(qteNbe) && qteNbe != null) {
            	quote.QuoteCode__c += qteNbe.substring(2) ;
            }
        }
        
        update lst;
        
    }

}