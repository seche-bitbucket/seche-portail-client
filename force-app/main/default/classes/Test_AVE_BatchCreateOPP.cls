/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-06-29
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for Batch create opportunity for the contracts Controller 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest(SeeAllData=false)
private class Test_AVE_BatchCreateOPP {
    @isTest static void testBatchExecute(){
        ByPassUtils.ByPass('ContractTrigger');
        Profile directp = [select id from profile where name = :'Directeur commercial' limit 1];
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = directp.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        
        insert director;
        Profile p = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];
        
        String testemail = 'Commercial-_User_test@test.com';
        User Commercial = new User(profileId = p.id, username = testemail, email = testemail,
                                   emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                   languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                   alias='cs', lastname='lastname2', IsActive=true);
        insert Commercial;
        
        
        System.RunAs(Commercial) {
            Pricebook2 pb1 = new Pricebook2(Name='Pricebook1', isActive=true);
            insert pb1;
            //UserRecordAccess
            Product2 prd = new product2(name = 'Autre Traitement' ,TECH_ExternalID__c = '012345',Family = 'Traitement');
            Product2 prd2 = new product2(name = 'Autre Prestation' ,TECH_ExternalID__c = '01234',Family = 'Prestation');
            Product2 prd3 = new product2(name = 'Autre Conditionnement' ,TECH_ExternalID__c = '0123',Family = 'Conditionnement');
            Product2 prd4 = new product2(name = 'Autre Valorisation' ,TECH_ExternalID__c = '012',Family = 'Valorisation');
            List<Product2> products=new List<Product2>();
            products.add(prd);
            products.add(prd2);
            products.add(prd3);
            products.add(prd4);
            insert products;
            PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = test.getStandardPricebookId(), Product2Id = prd.Id, UnitPrice = 100, IsActive = true, UseStandardPrice = false);
            PricebookEntry standardPrice2 = new PricebookEntry(Pricebook2Id = test.getStandardPricebookId(), Product2Id = prd2.Id, UnitPrice = 200, IsActive = true, UseStandardPrice = false);
            PricebookEntry standardPrice3 = new PricebookEntry(Pricebook2Id = test.getStandardPricebookId(), Product2Id = prd3.Id, UnitPrice = 300, IsActive = true, UseStandardPrice = false);
            PricebookEntry standardPrice4 = new PricebookEntry(Pricebook2Id = test.getStandardPricebookId(), Product2Id = prd4.Id, UnitPrice = 400, IsActive = true, UseStandardPrice = false);
            List<PricebookEntry> standardPricebookEntries=new List<PricebookEntry>();
            standardPricebookEntries.add(standardPrice);
            standardPricebookEntries.add(standardPrice2);
            standardPricebookEntries.add(standardPrice3);
            standardPricebookEntries.add(standardPrice4);
            insert standardPricebookEntries;
            PricebookEntry pbe = new PricebookEntry(UseStandardPrice = false,Product2Id=prd.Id,Pricebook2Id=pb1.Id,
                                                    isActive=true, UnitPrice = 100);            
            PricebookEntry pbe2 = new PricebookEntry(UseStandardPrice = false,Product2Id=prd2.Id,Pricebook2Id=pb1.Id,
                                                     isActive=true, UnitPrice = 200);
            PricebookEntry pbe3 = new PricebookEntry(UseStandardPrice = false,Product2Id=prd3.Id,Pricebook2Id=pb1.Id,
                                                     isActive=true, UnitPrice = 300);
            PricebookEntry pbe4 = new PricebookEntry(UseStandardPrice = false,Product2Id=prd4.Id,Pricebook2Id=pb1.Id,
                                                     isActive=true, UnitPrice = 400);
            List<PricebookEntry> PricebookEntries=new List<PricebookEntry>();
            PricebookEntries.add(pbe);
            PricebookEntries.add(pbe2);
            PricebookEntries.add(pbe3);
            PricebookEntries.add(pbe4);
            insert PricebookEntries;
            Account ac = new Account();
            ac.Name = 'testAccName';
            ac.CustomerNature__c = 'Administration';
            ac.Industry = 'Trader';
            ac.CurrencyIsoCode = 'EUR';
            ac.Producteur__c = false;
            ac.BillingPostalCode = '00000';
            ac.NAF_Number__c = '1234A';
            ac.SIRET_Number__c = '10111121131151';
            insert ac;
            Contact con=new Contact();
            con.FirstName='test1';
            con.LastName = 'testName';
            con.AccountId = ac.Id;
            con.Email = 'aaa@yopmail.com';
            con.Phone = '000000000';
            insert con;
            List<Contract__c> contractsToInsert=new List<Contract__c>();
            for(integer i=0;i<20;i++){
                Contract__c c = new Contract__c();
                c.Account__c=ac.id;
                c.Etat__c='Convention';
                c.RegroupmentIndex__c=null;
                c.AssociatedSalesRep__c=Commercial.Id;
                c.ContactRelated__c=con.Id;
                c.name='Contract Test'+i;
                c.Status__c='Valide';
                contractsToInsert.add(c);
            }
            if(contractsToInsert.size()>0){
                insert contractsToInsert;
            }
            List<Amendment__c> Amendments = new List<Amendment__c>();
            for(Contract__c contract:contractsToInsert){
                Amendment__c am = new Amendment__c();
                am.Contrat2__c=contract.id;
                am.WasteLabel__c='conditionnement amiante' + Integer.valueof((Math.random() * 100));
                am.Nature__c='Conditionnement';
                am.PriceNextYear__c=120;
                am.QuantityNextYear__c =10;
                am.UnitNextYear__c='Forfait';
                am.QuantityInvoiced2018__c=11;
                Amendment__c am2 = new Amendment__c();
                am2.Contrat2__c=contract.id;
                am2.WasteLabel__c='Transport amiante'+Integer.valueof((Math.random() * 100));
                am2.Nature__c='Prestation';
                am2.PriceNextYear__c=120;
                am2.QuantityNextYear__c=20;
                Amendment__c am3 = new Amendment__c();
                am3.Contrat2__c=contract.id;
                am3.WasteLabel__c='traitement amiante'+Integer.valueof((Math.random() * 100));
                am3.Nature__c='Traitement';
                am3.PriceNextYear__c=120;
                am3.QuantityNextYear__c=30;
                Amendment__c am4 = new Amendment__c();
                am4.Contrat2__c=contract.id;
                am4.WasteLabel__c='valorisation amiante'+Integer.valueof((Math.random() * 100));
                am4.Nature__c='Valorisation';
                am4.PriceNextYear__c=120;
                am4.QuantityNextYear__c=40;
                Amendments.add(am);
                Amendments.add(am2);
                Amendments.add(am3);
                Amendments.add(am4);
            }   
            
            if(Amendments.size()>0){
                for(Amendment__c am : Amendments){
                     system.debug('Amendment : ' + am);
                }
               
                insert Amendments;
            }

            insert new QuoteLineItem_Field__c(Name='Filiale__c', OppLineSyncField__c='Filiale__c');

            Test.startTest();
            
            Database.executeBatch(new AVE_BatchCreateOPP());
            Test.stopTest();

        }
    }
}