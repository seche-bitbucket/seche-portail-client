/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ContentVersionTriggerHandler
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ContentVersionTriggerHandler trigger
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 23-03-2020 PMB 1.0 - Initial version
* -- 04-06-2020 MGR 2.0 - Comment AfterInsert (Ticket 00951)
* -- 07-07-2020 PMB 3.0 - Add blockContentVersionSizeTitle
* --------------------------------------------------------------------------------------------------------------------
*/
public with sharing class ContentVersionTriggerHandler {
    public static void onBeforeUpdate(Map<Id,ContentVersion> mapContentVersion,Map<Id,ContentVersion> mapOldContentVersion){
        blockContentVersionQuote(mapContentVersion);
        blockContentVersionSizeTitle(mapContentVersion);
    }
    
    //Method commented since Ticket 00951
    /*public static void onAfterInsert(Map<Id,ContentVersion> mapContentVersion,Map<Id,ContentVersion> mapOldContentVersion){
        blockContentVersionQuote(mapContentVersion);
    }*/

    /**
    * Returns Void
    * 
    * This method block the created or updated of a file for the Quote Locked in a approval process or when it is status final 
    *
    * @param  mapContentVersion Map<Id,ContentVersion>
    * @return void
    */
    public static void blockContentVersionQuote(Map<Id,ContentVersion> mapContentVersion){
        List<Id> cDocuIdSet = new List<Id>();
        Map<Id,ContentVersion> mapContentDocumentIdContentVersion = new Map<Id,ContentVersion>();
        for(ContentVersion cv : mapContentVersion.values()){
            if(cv.ContentDocumentId != null && !cv.Title.Contains(Constants.STR_CVE_SIGNE)){
                mapContentDocumentIdContentVersion.put(cv.ContentDocumentId,cv);
            }
        }
        if(mapContentDocumentIdContentVersion.size()>0){
            List<ContentDocumentLink> lstCdl = [SELECT ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId IN:mapContentDocumentIdContentVersion.keySet()];
            if(lstCdl.size()>0){
                Map<Id,ContentVersion> mapIdQuoteContentVersion = new Map<Id,ContentVersion>();
                for(ContentDocumentLink cv : lstCdl){
                    mapIdQuoteContentVersion.put(cv.LinkedEntityId,mapContentDocumentIdContentVersion.get(cv.ContentDocumentId));
                }
                Map<Id,Quote> mapQuote = new Map<Id,Quote>([SELECT Id, TECH_IsLocked__c FROM Quote WHERE TECH_IsLocked__c=true AND Id IN:mapIdQuoteContentVersion.keySet()]);
                if(mapQuote.size()>0){
                    List<ContentVersion> listContentVerError = new List<ContentVersion>();
                    for(Quote q : mapQuote.values()){
                        if(q!=null && q.TECH_IsLocked__c){
                            listContentVerError.add(mapIdQuoteContentVersion.get(q.Id));
                        }
                    }
                    if(listContentVerError.size()>0){
                        for(ContentVersion cv : listContentVerError){
                            cv.addError(Constants.STR_CVE_ERRORMESSAGE_QUOTE_LOCKED);
                        }
                    }
                }
            }
        }
    }

    public static void blockContentVersionSizeTitle(Map<Id,ContentVersion> mapContentVersion){
        Set<Id> docId = new Set<Id>();
        List<ContentVersion> lstErrorCV = new List<ContentVersion>();
        for(Id idCV : mapContentVersion.keySet()){
            if(mapContentVersion.get(idCV).Title.length()>75){
                lstErrorCV.add(mapContentVersion.get(idCV));
            }
        }
        if(lstErrorCV.size()>0){
            for(ContentVersion cv : lstErrorCV){
                cv.addError(Constants.STR_CVE_ERRORMESSAGE_SIZE_TITLE);
            }
        }
    }
}