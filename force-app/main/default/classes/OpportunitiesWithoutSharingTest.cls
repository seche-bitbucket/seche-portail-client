@isTest
public class OpportunitiesWithoutSharingTest {
    @isTest (SeeAllData=true) 
    public static void ListOpportunitiesMethod() {
        AggregateResult[] opp;
        
        //Get a Record Type for Account
        RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
        
        //Create an account
        Account acc = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aucun', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
        insert acc;
        
        //Get a Record Type for Opportunity
        RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];
        
      
        
         //Get a profile to create User
        Profile p = [select id from profile where name = :'Chargé(e) d\'affaires' limit 1];
        
        //Create a User for the Test 
        String testemail = 'UserTest1@cap.com';
        String testemail2 = 'UserTest2@cap.com';
        
        User User1 = new User(profileId = p.id, username = testemail, email = testemail,
                               SubsidiaryCreationRight__c = 'Alcea',
                              emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                              languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                              alias='cspu', lastname='lastname');
        Insert User1;
        
        User User2 = new User(profileId = p.id, username = testemail2, email = testemail2,
                               SubsidiaryCreationRight__c = 'Alcea',
                              emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                              languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                              alias='cspu', lastname='lastname2');
        Insert User2;
        System.RunAs(User1) { 
            //Create Opportunity1
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT',OwnerId = User1.Id,RecordTypeId = rt2.Id, AccountID= acc.Id, Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', Filiere__c ='DASRI');
            insert opportTest;
            
        }
        System.RunAs(User2) {
            //Create Opportunity2
            Opportunity opportTest2 = new Opportunity(Name = 'TESTOPPORT2',OwnerId = User2.Id, RecordTypeId = rt2.Id, AccountID= acc.Id, Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', Filiere__c ='DASRI');
            insert opportTest2;
            
        }
        ApexPages.StandardController controller= new ApexPages.StandardController(acc);
        
        OpportunitiesWithoutSharing myController = new OpportunitiesWithoutSharing(controller);
        opp= myController.GetOpp();
        system.assertEquals(2, opp.size());
        
        
        
        
    }
    
}