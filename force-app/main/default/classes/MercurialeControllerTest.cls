@isTest
public class MercurialeControllerTest {
    
    @isTest static void testMercurialeController() {
        
        // Create data test
        String source = 'Copacel';
        Date selectedDate =  Date.today().toStartOfMonth();

        Product2 testProduct = new Product2(
            Name = 'productTest', 
            TECH_ExternalID__c = '723ML', 
            IsMercuriales__c = true, 
            MercurialesSources__c = source
        );
        insert testProduct;
        
        Index__c testIndex = new Index__c(Product__c=testProduct.Id);
        insert testIndex;

        IndexValue__c testIndexValue = new IndexValue__c(
            Indices__c = testIndex.Id,
            Source__c = source,
            Date__c = selectedDate,
            Value__c = 0.25
        );
        insert testIndexValue;

        // Call methods from the controller
        Test.startTest();
        List<Index__c> testGetProduct = MercurialeController.getProduct(source);
        List<IndexValue__c> testGetIndexValue = MercurialeController.getIndexValue(source, selectedDate);
        Map<Id, IndexValue__c> testGetMercuriale = MercurialeController.getMercuriale(source, selectedDate);
        Test.stopTest();

        // Assert the results
        System.assertNotEquals(0, testGetProduct.size());
        System.assertNotEquals(0, testGetIndexValue.size());
        System.assertNotEquals(0, testGetMercuriale.size());
        System.assertEquals(0.25, testGetMercuriale.get(testIndex.Id).Value__c);
    }
}