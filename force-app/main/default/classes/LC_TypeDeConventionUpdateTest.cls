/*
 * ------------------------------------------------------------------------------------------------------------------
 * -- - Name : LC_TypeDeConventionUpdateTest
 * -- - Author : NLE
 * -- - Company : Capgemini
 * -- - Purpose : Test class for LC_TypeDeConventionUpdate
 * --
 * -- Maintenance History:
 * --
 * -- Date 	Name Version Remarks
 * -- ----------- ---- ------- -------------------------------------------------------------------------------------
 * -- 06-08-2019 NLE 1.0 Initial version 
 * --------------------------------------------------------------------------------------------------------------------
 */
@isTest
public class LC_TypeDeConventionUpdateTest {
    @isTest
    public static void testUpdateContractsA(){        
        Contract__c c1 = new Contract__c();
        Contract__c c2 = new Contract__c();
        c1.Filiale__c = String_Helper.CON_Filiale_Drimm;
        c2.Filiale__c = String_Helper.CON_Filiale_Drimm;
       
        List<Contract__c> listC = new List<Contract__c>();
        listC.add(c1);
        listC.add(c2);
        insert listC ; 

		ApexPages.StandardSetController sc = new ApexPages.standardSetController(listC);        
        VFC_UpdateCONTypeDeConvention updateConv = new VFC_UpdateCONTypeDeConvention(sc);
        
        updateConv.contractsList = listC ;
        
        LC_TypeDeConventionUpdate updateConvLC = new LC_TypeDeConventionUpdate();  
        String typeDeConvention = LC_TypeDeConventionUpdate.getTypeValues('Filiale__c','Type_de_convention__c').get(c1.Filiale__c)[0];
        LC_TypeDeConventionUpdate.updateContractsA(  updateConv.getContractsId()  , typeDeConvention);
        Contract__c result = [select Type_de_convention__c from Contract__c where Id =:listC[0].Id ];
        System.assertEquals(typeDeConvention, result.Type_de_convention__c);
 
    }

      @isTest
    public static void testUpdateContractsConv(){        
        Contract__c c1 = new Contract__c();
        Contract__c c2 = new Contract__c();
        c1.Filiale__c = String_Helper.CON_Filiale_Drimm;
        c2.Filiale__c = String_Helper.CON_Filiale_Drimm;
       
        List<Contract__c> listC = new List<Contract__c>();
        listC.add(c1);
        listC.add(c2);
        insert listC ; 

		ApexPages.StandardSetController sc = new ApexPages.standardSetController(listC);        
        VFC_UpdateCONTypeDeConvention updateConv = new VFC_UpdateCONTypeDeConvention(sc);
        
        updateConv.contractsList = listC ;
        
        LC_TypeDeConventionUpdate updateConvLC = new LC_TypeDeConventionUpdate();  
        String typeDeConvention = LC_TypeDeConventionUpdate.getTypeValues('Filiale__c','Type_de_convention__c').get(c1.Filiale__c)[0];
        LC_TypeDeConventionUpdate.updateContractsConv( listC  , typeDeConvention);
        Contract__c result = [select Type_de_convention__c from Contract__c where Id =:listC[0].Id ];
        System.assertEquals(typeDeConvention, result.Type_de_convention__c);
 
    }

}