public class FlowResponse {
    public String FlowId{get;set;}
    public String ExternalReference{get;set;}
    public String DownloadURL{get;set;}
    public String F2FSigningURL{get;set;}
    public String EmailAddress{get;set;}
    public String CreationTime{get;set;}
    public List <SigningURL> SigningURL{get;set;}
    
    
    public class SigningURL{
        public String ExternalReference{get;set;}
        public String SignerId{get;set;}
        public String Url{get;set;}
        public SigningUrl(){}
    }
    
    public static FlowResponse deserializejson(String json){
        return (FlowResponse) System.JSON.deserialize(json, FlowResponse.class);
    }
}