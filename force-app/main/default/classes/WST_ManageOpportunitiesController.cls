public class WST_ManageOpportunitiesController {
    
    private static ApexPages.StandardSetController ctrl;
    
    //List of WST selected, with opportunities, without opporuntunites
    private static List<WST_Waste_Statement__c> LinkedOppToUpdate = new List<WST_Waste_Statement__c>();
    private static List<WST_Waste_Statement__c> LinkedOppToCreate = new List<WST_Waste_Statement__c>();
    private static List<WST_Waste_Statement__c> wsts = new List<WST_Waste_Statement__c>();
    
    //List of WST by Opportunity to create the generic opportunity line item 
    private static Map<Opportunity, WST_Waste_Statement__c> mapOppByWSTToCreateGenericOLI = new Map<Opportunity, WST_Waste_Statement__c>();
    //List of Opporunities to create or update
    //private static List<Opportunity> oppToCreate = new List<Opportunity>();
    private static List<Opportunity> oppToUpdate = new List<Opportunity>();
    
    //list of QuoteLine linked to selected WST
    private static Map<ID, List<QuoteLine__c>> qls = new Map<ID, List<QuoteLine__c>>();
    
    //List of PriceBookEntry per Type
    private static List<PriceBookEntry> pbesRemediation = new List<PriceBookEntry>();
    private static List<PriceBookEntry> pbesRepairs = new List<PriceBookEntry>();
    
    //Boolean to Control if everything is OK
    private static Boolean check = true;
    private static Exception exep;
    
    public WST_ManageOpportunitiesController(ApexPages.StandardSetController controller){
        ctrl = controller;
       
      
        wsts = [Select ID, Name, KVA__c, Opportunity__c, SendTransfoToElim__c, PCBContent__c, ServiceDemande__c, OilWeight__c, ServiceDemande__r.ApplicantCenter__c, ServiceDemande__r.ApplicantCenter__r.Name, 
                ServiceDemande__r.ApplicantCenter__r.BillingStreet, ServiceDemande__r.ApplicantCenter__r.BillingCity, ServiceDemande__r.ApplicantCenter__r.BillingState,
                ServiceDemande__r.ApplicantCenter__r.BillingPostalCode, ServiceDemande__r.ApplicantCenter__r.BillingCountry, ServiceDemande__r.ApplicantCenter__r.OwnerId, ServiceDemande__r.BilledAccount__r.OwnerId,
                ServiceDemande__r.BilledAccount__r.BillingStreet, ServiceDemande__r.BilledAccount__r.BillingCity, ServiceDemande__r.BilledAccount__r.BillingState,
                ServiceDemande__r.BilledAccount__r.BillingPostalCode, ServiceDemande__r.BilledAccount__r.BillingCountry,
                ServiceDemande__r.ResponsibleName__c, ServiceDemande__r.ResponsibleName__r.Email, ServiceDemande__r.ResponsibleName__r.Phone, ServiceDemande__r.ResponsibleName__r.Fax,
                ServiceDemande__r.PriceBook__c FROM WST_Waste_Statement__c WHERE ID in :ctrl.getSelected()AND IsDiagnosticDone__c = true];
        List<QuoteLine__c> qlList = [Select ID, TECH_PriceBookEntryID__c, Label__c, TECH_PriceBookEntryPrice__c, Quantity__c, Transformateur__c, Code__c FROM QuoteLine__c WHERE Transformateur__c in : wsts ];
        
        System.debug('WSTS : '+wsts);
        System.debug('qlList : '+qlList);
        qls = generateMapOfQuoteLines(qlList);
    }
    
    public static PageReference manageOpportunites(){
        
		Id SDID = apexPages.CurrentPage().getParameters().get('id');
        PageReference retPage = new PageReference('/'); 
        if(SDID != null){
            retPage  = new PageReference('/'+SDID); 
        }else{
             retPage  = new PageReference('/a1B'); 
        }
        
        
        
        if(wsts.size() > 0){ 
            for(WST_Waste_Statement__c wst : wsts){
                if(wst.Opportunity__c != null){
                    LinkedOppToUpdate.add(wst);
                    
                }else{
                    LinkedOppToCreate.add(wst);
                    
                }
            }
            System.debug('LinkedOppToCreate : '+LinkedOppToCreate);
            List<Opportunity> oppToCreate = createOpportunities(LinkedOppToCreate, false);
            System.debug('oppToCreate : '+oppToCreate);
            List<OpportunityLineItem> olisToAdd = generateGenericLineItem();
            List<Quote> qs = createQuotes(LinkedOppToCreate, oppToCreate);
            createOpportunityLineItems(oppToCreate, olisToAdd);
            if(check){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Offre(s) créée(nt) avec succès !'));
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Une erreur est survenue, contacter votre administrateur.'+ exep));
                
            }
        }else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Veuillez sélectionner un ou plusieurs transformateur(s) avant de procéder'));   
        }
        
        
        return retPage;
        
        
        
        
    }
    
    public static List<Opportunity> createOpportunities(List<WST_Waste_Statement__c> wstWithoutOpportunites, Boolean isSendInElimination){
        Map<ID, WST_Waste_Statement__c> mapWST = new Map<ID, WST_Waste_Statement__c>();
       
        List<Opportunity> oppToCreate = new List<Opportunity>();
		List<WST_Waste_Statement__c> wstToUpdate = new List<WST_Waste_Statement__c>();
          System.debug('wstWithoutOpportunites : '+wstWithoutOpportunites);
        //Generate List of Opporunity
        for(WST_Waste_Statement__c wst : wstWithoutOpportunites){
            Opportunity opp = new Opportunity();
            opp.RecordTypeId = Label.Opp_RT_IndusStd;
            opp.Name = wst.ServiceDemande__r.ApplicantCenter__r.Name+' - '+wst.Name;
            opp.AccountId = wst.ServiceDemande__r.ApplicantCenter__c;
            opp.Salesman__c = wst.ServiceDemande__r.BilledAccount__r.OwnerID;
            opp.ContactName__c = wst.ServiceDemande__r.ResponsibleName__c;
            opp.Pricebook2Id = wst.ServiceDemande__r.PriceBook__c;
            opp.Transformateur__c = wst.ID;
            opp.ServiceDemand__c = wst.ServiceDemande__c;
            opp.Nature__c = 'Développement récurrent';
            opp.CloseDate = system.today();
            opp.StageName = 'Proposition Commercial';
            opp.Amount = 1;
            opp.Filiale__c = 'Tredi Saint Vulbas';
            opp.Filiere__c = 'Transformateurs';
            oppToCreate.add(opp);
            System.debug('oppToCreate : '+oppToCreate);
            mapWST.put(wst.ID, wst);
           System.debug('mapWST : '+mapWST);
        }
        
        try{
            insert oppToCreate;
        }catch(Exception e){
            exep = e;
            check = false;
        }
        
        
        //update WST with the linked Opportunity
        for(Opportunity opp : oppToCreate){
            WST_Waste_Statement__c wst = mapWST.get(opp.Transformateur__c);
            wst.Opportunity__c = opp.ID;
            wst.Status__c = 'Devis en preparation';
            wst.SendTransfoToElim__c = isSendInElimination;
            wstToUpdate.add(wst);
            System.debug('WST - Start : '+wst);
            mapOppByWSTToCreateGenericOLI.put(opp,wst);
        }
        
        try{
            update wstToUpdate;
        }catch(Exception e){
            exep = e;
            check = false;
        }
        
        return oppToCreate;
        
    }
    
    public static List<Quote> createQuotes(List<WST_Waste_Statement__c> wsts, List<Opportunity> opps){
        List<Quote> quotes = new List<Quote>();
        List<Opportunity> oppTmpToUpdate = new List<Opportunity>();
        Map<ID, Quote> manageOppUpdate = new Map<Id, Quote>();
        
        for(WST_Waste_Statement__c wst : wsts){
            Quote q = new Quote();
            
            q.OpportunityId = wst.Opportunity__c;
            q.Name = wst.ServiceDemande__r.ApplicantCenter__r.Name+' - '+wst.Name +' V1';
            
            q.ContactId = wst.ServiceDemande__r.ResponsibleName__c;
            q.Email = wst.ServiceDemande__r.ResponsibleName__r.Email;
            q.Phone = wst.ServiceDemande__r.ResponsibleName__r.Phone;
            q.Fax = wst.ServiceDemande__r.ResponsibleName__r.Fax;
            q.BillingStreet = wst.ServiceDemande__r.ApplicantCenter__r.BillingStreet;
            q.BillingCity = wst.ServiceDemande__r.ApplicantCenter__r.BillingCity;
            q.BillingState = wst.ServiceDemande__r.ApplicantCenter__r.BillingState;
            q.BillingPostalCode = wst.ServiceDemande__r.ApplicantCenter__r.BillingPostalCode;
            q.BillingCountry = wst.ServiceDemande__r.ApplicantCenter__r.BillingCountry; 
            
            q.ShippingStreet = wst.ServiceDemande__r.BilledAccount__r.BillingStreet;
            q.ShippingCity = wst.ServiceDemande__r.BilledAccount__r.BillingCity;
            q.ShippingState = wst.ServiceDemande__r.BilledAccount__r.BillingState;
            q.ShippingPostalCode = wst.ServiceDemande__r.BilledAccount__r.BillingPostalCode;
            q.ShippingCountry = wst.ServiceDemande__r.BilledAccount__r.BillingCountry;
            q.Pricebook2Id = wst.ServiceDemande__r.PriceBook__c;
            q.DateDevis__c = getQuoteEndDate(q.Pricebook2Id);
            quotes.add(q);
            manageOppUpdate.put(q.OpportunityId, q);
            //opp.SyncedQuoteId = q.ID;
        }
        
        
        try{
            insert quotes;
        }catch(Exception e){
            exep = e;
            check = false;
        }
        
        for(Opportunity opp : opps){
            opp.SyncedQuoteId = manageOppUpdate.get(opp.ID).ID;
            oppTmpToUpdate.add(opp);
        }
        
        
        
        try{
            update oppTmpToUpdate;
        }catch(Exception e){
            exep = e;
            check = false;
        }
        
        return quotes;
        
    }
    
    public static Boolean createOpportunityLineItems(List<Opportunity> opps, List<OpportunityLineItem> olisGenerate){
        List<OpportunityLineItem> olis = new List<OpportunityLineItem>();
        olis.addAll(olisGenerate);
        for(Opportunity opp : opps){
            List<QuoteLine__c> qlList = qls.get(opp.Transformateur__c);
            if(qlList != null){
                for(QuoteLine__c ql : qlList){
                    OpportunityLineItem oli = new OpportunityLineItem();
                    
                    System.debug('///////Code '+ql.Code__c);
                    if(ql.Code__c != 'DEFAUT'){
                        oli.PricebookEntryId = ql.TECH_PriceBookEntryID__c;
                        oli.UnitPrice = ql.TECH_PriceBookEntryPrice__c;
                       
                    }else{
                        oli.PricebookEntryId = manageDefaultPriceBookEntry(opp.Pricebook2Id);
                        oli.UnitPrice = 0;
                        oli.Description = ql.Label__c;
                        System.debug('///////oli.PricebookEntryId '+oli.PricebookEntryId);
                    }
                    
                    oli.OpportunityId = opp.ID;
                    oli.Quantity = ql.Quantity__c;
                    oli.Unit__c = 'Unité';
                                        
                    olis.add(oli);
                }
            }
            
        }
        try{
            insert olis;
        }catch(Exception e){
            exep = e;
            check = false;
            return false;
        }
        
        return true;
    }
    
    public static Map<ID, List<QuoteLine__c>> generateMapOfQuoteLines(List<QuoteLine__c> qlList){
        
        for(QuoteLine__c ql : qlList){
            List<QuoteLine__c> tmp = new List<QuoteLine__c>();
            if(qls.containsKey(ql.Transformateur__c)){
                tmp = qls.get(ql.Transformateur__c);  
            }            
            tmp.add(ql);
            qls.put(ql.Transformateur__c, tmp);
        }
        
        return qls;
    }
    
    public static List<OpportunityLineItem> generateGenericLineItem(){
        
        List<OpportunityLineItem> oliGeneric = new List<OpportunityLineItem>();
        
        if(mapOppByWSTToCreateGenericOLI != null){
            generatePriceBookEntries();
            for(Opportunity opp : mapOppByWSTToCreateGenericOLI.keySet()){            
                String oppPBID = opp.Pricebook2Id;
                
                WST_Waste_Statement__c wst = mapOppByWSTToCreateGenericOLI.get(opp);
               System.debug('WST - Step 2 : '+wst);
                if(oppPBID.contains(Label.RCT_PB_Depollution)){
                    oliGeneric.addAll(generateGenericRemediationOLI(opp, wst));
                }else if(oppPBID.contains(Label.RCT_PB_Reparation)){
                    oliGeneric.addAll(generateGenericRepairsOLI(opp, wst, true, false));
                }
                  System.debug('oliGeneric : '+oliGeneric);
            }
        }
        
        return oliGeneric;
        
    }
    
    public static List<OpportunityLineItem> generateGenericRemediationOLI(Opportunity opp, WST_Waste_Statement__c wst){
        List<OpportunityLineItem> OLIs = new List<OpportunityLineItem>();
        System.debug('////GENERATE OLI REME');
        
        for(PricebookEntry pbe : pbesRemediation){
            if(pbe.Product2.Family == 'Traitement'){
                if(wst.KVA__c > pbe.Product2.Param_1_Num_Min__c &&
                   wst.KVA__c < pbe.Product2.Param_1_Num_Max__c &&
                   wst.PCBContent__c > pbe.Product2.Param_2_Num_Min__c &&
                   wst.PCBContent__c < pbe.Product2.Param_2_Num_Max__c){
                       OpportunityLineItem oli = new OpportunityLineItem();
                       oli.OpportunityId = opp.ID;
                       oli.PricebookEntryId = pbe.ID;
                       oli.UnitPrice = pbe.UnitPrice;
                       oli.Quantity = 1;
                       oli.Unit__c = 'Unité';
                       OLIs.add(oli);
                   }           
            }else if(pbe.Product2.Family == 'Prestation'){
                if(wst.KVA__c > pbe.Product2.Param_1_Num_Min__c &&
                   wst.KVA__c < pbe.Product2.Param_1_Num_Max__c){
                       OpportunityLineItem oli = new OpportunityLineItem();
                       oli.OpportunityId = opp.ID;
                       oli.PricebookEntryId = pbe.ID;
                       oli.UnitPrice = pbe.UnitPrice;
                       oli.Quantity = 1;
                       oli.Unit__c = 'Unité';
                       OLIs.add(oli);
                   }
            }
        }
        
        
        return OLIs;
    }
    
    public static List<OpportunityLineItem> generateGenericRepairsOLI(Opportunity opp, WST_Waste_Statement__c wst, Boolean addOil, Boolean Destruction){
        List<OpportunityLineItem> OLIs = new List<OpportunityLineItem>();
        
        if(pbesRepairs.size() == 0){
            generatePriceBookEntries();
        }
        
        System.debug('/////Destruction : '+Destruction);
        System.debug('/////PBES : '+pbesRepairs);
        System.debug('/////wst.KVA__c : '+wst.KVA__c);
        for(PricebookEntry pbe : pbesRepairs){
            System.debug('/////pbe : '+pbe);
            if(Destruction){
                if(pbe.Product2.Family == 'Traitement'){
                    if(pbe.Product2.Param_1_Num_Min__c == null &&
                       pbe.Product2.Param_1_Num_Max__c == null && 
                       pbe.Product2.Param_3_Text__c == 'Destruction'){
                           OpportunityLineItem oli = new OpportunityLineItem();
                           oli.OpportunityId = opp.ID;
                           oli.PricebookEntryId = pbe.ID;
                           oli.UnitPrice = pbe.UnitPrice;
                           oli.Quantity = 1;
                           oli.Unit__c = 'Unité';
                           OLIs.add(oli);
                       }       
                }else if(pbe.Product2.Family == 'Prestation'){
                    if(wst.KVA__c > pbe.Product2.Param_1_Num_Min__c &&
                       wst.KVA__c < pbe.Product2.Param_1_Num_Max__c && 
                       pbe.Product2.Param_3_Text__c == 'Destruction'){
                           OpportunityLineItem oli = new OpportunityLineItem();
                           oli.OpportunityId = opp.ID;
                           oli.PricebookEntryId = pbe.ID;
                           oli.UnitPrice = pbe.UnitPrice;
                           oli.Quantity = 1;
                           oli.Unit__c = 'Unité';
                           OLIs.add(oli);
                       }
                }
            }else{
                if(pbe.Product2.Family == 'Traitement'){
                    if(wst.KVA__c > pbe.Product2.Param_1_Num_Min__c &&
                       wst.KVA__c < pbe.Product2.Param_1_Num_Max__c){
                           OpportunityLineItem oli = new OpportunityLineItem();
                           oli.OpportunityId = opp.ID;
                           oli.PricebookEntryId = pbe.ID;
                           oli.UnitPrice = pbe.UnitPrice;
                           oli.Quantity = 1;
                           oli.Unit__c = 'Unité';
                           OLIs.add(oli);
                       }else if(pbe.Product2.Param_3_Text__c == 'Forfait_Huile' && addOil){
                           OpportunityLineItem oli = new OpportunityLineItem();
                           oli.OpportunityId = opp.ID;
                           oli.PricebookEntryId = pbe.ID;
                           oli.UnitPrice = pbe.UnitPrice;
                           if(wst.OilWeight__c != null){
                               Decimal weight = wst.OilWeight__c/1000;
                               oli.Quantity = weight;
                           }else{
                               oli.Quantity = 1;
                           }
                           oli.Unit__c = pbe.Product2.Unit__c;
                           OLIs.add(oli);
                       }         
                }else if(pbe.Product2.Family == 'Prestation'){
                    if(wst.KVA__c > pbe.Product2.Param_1_Num_Min__c &&
                       wst.KVA__c < pbe.Product2.Param_1_Num_Max__c && 
                       pbe.Product2.Param_3_Text__c != 'Destruction'){
                           OpportunityLineItem oli = new OpportunityLineItem();
                           oli.OpportunityId = opp.ID;
                           oli.PricebookEntryId = pbe.ID;
                           oli.UnitPrice = pbe.UnitPrice;
                           oli.Quantity = 1;
                           oli.Unit__c = 'Unité';
                           OLIs.add(oli);
                       }
                }
            }
            
        }
        System.debug('/////OLIs : '+OLIs);
        return OLIs;
    }
    
    private static void generatePriceBookEntries(){
        List<PriceBookEntry> pbes = [SELECT Id, Name, Product2.Unit__c, Pricebook2Id, Product2Id, Product2.Family, UnitPrice, IsActive, ProductCode,
                                     Product2.Param_1_Num_Min__c, Product2.Param_1_Num_Max__c, Product2.Param_2_Num_Min__c, Product2.Param_2_Num_Max__c, Product2.Param_3_Text__c
                                     FROM PricebookEntry 
                                     WHERE Pricebook2Id =: Label.RCT_PB_Depollution OR Pricebook2Id =: Label.RCT_PB_Reparation ];
        
        for(PriceBookEntry pbe : pbes){
            if(pbe.Pricebook2Id == Label.RCT_PB_Depollution){
                pbesRemediation.add(pbe);
            }else{
                pbesRepairs.add(pbe);
            }
        }
        
    }
    
    public static ID manageDefaultPriceBookEntry(String OppPriceBookId){
        
		 System.debug('///////OppRTID '+OppPriceBookId);
        if(OppPriceBookId.contains(Label.RCT_PB_Depollution)){
            System.debug('///////DEPOLL '+Label.RCT_PBE_DEFAUT_Depollution);
            return Label.RCT_PBE_DEFAUT_Depollution;
        }else if (OppPriceBookId.contains(Label.RCT_PB_Reparation)){
            System.debug('///////REP '+Label.RCT_PBE_DEFAUT_Reparation);
            return Label.RCT_PBE_DEFAUT_Reparation;
        }else{
             System.debug('///////TPC '+Label.RCT_PBE_DEFAUT_Hors_Marche);
            return Label.RCT_PBE_DEFAUT_Hors_Marche;
        }
		        
    }
    
    public static Date getQuoteEndDate(ID PriceBookID){
        
		Date d;
      
		
        if(PriceBookID == 	Label.RCT_PB_Reparation){
            
            d = date.parse(Label.RCT_REPA_END_DATE_CONTRACT);
            
        }else if(PriceBookID == Label.RCT_PB_Depollution){
           
            d = date.parse(Label.RCT_DEPOL_END_DATE_CONTRACT);
            
        }else if(PriceBookID == Label.RCT_PB_Elimination_N || PriceBookID == Label.RCT_PB_Elimination_S){
            
            d = date.parse(Label.RCT_ELIM_END_DATE_CONTRACT);
            
        }else if(PriceBookID == Label.RCT_PB_Elimination_SF6){
            
            d = date.parse(Label.RCT_SF6_END_DATE_CONTRACT);
            
        }else {
            d = Date.today();
        }
        
        
        return d;
		        
    }
    
}