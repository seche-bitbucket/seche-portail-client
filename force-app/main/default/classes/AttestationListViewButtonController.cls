public with sharing class AttestationListViewButtonController {
	ApexPages.StandardSetController stdSetController;
	public List <Attestation__c> lstSelectAtt = New List <Attestation__c>();
	List<SObject> opps = new List<SObject>();
	Boolean isAlreadySend;
	Boolean retButDisp;
	Boolean isNoSelectedAtt;
	Boolean isNotDisplayTextHelp;
	Boolean isEmailAddressBounced;
	Boolean isGenerateProcessOk;
	Map <Id,ContentVersion> fileContVersionMap = new Map  <Id,ContentVersion>();
	
	public AttestationListViewButtonController(ApexPages.StandardSetController stdSetController) {
		System.debug('Start AttestationListViewButtonController----: ');
		this.isGenerateProcessOk = false;
		this.isNoSelectedAtt = false;
		this.isAlreadySend = false;
		this.isNotDisplayTextHelp = false;
		this.isEmailAddressBounced = false;
		this.stdSetController = stdSetController;	
		this.lstSelectAtt = [SELECT id,Name, Status__c, Contact__r.Id, Contact__r.EMail, Contact__r.IsEmailBounced,AssociatedSalesRep__c,AssociatedSalesRep__r.Email, AssociatedSalesRep__r.Name,Sorted__c, Mixed__c, Papercardboard__c,Plastic__c,Metal__c, Glass__c, Wood__c, MineralFractions__c, Plaster__c,(SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLinks) FROM Attestation__c WHERE Id IN:stdSetController.getSelected()];
		
		
	}
	
	public void updateAttestation(List<Attestation__c> lstSelectAtt){
		for(Attestation__c a : lstSelectAtt) {
			a.Status__c = Constants.ATTESTATION_SEND_STATUS;
		}
		try {
			update lstSelectAtt;
		} catch (Exception e ) {
			System.debug('Error : '+e);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Une erreur est survenue pendant la mise à jour des attestations envoyées par mail, contacter votre administrateur.'+ e));
		}
	}
	
	
	public List<String> isNotStatusGenerated(List<Attestation__c> lstSelectAtt){
		List<String> listAttestationsGenerated = new List<String> ();
	
		for(Attestation__c a : lstSelectAtt) {
			if( a.Status__c != Constants.ATTESTATION_GENERATE_STATUS) {
				listAttestationsGenerated.add(a.Name);
			}
		}
	
		return listAttestationsGenerated;
	}
	
	
	public List<String> getAlreadySendAttestations(List<Attestation__c> lstSelectAtt){
		List<String> listAttestationsAlreadySend = new List<String> ();
		for(Attestation__c a : lstSelectAtt) {
			if( a.Status__c == Constants.ATTESTATION_SEND_STATUS) {
				listAttestationsAlreadySend.add(a.Name);
			}
		}
	
		return listAttestationsAlreadySend;
	}
	
	public List<String> getEmailAddresBounced (List<Attestation__c> lstSelectAtt){
		List<String> listAttestationsWithBouncedEmails = new List<String> ();
		for(Attestation__c a : lstSelectAtt) {
			if( a.Contact__r.IsEmailBounced==true) {
				listAttestationsWithBouncedEmails.add(a.Name);
			}
		}
		return listAttestationsWithBouncedEmails;
	}
	
	
	public List<String> isExistMultipleFilesOnAttest (List<Attestation__c> lstSelectAtt){
	
		Map<Id,List<ContentDocumentLink>> filesMapOnAttestId = new Map<Id,List<ContentDocumentLink>>();
		//Get Attestation__s Files ContentDocumentId
		Set<Id> lstAttestIds = new Set<Id>();
		for(Attestation__c a : lstSelectAtt) {
			lstAttestIds.add(a.Id);  //We will send only one file
		}
		
		 List<ContentDocumentLink> files  = [SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLink where LinkedEntityId IN :lstAttestIds ];
		//Get attachments by attest
		for(ContentDocumentLink f :files) {
			if(!filesMapOnAttestId.containsKey(f.LinkedEntityId)) {
				filesMapOnAttestId.put(f.LinkedEntityId, new list<ContentDocumentLink> {f});
			}else{
				filesMapOnAttestId.get(f.LinkedEntityId).add(f);
			}
		}
	
		List<String> listAttestationsWithMultiFiles = new List<String> ();
		for(Attestation__c a : lstSelectAtt) {
			if(filesMapOnAttestId.get(a.Id) != null  && filesMapOnAttestId.get(a.Id).size() > 1) { //More than one file
				listAttestationsWithMultiFiles.add(a.Name);
			}
		}
		return listAttestationsWithMultiFiles;
	} 
	
	/*
	public List<String> isExistMultipleFilesOnAttest (List<Attestation__c> lstSelectAtt){
		List<String> listAttestationsWithMultiFiles = new List<String>();
		 for(Attestation__c a : lstSelectAtt) {
			if(a.ContentDocumentLinks.size() > 1)
			listAttestationsWithMultiFiles.add(a.Name);	
		}	
		
		return listAttestationsWithMultiFiles;
	} */
	
	public List<String> getAttestationsWithoutFile (List<Attestation__c> lstSelectAtt){
		List<String> listAttestationsWithoutFile = new List<String> ();
		for(Attestation__c a : lstSelectAtt) {
			if( a.ContentDocumentLinks != null && a.ContentDocumentLinks.size() == 0 ) {
				listAttestationsWithoutFile.add(a.Name);
			}
		}
		System.debug('listAttestationsWithoutFile : '+listAttestationsWithoutFile.toString());
		return listAttestationsWithoutFile;
	}
	
	
	public void send(){
		System.debug('List Selected Records : '+lstSelectAtt);
	
		if (lstSelectAtt != null && lstSelectAtt.Size() == 0) {
			isNoSelectedAtt = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,' Veuillez sélectionner une attestation à envoyer !'));
	
		}else if(isAttestationNotConcerned(lstSelectAtt) != null && isAttestationNotConcerned(lstSelectAtt).size() > 0) { 
			isNoSelectedAtt = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,' Attestation(s) non concernée(s) : '+isAttestationNotConcerned(lstSelectAtt)));	
	
		}else if(getAlreadySendAttestations(lstSelectAtt) != null && getAlreadySendAttestations(lstSelectAtt).size() > 0) {
			isNoSelectedAtt = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,' Attestation(s) déjà envoyée(s) au client : '+getAlreadySendAttestations(lstSelectAtt)));
		
		}else if(isNotStatusGenerated(lstSelectAtt) != null && isNotStatusGenerated(lstSelectAtt).size() > 0) { 
			isNoSelectedAtt = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,' Attestation(s) non générée(s) : '+isNotStatusGenerated(lstSelectAtt)));
	
		}else if (getEmailAddresBounced(lstSelectAtt)!= null && getEmailAddresBounced(lstSelectAtt).Size() > 0) { //Check if we have Bounced Emails
			isNoSelectedAtt = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,' Il y a des rebonds sur les adresses mails des attestations suivantes :  '+ getEmailAddresBounced(lstSelectAtt)));
		}else if (getAttestationsWithoutFile(lstSelectAtt)!= null && getAttestationsWithoutFile(lstSelectAtt).Size() > 0) { //Check if we have Bounced Emails
			isNoSelectedAtt = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,' Les attestations suivantes n\'ont pas de fichiers liés :  '+ getAttestationsWithoutFile(lstSelectAtt).toString()));
		}else if (isExistMultipleFilesOnAttest(lstSelectAtt)!= null && isExistMultipleFilesOnAttest(lstSelectAtt).Size() > 0) { //Check if we have Bounced Emails
			isNoSelectedAtt = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,' Les attestations suivantes ont plusieurs fichiers liés :  '+ isExistMultipleFilesOnAttest(lstSelectAtt).toString()));
		}
	
	
	}
	
	public List<Contact> getContacts(Set<Id> ids){
		return [SELECT Id,Name,EMail FROM Contact WHERE Id IN : ids];
	}
	
	//Get the salesResp's Assistants Emails
	public Map<Id,List<String>> getAssistantEmails(Set<Id> ids){
		Map<Id,List<String>> mapSalesRespAssist = new Map<Id,List<String>> ();	
		List <User> lstSalesResp = [SELECT Name, Email, Assistant__r.Email FROM User WHERE id IN :ids];
	
		for(User u : lstSalesResp){
			mapSalesRespAssist.put(u.id,new List<String>{u.Assistant__r.Email, u.Email});
		}

		System.debug(mapSalesRespAssist);

		return mapSalesRespAssist ;
	}
	
	//Set Map Contacts and Attestations
	public Map<Id,Attestation__c> setMapContactAndAttestation(List<Attestation__c> lstAtt){
		Map<Id,Attestation__c> mapContactAttest = new Map<Id,Attestation__c> ();	
		
		for(Attestation__c a : lstAtt){
			mapContactAttest.put(a.Contact__r.id,a);
		}
		return mapContactAttest ;
	}
	
	//Set Map Contacts and Attestations
	public Map<Id,List<Attestation__c>> setMapContactAndListAttestation(List<Attestation__c> lstAtt){
		Map<Id,List<Attestation__c>> mapContactLstAttest = new Map<Id,List<Attestation__c>> ();	
		
		for(Attestation__c a : lstAtt){
			if(!mapContactLstAttest.containsKey(a.Contact__r.id)) {
				mapContactLstAttest.put(a.Contact__r.id, new list<Attestation__c> {a});
			}else{
				mapContactLstAttest.get(a.Contact__r.id).add(a);
			}
		}
		return mapContactLstAttest ;
	}
	
	public Map<Id,List<Messaging.Emailfileattachment> > getAttachementsTosendByContactEmail(List<Attestation__c> lstSelectAtt){
		Map<Id,ContentDocument> filesMapOnContactId = new  Map<Id,ContentDocument>();
		Map<String,List<Attestation__c> > mapContactAttestations = new  Map<String,List<Attestation__c> >();
		Map<Id,List<Messaging.Emailfileattachment> > resultMap = new Map<Id,List<Messaging.Emailfileattachment> >();
	
		//Get Attestation__s Files ContentDocumentId
		Set<Id> lstFilesContentDocumentIds = new Set<Id>();
		for(Attestation__c a : lstSelectAtt) {
			lstFilesContentDocumentIds.add(a.ContentDocumentLinks[0].ContentDocumentId);  //We will send only one file
		}

		System.debug(lstFilesContentDocumentIds);
	
		//Get attachments by contact
		for(Attestation__c a :lstSelectAtt) {
			if(!mapContactAttestations.containsKey(a.Contact__r.Id)) {
				mapContactAttestations.put(a.Contact__r.Id, new list<Attestation__c> {a});
			}else{
				mapContactAttestations.get(a.Contact__r.Id).add(a);
			}
		}
	
		System.debug('mapContactAttestations----'+mapContactAttestations);
		// Get Conversion files
		List<ContentVersion> lstCversion = [SELECT title, PathOnClient, FileType, versiondata, FileExtension, ContentDocumentId FROM contentversion WHERE ContentDocumentId IN : lstFilesContentDocumentIds  Order By CreatedDate desc];
	
		//map ContentVersion with files by ContentDocumentId
		for(ContentVersion cv : lstCversion) {
			fileContVersionMap.put(cv.ContentDocumentId, cv);
		}
	
		System.debug('fileContVersionMap----'+fileContVersionMap);
	
		//Create attachements Contact map
		for(List<Attestation__c> lisAss : mapContactAttestations.values()) {
			String contactId;
			List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
			for(Attestation__c a : lisAss) {
				Messaging.Emailfileattachment fileAttachment = new  Messaging.Emailfileattachment();
				ContentVersion cversion = new ContentVersion ();
				//Get the ContentVersion within the Attestation__c File ContentDocumentId
				cversion = fileContVersionMap.get(a.ContentDocumentLinks[0].ContentDocumentId);
				System.debug(cversion);
				if(cversion != null) {
					fileAttachment.setFileName(cversion.title);
					fileAttachment.setBody(cversion.versiondata);
					if(!fileAttachments.contains(fileAttachment)) {
						fileAttachments.add(fileAttachment);
					}
					contactId = a.Contact__r.Id; //its the same in this list
				}
	
			}
			//Add to map
			resultMap.put(contactId,fileAttachments);
			System.debug(resultMap);
		}
	
		return resultMap;
	}
	
	public List<String> isNotSortedOrMixed (List<Attestation__c> lstSelectAtt){
		List<String> listAttestationsSortedOrMixed = new List<String> ();
		for(Attestation__c a : lstSelectAtt) {
			if( a.Sorted__c==false && a.Mixed__c==false ) {
				listAttestationsSortedOrMixed.add(a.Name);
			}
		}
		return listAttestationsSortedOrMixed;
	}
	
	public List<String> isNoWasteChecked (List<Attestation__c> lstSelectAtt){
		List<String> listAttestationsSortedOrMixed = new List<String> ();
		for(Attestation__c a : lstSelectAtt) {
			if( a.Papercardboard__c==false && a.Plastic__c==false && a.Metal__c==false && a.Glass__c==false && a.Wood__c==false && a.MineralFractions__c ==false && a.Plaster__c == false) {
				listAttestationsSortedOrMixed.add(a.Name);
			}
		}
		return listAttestationsSortedOrMixed;
	}
	
	//   Method for sending Emails.
	//   Params : List<Attestation__c> selectedAttestation__s, Map<ContactId,List<Emailfileattachment>> ,
	
	public pageReference sendEmails(){
		System.debug('sendEmails ---> Start : '+lstSelectAtt);
	
		Map<Id, List<Id>> mapAttestContact = new Map<Id, List<Id>>();

		List<Id> attIds = new List<Id>();
		for(Attestation__c a: lstSelectAtt) {
			attIds.add(a.Id);
		}

		List<ContentDocumentLink> lstLink = [SELECT ID, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN :attIds];

		Map<Id, ContentDocumentLink> mapLinkAttest = new Map<Id, ContentDocumentLink>();

		for(ContentDocumentLink lnk: lstLink) {
			mapLinkAttest.put(lnk.LinkedEntityId, lnk);
		}

		system.debug(lstLink);

		List<Id> docIds = new List<Id>();
		for(ContentDocumentLink lnk: lstLink) {
			docIds.add(lnk.ContentDocumentId);
		}

		system.debug(docIds);

		Map<Id, ContentVersion> mapVerLink = new Map<Id, ContentVersion>();

		List<ContentVersion> lstVer = [SELECT ID, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN :docIds];

		for(ContentVersion ver: lstVer) {
			mapVerLink.put(ver.ContentDocumentId, ver);
		}

		Map<Id, ContentVersion> mapVerAttes = new Map<Id, ContentVersion>();

		for(Attestation__c a: lstSelectAtt) {

			ContentDocumentLink lnk = mapLinkAttest.get(a.Id);
			ContentVersion ver = mapVerLink.get(lnk.ContentDocumentId);

			mapVerAttes.put(a.Id, ver);

			if(mapAttestContact.containsKey(a.Contact__r.Id)) {
				List<Id> ids = mapAttestContact.get(a.Contact__r.Id);
				ids.add(ver.Id);
				mapAttestContact.put(a.Contact__r.Id, ids);
			} else {
				mapAttestContact.put(a.Contact__r.Id, new List<Id>{ver.Id});
			}

		}

		system.debug(mapAttestContact);

		Map<Id, List<String>> mapSalesContact = new Map<Id, List<String>>();
		for(Attestation__c a: lstSelectAtt) {
			if(mapSalesContact.containsKey(a.Contact__r.Id)) {
				List<String> ids = mapSalesContact.get(a.Contact__r.Id);
				ids.add(a.AssociatedSalesRep__r.Email);
				mapSalesContact.put(a.Contact__r.Id, ids);
			} else {
				mapSalesContact.put(a.Contact__r.Id, new List<String>{a.AssociatedSalesRep__r.Email});
			}
		}

		List<Task> lstTaskToCreate = new  List<Task>();
		List<ContentDocumentLink> lstContentDocumentLinksToCreate = new  List<ContentDocumentLink>(); 
		Map<Id,List<Messaging.Emailfileattachment> > filesMapOnContactId = getAttachementsTosendByContactEmail(lstSelectAtt);
		List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
		Boolean EmailDeliverabilityEnabled = true;
	
		Set<Id> contactIds = new Set<Id>();
		Set<Id> salesRepIds = new Set<Id>();
	
	//Get contacts ids
		for(Attestation__c a :lstSelectAtt ) {
			contactIds.add(a.Contact__r.Id);
		}
	
			List<Contact> contacts = getContacts(contactIds);
	
			//Get SalesRep ids
			for(Attestation__c a :lstSelectAtt ) {
					salesRepIds.add(a.AssociatedSalesRep__c);
				}
	
		 Map<Id,List<String>> mapSalesRespAssist = getAssistantEmails(salesRepIds);
	
			 System.debug('sendEmails ---> mapSalesRespAssist : '+mapSalesRespAssist);
	
		 //Map  Attestation on contact
		 Map<Id,Attestation__c> mapContactAndAttestation = setMapContactAndAttestation(lstSelectAtt);
	
		 // get All contact's seleted Attestation
			Map<Id,List<Attestation__c>> mapContactLstAttest = setMapContactAndListAttestation(lstSelectAtt);
	
			 System.debug('sendEmails ---> mapContactAndAttestation : '+mapContactAndAttestation);
	
		for(Contact c : contacts) {//send emails by contact
			Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
			String assistEmail ='';
			String salesRep = '';
			message.ccaddresses = new List<String>() ;
	
			if(mapContactAndAttestation != null){
				salesRep = mapSalesRespAssist.get(mapContactAndAttestation.get(c.Id).AssociatedSalesRep__c) != null ?  mapSalesRespAssist.get(mapContactAndAttestation.get(c.Id).AssociatedSalesRep__c)[1] : '';
				assistEmail = mapSalesRespAssist.get(mapContactAndAttestation.get(c.Id).AssociatedSalesRep__c) != null ?  mapSalesRespAssist.get(mapContactAndAttestation.get(c.Id).AssociatedSalesRep__c)[0] : '';
				//if(String.isNotBlank(assistEmail) && String.isNotBlank(salesRep)){
					//message.bccaddresses = new String[] {assistEmail};
				//}
			}	
	
		/*	String saleRepEmail = mapContactAndAttestation.get(c.Id).AssociatedSalesRep__r.Email != null ? mapContactAndAttestation.get(c.Id).AssociatedSalesRep__r.Email : '';
				if(String.isNotBlank(saleRepEmail)){
					//message.ccaddresses.add(saleRepEmail);
					message.bccaddresses = new String[] {saleRepEmail};
				} */
			

			System.debug('message.ccaddresses : '+message.ccaddresses);
			System.debug('filesMapOnContactId.get(c.Id)	: '+filesMapOnContactId.get(c.Id));
			
			message.setEntityAttachments(mapAttestContact.get(c.Id));
			//message.setFileAttachments(filesMapOnContactId.get(c.Id));//getting contact's email attachments
			message.setReplyTo(UserInfo.getUserEmail());
			message.toAddresses = new String[] {c.Email};	
			
                message.ccaddresses =(assistEmail != null) ? new String[] {salesRep, assistEmail}  :new String[] {salesRep} ;
			message.subject = Constants.massEmailSubject(Constants.yearOfAttestations);
			message.setSenderDisplayName(UserInfo.getName());
			message.htmlBody = Constants.massEmailBody(Constants.yearOfAttestations);
			message.setTargetObjectId(c.Id);
			message.setSaveAsActivity(true);
			mails.add(message);
	
			//create task to link to the attestation record	
		
	
			/*for(Task ta : createEmailActivities(message.subject,message.htmlBody,c.Id,mapContactLstAttest)){
				lstTaskToCreate.add(ta);
			} */
			
	
			System.debug('sendEmails ---> message : '+message);
		}
		try {
			Messaging.sendEmail(mails);
			Map<Id, Task> mapTask = createEmailActivities(Constants.massEmailSubject(Constants.yearOfAttestations), Constants.massEmailBody(Constants.yearOfAttestations), lstSelectAtt);
			insert mapTask.values();
			List<ContentDocumentLink> lstLinks = linkQuoteFilesToTasks(lstSelectAtt, mapTask, mapVerAttes);
			insert lstLinks;
		} catch (Exception e) {
			System.debug('Error : '+e);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,' Une erreur est survenue, contacter votre administrateur.'+ e));
			EmailDeliverabilityEnabled = false;
		}
	
		//Update Attestation if email sendding ok
		if(EmailDeliverabilityEnabled ) {
			try {
				updateAttestation(lstSelectAtt);
				isNoSelectedAtt = true;
			 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,' Attestation(s) envoyée(s)...'));		
				
			} catch (DmlException e) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,' L\' erreur suivante est survenue, pendant l\'envoi des attestations : '+ e.getMessage()));
			}
		}
	
	return stdSetController.cancel();
	
	}
	
	
	private Map<Id, Task> createEmailActivities(String subject,String description, List<Attestation__c> mapContactLstAttest) {
	
		Map<Id, Task> mapTask = new Map<Id, Task>();
	
		for(Attestation__c at : mapContactLstAttest){
	
				Task t = new Task(
				Subject = subject,
				Description = description.stripHtmlTags(),
				WhatId = at.Id,
				TaskSubtype = 'Email',
				ActivityDate = Date.today(),
				Status = 'Achevée');
				mapTask.put(at.Id, t);		
			
		}
		return mapTask;
	}   
	
	private List<ContentDocumentLink> linkQuoteFilesToTasks(List<Attestation__c> lstAtt, Map<Id, Task> mapAttTask, Map <Id,ContentVersion> mapVerAttes) {
	
		List<ContentDocumentLink> lstDocLink = new List<ContentDocumentLink> ();
		//get Attestation throught task whoId (= ContactId)
		for(Attestation__c at : lstAtt){
			
				Task t = mapAttTask.get(at.Id);
				ContentVersion cversion = new ContentVersion ();
				//Get the ContentVersion within the Attestation__c File ContentDocumentId
				cversion = mapVerAttes.get(at.Id);
		
				// Create Related file
				ContentDocumentLink cdl = new ContentDocumentLink();	
				cdl.ContentDocumentId = cversion.ContentDocumentId;
				cdl.LinkedEntityId = t.Id;
				cdl.ShareType = 'V';
				lstDocLink.add(cdl);
			
		}
			return lstDocLink;
	}
	
	//-----------------------------------Attestation methods-----------------------//
	
	public List<String> isAttestationAlreadyGenerated(List<Attestation__c> lstSelectAtt){
		List<String> listAttestationsAlreadyGen = new List<String> ();
		for(Attestation__c a : lstSelectAtt) {
			if( a.Status__c == Constants.ATTESTATION_GENERATE_STATUS || !a.ContentDocumentLinks.isEmpty()) {
				listAttestationsAlreadyGen.add(a.Name);
			}
		}
	
		return listAttestationsAlreadyGen;
	}
	
	public List<String> isAttestationNotConcerned(List<Attestation__c> lstSelectAtt){
		List<String> listAttestationsNotConcerned = new List<String> ();
		for(Attestation__c a : lstSelectAtt) {
			if( a.Status__c == Constants.ATTESTATION_NOTCONCERNED_STATUS) {
				listAttestationsNotConcerned.add(a.Name);
			}
		}
	
		return listAttestationsNotConcerned;
	}
	
	public void initAttestations(){
		System.debug('List Selected Records : '+lstSelectAtt);
	
		if(isAttestationNotConcerned(lstSelectAtt) != null && isAttestationNotConcerned(lstSelectAtt).size() > 0) {
			isNoSelectedAtt = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,' Attestation(s) non concernée(s) : '+isAttestationNotConcerned(lstSelectAtt)));
		}else if(isAttestationAlreadyGenerated(lstSelectAtt) != null && isAttestationAlreadyGenerated(lstSelectAtt).size() > 0) {
			isNoSelectedAtt = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,' Attestation(s) déjà générée(s) : '+isAttestationAlreadyGenerated(lstSelectAtt)));
		}else if (lstSelectAtt != null && lstSelectAtt.Size() == 0) {
			isNoSelectedAtt = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,' Veuillez sélectionner une attestation à générer !'));
		}else if (isNotSortedOrMixed(lstSelectAtt)!= null && isNotSortedOrMixed(lstSelectAtt).Size() > 0) { //Check if Sorted__c and Mixed__c are not checked
			isNoSelectedAtt = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,' Veuillez choisir entre \"Triés\" et \"En mélange\" pour les attestations suivantes :  '+ isNotSortedOrMixed(lstSelectAtt).toString()));
		}else if (isNoWasteChecked(lstSelectAtt)!= null && isNoWasteChecked(lstSelectAtt).Size() > 0) { //Check if none of the waste are checked
			isNoSelectedAtt = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,' Veuillez choisir un déchet si \"Triés\", ou plusieurs si \"En mélange\", pour les attestations suivantes :  '+ isNoWasteChecked(lstSelectAtt).toString()));
		}
	}
	
	
	
	public void generateAttestations(){
		System.debug('sendEmails ---> Start : '+lstSelectAtt);
		List<Attestation__c> lstSelectAttToUpdate = new List<Attestation__c> ();
	
		for(Attestation__c a : lstSelectAtt) {
			a.TECH_CongaAttestationToGenerate__c = true;
			lstSelectAttToUpdate.add(a);
		}
		try {		
			update lstSelectAttToUpdate;
			isNoSelectedAtt = true;
			 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,' Votre attestation est en cours de génération. Veuillez actualiser la page dans quelques instants pour accéder aux documents dans la liste des Fichiers.'));
		} catch (DmlException e) {		
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,' L\' suivante est survenue, pendant la mise à jour des status à envoyé : '+ e.getMessage()));
		}
	   // return stdSetController.cancel();
	}
	
	
	public pageReference returnBack(){
		return stdSetController.cancel();
	}
	
	public Boolean getIsNoSelectedAtt()
	{
		return isNoSelectedAtt;
	}
	
	public Boolean getIsGenerateProcessOk()
	{
		return isGenerateProcessOk;
	}
	
	}