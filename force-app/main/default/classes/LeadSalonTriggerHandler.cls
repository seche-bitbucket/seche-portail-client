/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Ramatoulaye GUEYE  <ramatoulaye.gueye@capgemini.com>
* @modifiedBy     Ramatoulaye GUEYE  <ramatoulaye.gueye@capgemini.com>
* @maintainedBy   Ramatoulaye GUEYE  <ramatoulaye.gueye@capgemini.com>
* @version        1.0
* @created        2018-08-2
* @modified       2018-09-05
     
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/


public class LeadSalonTriggerHandler {
    
    public static void isAfterInsert(List<lead> leads){
        system.debug('nom du lead'+ leads);
        Id devRecordTypeId = Schema.SObjectType.lead.getRecordTypeInfosByName().get('LED_PISTES_SALON').getRecordTypeId();
	
        for( lead l : leads){
            //Create even when contact_person__c!=Null
                  if (l.RecordTypeId ==devRecordTypeId && l.contact_person__c!=Null){
                      system.debug(' l.Id '+ l.Id);
                      Event e = new Event();
                      //e.DurationInMinutes=10;
                      e.StartDateTime= system.now();
                  
                      system.debug('  e.StartDateTime '+   e.StartDateTime);
                      e.Subject='Salon';
                      e.EndDateTime=system.now()+(10/1440);
                      system.debug(' e.EndDateTime '+e.EndDateTime);
                      e.OwnerId=l.contact_person__c;
                      e.whoId=l.id;
                      system.debug(' e '+e);
                      Insert e;
					  
                }
        }
    }
    // add Lead on campaign 
    
    public static void addLeadToCompaign(List<lead> leads){
        Id devRecordTypeId = Schema.SObjectType.lead.getRecordTypeInfosByName().get('LED_PISTES_SALON').getRecordTypeId();
        for( lead l : leads){
            List<Campaign> campaign = [select id, name from campaign where name=:l.Salon_Name__c];
               system.debug('campaign.size()'+campaign.size());
            if (campaign.size()!=0 && l.Salon_Name__c!=null){
                system.debug('test______');
                if (l.RecordTypeId ==devRecordTypeId && l.Salon_Name__c==campaign.get(0).name){
                    CampaignMember newCM = new CampaignMember();
                    
                    
                    newCM.campaignId = campaign.get(0).id;
                    newCM.leadId=l.id;
                    
                    insert newCM;
                    
                }
            }
        }
    }
    

}