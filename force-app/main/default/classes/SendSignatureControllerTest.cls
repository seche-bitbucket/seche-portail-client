/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-08-08
* @modified       2018-08-08
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for lightning component controller to send signature
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class SendSignatureControllerTest {
    @isTest static void Test(){
        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '01000';
        a.NAF_Number__c = '1234A';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        //c.Salesman__c = director.Id;
        c.Email = 'aaa@groupe-seche.com';
        c.Phone = '03 00 00 00 00';
        insert c;
        Opportunity opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        Quote q = new Quote();
        q.OpportunityId = opp.ID;
        q.Name = opp.Name;
        q.DateDevis__c = Date.today();
        q.QuoteCode__c=opp.Name;
        q.ContactId = opp.ContactName__c;
        q.Email = opp.ContactName__r.Email;
        q.Phone = opp.ContactName__r.Phone;
        q.Fax = opp.ContactName__r.Fax;
        q.TECH_UID__c='9001';
        q.BillingStreet = opp.Account.BillingStreet;
        q.BillingCity = opp.Account.BillingCity;
        q.BillingState = opp.Account.BillingState;
        q.BillingPostalCode = opp.Account.BillingPostalCode;
        q.BillingCountry = opp.Account.BillingCountry; 
        insert q;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new mockHttpResponseSendForSignature());
        SendSignatureController ssc=new SendSignatureController();
        SendSignatureController.getQuoteId( q.Id, '9001');
        SendSignatureController.getDocumentForSignature(q.Id,'');
        SendSignatureController.SendForSignature(q.Id,'Name','LastName','email@email.email','0100000000','Unknown');
        SendSignatureController.getContact('0031w000001UXviAAG','');
        ssc.ID='';
        ssc.Email='';
        ssc.FirstName='';
        ssc.LastName='';
        ssc.Phone='';
        ssc.UID='9001';
        ssc.ContactId=c.Id;
        Test.StopTest();
    }
}