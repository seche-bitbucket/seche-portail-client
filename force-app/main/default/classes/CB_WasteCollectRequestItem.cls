/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Mohammed Amine IDRISSI TAGHKI <mohammed-amine.idrissi-taghki@capgemini.com>
 * @version        1.0
 * @created        2021-09-14
 * @systemLayer    Extend Base Model
 *
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Desciption: Child class extends base model
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

 public with sharing class CB_WasteCollectRequestItem extends PolyMorphic_BaseClass {

    override
    public Object VGetRecord(String recordId){
        try {

            Boolean locked = Approval.isLocked(recordId);

            List<WasteCollectRequestItem__c> lstItems = [SELECT Id, Name, TECH_OutOfMarket__c, Quantite__c, ContainerNumber__c, Packaging__c, Product__r.Name, Product__r.ADR__c, Product__r.N_CAP__c, Product__r.Packaging__c FROM WasteCollectRequestItem__c WHERE Demande_Enlevement__c = :recordId];
            
            if(!locked) {
                for(WasteCollectRequestItem__c item: lstItems) {
                    if(item.Packaging__c == null) {
                        item.Packaging__c = item.Product__r.Packaging__c;
                    }
                }

                update lstItems;
            }

            return [SELECT Id, Name, Quantite__c, ProductName__c, CAP__c, ADR__c, TECH_OutOfMarket__c, ContainerNumber__c, Packaging__c, Product__r.Name, Product__r.ADR__c, Product__r.N_CAP__c, Product__r.Packaging__c FROM WasteCollectRequestItem__c WHERE Demande_Enlevement__c = :recordId];

        } catch (Exception e) {

            Map<String, Object> errorMap = new Map<String, Object>();
            errorMap.put('message', e.getMessage());
            errorMap.put('line', e.getLineNumber());
            errorMap.put('source', 'CB_WasteCollectRequestItem.VGetRecord');
            errorMap.put('inputs', new Map<String, String>{'recordId' => recordId});
            return errorMap;

        }
    }

    override
    public Object VSave(String metadata, String dataType) {

        List<WasteCollectRequestItem__c> lstWcri = (List<WasteCollectRequestItem__c>)super.VSave(metadata, dataType);

        List<Id> ids = new List<Id>();

        for(WasteCollectRequestItem__c wcri: lstWcri) {
            ids.add(wcri.Id);
        }

        lstWcri = [SELECT Id, Product__r.TECH_SpecialInstruction__c, 
                            Product__r.Description, Demande_Enlevement__c,
                            Demande_Enlevement__r.CallDate__c, 
                            Demande_Enlevement__r.Date_Enlevement__c, 
                            Demande_Enlevement__r.Account__c, 
                            Demande_Enlevement__r.RequestType__c, 
                            Quantite__c,
                            Demande_Enlevement__r.TECH_SpecialInstruction__c from WasteCollectRequestItem__c WHERE Id in :ids];

        String instructions;

        for(WasteCollectRequestItem__c wcri: lstWcri) {

            if(wcri.Product__r.TECH_SpecialInstruction__c && wcri.Quantite__c > 0) {

                if(instructions != null && !instructions.contains(wcri.Product__r.Description)) {
                        instructions += ' | '+wcri.Product__r.Description;
                } else {
                    instructions = wcri.Product__r.Description;
                }

            }

        }

        system.debug('instructions '+ instructions);

        WasteCollectRequest__c wcr = new WasteCollectRequest__c(CallDate__c=lstWcri[0].Demande_Enlevement__r.CallDate__c,
                                                                Date_Enlevement__c=lstWcri[0].Demande_Enlevement__r.Date_Enlevement__c, 
                                                                Account__c=lstWcri[0].Demande_Enlevement__r.Account__c,
                                                                RequestType__c=lstWcri[0].Demande_Enlevement__r.RequestType__c,
                                                                Id=lstWcri[0].Demande_Enlevement__c,
                                                                TECH_SpecialInstruction__c=instructions);
        upsert wcr;

        return lstWcri;

    }

}