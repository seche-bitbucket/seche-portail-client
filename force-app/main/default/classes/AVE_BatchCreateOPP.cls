/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-06-29
* @systemLayer    Batch Class         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Batch Class to create opportunities for contracts
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global class AVE_BatchCreateOPP implements Database.Batchable<sObject>{

    Boolean isRegroupmentIndexError = false;

    global Database.QueryLocator start(Database.BatchableContext BC)
    {	// retrieve contracts that don't have an opportunity linked to them
        
        String query = 'SELECT IncreaseComment__c, Opportunity__c, Etat__c, Type_de_convention__c,Name,Account__c,AssociatedSalesRep__c,AssociatedSalesRep__r.Trigramme__c, Filiale__c, Filiere__c, RegroupmentIndex__c,ContactRelated__c,ContactRelated__r.Actif__c, Id,Account__r.Name,Status__c,ContractEndDate__c,Face_Face__c,(SELECT ThirdPartyProducerName__c,Indexation_gasoil__c,Communal_tax__c FROM Avenants__r) FROM Contract__c WHERE Opportunity__c= Null AND Status__c = \'Valide\' AND IsUnderContract__c = false AND TECH_Excluded__c = false AND RegroupmentIndex__c = null ORDER BY Account__c';
        return Database.getQueryLocator(query);
    }


    global void execute(Database.BatchableContext BC, List<Contract__c> scope)
    {	
        // Map the contracts according to the associated User
        String OppPrefix=scope[0].Etat__c;//'Convention';
        // retrieve the pricebook entries we need to create the opportunity line items
        Pricebook2 pricebook=new Pricebook2();    

        List<FilialeCodeMapping__mdt> filialeCodes = [select Code__c , Filiale__c from FilialeCodeMapping__mdt];
        Map<String,String> filialeCodeMap = new Map<String , String>();
        for(FilialeCodeMapping__mdt temp:filialeCodes ){
            filialeCodeMap.put(temp.Filiale__c, temp.Code__c);
        }
        
        if (Test.isRunningTest())   {
            pricebook = new Pricebook2(Id=Test.getStandardPricebookId());
        }else{
            pricebook=getStdPriceBook(); 
        }
        PricebookEntry  traitement=[SELECT IsActive, UnitPrice, Product2Id, Pricebook2Id, Name, Id, UseStandardPrice FROM 
                                    PricebookEntry WHERE Name=:'Autre Traitement' AND  IsActive = true AND Pricebook2Id=:pricebook.ID  LIMIT 1];
        PricebookEntry  prestation=[SELECT IsActive, UnitPrice, Product2Id, Pricebook2Id, Name, Id, UseStandardPrice FROM 
                                    PricebookEntry WHERE Name=:'Autre Prestation' AND  IsActive = true AND Pricebook2Id=:pricebook.ID  LIMIT 1];
        PricebookEntry  conditionnement=[SELECT IsActive, UnitPrice, Product2Id, Pricebook2Id, Name, Id, UseStandardPrice FROM
                                    PricebookEntry WHERE Name=:'Autre Conditionnement' AND IsActive = true AND Pricebook2Id=:pricebook.ID  LIMIT 1];
        PricebookEntry  valorisation=[SELECT IsActive, UnitPrice, Product2Id, Pricebook2Id, Name, Id, UseStandardPrice FROM
                                    PricebookEntry WHERE Name=:'Autre Valorisation' AND IsActive = true AND Pricebook2Id=:pricebook.ID  LIMIT 1];
        Id traitementId=traitement.Id;
        Id prestationId=prestation.Id;
        Id conditionnementId=conditionnement.Id;
        Id valorisationId=valorisation.Id;
        String Nextyear = System.Label.YearN1;
        List<Amendment__c> AmendmentList=new List<Amendment__c> ();
        // retrieve amendments linked the the contracts retrieved 
        //AmendmentList =  [SELECT Contrat2__c,UnitNextYear__c,ByPassContract__c,IncludedTGAP__c,QuantityNextYear__c,Nature__c,PriceNextYear__c,WasteLabel__c,Indexation_gasoil__c,Communal_tax__c FROM Amendment__c WHERE Contrat2__c IN:scope];
        Map<Contract__c,Decimal> Indexes = new Map<Contract__c,Decimal>();
        List<Opportunity> OppToInsert=new List<Opportunity>();
        List<Contract__c> ContractsToUpdate=new List<Contract__c>();
        List<OpportunityLineItem> OliToInsert=new List<OpportunityLineItem>();
        map<Id,String> OppContractMap=new map<Id,String>();
        for(Contract__c contract : scope)
        {
            if(contract.ContactRelated__r.Actif__c) {
                if(contract.RegroupmentIndex__c!=null){
                    Indexes.put(contract,contract.RegroupmentIndex__c);
                }else{
                    String ReferenceWasteOwner='';
                    Boolean FuelPriceIndexation = false;
                    Boolean CommunalTax = false;
                    if(Contract.Avenants__r.size()>0){
                        ReferenceWasteOwner=contract.Avenants__r[0].ThirdPartyProducerName__c;
                        FuelPriceIndexation=contract.Avenants__r[0].Indexation_gasoil__c;
                        CommunalTax=contract.Avenants__r[0].Communal_tax__c;
                    }
                    //String Faf = (contract.Face_Face__c==true?' - FàF':'');
                    String NameOpp;
                    if(ReferenceWasteOwner != null){
                        NameOpp = OppPrefix+' '+Nextyear+' - '+filialeCodeMap.get(contract.Filiale__c)+' - '+contract.Account__r.Name+' - '+ReferenceWasteOwner;
                        if(NameOpp.length() > 120){ //Limit Opportunity Name length exceeded
                            NameOpp = OppPrefix+' '+Nextyear+' - '+filialeCodeMap.get(contract.Filiale__c)+' - '+contract.Account__r.Name;
                        }
                    }else{
                        NameOpp = OppPrefix+' '+Nextyear+' - '+filialeCodeMap.get(contract.Filiale__c)+' - '+contract.Account__r.Name;
                    }
                    Opportunity opp=new Opportunity(Name=NameOpp,
                                                    AccountId=contract.Account__c,CloseDate = date.today(),
                                                    StageName ='Proposition commerciale',TECH_Contrat2__c=contract.Id,ContractComment__c=contract.IncreaseComment__c,
                                                    ContactName__c=contract.ContactRelated__c,Salesman__c=contract.AssociatedSalesRep__c,
                                                    Filiale__c=contract.Filiale__c,Filiere__c=contract.Filiere__c,Nature__c='Renouvellement',
                                                    OwnerId=contract.AssociatedSalesRep__c,
                                                    ConventionType__c=contract.Type_de_convention__c,
                                                    ReferenceWasteOwner__c=ReferenceWasteOwner,                                             
                                                    Convention_face_face__c = contract.Face_Face__c,
                                                    DateFinEngagement__c= contract.ContractEndDate__c,
                                                    FuelPriceIndexation__c= FuelPriceIndexation ,
                                                    Communal_tax__c = CommunalTax,TECH_IsFromTarifIncrease__c = true,
                                                    DateRemiseOffre__c=date.newInstance(Integer.valueOf(System.Label.YearN1), 1, 1));
                    
                    OppToInsert.add(opp);
                }
            }
        }
        List<Contract__c> listc=new List<Contract__c>();
        listc.addAll(Indexes.KeySet());
        Map<Integer,List<Contract__c>> ContractMap=new Map<Integer,List<Contract__c>>();
        List<Contract__c> ContractsToList=new List<Contract__c>();
        for(Integer i=0;i<Indexes.values().size();i++){
            if(i<1){
                ContractsToList.add(listc[0]);
            }else{
                if(Indexes.values()[i]==Indexes.values()[i-1]){
                     if((listc[i].Account__c==listc[i-1].Account__c)&&  (listc[i].Filiale__c==listc[i-1].Filiale__c)
                        && (listc[i].Filiere__c==listc[i-1].Filiere__c) && (listc[i].ContactRelated__c==listc[i-1].ContactRelated__c)){ 
                        ContractsToList.add(listc[i]);
                     }else{
                         isRegroupmentIndexError = true;
                     }                  
                }else{
                    ContractMap.put(i,ContractsToList);
                    ContractsToList=new List<Contract__c>();
                    ContractsToList.add(listc[i]);
                }
                
                if(i==Indexes.values().size()-1){
                    ContractMap.put(i,ContractsToList);
                }
            }
        }
        //System.debug(ContractMap.keyset());
        if(!isRegroupmentIndexError){
            for(Integer d:ContractMap.keyset()){
                Boolean FuelPriceIndexation = false;
                Boolean CommunalTax = false;
                String ReferenceWasteOwner='';
                if(ContractMap.get(d)[0].Avenants__r.size()>0){
                    ReferenceWasteOwner=ContractMap.get(d)[0].Avenants__r[0].ThirdPartyProducerName__c;
                    FuelPriceIndexation=ContractMap.get(d)[0].Avenants__r[0].Indexation_gasoil__c;
                    CommunalTax=ContractMap.get(d)[0].Avenants__r[0].Communal_tax__c;
                }
                //String Faf = (ContractMap.get(d)[0].Face_Face__c==true?' - FàF':'');
                String NameOpp;
                if(ReferenceWasteOwner != null){
                    NameOpp = OppPrefix+' '+Nextyear+' - '+filialeCodeMap.get(ContractMap.get(d)[0].Filiale__c)+' - '+ContractMap.get(d)[0].Account__r.Name+' - '+ReferenceWasteOwner;
                    if(NameOpp.length() > 120){ //Limit Opportunity Name length exceeded
                        NameOpp = OppPrefix+' '+Nextyear+' - '+filialeCodeMap.get(ContractMap.get(d)[0].Filiale__c)+' - '+ContractMap.get(d)[0].Account__r.Name;
                    }
                }else{
                    NameOpp = OppPrefix+' '+Nextyear+' - '+filialeCodeMap.get(ContractMap.get(d)[0].Filiale__c)+' - '+ContractMap.get(d)[0].Account__r.Name;
                }
                Opportunity opp=new Opportunity(Name=NameOpp,
                                                AccountId=ContractMap.get(d)[0].Account__c,CloseDate = date.today()+30,
                                                StageName ='Proposition commerciale',TECH_Contrat2__c=ContractMap.get(d)[0].Id,
                                                ContactName__c=ContractMap.get(d)[0].ContactRelated__c,
                                                Salesman__c=ContractMap.get(d)[0].AssociatedSalesRep__c,
                                                Filiale__c=ContractMap.get(d)[0].Filiale__c,Filiere__c=ContractMap.get(d)[0].Filiere__c,
                                                OwnerId=ContractMap.get(d)[0].AssociatedSalesRep__c,
                                                Nature__c='Renouvellement',ConventionType__c=ContractMap.get(d)[0].Type_de_convention__c,
                                                ReferenceWasteOwner__c=ReferenceWasteOwner,                                        
                                                //DateFinEngagement__c=date.newInstance(System.today().year()+1, 12, 31),
                                                DateFinEngagement__c= ContractMap.get(d)[0].ContractEndDate__c,
                                                Convention_face_face__c = ContractMap.get(d)[0].Face_Face__c,
                                                FuelPriceIndexation__c= FuelPriceIndexation ,
                                                Communal_tax__c = CommunalTax,TECH_IsFromTarifIncrease__c = true,
                                                //DateRemiseOffre__c=date.newInstance(System.today().year()+1, 1, 1));
                                                DateRemiseOffre__c=date.newInstance(Integer.valueOf(System.Label.YearN1), 1, 1));
                OppToInsert.add(opp);
            }
        }else{
            System.debug('AVE_BatchCreateOpp : erreur de regroupement, les contrats regroupés doivent avoir les même filiale, filière, compte et contact.');
            if(isRegroupmentIndexError){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            mail.setToAddresses(new String[] {'sechecrm69@gmail.com','a.lepevedic@groupe-seche.com'});
            mail.setReplyTo('sechecrm69@gmail.com');
            mail.setSenderDisplayName('AVE_BatchCreateOpp');
            mail.setSubject('Erreur de regroupement');
            mail.htmlBody = 'Les contrats regroupés doivent avoir les mêmes filiale, filière, compte et contact. Les opportunités n\'ont pas toutes été générées.';

            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
            }
        }
        
        if(OppToInsert.size()>0){
            insert OppToInsert;
        }
        List<Opportunity> Opps=[Select Name,AccountId,CloseDate,StageName,TECH_Contrat2__c,
                                ContactName__c,Salesman__c,Salesman__r.Trigramme__c,Filiale__c,Filiere__c,
                                Owner.Trigramme__c,Account.BillingStreet,Account.BillingCity,
                                Account.BillingState,Account.BillingPostalCode,Account.BillingCountry,
                                ContactName__r.Phone,Pricebook2Id,ContactName__r.Fax,ContactName__r.Email,
                                Opportunity.TECH_QuoteCount__c,ReferenceWasteOwner__c,DateFinEngagement__c,
                                DateRemiseOffre__c, Convention_face_face__c From Opportunity Where Id IN:OppToInsert];
        List<Quote> QuotesToInsert=new List<Quote>();
        RecordType r=[Select ID,Name From RecordType Where DeveloperName='Devis_approuv_Read_Only'];
        for(Opportunity opp:Opps){
            Quote vQuote = new Quote();
            vQuote.DateDevis__c = Date.today();
            if(opp.TECH_QuoteCount__c==null){
                vQuote.Name= opp.Name +' V'+ 1;
            }else{
                vQuote.Name= opp.Name +' V'+ (opp.TECH_QuoteCount__c + 1);
            }
            vQuote.RecordTypeId=r.Id;
            vQuote.ContactId=opp.ContactName__c;
            vQuote.Pricebook2Id=traitement.Pricebook2Id;
            vQuote.TECH_isApprouved__c=true;
            vQuote.OpportunityId = Opp.ID;
            vQuote.DateDevis__c = Date.today();
            vQuote.Quote_Type__c='Convention';
            vQuote.status = 'Brouillon';
            vQuote.Email = opp.ContactName__r.Email;
            vQuote.Phone = opp.ContactName__r.Phone;
            vQuote.Fax = opp.ContactName__r.Fax;
            vQuote.BillingStreet = opp.Account.BillingStreet;
            vQuote.BillingCity = opp.Account.BillingCity;
            vQuote.BillingState = opp.Account.BillingState;
            vQuote.BillingPostalCode = opp.Account.BillingPostalCode;
            vQuote.BillingCountry = opp.Account.BillingCountry; 
            vQuote.ExpirationDate=opp.DateFinEngagement__c;
            vQuote.EffectiveDate__c=opp.DateRemiseOffre__c;
            vQuote.TECH_IsFromTarifIncrease__c = true;
            vQuote.ReferenceWasteOwner__c = opp.ReferenceWasteOwner__c;
            if(opp.ReferenceWasteOwner__c == null){
                vQuote.ShippingStreet = opp.Account.BillingStreet;
                vQuote.ShippingCity = opp.Account.BillingCity;
                vQuote.ShippingState = opp.Account.BillingState;
                vQuote.ShippingPostalCode = opp.Account.BillingPostalCode;
                vQuote.ShippingCountry = opp.Account.BillingCountry;
            }
            if(opp.Pricebook2Id != null)
            {
                vQuote.Pricebook2Id = opp.Pricebook2Id;
            }
            
            QuotesToInsert.add(vQuote);
        }

        if(QuotesToInsert.size()>0){
            insert QuotesToInsert;
        }

        Map<Id,Id> QuoteOppIdMap= new Map<Id,Id>();
        for(Quote q:QuotesToInsert){
            QuoteOppIdMap.put(q.OpportunityId,q.Id);
        }

        // replacing a loop by Amine ID
        Map<String, Id> OppTechContract2OppId = new Map<String, Id>();
        for(Opportunity opp: Opps) {
            OppTechContract2OppId.put(opp.TECH_Contrat2__c, opp.Id);
        }

        // mappind indexes to opp Id
        Map<Decimal, Id> mapIndexToContract = new Map<Decimal, Id>();
        List<Contract__c> lstCwithIndex = [SELECT RegroupmentIndex__c FROM Contract__c WHERE ID IN: OppTechContract2OppId.keySet()];
        for(Contract__c c: lstCwithIndex) {
            mapIndexToContract.put(c.RegroupmentIndex__c, c.Id);
        }

        Map<Decimal, Id> mapIndexToOpp = new Map<Decimal,Id>();
        for(Decimal index: mapIndexToContract.keySet()) {
            mapIndexToOpp.put(index, OppTechContract2OppId.get(mapIndexToContract.get(index)));
        }

        for(Opportunity opp:Opps){
            // for(Quote q:QuotesToInsert){ removing loop
            if(QuoteOppIdMap.containsKey(opp.Id)) {
                // if(opp.Id==q.OpportunityId){
                    opp.SyncedQuoteId = QuoteOppIdMap.get(opp.Id);
                //     break;
                // }
            }
        }
        Update Opps;

        // regroupment => adding opp id for each contract in an regroupement
        for(Integer d:ContractMap.keyset()){
        //     Id opportunityId=null;
            for(Contract__c contract: ContractMap.get(d)){
                // for(Opportunity opp:Opps){ replaced with map lookup
                //     if(opp.TECH_Contrat2__c==contract.Id){
                //         opportunityId=opp.Id;
                //     }
                // }
                
                // if(opportunityId!=null){ not needed anymore
                //     contract.Opportunity__c=opportunityId;
                //     ContractsToUpdate.add(contract);
                // }

                if(mapIndexToOpp.containsKey(contract.RegroupmentIndex__c)) {
                    contract.Opportunity__c=mapIndexToOpp.get(contract.RegroupmentIndex__c);
                    ContractsToUpdate.add(contract);
                }
            }
        }

        for(Contract__c contract : scope)
        {
            if(contract.RegroupmentIndex__c==null){
                // for(Opportunity opp:Opps){
                    // if(opp.TECH_Contrat2__c==contract.Id){
                    if(OppTechContract2OppId.containsKey(contract.Id)){
                        OppContractMap.put(contract.Id,OppTechContract2OppId.get(contract.Id));
                        contract.Opportunity__c=OppTechContract2OppId.get(contract.Id);
                        ContractsToUpdate.add(contract);
                    }
                // }
            }
        }

        if(ContractsToUpdate.size()>0){
            Update ContractsToUpdate;
        }

        AmendmentList =  [SELECT Contrat2__c,Contrat2__r.Opportunity__c,UnitNextYear__c,ByPassContract__c,	QuantityNextYear__c ,IncludedTGAP__c,Planned_next_year__c,
                          Nature__c,PriceCurrentYear__c,PriceNextYear__c,TechnicInfo__c,Label__c,WasteLabel__c,ActifAcceptationContrat__c,CEECode__c,Conditionnement__c,OnuCode__c,
                          Class__c,Filiale__c,CommentaryAmendementsX3__c,HeaderContractX3__c,FooterContractX3__c,TextSalesX3__c,PackagingGroup__c,TECH_CatalogueMatch__c,
                          LabelArticleCode__c, Filiere__c, Name, QuantityInvoiced2018__c FROM Amendment__c WHERE TECH_Excluded__c = false AND Contrat2__c IN:ContractsToUpdate];
        
        Map<String, Contract__c> contract2Id = new Map<String, Contract__c>();
        for(Contract__c contract:Indexes.keyset()){
            contract2Id.put(contract.Id, contract);
        }
        
        for(Amendment__c Amendment:AmendmentList)
        {	
            // for(Contract__c contract:Indexes.keyset()){
            if(contract2Id.containsKey(Amendment.Contrat2__c)) {
                // if(Amendment.Contrat2__c==contract.Id){
                    OliToInsert.addall(createOpportunityLineItems(Amendment,contract2Id.get(Amendment.Contrat2__c).Opportunity__c,traitementId,prestationId,conditionnementId,valorisationId));
                // }
            }
            
            // for(Id contractid:OppContractMap.keySet()){
                if(OppContractMap.containsKey(Amendment.Contrat2__c)) {
                // if(Amendment.Contrat2__c==contractid){
                    OliToInsert.addall(createOpportunityLineItems(amendment,OppContractMap.get(Amendment.Contrat2__c),traitementId,prestationId,conditionnementId,valorisationId));
                // }
            }
        }
        if(OliToInsert.size()>0){
            insert OliToInsert;
            copyOLI2QLI(OliToInsert);
        }
    }  

    private static void copyOLI2QLI(List<OpportunityLineItem> olis) {

        System.debug('Start copyOLI2QLI -------');
        System.debug('copyOLI2QLI olis: '+olis+' -------');

        try{

            List<String> fields_lst = new List<String>();
            List<QuoteLineItem_Field__c> lstSyncedFields = [SELECT Name, OppLineSyncField__c FROM QuoteLineItem_Field__c];
            List<QuoteLineItem> listQlis = new List<QuoteLineItem>();

            Map<String, Schema.SObjectField> fs = Schema.getGlobalDescribe().get('OpportunityLineItem').getDescribe().fields.getMap();
            Map<String, Schema.SObjectField> fs_target = Schema.getGlobalDescribe().get('QuoteLineItem').getDescribe().fields.getMap();

            Map<String, Schema.SObjectField> mapFsTtoName = new Map<String, Schema.SObjectField>();
            for(Schema.SObjectField f: fs_target.values()) {
                mapFsTtoName.put(f.getDescribe().getName(), f);
            }

            // re-get olis
            List<Id> oliIds = new List<Id>();
            for(OpportunityLineItem oli: olis) {
                oliIds.add(oli.Id);
            }

            List<OpportunityLineItem> fresh_olis = Database.query('SELECT '+String.join(new List<String>(fs.keySet()), ', ')+' FROM OpportunityLineItem WHERE ID IN :oliIds');
            List<QuoteLineItem> fresh_qlis = Database.query('SELECT '+String.join(new List<String>(fs_target.keySet()), ', ')+' FROM QuoteLineItem WHERE OpportunityLineItemId IN :oliIds');

            Map<Id, QuoteLineItem> mapQlisToOlis = new Map<Id, QuoteLineItem>();
            for(QuoteLineItem qli: fresh_qlis) {
                mapQlisToOlis.put(qli.OpportunityLineItemId, qli);
            }

            for(SObject oli: fresh_olis) {
                
                // for(QuoteLineItem qli: fresh_qlis) {
                if(mapQlisToOlis.containsKey((Id)oli.get('Id'))) {

                    QuoteLineItem qli = mapQlisToOlis.get((Id)oli.get('Id'));

                    if(qli.OpportunityLineItemId == oli.Id) {

                        // add values for all standards fields
                        for(Schema.SObjectField f: fs.values()) {
                            // for(Schema.SObjectField f_t: fs_target.values()) {
                            if(mapFsTtoName.containsKey(f.getDescribe().getName())) {
                                Schema.SObjectField field = mapFsTtoName.get(f.getDescribe().getName());
                                if(field.getDescribe().getName() != 'TotalPrice' && field.getDescribe().isUpdateable()) {
                                    if(oli.get(field.getDescribe().getName()) != null) {
                                        qli.put(field.getDescribe().getName(), oli.get(field.getDescribe().getName()));
                                    }
                                }
                            }
                        }

                        // add values for all custom fileds
                        for(QuoteLineItem_Field__c qsf: lstSyncedFields) {
                            qli.put(qsf.Name, oli.get(qsf.OppLineSyncField__c));
                        }

                        // append to list
                        listQlis.add(qli);
                    }
                }
            }

            System.debug('copyOLI2QLI listQlis: '+listQlis.size()+' -------');

            upsert listQlis;

        } catch (Exception e) {
            System.debug('At Line '+e.getLineNumber()+' '+e.getMessage());
        }

        System.debug('Finished copyOLI2QLI -------');

    }

    //  -------------------------------------------------------
    //  getStdPriceBook: handle test class
    //  -------------------------------------------------------
    public static Pricebook2  getStdPriceBook () {
        Pricebook2 stdPriceBook =new  Pricebook2();
        stdPriceBook = [select id, name from Pricebook2 where isStandard = true and IsActive = true limit 1];
        return stdPriceBook;    
    }
    public List<OpportunityLineItem> createOpportunityLineItems(Amendment__c Amendment,Id OppId,Id traitementId,Id prestationId,Id conditionnementId,Id valorisationId){
        List<OpportunityLineItem> OppList=new List<OpportunityLineItem>();
        
        if(amendment.Nature__c!=null){
            
            //Ticket 01238. Pour Drimm, la filière de la ligne d'opportunité peut être différente de la filière de l'avenant
            //--> Désormais géré dans le Flow "CAT_DND_SECTOR_AFFECTATION" fait par Aurélien
            String sector;
            /*if(Amendment.Filiale__c == 'Drimm'){
                switch on Amendment.Name.substring(0,3) {
                    when 'TRI'{
                        sector = 'Centre de tri CS';
                    }
                    when 'DR4'{
                        sector = 'Centre de tri DAE';
                    }
                    when 'DRI'{
                        sector = 'Stockage DND';
                    }
                    when 'LAL' {
                        sector = 'Quai de transfert';
                    }
                }
            }else if(Amendment.Filiale__c == 'Séché Eco Industries Changé' || Amendment.Filiale__c == 'Séché Eco Industries La Dominelais' 
                                                || Amendment.Filiale__c == 'Séché Eco Industries Le Vigeant'){
                sector = Amendment.Filiere__c;
            }*/

            String priceBookEntryId;
            switch on Amendment.Nature__c {
                when 'Traitement'{
                    priceBookEntryId = traitementId;
                }
                when 'Prestation'{
                    priceBookEntryId = prestationId;
                }
                when 'Conditionnement'{
                    priceBookEntryId = conditionnementId;
                }
                when 'Valorisation' {
                    priceBookEntryId = valorisationId;
                }
            }
            
            Decimal Quantity=1.0;
            if(Amendment.UnitNextYear__c!=null && Amendment.UnitNextYear__c.toLowerCase()!='tonne' && Amendment.QuantityInvoiced2018__c!=null && Amendment.QuantityInvoiced2018__c!=0){
                Quantity=Amendment.QuantityInvoiced2018__c;
            }else if(Amendment.QuantityNextYear__c!=null && Amendment.QuantityNextYear__c!=0){
                Quantity=Amendment.QuantityNextYear__c;
            }
            Decimal price=1.0;
            if(Amendment.PriceNextYear__c!=null && Amendment.PriceNextYear__c!=0){
                price=Amendment.PriceNextYear__c;
            }
            OpportunityLineItem oli=new OpportunityLineItem(OpportunityId=OppId,Unit__c=Amendment.UnitNextYear__c,
                                                            quantity=Quantity, UnitPrice=price,PriceBookEntryID=priceBookEntryId,
                                                            Description= amendment.WasteLabel__c,Features__c=amendment.TechnicInfo__c,
                                                            BuyingPrice__c=Amendment.PriceCurrentYear__c,
                                                            N_CAP__c=amendment.ActifAcceptationContrat__c,
                                                            EuropeanCode__c=amendment.CEECode__c,
                                                            Packaging__c=amendment.Conditionnement__c,
                                                            UNCode__c=amendment.OnuCode__c,Classe__c=amendment.Class__c,
                                                            Filiale__c=amendment.Filiale__c,GE__c=amendment.PackagingGroup__c,
                                                            Nature__c=amendment.Nature__c,TECH_CatalogueMatch__c=amendment.TECH_CatalogueMatch__c,
                                                            Label__c=amendment.Label__c,CommentaryAmendementsX3__c=amendment.CommentaryAmendementsX3__c,
                                                            HeaderContractX3__c=amendment.HeaderContractX3__c,IncludedTGAP__c=amendment.IncludedTGAP__c,
                                                            Planned_next_year__c= amendment.Planned_next_year__c,
                                                            FooterContractX3__c=amendment.FooterContractX3__c,TextSalesX3__c=amendment.TextSalesX3__c,
                                                            Amendment__c=amendment.Id, LabelArticleCode__c=amendment.LabelArticleCode__c,
                                                            Sector__c=sector);
                                                            OppList.add(oli);
        }
        return OppList;
    }

    //Batch utilisé en 2018
    /* global void finish(Database.BatchableContext BC)
    {  
        if(!Test.isRunningTest()){
            CTR_BatchCourrierCleanFaF batch =new CTR_BatchCourrierCleanFaF();
            id BatchId= Database.executeBatch(batch);
        }
    }*/

    //En 2018, ces batchs étaient appelés à la fin de CTR_BatchCourrierCleanFaF
    global void finish(Database.BatchableContext BC)
    {              
        if(!Test.isRunningTest()){
            List<OpportunityLineItem> Olis=[SELECT Id, Name,TECH_CatalogueMatch__c, Features__c,N_CAP__c  FROM OpportunityLineItem Where TECH_CatalogueMatch__c !=null];
            if(olis.size()>0){
                BatchProductInfoToOLI batch =new BatchProductInfoToOLI();
                id BatchId= Database.executeBatch(batch,25);
            }else{
                CourrierConventionBatch batch =new CourrierConventionBatch();
                id BatchId= Database.executeBatch(batch,25);
            }
        }
    }
}