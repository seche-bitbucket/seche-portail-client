@isTest
global class WS004_ConnectiveMock implements  HTTPCalloutMock{
    global Static String externalId;
	global HTTPResponse respond(HTTPRequest req){
        HttpResponse res = new HTTPResponse();
        res.setHeader('Content-Type', 'application/JSON');
        res.setBody('{"ExternalReference":"a02c9f7a-cfd0-46d2-8ba3-465a5bd79fce","ConfirmedStatus":"REVOKED","ErrorLog":[]}');
        req.setMethod('GET');
        res.setStatusCode(200);
        return res;
    }
}