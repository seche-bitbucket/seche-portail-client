public with sharing class GenGUIDContactsInvitation_Pollutec {

    @InvocableMethod(label='Set GUID Contacts Invitation' description='Set a GUID passcode for Contacts Invitation')
    public static void genGuid(List<Id> contactInv) {
        Contacts_Invitation__c conInv = [SELECT Id FROM Contacts_Invitation__c WHERE Id = :contactInv[0]];
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        
        upsert new Contacts_Invitation__c(Id=contactInv[0], GUID__c=guid); 
        return;
    }
}