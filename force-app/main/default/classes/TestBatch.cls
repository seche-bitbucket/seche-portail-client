/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : LogBatchApexErrorEvent_Handler
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : Batch use to test LogBatchApexErrorEvent_Handler method
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 27-02-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
@isTest
public with sharing class TestBatch implements Database.Batchable<sObject>, Database.RaisesPlatformEvents {
    public Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id FROM Account';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<SObject> scope){
        Integer i = 1 / 0;
    }

    public void finish(Database.BatchableContext BC){ }
}