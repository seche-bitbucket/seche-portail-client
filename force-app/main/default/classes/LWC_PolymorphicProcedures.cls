/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ─────────────────────────────────────────────────────────────────────────────────────────────────|
 * @author         Mohammed Amine IDRISSI TAGHKI <mohammed-amine.idrissi-taghki@capgemini.com>
 * @version        1.0
 * @created        2021-09-14
 * @systemLayer    Controller
 *
 * ─────────────────────────────────────────────────────────────────────────────────────────────────|
 * @Desciption: Lightning web component Controller for all polymorphic generic LWC
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

public without sharing class LWC_PolymorphicProcedures {

    public static PolyMorphic_BaseClass initInstance(Object o){
        try {

            PolyMorphic_BaseClass cc = null;

            if(o instanceOf Id) {
                Id id = Id.valueOf(String.valueOf(o));
                cc = PolyMorphic_BaseClass.getInstance(String.valueOf(id.getSobjectType()));
            } else if (o instanceOf String) {
                cc = PolyMorphic_BaseClass.getInstance(String.valueOf(o));
            }

            return cc;

        } catch (Exception e) {
            System.debug(e.getLineNumber() + e.getMessage());
            return null;
        }
    }

    @AuraEnabled
    public static string getUrl(){
        return System.URL.getSalesforceBaseUrl().toExternalForm();
    }

    @AuraEnabled
    public static Object init(Object o, Id recordId){
        return initInstance(o).VInit(recordId);
    }

    @AuraEnabled
    public static Object save(Object o, String metadata, String dataType){
        return initInstance(o).VSave(metadata, dataType);
    }

    @AuraEnabled
    public static Object customInsert(Object o, String data, String apiName){
        return initInstance(o).VInsert(data, apiName);
    }

    @AuraEnabled
    public static Object getRecord(Object o, String recordId){
        return initInstance(o).VGetRecord(recordId);
    }

    @AuraEnabled
    public static Object getPickListValuesIntoList(Object o, Object obj, String field){
        return initInstance(o).SGetPickListValuesIntoList(obj, field);
    }

    @AuraEnabled
    public static Object getFieldsLabelAndValue(Object o, Id recordId, String fields){
        return initInstance(o).SGetFieldsLabelAndValue(recordId, fields);
    }

    @AuraEnabled
    public static Object getRecordTypeId(String name){
        try {
            return [SELECT Id FROM RecordType WHERE DeveloperName = :name OR Name = :name].Id;
        } catch (Exception e) {
            
            Map<String, Object> errorMap = new Map<String, Object>();
            errorMap.put('message', e.getMessage());
            errorMap.put('line', e.getLineNumber());
            errorMap.put('source', 'LC_Custom.getRecordTypeId');
            errorMap.put('inputs', new Map<String, String>{'name' => name});
            return errorMap;

        }
    }

    @AuraEnabled
    public static String getObjectApiName(Id recordId) {
        return recordId.getSobjectType().getDescribe().getName();
    }

    @AuraEnabled
    public static Object custom(Object o, String recordJSON, String action) {
        return initInstance(o).VCustomAction(recordJSON, action);
    }

}