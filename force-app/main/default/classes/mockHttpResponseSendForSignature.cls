@isTest
global class mockHttpResponseSendForSignature implements HttpCalloutMock {
    private Integer mockCount = 0;
    
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        mockCount++;
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(200);
        if(mockCount==1){
            res.setBody('{"access_token":"SESSION_ID_REMOVED","instance_url":"https://seche--DevAug.cs105.my.salesforce.com","id":"https://test.salesforce.com/id/00D1w0000008bqzEAA/00558000002XTbGAAW","token_type":"Bearer","issued_at":"1533719501281","signature":"slvA/XoQx35NjlBrePTrdrjfX1hUTgYKMPJ3zM2OESQ="}');
        }else if(mockCount==2){
            res.setBody('{"attributes":{"type":"Quote","url":"/services/data/v43.0/sobjects/Quote/0Q01w00000000dqCAA"},"TECH_UID__c":"9001","QuoteCode__c":"2018-07-27-FLEMO-1641","TECH_SendForSignature__c":true,"Id":"0Q01w00000000dqCAA"}');
        }else if(mockCount==3){
            res.setBody('{"totalSize":0,"done":true,"records":[]}');
        }else if(mockCount==4){
            res.setBody('{"access_token":"SESSION_ID_REMOVED","instance_url":"https://seche--DevAug.cs105.my.salesforce.com","id":"https://test.salesforce.com/id/00D1w0000008bqzEAA/00558000002XTbGAAW","token_type":"Bearer","issued_at":"1533719501281","signature":"slvA/XoQx35NjlBrePTrdrjfX1hUTgYKMPJ3zM2OESQ="}');
        }else if(mockCount==5){
            res.setBody('{"attributes":{"type":"Quote","url":"/services/data/v43.0/sobjects/Quote/0Q01w00000000dqCAA"},"TECH_UID__c":"9001","QuoteCode__c":"2018-07-27-FLEMO-1641","TECH_SendForSignature__c":false,"Id":"0Q01w00000000dqCAA"}');
        }else if(mockCount==6){
            
        }else if(mockCount==7){
            res.setBody('{"attributes":{"type":"Contact","url":"/services/data/v43.0/sobjects/Contact/0031w000001UXviAAG"},"FirstName":"Jean","LastName":"DUPONT","Phone":"02137798213","Email":"sechecrm69@gmail.com","Id":"0031w000001UXviAAG"}');
        }
        return res;
    }
}