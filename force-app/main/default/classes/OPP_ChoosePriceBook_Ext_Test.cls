@isTest
public class OPP_ChoosePriceBook_Ext_Test {
    
    static testMethod void testSearchProducts(){
        
        Id priceBookId;
        integer size=200;
        Integer noOfRecords;

       String[] profiLisList = new String[]{'Administrateur système', 'System Administrator'};

        //Get a profile to create User
        Profile p = [select id from profile where name in:profiLisList limit 1];
         
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        insert director;
        System.RunAs(director) {
            Account ac = new Account();
            ac.Name = 'testAccName';
            ac.CustomerNature__c = 'Administration';
            ac.Industry = 'Trader';
            ac.CurrencyIsoCode = 'EUR';
            ac.Producteur__c = false;
            ac.BillingPostalCode = '00000';
            ac.NAF_Number__c = '1234A';
            ac.SIRET_Number__c = '11111111111111';
            insert ac;
            
            Contact contactTest = new Contact();
            contactTest.LastName = 'testName';
            contactTest.AccountId = ac.Id;
            contactTest.Salesman__c = director.Id;
            contactTest.Email = 'aaa@yopmail.com';
            //contactTest.Phone = '0000000000';
            insert contactTest;
            
            Pricebook2 pb1 = new Pricebook2(Name='Pricebook1', isActive=true);
            insert pb1;
            
             Pricebook2 pb2 = new Pricebook2(Name='Pricebook2', isActive=true);
            insert pb2;
            
            Pricebook2 pb3 = new Pricebook2(Name='Pricebook3', isActive=true);
            insert pb3;
            
          
            //Get a Record Type for Opportunity
            RecordType rt = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];
            
            //Create Opportunity Mother
            Opportunity opport = new Opportunity(Name = 'OPPtest', RecordTypeId = rt.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', PriceBook2Id = pb1.Id);
            
            insert opport;
            
            Test.startTest();
            PageReference pageRef = Page.OPP_ChoosePriceBook;
            pageRef.getParameters().put('OppId',opport.Id);

            Test.setCurrentPage(pageRef);
            //Test Init controller
            OPP_ChoosePriceBook_Ext myController = new OPP_ChoosePriceBook_Ext();
            
            System.assertEquals(pb1.ID,  myController.selectedPricebook );
            
            
            //Test Save Record
            PageReference savePage =  myController.savePriceBook();
            
            System.assertEquals( '/apex/OLI_ProductSearch?OppId='+opport.Id+'&OppName='+opport.Name, savePage.getUrl() );
            
            //Test Cancel Page
            PageReference cancelPage =  myController.cancelProcess();
            
            System.assertEquals( '/'+opport.Id, cancelPage.getUrl() );
            
            //Test GetPriceBookOptions
            List<SelectOption>  so =  OPP_ChoosePriceBook_Ext.getPricebookOptions();
            System.assertNotEquals( so.size(), 0 );
            
            
        }
        
    }
    

}