@isTest
public class ContractTriggerHandlerTest {
    @isTest static void ContractTriggerTest(){

        PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'CP_AugTar_Contract'];
        Profile directp = [select id from profile where name = :'Directeur commercial' limit 1];
        String testemail2 = 'assistant_-_User_test@test.com';
        String testemail3 = 'assist_-_User_test@test.com';
        User director = new User(profileId = directp.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        
        insert director;

        User director1 = new User(profileId = directp.id, username = testemail3, email = testemail3,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        
        insert director1;

        Profile p = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];
        
        String testemail1 = 'Commercial-_User_test@test.com';
        User Commercial = new User(profileId = p.id, username = testemail1, email = testemail1,
                                   emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                   languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                   alias='cs', lastname='lastname2', IsActive=true);
        insert Commercial;

        System.runAs(new User(Id = Userinfo.getUserId())) {
            insert new PermissionSetAssignment(AssigneeId=Commercial.Id,
                    PermissionSetId=ps.Id);
        }

        Account ac = new Account();
        ac.Name = 'testAccName';
        ac.CustomerNature__c = 'Administration';
        ac.Industry = 'Trader';
        ac.CurrencyIsoCode = 'EUR';
        ac.Producteur__c = false;
        ac.BillingPostalCode = '00000';
        ac.NAF_Number__c = '1234A';
        ac.SIRET_Number__c ='87857683400000' ;
        insert ac;

        List<Contact> lstCont = new List<Contact>();
      
        for(Integer i= 0; i< 20;i++){
            Contact con=new Contact();
            con.FirstName='test1'+i;
            con.LastName = 'testName'+i;
            con.AccountId = ac.Id;
            con.Email = 'aaa@yopmail.com';
            con.Phone = '000000000';
            lstCont.add(con);
        }
        insert lstCont;

       List<Contract__c> lstContract = new List<Contract__c>();
        for(Integer i= 0; i< 20;i++){ 
             Contract__c c = new Contract__c();
            c.Account__c=ac.id;
            c.Etat__c='Convention';
            c.RegroupmentIndex__c=null;
            c.AssociatedSalesRep__c=Commercial.Id;
            c.BillingContact__c=lstCont[0].Id;
            c.name='Contract Test'+i;
            c.Status__c ='Valide';
            c.RegroupmentIndex__c = 1;
            c.Filiale__c = String_Helper.CON_Filiale_Drimm;
            c.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;
            lstContract.add(c);
        }       
        insert lstContract;

       
        System.RunAs(Commercial) {

            ByPassUtils.ByPass('ContractTrigger');
            ByPassUtils.ByPass('OpportunityTrigger');
            ByPassUtils.ByPass('QuoteLineItemTrigger');

            System.debug(FeatureManagement.checkPermission('CreateOppFromContractTrigger'));

            test.startTest();
            for(Contract__c c : lstContract){
                c.AssociatedSalesRep__c=director1.Id;
            }
          
            Update lstContract;
            List<Contract__c> contracts = Database.query('SELECT '+String.join(new List<String>(Schema.getGlobalDescribe().get('Contract__c').getDescribe().fields.getMap().keySet()), ', ')+', ContactRelated__r.Actif__c, Account__r.Name, AssociatedSalesRep__r.Trigramme__c, (SELECT '+String.join(new List<String>(Schema.getGlobalDescribe().get('Amendment__c').getDescribe().fields.getMap().keySet()), ', ')+' FROM Avenants__r) FROM Contract__c WHERE Id IN :lstContract');
            ContractTriggerHandler.ContractConvention(contracts);

            test.stopTest();
            List<Contract__c> lstc=[Select id,OwnerId FROM Contract__c];
           // System.assertEquals(director1.Id, lstc[0].ownerID);
        }
    }
}