public class AccountTriggerHandler {
    
    public static boolean stop = false;
    
    public static void handler(Boolean isInsert, Boolean isDelete, Boolean isUpdate, Boolean isBefore, Boolean isAfter, List<Account> lst, List<Account> oldLst, Map<Id,Account> oldMap) {
        
        if(!stop) {
        
            if(isInsert){
                AccountTriggerUtil.updateMaisonMere(lst, 'INSERT');
                //AccountTriggerUtil.updateUsualName(lst);
            }
            else if(isDelete && isBefore){
                AccountTriggerUtil.updateMaisonMere(oldLst, 'DELETE');
            }
            else if(isUpdate){
                
                if(isAfter){
                    
                    List<Account> toIncrement = new List<Account>();
                    List<Account> toDecrement = new List<Account>();
                    for(Account a : lst){
                        Account old = oldMap.get(a.Id);
                        if(a.Maison_mere__c != null && old.Maison_mere__c == null)
                            toIncrement.add(a);
                        else if(a.Maison_mere__c == null && old.Maison_mere__c != null){
                            toDecrement.add(old);
                        }
                        
                    }
                    AccountTriggerUtil.updateMaisonMere(toIncrement, 'INSERT');
                    AccountTriggerUtil.updateMaisonMere(toDecrement, 'DELETE');
                }if(isBefore){
                    for(Account a : lst){
                        
                        if(a.NomUsuel__c == null){
                            Account old = oldMap.get(a.Id);
                            a.NomUsuel__c = old.Name;
                        }
                        
                        
                    }
                    
                }
            }
            
        }
        
    }
    
}