@IsTest
public with sharing class LC_ValidationManager_AugTarifTest {
   
    @testSetup static void setup() {
        List<Contract__c> listC = new  List<Contract__c>();
        List<Amendment__c> listA = new  List<Amendment__c>();

        Account acc = TestDataFactory.createAccountWithInsert('Test Acc');     

        Contact cont = TestDataFactory.createContactWithInsert(acc);

        User assistantCo = TestDataFactory.createAssistantCommercialUser();
        User respCom =  TestDataFactory.createResponsableCommercialUser(assistantCo);
        User respCom1 = TestDataFactory.createResponsableCommercialUser(assistantCo);

        Contract__c c = new Contract__c();   
        c.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;   
        c.Account__c = acc.Id;  
        c.AssociatedSalesRep__c = respCom1.Id;
        c.Status__c = 'Soumis';  

        Contract__c c1 = new Contract__c();   
        c1.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;   
        c1.Account__c = acc.Id;  
        c1.AssociatedSalesRep__c = respCom.Id; 
        c1.Status__c = 'Soumis';  
        
        Contract__c c2 = new Contract__c();   
        c2.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c2.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 
        c2.Account__c = acc.Id; 
        c2.Face_Face__c = true;
        c2.AssociatedSalesRep__c = respCom.Id; 
        c2.Status__c = 'Valide'; 
        
        Contract__c c3 = new Contract__c();   
        c3.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c3.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c3.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 
        c3.Account__c = acc.Id; 
        c3.Face_Face__c = true;
        c3.AssociatedSalesRep__c = respCom.Id; 
        c3.Status__c = 'Rejete';
        
        Contract__c c4 = new Contract__c();   
        c4.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c4.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c4.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 
        c4.Account__c = acc.Id; 
        c4.Face_Face__c = true;
        c4.AssociatedSalesRep__c = respCom.Id; 
        c4.Status__c = 'Soumis';
        
        listC.add(c);listC.add(c1); listC.add(c2); listC.add(c3); listC.Add(c4);
        insert listC;      
        
        for(Contract__c contrat: listC) {
            Amendment__c a = new Amendment__c();
            a.Contrat2__c = contrat.id;
            a.TECH_Excluded__c = false;
            listA.add(a);
        }
        
        insert listA;
           
    }

    @isTest 
    static void testFetchContracts(){
        List<Object> conts = LC_ValidationManager_AugTarif.fetchContracts();
        System.assertEquals(2, conts.size());
    }

    @isTest 
    static void testgetAccountIds(){
        List <Contract__c> conts = LC_ValidationManager_AugTarif.getAccountIds();
        System.assertEquals(true, conts.size() > 0);
    }

     @isTest
    static void testgetReportData(){
        List<String> lstReportName = new List<String>();
        lstReportName.add('Approb_01_Tout_Hors_FaF_9Bz');
        lstReportName.add('Approb_02_Tout_FaF_HXD');


        User associatedSalesRep = [SELECT Id FROM User WHERE profileId = :[select id from profile where name = :'Responsable commercial' Limit 1].id Limit 1] ;

        List <String> datas = LC_ValidationManager_AugTarif.getReportData(lstReportName,  associatedSalesRep.Id,  String_Helper.CON_Filiale_Drimm,  String_Helper.CON_Filiere_CentreDeTriDAE, false, false);
        System.assertEquals(true, datas == null);
        List <String> datas2 = LC_ValidationManager_AugTarif.getReportData_chart(lstReportName,  associatedSalesRep.Id,  String_Helper.CON_Filiale_Drimm,  String_Helper.CON_Filiere_CentreDeTriDAE);
        System.assertEquals(true, datas2 == null);
    }

    @isTest
    static void testfetchContractsByAccountIds(){
        List<String> lstReportName = new List<String>();
        lstReportName.add('Approb_01_Tout_Hors_FaF_9Bz');
        lstReportName.add('Approb_02_Tout_FaF_HXD');


        User associatedSalesRep = [SELECT Id FROM User WHERE profileId = :[select id from profile where name = :'Responsable commercial' Limit 1].id Limit 1] ;
        String accountIds = [SELECT Id FROM Account Limit 1].Id;
        List <Contract__c> datas = LC_ValidationManager_AugTarif.fetchContractsByAccountIds(accountIds,String_Helper.CON_Filiale_Drimm, String_Helper.CON_Filiere_CentreDeTriDAE);
        System.assertEquals(true, datas.size() > 0);
    }

    @isTest
    static void testValidate(){
       
        User associatedSalesRep = [SELECT Id FROM User WHERE profileId = :[select id from profile where name = :'Responsable commercial' Limit 1].id Limit 1];

        Id acc = [SELECT Id FROM Account LIMIT 1].id;
        List<String> accountsIds = new List<String>();
        accountsIds.add(String.valueOf(acc));
       
        String resp = LC_ValidationManager_AugTarif.validate(associatedSalesRep.Id, String_Helper.CON_Filiale_Drimm, String_Helper.CON_Filiere_CentreDeTriDAE, false, accountsIds);
        String resp2 = LC_ValidationManager_AugTarif.validate(associatedSalesRep.Id, String_Helper.CON_Filiale_Drimm, String_Helper.CON_Filiere_CentreDeTriDAE, true, accountsIds);
        System.assertEquals('Valide', resp);

        String resp3 = LC_ValidationManager_AugTarif.getStatusFaf(String.valueOf(associatedSalesRep), String_Helper.CON_Filiale_Drimm, String_Helper.CON_Filiere_CentreDeTriDAE);
    }

    @isTest
    static void testReject(){
       
        List<List<String>> comments = new List<List<String>>{
            new List<String>{'test', 'comm', 'test'},
            new List<String>{'test', 'Ken', 'Mary'}
        };

        Id acc = [SELECT Id FROM Account LIMIT 1].id;
        List<String> accountsIds = new List<String>();
        accountsIds.add(String.valueOf(acc));

       List<User> associatedSalesReps = [SELECT Id FROM User WHERE profileId = :[select id from profile where name = :'Responsable commercial' Limit 1].id] ;
       
        String resp = LC_ValidationManager_AugTarif.reject(associatedSalesReps[0].Id,String_Helper.CON_Filiale_Drimm, String_Helper.CON_Filiere_CentreDeTriDAE,comments, false, accountsIds);
        //System.assertEquals('Valide', resp);
        String resp1 = LC_ValidationManager_AugTarif.reject(associatedSalesReps[1].Id,String_Helper.CON_Filiale_Drimm, String_Helper.CON_Filiere_CentreDeTriDAE,comments, true, accountsIds);
        //System.assertEquals('Valide', resp1);
    }
    
    @isTest
    static void test() {
        List<Id> ids = new List<Id>();
        for(Contract__c c: [SELECT ID FROM Contract__c]) {
            ids.add(c.Id);
        }
        System.assertNotEquals(null, LC_ValidationManager_AugTarif.getStatusById(ids));
        System.assertNotEquals(null, LC_ValidationManager_AugTarif.getAccountId('Test Acc'));
    }
        
}