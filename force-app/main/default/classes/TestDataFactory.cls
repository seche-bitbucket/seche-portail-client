/*----------------------------------------------
 * 
 * Capgemini - Valentin Dupré
 * 
 * valentin.dupre@capgemini.com
 * 
 * 31/01/2018
 * 
 * Update : Use this class only in the test context by Baïla TOURE 
 * 19/02/2020
 * ----------------------------------------------*/

@isTest
public class TestDataFactory {
    
    /************************
     * Account
     * *********************/
    
    public static Account createAccount(String Name){
        
        Account acc = new Account();
        acc.Name = Name;
        acc.CustomerNature__c = 'Collecteur';
        acc.Industry = 'Chimie';
        acc.NAF_Number__c = randomStringNumber(4) + 'Z';
        System.debug(' acc.NAF_Number__c : '+  acc.NAF_Number__c);
        acc.BillingStreet = randomStringNumber(3) + 'rue du test';
        acc.BillingCity = 'Ville-de-test';
        acc.BillingPostalCode = randomStringNumber(5)+'';
        System.debug(' acc.BillingPostalCode : '+  acc.BillingPostalCode);
        acc.SIRET_Number__c = '12345432345432';
        
        return acc;
    }
        
     public static Account createAccountWithInsert(String Name){

        Account acc = createAccount(Name);
        insert acc;
        
        return acc;
    }    
    
    public static List<Account> createAccountList(String Name, Integer size){
        
        List<Account> lstAcc = new List<Account>();
        
        for(Integer i = 0; i < size; i++){
            Account acc = createAccount(Name + ' '+ i);
            lstAcc.add(acc);
        }
        
        return lstAcc;
    }
    
    
    /************************
     * Contact
     * *********************/
    
    public static Contact createContact(Account acc){
        
        Contact ctc = new Contact();
        
        ctc.AccountId = acc.Id;
        ctc.FirstName = randomString(6);
        ctc.LastName = randomString(8);
        ctc.Email =  ctc.FirstName+'.'+ctc.LastName+'@groupe-seche.com';
        ctc.Phone = randomStringNumber(10);
        
        return ctc;
    }
    
    public static Contact createContactWithInsert(Account acc){
        
        Contact ctc = createContact(acc);
        insert ctc;
        
        return ctc;
    }
    
    public static List<Contact> createContactList(Account acc, Integer size){
        
        List<Contact> lstCtc = new List<Contact>();
        
        for(Integer i = 0; i < size; i++){
            Contact ctc = createContact(acc);
            lstCtc.add(ctc);
        }
        
        return lstCtc;
    }
    
    
    /************************
    * Service Demand
    * *********************/
    
    public static SD_ServiceDemand__c createServiceDemand(Account acc, Contact ctc, String type){
        // Type : Repairs, Remediation, Elimination, EliminationSF6, Waste
        RecordType rt = [SELECT Id, DeveloperName, SobjectType FROM RecordType WHERE SobjectType = 'SD_ServiceDemand__c' AND DeveloperName = : type];
        
        SD_ServiceDemand__c sd = new SD_ServiceDemand__c();
        sd.ApplicantCenter__c = acc.Id;
        sd.BilledAccount__c = acc.ID;
        sd.ResponsibleName__c = ctc.Id;
        sd.RecordTypeId = rt.ID;
        
        return sd;
    }
    
    /************************
    * Waste Statement
    * *********************/
    
    public static WST_Waste_Statement__c createWasteStatement(SD_ServiceDemand__c sd){
        
        WST_Waste_Statement__c wst = new WST_Waste_Statement__c();
        wst.ServiceDemande__c = sd.Id;
        wst.TransfoSerialNumber__c = randomStringNumber(10);
        wst.Brand__c = randomString(3);
        wst.KVA__c = Decimal.valueOf(randomStringNumber(3));
        wst.TotalWeight__c = Decimal.valueOf(randomStringNumber(3));
        wst.Status__c = 'Demande effectuee';
                
        return wst;
    }
    
    
        public static WST_Waste_Statement__c createWasteStatement(String pAccountName){
        //Create Account
        Account acc = TestDataFactory.createAccountWithInsert(pAccountName);
        //create ServiceDemand
        SD_ServiceDemand__c sd = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Repairs');
        insert sd;
        //create Waste_Statement    
        WST_Waste_Statement__c wst = new WST_Waste_Statement__c();
        //Link Waste_Statment to ServiceDemande
        wst.ServiceDemande__c = sd.Id;
        return wst;
    }
    
    
 
    
    public static List<WST_Waste_Statement__c> createWasteStatementList(SD_ServiceDemand__c sd, Integer size){
        
        List<WST_Waste_Statement__c> wstList = new List<WST_Waste_Statement__c>();
        
        for(Integer i = 0; i < size; i++){
            WST_Waste_Statement__c wst = createWasteStatement(sd);
            wstList.add(wst);
        }
        
        return wstList;
    }
    
    /************************
    * Quote Line (Custom Object)
    * *********************/
    public static List<QuoteLine__c> createCustomQuoteLineList_WST_Parent(WST_Waste_Statement__c parent, Integer size, PricebookEntry pbe){
        
        List<QuoteLine__c> qlList = new List<QuoteLine__c>();
        
        for(Integer i = 0; i < size; i++){
            QuoteLine__c ql = new QuoteLine__c();
            ql.Transformateur__c = parent.ID;
            ql.TECH_PriceBookEntryID__c = pbe.ID;
            ql.Label__c = pbe.Name;
            ql.Code__c = pbe.ProductCode;
             ql.Quantity__c = Decimal.valueOf(randomStringNumber(1));
            qlList.add(ql);
        }
        
        return qlList;
    }
    
    
    /************************
    * Opportunity
    * *********************/
    public static Opportunity createOpportunity(String pOpportunityName){
        Opportunity vOpportunity = new Opportunity();
        vOpportunity.Name=('pOpportunityName');
        vOpportunity.StageName='TestStageName';
        vOpportunity.CloseDate=date.newInstance(2016, 27, 7);
        return vOpportunity;
        
    }
    
    public static List<Opportunity> createOpportunityListWithInsert(String pOpportunityName, Integer size){
        
        List<Opportunity> vOpportunityList = new List<Opportunity>();
        
        for(Integer i = 0; i < size; i++){
            Opportunity vOpportunity = new Opportunity();
            vOpportunity.Name='pOpportunityName' + i;
            vOpportunity.StageName='TestStageName';
            vOpportunity.CloseDate=date.newInstance(2016, 27, 7);
            vOpportunityList.add(vOpportunity);
        }
        
        insert vOpportunityList;
        return vOpportunityList;
        
    }
    
    public static Boolean manageSyncedOpportunityList(List<Opportunity> vOpportunityList, List<Quote> vQuoteList){
        
        List<Opportunity> vOpportunityListToUpdate = new List<Opportunity>();
        Map<Id, Quote> vQuoteMap = new Map<Id, Quote>();
        
        for(Quote vQuote : vQuoteList){
            vQuoteMap.put(vQuote.OpportunityId, vQuote);
        }
        
        for(Opportunity opp : vOpportunityList){
            
            opp.SyncedQuoteId = vQuoteMap.get(opp.ID).ID;
            vOpportunityListToUpdate.add(opp);
        }
        
        try{
            update vOpportunityListToUpdate;
            return true;
        }catch(Exception e){
            return false;
        }
                
    }
    
    
     /************************
    * Quote
    * *********************/
    public static Quote createQuote(Opportunity opp){
        Quote vQuote = new Quote();
        vQuote.Name= opp.Name + 'v1';
        vQuote.OpportunityId = Opp.ID;
        return vQuote;
    }
    
    public static List<Quote> createQuoteListWithInsert(List<Opportunity> vOpportunityList){
        
        List<Quote> vQuoteList = new List<Quote>();
        
        for(Opportunity vOpp : vOpportunityList){
            Quote vQuote = new Quote();
            vQuote.Name= vOpp.Name + 'v1';
            vQuote.OpportunityId = vOpp.ID;
            vQuote.Pricebook2Id = Test.getStandardPricebookId();
            vQuoteList.add(vQuote);
        }
        
        insert vQuoteList;
        return vQuoteList;
        
    }
    
    /************************
    * QuoteLineItem
    * *********************/
  
   
    public static List<QuoteLineItem> createQuoteLineItemPerQuotes(List<Quote> pQuoteList, Integer pQuoteLinePerWST){
        
        List<QuoteLineItem> vQuoteLineItemList = new List<QuoteLineItem>();
        
        PricebookEntry vPBE = getPriceBookEntry();
        
        for(Quote vQuote : pQuoteList){
            for(Integer i = 0; i<pQuoteLinePerWST; i++){
                QuoteLineItem vQLI = new QuoteLineItem();
                vQLI.QuoteId = vQuote.ID;
                vQLI.PricebookEntryId = vPBE.Id;
                vQLI.UnitPrice = vPBE.UnitPrice;
                vQlI.Quantity = Integer.valueOf(randomStringNumber(1))+1;
                vQuoteLineItemList.add(vQLI);
            }
        }
        
        insert vQuoteLineItemList;
        return vQuoteLineItemList;
        
    }
    
    
    
    /************************
     * Users
     * *********************/    
    
    public static User createResponsableCommercialUser(User assistantCo){
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt+'@test.com';
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Responsable commercial' limit 1];
        
        String testemail = 'community_-_User_test@test.com';
        
        User responsableCo = new User(profileId = p.id, username = uniqueName, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                      alias='cspu', lastname='lastname', Assistant__c = assistantCo.Id , IsActive=true);
        insert responsableCo;
        return responsableCo;
    }
    
     public static User createAssistantCommercialUser(){
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt+'@test.com';
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Assistante commerciale' limit 1];
        
        String testemail = 'community_-_User_test@test.com';
        
        User assistantCo = new User(profileId = p.id, username = uniqueName, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                      alias='cspu', lastname='lastname', IsActive=true);
        insert assistantCo;
        return assistantCo;
    }

    public static User createCommmunityUser(String pAccountName){
        
        Account acc = createAccountWithInsert(pAccountName);
        Contact ctc = TestDataFactory.createContactWithInsert(acc);
        
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Community TREDI Transfo' limit 1];
        
        String testemail = 'community_-_User_test@test.com';
        
        User communityUser = new User(profileId = p.id, username = testemail, email = testemail, contactID = ctc.ID,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                      alias='cspu', lastname='lastname', IsActive=true);
        insert communityUser;
        return communityUser;
    }
    
     public static User createCommmunityUser(Account acc, Contact ctc){
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Community TREDI Transfo' limit 1];
        
        String testemail = 'community_-_User_test@test.com';
        
        User communityUser = new User(profileId = p.id, username = testemail, email = testemail, contactID = ctc.ID,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                      alias='cspu', lastname='lastname', IsActive=true);
        insert communityUser;
         
        return communityUser;
    }
    
    /************************
     * Tools
     * *********************/
    
    private static String randomStringNumber(Integer length){
         return String.valueOf(Crypto.getRandomInteger()).right(length);
    }
    
    private static String randomString(Integer length) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < length) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    }
    
    public static PriceBookEntry getPriceBookEntry(){
         // First, set up test price book entries.
        // Insert a test product.
        Product2 prod = new Product2(Name = 'HT', 
            Family = 'Traitement', TECH_ExternalID__c ='TREA-HT-OK10');
        insert prod;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
       
        return standardPrice;
    }

/******** Attetstation Data Test *************/  
    
    public static Attestation__c createAttestation(Contact c, User responsableCo, Boolean isWood, Boolean isMixed, Boolean isPaperCardboard, Boolean isPlastic, Boolean isGlass, Boolean isPlaster, Boolean isMineralFractions, Boolean isSorted, Boolean isProducer, Boolean isTechCongaAttestationToGenerate, String status){
        
        Attestation__c att = new Attestation__c();
        Att.SiteCode__c = '1023';
        att.Account__c = c.AccountId;
        att.Contact__c = c.Id;
        att.AssociatedSalesRep__c= responsableCo.Id;
        att.Status__c = status;
        att.Wood__c = isWood;
        att.Mixed__c = isMixed;
        att.PaperCardboard__c = isPaperCardboard;
        att.Plastic__c = isPlastic;
        att.Sorted__c = isSorted;
        att.Glass__c = isGlass;
        att.Plaster__c = isPlaster;
        att.MineralFractions__c = isMineralFractions;
        att.TECH_CongaAttestationToGenerate__c = isTechCongaAttestationToGenerate;
        att.Producer__c = isProducer;

        return att;
       

    }

     public static ContentDocumentLink createFacture(Attestation__c att, Integer rank){
        System.System.debug('createFacture.att : '+att); 
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document'+att.Name+rank;
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv; 

        ContentVersion cv1 = new ContentVersion();
        cv1.Title = 'Test Document'+att.Name;
        cv1.PathOnClient = 'TestDocument.pdf';
        cv1.VersionData = Blob.valueOf('Test Content');
        cv1.IsMajorVersion = true;
        Insert cv1;           
        
        List<ContentVersion> lstConDoc = [SELECT ContentDocumentId FROM ContentVersion];
       

        //create ContentDocumentLink  record 
        ContentDocumentLink conDocLink = New ContentDocumentLink();
        conDocLink.LinkedEntityId = att.id; // Specify RECORD ID here i.e Any Object ID (Standard Object/Custom Object)
        conDocLink.ContentDocumentId = lstConDoc[rank].ContentDocumentId;  //ContentDocumentId Id from ContentVersion
        conDocLink.shareType = 'V';
       // insert conDocLink;

        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
         System.System.debug('createFacture.documents : '+documents); 

       return conDocLink;        
       

    }
    
    
}