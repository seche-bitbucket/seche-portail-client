public with sharing class LC_GenericEmailSend {

    public static Task createEmailActivity(String subject,String description, String contactId, String quoteId) {

        Task t = new Task(
            Subject = subject,
            Description = description.stripHtmlTags(),
            WhoId = contactId,
            WhatId = quoteId,
            TaskSubtype = 'Email',
            ActivityDate = Date.today(),
            Status = 'Achevée');
            return t;
    }
    
    public static List<ContentDocumentLink> linkAttToTask(String taskId, List<ContentVersion> cversions) {
        // Create Related file
            List<ContentDocumentLink> lst = new List<ContentDocumentLink>();

            for(ContentVersion cv: cversions){
                ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.ContentDocumentId = cv.ContentDocumentId;
                cdl.LinkedEntityId = taskId;
                cdl.ShareType = 'V'; 
                lst.add(cdl);
            }
            return lst;
    }
    
    //__________________GENERIC METHODS__________________\\
    @AuraEnabled
    public static Map<String,Object> genericGetEmailPrev(String objId){	
    
        try{
            Map<String,Object> result = new Map<String,Object>();
            Boolean isEmailAddressBounced = false;

            System.debug(objId);
            System.debug(Id.valueOf(objId));
    
            Id id = Id.valueOf(objId);
    
            Schema.SObjectType sobjectType = id.getSObjectType();
            String sobjectName = sobjectType.getDescribe().getName();
            List<String> fields = new List<String>(sobjectType.getDescribe().fields.getMap().keySet());
            
            List<String> ADD_FIELDS = new List<String>{', Account__r.Name, Lead__r.OwnerId, Lead__r.Email, Lead__r.Name, Contact__r.Id, Contact__r.Name, Contact__r.Email, Contact__r.IsEmailBounced',
                                                ', Contact.Name, Account.Name, Contact.Email, Opportunity.Filiale__c, Opportunity.Salesman__c, Opportunity.Assistant__c, Opportunity.SyncedQuoteId, Contact.IsEmailBounced'};
    
            String addFields = (sobjectType == Quote.sObjectType) ? ADD_FIELDS[1] : ADD_FIELDS[0];

            String f = String.join(fields, ',');
    
            SObject record = Database.query('SELECT '+f+addFields+' FROM '+sobjectName+' WHERE Id = :objId');
    
            String status;
    
            String TECH_UID = (sobjectType == Quote.sObjectType) ? GuidUtil.NewGuid() : null;
    
            for(String field: fields) {
                if(field == 'status') {
                    status = String.valueOf(record.get(field));
                    break;
                }
            }
    
            if((String)record.get('ContactId') == null) {
                result.put('message',' Contact manquant sur le deivs ');
                return result;
            } 

            String name = (sobjectType == Quote.sObjectType) ? ((record.getSObject('Contact').get('Id') != null) ? (String)record.getSObject('Contact').get('Name') : '') : (String)record.getSObject('Lead__r').get('Name');
            String email = (sobjectType == Quote.sObjectType) ? ((record.getSObject('Contact').get('Id') != null) ? (String)record.getSObject('Contact').get('Email') : '') : (String)record.getSObject('Lead__r').get('Email');
            Boolean isEmailBounced = (sobjectType == Quote.sObjectType && record.get('ContactId') != null) ? (Boolean)record.getSObject('Contact').get('IsEmailBounced') : false;
            String syncedQuoteId = (String)record.getSObject('Opportunity').get('SyncedQuoteId') ;
            Quote syncedQuote = [SELECT Id, Name, Status FROM Quote WHERE Id = :syncedQuoteId];
           
            //Check if Email address is bounced
            if(isEmailBounced ==true) {
                isEmailAddressBounced = true;
                result.put('message',' L\'adresse mail suivante est incorrecte : '+email+' ('+name+')');
                result.put('isEmailAddressBounced', 'isEmailAddressBounced');
                result.put('initialStatus', Status);
            }else if(status == 'Envoyé pour signature'){
                result.put('message',' Le devis a déjà été envoyé pour signature ');
            }else if(syncedQuote.Status== 'Signé'){
                result.put('message',' Le devis synchronisé est déjà signé ');
            }else{
                LC_GenericEmailSend.RequestResponse resp = new LC_GenericEmailSend.RequestResponse();
                //generate the Tech_UID
                resp = genericGetEmailVal(record);
    
                result.put('message', resp.message !=null ? (String)resp.message : '');
                /* if(sobjectType == T5F_PreFip__c.sObjectType) {
                    result.put('contactId', (String)record.get('Lead__c'));
                    result.put('contactName', (String)record.getSObject('Lead__r').get('Name'));
                    result.put('toAddresses', (String)record.getSObject('Lead__r').get('Email'));
                } else */if(sobjectType == Quote.sObjectType) {
                    System.debug(record.get('ContactId'));
                    result.put('tech_uid', (String)TECH_UID);
                    result.put('contactId', (String)resp.contactId);
                    result.put('contactName', (String)resp.contactName);
                    result.put('toAddresses', resp.toAddresses!=null ? (String)resp.toAddresses[0] : '');
                }
    
                result.put('ccaddresses', resp.ccaddresses!=null ? String.join(resp.ccaddresses, ' ; ') : '');
                result.put('bccaddresses', resp.bccaddresses!=null ? (String)resp.bccaddresses[0] : '');
                result.put('replyTo', resp.replyTo!=null ? (String)resp.replyTo[0] : '');
                result.put('senderDisplayName', resp.senderDisplayName!=null ? (String)resp.senderDisplayName : '');
                result.put('subject', resp.subject!=null ? (String)resp.subject : '');
                result.put('fileId', resp.fileId!=null ? (List<String>)resp.fileId : new List<String>());
                result.put('body', resp.htmlBody!=null ? (String)resp.htmlBody : '');
                result.put('isShowForm', (String)resp.isShowForm);
                result.put('status', (String)status);
            }
    
            return result;
    
        } catch (Exception e) {
            system.debug(e.getMessage());
            system.debug(e.getLineNumber());
            return null;
        }
    }
    
    @AuraEnabled
    public static RequestResponse genericGetEmailVal(SObject record){
    
        try{
            String userTheme = UserInfo.getUiThemeDisplayed();
            LC_GenericEmailSend.RequestResponse resp = new LC_GenericEmailSend.RequestResponse();
            resp.message = 'Nombre de pièces-jointes non conforme sur le devis.';
            resp.showToastMode = 'warning';
            resp.mailStatus = false;
            resp.isShowForm = 'false';
            String filtrePrefip;

            resp.showToastMode = 'warning';
            resp.mailStatus = false;
            resp.isShowForm = 'false';
            String returnValue='Wait';
            String docType;
            String recordType;
            String filiale;
            String quoteCodeText;
            
            Boolean contactIsNull = true;
    
            Id recordId = (Id)record.get('Id');
            System.debug('recordId: '+recordId);
            User user;
            User assistant;

            for(String key: Schema.getGlobalDescribe().get(recordId.getSobjectType().getDescribe().getName()).getDescribe().fields.getMap().keySet()) {
                if(key.toLowerCase() == 'status') {
                    if(record.get(key) == 'Envoyé pour signature' || record.get(key) == 'Signé' || record.get(key) == 'Refusé') {
                        resp.message = 'Ce devis a déjà été envoyé pour signature.'; 
                        return resp;
                    }
                }
            }
    
            System.debug(recordId.getSobjectType());
    
            if(recordId.getSobjectType() == Quote.sObjectType) {
                filtrePrefip = '';
                filiale = (String)record.getSObject('Opportunity').get('Filiale__c');
                recordType = 'Devis';
                if((String)record.get('Quote_Type__c') == 'Convention') {
                    recordType = 'Convention';
                }
                quoteCodeText = '</br>Code devis : '+ (String)record.get('QuoteCode__c');
                docType = 'le devis';
                String contactId = (String)record.get('ContactId');
                System.debug(contactId);
                String salesmanId = (String)record.getSObject('Opportunity').get('Salesman__c');
                String salesman_assistantId = (String)record.getSObject('Opportunity').get('Assistant__c');
                user = [SELECT Id, Name, email, Signature FROM User WHERE Id = :salesmanId];
                if(salesman_assistantId != null) assistant = [SELECT Email FROM User WHERE Id = :salesman_assistantId];
                RecordType rt=[SELECT Id, Name, DeveloperName, Description FROM RecordType Where DeveloperName='Devis_approuv_Read_Only' LIMIT 1];
                List<Contact> contact=[SELECT ID,Name,FirstName,LastName,Email,Phone From Contact WHERE Id=:contactId];
                system.debug(contact);
                Quote qt = new Quote(Id=(String)record.get('Id'));
                resp.objectQuote = qt;
                if(contact.size() >= 1 && contact != null) {
                    contactIsNull = false;
                    resp.contactId = contact[0].Id;
                    resp.contactName = contact[0].Name;
                    resp.toAddresses = new String[] { contact[0].Email };
                }
                
                if(assistant != null && String.isNotEmpty(assistant.Email)) {
                	resp.ccaddresses = new String[] { assistant.Email, user.Email };
                } else {
                    resp.ccaddresses = new String[] { user.Email };
                }
    
            } /* else if(recordId.getSobjectType() == T5F_PreFip__c.sObjectType) {
                contactIsNull = false;
                filtrePrefip = 'ContentDocument.title LIKE \'%Final%\' AND';
                filiale = 'Séché Environnement';
                recordType = 'PréFip';
                docType = 'la préfip';
                String uId = (String)record.getSObject('Lead__r').get('OwnerId');
                user = [SELECT Id, Name, email, Assistant__r.Email, Signature FROM User WHERE Id = :uId];
                T5F_PreFip__c prefip = new T5F_PreFip__c(Id=(String)record.get('Id'));
                resp.objectPreFip = prefip;
                resp.ccaddresses = new String[] { user.Email, user.Assistant__r.Email };
            } */

            // to delete ?
            System.debug(UserInfo.getUserEmail());
            if(UserInfo.getUserEmail() != null && !resp.ccaddresses.contains(UserInfo.getUserEmail())) {
                resp.ccaddresses.add(UserInfo.getUserEmail());
            }
            System.debug(resp.ccaddresses);
    
            String linkedId = (String)record.get('Id');
    
            resp.message = 'Contact manquant ou nombre de pièces-jointes non conforme sur '+docType;
    
            List<DocumentsForSignature__c> document = new List<DocumentsForSignature__c>();
            document = [SELECT Id, Status__c, FlowId__c,SigningUrl__c,Package__r.ConnectivePackageId__c FROM DocumentsForSignature__c WHERE TECH_ExternalId__c= :linkedId Limit 1];
    
            List<Attachment> att=[SELECT id, Name, body, ContentType,ParentId FROM Attachment WHERE ParentId=:linkedId];
            
            List<ContentDocumentLink> files  = Database.Query('SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLink where '+filtrePrefip+' LinkedEntityId =:linkedId');
    
            System.debug('files: '+files);

            List<String> lstIds = new List<String>();
            /*for(ContentDocumentLink file: files) {
                lstIds.add(file.ContentDocumentId);
            }*/
    
            Messaging.Emailfileattachment fileAttachment = new Messaging.Emailfileattachment();
            String contactGreeting;
            String CommercialText;
            String secheIndustryText;
            String ConnectiveText;
            String greeting;
            String notContact;
            if(files != null && !contactIsNull) {
                if(files.size()>0) {
                    if(document.isEmpty()==false) {
                        resp.message = 'Document déja envoyé pour signature';
                        resp.showToastMode = 'warning';
                        resp.isShowForm = 'false';
                    }else if(files[0].ContentDocument.title.length() > 75){
                        resp.message = 'Le nom du fichier est trop long (maximum 75 caractères).';
                        resp.showToastMode = 'warning';
                        resp.isShowForm = 'false';
                    }else{
                        
                        lstIds.add(files[0].ContentDocumentId);
                        String userSignature;
                        if(user.Signature!=Null) {
                            userSignature ='<pre>'+user.Signature+'</pre>';
                        }else{
                            userSignature=' ';
                        }
                        
                        String userName=user.Name;
                        ContentVersion cversion =  [SELECT title, PathOnClient, FileType, versiondata, FileExtension, CreatedDate FROM contentversion
                                                    WHERE ContentDocumentId =: files[0].ContentDocumentId ORDER BY CreatedDate DESC LIMIT 1];
                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
    
                        contactGreeting ='Cher client,<br><br>';
                        CommercialText= user.Name+' vous prie de trouver ci-joint votre '+recordType.toLowerCase()+'.';//+NextYear+'. ';
                        ConnectiveText='<br><br>'+filiale+' vous invite, via Connective, à signer en 3 étapes votre '+recordType+' en cliquant sur le lien que vous allez recevoir dans un autre mail.<br><br>NB : pour indiquer les coordonnées d\'un autre signataire de votre établissement, il vous est possible par ce lien de <strong>\'réattribuer\'</strong> cette convention.<br>';
                        notContact='<br><br><i> Séché Environnement collecte et traite des données à caractère personnel dans le cadre de la gestion des contrats et des obligations légales et réglementaires s’y rattachant. Elles sont uniquement destinées à nos services internes et à nos filiales habilitées. Conformément au Règlement Général sur la Protection des Données, le Groupe met à disposition des droits d’accès, de rectification et d’opposition sur les données à caractère personnel. Il est possible d\'exercer ces droits en contactant notre Délégué à la Protection des Données par courrier électronique adressé à <a href="dp@groupe-seche.com">dp@groupe-seche.com</a> , qui traitera votre demande. Pour plus de détails sur l’engagement du Groupe :  <a href="https://www.groupe-seche.com/fr/mentions-legales">RGPD</a>';
                        greeting= '<br>Cordialement,';
                        //Create Email content
                        resp.replyTo = new String[] {user.Email};
                        resp.senderDisplayName = user.Name;
                        resp.subject = (recordId.getSobjectType() == Quote.sObjectType) ? recordType + ' - '+filiale+' - '+(String)record.getSObject('Account').get('Name') : (String)record.get('Name')+' - '+(String)record.getSObject('Lead__r').get('Name');
                        resp.htmlBody = contactGreeting+CommercialText+quoteCodeText+ConnectiveText+greeting;//+userSignature; //+ notContact;
                        resp.atts = new List<Messaging.Emailfileattachment>();
                        resp.atts.add(fileAttachment);
                        resp.isShowForm = 'true';
                        resp.fileId = lstIds;
                        resp.cversions = new List<ContentVersion>();
                        resp.cversions.add(cversion);
                    }
                }
            }
            
            return resp;
        } catch(Exception e){
            system.debug(e.getMessage());
            System.debug(e.getLineNumber());
            return null;
        }
    }
    
    @AuraEnabled
    public static Map<String,String> genericCreatePackage(String signerId, String contactName, String fromAddresses, String toAddresses, String cciEmail, String senderDisplayName, String subject, String body, String objId, List<String> filesIds, List<String> filesToAttach){
        Map<String,String> result = new Map<String,String>();
        System.debug('########## BEGIN genericCreatePackage '+signerId);
        
        //T5F_PreFip__c preFip = [SELECT Id, Name FROM T5F_PreFip__c WHERE Id =: preFipId];
        PackageV4Wrapper pkg = ConnectiveV4ApiUtils.createPackage(Id.valueOf(objId), Id.valueOf(signerId));
        String httpResp = ConnectiveV4ApiManager.createPackage(pkg, Id.valueOf(objId));
    
        System.debug(httpResp);
    
        Schema.SObjectType sobjectType = Id.valueOf(objId).getSObjectType();
        String sobjectName = sobjectType.getDescribe().getName();
        List<String> fields = new List<String>(sobjectType.getDescribe().fields.getMap().keySet());
    
        System.debug(objId);
        System.debug(' FROM '+sobjectName+' WHERE Id = :objId');
    
        SObject record = Database.query('SELECT '+String.join(fields, ',')+ ' FROM '+sobjectName+' WHERE Id = :objId');

        Id recordId = (Id)record.get('Id');

        RecordType rt=[SELECT Id, Name, DeveloperName, Description FROM RecordType Where DeveloperName='Devis_approuv_Read_Only' LIMIT 1];
    
        if (httpResp == 'OK'){
            try{ 
                result = genericSendEmail(signerId, contactName, fromAddresses, toAddresses, cciEmail, senderDisplayName, subject, body, objId, filesIds, filesToAttach);
                for(String field: fields) {
                    System.debug(field);
                    if(field.contains('status') && field.length() <= 9) {
                        System.debug('2: '+field);
                        record.put(field, 'Envoyé pour Signature');
                    }
                }
                
                if(recordId.getSobjectType() == Quote.sObjectType) {
                    record.put('RecordTypeId', rt.Id);
                    record.put('TECH_isApprouved__c', true);

                    updateLockedRecord unlock = new updateLockedRecord();
                    unlock.updateRecord(record);
                } else {
                    update record;
                }
                
                if(filesToAttach != null && filesToAttach.size() > 0) {
                    System.debug(filesToAttach);
                    List<ContentDocumentLink> lstCdls = new List<ContentDocumentLink>();
                    for(String file: filesToAttach) {
                        lstCdls.add(new ContentDocumentLink(ContentDocumentId=file, LinkedEntityId=objId));
                    }
                    insert lstCdls;
                }
            }
            catch(exception ex){
                System.debug('####### Error : '+ex.getLineNumber()+' - '+ ex.getMessage());
            }
        }
        else{
            System.debug('####### Error');
        }
        System.debug('########## END createPackagePreFip');
        return result;
    }
    
    public static Map<String,String> genericSendEmail(String contactId, String contactName, String fromAddresses, String toAddresses, String cciEmail, String senderDisplayName, String subject, String body, String objId, List<String> filesIds, List<String> filesToAttach){
    
        Map<String,String> result = new Map<String,String>();
    
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
    
        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
        Messaging.Emailfileattachment fileAttachment = new Messaging.Emailfileattachment();

        Contact c = [Select Language__c from Contact where Id = :contactId];
    
        ContentVersion cversion = new ContentVersion ();
    
        Quote qte;
    
        String finalDocFilter;
        String taskContactId;
    
        if(objId != null) {
    
            List<Messaging.Emailfileattachment> fileLs = new List<Messaging.Emailfileattachment>();
    
            /* T5F_PreFip__c.sObjectType) {
                taskContactId = null;
                finalDocFilter = 'ContentDocument.title like \'%Final%\' AND ';
            } else */ if(Id.valueOf(objId).getSobjectType() == Quote.sObjectType) {
                taskContactId = contactId;
                qte = [SELECT Quote_Type__c, OpportunityBranch__c FROM Quote WHERE Id = :objId];
                if (qte.Quote_Type__c == 'Proposition commerciale'){
                    String fileName = ConnectiveV4ApiUtils.getStaticResourceNameFromFiliale(qte.OpportunityBranch__c, c.Language__c);
                    StaticResource sr = [SELECT Id, Name, Body FROM StaticResource WHERE Name=:fileName];
                    Messaging.Emailfileattachment srAttachment = new Messaging.Emailfileattachment();
                    srAttachment.setFileName(sr.Name + '.pdf');
                    srAttachment.setBody(sr.Body);
                    fileLs.add(srAttachment);
                    message.setFileAttachments(fileLs);
                }
            }

            List<ContentVersion> cvtasks = new List<ContentVersion>();
    
            if(filesIds == null) {
                List<Attachment> att=[SELECT id, Name, body, ContentType,ParentId FROM Attachment WHERE ParentId=:objId];
                List<ContentDocumentLink> files  = Database.query('SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLink WHERE '+finalDocFilter+'LinkedEntityId =:objId');
                System.debug(files);
                if(files != null && files.size()>0) {//Salesforce Lightning UI running
                    cversion =  [SELECT title, PathOnClient, FileType, versiondata, FileExtension, ContentDocumentId FROM contentversion WHERE ContentDocumentId =: files[0].ContentDocumentId];
                    System.debug(cversion);
                    cvtasks.add(cversion);
                    String title = (cversion.title.lastIndexOf('.') != -1) ? cversion.title : cversion.title +'.'+ cversion.FileExtension;
        			message.setEntityAttachments(new List<ID> {cversion.Id});
                } else if(att !=null && att.size()>0) { //Salesforce Classic UI running
                    fileAttachment.setFileName(att[0].Name);
                    fileAttachment.setBody(att[0].Body);
                    fileLs.add(fileAttachment);
                    message.setFileAttachments(fileLs);
                }	
            } else {
                List<Id> ids = new List<Id>();
                ids.addAll(filesIds);
                if(filesToAttach != null && filesToAttach.size() > 0)
                    ids.addAll(filesToAttach);
                List<ContentVersion> cverisons = [SELECT ID, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN :ids];
                cvtasks = cverisons;
                List<Id> cvids = new List<Id>();
                for(ContentVersion cv: cverisons) {
                    cvids.add(cv.Id);
                }
                message.setEntityAttachments(cvids);
            }
    
            // Add to attachment file list
            //message.setEntityAttachments(new List<ID> {cversion.Id});
            message.setReplyTo(fromAddresses);
            message.toAddresses = new String[] {toAddresses};
            message.ccaddresses = cciEmail.trim().split(';');
            message.bccaddresses = new String[] {fromAddresses};
            message.subject = subject;
            message.setSenderDisplayName(senderDisplayName);
            message.htmlBody = body;
            message.setWhatId(objId);
            message.setTargetObjectId(contactId);
            message.setSaveAsActivity(true);

    
            mails.add(message);
            Messaging.sendEmail(mails);
            //Create activity
            // Task t = createEmailActivity(subject, body, taskContactId, objId);
            // insert t;
            //Add the attachment on task
            // insert linkAttToTask(t.Id, cvtasks);
    
        }
    
        result.put('message', 'Envoyé pour signature');
        result.put('showToastMode', 'success');
        return result;
    }

    @AuraEnabled
    public static Map<String,String> deleteDocument(String Id){
        ContentDocument doc = [SELECT Id FROM ContentDocument WHERE Id =: Id];
        try {
            delete doc;
            return new Map<String,String>{'deleted'=>'OK'};
        } catch (Exception e) {
            System.debug(e.getMessage()+' '+e.getLineNumber());
            return new Map<String,String>{'deleted'=>'KO'};
        }
    }

    class RequestResponse {
        @AuraEnabled
        public String message {get; set;}
        @AuraEnabled
        public String showToastMode {get; set;}
        @AuraEnabled
        public Quote objectQuote {get; set;}
        @AuraEnabled
        public T5F_PreFip__c objectPreFip {get; set;}
        @AuraEnabled
        public  List<String> toAddresses {get; set;}
        @AuraEnabled
        public List<String> ccaddresses {get; set;}
        @AuraEnabled
        public List<String> bccaddresses {get; set;}
        @AuraEnabled
        public List<String> replyTo {get; set;}
        @AuraEnabled
        public String senderDisplayName {get; set;}
        @AuraEnabled
        public String subject {get; set;}
        @AuraEnabled
        public String contactName {get; set;}
        @AuraEnabled
        public String contactId {get; set;}
        @AuraEnabled
        public String htmlBody {get; set;}
        @AuraEnabled
        public Boolean mailStatus {get; set;}
        @AuraEnabled
        public String isShowForm {get; set;}
        @AuraEnabled
        public List<ContentVersion> cversions {get; set;}
        @AuraEnabled
        public List<Messaging.Emailfileattachment> atts {get; set;}
        @AuraEnabled
        public List<String> fileId {get; set;}
        @AuraEnabled
        public String status {get; set;}
        
    }
        
    //-------------------------------Mass Email methods---------------------
        
    @auraEnabled
    public static List<Quote> getQuoteRecords() {//Initialize SendMassEmailList
        system.debug('getQuoteRecords::Start: ');
        //Integer thisYear = System.today().year();
        //Integer nextYear = thisYear + 1;
        String Nextyear = System.Label.YearN1;

        Date dt;
        DateTime dtTime;

        if(System.today().month() < 9) {
            dt = Date.valueOf(String.valueOf(Integer.valueOf(System.today().year())-1)+'-09-01');
            dtTime = DateTime.newInstanceGmt(dt.year(), dt.month(), dt.day(), 0, 0, 0);
        } else {
            dt = Date.valueOf(System.today().year()+'-09-01');
            dtTime = DateTime.newInstanceGmt(dt.year(), dt.month(), dt.day(), 0, 0, 0);
        }
        
        String query = 'SELECT Name,Quote_Type__c,Contact.Email,OpportunityId,Opportunity.Salesman__c,Opportunity.Filiale__c,AccountId,Account.Name,'+
        'Commercial__c,ContactId,Contact.Name,TotalPrice,Status, Opportunity.Id, Opportunity.Convention_face_face__c,IsSyncing '+
        'FROM QUOTE WHERE Status NOT IN (\'Envoyé pour signature\',\'Signé\', \'Refusé\') AND IsSyncing = true '+
        'AND RecordType.DeveloperName NOT IN (\'QUO_NotApproved\', \'Devis_non_approuv_Conga\') '+
        'AND Opportunity.StageName!=\'Abandonnée\' AND CreatedDate >= :dtTime';

        return Database.query(query);
    }
        
    @auraEnabled
    public static String SendEmailRecords(List<Quote> lstQuote) {
        system.debug('SendEmailRecords --- > Start ');
        for(Quote q:lstQuote) {
            q.TECH_MassSignature__c = true;
            q.TECH_Assistante__c=UserInfo.getUserId();
        }	
        LC_MassCreatedPackage_AugTarifCtrl sendMassMailJob = new LC_MassCreatedPackage_AugTarifCtrl(lstQuote,null);
        // enqueue the job for processing
        ID jobID = System.enqueueJob(sendMassMailJob);
        System.debug('jobID : '+jobID);
    
        return 'L’envoi pour signature a été exécuté avec succès';
    }
        
    @auraEnabled
    public static String  checkAndSendQuoteRecords( List<Quote> lstQuote) {
        String resp = '';
        Boolean isAnError = false;
        Boolean isAlreadySent = false;
        Boolean isEmailAddressBounced = false;
        Boolean isFileNameTooLong = false;
        List<Quote> quotesToSend = new List<Quote>();
        List<Quote> quotesWithError = new List<Quote>();
        List<Quote> quotesSent = new List<Quote>();
        List<Quote> quotesEmailBounced = new List<Quote>();
        List<ContentDocumentLink> docsFileNameTooLong = new List<ContentDocumentLink>();
        Set<Id> ids = new Set<Id>();
    
        for(Quote q : lstQuote) {
            ids.add(q.Id);
        }
        //Get Quotes with related docs
        List<Quote> quotes = [SELECT Name,Quote_Type__c,TECH_UID__c,QuoteCode__c,Email,OpportunityId, Opportunity.Filiale__c,AccountId,Account.Name,Commercial__c,ContactId,Contact.Name,Contact.Email,Contact.IsEmailBounced,TotalPrice,Status,Opportunity.Salesman__c, IsSyncing,(SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLinks),(SELECT ID,Name FROM Attachments) FROM Quote Where ID IN :ids];
        for(quote q:Quotes) {
            Integer docSize = q.ContentDocumentLinks.size();
            List<ContentDocumentLink> lstDocs = q.ContentDocumentLinks;
            List<Attachment> lstAtts = q.Attachments;
            if(lstDocs.size()==1 && q.Opportunity.Salesman__c!=Null && q.ContactId!=Null && q.Contact.Email!=Null) {
                if(lstDocs[0].ContentDocument.title.length() > 75){
                    docsFileNameTooLong.add(lstDocs[0]);
                    isFileNameTooLong = true;
                }
                QuotesTosend.add(q);
            }else{
                QuotesWithError.add(q);
                isAnError=true;
            }
            if(q.TECH_UID__c!=null) {
                QuotesSent.add(q);
                isAlreadySent=true;
            }
    
            if(q.Contact.IsEmailBounced ==true) {
                quotesEmailBounced.add(q);
                isEmailAddressBounced=true;
            }
        }
    
        if(!isAnError && !isAlreadySent && !isEmailAddressBounced && !isFileNameTooLong) {
            resp =  SendEmailRecords(QuotesTosend);
            //resp = 'Success !';
        }else{
            if(isAlreadySent) {
                resp = 'Vous avez sélectionné un devis déjà envoyé pour signature';
            }else if(isAnError) {
                resp = 'Erreur pendant l\'envoi pour signature. Pas de convention rattachée au dévis, ou adresse email manquante sur le contact.';
            }else if(isEmailAddressBounced) {			
                for(Quote qt : quotesEmailBounced){				
                    resp +='- '+qt.Contact.Email + ' ('+qt.Contact.Name +')  ';
                    
                }
            
            }else if(isFileNameTooLong) {
                resp = 'Les noms de fichier suivants sont trop longs (maximum 75 caractères) : \n\n';
                for(ContentDocumentLink doc : docsFileNameTooLong){
                    resp += '- ' + doc.ContentDocument.title + '\n';
                }
            }
        }
        System.debug('resp :' +resp );
        return resp;
    }
        
    // Class for updating locked record (without sharing to by pass locked record)
    without sharing class updateLockedRecord {	  
        public void updateRecord(SObject qt) { 		
            update qt; 
        }
    }
        
    //-------------------------------Mass Reminder methods---------------------
        
    @auraEnabled
    public static List<Quote> getQuoteRecordsReminder() {//Initialize SendMassEmailList
        String nextyear = System.Label.YearN1;
        //Integer nextYear = 2021;

        Date dt;
        DateTime dtTime;

        if(System.today().month() < 9) {
            dt = Date.valueOf(String.valueOf(Integer.valueOf(System.today().year())-1)+'-09-01');
            dtTime = DateTime.newInstanceGmt(dt.year(), dt.month(), dt.day(), 0, 0, 0);
        } else {
            dt = Date.valueOf(System.today().year()+'-09-01');
            dtTime = DateTime.newInstanceGmt(dt.year(), dt.month(), dt.day(), 0, 0, 0);
        }
        
        String query = 'SELECT Name,Quote_Type__c,NbSendReminder__c,Contact.Email,OpportunityId,Opportunity.Salesman__c,Opportunity.Filiale__c,AccountId,Account.Name,Commercial__c,ContactId,Contact.Name,TotalPrice,Status, Opportunity.Id, Opportunity.Convention_face_face__c,IsSyncing FROM QUOTE '+
        'WHERE Status IN (\'Envoyé pour signature\') AND IsSyncing = true '+
        'AND RecordType.DeveloperName NOT IN (\'QUO_NotApproved\', \'Devis_non_approuv_Conga\') '+
        'AND Opportunity.StageName != \'Abandonnée\' AND CreatedDate >= :dtTime';

        return Database.query(query);
            
    }

        @auraEnabled
    public static String SendReminders(List<Quote> lstQuote) {
        system.debug('SendEmailRecords --- > Start ');
        ConnectiveV4AsyncSendReminder sendMassMailJob = new ConnectiveV4AsyncSendReminder(lstQuote,lstQuote);
        // enqueue the job for processing
        ID jobID = System.enqueueJob(sendMassMailJob);
        System.debug('jobID : '+jobID);
        return 'L’envoi de rappel a été exécuté avec succès';
    }
        
}