/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : Batch_Connective_Get_UpdatedInfo
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : Batch_Connective_Get_UpdatedInfo
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 10-08-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
public class Batch_Connective_Get_UpdatedInfo implements Queueable, Database.AllowsCallouts {

    public Map<Id,DocumentsForSignature__c> docToUpload = new Map<Id,DocumentsForSignature__c>();
    public List<Id> quoteMailedLs = new List<Id>(); //create a set of quote id already mailed to avoid double mails

    public Batch_Connective_Get_UpdatedInfo(List<DocumentsForSignature__c> docToUpload){
            if(docToUpload !=null){
                Map<Id,DocumentsForSignature__c> docs = new Map<Id,DocumentsForSignature__c>(docToUpload);
                this.docToUpload = new Map<Id,DocumentsForSignature__c>([SELECT Id,ReasonRejected__c,DownloadSignedURL__c,SigningUrl__c,Quote__c,Quote__r.OwnerId,FIP_FIP__c,FIP_FIP__r.OwnerId,Package__r.ConnectivePackageId__c,ConnectiveDocumentId__c FROM DocumentsForSignature__c WHERE Id IN :docs.keySet()]);
            }
        }

    public void execute(QueueableContext context) {
        try{
            if(docToUpload.size()>0){
                for(Id docId : docToUpload.keySet()){
                    manageGetUpdateInfo(docToUpload.get(docId));
                    docToUpload.remove(docId);
                    //also remove doubles
                    for (Id docDouble : docToUpload.keySet()){
                        if (quoteMailedLs.contains(docToUpload.get(docDouble).Quote__c)){
                            docToUpload.remove(docDouble);
                        }
                    }
                    if(docToUpload.size()>0){
                        System.enqueueJob(new Batch_Connective_Get_UpdatedInfo(docToUpload.values()));
                    }
                    break;
                }
            }
        }
        catch(Exception ex){
            Log__c error = new Log__c(
                ApexJob__c= Batch_Connective_Get_UpdatedInfo.class.getName(),
                ErrorMessage__c=ex.getMessage(),
                ExceptionType__c='APEX'
            );
            insert error;
        }
    }

    public void manageGetUpdateInfo(DocumentsForSignature__c doc){
        Id objId;
        //check if quote and if quote not already sent
        if(doc.Quote__c !=null && !this.quoteMailedLs.contains(doc.Quote__c)){
            ConnectiveApiUtils.updateAllStatus(doc.Quote__c); // this will update all docs on the quote
            this.quoteMailedLs.add(doc.Quote__c);
        }
        else if(doc.FIP_FIP__c !=null){
            ConnectiveApiManager.updateStatusByFipQuote(doc.FIP_FIP__c);
        }
    }
}