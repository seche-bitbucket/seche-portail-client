/* -------------
  Auth : Baïla TOURE
  Date : 24/02/2020
*/
@isTest
public class AttestationListViewButtonControllerTest {

   @testSetup static void setup() {
        // Create common test accounts
       List<Account> listAccount = TestDataFactory.createAccountList('TestName',5); 
       insert listAccount;

       //Create Contacts 
       List<Contact> listContact = new List<Contact>();
       for(Account acc : listAccount){
           listContact.add(TestDataFactory.createContact(acc));
       } 
       insert listContact;

       //Create Assistant Commercial
       User assistantCo = TestDataFactory.createAssistantCommercialUser();

        //Create Resp Commercial
       User responsableCo = TestDataFactory.createResponsableCommercialUser(assistantCo);
       

       //Create Attestations 
       // params : TestDataFactory.createAttestation(Contact c, User responsableCo, Boolean isWood, Boolean isMixed, Boolean isPaperCardboard, Boolean isPlastic, Boolean isGlass, Boolean isSorted, Boolean isTechCongaAttestationToGenerate, String status)
       /* List<Attestation__c> listAttestation = new List<Attestation__c>(); */

       RecordType recType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Attestation_Tri_5_Flux'];

       Attestation__c att1 = TestDataFactory.createAttestation(listContact[0], responsableCo, false, false, false,false,false, true, false, true/* false */, true, false, Constants.ATTESTATION_DRAFT_STATUS /* ATTESTATION_GENERATE_STATUS */ );
       att1.RecordTypeId = recType.Id;
       insert att1;

       Attestation__c att2 = TestDataFactory.createAttestation(listContact[3], responsableCo, true, true, true,true,false, false, false, false,true, false,  Constants.ATTESTATION_DRAFT_STATUS /* ATTESTATION_NOTCONCERNED_STATUS */);
       att2.RecordTypeId = recType.Id;
       insert att2;

       Attestation__c att3 = TestDataFactory.createAttestation(listContact[1], responsableCo, true, true, true,false,false, false, false, false,true, false, Constants.ATTESTATION_DRAFT_STATUS /* ATTESTATION_SEND_STATUS */);
       att3.RecordTypeId = recType.Id;
       insert att3;

       Attestation__c att4 = TestDataFactory.createAttestation(listContact[2], responsableCo, false, false, false,false,false, true, false, true,true, true, Constants.ATTESTATION_DRAFT_STATUS);
       att4.RecordTypeId = recType.Id;
       insert att4;

       Attestation__c att5 = TestDataFactory.createAttestation(listContact[4], responsableCo, false, false, false,false,false, true, false, true, true, true, Constants.ATTESTATION_DRAFT_STATUS);
       att5.RecordTypeId = recType.Id;
       insert att5;

       /* listAttestation.add(TestDataFactory.createAttestation(listContact[0], responsableCo, false, false, false,false,false, true, false, false, true, false, Constants.ATTESTATION_GENERATE_STATUS)); */ 
       /* listAttestation.add(TestDataFactory.createAttestation(listContact[3], responsableCo, true, true, true,true,false, false, false, false,true, false,  Constants.ATTESTATION_NOTCONCERNED_STATUS));   */    
       /* listAttestation.add(TestDataFactory.createAttestation(listContact[1], responsableCo, true, true, true,false,false, false, false, false,true, false, Constants.ATTESTATION_SEND_STATUS));    */    
       /* listAttestation.add(TestDataFactory.createAttestation(listContact[2], responsableCo, false, false, false,false,false, true, false, false,true, true, Constants.ATTESTATION_DRAFT_STATUS)); */ //with generate status (workflow)
       /* listAttestation.add(TestDataFactory.createAttestation(listContact[4], responsableCo, false, false, false,false,false, true, false, false, true, true, Constants.ATTESTATION_DRAFT_STATUS)); */
      
       /* insert listAttestation;  */
       
    
    }

    
    

     @isTest static void testNoSelectedAttestToSend() {
        PageReference pageRef = Page.VFP_SendAttestations;              
        List<Attestation__c> listAttestation = new List<Attestation__c>();
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(listAttestation); 
        //stdSetController.setSelected(listAttestation); 
        Test.StartTest();    
        Test.setCurrentPage(pageRef);       
        AttestationListViewButtonController ext = new AttestationListViewButtonController(stdSetController);
        ext.send();
        Test.StopTest();
        System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.WARNING));
    }

    @isTest static void testNotConcernedAttestToSend() {
        PageReference pageRef = Page.VFP_SendAttestations;         
        String status = Constants.ATTESTATION_NOTCONCERNED_STATUS; 
        System.System.debug('status : '+status);       
        List<Attestation__c> listAttestation = [SELECT Id, Name, Status__c FROM Attestation__c WHERE Status__c = :status];   
        System.System.debug('testNotConcernedAttestToGenerate : '+listAttestation);   
        Test.StartTest(); 
        Test.setCurrentPage(pageRef);   
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(listAttestation);    
        stdSetController.setSelected(listAttestation);    
        AttestationListViewButtonController ext = new AttestationListViewButtonController(stdSetController);
        ext.send();
        Test.StopTest();

        System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.WARNING));
    }

     @isTest static void testAttestAlreadySend() {
        PageReference pageRef = Page.VFP_SendAttestations;        
        String status = Constants.ATTESTATION_SEND_STATUS;           
        List<Attestation__c> listAttestation = [SELECT Id, Name, Status__c FROM Attestation__c WHERE Status__c = :status];   
        System.System.debug('testNotConcernedAttestToGenerate : '+listAttestation);   
        Test.StartTest();  
        Test.setCurrentPage(pageRef);  
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(listAttestation);    
        stdSetController.setSelected(listAttestation);    
        AttestationListViewButtonController ext = new AttestationListViewButtonController(stdSetController);
        ext.send();
        Test.StopTest();
        System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.WARNING));
    }

 @isTest static void testIsNotGenerateSatusEmailSend() {
        PageReference pageRef = Page.VFP_SendAttestations;         
        String status = Constants.ATTESTATION_DRAFT_STATUS;           
        List<Attestation__c> listAttestation = [SELECT Id, Name, Status__c FROM Attestation__c WHERE Status__c = :status];   
        System.System.debug('testNotConcernedAttestToGenerate : '+listAttestation); 
        Test.StartTest();   
        Test.setCurrentPage(pageRef);  
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(listAttestation);    
        stdSetController.setSelected(listAttestation);    
        AttestationListViewButtonController ext = new AttestationListViewButtonController(stdSetController);
        ext.send();
        Test.StopTest();
        System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.WARNING));
    }

    

    @isTest static void testAttestationWithoutFile() {
        PageReference pageRef = Page.VFP_SendAttestations;          
        String status = Constants.ATTESTATION_GENERATE_STATUS;           
        List<Attestation__c> listAttestation = [SELECT Id, Name, Status__c FROM Attestation__c WHERE Status__c = :status];   
        System.System.debug('testNotConcernedAttestToGenerate : '+listAttestation);  
        Test.StartTest(); 
        Test.setCurrentPage(pageRef);  
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(listAttestation);    
        stdSetController.setSelected(listAttestation);    
        AttestationListViewButtonController ext = new AttestationListViewButtonController(stdSetController);
        ext.send();
        Test.StopTest();
        System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.WARNING));
    }

     @isTest static void testAttestationWithMultiFiles() {
        PageReference pageRef = Page.VFP_SendAttestations;       
        String status = Constants.ATTESTATION_DRAFT_STATUS;           
        /* String status = Constants.ATTESTATION_GENERATE_STATUS;  */          
        List<Attestation__c> listAttestation = [SELECT Id, Name, Status__c FROM Attestation__c WHERE Status__c = :status];   
        System.System.debug('testAttestationWithMultiFiles : '+listAttestation); 
        //Create factures (files) 
            // 1- Attestation with multiple files
            List<ContentDocumentLink> listContentDocumentLink = new List<ContentDocumentLink>();
            listContentDocumentLink.add(TestDataFactory.createFacture(listAttestation[0],0));
            listContentDocumentLink.add(TestDataFactory.createFacture(listAttestation[0],1));  
            insert listContentDocumentLink;
        Test.StartTest(); 
        Test.setCurrentPage(pageRef);   
        List<Attestation__c> listSelectedAttestation = [SELECT id,Name, Status__c, Contact__r.Id, Contact__r.EMail, Contact__r.IsEmailBounced,AssociatedSalesRep__c,AssociatedSalesRep__r.Email, AssociatedSalesRep__r.Name,Sorted__c, Mixed__c, Papercardboard__c,Plastic__c,Metal__c, Glass__c, Wood__c,(SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLinks) FROM Attestation__c WHERE Id IN :listAttestation];   
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(listSelectedAttestation);    
        stdSetController.setSelected(listSelectedAttestation);    
        AttestationListViewButtonController ext = new AttestationListViewButtonController(stdSetController);
        ext.send();
        Test.StopTest();
        System.assert( ext.isExistMultipleFilesOnAttest(listSelectedAttestation).size() >0);
    }

     @isTest static void testSendEmail() {
        PageReference pageRef = Page.VFP_SendAttestations;        
        String status = Constants.ATTESTATION_GENERATE_STATUS;            
        List<Attestation__c> listAttestation = [SELECT Id, Name, Status__c FROM Attestation__c];   
         //Create factures (files) 
            // 1- Attestation with multiple files
            List<ContentDocumentLink> listContentDocumentLink = new List<ContentDocumentLink>();
            listContentDocumentLink.add(TestDataFactory.createFacture(listAttestation[0],0));
            listContentDocumentLink.add(TestDataFactory.createFacture(listAttestation[1],0));
            listContentDocumentLink.add(TestDataFactory.createFacture(listAttestation[2],0));
            listContentDocumentLink.add(TestDataFactory.createFacture(listAttestation[3],0)); 
            listContentDocumentLink.add(TestDataFactory.createFacture(listAttestation[4],0));
            insert listContentDocumentLink;
        Test.StartTest(); 
        Test.setCurrentPage(pageRef);   
        List<Attestation__c> listSelectedAttestation = [SELECT id,Name, Status__c, Contact__r.Id, Contact__r.EMail, Contact__r.IsEmailBounced,AssociatedSalesRep__c,AssociatedSalesRep__r.Email, AssociatedSalesRep__r.Name,Sorted__c, Mixed__c, Papercardboard__c,Plastic__c,Metal__c, Glass__c, Wood__c,(SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLinks) FROM Attestation__c WHERE Id IN :listAttestation]; 
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(listSelectedAttestation);    
        stdSetController.setSelected(listSelectedAttestation);    
        AttestationListViewButtonController ext = new AttestationListViewButtonController(stdSetController);            
        ext.sendEmails();
        Test.StopTest();
        System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.CONFIRM));
    }

    //Generate attestation Tests

    
 @isTest static void testIsNotGenerateSatusGenAtt() {
    PageReference pageRef = Page.VFP_GenerateAttestation;         
    String status = Constants.ATTESTATION_DRAFT_STATUS;           
    List<Attestation__c> listAttestation = [SELECT Id, Name, Status__c FROM Attestation__c WHERE Status__c = :status];   
    System.System.debug('testIsNotGenerateSatusGenAtt : '+listAttestation); 
    Test.StartTest();   
    Test.setCurrentPage(pageRef);  
    ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(listAttestation);    
    stdSetController.setSelected(listAttestation);    
    AttestationListViewButtonController ext = new AttestationListViewButtonController(stdSetController);
    ext.initAttestations();
    Test.StopTest();
    System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.WARNING));
}


@isTest static void testIsNoAttestationSelected() {
    PageReference pageRef = Page.VFP_GenerateAttestation;         
    String status = Constants.ATTESTATION_GENERATE_STATUS;            
    List<Attestation__c> listAttestation = new List<Attestation__c>();   
    System.System.debug('testIsAlreadyGenerate : '+listAttestation); 
    Test.StartTest();   
    Test.setCurrentPage(pageRef);  
    ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(listAttestation);    
    stdSetController.setSelected(listAttestation);    
    AttestationListViewButtonController ext = new AttestationListViewButtonController(stdSetController);
    ext.initAttestations();
    Test.StopTest();
    System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.WARNING));
}

@isTest static void testIsAlreadyGenerate() {
    PageReference pageRef = Page.VFP_GenerateAttestation;         
    String status = Constants.ATTESTATION_GENERATE_STATUS;            
    List<Attestation__c> listAttestation = [SELECT Id, Name, Status__c FROM Attestation__c WHERE Status__c = :status];   
    System.System.debug('testIsAlreadyGenerate : '+listAttestation); 
    Test.StartTest();   
    Test.setCurrentPage(pageRef);  
    ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(listAttestation);    
    stdSetController.setSelected(listAttestation);    
    AttestationListViewButtonController ext = new AttestationListViewButtonController(stdSetController);
    ext.initAttestations();
    Test.StopTest();
    System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.WARNING));
}

@isTest static void testNotConcernedAttestToGen() {
    PageReference pageRef = Page.VFP_SendAttestations;         
    String status = Constants.ATTESTATION_NOTCONCERNED_STATUS; 
    System.System.debug('status : '+status);       
    List<Attestation__c> listAttestation = [SELECT Id, Name, Status__c FROM Attestation__c WHERE Status__c = :status];   
    System.System.debug('testNotConcernedAttestToGenerate : '+listAttestation);   
    Test.StartTest(); 
    Test.setCurrentPage(pageRef);   
    ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(listAttestation);    
    stdSetController.setSelected(listAttestation);    
    AttestationListViewButtonController ext = new AttestationListViewButtonController(stdSetController);
    ext.initAttestations();
    Test.StopTest();

    System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.WARNING));
}


@isTest static void testGenerateAttestations() {
    PageReference pageRef = Page.VFP_GenerateAttestation;         
    String status = Constants.ATTESTATION_DRAFT_STATUS;            
    List<Attestation__c> listAttestation = [SELECT Id, Name, Status__c FROM Attestation__c WHERE Status__c = :status];   
    System.System.debug('testGenerateAttestations : '+listAttestation); 
    Test.StartTest();   
    Test.setCurrentPage(pageRef);  
    ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(listAttestation);    
    stdSetController.setSelected(listAttestation);    
    AttestationListViewButtonController ext = new AttestationListViewButtonController(stdSetController);
    ext.generateAttestations();
    Test.StopTest();
    System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.CONFIRM));
}


     

}