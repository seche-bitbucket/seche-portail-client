/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-07-17
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes      Sales details controller 
* 2018-07-03     Added details so that the numbers are better formatted and fixed an error where
the operation took too much time and cpu limit was reached
2018-07-17	Added CAN For year -2 , added number of contracts on the page, and the price that was
added from CAN to CAN+1 per account
* 				reorganised everything and added tabs for traitement only.
2018-26-09 	Added filiale as a parameter and also as a column for all accounts
added number of amendments face a face and amendments increased 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class VFC_SalesDetails {
    public Id UserId {get;set;}
    public String Filiale {get;set;}
    public String Filiere {get;set;}
    public String FiliereUrl {get;set;}
    public String FilialeURL{get;set;}
    public String FiliereParam {get;set;}
    public String FilialeParam{get;set;}
    public User user{get;set;}
    public String AmendmentsSize {get;set;}
    public Transient List<Contract__c> Contracts {get;set;}
    public List<ID> ContractsID{get;set;}
    public List<ContractByAccount> ContractsByAccounts{get;set;}
    public String CANminus2UserString{get;set;}
    public String CANminus1UserString{get;set;}
    public String CanUserString{get;set;}
    public String CAN1UserString{get;set;}
    public String TonnageUserString{get;set;}
    public String Tonnageminus1UserString{get;set;}
    public String Tonnageminus2UserString{get;set;}
    public String CANProjete{get;set;}
    public String CAN1Projete{get;set;}
    public String TonnageProjete{get;set;}
    public String ValorisationEuro{get;set;}
    public String ValorisationEuroProjete{get;set;}
    public String PourcentageAugmentation {get;set;}
    public String PourcentageAugmentationProjete{get;set;}
    public String CANminus2UserStringTraitement{get;set;}
    public String CANminus1UserStringTraitement{get;set;}
    public String CanUserStringTraitement{get;set;}
    public String CAN1UserStringTraitement{get;set;}
    public String TonnageUserStringTraitement{get;set;}
    public String Tonnageminus1UserStringTraitement{get;set;}
    public String Tonnageminus2UserStringTraitement{get;set;}
    public String CANProjeteTraitement{get;set;}
    public String CAN1ProjeteTraitement{get;set;}
    public String TonnageProjeteTraitement{get;set;}
    public String PourcentageAugmentationTraitement {get;set;} 
    public String PourcentageAugmentationProjeteTraitement{get;set;}
    public String ValorisationEuroTraitement{get;set;}
    public String ValorisationEuroProjeteTraitement{get;set;}
    public String AvenantsAugmentes{get;set;}
    public String CANFaceaFace{get;set;}
    public String AvenantsFaceaFace{get;set;}
    public Report reportAmendments{get;set;}
    public String Status{get;set;}
    public VFC_SalesDetails(){
        this.ContractsID=new List<Id>();
        Contracts=new List<Contract__c>();
        this.ContractsByAccounts=new List<ContractByAccount>();
        // get the associated sales rep from the visualforce page parameter
        this.UserId = System.currentPageReference().getParameters().get('userid');
        String filialeFiliere = System.currentPageReference().getParameters().get('name');
        this.FilialeParam = System.currentPageReference().getParameters().get('FilialeParam');
        this.FiliereParam = System.currentPageReference().getParameters().get('FiliereParam');
        Approbation_user__mdt filialeFiliereMdt  = [SELECT Filiale__c, Filiere__c  FROM Approbation_user__mdt WHERE DeveloperName =:filialeFiliere];

        this.FilialeURL = filialeFiliereMdt.Filiale__c;
        this.FiliereUrl = filialeFiliereMdt.Filiere__c;
        // retrieve the report that shows the details for the amendments linked to the associated sales rep
        reportAmendments=[SELECT Id, DeveloperName FROM Report WHERE DeveloperName = 'X07cAug_en_CA_aug_VS_non_soumis_AUP' LIMIT 1];
       
        if(userid!=null){         
            this.user=[SELECT LastName, FirstName, Username, Id,email, Name, Title, MediumPhotoUrl FROM User Where Id=:userId];

            if(FilialeParam!=null && FiliereParam!=null){
                this.Contracts = [SELECT Id, Name, AssociatedSalesRep__c,Filiale__c, Account__r.Name, Account__c,RecordLocked__c, toLabel(status__c),
                                  (SELECT Id,Nature__c ,Price2YearsAgo__c,UnitNextYear__c ,Quantity2YearsAgo__c, QuantityCurrentYear__c,QuantityPreviousYear__c,
                                   ByPassContract__c,UnitCurrentYear__c,UnitPreviousYear__c,PricePreviousYear__c, PriceCurrentYear__c, PriceNextYear__c, WasteLabel__c, Account__c,Contrat2__r.Filiale__c, 
                                   Contrat2__c, Contrat2__r.createdDate,toLabel(Contrat2__r.status__c) FROM Avenants__r) 
                                   FROM Contract__c Where AssociatedSalesRep__c=:UserId and Status__c ='Soumis' and  Filiale__c=:FilialeURL and Filiere__c=:FiliereUrl Order By Account__r.Name];
            }else if(FilialeParam!=null){
                  this.Contracts = [SELECT Id, Name, AssociatedSalesRep__c,Filiale__c, Account__r.Name, Account__c,RecordLocked__c, toLabel(status__c),
                                  (SELECT Id,Nature__c ,Price2YearsAgo__c,UnitNextYear__c ,Quantity2YearsAgo__c, QuantityCurrentYear__c,QuantityPreviousYear__c,
                                   ByPassContract__c,UnitCurrentYear__c,UnitPreviousYear__c,PricePreviousYear__c, PriceCurrentYear__c, PriceNextYear__c, WasteLabel__c, Account__c,Contrat2__r.Filiale__c, 
                                   Contrat2__c, Contrat2__r.createdDate,toLabel(Contrat2__r.status__c) FROM Avenants__r) 
                                   FROM Contract__c Where AssociatedSalesRep__c=:UserId and Status__c ='Soumis' and Filiale__c=:FilialeURL  Order By Account__r.Name];
            }else{
                 // retrieve user details and contracts associated with the user
              
                this.Contracts = [SELECT Id, Name, AssociatedSalesRep__c,Filiale__c, Account__r.Name, Account__c,RecordLocked__c, toLabel(status__c),
                                (SELECT Id,Nature__c ,Price2YearsAgo__c,UnitNextYear__c,Quantity2YearsAgo__c, QuantityCurrentYear__c,QuantityPreviousYear__c,
                                ByPassContract__c,UnitCurrentYear__c,UnitPreviousYear__c,PricePreviousYear__c, PriceCurrentYear__c, PriceNextYear__c, WasteLabel__c, Account__c,Contrat2__r.Filiale__c, 
                                Contrat2__c, Contrat2__r.createdDate,toLabel(Contrat2__r.status__c) FROM Avenants__r) 
                                FROM Contract__c Where AssociatedSalesRep__c=:UserId and Status__c ='Soumis' Order By Account__r.Name];
            }
        }

        if(this.Contracts.size()>0){
            this.CANProjete ='0.0';
            this.CAN1Projete ='0.0';
            this.TonnageProjete='0';
            AmendmentsSize='0';
            Decimal AvenantsAugmentes=0;
            // filling map with amendments for every unique account
            Map<String,List<Amendment__c>> AmendmentsMap =new Map<String,List<Amendment__c>>();
            Integer z=0;
            for(Contract__c t: this.Contracts){
                ContractsID.add(t.Id);
                List<Amendment__c> Amendments=new List<Amendment__c>();
                while( z<this.contracts.size() && t.Account__r.Name==this.Contracts[z].Account__r.Name ){
                    amendments.addAll(this.Contracts[z].Avenants__r);
                    z++;
                }
                if(Amendments.size()>0){
                    AmendmentsMap.put(t.Account__r.Name,amendments);
                }
                AvenantsAugmentes=AvenantsAugmentes+t.Avenants__r.size();
            }
            List<Amendment__c> AmendmentsTraitement=new List<Amendment__c>();
            AmendmentsTraitement=[SELECT Id,Nature__c, Contrat2__c FROM Amendment__c Where Contrat2__c IN:ContractsID AND Nature__c='Traitement'];
            AmendmentsSize=FormatNumberUtils.getCorrectNumberInteger(AmendmentsTraitement.size());
            //Parameters shown to the user filled from amendment prices and tonnage
            Decimal CANUser=0.0;
            Decimal CAN1User=0.0;
            Decimal CANMinus1User=0.0;
            Decimal CANMinus2User=0.0;
            Integer TonnageUser=0;
            Integer TonnageMinus1User=0;
            Integer TonnageMinus2User=0;
            Decimal CANUserTraitement=0.0;
            Decimal CAN1UserTraitement=0.0;
            Decimal CANMinus1UserTraitement=0.0;
            Decimal CANMinus2UserTraitement=0.0;
            Integer TonnageUserTraitement=0;
            Integer TonnageMinus1UserTraitement=0;
            Integer TonnageMinus2UserTraitement=0;
            DateTime creationd;
            String WeekOfcreation;
            Decimal ValorisationEuroVal=0.0;
            Decimal ValorisationEuroTraitementVal=0.0;
            Decimal AvenantsFaceaFace=0;
            Decimal CANFaF=0;   
            Set<String> FilialesDuCommercial=new Set<String>();
            Set<String> Status=new Set<String>();
            for(String i:AmendmentsMap.keyset()){
                Integer Ton=0;
                Integer Ton1=0;
                Integer Ton2=0;
                Decimal CAN=0.0;
                Decimal CAN1=0.0;
                Decimal CANminus1=0.0;
                Decimal CANminus2=0.0;
                Integer TonTraitement=0;
                Integer Ton1Traitement=0;
                Integer Ton2Traitement=0;
                Decimal CANTraitement=0.0;
                Decimal CAN1Traitement=0.0;
                Decimal CANminus1Traitement=0.0;
                Decimal CANminus2Traitement=0.0;
                Set<String> StatusPerAcc=new Set<String>();
                Boolean FaceaFace=false;
                for(Amendment__c n:AmendmentsMap.get(i)){
                    if(n.Contrat2__r.Filiale__c!=null && n.Contrat2__r.Filiale__c!='null' ){
                        FilialesDuCommercial.add(n.Contrat2__r.Filiale__c);
                    }
                    StatusPerAcc.add(n.Contrat2__r.status__c);
                    Status.add(n.Contrat2__r.status__c);
                    Integer k=1;
                    if(k==1){
                        creationd=n.Contrat2__r.createdDate;
                        WeekOfcreation= creationd.format('w');
                        k++;
                    } 
                    if(n.Quantity2YearsAgo__c!=null && n.UnitPreviousYear__c!=null) {
                        if(n.UnitPreviousYear__c=='Tonne')  {
                            Ton2+=n.Quantity2YearsAgo__c.intValue();
                        }
                    }
                    if(n.QuantityPreviousYear__c!=null  && n.UnitPreviousYear__c!=null) {
                        if(n.UnitPreviousYear__c=='Tonne')  {
                            Ton1=Ton1+n.QuantityPreviousYear__c.intValue();
                        }
                    }
                    if(n.QuantityCurrentYear__c!=null  && n.UnitCurrentYear__c!=null) {
                        if(n.UnitCurrentYear__c=='Tonne')  {
                            Ton+=n.QuantityCurrentYear__c.intValue();
                        }  
                    }
                    
                    if(n.PricePreviousYear__c!=null && n.QuantityPreviousYear__c!=null && n.UnitPreviousYear__c!=null){
                        if(n.UnitPreviousYear__c=='Forfait'){
                            CANminus1+=n.PricePreviousYear__c;
                        }else{
                            CANminus1+= (n.PricePreviousYear__c* n.QuantityPreviousYear__c.intValue());
                        }
                    }
                    if(n.Price2YearsAgo__c!=null && n.Quantity2YearsAgo__c!=null && n.UnitPreviousYear__c!=null){
                        if(n.UnitPreviousYear__c=='Forfait'){
                            CANminus2+= n.Price2YearsAgo__c;
                        }else{
                            CANminus2+= (n.Price2YearsAgo__c* n.Quantity2YearsAgo__c.intValue());
                        }
                    }
                    if(n.Nature__c=='Traitement'){
                        if(Ton2Traitement!=null && n.Quantity2YearsAgo__c!=null && n.UnitPreviousYear__c!=null && n.UnitPreviousYear__c=='Tonne') {
                            Ton2Traitement+=n.Quantity2YearsAgo__c.intValue();}
                        if(Ton1Traitement!=null && n.QuantityPreviousYear__c!=null&& n.UnitPreviousYear__c!=null && n.UnitPreviousYear__c=='Tonne') {
                            Ton1Traitement+=n.QuantityPreviousYear__c.intValue();}
                        if(TonTraitement!=null && n.QuantityCurrentYear__c!=null&& n.UnitCurrentYear__c!=null && n.UnitCurrentYear__c=='Tonne') {
                            TonTraitement+=n.QuantityCurrentYear__c.intValue();}
                        if(CANminus1Traitement!=null && n.PricePreviousYear__c!=null && n.QuantityPreviousYear__c!=null && n.UnitPreviousYear__c!=null){
                            if(n.UnitPreviousYear__c=='Forfait'){
                                CANminus1Traitement+=n.PricePreviousYear__c;
                            }else{
                                CANminus1Traitement+= (n.PricePreviousYear__c* n.QuantityPreviousYear__c.intValue());
                            }
                        }
                        if(CANminus2Traitement!=null && n.Price2YearsAgo__c!=null && n.Quantity2YearsAgo__c!=null && n.UnitPreviousYear__c!=null){
                            if(n.UnitPreviousYear__c=='Forfait'){
                                CANminus2Traitement+=n.Price2YearsAgo__c;
                            }else{
                                CANminus2Traitement+= (n.Price2YearsAgo__c* n.Quantity2YearsAgo__c.intValue());
                            }
                        }
                    }
                    if((n.PriceNextYear__c!=null && n.PriceNextYear__c!=0) && n.ByPassContract__c!=true){
                        FaceaFace=true;
                        if(n.QuantityCurrentYear__c!=null)  {
                            if(n.UnitCurrentYear__c=='Forfait'){
                                CAN1+=n.PriceNextYear__c;
                            }else{
                                CAN1+= (n.PriceNextYear__c* n.QuantityCurrentYear__c.intValue());
                            }
                        }
                        if((n.PriceCurrentYear__c!=null||n.PriceCurrentYear__c!=0) && n.QuantityCurrentYear__c!=null && n.UnitCurrentYear__c!=null)  {
                            if(n.UnitCurrentYear__c=='Forfait'){
                                CAN+=n.PriceCurrentYear__c;
                            }else{
                                CAN+= (n.PriceCurrentYear__c* n.QuantityCurrentYear__c.intValue());
                            }
                        }
                        if(n.Nature__c=='Traitement'){
                            if(CANTraitement!=null && n.PriceCurrentYear__c!=null && n.QuantityCurrentYear__c!=null && n.UnitCurrentYear__c!=null)  {
                                if(n.UnitCurrentYear__c=='Forfait'){
                                    CANTraitement+=n.PriceCurrentYear__c;
                                }else{
                                    CANTraitement+= (n.PriceCurrentYear__c* n.QuantityCurrentYear__c.intValue());
                                }
                            }
                            if(CANTraitement!=null && n.PriceNextYear__c!=null && n.QuantityCurrentYear__c!=null && n.UnitNextYear__c!=null)  {
                                if(n.UnitNextYear__c=='Forfait'){
                                    CAN1Traitement+=n.PriceNextYear__c;
                                }else{
                                    CAN1Traitement+= (n.PriceNextYear__c* n.QuantityCurrentYear__c.intValue());
                                }
                            }
                        }
                    }else{
                        if(n.QuantityCurrentYear__c!=null && n.PriceCurrentYear__c!=Null)  {
                            if(n.UnitCurrentYear__c=='Forfait'){
                                CANFaF+=n.PriceCurrentYear__c;
                            }else{
                                CANFaF+=n.PriceCurrentYear__c* n.QuantityCurrentYear__c.intValue();
                            }
                        }
                    }
                }
                ContractByAccount CBA=new ContractByAccount();
                CBA.Account = i;
                CBA.TonnageString=FormatNumberUtils.getCorrectNumber(Ton);
                CBA.ChiffreAffaireNString=FormatNumberUtils.getCorrectNumber(CAN);
                CBA.ChiffreAffaireN1String=FormatNumberUtils.getCorrectNumber(CAN1);
                CBA.MontantAugmentation=FormatNumberUtils.getCorrectNumber(CAN1-CAN);
                CBA.Filiales=''; 
                if(StatusPerAcc.size()>1){
                    CBA.Status='Validé partiellement';
                }else{
                    CBA.Status= (new list<string>(StatusPerAcc))[0];
                }  
                for(Amendment__c Amendment:AmendmentsMap.get(i)){
                        if(Amendment.ByPassContract__c && (Amendment.PriceNextYear__c==null || Amendment.PriceNextYear__c==0) ){ 
                            CBA.Augmentation='Augmentation Face à Face';
                            AvenantsFaceaFace++;
                        }else{
                            if(CAN!=0){
                                Decimal Augmentation = (((CAN1-CAN)/CAN)*100).setScale(2);
                                CBA.Augmentation = Augmentation.toPlainString()+'%';
                            }
                        }
                    if(!CBA.Filiales.contains(Amendment.Contrat2__r.Filiale__c)){
                        CBA.Filiales=CBA.Filiales+' '+Amendment.Contrat2__r.Filiale__c;
                    }
                }
                ValorisationEuroVal=ValorisationEuroVal+CAN1-CAN;
                ContractsByAccounts.add(CBA);
                if(CANminus1!=null && CANMinus1User!=null){
                    CANMinus1User=CANMinus1User+CANminus1;
                }
                if(CANminus2!=null && CANMinus2User!=null){
                    CANMinus2User=CANMinus2User+CANminus2;
                }
                if(CAN!=null && CANUser!=null){
                    if(FaceaFace){
                        CANUser=CANUser+CAN;
                    }
                }
                if(CAN1!=null && CAN1User!=null){
                    CAN1User=CAN1User+CAN1;
                }
                if(Ton!=null && TonnageUser!=null){
                    TonnageUser=TonnageUser+Ton;
                }
                if(Ton1!=null && TonnageMinus1User!=null){
                    TonnageMinus1User=TonnageMinus1User+Ton1;
                }
                if(Ton2!=null && TonnageMinus2User!=null){
                    TonnageMinus2User=TonnageMinus2User+Ton2;
                }
                if(CANminus1Traitement!=null && CANMinus1UserTraitement!=null){
                    CANMinus1UserTraitement=CANMinus1UserTraitement+CANminus1Traitement;
                }
                if(CANminus2Traitement!=null && CANMinus2UserTraitement!=null){
                    CANMinus2UserTraitement=CANMinus2UserTraitement+CANminus2Traitement;
                }
                if(CANTraitement!=null && CANUserTraitement!=null){
                    if(FaceaFace){
                        CANUserTraitement=CANUserTraitement+CANTraitement;
                    }
                }
                if(CAN1Traitement!=null && CAN1UserTraitement!=null){
                    CAN1UserTraitement=CAN1UserTraitement+CAN1Traitement;
                }
                if(TonTraitement!=null && TonnageUserTraitement!=null){
                    TonnageUserTraitement=TonnageUserTraitement+TonTraitement;
                }
                if(Ton1Traitement!=null && TonnageMinus1UserTraitement!=null){
                    TonnageMinus1UserTraitement=TonnageMinus1UserTraitement+Ton1Traitement;
                }
                if(Ton2Traitement!=null && TonnageMinus2UserTraitement!=null){
                    TonnageMinus2UserTraitement=TonnageMinus2UserTraitement+Ton2Traitement;
                }
            }
            Integer  mydtstring = Integer.valueof(datetime.now().format('yyyy')); 
            datetime ThisYear = datetime.newInstance(mydtstring , 12, 28); 
            Decimal  WeeksOfYear= Decimal.valueof(ThisYear.format('w'));
            Decimal  WeekOfCreationInt=Decimal.valueof(WeekOfcreation);
            Decimal  Value=WeeksOfYear/WeekOfCreationInt;
            this.CANProjete =FormatNumberUtils.getCorrectNumber(CANUser*Value);
            this.CAN1Projete =FormatNumberUtils.getCorrectNumber(CAN1User*Value);
            this.TonnageProjete =FormatNumberUtils.getCorrectNumber(TonnageUser*Value);
            if(CANUser!=0){
                Decimal percentageAugmentation = (((CAN1User-CANUser)/CANUser)*100).setScale(2);
                this.PourcentageAugmentation =FormatNumberUtils.getCorrectNumber(percentageAugmentation);
            }
            this.CANminus2UserString = FormatNumberUtils.getCorrectNumber(CANMinus2User);
            this.CANminus1UserString = FormatNumberUtils.getCorrectNumber(CANMinus1User);
            this.CANUserString = FormatNumberUtils.getCorrectNumber(CANUser);
            this.CAN1UserString = FormatNumberUtils.getCorrectNumber(CAN1User);
            this.TonnageUserString = FormatNumberUtils.getCorrectNumberInteger(TonnageUser);
            this.Tonnageminus1UserString = FormatNumberUtils.getCorrectNumberInteger(TonnageMinus1User);
            this.Tonnageminus2UserString = FormatNumberUtils.getCorrectNumberInteger(TonnageMinus2User);
            
            if(CANUser!=0){
                Decimal PourcentageAugmentationProjeteVal = (((CAN1User-CANUser)/CANUser)*100).setScale(2);
                this.PourcentageAugmentationProjete =FormatNumberUtils.getCorrectNumber(PourcentageAugmentationProjeteVal);
            }
            this.ValorisationEuro=FormatNumberUtils.getCorrectNumber(ValorisationEuroVal);
            this.ValorisationEuroProjete=FormatNumberUtils.getCorrectNumber(ValorisationEuroVal*Value);
            
            //traitement
            this.CANProjeteTraitement =FormatNumberUtils.getCorrectNumber(CANUserTraitement*Value);
            this.CAN1ProjeteTraitement =FormatNumberUtils.getCorrectNumber(CAN1UserTraitement*Value);
            this.TonnageProjeteTraitement =FormatNumberUtils.getCorrectNumber(TonnageUserTraitement*Value);
            if(CANUserTraitement!=0){
                Decimal percentageAugmentationTraitement = (((CAN1UserTraitement-CANUserTraitement)/CANUserTraitement)*100).setScale(2);
                this.PourcentageAugmentationTraitement =FormatNumberUtils.getCorrectNumber(percentageAugmentationTraitement);
            }
            this.CANminus2UserStringTraitement = FormatNumberUtils.getCorrectNumber(CANMinus2UserTraitement);
            this.CANminus1UserStringTraitement = FormatNumberUtils.getCorrectNumber(CANMinus1UserTraitement);
            this.CANUserStringTraitement = FormatNumberUtils.getCorrectNumber(CANUserTraitement);
            this.CAN1UserStringTraitement = FormatNumberUtils.getCorrectNumber(CAN1UserTraitement);
            this.TonnageUserStringTraitement = FormatNumberUtils.getCorrectNumberInteger(TonnageUserTraitement);
            this.Tonnageminus1UserStringTraitement = FormatNumberUtils.getCorrectNumberInteger(TonnageMinus1UserTraitement);
            this.Tonnageminus2UserStringTraitement = FormatNumberUtils.getCorrectNumberInteger(TonnageMinus2UserTraitement);
            if(CANUserTraitement!=0){
                Decimal PourcentageAugmentationProjeteTraitementVal = (((CAN1UserTraitement-CANUserTraitement)/CANUserTraitement)*100).setScale(2);
                this.PourcentageAugmentationProjeteTraitement =FormatNumberUtils.getCorrectNumber(PourcentageAugmentationProjeteTraitementVal);
            }
            this.ValorisationEuroTraitement=FormatNumberUtils.getCorrectNumber(CAN1UserTraitement-CANUserTraitement);
            this.ValorisationEuroProjeteTraitement=FormatNumberUtils.getCorrectNumber((CAN1UserTraitement-CANUserTraitement)*Value);
            
            AvenantsAugmentes=AvenantsAugmentes - AvenantsFaceaFace;
            this.AvenantsFaceaFace=FormatNumberUtils.getCorrectNumber(AvenantsFaceaFace);
            this.AvenantsAugmentes=FormatNumberUtils.getCorrectNumber(AvenantsAugmentes);	
            this.CANFaceaFace=FormatNumberUtils.getCorrectNumber(CANFaF);
            This.Filiale=' ';
            for(String f:FilialesDuCommercial){
                this.Filiale=this.Filiale+'    '+f;
            }
            
            if(Status.size()>1){
                this.Status='Validé partiellement';
            }else{
                this.Status= (new list<string>(Status))[0]+' ';
            }            
        }
    }
    // redirects to the report with all amendments the user has
    public PageReference redirectToListview(){
        //String url='/'+reportAmendments.Id+'?pv0='+user.Name;
        String url='/'+reportAmendments.Id;
        PageReference pageref = new PageReference(url);
        pageref.setRedirect(True);
        return pageref;
    }
    // redirects to the Dashboard personalized for the user
    public PageReference redirectToIndicators(){
        String userName=user.Name.toUpperCase();
        String developerName='Suivi_Augmentation_Tarifaire_'; //+ userName
        Dashboard Dash=[SELECT Title, DeveloperName, Id FROM Dashboard Where DeveloperName='Suivi_Augmentation_Tarifaire'];//=:developerName
        String url='/'+Dash.Id;
        PageReference pageref = new PageReference(url);
        pageref.setRedirect(True);
        return pageref;
    }
    // Approve the contracts and amendments , by changing the status to valide 
    public PageReference Approuver(){
        this.Contracts = [SELECT Id, Name, AssociatedSalesRep__c, Account__c,RecordLocked__c, toLabel(status__c) FROM Contract__c Where ID IN:this.ContractsID AND status__c='Soumis' Order By Name];
        try{
            for(contract__c c:this.Contracts ){
                c.status__c='Valide';
                System.debug('Contract Valide : '+ c );
            }
            update this.contracts;
            // Sending the email to the user who is associated to the contracts
            String userName = UserInfo.getUserName();
            User activeUser = [Select Email From User where Username = : userName limit 1];
            String userEmail = activeUser.Email;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new String[] {this.user.Email});
            mail.setReplyTo(userEmail);
            mail.setSenderDisplayName('Salesforce Augmentation Tarifaire');
            mail.setSubject('Augmentations tarifaires validées');
            Integer Nextyear = system.today().year() + 1;
            if(FilialeURL!=null && FilialeURL!=' '){
                mail.setHtmlBody('Bonjour '+this.user.Name+
                                 ', <br/><br/>Votre demande d\'augmentations tarifaires '+Nextyear
                                 +' a été validée pour les comptes que vous gérez sur '+FilialeURL+' '+FiliereURL
                                 +'. <br/><br/>Les opportunités et conventions associées seront créées en J+1.'
                                 +'<br/>Vous pourrez ainsi poursuivre le process d’augmentation par l’envoi des conventions.'
                                 +'<br/><br/>Cordialement, <br/><br/>'+UserInfo.getName());
            }else{
                mail.setHtmlBody('Bonjour '+this.user.Name+
                                 ', <br/><br/>Votre demande d\'augmentations tarifaires '+Nextyear
                                 +' a été validée.'
                                 +'. <br/><br/>Les opportunités et conventions associées seront créées en J+1.'
                                 +'<br/>Vous pourrez ainsi poursuivre le process d’augmentation par l’envoi des conventions.'
                                 +'<br/><br/>Cordialement, <br/><br/>'+UserInfo.getName());

            }
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }catch(Exception E){
            System.debug(E);
        }
        update this.contracts;
        // redirect to the commercialDirectorTable that lead us to this page
        String url='/apex/VFP_CommercialDirectorTable?filiale='+FilialeURL+'&filiere='+FiliereURL;
        PageReference pageref = new PageReference(url);
        pageref.setRedirect(True);
        return pageref;
    }
    // refuse the contracts and send them back to be rechanged 
    public PageReference Refuser(){
        Boolean valide=false;
        this.Contracts = [SELECT Id, Name, AssociatedSalesRep__c, Account__c,RecordLocked__c, status__c FROM Contract__c Where ID IN:this.ContractsID AND AssociatedSalesRep__c=:UserId Order By Name];
        for(Contract__c c:this.Contracts){
            if(c.Status__c=='Valide'){
                valide=true;
            }
        }
        if(!valide){
            try{
                for(contract__c c:this.Contracts ){
                    c.status__c='Brouillon';
                    c.RecordLocked__c=false;
                }
                update this.contracts;
                String userName = UserInfo.getUserName();
                User activeUser = [Select Email From User where Username = : userName limit 1];
                String userEmail = activeUser.Email;
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new String[] {this.user.Email});
                mail.setReplyTo(userEmail);
                mail.setSenderDisplayName('Salesforce Augmentation Tarifaire');
                mail.setSubject('Augmentations tarifaires refusées');
                Integer Nextyear = system.today().year() + 1;
                if(FilialeURL!=null && FilialeURL!=''){
                    mail.setHtmlBody('Bonjour '+this.user.Name+
                                     ', <br/><br/>Votre demande d\'augmentations tarifaires '+Nextyear
                                     +' a été rejetée sur '+FilialeURL+' '+FiliereURL
                                     +'. <br/><br/> Veuillez vous rapprocher de l’approbateur concerné pour définir les modifications à apporter.'
                                     +'<br/> Cordialement, <br/><br/>'+UserInfo.getName());
                }else{
                    mail.setHtmlBody('Bonjour '+this.user.Name+
                                     ', <br/><br/>Votre demande d\'augmentations tarifaires '+Nextyear
                                     +' a été rejetée.'
                                     +'. <br/><br/> Veuillez vous rapprocher de l’approbateur concerné pour définir les modifications à apporter.'
                                     +'<br/><br/> Cordialement, <br/><br/>'+UserInfo.getName());
                }
                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }catch(Exception E){
                System.debug(E);
            }
            // redirect to the commercialDirectorTable that lead us to this page
            String url='/apex/VFP_CommercialDirectorTable?filiale='+FilialeURL+'&filiere='+FiliereURL;
            PageReference pageref = new PageReference(url);
            pageref.setRedirect(True);
            return pageref;
        }else{
            PageReference pageref =null;
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Les avenants sont validés et ne peuvent pas être refusés');
            ApexPages.addMessage(errorMessage);
            return pageref;
        }
    }
    // Contract Wrapper for the visualforce Page to show accounts and the details they have
    public class ContractByAccount{
        public String Status{get;set;}
        public String Account{get;set;}
        public String TonnageString{get;set;}
        public String ChiffreAffaireNString{get;set;}
        public String ChiffreAffaireN1String{get;set;}
        public String MontantAugmentation{get;set;}
        public String Augmentation{get;set;}
        public String Filiales{get;set;}
    }
}