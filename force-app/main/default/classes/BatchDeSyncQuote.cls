/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2019-03-13
* @modified       2019-03-18
* @systemLayer    Batch Class         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		 batch class to desync quotes who are approuved or sent for approval , and with opportunities that are not abandonned, won or lost
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global class BatchDeSyncQuote implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Datetime dtTime=System.now();
        Datetime dateBefore=dtTime.addMonths(-1);
        String dateCorrectFormat=dateBefore.format('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        String query = 'SELECT Id,Status From Quote Where (Status=\'Approuvé\' OR Status=\'Soumis pour approbation\') AND isSyncing=true AND LastModifiedDate >= '+dateCorrectFormat;
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<Quote> scope)
    {	
        List<Quote> quotesToDesync=new List<Quote>();
        for(Quote q:scope){
            if(Approval.isLocked(q.Id)){
                quotesToDesync.add(q);
            }
        }
        List<Opportunity> opportunities=[Select Id,Name,SyncedQuoteId FROM Opportunity WHERE SyncedQuoteId IN:quotesToDesync AND 
                                         (StageName!='Clôturée / Gagnée' OR StageName!='Clôturée / Perdue' OR StageName!='Abandonnée')];
        for(Opportunity opp:opportunities){
            opp.SyncedQuoteId=null;
        }
        try{
        	Update opportunities;
        }catch(Exception e){
            system.debug('Failed to update records '+e);
        }
    }
    global void finish(Database.BatchableContext BC)
    {  
    }
}