public with sharing class LeadFormController {
    
    @AuraEnabled
    public String ID {get; set;}
    
    @AuraEnabled
    public String UID {get; set;}
    
    @AuraEnabled
    public String LastName {get; set;}
    
    @AuraEnabled
    public String FirstName {get; set;}
    
    @AuraEnabled
    public String Salutation {get; set;}
    
    @AuraEnabled
    public String Company {get; set;}
    
    @AuraEnabled
    public String Phone {get; set;}
    
    @AuraEnabled
    public String Email {get; set;}

	@AuraEnabled
    public String Website {get; set;}

    @AuraEnabled
    public String CodeNAF {get; set;}

    @AuraEnabled
    public String NSIRET {get; set;}
    
    @AuraEnabled
    public String Street {get; set;}
    
    @AuraEnabled
    public String City {get; set;}
    
    @AuraEnabled
    public String PostalCode {get; set;}
    
    @AuraEnabled
    public String State {get; set;}
    
    @AuraEnabled
    public String Country {get; set;}
    
    @AuraEnabled
    public Boolean isInterrestedOffer {get; set;}
    
    @AuraEnabled
    public Boolean isInterrestedSeche {get; set;}
    
    @AuraEnabled
    public String Tonnage {get; set;}
    
    @AuraEnabled
    public String Frequency {get; set;}
    
    @AuraEnabled
    public String OtherOffer {get; set;}

    
    @AuraEnabled
    public static LeadFormController find_LeadById(String URL){
        //Lead l = new Lead();
        String UID ='';
        String ID ='';
        Lead l = new Lead();
        
        try{
            String[] parameters = URL.split('\\?');
            String[] parameter = parameters[1].split('&');
            if(parameter[0].contains('UID') && parameter[1].contains('ID')){
                String[] temp = parameter[0].split('=');
                UID = temp[1];
                temp = parameter[1].split('=');
                ID = temp[1];
            }else if(parameter[0].contains('ID') && parameter[1].contains('UID')){
                String[] temp = parameter[1].split('=');
                UID = temp[1];
                temp = parameter[0].split('=');
                ID = temp[1];
            }
            
        }catch(Exception e){
            return new LeadFormController();
        }
        String token = getTokenAccess();
        if(token != 'ERROR'){
             l = getCalloutResponseContents(ID, UID, token);
        }else{
            return new LeadFormController();
        }
        
        if(l.TECH_UID__c == UID){
            
            LeadFormController cl =new LeadFormController();
            cl.ID = ID;
            cl.Firstname = l.FirstName;
            cl.Lastname = l.Lastname;
            cl.Salutation = l.Salutation;
            cl.Company = l.Company;
            cl.phone = l.Phone;
            cl.Email = l.Email;
            cl.Website = l.Website;
            cl.CodeNAF = l.Code_NAF__c;
            cl.NSIRET = l.N_SIRET__c;
            cl.Street = l.Street;
            cl.City = l.City;
            cl.PostalCode = l.PostalCode;
            cl.State = l.State;
            cl.Country = l.Country;
            cl.isInterrestedOffer = l.IsInterested__c;
            cl.isInterrestedSeche = l.IsInterestedBySeche__c;
            cl.Tonnage = l.Tonnage__c;
            cl.Frequency = l.GatheringFrequency__c;
            cl.OtherOffer = l.InterestedOtherServices__c;
            cl.UID = UID;
            
            return cl;
            
        }else{
            return new LeadFormController();
        }
        
        return new LeadFormController();
        
    }
    
    public static String getTokenAccess() {
        System.debug('..................GenerateToken');
        String endPointURL = Label.ORG_URL_LOGIN+'/services/oauth2/token';
        String tokenSF = '';
        Boolean found = false;
        String username = Label.USR_API_Portal;
        String password = Label.USR_API_Password;
        String clientID = Label.API_ClientID;
        String clientSecret = Label.API_Client_Secret ;
        
        // Instantiate a new http object
        Http h = new Http();
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint(endPointURL+'?grant_type=password&client_id='+clientID+'&client_secret='+clientSecret+'&username='+username+'&password='+password);
        req.setMethod('POST');
        // Send the request, and return a response
        HttpResponse res = h.send(req);
        if(res.getStatusCode() == 200){
            //Parse JSON to retrive Token
            System.JSONToken token;
            string text;
            JSONParser parser = JSON.createParser(res.getBody());
            System.debug(res.getBody());
            parser.nextToken(); // Eat first START_OBJECT {
            while((token = parser.nextToken()) != null && !found) {
                text = parser.getText();
                if (token == JSONToken.FIELD_Name && text == 'access_token') {
                    token=parser.nextToken();
                    tokenSF = parser.getText();
                    found = true;
                }
            }
            
        }
        
        if(tokenSF != ''){
            return tokenSF;   
        }else{
            return 'ERROR';
        }
        
    }
    
    public static Lead getCalloutResponseContents(String ID, String UID, String access_token){
        
        String endPointURL = Label.ORG_URL+'/services/data/v20.0/sobjects/LEAD/'+ID+'?fields=TECH_UID__c,Lastname,Firstname,Salutation,Company,Phone,Email,Website,Code_NAF__c,N_SIRET__c,Street,City,PostalCode,State,Country,IsInterestedBySeche__c,IsInterested__c,Tonnage__c,GatheringFrequency__c,InterestedOtherServices__c';
        Http h = new Http();
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint(endPointURL);
        req.setMethod('GET');
        req.setHeader('Authorization','Bearer '+access_token);
        HttpResponse res = h.send(req);
        System.debug(res.getBody());
        return (Lead) System.JSON.deserialize(res.getBody(), LEAD.class);
    }
    
    @AuraEnabled
    public static String saveContents(String lfcJson) {
        String token_access = getTokenAccess();
        
        JSONParser parser = JSON.createParser(lfcJson);
		LeadFormController lfc = (LeadFormController)parser.readValueAs(LeadFormController.class);
		lfc = checkNullValue(lfc);
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('Company', lfc.Company);
        gen.writeStringField('FirstName', lfc.Firstname);
        gen.writeStringField('Lastname', lfc.Lastname);
        gen.writeStringField('Phone', lfc.phone);
        gen.writeStringField('Email', lfc.Email);
        gen.writeStringField('Website', lfc.Website);
        gen.writeStringField('Code_NAF__c', lfc.CodeNAF);
        gen.writeStringField('N_SIRET__c', lfc.NSIRET);
        gen.writeStringField('Street', lfc.Street);
        gen.writeStringField('City', lfc.City);
        gen.writeStringField('PostalCode', lfc.PostalCode);
        gen.writeStringField('State', lfc.State);
        gen.writeStringField('Country', lfc.Country);
        if(lfc.isInterrestedOffer != null){ gen.writeBooleanField('IsInterested__c', lfc.isInterrestedOffer); }      
        if(lfc.isInterrestedSeche != null){ gen.writeBooleanField('IsInterestedBySeche__c', lfc.isInterrestedSeche); }      
        if(lfc.Tonnage != null){ gen.writeStringField('Tonnage__c', lfc.Tonnage); }      
        if(lfc.Frequency != null){ gen.writeStringField('GatheringFrequency__c', lfc.Frequency); }      
        if(lfc.OtherOffer != null){ gen.writeStringField('InterestedOtherServices__c', lfc.OtherOffer); }  
        gen.writeBooleanField('TECH_isFiledByCustomer__c', true);
      
        gen.writeEndObject();              
        String JSONString = gen.getAsString();
        
        Lead leadWithUID = getCalloutResponseContents(lfc.ID, lfc.UID, token_access);
        
        if(leadWithUID != null){
            String endPointURL = Label.ORG_URL+'/services/data/v39.0/sobjects/LEAD/'+lfc.ID+'?_HttpMethod=PATCH';
            Http h = new Http();
            // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
            HttpRequest req = new HttpRequest();
            
            req.setEndpoint(endPointURL);
            req.setMethod('POST');
            req.setHeader('Authorization','Bearer '+token_access);
            req.setHeader('Content-Type', 'application/json');
            req.setEndpoint(endPointURL);
            req.setBody(JSONString);
            
            HttpResponse res = h.send(req);
            System.debug(res.getStatus());
            return 'SUCCESS';
            
            
        }else{
            return 'ERROR';
        }
    }
    
    public static LeadFormController checkNullValue(LeadFormController l){
        	
        if(l.FirstName == null)
            l.FirstName = '';
        
        if(l.Lastname == null)
            l.Lastname = '';
        
        if(l.Salutation == null)
            l.Salutation = '';
        
        if(l.Company == null)
            l.Company = '';
        
        if(l.Phone == null)
            l.Phone = '';
        
        if(l.Email == null)
            l.Email = '';
        
        if(l.Website == null)
            l.Website = '';
        
        if(l.CodeNAF == null)
            l.CodeNAF = '';
        
        if(l.NSIRET == null)
            l.NSIRET = '';
        
        if(l.Street == null)
            l.Street = '';
        
        if(l.City == null)
            l.City = '';
        
        if(l.PostalCode == null)
            l.PostalCode = '';
        
        if(l.State == null)
            l.State = '';
        
        if(l.Country == null)
            l.Country = '';
        
        if(l.Tonnage == null)
            l.Tonnage = '';
        
        if(l.Frequency == null)
            l.Frequency = '';
        
        if(l.OtherOffer == null)
            l.OtherOffer = '';
    
		return l;
        
    }
}