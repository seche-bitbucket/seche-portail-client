/*
*/
global without sharing class AP_PreFipManager {
    
    @AuraEnabled
    public static List<Producer_Certificate_Questions__mdt> fetchQuestions(String accountNature){
        try {
            Boolean type_public = (accountNature == 'Collectivité et administration') ? true : false;
            return [SELECT DescriptionPlaceholder__c, Question__c, isLastQuestion__c FROM Producer_Certificate_Questions__mdt WHERE IsPublicService__c = :type_public];  
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage()+' '+ex.getLineNumber());
        }
    }

    // Depreciated
    @AuraEnabled
    public static List<Attestation__c> fetchLinkedAttestation(List<Id> oppsIds){
        try {
            return [SELECT Id, OpportunityLineItem__c, Name, TECH_AddedFiles__c, OwnerId, Contact__c FROM Attestation__c WHERE OpportunityLineItem__c IN :oppsIds AND Status__c = 'Brouillon' ORDER BY CreatedDate ASC];  
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage()+' '+ex.getLineNumber());
        }
    }

    @AuraEnabled
    public static List<Attestation__c> fetchLinkedAttestationsFromForm(List<Id> formsIds){
        try {
            return [SELECT Id, OpportunityLineItem__c, Account__c, Name, TECH_AddedFiles__c, OwnerId, Contact__c, Form__c, OperationalAgency__c, Producer__c, ProducerAddress__c, ProducerCode__c, ProducerCorp__c, ProducerEmail__c, ProducerFirstName__c, ProducerLastName__c FROM Attestation__c WHERE Form__c IN :formsIds AND Status__c = 'Brouillon' ORDER BY CreatedDate ASC];  
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage()+' '+ex.getLineNumber());
        }
    }

    @AuraEnabled
    public static Boolean validateSign(String oppsJson, String featuresJson, String producerJson, String loadingAddress, String attestationsJson){
        try {
            
            List<Feature__c> features = (List<Feature__c>)JSON.deserialize(featuresJson, List<Feature__c>.class);
            List<Attestation__c> attestations = (List<Attestation__c>)JSON.deserialize(attestationsJson, List<Attestation__c>.class);

            System.debug(JSON.serialize(attestations));

            Set<Id> attIds = new Set<Id>();
            for(Feature__c f: features) {
                attIds.add(f.Attestation__c);
            }
            delete [SELECT Id FROM Feature__c WHERE Attestation__c IN :attIds];

            List<OpportunityLineItem> opps = (List<OpportunityLineItem>)JSON.deserialize(oppsJson, List<OpportunityLineItem>.class);
            Map<String, String> attestationsExemptionReason = new Map<String, String>();
            for(OpportunityLineItem opp: opps) {
                attestationsExemptionReason.put(opp.Id, opp.ExemptionReason__c);                
            }
            update opps;
            upsert features;

            // update new Opportunity(
            //     Id = opps[0].OpportunityId,
            //     GUID__c = null
            // );

            Form__c form = new Form__c(ID = attestations[0].Form__c);
            form.GUID__c = null;
            
            // update new Form__c(
                //     Id = attestations[0].Form__c,
                //     GUID__c = null
                // );
                
                Map<String, Object> producer;
                if(producerJson != null) {
                    producer = (Map<String, Object>)JSON.deserializeUntyped(producerJson);
                }
                
                System.debug(producer);
                
                //List<Attestation__c> lstAtt = [SELECT Id, Contact__r.FirstName, Contact__r.LastName, Contact__r.Email, Contact__r.TECH_AccountAddress__c, Contact__r.TECH_AccountName__c, Contact__c, Status__c, ProducerAddress__c, ProducerEmail__c, ProducerFirstName__c, ProducerLastName__c, ProducerCorp__c FROM Attestation__c WHERE OpportunityLineItem__c IN :opps AND Status__c = 'Brouillon' ];
            
            if(producer != null) {
                for(Attestation__c att: attestations) {
                    att.ProducerAddress__c =  (producer.containsKey('Address')) ? (String)producer.get('Address') : '';
                    att.ProducerEmail__c = (producer.containsKey('Email')) ? (String)producer.get('Email') : '';
                    att.ProducerFirstName__c = (producer.containsKey('FirstName')) ? (String)producer.get('FirstName') : '';
                    att.ProducerLastName__c = (producer.containsKey('LastName')) ? (String)producer.get('LastName')  : '';
                    att.ProducerCorp__c = (producer.containsKey('Corp')) ? (String)producer.get('Corp') : (producer.containsKey('LastName')) ? (String)producer.get('LastName') : '';
                    att.OperationalAgency__c = (producer.containsKey('OperationalAgency')) ?(String)producer.get('OperationalAgency') : '' ;
                }
                form.ProducerAddress__c =  (producer.containsKey('Address')) ? (String)producer.get('Address') : '';
                form.ProducerEmail__c = (producer.containsKey('Email')) ? (String)producer.get('Email') : '';
                form.ProducerFirstName__c = (producer.containsKey('FirstName')) ? (String)producer.get('FirstName') : '';
                form.ProducerLastName__c = (producer.containsKey('LastName')) ? (String)producer.get('LastName')  : '';
                form.ProducerCorp__c = (producer.containsKey('Corp')) ? (String)producer.get('Corp') : (producer.containsKey('LastName')) ? (String)producer.get('LastName') : '';
                form.OperationalAgency__c = (producer.containsKey('OperationalAgency')) ?(String)producer.get('OperationalAgency') : '' ;
            }

            
            for(Attestation__c att: attestations) {
                att.LoadingAddress__c = loadingAddress;
                att.TECH_CongaAttestationToGenerate_PreFip__c = true;
                att.ExemptionReason__c = attestationsExemptionReason.get(att.OpportunityLineItem__c);
            }
            update attestations;
            update form;
            
            return true;
        } catch (Exception ex) {
            System.debug(ex.getMessage()+' '+ex.getLineNumber());
            return false;
        }
    }

    global class AttestationsList {
        @InvocableVariable
        global List<Attestation__c> atts;
    }
    
    @InvocableMethod(label='Send Packages' description='Send document for signature' category='Pré-Fip')
    public static void createPackages(List<AttestationsList> request) {
        System.enqueueJob(new Batch_MassSendForSignature(request[0].atts, null));
    }

}