global class NotifySharedOpportunitySchedule implements Schedulable {
    
    global void execute(SchedulableContext sc) {
        
        NotifySharedOpportunity opp = new NotifySharedOpportunity();        
        
        Database.executebatch(opp);
    }
}