/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-07-17
* @modified       2018-07-17
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for Amendments Submitted For Validation Controller 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class Test_VFC_AMESubmitForValidation {
    public static PageReference myVfPage {get;set;}
    public static ApexPages.StandardSetController stc {get;set;}
    public static VFC_AMESubmitForValidation vfc{get;set;}  
    @isTest static void TestSubmit(){   
        PageReference ref = new PageReference('/001/o');
        Test.setCurrentPage(ref);
        ApexPages.currentPage().getParameters().put('retURL', '/001/o');
        Profile p = [select id from profile where name = :'Responsable commercial' limit 1];
        String testemail2 = 'Commercial-_User_test@test.com';
        User user = new User(profileId = p.id, username = testemail2, email = testemail2,
                             emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                             languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                             alias='cspu', lastname='Commercial', IsActive=true,firstName='CA');
        insert user;
        Profile directp = [select id from profile where name = :'Directeur commercial' limit 1];
        
        String testemailDirecteur = 'director-_User_test@test.com';
        
        User director = new User(profileId = directp.id, username = testemailDirecteur, email = testemailDirecteur,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        insert director;
        String testemailDirecteur2 = 'director2-_User_test@test.com';
        
        User director2 = new User(profileId = directp.id, username = testemailDirecteur2, email = testemailDirecteur2,
                                  emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                  languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                  alias='cspu', lastname='lastname', IsActive=true);
        insert director2;        
        Account ac = new Account();
        ac.Name = 'testAccName';
        ac.CustomerNature__c = 'Administration';
        ac.Industry = 'Trader';
        ac.CurrencyIsoCode = 'EUR';
        ac.Producteur__c = false;
        ac.BillingPostalCode = '00000';
        ac.NAF_Number__c = '1234A';
        insert ac;
        Account ac2 = new Account();
        ac2.Name = 'testAccName2';
        ac2.CustomerNature__c = 'Administration';
        ac2.Industry = 'Auto';
        ac2.CurrencyIsoCode = 'EUR';
        ac2.Producteur__c = false;
        ac2.BillingPostalCode = '00000';
        ac2.NAF_Number__c = '1234A';
        insert ac2;
        Account ac3 = new Account();
        ac3.Name = 'testAccName3';
        ac3.CustomerNature__c = 'Administration';
        ac3.Industry = 'Medical';
        ac3.CurrencyIsoCode = 'EUR';
        ac3.Producteur__c = false;
        ac3.BillingPostalCode = '00000';
        ac3.NAF_Number__c = '1234A';
        insert ac3;
        List<Contract__c> contractsToInsert=new List<Contract__c>();
        for(integer i=0;i<50;i++){
            Contract__c c = new Contract__c();
            c.Account__c=ac.id;
            c.AssociatedSalesRep__c=user.Id;
            c.RegroupmentIndex__c=4;
            c.Filiale__c='Tredi Salaise 1';
            c.Type_de_convention__c='INC avec transport';
            c.name='Contract Test'+i;
            c.Status__c='Brouillon';
            contractsToInsert.add(c);
        }
        for(integer i=0;i<150;i++){
            Contract__c c = new Contract__c();
            c.Account__c=ac2.id;
            c.AssociatedSalesRep__c=user.Id;
            c.RegroupmentIndex__c=2;
            c.Filiale__c='Sotrefi';
            c.name='Contract Test 2'+i;
            c.Status__c='Brouillon';
            contractsToInsert.add(c);
        }
        for(integer i=0;i<150;i++){
            Contract__c c = new Contract__c();
            c.Account__c=ac3.id;
            c.AssociatedSalesRep__c=user.Id;
            c.RegroupmentIndex__c=5;
            c.Filiale__c='Triadis Services Etampes';
            c.Type_de_convention__c='PF avec transport';
            c.name='Contract Test 2'+i;
            c.Status__c='Brouillon';
            contractsToInsert.add(c);
        }
        for(integer i=0;i<50;i++){
            Contract__c c = new Contract__c();
            c.Account__c=ac2.id;
            c.AssociatedSalesRep__c=user.Id;
            c.Filiale__c='Tredi Salaise 3';
            c.name='Contract Test 2'+i;
            c.Status__c='Brouillon';
            contractsToInsert.add(c);
        }
        for(integer i=0;i<150;i++){
            Contract__c c = new Contract__c();
            c.Account__c=ac3.id;
            c.AssociatedSalesRep__c=user.Id;
            c.RegroupmentIndex__c=5;
            c.Filiale__c='Triadis Services Etampes';
            c.Type_de_convention__c='PF avec transport';
            c.name='Contract Test 2'+i;
            c.Status__c='Brouillon';
            contractsToInsert.add(c);
        }
        System.RunAs(user) {
            insert contractsToInsert;
            List<Amendment__c> Amendments = new List<Amendment__c>();
            Integer i=1;
            for(Contract__c contract:contractsToInsert){
                Amendment__c am = new Amendment__c();
                am.Contrat2__c=contract.id;
                am.WasteLabel__c='conditionnement amiante';
                if(contract.Filiale__c!='Triadis Services Etampes'){
                    am.PriceNextYear__c=1*i;
                }else{
                    am.PriceNextYear__c=0;
                    am.ByPassContract__c=false;
                    am.PriceCurrentYear__c=30; 
                } 
                am.ByPassContract__c=false;
                Amendment__c am2 = new Amendment__c();
                am2.Contrat2__c=contract.id;
                am2.WasteLabel__c='Transport amiante';
                am2.PriceNextYear__c=1*i;
                am2.PriceCurrentYear__c=1; 
                am2.ByPassContract__c=false;
                Amendment__c am3 = new Amendment__c();
                am3.Contrat2__c=contract.id;
                am3.WasteLabel__c='traitement amiante';
                am3.PriceNextYear__c=1*i;
                am3.PriceCurrentYear__c=1; 
                am3.ByPassContract__c=false;
                Amendments.add(am);
                Amendments.add(am2);
                Amendments.add(am3);
                i++;
            }        
            insert Amendments;
        }
        System.RunAs(director) {
            List<Amendment__c> Amendments=new List<Amendment__c>();
            stc=new Apexpages.standardSetController(Amendments);
            vfc = new VFC_AMESubmitForValidation(stc);
            System.assertEquals('Votre utilisateur n\'est associé à aucun contrat ou n\'est pas autorisé à soumettre pour validation', ApexPages.getMessages()[0].getSummary());
            System.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages()[0].getSeverity());
        }
        System.RunAs(user) {
            List<Amendment__c> AmendmentsInserted=new List<Amendment__c>();
            AmendmentsInserted=[SELECT Id,Name FROM Amendment__c];   
            stc=new Apexpages.standardSetController(AmendmentsInserted);
            vfc = new VFC_AMESubmitForValidation(stc);
            vfc.Submit();
            VFC_AMESubmitForValidation.UserWrapper uw=new VFC_AMESubmitForValidation.UserWrapper(director);
            uw.isSelected=true;
            vfc.DirectorWrapper.add(uw);
            vfc.Submit();
            vfc = new VFC_AMESubmitForValidation(stc);
            VFC_AMESubmitForValidation.UserWrapper uw2=new VFC_AMESubmitForValidation.UserWrapper(director2);
            uw2.isSelected=true;
            vfc.DirectorWrapper.add(uw2);
            VFC_AMESubmitForValidation.UserWrapper uw3=new VFC_AMESubmitForValidation.UserWrapper(director);
            uw3.isSelected=true;
            vfc.DirectorWrapper.add(uw3);
            vfc.Submit();
            vfc = new VFC_AMESubmitForValidation(stc);
            vfc.cancel();
            List<Contract__c> Contracts=[SELECT Id,Name,Status__c,Filiale__c,AssociatedSalesRep__c,AssociatedSalesRep__r.Name FROM Contract__c WHERE Status__c=:'Soumis'];
            System.assertEquals(50, Contracts.size());
        }
        List<Contract__c> contractsToInsert2=new List<Contract__c>();
        for(Contract__c c:contractsToInsert){
            if(c.Filiale__c=='Tredi Salaise 3'){
                c.Status__c='Valide';
            }else{
                contractsToInsert2.add(c);
            }
        }
        Update ContractstoInsert;
        delete contractsToInsert2;
        System.RunAs(user) {
            List<Amendment__c> AmendmentsInserted=new List<Amendment__c>();
            AmendmentsInserted=[SELECT Id,Name FROM Amendment__c];   
            stc=new Apexpages.standardSetController(AmendmentsInserted);
            vfc = new VFC_AMESubmitForValidation(stc);
        }
    }
}