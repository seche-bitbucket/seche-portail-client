/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ConnectiveAsyncUpdateStatus
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ConnectiveAsyncUpdateStatus
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 27-05-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
public class ConnectiveAsyncUpdateStatus implements Queueable, Database.AllowsCallouts {
    
    //create set so there won't be double ids
    public Set<Package__c> pkgsToUpdate = new Set<Package__c>();
    public Set<DocumentsForSignature__c> docsToUpdate = new Set<DocumentsForSignature__c>();
    public Map<Id,Quote> quoteToUpdate = new Map<Id,Quote>();
    public List<DocumentsForSignature__c> scope = new List<DocumentsForSignature__c>();
    //Nous avions voulu gérer une limite (limitCallGetBatch50Package) mais ce n'est pas nécessaire
    //et cela entraînait une erreur "Too many queueable jobs added to the queue: 2" car il y avait 2 batchs appelé dans le Finally
    //public List<DocumentsForSignature__c> scopeToFinish = new List<DocumentsForSignature__c>();
    public List<DocumentsForSignature__c> scopeDocToRetrieved = new List<DocumentsForSignature__c>();
    public List<DocumentsForSignature__c> scopeDocToUpdateInfo = new List<DocumentsForSignature__c>();
    public PackageResponse.ResponseGetListPackage listPkgInit = new PackageResponse.ResponseGetListPackage();
    public Integer limitCallGetBatch50Package = 10;
    public Integer callGetBatch50Package = 1;

    public List<Id> quoteToSend = new List<Id>(); //list of signed quote to send a mail

    public ConnectiveAsyncUpdateStatus(List<DocumentsForSignature__c> scope,PackageResponse.ResponseGetListPackage listPkgInit){
        this.scope=scope;
        this.listPkgInit=listPkgInit;
    }

    public void execute(QueueableContext context) {
        PackageResponse.ResponseGetListPackage listPkg;
        if(this.listPkgInit == null){
            listPkg = ConnectiveApiManager.getBatch50Package(true,null,null);
        }
        else{
            listPkg=listPkgInit;
        }
        try{
            recursiveMethod(scope,listPkg);

            //create lists from the sets to update
            List<Package__c> packToUpdLs = new List<Package__c>();
            packToUpdLs.addAll(pkgsToUpdate);
            List<DocumentsForSignature__c> docToUpdLs = new List<DocumentsForSignature__c>();
            docToUpdLs.addAll(docsToUpdate);

            update packToUpdLs;
            update docToUpdLs;
            update quoteToUpdate.values();

            if (quoteToSend.size() > 0){
                //prepare and send the emails
                List<Messaging.SingleEmailMessage> msgLs = new List<Messaging.SingleEmailMessage>();
                List<Quote> quoteLs = [SELECT Id,Name,NameCustomer__c,Opportunity.Salesman__r.Email,
                                            Opportunity.Salesman__r.Assistant__r.Email,Quote_Type__c,QuoteNumber 
                                        FROM Quote WHERE Id in :quoteToSend];
                for (Quote qt : quoteLs){
                    Messaging.SingleEmailMessage msg = ConnectiveApiManager.generateContentEmail(qt,'Signé',new DocumentsForSignature__c());
                    msgLs.add(msg);   
                }
                Messaging.sendEmail(msgLs);
            }
        }
        catch (Exception e){
            System.debug(LoggingLevel.ERROR, e.getMessage());
        }
        finally{
            if(!Test.isRunningTest()){
                ID jobDoc = System.enqueueJob(new Batch_Connective_Get_Document(scopeDocToRetrieved,scopeDocToUpdateInfo));
            }
            // https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_queueing_jobs.htm
           /*if(!Test.isRunningTest() && scopeToFinish.size()>0){
                listPkg = ConnectiveApiManager.getBatch50Package(false,Integer.valueOf(listPkg.ContinuationToken),listPkg.Total);
                ID jobID = System.enqueueJob(new ConnectiveAsyncUpdateStatus(scopeToFinish,listPkg));
            }*/
        }
    }

    public void recursiveMethod(List<DocumentsForSignature__c> docsProcessed,PackageResponse.ResponseGetListPackage listPkg){
        Map<String,PackageResponse.ResponseItemPackage> mapPkgConnProcessed = generateMapIdPackageStatus(listPkg);
        List<DocumentsForSignature__c> docToFind = new List<DocumentsForSignature__c>();
        Integer maxQuantity = 50;
        Integer limitContinuationToken = Integer.valueOf(Math.round(listPkg.Total/maxQuantity)+1);

        for(DocumentsForSignature__c doc : docsProcessed){
            System.debug('doc.Id : '+doc.Id);
            System.debug('doc.Package__r.ConnectivePackageId__c '+doc.Package__r.ConnectivePackageId__c);
            System.debug('mapPkgConnProcessed.getKeys() : '+mapPkgConnProcessed.keySet());
            System.debug('mapPkgConnProcessed.getValues() : '+mapPkgConnProcessed.values());
            System.debug('mapPkgConnProcessed.get(doc.Package__r.ConnectivePackageId__c) : '+mapPkgConnProcessed.get(doc.Package__r.ConnectivePackageId__c));
            if(mapPkgConnProcessed.containsKey(doc.Package__r.ConnectivePackageId__c)){
                String status =mapPkgConnProcessed.get(doc.Package__r.ConnectivePackageId__c).PackageStatus;
                DocumentsForSignature__c docToUpdate = new DocumentsForSignature__c(
                    Id=doc.Id,
                    Status__c=status);
                Package__c pkgToUpdate = new Package__c(
                    Id=doc.Package__c,
                    ConnectivePackageStatus__c=status);
                    
                this.docsToUpdate.add(docToUpdate);
                this.pkgsToUpdate.add(pkgToUpdate);

                String qStatus = '';
                if(doc.Quote__c!=null && status!='Pending'){
                    Boolean qESigned = false;
                    if(status=='Finished'){
                        qStatus ='Signé';
                        qESigned = true;
                    }
                    else if(status=='Rejected'){
                        qStatus='Refusé';
                    }
                    Quote quote = new Quote(
                        Id=doc.Quote__c,
                        Status=qStatus,
                        ESigned__c=qESigned);
                    if(!quoteToUpdate.containsKey(quote.Id)){
                        this.quoteToUpdate.put(quote.Id,quote);
                    }
                }

                if(status == 'Finished'){
                    scopeDocToRetrieved.add(docToUpdate);
                    //prepare the list of quote to send a mail
                    if (!quoteToSend.contains(doc.Quote__c)){
                        quoteToSend.add(doc.Quote__c);
                    }
                }
                if(status == 'Rejected'){
                    scopeDocToUpdateInfo.add(docToUpdate); //Get rejected reason
                }
            }
            else{
                docToFind.add(doc);
            }
        }
        if(docToFind.size()>0 && this.callGetBatch50Package < this.limitCallGetBatch50Package && Integer.valueOf(listPkg.ContinuationToken) <= limitContinuationToken){
            PackageResponse.ResponseGetListPackage newListPkg = ConnectiveApiManager.getBatch50Package(false,Integer.valueOf(listPkg.ContinuationToken),listPkg.Total);
            this.callGetBatch50Package++;
            recursiveMethod(docToFind,newListPkg);
        }
        /*else{
            this.scopeToFinish = docToFind;
        }*/
    }

    public Map<String,PackageResponse.ResponseItemPackage> generateMapIdPackageStatus(PackageResponse.ResponseGetListPackage listPkg){
        Map<String,PackageResponse.ResponseItemPackage> mapPkgIdStatus = new Map<String,PackageResponse.ResponseItemPackage>();
        for(PackageResponse.ResponseItemPackage item : listPkg.Items){
            mapPkgIdStatus.put(item.PackageId,item);
        }
        return mapPkgIdStatus;
    }
}