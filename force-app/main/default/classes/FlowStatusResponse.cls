public class FlowStatusResponse {
    public String DownloadURL{get;set;}
    public String ExternalReference{get;set;}
    public String Status{get;set;}
    public List<Signer> Signers{get;set;}
    
    public class Signer{
        public String SignerId{get;set;}
        public String ExternalReference{get;set;}
        public String EmailAddress{get;set;}
        public String Url{get;set;}
        public String Status{get;set;}
        public String RejectReason{get;set;}
        public String SignatureTimestamp{get;set;}
        public String SignatureSigner{get;set;}
        public Signer(){}
        
    }
    public class ErrorLog{}
    public static FlowStatusResponse deserializejson(String json){
        return (FlowStatusResponse) System.JSON.deserialize(json, FlowStatusResponse.class);
    }
}