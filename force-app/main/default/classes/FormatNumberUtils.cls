public class FormatNumberUtils {
    
    public static String getCorrectNumber(Decimal d){
        List<String> args = new String[]
        {'0','number','<span>### ### ###.###</span>'};
            String ds = String.format(d.format(), args);
        return ds;
    }
    public static String getCorrectNumberInteger(Integer d){
        List<String> args = new String[]
        {'0','number','<span>### ### ###</span>'};
            String ds = String.format(d.format(), args);
        return ds;
    }
}