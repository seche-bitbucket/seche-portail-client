/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : LogBatchApexErrorEvent_HandlerTest
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : Test class for LogBatchApexErrorEvent trigger and LogBatchApexErrorEvent_Handler
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 27-02-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
@isTest
public with sharing class LogBatchApexErrorEvent_HandlerTest {

    static testMethod void testBatchApexErrorEvent() {    
        insert new Account(Name = 'Test Account');
        try{
             Test.startTest();
             TestBatch tb = new TestBatch();
             Database.executeBatch(tb); 
             Test.getEventBus().deliver();
             Test.stopTest();
         } catch(System.MathException e){}
         Test.getEventBus().deliver(); 
     
        // System.assertEquals(1, [SELECT Id FROM Log__c].size());
    }
}