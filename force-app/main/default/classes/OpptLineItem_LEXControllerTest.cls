@isTest
public with sharing class OpptLineItem_LEXControllerTest {
   static testMethod void initTest() {

    List<Opportunity> listOpp = new List<Opportunity>();

	String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

	//Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;

	Account ac = new Account();
	ac.Name = 'testAccName';
	ac.CustomerNature__c = 'Administration';
	ac.Industry = 'Trader';
	ac.CurrencyIsoCode = 'EUR';
	ac.Producteur__c = false;
	ac.BillingPostalCode = '00000';
	ac.NAF_Number__c = '1234A';
	insert ac;

	Contact contactTest = new Contact();
	contactTest.LastName = 'testName';
	contactTest.AccountId = ac.Id;
	contactTest.Salesman__c = director.Id;
	contactTest.Email = 'aaa@yopmail.com';
	//contactTest.Phone = '0000000000';
	insert contactTest;


	Pricebook2 pb = new Pricebook2(Name='PricebookFille', isActive=true);
	insert pb;

	Product2 prd = new product2(name = 'Test',TECH_ExternalID__c = '012345',Family = 'Traitement');
	insert prd;

	PricebookEntry pbeMere = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=Test.getStandardPricebookId(),
	                                            isActive=true);
	insert pbeMere;

	PricebookEntry pbe = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=pb.Id,
	                                        isActive=true,usestandardprice = true);
	insert pbe;
	//Get a Record Type for Opportunity
	RecordType rt = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_OpportuniteMere' LIMIT 1];

	//Create Opportunity Mother
	Opportunity opport = new Opportunity(Name = 'OPPtest', RecordTypeId = rt.Id, AccountID= ac.Id, ContactName__c = contactTest.Id, Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', PriceBook2Id = pb.Id);
    Opportunity opp = new Opportunity(Name = 'OPPtest', RecordTypeId = rt.Id, AccountID= ac.Id, ContactName__c = contactTest.Id, Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
	insert opport;

	System.RunAs(director) {
		PageReference pageRef = Page.VFP_SelectPriceBook;
		Test.setCurrentPage(pageRef);
		pageRef.getParameters().put('id',opport.Id);
        listOpp.add(opport);
        listOpp.add(opp);
		ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(listOpp);
		OpptLineItem_LEXController myController = new OpptLineItem_LEXController(ssc);
		myController.init();

		
		insert opp;
        pageRef.getParameters().put('id',opp.Id);
		ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOpp);
		OpptLineItem_LEXController myContr = new OpptLineItem_LEXController(sc);
		myContr.init();
	}
}

}