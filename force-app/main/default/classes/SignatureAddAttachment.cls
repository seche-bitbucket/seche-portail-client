// this class is to be executed once a day after we retrieve status of all signatures to add the signed documents to salesforce related list attachment
global class SignatureAddAttachment implements Schedulable{

    global void execute(SchedulableContext sc)
    {
       addAttachmentToSignedDoc();
        
    }  
    global static void addAttachmentToSignedDoc(){
        List<DocumentsForSignature__c> docs=[SELECT Id, Name, Status__c, SentTo__c, TECH_ExternalId__c, FlowId__c, Document__c, CreatedById FROM DocumentsForSignature__c];
        for(DocumentsForSignature__c docum: docs){
            if(docum.Status__c=='SIGNED'){
                String DocName= ConnectiveCallout.retrieveName(docum.TECH_ExternalId__c);
                // this part adds the document as an attachment in the quote
                List<Attachment> atts = [SELECT id,Name FROM Attachment WHERE ParentId = : docum.TECH_ExternalId__c ORDER BY CreatedDate];
                Boolean temp=false;
                for(Attachment at: atts){
                    if(at.Name==DocName+' -  Signée.pdf'){
                        temp=true;
                    }
                }
                If(temp==false){
                    PageReference pageRef = new PageReference(docum.Document__c);
                    Blob ret;
                    if(Test.isRunningTest()){
                        ret = blob.valueOf('Unit.Test');
                    }else{
                        ret = pageRef.getContentAsPDF(); 
                    }
                    Attachment Att=new Attachment();
                    att.Body = ret; 
                    att.Name = DocName+' -  Signée.pdf' ;	
                    att.ParentId = docum.TECH_ExternalId__c;             
                    insert att;  
                }else{
                    System.debug('Attachment already exists');
                }
            }
        }
    }
}