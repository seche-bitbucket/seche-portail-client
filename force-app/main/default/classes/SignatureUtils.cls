/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : SignatureUtils
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : SignatureUtils
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 17-08-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
public class SignatureUtils {

    public static Map<Contact,List<Quote>> groupedQuote = new Map<Contact,List<Quote>>();
    public static Map<Id,String> mapQIdUrlSigned = new Map<Id,String>();
    public static Map<Id,User> mapQIdSalesman = new Map<Id,User>();
    public static List<Task> lstTackToCreated  = new List<Task>();
    public static Map<Id,ContentVersion> mapQIdDoc = new Map<Id,ContentVersion>();
    public static User Assistante = [SELECT Id, Name,email,Signature FROM User WHERE Id = :UserInfo.getUserId()];
    //public static Integer NextYear = System.today().year()+1;
    public static Integer Nextyear = Integer.valueOf(System.Label.YearN1);
    public static String emailBody = '';
    public static String emailSubject = '';
    public static Map<Id,String> mapContactIdEmailSubject = new Map<Id,String>();
   
    
    public static void sendEmailGroup(List<Quote> quotesProcessed, Boolean isReminder){
        try{
            generateMap(new Map<Id,Quote>(quotesProcessed));
            List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();
            if(groupedQuote.size()>0){
                for(Contact c : groupedQuote.keySet()){
                    String subject = '';
                    User salesman = new User();
                    List<Quote> lstQuote = groupedQuote.get(c);
                    List<String> lstLink = new List<String>();
                    List<String> lstCodes = new List<String>();
                    List<ContentVersion> lstCv = new List<ContentVersion>();
                    for(Quote q : lstQuote){
                        lstLink.add(mapQIdUrlSigned.get(q.Id));
                        lstCv.add(mapQIdDoc.get(q.Id));
                        lstCodes.add(q.QuoteCode__c);
                    }
                    if(lstQuote.size()==1){
                        subject = 'Convention '+NextYear+' - '+lstQuote.get(0).Opportunity.Filiale__c+' - '+lstQuote.get(0).Account.Name;
                        salesman = mapQIdSalesman.get(lstQuote.get(0).Id);
                    }
                    else{
                        subject = 'Conventions '+NextYear;
                    }

                    if(isReminder){
                        subject = 'RELANCE : ' + subject;
                    }
                    //map subject to contact
                    mapContactIdEmailSubject.put(c.Id, subject);


                    Messaging.SingleEmailMessage emailToSend = createPackageMail(c,Assistante,subject,lstLink,lstCv,salesman,lstCodes);
                    emailsToSend.add(emailToSend);
                    
                   /* if(String.isBlank(emailBody) && String.isBlank(emailSubject)){
                        emailBody = emailToSend.htmlBody;
                        emailSubject = emailToSend.subject;
                    }*/
                }
            }
            
            if(emailsToSend.size()>0){
                Messaging.sendEmail(EmailsToSend);
                for(Quote qt : quotesProcessed){
                    emailSubject = mapContactIdEmailSubject.get(qt.ContactId);
                    Task t = LC_GenericEmailSend.createEmailActivity(emailSubject,emailBody, qt.ContactId, qt.Id);
                    lstTackToCreated.add(t);
                }
                //insert lstTackToCreated;
                
                // List<ContentDocumentLink> lstDlink = new List<ContentDocumentLink>();
                
                // for(Task t: lstTackToCreated) {
                //     lstDlink.add(LC_GenericEmailSend.linkAttToTask(t.id, new List<ContentVersion>{mapQIdDoc.get(t.WhatId)})[0]);             
                // }
                // insert lstDlink;
                
            }
        }
        catch(Exception ex){
            System.debug(ex.getMessage());
        }
    }

    public static void generateMap(Map<Id,Quote> quotesProcessed){
        Map<Id,Id> QIdSalesmanId = new Map<Id,Id>();
        Set<Id> salesmanUserIds = new Set<Id>();      
        Set<Id> contactIds = new Set<Id>();
        system.debug(quotesProcessed);
        if(quotesProcessed.size()>0){
            for(Quote q : quotesProcessed.values()){
                if(q.ContactId != null){
                    contactIds.add(q.ContactId);
                    salesmanUserIds.add(q.Opportunity.Salesman__c);
                    QIdSalesmanId.put(q.Id,q.Opportunity.Salesman__c);
                }
            }
            /* BEGIN GET groupedQuote */     
            Map<Id,Contact> lstContact = new Map<Id,Contact>([SELECT Id,Name,Email FROM Contact WHERE Id IN :contactIds]);
            
            for(Quote q : quotesProcessed.values()){
                Contact c = lstContact.get(q.ContactId);
                if(!groupedQuote.containsKey(c)){
                    List<Quote> lstQuote = new List<Quote>();
                    lstQuote.add(q);
                    groupedQuote.put(c,lstQuote);
                }
                else{
                    List<Quote> lstQuote = groupedQuote.get(c);
                    lstQuote.add(q);
                    groupedQuote.put(c,lstQuote);
                }
            }
            /* END GET groupedQuote */
            /* BEGIN GET UrlQuoteToSigned */  
            List<DocumentsForSignature__c> docToSigned = [SELECT Id,SigningUrl__c,Quote__c FROM DocumentsForSignature__c WHERE Quote__c IN : quotesProcessed.keySet()];
            for(DocumentsForSignature__c doc : docToSigned){
                String link='<a href="'+doc.SigningUrl__c+'"><br><br>';
                mapQIdUrlSigned.put(doc.Quote__c,doc.SigningUrl__c);
            }
            /* END GET UrlQuoteToSigned */
            /* BEGIN GET mapQIdSalesman */
            List<User> Users=[SELECT Id, Name,Signature,email FROM User WHERE Id IN:salesmanUserIds];
            for(User u : Users){
                for(Id qID : QIdSalesmanId.keySet()){
                    if(QIdSalesmanId.get(qID) == u.Id){
                        mapQIdSalesman.put(qID,u);
                    }
                }
            }
            /* END GET mapQIdSalesman */
            /* BEGIN GET mapQIdDoc */
            List<ContentDocumentLink> cdls= [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN :quotesProcessed.keySet()];
            Map<Id,Id> docIdQId = new Map<Id,Id>();
            for(ContentDocumentLink cdl : cdls){
                docIdQId.put(cdl.ContentDocumentId,cdl.LinkedEntityId);
            }
            List<ContentVersion> cvs = [select PathOnClient, ContentLocation, Origin, OwnerId, Title, VersionData, ContentDocumentId,FileExtension from ContentVersion where ContentDocumentId IN :docIdQId.keySet() ];
            for(ContentVersion cv : cvs){
                mapQIdDoc.put(docIdQId.get(cv.ContentDocumentId),cv);
            }
            /* END GET mapQIdDoc */
        }
    }

    private static Messaging.SingleEmailMessage createPackageMail(Contact cont, User ass, String subject, List<String> tempLinkTosend, List<ContentVersion> tempFileTosend,User salesman, List<String> lstCodes){
        System.debug('***LinkPackage  +'+tempLinkTosend);
        String contactGreeting;
        String CommercialText;
        String secheIndustryText;
        String link ='';
        String greeting;
        String codesList='';

        if(lstCodes.size() > 0) {
            codesList = '</br>Liste des codes devis : <ul>';
            for(String code : lstCodes) {
                codesList = codesList + '<li>' + code + '</li>';
            }
            codesList = codesList + '</ul>';
        }
    
        contactGreeting ='Cher client,<br><br>';
        CommercialText='Nous vous prions de bien vouloir trouver ci-joint vos conventions ';
        secheIndustryText='et vous invitons, via Connective, à les signer en 3 étapes en cliquant sur les liens que vous allez recevoir dans un autre mail. <br><br>NB : pour indiquer les coordonnées d\'un autre signataire de votre établissement, il vous est possible par ce lien de <strong>\'réattribuer\'</strong> cette convention.<br>';

        /*for(String li : tempLinkTosend) {
            link=link+li+'<br>';
        }*/
        greeting= '<br><br>Cordialement,<br>';
    
        String body = contactGreeting+CommercialText+secheIndustryText+link+codesList+greeting;
    
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
    
        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        system.debug('LC_SendMassEmail_AugTarifCtrl.execute.createPackageMail.tempFileTosend --- >  '+tempFileTosend.size());

        
        List<Id> lstIds = new List<Id>();
        
        for(ContentVersion cversion : tempFileTosend) {
            Messaging.EmailFileAttachment fileAttachment = new Messaging.EmailFileAttachment();			
            fileAttachment.setFileName(cversion.title+'.'+cversion.FileExtension);
            fileAttachment.setBody(cversion.versiondata);
            // Add to attachment file list
            fileAttachments.add(fileAttachment);
        	lstIds.add(cversion.Id);
        }
        
        // message.setEntityAttachments(lstIds);
        message.setFileAttachments(fileAttachments);
        
        String fromAddresses = ass.Email;
        String toAddresses = cont.Email;
        String cciEmail = ass.Email;
        String salesmanEmail = '';
        if(salesman !=null){
            salesmanEmail = salesman.Email;
        }
        String[] ccAdr;
        
        message.setReplyTo(fromAddresses);
        message.toAddresses = new String[] {toAddresses};
        if(salesmanEmail !=null && salesmanEmail!=''){
            ccAdr = new String[] {cciEmail,salesmanEmail};
        }
        else{
            ccAdr = new String[] {cciEmail};
        }

        System.debug(UserInfo.getUserEmail());
        if(UserInfo.getUserEmail() != null && !ccAdr.contains(UserInfo.getUserEmail())) {
            ccAdr.add(UserInfo.getUserEmail());
        }
        System.debug(ccAdr);

        message.ccaddresses = ccAdr;
        message.subject = subject;
        message.setSenderDisplayName(ass.Name);
        message.htmlBody = body;
        message.setTargetObjectId(cont.Id);
        message.setSaveAsActivity(true);
    
        return message;
    }
}