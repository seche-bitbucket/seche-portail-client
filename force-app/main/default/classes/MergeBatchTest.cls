@isTest(SeeAllData=false)
public class MergeBatchTest {
    
    @isTest
    public static void test() {
        
        List<Account> accs = new List<Account>{new Account(Name = 'Test Acc dup 1', SIRET_Number__c='01234567890123', ELLISPHERE__Elli_Ellinumber__c='TEST001', TECH_X3SECHEID__c='TEST001', TECH_IsPassed__c=false),
            new Account(Name = 'Test Acc 1', SIRET_Number__c='01234567890123', TECH_IsPassed__c=false),
            new Account(Name = 'Test Acc 2', SIRET_Number__c='98765432109876', ELLISPHERE__Elli_Ellinumber__c='TEST002', TECH_X3SECHEID__c='TEST002'),
            new Account(Name = 'Test Acc dup 3', SIRET_Number__c='52201552220001'),
            new Account(Name = 'Test Acc 3', SIRET_Number__c='52201552220001', ELLISPHERE__Elli_Ellinumber__c='TEST003', TECH_X3SECHEID__c='TEST003', ContractRoot__c='TEST001'),
            new Account(Name = 'Test Acc dup 2', SIRET_Number__c='98765432109876')};
        
        insert accs;
        
        Contact cont = new Contact(LastName = 'Test Contact', FirstName = 'Test', Email = 'test@Test.com', Phone= '0606060606', AccountId=accs[0].id);
        insert cont;
        
        Contact cont1 = new Contact(LastName = 'Test Contact dup 1', FirstName = 'Test', Email = 'test01@Test.com', Phone= '0606060606', AccountId=accs[1].id);
        insert cont1;
        
        Contact cont2 = new Contact(LastName = 'Test Contact 2', FirstName = 'Test', Email = 'test02@Test.com', Phone= '0606060606', AccountId=accs[3].id);
        insert cont2;
        
        Contact cont3 = new Contact(LastName = 'Test Contact dup 2', FirstName = 'Test', Email = 'testdup02@Test.com', Phone= '0606060606', AccountId=accs[4].id);
        insert cont3;
              
        AccountContactRelation rel02 = new AccountContactRelation(AccountId = accs[1].Id, ContactId = cont.Id);
        
        insert rel02;
        
        System.debug(Database.query('SELECT Id, Account.Id, Account.TECH_IsDuplicate__c, Account.TECH_NotToMerge__c, Account.TECH_IsPassed__c, Account.SIRET_Number__c, Account.ContractRoot__c FROM Contact WHERE Account.TECH_IsPassed__c = false AND Account.SIRET_Number__c != NULL AND (Account.SIRET_Number__c LIKE \'______________\') AND (NOT Account.CustomerNature__c IN (\'Collectivité et administration\',\'Particulier\', \'Groupe Séché\'))'));
        
        Test.startTest();
        
        Id batchJobId = Database.executeBatch(new Batch_CheckForDuplicates());
		system.debug(batchJobId);
        
        AsyncApexJob queryJob = [SELECT ApexClassId,CompletedDate,CreatedById,CreatedDate,ExtendedStatus,Id,JobItemsProcessed,JobType,LastProcessed,LastProcessedOffset,MethodName,NumberOfErrors,ParentJobId,Status,TotalJobItems FROM AsyncApexJob where id =: batchJobId];
        system.debug(queryJob.TotalJobItems);
        
        Test.stopTest();
        
        System.assertEquals(4, [SELECT ID FROM ACCOUNT WHERE TECH_IsDuplicate__c = true].size());
        
    }
    
    @isTest
    public static void test2() {
        
        List<Account> accs = new List<Account>{new Account(Name = 'Test Acc dup 1', SIRET_Number__c='01234567890123', ELLISPHERE__Elli_Ellinumber__c='TEST001', TECH_X3SECHEID__c='TEST001', TECH_IsPassed__c=false),
            new Account(Name = 'Test Acc 1', SIRET_Number__c='01234567890123', TECH_IsPassed__c=false),
            new Account(Name = 'Test Acc 2', SIRET_Number__c='98765432109876', ELLISPHERE__Elli_Ellinumber__c='TEST002', TECH_X3SECHEID__c='TEST002'),
            new Account(Name = 'Test Acc dup 3', SIRET_Number__c='52201552220001'),
            new Account(Name = 'Test Acc 3', SIRET_Number__c='52201552220001', ELLISPHERE__Elli_Ellinumber__c='TEST003', TECH_X3SECHEID__c='TEST003', ContractRoot__c='TEST001'),
            new Account(Name = 'Test Acc dup 2', SIRET_Number__c='98765432109876')};
        
        insert accs;
        
        Test.startTest();
        
        Id batchJobId = Database.executeBatch(new Batch_CheckDuplicateAccounts());
               
        Test.stopTest();
        
        System.assertEquals(6, [SELECT ID FROM ACCOUNT WHERE TECH_IsDuplicate__c = true].size());
        Database.executeBatch(new Batch_MergeDuplicateAccounts());
        
        
    }

}