public class ContactController {
    public Contact contact {get; set;}
    public Account account {get; set;}

    public ContactController(ApexPages.StandardSetController controller){
    contact = [SELECT Name, AccountId FROM Contact where ID= :ApexPages.currentPage().getParameters().get('ID') limit 1 ];
    account = [SELECT Name FROM Account where ID= :contact.AccountId limit 1 ];
  }
}