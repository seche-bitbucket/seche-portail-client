public with sharing class LC_SetQuoteStatusToSignedCtrl {
	@AuraEnabled
	public String message {get; set;}
	@AuraEnabled
	public String showToastMode {get; set;}
	@AuraEnabled
	public Quote objectQuote {get; set;}
	@AuraEnabled
	public Boolean success {get; set;}


	@AuraEnabled
	public static Quote getQuote(Id quoteId){
		System.debug('getQuote-----Start- parm: Quote ID : '+quoteId);
		return [SELECT Id, Name, Status From Quote Where Id =: quoteId];
	}

	@AuraEnabled
	public static LC_SetQuoteStatusToSignedCtrl setStatus(Quote quot, String status){
		System.debug('setTosigned-----Start- parm: quot : '+quot.Status+' => '+status);
		LC_SetQuoteStatusToSignedCtrl wrapper = new LC_SetQuoteStatusToSignedCtrl();
		wrapper.objectQuote = quot;	
		try {
			if(quot != null) {
				if(quot.Status == status) {
					wrapper.message = 'Le devis est déjà à l\'état '+status;
					wrapper.showToastMode = 'warning';
					System.debug('setTosigned-----End- result:  '+wrapper);				
				}else {
					quot.Status=status;
					updateLockedRecord updateLockedRecord = new updateLockedRecord();
					updateLockedRecord.updateRecord(quot);

					wrapper.message = 'Etat du devis modifé avec succès';
					wrapper.showToastMode = 'success';
					System.debug('setTosigned-----End- result:  '+wrapper);
				}
			}
		} catch(Exception e) {
			wrapper.message = errorHandling(e);
			wrapper.showToastMode = 'error';
		}
		return wrapper;
	}

	// error handling
	private static String errorHandling(Exception e) {

        String message = e.getMessage();
        // split message => message = apex message; system message  
        List<String> separatedMsg;
        // we will work with system message
        String systemError;
        String finalMessage;

        // message are like
        // System.DmlException: Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Le devis synchronisé n'est pas signé, vous ne pouvez pas passer en gagné l'opportunité : veuillez mettre à jour le statut du devis synchronisé.: []

        // first we split with ; if there is any
        if(message.contains(';')) {
            separatedMsg = message.split(';');
            // we delete the first one, and recreate the string with join
            separatedMsg.remove(0);

            systemError = String.join(separatedMsg, ';');
            // we recreate the msg to be sure not removing any custom message

            // recreated msg : first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Le devis synchronisé n'est pas signé, vous ne pouvez pas passer en gagné l'opportunité : veuillez mettre à jour le statut du devis synchronisé.: []
            // we need to remove first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, we use the same process
            if(systemError.contains(',')) {
                System.Debug(systemError);
                separatedMsg = systemError.split(',');

                // we delete the first one, and recreate the string with join
                separatedMsg.remove(0);

                systemError = String.join(separatedMsg, ',');
                // we recreate the msg to be sure not removing any custom message

                if(systemError.contains(':')) {
                    System.Debug(systemError);

                    separatedMsg = systemError.split(':');

                    // we delete the last and recreate the string with join
                    separatedMsg.remove(separatedMsg.size()-1);

                    finalMessage = String.join(separatedMsg, ':');
                    // we recreate the msg to be sure not removing any custom message
                
                } else {
                    finalMessage = systemError;
                }
            } else {
                finalMessage = systemError;
            }
        } else {
            finalMessage = message;
        }

        return finalMessage;

    }

//Class for updating locked record (without sharing to by pass locked record)
	without sharing class updateLockedRecord {
	public void updateRecord(Quote qt) {
		update qt;
	    }
	}
}