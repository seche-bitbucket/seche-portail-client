@isTest(SeeAllData=true)

public class TestUploadData {
    @isTest static void testInsertDemandeDeService() {
        //PageReference pageRef = Page.UploadDataVFP;
        
string myfile='HOPITAL :;Eau de Bicêtre;;;;;;;;2017ACHD173024;1;;;;\n';
myfile+='"SERVICE :\n';
myfile+='Préciser secteur/bâtiment/porte";Laboratoires secteur Van bâtiment Broca 3eme étage;;;;;;;;FR client :;;FR92CL0095;;;\n';
myfile+='Adresse de collecte :;Local Commun DECHETS CHIMIQUES;;;;;;;;Interlocuteur AP-HP :;;SOL;Agnès PRADEL;;\n';
myfile+='Date de collecte :;14/05/2018;;;Horaire collecte :;;De 6h00 à 13h00;;;01 42 18 65 92;;;;;\n';
myfile+='"TRIADIS SERVICES  - Séché Environnement - Courriel :  planningtriadisetampes@groupe-seche.com    \n';
myfile+='Tél : 01 69 16 13 13 / 01 69 16 13 11 - Fax : 01 69 16 13 12 ";;;;;;;;;;;;;;\n';
myfile+='ENLEVEMENT DES DECHETS CHIMIQUES;;;;;;;;;;;;;;\n';
myfile+=';BIDON 2 L;BIDON 5 L;BIDON 10 L;BIDON 20 L;SEAU 2 L;SEAU 5 L;SEAU 10 L;SEAU 20 L;SEAU 30 L;FUT BLEU 30 L;FUT BLEU 50 L;Caisse 60 L;GRV 660 L;\n';
myfile+='Solvants et produits organiques non halogénés - ROUGE (E1);;;3;4;;;;;;;;;;\n';
myfile+='Solvants halogénés - ROUGE (E2);;;;3;;;;;;;;;;\n';
myfile+='Acides - JAUNE (E3);;;;;;;;;;;;;;\n';
myfile+='Sels (à l\'exclusion des cyanurés et arséniés) - BLEU (E4);;;;;;;;;;;;;;\n';
myfile+='Bases - BLEU (E5);;;;;;;;;;;;;;\n';
myfile+='Cyanurés & Arséniés - VERT (E6);;;;;;;;;;;;;;\n';
myfile+='CMR solides / pateux - VERT (E7);;;;;;;;;;;;;;\n';
myfile+='CMR médicaments cytotoxiques / cytostatiques - VERT (E8);;;;;;;;;;;;;;\n';
myfile+='CMR liquides - VERT (E9);;;;1;;;;;;;;;;\n';
myfile+='Déchet dangereux à risque infectieux associé, non décontaminable -  VERT (E10);;2;;6;;;;;;;;;;\n';
myfile+='Hautement réactif éliminé pur - VERT (E11);;;;;;;;;;;;;;\n';
myfile+='Nature inconnue, non spécifiée, petits flaconnages liquides ou solides - BLANC (E12);;;;;;;;;;;;;;\n';
myfile+='Déchets chimiques solides - BLANC (E13);;;;;;;;;;;;;;\n';
myfile+='Emballages vides souillés - BLANC (E14);;;;;;;;;;;;;;\n';
myfile+='Médicaments périmés hors anti-cancéreux - BLANC (E15);;;;;;;;;;;;;;\n';
myfile+='Aérosols - GRIS (E16);;;;;;;;;;;;;;\n';
myfile+='Produits phytosanitaires - TURQUOISE (E17);;;;;;;;;;;;;;\n';
myfile+='Peintures, vernis, colles - VERT CLAIR (E18);;;;;;;;;;;;;;\n';
myfile+='Déchets chimiques souillés au plomb  - BLANC (E19);;;;;;;;;;;;;;\n';
myfile+='Déchets chimiques contenant du mercure  - BLANC (E20);;;;;;;;;;;;;;\n';

        
        blob blobfile=blob.valueOf(myfile);
        
        UploadData controller = new UploadData();
        controller = new UploadData();
        controller.contentFile=blobfile; 
         System.assertNotEquals(null, blobfile);
        controller.ReadFile();
        List<SD_ServiceDemand__c> DSrs= [select LocalContact__c from SD_ServiceDemand__c where LocalContact__c='Local Commun DECHETS CHIMIQUES' LIMIT 1];
        
       // system.assertEquals('Local Commun DECHETS CHIMIQUES', DSrs.get(0).LocalContact__c);
        
        
        
        
        
    }
    
    @isTest static void testInsertQuoteLine() {
        //PageReference pageRef = Page.UploadDataVFP;
string myfile='HOPITAL :;Eau de Bicêtre;;;;;;;;2017ACHD173024;1;;;;\n';
myfile+='"SERVICE :\n';
myfile+='Préciser secteur/bâtiment/porte";Laboratoires secteur Van bâtiment Broca 3eme étage;;;;;;;;FR client :;;FR92CL0095;;;\n';
myfile+='Adresse de collecte :;Local Commun DECHETS CHIMIQUES;;;;;;;;Interlocuteur AP-HP :;;SOL;Agnès PRADEL;;\n';
myfile+='Date de collecte :;14/05/2018;;;Horaire collecte :;;De 6h00 à 13h00;;;01 42 18 65 92;;;;;\n';
myfile+='"TRIADIS SERVICES  - Séché Environnement - Courriel :  planningtriadisetampes@groupe-seche.com    \n';
myfile+='Tél : 01 69 16 13 13 / 01 69 16 13 11 - Fax : 01 69 16 13 12 ";;;;;;;;;;;;;;\n';
myfile+='ENLEVEMENT DES DECHETS CHIMIQUES;;;;;;;;;;;;;;\n';
myfile+=';BIDON 2 L;BIDON 5 L;BIDON 10 L;BIDON 20 L;SEAU 2 L;SEAU 5 L;SEAU 10 L;SEAU 20 L;SEAU 30 L;FUT BLEU 30 L;FUT BLEU 50 L;Caisse 60 L;GRV 660 L;\n';
myfile+='Solvants et produits organiques non halogénés - ROUGE (E1);;;3;4;;;;;;;;;;\n';
myfile+='Solvants halogénés - ROUGE (E2);;;;3;;;;;;;;;;\n';
myfile+='Acides - JAUNE (E3);;;;;;;;;;;;;;\n';
myfile+='Sels (à l\'exclusion des cyanurés et arséniés) - BLEU (E4);;;;;;;;;;;;;;\n';
myfile+='Bases - BLEU (E5);;;;;;;;;;;;;;\n';
myfile+='Cyanurés & Arséniés - VERT (E6);;;;;;;;;;;;;;\n';
myfile+='CMR solides / pateux - VERT (E7);;;;;;;;;;;;;;\n';
myfile+='CMR médicaments cytotoxiques / cytostatiques - VERT (E8);;;;;;;;;;;;;;\n';
myfile+='CMR liquides - VERT (E9);;;;1;;;;;;;;;;\n';
myfile+='Déchet dangereux à risque infectieux associé, non décontaminable -  VERT (E10);;2;;6;;;;;;;;;;\n';
myfile+='Hautement réactif éliminé pur - VERT (E11);;;;;;;;;;;;;;\n';
myfile+='Nature inconnue, non spécifiée, petits flaconnages liquides ou solides - BLANC (E12);;;;;;;;;;;;;;\n';
myfile+='Déchets chimiques solides - BLANC (E13);;;;;;;;;;;;;;\n';
myfile+='Emballages vides souillés - BLANC (E14);;;;;;;;;;;;;;\n';
myfile+='Médicaments périmés hors anti-cancéreux - BLANC (E15);;;;;;;;;;;;;;\n';
myfile+='Aérosols - GRIS (E16);;;;;;;;;;;;;;\n';
myfile+='Produits phytosanitaires - TURQUOISE (E17);;;;;;;;;;;;;;\n';
myfile+='Peintures, vernis, colles - VERT CLAIR (E18);;;;;;;;;;;;;;\n';
myfile+='Déchets chimiques souillés au plomb  - BLANC (E19);;;;;;;;;;;;;;\n';
myfile+='Déchets chimiques contenant du mercure  - BLANC (E20);;;;;;;;;;;;;;\n';


        
        
        blob blobfile=blob.valueOf(myfile);
        
        UploadData controller = new UploadData();
        controller = new UploadData();
        controller.contentFile=blobfile; 
                 System.assertNotEquals(null, blobfile);

        controller.ReadFile();
        string labTest='TEST DECHET';
       // list<QuoteLine__c> qltype1 = [select label__c from QuoteLine__c where label__c =:labTest];
       // system.assertEquals('TEST DECHET', qltype1.get(0).label__c );
        
        
        
    }
    
}