@isTest
global class WS001_ConnectiveMock implements  HTTPCalloutMock{
        global Static String externalId;
	global HTTPResponse respond(HTTPRequest req){
        
        System.assertEquals('callout:ConnectiveApi/flows/', req.getEndpoint());
        HttpResponse res = new HTTPResponse();
        res.setHeader('Content-Type', 'application/JSON');
        res.setBody('{ "FlowId": "5a2aff04-3cfa-4278-9480-64ac39f74734", "ExternalReference": "'+externalId+'", "DownloadURL": "http://myserver/downloadapi/services/signingflow/5a2aff04-3cfa-4278-9480-64ac39f74734/download", "F2FSigningURL": "http://myserver/signinit?id=5a2aff04-3cfa-4278-9480-64ac39f74734", "EmailAddress": "tech@connective.be", "CreationTime": "2016-02-29P04:09:03++00:+00", "SigningURL": [ { "Url": "http://myserver/signinit?id=ad17c85c-8080-468c-b971-d8cae988503b", "ExternalReference": "ReferenceSigner1"}, {"Url": "http://myserver/signinit?id=31242bc8-9c1b-4b3e-919b-8a5fa970e3b4", "ExternalReference": "ReferenceSigner2" } ], "ErrorLog": [] }');
        req.setMethod('POST');
        res.setStatusCode(200);
        return res;
    }
}