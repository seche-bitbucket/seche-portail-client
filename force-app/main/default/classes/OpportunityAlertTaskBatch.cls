global class OpportunityAlertTaskBatch implements Database.Batchable<sObject>{
    
    private String query;
    public OpportunityAlertTaskBatch(String query)
    {
        this.query = query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Opportunity> scope){
        List<Task> taches = new List<Task>();
        for(Opportunity opp : scope)
        {
            Task t = new Task();
            t.Subject = 'Opportunité avec date de clôture dépassée';
            t.Priority = 'Normal';
            t.WhatId = opp.Id;
            t.ActivityDate = Date.today();
            t.OwnerId = opp.OwnerId;
            t.Status = 'Non démarrée';
            t.IsReminderSet = true;
            t.ReminderDateTime = System.today();
            taches.add(t);
        }
        insert taches;
    }
    
    global void finish(Database.BatchableContext BC){
    }
}