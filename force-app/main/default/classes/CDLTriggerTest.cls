@isTest
public class CDLTriggerTest {
    static FIP_FIP__c fip;
    @isTest
    public static void init(){
        Account a;
        Contact c;
        Opportunity opp;
        
        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        fip = new FIP_FIP__c();
        fip.Collector__c = a.ID;
        fip.Contact__c = c.ID;
        system.debug(fip.id);
        insert fip;
    }
    static testMethod void testCDLtrigger(){
        init();
        string before = 'Testing base 64 encode';            
        Blob beforeblob = Blob.valueOf(before);
        
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=fip.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        
        
        Test.startTest();
        /*Test.setMock(HttpCalloutMock.class, new WS001_ConnectiveMock()); 
        WS001_ConnectiveMock.externalId=fip.id;
        */
        insert contentlink;
        test.stopTest();
        
    }
}