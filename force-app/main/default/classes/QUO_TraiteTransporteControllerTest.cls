@isTest
public class QUO_TraiteTransporteControllerTest {
    public static PageReference myVfPage {get;set;}
    public static ApexPages.StandardSetController stc {get;set;}
    public static QUO_TraiteTransporteController vfc{get;set;}
    @isTest Static void testStdController(){
       	ID  standardPb = Test.getStandardPricebookId();
        Product2 prd1 = new Product2 (Name='Test Product Entry 1',Description='Test Product Entry 1',productCode = 'ABC', isActive = true, TECH_ExternalID__c='zExternalID_Capgemini001');
        insert prd1;
        PricebookEntry pbe1 = new PricebookEntry (Product2ID=prd1.id,Pricebook2ID=standardPb ,UnitPrice=50, isActive=true);
        insert pbe1;
        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '01000';
        a.NAF_Number__c = '1234A';
        insert a;
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        //c.Salesman__c = director.Id;
        c.Email = 'aaa@groupe-seche.com';
        c.Phone = '03 00 00 00 00';
        insert c;
        Opportunity opp = new Opportunity();
        opp.Name = 'test';
        //opp.Salesman__c = director.Id;
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        opp.Pricebook2Id = pbe1.Pricebook2Id;
        insert opp;
        
        Quote quo = new Quote();
        quo.OpportunityId = opp.ID;
        quo.name = 'Mon Devis Test';
        quo.Pricebook2Id =pbe1.Pricebook2Id;
        insert quo;
        List<QuoteLineItem> qliToInsert=new List<QuoteLineItem>();
        QuoteLineItem_Field__c f = new QuoteLineItem_Field__c(Name='INCOTERMS__c', OppLineSyncField__c='INCOTERMS__c');
        insert f;
        Product2 prd = new product2(name = 'Test' ,TECH_ExternalID__c = '012345',Family = 'Traitement', isActive = true );
        insert prd;
        PricebookEntry pbe = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=Test.getStandardPricebookId(), IsActive = true);
        insert pbe;
        OpportunityLineItem oli = new OpportunityLineItem (OpportunityID = opp.id, PriceBookEntryID=pbe.id, quantity=4, UnitPrice=4);
        insert oli;
        QuoteLineItem qli = new QuoteLineItem (QuoteID=quo.id,PriceBookEntryID=pbe1.id, quantity=4, UnitPrice=10, OpportunityLineItemId = oli.Id);
        QuoteLineItem qli2 = new QuoteLineItem (QuoteID=quo.id,PriceBookEntryID=pbe1.id, quantity=2, UnitPrice=30, OpportunityLineItemId = oli.Id);
        qliToInsert.add(qli);
        qliToInsert.add(qli2);
        insert qliToInsert;
        List<QuoteLineItem> qlis=new List<QuoteLineItem>();
        stc=new Apexpages.standardSetController(qlis);
        vfc = new QUO_TraiteTransporteController(stc);
        ApexPages.currentPage().getParameters().put('retUrl', quo.Id);
        PageReference pageRef = vfc.returnToPrevious();
        test.setCurrentPage(pageRef);
        Test.startTest();
        stc=new Apexpages.standardSetController(qliToInsert);
        stc.setSelected(qliToInsert);
        vfc = new QUO_TraiteTransporteController(stc);  
        vfc.QuoteLineItemList[2].QLI.UnitPrice=10;
        vfc.save();
        vfc.cancelProcess();
        Test.stopTest();
        List<QuoteLineItem> changedQuotes=[SELECT UnitPrice,TreatmentPrice__c,TransportPrice__c,Nature__c FROM QuoteLineItem WHERE QuoteId=:quo.Id];
        System.assertEquals(2, changedQuotes.size());
    }
}