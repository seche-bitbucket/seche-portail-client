public class ServiceDemandTriggerUtil {
    
    
    public static void ManageOwner(List<SD_ServiceDemand__c> sds){
        
        //Check if it's a RCT Service Demand
        if(isTransfoRecordType(sds)){
            for(SD_ServiceDemand__c sd : sds){
                sd.OwnerId = Label.RCT_DEFAULT_DS_OWNER;
            }
        }
    }
        
    public static void updateDetails(List<SD_ServiceDemand__c> sds){
        
        User usr = [Select ContactID, Contact.AccountID from User where id =: Userinfo.getUserid()];
        
        for(SD_ServiceDemand__c sd : sds){
            //Check if it's a RCT Service Demand
            if(sd.RecordTypeId == Label.RCT_RT_DemandeService_Elimination 
              || sd.RecordTypeId == Label.RCT_RT_DemandeService_Repairs
              || sd.RecordTypeId == Label.RCT_RT_DemandeService_Waste
              || sd.RecordTypeId == Label.RCT_RT_DemandeService_Remediation
              || sd.RecordTypeId == Label.RCT_RT_DemandeService_EliminationSF6)
            if(usr.ContactID != null){
                sd.ApplicantCenter__c = usr.Contact.AccountID;
                sd.ResponsibleName__c = usr.ContactID;
            }
        }    
    }
    
    public static void updateLocalContact(List<SD_ServiceDemand__c> sds){
        
        System.debug('Contact Local : here');
        //Check if were are working with Transfo Service Demands
        if(isTransfoRecordType(sds)){
            //Collect All Contact IDs
            Set<ID> listOfContacts = new Set<ID>();
            for(SD_ServiceDemand__c sd : sds){
                listOfContacts.add(sd.ResponsibleName__c);
                
            }
            
            //Get the map of all Contacts 
            Map<ID, Contact> MapContact = new Map<ID, Contact>([Select ID, Name, Phone, MobilePhone, Fax,  Email FROM Contact WHERE ID in : listOfContacts]);
            

            for(SD_ServiceDemand__c sd : sds){
                if(sd.ResponsibleName__c != null){
                    Contact ctc = MapContact.get(sd.ResponsibleName__c);
                    
                    sd.Phone__c = ctc.Phone;
                    
                    sd.Mobile__c = ctc.MobilePhone;
                    
                    sd.Fax__c = ctc.Fax;
                    
                    sd.Email__c = ctc.Email;
                }
                 if(sd.IsSameLocalContact__c || (sd.LocalContact__c == null && sd.ContactPhone__c == null)){
                    if(sd.ResponsibleName__c != null){
                        Contact ctc = MapContact.get(sd.ResponsibleName__c);
                        sd.LocalContact__c = ctc.Name;
                    }

                    sd.ContactPhone__c = sd.Phone__c;
                    sd.ContactMobile__c = sd.Mobile__c;
                    sd.ContactFax__c = sd.Fax__c;
                    sd.ContactEmail__c = sd.Email__c;
                }
                
            }
        }
        
    }
      
    //If the removalAdress field is filled with an account 
    //All adress' information will be put in the Removal adress field of the DS
    /*public static void updateRemovalAdress(List<SD_ServiceDemand__c> sds){
        
        for(SD_ServiceDemand__c sd : sds){
            if(sd.IsSameLocalContact__c || (sd.LocalContact__c == null && sd.ContactPhone__c == null)){
                sd.LocalContact__c = sd.ResponsibleName__r.Name;
                sd.ContactPhone__c = sd.Phone__c;
                sd.ContactMobile__c = sd.Mobile__c;
                sd.ContactFax__c = sd.Fax__c;
                sd.ContactEmail__c = sd.Email__c;
            }
            
        }
        
    }*/
    
    public static void updateAddress(List<SD_ServiceDemand__c> sds){
        
        //Collect All account IDs
        Set<ID> listOfAccounts = new Set<ID>();
        for(SD_ServiceDemand__c sd : sds){
            listOfAccounts.add(sd.BilledAccount__c);
            listOfAccounts.add(sd.RemovalAdress__c);
            
        }
        
        //Get the map of all accounts 
        Map<ID, Account> MapAccount = new Map<ID, Account>([Select ID, BillingStreet, BillingPostalCode, BillingCity FROM Account WHERE ID in : listOfAccounts]);
        System.debug('MapAccount : '+MapAccount);
        for(SD_ServiceDemand__c sd : sds){
            if(sd.RemovalAdress__c != null){
                Account acc = MapAccount.get(sd.RemovalAdress__c);
                System.debug('acc : '+acc);
                sd.RemovalStreet__c = acc.BillingStreet;
                sd.RemovalPostalCode__c = acc.BillingPostalCode;
                sd.RemovalCity__c = acc.BillingCity;
                
                if(sd.IsSameShippingAddress__c || (sd.ShippingStreet__c == null && sd.ShippingPostalCode__c == null)){
                    sd.ShippingStreet__c = acc.BillingStreet;
                    sd.ShippingPostalCode__c = acc.BillingPostalCode;
                    sd.ShippingCity__c = acc.BillingCity;
                    sd.IsSameShippingAddress__c = true;
                }
                
                
            }else if(sd.BilledAccount__c != null){
                Account acc = MapAccount.get(sd.BilledAccount__c);
                sd.RemovalStreet__c = acc.BillingStreet;
                sd.RemovalPostalCode__c = acc.BillingPostalCode;
                sd.RemovalCity__c = acc.BillingCity;
                
                if(sd.IsSameShippingAddress__c || (sd.ShippingStreet__c == null && sd.ShippingPostalCode__c == null)){
                    sd.ShippingStreet__c = acc.BillingStreet;
                    sd.ShippingPostalCode__c = acc.BillingPostalCode;
                    sd.ShippingCity__c = acc.BillingCity;
                }
            }
            
        }
    }
    
    
    
    public static void updatePriceBook(List<SD_ServiceDemand__c> sds){
        List<SD_ServiceDemand__c> sdsDataCollected = ([SELECT ID, Recordtype.DeveloperName,ApplicantCenter__r.EnedisRegion__c, PriceBook__c  FROM SD_ServiceDemand__c WHERE ID in : sds]);
        List<SD_ServiceDemand__c> sdsToUpdate = new List<SD_ServiceDemand__c>();
        
        for(SD_ServiceDemand__c sd : sdsDataCollected){
            if(sd.Recordtype.DeveloperName == 'Remediation'){
                sd.PriceBook__c = Label.RCT_PB_Depollution;
            }else  if(sd.Recordtype.DeveloperName == 'Repairs'){
                sd.PriceBook__c = Label.RCT_PB_Reparation;
            }else if(sd.Recordtype.DeveloperName == 'Elimination'){
                if(sd.ApplicantCenter__r.EnedisRegion__c == 'Nord'){
                    sd.PriceBook__c = Label.RCT_PB_Destruction_Nord;
                }else{
                    sd.PriceBook__c = Label.RCT_PB_Destruction_Sud;
                }
            }else if(sd.Recordtype.DeveloperName == 'EliminationSF6'){
                sd.PriceBook__c = Label.RCT_PB_Elimination_SF6;
            }else{
                sd.PriceBook__c = Label.RCT_PB_TPC;
            }
            sdsToUpdate.add(sd);
        }
        
        update(sdsToUpdate);
    }
    
    public static List<WST_Waste_Statement__c> manageWSTStatus(List<SD_ServiceDemand__c> sds){
        System.debug('sds id : '+ sds.get(0).id);
        
        List<WST_Waste_Statement__c> wstList = new List<WST_Waste_Statement__c>([Select ID, Status__c, ServiceDemande__c  FROM WST_Waste_Statement__c WHERE ServiceDemande__c in : sds ]);
        List<WST_Waste_Statement__c> wstsToUpdate = new List<WST_Waste_Statement__c>();
        System.debug('WSTList : '+ wstList);
        Map<ID, List<WST_Waste_Statement__c>> wstsMap = getWSTMap(wstList);
        if(wstsMap.size() > 0){
            wstsToUpdate = getMyWstToUpdate(wstsMap, sds);
            
        }
        
        return wstsToUpdate;
        
    }
    
    public static Map<ID, List<WST_Waste_Statement__c>> getWSTMap(List<WST_Waste_Statement__c> wsts){
        Map<ID, List<WST_Waste_Statement__c>> myMap = new Map<ID, List<WST_Waste_Statement__c>>();
        
        for(WST_Waste_Statement__c wst : wsts){
            List<WST_Waste_Statement__c> wstTmp = myMap.get(wst.ServiceDemande__c);
            if(wstTmp == null){
                wstTmp = new List<WST_Waste_Statement__c>();
            }
            wstTmp.add(wst);
            myMap.put(wst.ServiceDemande__c, wstTmp);
        }
        
        return myMap;
    }
    
    public static List<WST_Waste_Statement__c> getMyWstToUpdate(Map<ID, List<WST_Waste_Statement__c>> wstsMap, List<SD_ServiceDemand__c> sds){
        List<WST_Waste_Statement__c> result = new List<WST_Waste_Statement__c>();
        for(SD_ServiceDemand__c sd : sds){
            String Status = '';
            if(sd.Status__c == 'Envoyee a TREDI'){
                Status = 'Demande effectuee';
            }else if(sd.Status__c == 'Logistique'){
                Status = 'Logistique';
            }else if(sd.Status__c == 'Recu'){
                Status = 'Recu';
            }else if(sd.Status__c == 'En livraison'){
                Status = 'En livraison';
            }
           System.debug('//wsd'+sd.name);
            if(Status != ''){
                List<WST_Waste_Statement__c> wstsToUpdateStatus = wstsMap.get(sd.ID);
                 System.debug('//wst.wstsToUpdateStatus'+wstsToUpdateStatus);
                if(wstsToUpdateStatus != null){
                    for(WST_Waste_Statement__c wst : wstsMap.get(sd.ID)){
                        System.debug('//wst.Status__c'+wst.Status__c);
                        System.debug('//wst.Boolean'+(Label.RCT_WST_Status_Avoid_SD_Update).contains(wst.Status__c));
                        if(!(Label.RCT_WST_Status_Avoid_SD_Update).contains(wst.Status__c)){
                            wst.Status__c = Status;
                            result.add(wst);
                        }
                        
                    }
                }
            }
        }
        
        return result;
    }
    
    public static Boolean isTransfoRecordType(List<SD_ServiceDemand__c> sds){
        for(SD_ServiceDemand__c sd : sds){
            if(sd.RecordTypeId != Label.RCT_RT_DemandeService_Waste &&
               sd.RecordTypeId != Label.RCT_RT_DemandeService_Repairs &&
               sd.RecordTypeId != Label.RCT_RT_DemandeService_Remediation &&
               sd.RecordTypeId != Label.RCT_RT_DemandeService_EliminationSF6 &&
               sd.RecordTypeId != Label.RCT_RT_DemandeService_Elimination){
                   return false;
               }
            
        }
        
        return true;
    }
    
}