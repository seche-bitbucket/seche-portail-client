@isTest
public class TRANSFO_ManageEliminationControllerTest {

     @IsTest(SeeAllData=true) 
    public static void sendWasteInstanceToEliminationTest(){
         Test.startTest(); 
         
         //create Waste_Statement
         WST_Waste_Statement__c vWst = TestDataFactory.createWasteStatement('AccountName1');
         
         //Insert Waste_Statement
         insert vWst;
        
        //Create opportunity
		 Opportunity vOpportunity = TestDataFactory.createOpportunity('OpportunityName1');  
         vOpportunity.Transformateur__c = vWst.id;
         
         //insert opportunity
         insert vOpportunity;
                 
         //link Waste_Statement to opportunity
          vWst.Opportunity__c = vOpportunity.Id;

         //call the method 
         Boolean vResult = TRANSFO_ManageEliminationController.sendWasteInstanceToElimination(vWst.ID);
        
        //check the result
        System.assertEquals(true, vResult);
          
        Test.stopTest();
        
    }
    
}