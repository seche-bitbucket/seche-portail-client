/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Tiphaine VIANA  <tiphaine.viana@capgemini.com>
* @version        1.0
* @created        2023-01-17
* @modified        
* @systemLayer    Class to enable to identify Pardot prospect to delete        
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes      
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public with sharing class Pardot_CleanProspects {

    public static void GetContactIds(Integer nbYear, String query) {

        List<Contact> contacts = Database.query(query);


        List <Id> contactsToDeleteIds = new List<Id>();
        List<Opportunity> oppToTest = new List<Opportunity>();
        Date threeYearsAgo = Date.today() - nbYear*365;


        for (Contact contact : contacts) {
            if (contact.opportunities !=null && contact.Opportunities.size()>0){
                oppToTest.add(contact.opportunities);
            }
        }


        for (Opportunity opp : oppToTest) {

            if (opp.pi_LastActivityDate__c < threeYearsAgo) {
                contactsToDeleteIds.add(opp.ContactId);
            }
        }


        List<Contact> filteredContacts = Database.query('SELECT Id, IsToBeDeletedOnPardot__c FROM Contact Where Id IN :contactsToDeleteIds');

        for (Contact contact : filteredContacts) {
            contact.IsToBeDeletedOnPardot__c = true;
        }

        update filteredContacts;

    }
}