@isTest (seeAllData=true)
public class OpportunityHierarchyStructureTest {
    
    static Opportunity oppHeader;
    static Opportunity oppInter1 = new Opportunity();
    static Opportunity oppInter2 = new Opportunity();
    static Opportunity oppSub1_1 = new Opportunity();
    static Opportunity oppSub1_2 = new Opportunity();
    
    static void init(){
        
        List<Opportunity> Opps = new List<Opportunity>();
        
        //Get a Record Type for Account
        RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
        
        //Create Account
        Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aucun', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
        
        try{
            insert ac;
        }catch(Exception e){
        }
        
        //Create Contact
        Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '03 00 00 00 00', Salesman__c = UserInfo.getUserId()  );
        
        try{
            insert contactTest;
        }catch(Exception e1){
        }
        
        //Get a Record Type for Opportunity
        
        Map<String, RecordType> RTs = new Map<String, RecordType>(); 
        for(RecordType rtOpp : [Select r.DeveloperName, r.Id From RecordType r  Where SobjectType='Opportunity'])
            RTs.put(rtOpp.DeveloperName, rtOpp);
        
        
        
        //Create Opportunity Header
        oppHeader = new Opportunity(Name = 'Opp_Header', RecordTypeId = RTs.get('OPP_OpportuniteMere').ID , AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
        //Opps.add(oppHeader);
        insert oppHeader;
        
        
        //Create Opportunity Inter 1
        oppInter1 = new Opportunity(Name = 'Opp_Inter1', RecordTypeId = RTs.get('OPP_06_AutreOffreSecteurPriveIntermediaire').ID , Opportunit_mere__c = oppHeader.ID ,AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
        Opps.add(oppInter1);
        
        //Create Opportunity Inter 1
        oppInter2 = new Opportunity(Name = 'Opp_Inter2', RecordTypeId = RTs.get('OPP_06_AutreOffreSecteurPriveIntermediaire').ID , Opportunit_mere__c = oppHeader.ID ,AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
        Opps.add(oppInter2);
        
        //Create Opportunity Sub 1 of Inter 1
        oppSub1_1 = new Opportunity(Name = 'Opp_Sub1', RecordTypeId = RTs.get('OPP_OpportuniteIntraGroupe').ID , OpportuniteMereIntra__c = oppInter1.ID ,AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
        Opps.add(oppSub1_1);
        
        //Create Opportunity Sub 2 of Inter 1
        oppSub1_2 = new Opportunity(Name = 'Opp_Sub2', RecordTypeId = RTs.get('OPP_OpportuniteIntraGroupe').ID , OpportuniteMereIntra__c = oppInter1.ID ,AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
        Opps.add(oppSub1_2);
        
        
        try{
            insert Opps;
        }catch(Exception e2){
            System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
        }                  
    }
    
    
    @isTest(seeAllData=true)
    static void TestOpportunityHeader(){
        
        init();
        //    Start TEST
        
        
        test.startTest();
        PageReference pageRef = Page.OpportunityHierarchy;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',oppHeader.id);
        ApexPages.StandardController sc = new ApexPages.standardController(oppHeader);
        OpportunityHierarchyStructure  controller = new OpportunityHierarchyStructure(sc);
        System.assertEquals(controller.OpportunityHeader.ID, oppHeader.id);
        
        test.stopTest();
        
        
    }
    
    @isTest(seeAllData=true)
    static void TestOpportunityInter(){
        
        init();
        //    Start TEST
        
        
        test.startTest();
        PageReference pageRef = Page.OpportunityHierarchy;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',oppInter1.id);
        ApexPages.StandardController sc = new ApexPages.standardController(oppInter1);
        OpportunityHierarchyStructure  controller = new OpportunityHierarchyStructure(sc);
        System.assertEquals(controller.OpportunityHeader.ID, oppHeader.id);
        
        test.stopTest();
        
        
    }
    
}