public with sharing class GUIDController {

    @InvocableMethod(label='Set GUID Generic' description='Set a GUID for any object' category='GUID')
    public static void genGuid(List<Id> recordIds) {
        if (!recordIds[0].getSobjectType().getDescribe().fields.getMap().containsKey('guid__c')){
            return;
        }
        List<SObject> objList = Database.query('SELECT GUID__c FROM '+recordIds[0].getSobjectType().getDescribe().getName()+' WHERE ID IN :recordIds');
        for (SObject obj : objList) {
            Blob b = Crypto.GenerateAESKey(128);
            String h = EncodingUtil.ConvertTohex(b);
            String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
            if (String.Isblank((String)obj.get('guid__c')) || String.IsEmpty((String)obj.get('guid__c'))) {
                obj.put('guid__c', guid); 
            }
        }
        update objList;
    }
}