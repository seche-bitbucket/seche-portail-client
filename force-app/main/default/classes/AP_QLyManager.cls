/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Baïla TOURE <baila.toure@capgemini.com>
 * @version        1.0
 * @created        2020-11-13
 * @systemLayer    Manager
 *
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Desciption: This class contains methods to handle the QLy requests)
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

 //"Questionnaire + [Nom du compte] + [Année]" ?
 
public without sharing class AP_QLyManager {

	@AuraEnabled(cacheable=true)
	public static List<User> fetchUserInfo(String userId){
		return [SELECT id, Name, City, Country, PostalCode, Address, Email, Phone, ContactId, Contact.Name, Contact.FirstName, Contact.LastName, Contact.Email, Contact.Phone, Account.Name, Account.QLY_SchoolCode__c,  Account.Phone, Account.SIRET_Number__c, Account.BillingAddress FROM user where Id = :userId];
	}

	@AuraEnabled(cacheable=true)
	public static List<QLY_SchoolForm__c > fetchSchoolServices(String accountId, String collection){
		system.debug('AP_QLyManager.fetchSchoolServices.AccountId ---start :'+accountId);
		system.debug('AP_QLyManager.fetchSchoolServices.collection ---start :'+collection);
		List<QLY_SchoolForm__c> lst = [SELECT Id, Name, CreatedDate, Account__c, RemovalAddress__c, Year__c, FormStatus__c, ApprovalStatus__c, SelectedStep__c, IsSameAddressForWaste__c, Contact__c, HasWasteTrackAccess__c, IsUserWasteTrackUser__c, WasteTrackFirstName__c, WasteTrackSIRET__c, WasteTrackLastName__c, WasteTrackEmail__c, WasteTrackSignCode__c, Truck19T__c, Truck7T__c, Truck3_5T__c, SelectedCollections__c, CollectionDays__c, Comments__c, FormFinishedDate__c,  WasteCollectionReferent__c, (Select Id, Name, Packaging__c, Product__c, ProxideName__c, PeroxideClassification__c, Product__r.Name, Product__r.QLY_DisplayType__c, Product__r.QLY_ServiceType__c,
		Product__r.QLY_ServiceIcon__c, Product__r.QLY_CLPIcon__c, Product__r.QLY_AvailablePackaging__c, Product__r.QLY_ADRIcon__c, Product__r.Features__c, Product__r.ADR__c , Product__r.Description, PackagingQuantity__c, Quantity__c, UnitPrice__c, OriginContenantService__c, OriginTreatmentService__c FROM QLY_Services__r ) FROM QLY_SchoolForm__c WHERE Collection__c = :collection AND Account__c = :accountId];
		return lst;

	}

	//method to get Customer user current campaign using the contactId
	@AuraEnabled(cacheable=true)
	public static List<CampaignMember > fetchUserCampaignInfo(String contactId){
		return [SELECT Id, ContactId, CampaignId, Status, Campaign.Name, Campaign.StartDate, Campaign.EndDate, Campaign.PriceBook__c, Campaign.Collection__c FROM CampaignMember WHERE ((Campaign.IsActive = true AND Campaign.StartDate <= TODAY AND Campaign.EndDate >= TODAY) OR (Status = 'Répondu')) AND ContactId = :contactId AND Campaign.Name LIKE 'QLY%' ORDER BY Campaign.StartDate DESC LIMIT 1];
	}

	// return whether a campaign is over or not
	@AuraEnabled
	public static Boolean isCampaignOver(String campaignId) {
		Campaign camp = [SELECT EndDate, StartDate FROM Campaign WHERE Id = :campaignId];
		if(Date.today() > camp.EndDate || Date.today() < camp.StartDate) {
			
			return true;
		}
		return false;
	}

	
	//method to get products related to the current campaign (by using the campagne priceBook Id)
	@AuraEnabled(cacheable=true)
	public static List<PricebookEntry > fetchCampaignProducts(String priceBookId){
		return [ SELECT ID, Name, UnitPrice, Product2Id, ProductCode, Product2.Name, Product2.QLY_ServiceType__c, Product2.QLY_ServiceIcon__c, Product2.QLY_CLPIcon__c, Product2.QLY_ADRIcon__c, Product2.QLY_DisplayType__c, Product2.Features__c, Product2.ADR__c , Product2.Description FROM PricebookEntry WHERE Product2.IsActive = true AND Pricebook2Id = :priceBookId];
	}

	//method to get products related to the current campaign (by using the campagne priceBook Id)
	@AuraEnabled(cacheable=true)
	public static Map<String, Double> getCampaignProductsPrices(String priceBookId){
		system.debug('AP_QLyManager.getCampaignProductsPrices.priceBookId ---start :'+priceBookId);
		Map<String, Double> pricesMap = new Map<String, Double>();
		List<PricebookEntry> prices = [ SELECT ID, Name, UnitPrice FROM PricebookEntry WHERE Pricebook2Id = :priceBookId];

		for(PricebookEntry e : prices){
			pricesMap.put(e.Name, e.UnitPrice);
		}

		system.debug('AP_QLyManager.getCampaignProductsPrices.pricesMap ---end :'+pricesMap);
		return pricesMap;
	}

	@AuraEnabled	
	public static ID createForm(String form, String status) {
		Map<String, Object> formMap = (Map<String, Object>) JSON.deserializeUntyped(form);
		return createForm( formMap, status);	
	}
	

	@AuraEnabled
	public static String processSave(String form, String waste, String service, String container, String status){
		List<LwcQLyWrapper.Form> objList = new List<LwcQLyWrapper.Form>();
		//LwcQLyWrapper.Form objForm = (LwcQLyWrapper.Form) JSON.deserializeStrict(form, LwcQLyWrapper.Form.class);
		LwcQLyWrapper.Form objForm = new LwcQLyWrapper.Form();
		system.debug('AP_QLyManager.processSave.form ---start :'+form);
		Map<String, Object> formMap = (Map<String, Object>) JSON.deserializeUntyped(form);

		String companyId = (String) formMap.get('companyId');
		if(companyId != null && companyId != ''){
			updateCompanyFromForm(formMap, companyId);
		}

		String contactId = (String) formMap.get('contactId');
		if(contactId != null && contactId != ''){
			updateContactFromForm(formMap, contactId);
		}

		String formId = (String) formMap.get('formId');	
		if(formId == null){
			formId=  createForm(form, 'Brouillon');
		}

		//Update the form
		updateForm(form, status, formId);
		
		objForm.formId = formId;

		//JSONParser parserWaste = JSON.createParser(waste);
		JSONParser parserContainer = JSON.createParser(container);	

		//map product campaign prices
		Map <String, Double> mapProductPriceByName = getPoductPriceByName((String) formMap.get('priceBookId'));
		system.debug('AP_QLyManager.createForm.mapProductPriceByName : '+mapProductPriceByName);

		
		
		//create the selected wastes
		List<LwcQLyWrapper.Waste> wastes = (List<LwcQLyWrapper.Waste>)JSON.deserialize(waste, List<LwcQLyWrapper.Waste>.class);
		createWaste(formId, wastes, mapProductPriceByName);

		//Create selected services
		List<LwcQLyWrapper.Service> services = (List<LwcQLyWrapper.Service>) JSON.deserialize(service, List<LwcQLyWrapper.Service>.class);
		objForm.services = createService(formId,services);

		//Create selected Containers
		List<LwcQLyWrapper.Container> containers = (List<LwcQLyWrapper.Container>) JSON.deserialize(container, List<LwcQLyWrapper.Container>.class);
		List<Map<ID,QLY_Service__c>> result = createContainer(formId, containers, mapProductPriceByName);
		//createContainer(ID formId, ID parentId, List<LwcQLyWrapper.Container> containers);
		
		objList.add(objForm);

		// deleteChildContainerIfAddContainerIsFalse(wastes, containers);

		system.debug('AP_QLyManager.processSave.objList : '+objList);

		if(status == 'Terminé'){

		}
		
		return JSON.serialize(objList);
	}

	@AuraEnabled
	public static void processDelete(List<String> ids){
		List<LwcQLyWrapper.Form> objList = new List<LwcQLyWrapper.Form>();
		system.debug('AP_QLyManager.processDelete.ids ---start :'+ids);
		List<QLY_Service__c> servicesToDelete = [SELECT Id FROM QLY_Service__c WHERE Id IN:ids OR OriginContenantService__c in :ids OR OriginTreatmentService__c in :ids];
		try {
			delete servicesToDelete;
			
		} catch (DmlException e) {
			for (Integer i = 0; i < e.getNumDml(); i++) {
				// Process exception here
				System.debug(e.getDmlMessage(i)); 
			}
		}
		

	}

	//Method to fetch available packaging
	@AuraEnabled
	public static List<Product2> getAvailablePackaging(){
		System.debug([SELECT ID, QLY_AvailablePackaging__c FROM Product2 WHERE QLY_AvailablePackaging__c != NULL ]);
		return [SELECT ID, QLY_AvailablePackaging__c FROM Product2 WHERE QLY_AvailablePackaging__c != NULL];
	}

	//method to update CampaignMember Status to 'Répondu' when status is 'Terminé'
	@AuraEnabled
	public static void processCampaignMemberStatus(String campaignMemberId){
		CampaignMember campaignMember = [SELECT Id, Status FROM CampaignMember WHERE Id = :campaignMemberId LIMIT 1];
		campaignMember.Status = 'Répondu';
		update campaignMember;	
	}

	@AuraEnabled(cacheable=true)
		public static List < Map < String, String >> getPiklistValues(String objApiName, String fieldName) {
			return QLY_Utils.getPiklistValues(objApiName, fieldName);
		} 
		
	@AuraEnabled	
	public static ContentVersion uploadFile(String base64, String filename, String productId) {
		return  QLY_Utils.uploadFile(base64, filename, productId);
	}
	
	@AuraEnabled	
	public static ContentVersion uploadFileWithRecordId(String base64, String filename, String recordId, String productId) {
		return  QLY_Utils.uploadFileToRecord(base64, filename, recordId, productId);
	}

		//methode to get all campaign product price from campaign priceBook
	private static  Map <String, Double> getPoductPriceByName(String priceBookId) {

		List<PricebookEntry > entry =  fetchCampaignProducts(priceBookId);	
		Map <String,Double> mapPrices = new Map <String, Double>();	

		for(PricebookEntry  e : fetchCampaignProducts(priceBookId)){
			mapPrices.put(e.Name, e.UnitPrice);
		}
		return mapPrices;
		} 


	private static ID getProductIdByName(String name){
		system.debug('AP_QLyManager.getProductIdByName ---start : param Name : '+name);
		return  [ SELECT  Name FROM Product2 WHERE Name = : name].Id;
	}


	private static ID 
	updateForm(String pForm, String status, ID formId){
		system.debug('AP_QLyManager.updateForm ---start : param formMap : '+pform+ 'param status: '+status);
		Map<String, Object> formMap = (Map<String, Object>) JSON.deserializeUntyped(pForm);

		QLY_SchoolForm__c form = new QLY_SchoolForm__c();
		
		//get selected days
		List<String> collectionDays = new List<String>();
		String mondayMorning = (String) formMap.get('mondayMorning');
		if(!String.isBlank(mondayMorning)){
			collectionDays.add(mondayMorning);
		}
		String mondayAfternoon = (String) formMap.get('mondayAfternoon');
		if(!String.isBlank(mondayAfternoon)){
			collectionDays.add(mondayAfternoon);
		}
		String tuesdayMorning = (String) formMap.get('tuesdayMorning');
		if(!String.isBlank(tuesdayMorning)){
			collectionDays.add(tuesdayMorning);
		}
		String tuesdayAfternoon = (String) formMap.get('tuesdayAfternoon');
		if(!String.isBlank(tuesdayAfternoon)){
			collectionDays.add(tuesdayAfternoon);
		}
		String wednesdayMorning = (String) formMap.get('wednesdayMorning');
		if(!String.isBlank(wednesdayMorning)){
			collectionDays.add(wednesdayMorning);
		}
		String wednesdayAfternoon = (String) formMap.get('wednesdayAfternoon');
		if(!String.isBlank(wednesdayAfternoon)){
			collectionDays.add(wednesdayAfternoon);
		}
		String thursdayMorning = (String) formMap.get('thursdayMorning');
		if(!String.isBlank(thursdayMorning)){
			collectionDays.add(thursdayMorning);
		}	
		String thursdayAfternoon = (String) formMap.get('thursdayAfternoon');
		if(!String.isBlank(thursdayAfternoon)){
			collectionDays.add(thursdayAfternoon);
		}
		String fridayMorning = (String) formMap.get('fridayMorning');
		if(!String.isBlank(fridayMorning)){
			collectionDays.add(fridayMorning);
		}
		String fridayAfternoon = (String) formMap.get('fridayAfternoon');
		if(!String.isBlank(fridayAfternoon)){
			collectionDays.add(fridayAfternoon);
		}
		system.debug('AP_QLyManager.updateForm.form.collectionDays ---start :'+collectionDays);

		//get the collect periods
		List<String> SelectedCollections = new List<String>{(String) formMap.get('firstCollect'), (String) formMap.get('secondCollect')};
		system.debug('AP_QLyManager.updateForm.form.SelectedCollections ---start :'+SelectedCollections);

		Boolean isSameAddressForWaste = (Boolean) formMap.get('isSameAddressForWaste');
		
		form.Id = formId;

		if(isSameAddressForWaste) {
			form.RemovalAddress__c = (String) formMap.get('companyAddressText');
		} else {
			form.RemovalAddress__c = (String) formMap.get('removalAddressText');	
		}
		/** update Collection__c  
		AcountId = (String) formMap.get('Id');
		collection = [ SELECT  Collection__c  FROM Campaign  WHERE CreatedBy.Id =: AcountId];
		system.debug('collection:'+collection);*/

		form.FormStatus__c = status;	
		form.Truck19T__c =  (Boolean) formMap.get('access19TCheckBox') != null ? (Boolean) formMap.get('access19TCheckBox') : false;
		form.Truck7T__c = (Boolean) formMap.get('access7TCheckBox') != null ? (Boolean) formMap.get('access7TCheckBox') : false;
		form.Truck3_5T__c = (Boolean) formMap.get('access3TCheckBox') != null ? (Boolean) formMap.get('access3TCheckBox') : false;
		form.CollectionDays__c = format((collectionDays));
		form.SelectedCollections__c = format(SelectedCollections);
		form.Collection__c = (String) formMap.get('firstCollect') ;
		form.Comments__c = (String) formMap.get('comment');	
		form.SelectedStep__c = (String) formMap.get('selectedStep');
		form.IsSameAddressForWaste__c = (Boolean) formMap.get('isSameAddressForWaste');
		form.WasteCollectionReferent__c =  (String) formMap.get('referentCollect');
		form.FormFinishedDate__c = Datetime.now();
		form.HasWasteTrackAccess__c = (String) formMap.get('HasWasteTrackAccess__c');
		form.WasteTrackFirstName__c = (formMap.containsKey('WasteTrackFirstName__c')) ? (String) formMap.get('WasteTrackFirstName__c') : '';
		form.WasteTrackLastName__c = (formMap.containsKey('WasteTrackLastName__c')) ? (String) formMap.get('WasteTrackLastName__c') : '';
		form.WasteTrackEmail__c = (formMap.containsKey('WasteTrackEmail__c')) ? (String) formMap.get('WasteTrackEmail__c') : '';
		form.WasteTrackSIRET__c = (formMap.containsKey('WasteTrackSIRET__c')) ? (String) formMap.get('WasteTrackSIRET__c') : '';
		form.IsUserWasteTrackUser__c = (formMap.containsKey('IsUserWasteTrackUser__c')) ? (String) formMap.get('IsUserWasteTrackUser__c') : 'Non';
		form.WasteTrackSignCode__c = (formMap.containsKey('WasteTrackSignCode__c')) ? (String) formMap.get('WasteTrackSignCode__c') : '';
		
		upsert form;

		system.debug('AP_QLyManager.updateForm ---end :'+form);
		
		return form.Id;
	}

	//Method to update the company from the form
	public static void updateCompanyFromForm(Map<String, Object> formMap, String companyId){

		Account accToUpdate = new Account();
		accToUpdate.Id = (ID) companyId;
		
		Map<String, Object> companyAddressMap = (Map<String, Object>) formMap.get('companyAddress');
		if(companyAddressMap != null){
			accToUpdate.BillingStreet  = (String) companyAddressMap.get('street');
			accToUpdate.BillingCity  = (String) companyAddressMap.get('city');
			accToUpdate.BillingPostalCode  = (String) companyAddressMap.get('postalCode');
			accToUpdate.BillingState  = (String) companyAddressMap.get('province');
			accToUpdate.BillingCountry  = (String) companyAddressMap.get('country');
		}

		accToUpdate.Phone = (String) formMap.get('companyPhone');

		update accToUpdate;
	}

	//Method to update the contact from the form
	public static void updateContactFromForm(Map<String, Object> formMap, String contactId){
		Contact contactToUpdate = new Contact();
		contactToUpdate.Id = (Id) contactId;
		contactToUpdate.Phone = (String) formMap.get('phone');

		update contactToUpdate;
	}

	//Method to create the School form
	public static ID createForm(Map<String, Object> formMap, String status){

		system.debug('AP_QLyManager.createForm ---start : param formMap : '+formMap+ 'param status: '+status);
		System.debug('AP_QLyManager.createForm ---start : param formMap firstCollect : '+ formMap.get('firstCollect'));
		System.debug('AP_QLyManager.createForm ---start : param formMap secondCollect : '+ formMap.get('secondCollect'));

		QLY_SchoolForm__c form = new QLY_SchoolForm__c();
		
		//get selected days
		List<String> collectionDays = new List<String>();
		String mondayMorning = (String) formMap.get('mondayMorning');
		if(!String.isBlank(mondayMorning)){
			collectionDays.add(mondayMorning);
		}
		String mondayAfternoon = (String) formMap.get('mondayAfternoon');
		if(!String.isBlank(mondayAfternoon)){
			collectionDays.add(mondayAfternoon);
		}
		String tuesdayMorning = (String) formMap.get('tuesdayMorning');
		if(!String.isBlank(tuesdayMorning)){
			collectionDays.add(tuesdayMorning);
		}
		String tuesdayAfternoon = (String) formMap.get('tuesdayAfternoon');
		if(!String.isBlank(tuesdayAfternoon)){
			collectionDays.add(tuesdayAfternoon);
		}
		String wednesdayMorning = (String) formMap.get('wednesdayMorning');
		if(!String.isBlank(wednesdayMorning)){
			collectionDays.add(wednesdayMorning);
		}
		String wednesdayAfternoon = (String) formMap.get('wednesdayAfternoon');
		if(!String.isBlank(wednesdayAfternoon)){
			collectionDays.add(wednesdayAfternoon);
		}
		String thursdayMorning = (String) formMap.get('thursdayMorning');
		if(!String.isBlank(thursdayMorning)){
			collectionDays.add(thursdayMorning);
		}	
		String thursdayAfternoon = (String) formMap.get('thursdayAfternoon');
		if(!String.isBlank(thursdayAfternoon)){
			collectionDays.add(thursdayAfternoon);
		}
		String fridayMorning = (String) formMap.get('fridayMorning');
		if(!String.isBlank(fridayMorning)){
			collectionDays.add(fridayMorning);
		}
		String fridayAfternoon = (String) formMap.get('fridayAfternoon');
		if(!String.isBlank(fridayAfternoon)){
			collectionDays.add(fridayAfternoon);
		}
		system.debug('AP_QLyManager.createForm.form.collectionDays ---start :'+collectionDays);

		//get the collect periods
		List<String> SelectedCollections = new List<String>{(String) formMap.get('firstCollect'), (String) formMap.get('secondCollect')};
		system.debug('AP_QLyManager.createForm.form.SelectedCollections ---start :'+SelectedCollections);
		
		form.Name = QLyConstants.PREFIX_NAME+' - '+formMap.get('schoolCode')+' - '+QLyConstants.CURRENT_YEAR;
		form.RemovalAddress__c = (String) formMap.get('removalAddressText');
		form.Year__c = QLyConstants.CURRENT_YEAR;
		form.Account__c = (String) formMap.get('companyId');
		form.FormStatus__c = status;
		form.Contact__c = (String) formMap.get('contactId');
		form.SchoolName__c = (String) formMap.get('contactName');
		form.Truck19T__c =  (Boolean) formMap.get('access19TCheckBox') != null ? (Boolean) formMap.get('access19TCheckBox') : false;
		form.Truck7T__c = (Boolean) formMap.get('access7TCheckBox') != null ? (Boolean) formMap.get('access7TCheckBox') : false;
		form.Truck3_5T__c = (Boolean) formMap.get('access3TCheckBox') != null ? (Boolean) formMap.get('access3TCheckBox') : false;
		form.CollectionDays__c = format((collectionDays));
		form.SelectedCollections__c = format(SelectedCollections);
		form.Comments__c = (String) formMap.get('comment');
		form.Account__c = (String) formMap.get('companyId');
		form.Contact__c = (String) formMap.get('contactId');
		form.FormFinishedDate__c  =  Datetime.now();
		form.SelectedStep__c = (String) formMap.get('selectedStep');
		form.IsSameAddressForWaste__c = (Boolean) formMap.get('isSameAddressForWaste');
		form.HasWasteTrackAccess__c = (String) formMap.get('HasWasteTrackAccess__c');
		form.WasteTrackFirstName__c = (formMap.containsKey('WasteTrackFirstName__c')) ? (String) formMap.get('WasteTrackFirstName__c') : '';
		form.WasteTrackLastName__c = (formMap.containsKey('WasteTrackLastName__c')) ? (String) formMap.get('WasteTrackLastName__c') : '';
		form.WasteTrackEmail__c = (formMap.containsKey('WasteTrackEmail__c')) ? (String) formMap.get('WasteTrackEmail__c') : '';
		form.WasteTrackSIRET__c = (formMap.containsKey('WasteTrackSIRET__c')) ? (String) formMap.get('WasteTrackSIRET__c') : '';
		form.IsUserWasteTrackUser__c = (formMap.containsKey('IsUserWasteTrackUser__c')) ? (String) formMap.get('IsUserWasteTrackUser__c') : 'Non';
		form.WasteTrackSignCode__c = (formMap.containsKey('WasteTrackSignCode__c')) ? (String) formMap.get('WasteTrackSignCode__c') : '';
		

		insert form;

		system.debug('AP_QLyManager.createForm ---end :'+form);
		
		return form.Id;
	}

	//Method to create the School form
	public static void createWaste(ID formId, List<LwcQLyWrapper.Waste> wastes, Map <String, Double> mapProductPriceByName){
		system.debug('AP_QLyManager.createWaste ---start : param formId : '+formId+ 'param wastes : '+wastes);

		List<QLY_Service__c>  listToCreate = new List<QLY_Service__c>();	
		List<LwcQLyWrapper.WasteItem>  listAllWasteRows = new List<LwcQLyWrapper.WasteItem>();
		//To save index of wastes these need to create container related
		Set<Integer> wasteIndexRelatedToContainer = new Set<Integer>()	;

		//map the waste number container value
		map<Integer, Integer> mapNumberContainerByWasteIndex = new Map<Integer, Integer>();

		Integer iter = 0;
		for(LwcQLyWrapper.Waste w : wastes){
			for(LwcQLyWrapper.WasteItem waste : w.rows){
				system.debug('AP_QLyManager.createWaste.wast.Indexs :  '+waste.index);
				QLY_Service__c wast = new QLY_Service__c();
				if(!String.isBlank(waste.wasteId)){//update row
					wast.Id = waste.wasteId;
					wast.Quantity__c = getQuantity(waste.displayStyle, waste);
					wast.Packaging__c =   waste.displayStyle == QLyConstants.NORMAL ? waste.packaging : '';
					wast.PackagingQuantity__c = (waste.displayStyle == QLyConstants.NORMAL && !String.isBlank(waste.nbrPackaging)) ? Integer.valueOf(waste.nbrPackaging) : 0;
					wast.ProxideName__c = waste.peroxyName;
					wast.PeroxideClassification__c = waste.classification;
				}else{
					wast.QLY_SchoolForm__c = formId;
					wast.Product__c = w.productId;
					wast.Quantity__c = getQuantity(waste.displayStyle, waste);
					wast.UnitPrice__c = waste.unitPrice;
					wast.Packaging__c =   waste.displayStyle == QLyConstants.NORMAL ? waste.packaging : '';
					//Packaging is available only for the normal display
					wast.PackagingQuantity__c = (waste.displayStyle == QLyConstants.NORMAL && !String.isBlank(waste.nbrPackaging)) ? Integer.valueOf(waste.nbrPackaging) : 0;
					wast.ProxideName__c = waste.peroxyName;
					wast.PeroxideClassification__c = waste.classification;
				}
				
			
				//check if the waste have related container
				// if(waste.addContainer && waste.displayStyle == QLyConstants.NORMAL){// we create a related container
				// 	wasteIndexRelatedToContainer.add(iter);
				// 	mapNumberContainerByWasteIndex.put(iter, getContainerQty(waste, null));				
				// } 
				listToCreate.add(wast);
				iter++;

				listAllWasteRows.add(waste);
			}	
			
		}

		upsert listToCreate;
		system.debug('AP_QLyManager.createWaste.wastes :  '+listToCreate);

		//create related containers
		List<QLY_Service__c>  relatedContainers =  createRelatedContainers(wasteIndexRelatedToContainer, listToCreate, mapNumberContainerByWasteIndex, 'waste', listAllWasteRows, null, mapProductPriceByName, formId);
		upsert relatedContainers;

		system.debug('AP_QLyManager.createWaste.relatedContainers --- :  '+relatedContainers);

		system.debug('AP_QLyManager.createWaste.wastes --- end ');

	}

	//Method to create the School form
	public static List<LwcQLyWrapper.Service> createService(ID formId, List<LwcQLyWrapper.Service> services){
		system.debug('AP_QLyManager.createService ---start : param formId : '+formId+ 'param services: '+services);
		List<QLY_Service__c>  listToCreate = new List<QLY_Service__c>();
		List<LwcQLyWrapper.Service> formServices = new List<LwcQLyWrapper.Service>();
		
		for(LwcQLyWrapper.Service s : services ){
			QLY_Service__c serv = new QLY_Service__c();
			if(!String.isBlank(s.Id)){//update row
				serv.Id = s.Id.split('-')[0];
				serv.Quantity__c = 1;	
				formServices.add(s);
			}else{
				serv.QLY_SchoolForm__c = formId;
				serv.Product__c = s.productId;
				serv.UnitPrice__c =  s.unitPrice;	
				serv.Quantity__c = 1;	
				listToCreate.add(serv);
			}
			
		}
		system.debug('AP_QLyManager.createService.listToCreate --- Before insert :  '+listToCreate);
		upsert listToCreate;
		system.debug('AP_QLyManager.createService.listToCreate --- end :  '+listToCreate);

		for(QLY_Service__c serv : listToCreate){
			for(LwcQLyWrapper.Service s : services){
				if(serv.Product__c == s.productId){
					s.Id = serv.Id;
				}
			}  
		}

		return formServices;
	}


	//Method to create containers
	public static  List<Map<ID,QLY_Service__c>> createContainer(ID formId, List<LwcQLyWrapper.Container> containers, Map <String, Double> mapProductPriceByName){
		system.debug('AP_QLyManager.createContainer ---start : param formId : '+formId+ 'param containers: '+containers);
		List<QLY_Service__c>  listToCreate = new List<QLY_Service__c>();
		List<Map<ID,QLY_Service__c>> mapContianerToRealtedContainer = new List<Map<ID,QLY_Service__c>>();
		List<LwcQLyWrapper.Container>  listAllContainerRows = new List<LwcQLyWrapper.Container>();
		//map the container number container value
		map<Integer, Integer> mapNumberContainerByContainerIndex = new Map<Integer, Integer>();

		//To save index of containers these need to create container related
		Set<Integer> containerIndexRealatedToAContainer = new Set<Integer>();
		
		Integer iter = 0;
		for(LwcQLyWrapper.Container c : containers ){
			QLY_Service__c cont = new QLY_Service__c();
			if(!String.isBlank(c.Id)){//update row
				cont.Id = c.Id;
				cont.Quantity__c = !String.isBlank(c.numberContainer) ? Integer.valueOf(c.numberContainer) : 1;	
			}else{
				cont.QLY_SchoolForm__c = formId;
				cont.Product__c = c.productId;
				cont.Quantity__c = !String.isBlank(c.numberContainer) ? Integer.valueOf(c.numberContainer) : 1;		
				cont.UnitPrice__c = c.unitPrice;
			}
			

			listToCreate.add(cont);	

			//check if the waste have related container
			if(c.addContainer){// we create a related container
				containerIndexRealatedToAContainer.add(iter);
				mapNumberContainerByContainerIndex.put(iter, getContainerQty(null, c));				
			}
			iter++;
			listAllContainerRows.add(c);
			}

		upsert listToCreate;

		//create related containers
		List<QLY_Service__c>  relatedContainers =  createRelatedContainers(containerIndexRealatedToAContainer, listToCreate, mapNumberContainerByContainerIndex, 'container', null, listAllContainerRows, mapProductPriceByName, formID);
		upsert relatedContainers;

		system.debug('AP_QLyManager.createContainer.listToCreate --- end :  '+listToCreate);
		return null;
		

	}

	//Method to create containers related to waste/other container
	public static  List<QLY_Service__c> createRelatedContainers(Set<Integer> parentIndexRealatedToContainer, List<QLY_Service__c>  parentObjectList, Map<Integer, Integer> mapNumberContainer, String parentType, List<LwcQLyWrapper.WasteItem> rows, List<LwcQLyWrapper.Container> containers, Map <String, Double> mapProductPriceByName, ID formID){
		system.debug('AP_QLyManager.createRelatedContainers ---start : param parentIndexRealatedToContainer : '+parentIndexRealatedToContainer+ 'param parentObjectList: '+parentObjectList +' param parentType: '+parentType);
		List<QLY_Service__c>  reletedContainerList = new List<QLY_Service__c>();

		//create related containers
		for(Integer index : parentIndexRealatedToContainer){
			QLY_Service__c relatedContainer = new QLY_Service__c();
			relatedContainer.QLY_SchoolForm__c = formId;			
			if(parentType == 'waste'){//
				if(!String.isBlank(rows[index].containerId)){//update row
					relatedContainer.Id = (rows[index].containerId);
				}
				relatedContainer.Product__c =  getProductIdByName(rows[index].packaging);
				relatedContainer.OriginTreatmentService__c = parentObjectList[index].Id;		
				relatedContainer.Quantity__c = mapNumberContainer.get(index);
				relatedContainer.CreationType__c = 'Contenant d\'un Traitement de déchets';
				relatedContainer.UnitPrice__c = mapProductPriceByName.get(rows[index].packaging);
			}else if(parentType == 'container'){
				if(!String.isBlank(containers[index].containerId)){//update row
					relatedContainer.Id = (containers[index].containerId);
				}
				relatedContainer.Product__c = getProductIdByName(QLyConstants.BOUCHON_DEGAGEUR);
				relatedContainer.OriginContenantService__c =  parentObjectList[index].Id;
				relatedContainer.Quantity__c =  mapNumberContainer.get(index);
				relatedContainer.CreationType__c = 'Contenant supplémentaire';
				relatedContainer.UnitPrice__c = mapProductPriceByName.get(QLyConstants.BOUCHON_DEGAGEUR);
			}	
			
			reletedContainerList.add(relatedContainer);	
		}		
		

		system.debug('AP_QLyManager.createRelatedContainers.reletedContainerList --- end :  '+reletedContainerList);
		return reletedContainerList;
		

	}

	public static void deleteChildContainerIfAddContainerIsFalse(List<LwcQLyWrapper.Waste> wastes, List<LwcQLyWrapper.Container> containers){
		
		List<ID> parentServicesIds = new List<ID>();
		for (LwcQLyWrapper.Waste waste : wastes){
			for(LwcQLyWrapper.WasteItem wasteItem : waste.rows){
				if(!wasteItem.addContainer && !String.isBlank(wasteItem.wasteId)){
					parentServicesIds.add(wasteItem.wasteId);
				}
			}
		}
		for(LwcQLyWrapper.Container container : containers){
			if(!container.addContainer && !String.isBlank(container.id)){
				parentServicesIds.add(container.id);
			}
		}
		List<QLY_Service__c> childServicesToDelete = [SELECT Id FROM QLY_Service__c WHERE OriginContenantService__c in :parentServicesIds OR OriginTreatmentService__c in :parentServicesIds];
		delete childServicesToDelete;
	}


	//Method to get the waste quantity input by the display row style in the form
	private static Integer getQuantity (String displayStyle, LwcQLyWrapper.WasteItem wasteItem) {
		Integer quantity = 0;
		
		if(displayStyle == QLyConstants.NORMAL && !String.isBlank(wasteItem.quantity)){
			quantity = Integer.valueOf(wasteItem.quantity);
		}else if (displayStyle == QLyConstants.QUANTITY_PHOTO && !String.isBlank(wasteItem.photoQuantity)){
			quantity = Integer.valueOf(wasteItem.photoQuantity);
		}else if (displayStyle == QLyConstants.QUANTITY_LIST_PHOTO && !String.isBlank(wasteItem.photoListQuantity)){
			quantity = Integer.valueOf(wasteItem.photoListQuantity);
		}else if (displayStyle == QLyConstants.PEROXYDE_CLASSIFICATION_QUANTITY_PHOTO && !String.isBlank(wasteItem.peroxydeQuantity)){
			quantity = Integer.valueOf(wasteItem.peroxydeQuantity);
		}
	
		return quantity;
	}

	//Method to get the container quantity input from waste/container
	private static Integer getContainerQty (LwcQLyWrapper.WasteItem wasteItem, LwcQLyWrapper.Container container) {
		Integer quantity = 0;
		if(wasteItem != null && !String.isBlank(wasteItem.numbContainer)){
			quantity = Integer.valueOf(wasteItem.numbContainer);
		}else if (container != null && !String.isBlank(container.numberPlug)){
			quantity = Integer.valueOf(container.numberPlug);
		}  
		return quantity;
	}

	//use to set multipickList values
	private static String format(List<String> values) {
		if (values == null) return null;
		return String.join(values, ';');
	}

	//retrieve all document link to a form's ID
	// Author: Amine IDRISSI
	// Date: 11/01/2021
	@AuraEnabled(cacheable=true)
	public static List<ContentDocument> getContentDocumentFromForm(String formID, Boolean isSummary){
		
		List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink>();
		List<ContentDocument> contentDocuments = new List<ContentDocument>();
		List<String> lstContentDocIDs = new List<String>();

		if(formID != null){

			contentDocumentLinks = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: formID];

			system.debug(contentDocumentLinks);

			if(contentDocumentLinks != null && contentDocumentLinks.size() > 0)
			{
				for (ContentDocumentLink contentdoc: contentDocumentLinks)
				{
					lstContentDocIDs.add(contentdoc.ContentDocumentID);
				}// end for 
			}// end if contentDocumentLinks

			if(lstContentDocIDs != null && lstContentDocIDs.size() > 0)
			{
				if(isSummary){
					contentDocuments = [SELECT ID, Title, FileExtension FROM ContentDocument WHERE ID IN: lstContentDocIDs AND Title LIKE 'Synthèse - %'];
				} else {
					contentDocuments = [SELECT ID, Title, FileExtension FROM ContentDocument WHERE ID IN: lstContentDocIDs AND (NOT Title LIKE 'Synthèse - %') AND (NOT Title LIKE '%QLY_%')];
				}
			}// end if lstContentDocIDs

			return contentDocuments;
		} else {
			return null;
		}
		
	}

	@AuraEnabled
	public static List<ContentVersion> getContentVersionFromForm(String formID, Boolean isSummary){

		List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink>();
		List<ContentVersion> contentVersions = new List<ContentVersion>();
		List<String> lstContentDocIDs = new List<String>();

		if(formID != null){

			contentDocumentLinks = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: formID];

			if(contentDocumentLinks != null && contentDocumentLinks.size() > 0) {

				for (ContentDocumentLink contentdoc: contentDocumentLinks)
				{
					lstContentDocIDs.add(contentdoc.ContentDocumentID);
				}// end for 

				if(isSummary) {
					contentVersions = [SELECT ContentDocumentId, FileExtension, QLY_Product2__c, Title FROM ContentVersion WHERE (ContentDocumentId IN :lstContentDocIDs) AND (Title LIKE 'Synthèse - %')];
				} else {
					contentVersions = [SELECT ContentDocumentId, FileExtension, QLY_Product2__c, Title FROM ContentVersion WHERE (ContentDocumentId IN :lstContentDocIDs) AND (NOT Title LIKE 'Synthèse - %')  AND (NOT Title LIKE '%QLY_%')];
				}

			}

		}

		return contentVersions;

	}

	// Change document title for better identification
	// Author: Amine IDRISSI
	// Date: 11/01/2021
	@AuraEnabled
	public static List<ContentDocument> prefixSummary(List<String> documentIds){
		
		List<ContentDocument> documents = new List<ContentDocument>();
		documents = [SELECT ID, Title, FileExtension FROM ContentDocument WHERE ID IN: documentIds];
		System.debug('documents: '+documents);

		if(documents != null && documents.size() > 0)
		{
			for(ContentDocument doc: documents)
			{
				doc.Title = 'Synthèse - '+doc.Title;
			}// end for doc
		}// end if documents

		System.debug('documents: '+documents);
		update documents;
		return documents;

	}

	// Suppression des ContentDocuments by ID
	// Author: Amine IDRISSI
	// Date: 11/01/2021
	@AuraEnabled
	public static Boolean deleteContentDocument(String docId){
		
		ContentDocument doc = [SELECT ID FROM ContentDocument WHERE ID =: docId];

		try {
			delete doc;
			return true;
		}
		catch(exception e)
		{
			System.debug(e.getMessage());
			return false;
		}

	}

	@AuraEnabled
	public static ContentDocument getDocFromVersion(String versionId){
			ContentVersion docVer = [SELECT ContentDocumentId FROM ContentVersion WHERE ID = :versionId];
			ContentDocument doc = [SELECT ID FROM ContentDocument WHERE ID = :docVer.ContentDocumentId];
			return doc;
	}

	@AuraEnabled
	public static Boolean deleteContentDocuments(List<String> docIds){
		
		List<ContentDocument> docs = [SELECT ID FROM ContentDocument WHERE ID IN: docIds];

		try {
			delete docs;
			return true;
		}
		catch(exception e)
		{
			System.debug(e.getMessage());
			return false;
		}

	}

	// Lier les ContentDocument avec le questionnaire
	// Author: Amine IDRISSI
	// Date: 13/01/2021
	@AuraEnabled
	public static List<ContentDocumentLink> linkDocToForm(List<String> listDocIds, String formId){
		
		List<ContentDocumentLink> lstLink = new List<ContentDocumentLink>();
		List<ContentDocument> lstDocuments = new List<ContentDocument>();

		if(listDocIds != null && listDocIds.size() > 0)
		{
			lstDocuments = [SELECT ID FROM ContentDocument WHERE ID IN: listDocIds];

			if(lstDocuments != null && lstDocuments.size() > 0)
			{
				for(ContentDocument doc: lstDocuments)
				{
					lstLink.add(new ContentDocumentLink(ContentDocumentId = doc.Id,
														LinkedEntityId = formId));
				}

				upsert lstLink;
			}
		}

		return lstLink;
		
	}

	@AuraEnabled
	public static List<QLY_SchoolForm__c> getApprovStatus(String accountId)
	{
		system.debug('AP_QLyManager.getApprovStatus ---start :'+accountId);
		AutomationInputs__c ai = AutomationInputs__c.getOrgDefaults();
		for(QLY_SchoolForm__c form: [SELECT ApprovalStatus__c, Collection__c FROM QLY_SchoolForm__c WHERE Account__c = : accountId ORDER BY CreatedDate DESC]) {
			if(form.Collection__c.contains(ai.QLYCollection__c)) {
				return new List<QLY_SchoolForm__c>{form};
			}
		}

		return new List<QLY_SchoolForm__c>{};
	}

	@AuraEnabled
	public static string cancelForm(String jsonSobject){
		try {
			SObject obj = (SObject)JSON.deserialize(jsonSobject, QLY_SchoolForm__c.class);
			update obj;
			return 'OK';
		} catch (Exception e) {
			System.debug(e.getMessage());
			return 'NOK';
		}
	}
}