@isTest
private class Test_BatchProductInfoToOLI {
    @isTest static void testBatchExecute(){
        Profile p = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];
        
        String testemail = 'Commercial-_User_test@test.com';
        User Commercial = new User(profileId = p.id, username = testemail, email = testemail,
                                   emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                   languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                   alias='cs', lastname='lastname2', IsActive=true);
        insert Commercial;
        Account ac = new Account();
        ac.Name = 'testAccName';
        ac.CustomerNature__c = 'Administration';
        ac.Industry = 'Trader';
        ac.CurrencyIsoCode = 'EUR';
        ac.Producteur__c = false;
        ac.BillingPostalCode = '00000';
        ac.NAF_Number__c = '1234A';
        insert ac;
        Contact cont = new Contact(FirstName='Test',
                                   LastName='Test',
                                   Salesman__c = Commercial.ID,
                                   Accountid= ac.id);
        insert cont;
        Pricebook2 pb1 = new Pricebook2(Name='Pricebook1', isActive=true);
        insert pb1;
        //UserRecordAccess
        Product2 prd = new product2(name = 'Autre Traitement' ,TECH_ExternalID__c = '012345',Family = 'Traitement',ProductCode='TREAT-FHGF1');
        List<Product2> products=new List<Product2>();
        products.add(prd);
        insert products;
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = test.getStandardPricebookId(), Product2Id = prd.Id, UnitPrice = 100, IsActive = true, UseStandardPrice = false);
        List<PricebookEntry> standardPricebookEntries=new List<PricebookEntry>();
        standardPricebookEntries.add(standardPrice);
        insert standardPricebookEntries;
        PricebookEntry pbe = new PricebookEntry(UseStandardPrice = false,Product2Id=prd.Id,Pricebook2Id=pb1.Id,
                                                isActive=true, UnitPrice = 100);            
        List<PricebookEntry> PricebookEntries=new List<PricebookEntry>();
        PricebookEntries.add(pbe);
        insert PricebookEntries;
        PricebookEntry  traitement=[SELECT IsActive, UnitPrice, Product2Id, Pricebook2Id, Name, Id, UseStandardPrice FROM 
                                    PricebookEntry WHERE Name=:'Autre Traitement' AND  IsActive = true AND Pricebook2Id=:test.getStandardPricebookId()  LIMIT 1];
        Opportunity opp=new Opportunity(Name='Convention'+' 2019 - '+ac.Name,
                                        AccountId=ac.Id,CloseDate = date.today(),
                                        StageName ='Renouvellement',
                                        ContactName__c=cont.Id,Salesman__c=Commercial.ID,
                                        Filiale__c='Tredi Salaise 1');
        insert opp;
        OpportunityLineItem Opplineitem = new OpportunityLineItem (Quantity=2, OpportunityId=opp.Id,UnitPrice=0.01,TECH_CatalogueMatch__c='TREAT-FHGF1',PriceBookEntryID=traitement.Id);
        insert Opplineitem;
        Test.startTest();
        
        BatchProductInfoToOLI batch = new BatchProductInfoToOLI(); 
        Id batchId = Database.executeBatch(batch);
        Test.stopTest();
        
    }
}