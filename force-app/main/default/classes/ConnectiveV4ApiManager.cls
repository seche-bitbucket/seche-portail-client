/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ConnectiveV4ApiManager
* -- - Author : MOIDRISS
* -- - Company : Capgemini
* -- - Purpose : ConnectiveV4ApiManager
* --
* -- Maintenance History:
* --
* -- Date Name  Ver  Remarks
* -- ---------- ---  ---------------------------------------------------------------------------------------------
* -- 15-03-2021 1.0  Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
Global class ConnectiveV4ApiManager implements Database.AllowsCallouts,Database.Stateful {
    
    //___________________ALL CONNECTIVE HTTP CALLOUTS__________________\\

  //_____________________________________________________________________\\
 //___________________THIS METHOD CREATES AN ALL IN ONE___________________\\
    public static String createPackage(PackageV4Wrapper pkg, Id objId) {

        Boolean createPackage = true;
        if(objId.getSObjectType() == FIP_FIP__c.sObjectType){
            List<DocumentsForSignature__c> docs = new List<DocumentsForSignature__c>();
            docs = [SELECT Id, Status__c, FlowId__c, FIP_FIP__c FROM DocumentsForSignature__c WHERE FIP_FIP__c= :objId Limit 1];
            if(docs.isEmpty()!=True){
                createPackage = false;
            }
        }
        if(objId.getSObjectType() == Lead.sObjectType){
            List<DocumentsForSignature__c> docs = new List<DocumentsForSignature__c>();
            docs = [SELECT Id, Status__c, FlowId__c, Lead__c FROM DocumentsForSignature__c WHERE Lead__c= :objId Limit 1];
            if(docs.isEmpty()!=True){
                createPackage = false;
            }
        }
        if(objId.getSObjectType()==CAP__c.sObjectType){
            List<DocumentsForSignature__c> docs = new List<DocumentsForSignature__c>();
            docs = [SELECT Id, Status__c, FlowId__c, CAP__c FROM DocumentsForSignature__c WHERE CAP__c =: objId];
            if(!docs.isEmpty()){
                createPackage = false;
            }
        }

        if(createPackage) {
            Http http = new Http();
            HttpRequest req = new HttpRequest();

            req.setEndpoint('callout:ConnectiveApiV4/packages/');
            req.setHeader('Content-Type', 'application/json');
            req.setMethod('POST');
            req.setTimeout(120000);
            req.setBody(JSON.serialize(pkg));

            System.debug(JSON.serialize(pkg));

            try{
                HttpResponse res = http.send(req);
                System.debug(res.getStatusCode());

                if(res.getStatusCode() == 201) {
                    if(res.getBody() != null) {
                        ConnectiveV4ApiUtils.insertDocuments(res.getBody(), objId);
                    }
                    return 'OK';
                }else{
                    Log__c error = new Log__c(
                        ApexJob__c= ConnectiveV4ApiManager.class.getName(),
                        ErrorMessage__c='response :' + res.getBody(),
                        ExceptionType__c='HTTP'
                        );
                    insert error;
                    return 'KO';
                }
            }
            catch(Exception ex) {
                system.debug(ex.getMessage()+ex.getLineNumber());
                Log__c error = new Log__c(
                    ApexJob__c= ConnectiveV4ApiManager.class.getName(),
                    ErrorMessage__c=ex.getMessage()+ex.getLineNumber(),
                    ExceptionType__c='Error APEX'
                    );
                insert error;
                return 'KO';
            }
        } else {
            return 'KO';
        }
    }

    @future (callout=true)
    public static void createInstantPackageFIPTrigger(Id fipId){
        String result = '';
        result = createPackage(ConnectiveV4ApiUtils.createPackage(fipId, null), fipId);
    }

    //____________________________________________________________\\
    //___________________GET PACKAGE STATUS BY ID___________________\\
    public static String getStatusByConnectivePackageId(String connectiveIdPackage){

        String resultStatus;

        HttpResponse res = new HttpResponse();
        HttpRequest req = new HttpRequest();
        Http http = new Http();

        req.setHeader('Content-Type','application/json');
        req.setEndpoint('callout:ConnectiveApiV4/packages/'+connectiveIdPackage);
        req.setMethod('GET');
        req.setTimeout(120000);

        try{
            res = http.send(req);
            if(res.getStatusCode() == 200){
                resultStatus =res.getBody();        
            }
            else{
                Log__c error = new Log__c(
                    ApexJob__c= ConnectiveV4ApiManager.class.getName(),
                    ErrorMessage__c='response :' + res.getBody(),
                    ExceptionType__c='HTTP'
                );
                insert error;
            }
        }
        catch(Exception ex){
            Log__c error = new Log__c(
                    ApexJob__c= ConnectiveV4ApiManager.class.getName(),
                    ErrorMessage__c=ex.getMessage() + ex.getLineNumber(),
                    ExceptionType__c='Error APEX'
                );
            insert error;
        }
        return resultStatus;
    }
    
    //_________________________________________________\\
    //___________________SEND REMINDER___________________\\
    public static String sendRedminder(Id objId) {
        try{
            DocumentsForSignature__c doc;
            Attestation__c prefip;
            Quote quote;
            Lead ld;
            CAP__c cap;

            if(objId.getSObjectType() == FIP_FIP__c.sObjectType){
                String recordTypeName = [SELECT DeveloperName FROM RecordType WHERE Id IN (SELECT RecordTypeId FROM FIP_FIP__c WHERE ID = :objId)].DeveloperName;
                if(recordTypeName == 'FIP_Asbestos') {
                    doc = [SELECT Id, Status__c, FlowId__c, FIP_FIP__c,Package__r.ConnectivePackageId__c FROM DocumentsForSignature__c WHERE FIP_FIP__c= :objId Limit 1];
                }
            } else if(objId.getSObjectType() == Attestation__c.sObjectType) {
                doc = [SELECT Id, Status__c, FlowId__c, FIP_FIP__c,Package__r.ConnectivePackageId__c FROM DocumentsForSignature__c WHERE Attestation__c= :objId Limit 1];
                prefip = [SELECT ID, NbSendReminder__c, Opportunity__c, Contact__c FROM Attestation__c WHERE ID = :objId];
            } else if(objId.getSObjectType() == Lead.sObjectType) {
                doc = [SELECT Id, Status__c, FlowId__c, FIP_FIP__c,Package__r.ConnectivePackageId__c FROM DocumentsForSignature__c WHERE Lead__c= :objId Limit 1];
                ld = [SELECT Id, NbSendReminder__c, Name FROM Lead WHERE Id = :objId];
            } else if(objId.getSObjectType() == CAP__c.sObjectType) {
                doc = [SELECT Id, Status__c, FlowId__c, CAP__c, Package__r.ConnectivePackageId__c FROM DocumentsForSignature__c WHERE CAP__c= :objId Limit 1];
                cap = [SELECT Id, NbSendReminder__c, Name, Contact__c FROM CAP__c WHERE Id = :objId];
            } else{
                doc = [SELECT Id, Status__c, FlowId__c, FIP_FIP__c,Package__r.ConnectivePackageId__c FROM DocumentsForSignature__c WHERE Quote__c = :objId Limit 1];
                quote = [SELECT ID, Name, NbSendReminder__c, Opportunity.Filiale__c, Opportunity.Name, Account.Name, ContactId FROM Quote WHERE ID = :objId];
            }

            String packageID = doc.Package__r.ConnectivePackageId__c;
            system.debug(packageID);
            if(String.IsNotBlank(packageID)){
                HttpResponse res = new HttpResponse();
                HttpRequest req = new HttpRequest();
                Http http = new Http();
                req.setEndpoint('callout:ConnectiveApiV4/packages/'+packageID+'/reminders');
                req.setMethod('POST');
                req.setHeader('Content-Length', '0');
                req.setTimeout(120000);
                try{
                    res = http.send(req);
                    System.debug('res.getStatusCode() : ' + res.getStatusCode());
                    if(res.getStatusCode() == 200){
                        if(prefip != null) {
                            if(prefip.NbSendReminder__c == null) {
                                prefip.NbSendReminder__c = 1;
                            } else {
                                prefip.NbSendReminder__c = prefip.NbSendReminder__c + 1;
                            }
                            String subject = 'RELANCE : ' + prefip.Opportunity__c;
                            Task t = LC_GenericEmailSend.createEmailActivity(subject, 'Relance envoyée au client', prefip.Contact__c, prefip.Id);
                            insert t;
                            update prefip;
                        } else if(quote != null) {
                            if(quote.NbSendReminder__c == null) {
                                quote.NbSendReminder__c = 1;
                            } else {
                                quote.NbSendReminder__c = quote.NbSendReminder__c + 1;
                            }
                            updateLockedRecord updateLockedRecord = new updateLockedRecord();
                            updateLockedRecord.updateRecord(quote);
                            //update quote;
                            String Nextyear = System.Label.YearN1;
                            String subject = 'RELANCE : '+quote.Opportunity.Name;
                            Task t = LC_GenericEmailSend.createEmailActivity(subject, 'Relance envoyée au client', quote.ContactId, quote.Id);
                            insert t;
                        } else if (ld != null) {
                            if(ld.NbSendReminder__c == null) {
                                ld.NbSendReminder__c = 1;
                            } else {
                                ld.NbSendReminder__c = ld.NbSendReminder__c + 1;
                            }
                            //String subject = 'RELANCE : '+ Lead.Name;
                            //Task t = LC_GenericEmailSend.createEmailActivity(subject, 'Relance envoyée au client', Lead.Id, Lead.Id);
                            //insert t;
                            update ld;
                        } else if (cap != null) {
                            List<CAP__c> caps = [SELECT NbSendReminder__c, LastReminderDate__c FROM CAP__c WHERE Contact__c = :cap.Contact__c];
                            for (CAP__c capToUpdate : caps) {
                                capToUpdate.LastReminderDate__c = System.today();
                                if(capToUpdate.NbSendReminder__c == null) {
                                    capToUpdate.NbSendReminder__c = 1;
                                } else {
                                    capToUpdate.NbSendReminder__c = capToUpdate.NbSendReminder__c + 1;
                                }  
                            }
                            update caps;
                        }
                        return 'OK';
                    } else {
                        Log__c error = new Log__c(
                            ApexJob__c= ConnectiveV4ApiManager.class.getName(),
                            ErrorMessage__c='request (reminder) : '+ packageID + 'response :' + res.getBody(),
                            ExceptionType__c='HTTP'
                        );
                        insert error;
                        return 'KO';
                    }
                }
                catch(Exception ex){
                    Log__c error = new Log__c(
                            ApexJob__c= ConnectiveV4ApiManager.class.getName(),
                            ErrorMessage__c=ex.getMessage() + ex.getLineNumber(),
                            ExceptionType__c='Error APEX'
                        );
                    insert error;
                    return 'KO : '+ ex.getMessage() + ex.getLineNumber();
                }
            }
            return 'Not processed';
        }catch(Exception e) {
            System.debug(e.getMessage());
            System.debug(e.getLineNumber());
            return 'KO';
        }
    }
    
    //________________________________________________________________________\\
    //___________________UPDATE STATUS USED FOR BATCH (TBD ?)___________________\\
    public static String updateStatus(Id objId) {

        SObject objToUpdate;
        Map<Id,String> pkgStatus = new Map<Id,String>();
        List<Package__c> pkgsToUpdate = new List<Package__c>();
        List<DocumentsForSignature__c> docToDownload = new List<DocumentsForSignature__c>();
        String simpleReturn = 'KO'; //use for the button get status

        List<DocumentsForSignature__c> docs = new List<DocumentsForSignature__c>();
        if(objId.getSObjectType() == FIP_FIP__c.sObjectType){
            docs = [SELECT Id, Status__c, FlowId__c, FIP_FIP__c,Package__c FROM DocumentsForSignature__c WHERE FIP_FIP__c= :objId];
        } else if(objId.getSObjectType() == Attestation__c.sObjectType) {
            docs = [SELECT Id, Status__c, FlowId__c, FIP_FIP__c,Package__c FROM DocumentsForSignature__c WHERE Attestation__c= :objId];
        } else if(objId.getSObjectType() == Lead.sObjectType){
            docs = [SELECT Id, Status__c, FlowId__c, Lead__c,Package__c FROM DocumentsForSignature__c WHERE Lead__c= :objId];
        } else if(objId.getSObjectType() == CAP__c.sObjectType){
            docs = [SELECT Id, Status__c, FlowId__c, CAP__c,Package__c FROM DocumentsForSignature__c WHERE CAP__c= :objId];
        } else{
            docs = [SELECT Id, Status__c, FlowId__c, FIP_FIP__c,Package__c FROM DocumentsForSignature__c WHERE Quote__c= :objId];
        }

        if(docs.size() > 0) {
            Map<Package__c,List<DocumentsForSignature__c>> pkgDoc = ConnectiveV4ApiUtils.getPackageDoc(docs);
            for(Package__c pkg : pkgDoc.keyset()){
                String result =  getStatusByConnectivePackageId(pkg.ConnectivePackageId__c);
                System.debug('result : '+result);
                if(result !=null){
                    Map<String,Object> jsonTransform = (Map<String,Object>)JSON.deserializeUntyped(result);
                    if(String.isNotBlank((String)jsonTransform.get('Status'))){
                        objToUpdate = ConnectiveV4ApiUtils.updateDocumentForSig(jsonTransform,pkg.ConnectivePackageId__c);
                        pkgStatus.put(pkg.Id,(String)jsonTransform.get('Status'));
                        simpleReturn = 'OK';
                    }
                    else{
                        simpleReturn = 'KO';
                    }
                }
            }

            for(Package__c pkg : pkgDoc.keySet()){
                Package__c pkgToUpdate = new Package__c(Id=pkg.Id);
                pkgToUpdate.ConnectivePackageStatus__c = pkgStatus.get(pkg.Id);
                pkgsToUpdate.add(pkgToUpdate);
                System.debug(pkgStatus.get(pkg.Id));
                if(pkgStatus.get(pkg.Id).toUpperCase() == 'FINISHED'){
                    docToDownload.addAll(pkgDoc.get(pkg));
                }
            }

            if(docToDownload.size()>0){
                Batch_Connective_Get_Document batch = new Batch_Connective_Get_Document(docToDownload, (objToUpdate != null) ? new List<SObject>{objToUpdate} : null);
                system.enqueueJob(batch);
            } else {
                if(objToUpdate != null)
                    update objToUpdate;
            }
            
            try{
                update pkgsToUpdate;
            }
            catch(Exception ex){
                Log__c error = new Log__c(
                        ApexJob__c= ConnectiveV4ApiManager.class.getName(),
                        ErrorMessage__c=ex.getMessage() + ex.getLineNumber(),
                        ExceptionType__c='Error APEX'
                    );
                insert error;
                simpleReturn = 'KO';
            }
        }
        return simpleReturn;
    }

    //______________________________________________________\\
    //___________________CONNECTIVE GET DOC___________________\\
    public static void getDocumentByName(Id objId, String connectiveIdPackage, String documentId, String docName){
        HttpResponse res = new HttpResponse();
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        req.setHeader('Content-Type','application/json');
        req.setEndpoint('callout:ConnectiveApiV4/packages/'+connectiveIdPackage+'/download/'+documentId);
        req.setMethod('GET');
        req.setTimeout(120000); 
        try{
            res = http.send(req);
            if(res.getStatusCode() == 200){
                Blob result =res.getBodyAsBlob();
                String titleDoc = '';
                titleDoc = retrieveName(objId);
                //if doc is CGV then add CGV to the name
                if (docName.contains('CGV')){
                    titleDoc += ' - CGV';
                }
                insertFile(result,titleDoc,objId);
            }
            else{
                Log__c error = new Log__c(
                    ApexJob__c= ConnectiveV4ApiManager.class.getName(),
                    ErrorMessage__c='response :' + res.getBody(),
                    ExceptionType__c='HTTP'
                );
                insert error;
            }
        }
        catch(Exception ex){
            Log__c error = new Log__c(
                    ApexJob__c= ConnectiveV4ApiManager.class.getName(),
                    ErrorMessage__c=ex.getMessage() + ex.getLineNumber(),
                    ExceptionType__c='Error APEX'
                );
            insert error;
        }
    }

    public static String retrieveName(Id objId){
        String returnValue = '';
        String sobjectType = objId.getSObjectType().getDescribe().getName();
        Schema.SObjectType convertType = Schema.getGlobalDescribe().get(sobjectType);

        if (convertType == Quote.sObjectType) {
            returnValue = [Select Id,QuoteCode__c FROM Quote Where Id= : objId LIMIT 1].QuoteCode__c;
        } else if (convertType == FIP_FIP__c.sObjectType) {
            returnValue = [Select Id,Name FROM FIP_FIP__c Where Id= : objId LIMIT 1].Name;
        } else if (convertType == Attestation__c.sObjectType) {
            returnValue = [Select Id,Name FROM Attestation__c Where Id= : objId LIMIT 1].Name; 
        } else if(convertType == Lead.sObjectType) {
            Lead lead = [Select Id, RequestNumber__c, IsUnderContract__c, DB_Created_Date_without_Time__c, LeadSource, Owner.Alias, CreatedDate FROM Lead Where Id= : objId LIMIT 1];
            String dateWithoutTime = lead.CreatedDate.format('yyyy-MM-dd');
            if (lead.LeadSource == 'Devis Flash Garage') {
                returnValue = dateWithoutTime + ' - ' + lead.Owner.Alias + ' - ' + lead.RequestNumber__c;
            }
            if (lead.LeadSource == 'Urgence Assainissement') {
                if (lead.IsUnderContract__c) {
                    returnValue = 'Urgence Assainissement N°' + lead.RequestNumber__c + ' - ' + dateWithoutTime;
                } else {
                returnValue = 'Devis Urgence Assainissement N°' + lead.RequestNumber__c + ' - ' + 	dateWithoutTime;
                }
            }
        } else if (convertType == CAP__c.sObjectType) {
            returnValue = 'CAP N°' + [Select Id, Name FROM CAP__c Where Id= : objId LIMIT 1].Name + ' - Complément FIP'; 
        } else {
            System.debug('ERROR OBJECT NOT HANDLED BY CALLOUT');
        }
        return returnValue;
    }

    private static void insertFile(Blob resultpdf, String docName, Id parent){
        //Insert ContentVersion
        ContentVersion cVersion = new ContentVersion();
        cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
        cVersion.PathOnClient = docName + ' - Signée.pdf';//File name with extention
        cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.
        cVersion.OwnerId = userinfo.getUserId();//Owner of the file
        cVersion.Title = docName + ' - Signée';//Name of the file
        cVersion.VersionData = resultpdf;//File content
        cVersion.firstPublishLocationId= userinfo.getUserId();
        Insert cVersion;

        //After saved the Content Verison, get the ContentDocumentId
        Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;

        //Insert ContentDocumentLink
        ContentDocumentLink cDocLink = new ContentDocumentLink();
        cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
        cDocLink.LinkedEntityId = parent;//Add attachment parentId
        cDocLink.ShareType = 'V';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
        cDocLink.Visibility = 'AllUsers';//AllUsers, InternalUsers, SharedUsers
        Insert cDocLink;
    }

    without sharing class updateLockedRecord {	  
        public void updateRecord(Quote q) { 		
            update q; 
        }
    }

}