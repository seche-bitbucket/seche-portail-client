/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ContentDocumentTriggerHandlerTest
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ContentDocumentTriggerHandlerTest trigger
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 23-03-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
@isTest
public with sharing class ContentDocumentTriggerHandlerTest {
    @isTest
    public static void blockContentDocumentQuoteTest(){
        Opportunity opp1 = TestDataFactory.createOpportunity('opp1');
        insert opp1;
        Quote q1 = TestDataFactory.createQuote(opp1);
        insert q1;
        ContentVersion cv = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert cv;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = q1.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        q1.TECH_IsLocked__c = true;
        update q1;

        try{
            test.startTest();
            delete documents[0];
            test.stopTest();
        }catch(Exception ex){
            Boolean expectedExceptionThrown =  ex.getMessage().contains(Constants.STR_CVE_ERRORMESSAGE_QUOTE_LOCKED) ? true : false;
            System.assertEquals(expectedExceptionThrown, true);
        }
    }  
}