/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-06-29
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for Increase by Percentage in contract Controller 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class Test_VFC_ContractIncreaseByPercentage {
    public static PageReference myVfPage {get;set;}
    public static ApexPages.StandardSetController stc {get;set;}
    public static VFC_ContractIncreaseByPercentage vfc{get;set;}    
    @isTest static void TestIncreasePercentage(){
        PageReference ref = new PageReference('/001/o');
        Test.setCurrentPage(ref);
        ApexPages.currentPage().getParameters().put('retURL', '/001/o');
        Contract__c c = new Contract__c();
        c.name='t';
        insert c;
        List<Contract__c> contracts = new List<Contract__c>();
        contracts.add(c);
        Amendment__c am = new Amendment__c();
        am.Contrat2__c=c.id;
        am.PriceCurrentYear__c=120.12;
        am.Nature__c='Traitement';
        Amendment__c am2 = new Amendment__c();
        am2.Nature__c='Prestation';
        am2.Contrat2__c=c.id;
        am2.PriceCurrentYear__c=100.78;
        Amendment__c am3 = new Amendment__c();
        am3.Nature__c='Conditionnement';
        am3.Contrat2__c=c.id;
        am3.PriceCurrentYear__c=200;
        List<Amendment__c> Amendments = new List<Amendment__c>();
        Amendments.add(am);
        Amendments.add(am2);
        Amendments.add(am3);
        insert Amendments;
        stc=new Apexpages.standardSetController(contracts);
        stc.setSelected(contracts);
        vfc = new VFC_ContractIncreaseByPercentage(stc);
        test.startTest();
        vfc.save();
        System.assertEquals(ApexPages.Severity.WARNING, ApexPages.getMessages()[0].getSeverity());
        System.assertEquals('Veuillez saisir une valeur positive', ApexPages.getMessages()[0].getSummary());
        vfc.inputValue=10;
        vfc.changeAllInput();
        vfc.EntierHigh=true;
        vfc.save();
        test.stopTest();
        List<Amendment__c> amendmentsUpdated=[SELECT PriceNextYear__c,PriceCurrentYear__c from Amendment__c Where Contrat2__c=:c.Id ORDER BY PriceNextYear__c ASC];
        System.assertEquals(111.00, amendmentsUpdated[0].PriceNextYear__c);
        System.assertEquals(133.00, amendmentsUpdated[1].PriceNextYear__c);
        System.assertEquals(220.00, amendmentsUpdated[2].PriceNextYear__c);
        vfc = new VFC_ContractIncreaseByPercentage(stc);
        vfc.inputValueConditionnement=5;
        vfc.inputValueTransport=5;
        vfc.inputValueTraitement=5;
        vfc.EntierAuto=true;
        vfc.save();
        List<Amendment__c> amendmentsUpdated2=[SELECT PriceNextYear__c,PriceCurrentYear__c from Amendment__c Where Contrat2__c=:c.Id ORDER BY PriceNextYear__c ASC];
        System.assertEquals(106.00, amendmentsUpdated2[0].PriceNextYear__c);
        System.assertEquals(126.00, amendmentsUpdated2[1].PriceNextYear__c);
        System.assertEquals(210.00, amendmentsUpdated2[2].PriceNextYear__c);
        vfc = new VFC_ContractIncreaseByPercentage(stc);
        vfc.inputValueConditionnement=10;
        vfc.save();
        List<Amendment__c> amendmentsUpdated3=[SELECT PriceNextYear__c,PriceCurrentYear__c from Amendment__c Where Contrat2__c=:c.Id ORDER BY PriceNextYear__c ASC];
        System.assertEquals(106.00, amendmentsUpdated3[0].PriceNextYear__c);
        System.assertEquals(220.00, amendmentsUpdated3[2].PriceNextYear__c);
        c.RecordLocked__c=true;
        Update c;
        vfc = new VFC_ContractIncreaseByPercentage(stc);
    }
    @isTest static void TestExceptions(){
        List<Contract__c> contracts = new List<Contract__c>();
        stc=new Apexpages.standardSetController(contracts);
        stc.setSelected(contracts);
        vfc = new VFC_ContractIncreaseByPercentage(stc);
        System.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages()[0].getSeverity());
        System.assertEquals('Veuillez sélectionner un contrat', ApexPages.getMessages()[0].getSummary());
    }
}