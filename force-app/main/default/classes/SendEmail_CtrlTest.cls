@isTest
public class SendEmail_CtrlTest {
    
    @isTest
    static void getEmailValuesNoFilesTest(){
       Contact cont = new Contact(LastName = 'Test Contact', Email  = 'test@test.com');
       insert cont;
       cont = [Select Id from Contact Limit 1];
       
        Account acc = new Account (name = 'test ACC');
        insert acc;
        acc = [Select Id from Account Limit 1];
        
        Attestation__c attestation = new Attestation__c(Year__c=2020, Name = 'Test Attestation', Account__c = acc.Id, AssociatedSalesRep__c = UserInfo.getUserId() , Status__c = Constants.ATTESTATION_GENERATE_STATUS, Contact__c = cont.Id, Sorted__c=true, Producer__c=true);
        insert attestation;
        attestation = [Select Id from Attestation__c Limit 1];
        
        SendEmail_Ctrl.RequestResponse resp = SendEmail_Ctrl.getEmailValues(String.valueOf(attestation.Id));
        
        System.assertEquals(resp.showToastMode , 'warning');
        System.assertEquals(resp.message , 'Nombre de pièces jointes non conforme');
        
    }
    
    @isTest
    static void getEmailValues2FilesTest(){
       Contact cont = new Contact(LastName = 'Test Contact', Email  = 'test@test.com');
       insert cont;
       cont = [Select Id from Contact Limit 1];
       
        Account acc = new Account (name = 'test ACC');
        insert acc;
        acc = [Select Id from Account Limit 1];
        
        Attestation__c attestation = new Attestation__c(Year__c=2020, Name = 'Test Attestation', Account__c = acc.Id, AssociatedSalesRep__c = UserInfo.getUserId() , Status__c = Constants.ATTESTATION_GENERATE_STATUS, Contact__c = cont.Id, Sorted__c=true, Producer__c=true);
        insert attestation;
        attestation = [Select Id from Attestation__c Limit 1];
        
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
        
        ContentVersion cv2 = new ContentVersion();
        cv2.Title = 'Test Document';
        cv2.PathOnClient = 'TestDocument.pdf';
        cv2.VersionData = Blob.valueOf('Test Content');
        cv2.IsMajorVersion = true;
        Insert cv2;
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument limit 2];

        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=attestation.id;
        contentlink.ShareType= 'V';
        contentlink.ContentDocumentId=documents[0].Id;
        contentlink.Visibility = 'AllUsers'; 
        insert contentlink;
        
        ContentDocumentLink contentlink2=new ContentDocumentLink();
        contentlink2.LinkedEntityId=attestation.id;
        contentlink2.ShareType= 'V';
        contentlink2.ContentDocumentId=documents[1].Id;
        contentlink2.Visibility = 'AllUsers'; 
        insert contentlink2;
        
        
        SendEmail_Ctrl.RequestResponse resp = SendEmail_Ctrl.getEmailValues(String.valueOf(attestation.Id));
        
        System.assertEquals(resp.showToastMode , 'warning');
        System.assertEquals(resp.message , 'Vous ne pouvez pas envoyer une attestation avec plus d\'un fichier');
        
    }
    
    @isTest
    static void getEmailValuesStatusNotValidTest(){
       Contact cont = new Contact(LastName = 'Test Contact', Email  = 'test@test.com');
       insert cont;
       cont = [Select Id from Contact Limit 1];
       
        Account acc = new Account (name = 'test ACC');
        insert acc;
        acc = [Select Id from Account Limit 1];
        
        Attestation__c attestation = new Attestation__c(Year__c=2020, Name = 'Test Attestation', Account__c = acc.Id, AssociatedSalesRep__c = UserInfo.getUserId() , Status__c = Constants.ATTESTATION_DRAFT_STATUS, Contact__c = cont.Id, Sorted__c=true, Producer__c=true);
        insert attestation;
        attestation = [Select Id from Attestation__c Limit 1];
        
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument limit 1];

        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=attestation.id;
        contentlink.ShareType= 'V';
        contentlink.ContentDocumentId=documents[0].Id;
        contentlink.Visibility = 'AllUsers'; 
        insert contentlink;
        
        SendEmail_Ctrl.RequestResponse resp = SendEmail_Ctrl.getEmailValues(String.valueOf(attestation.Id));
        
        System.assertEquals(resp.showToastMode , 'warning');
        System.assertEquals(resp.message , 'Vous ne pouvez pas envoyer une attestation si elle n\'est pas au statut '+Constants.ATTESTATION_GENERATE_STATUS);        
    }
    
     @isTest
    static void getPreviwEmailTest(){
       Contact cont = new Contact(LastName = 'Test Contact', Email  = 'test@test.com');
       insert cont;
       cont = [Select Id from Contact Limit 1];
       
        Account acc = new Account (name = 'test ACC');
        insert acc;
        acc = [Select Id from Account Limit 1];
        
        Attestation__c attestation = new Attestation__c(Name = 'Test Attestation', Account__c = acc.Id, AssociatedSalesRep__c = UserInfo.getUserId() , Status__c = Constants.ATTESTATION_GENERATE_STATUS, Contact__c = cont.Id, Sorted__c=true, Producer__c=true);
        insert attestation;
        attestation = [Select Id from Attestation__c Limit 1];
        
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument limit 1];

        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=attestation.id;
        contentlink.ShareType= 'V';
        contentlink.ContentDocumentId=documents[0].Id;
        contentlink.Visibility = 'AllUsers'; 
        insert contentlink;
        
        Map<String,String> resp = SendEmail_Ctrl.getPreviewEmail(String.valueOf(attestation.Id));        
        System.assertEquals(resp.get('status') , Constants.ATTESTATION_GENERATE_STATUS);        
        
        List<String> lst = resp.get('ccaddresses').trim().split(';');
        
        Map<String,String> resp2 = SendEmail_Ctrl.sendMailMethod(resp.get('contactId'), resp.get('contactName'), lst[0], resp.get('toAddresses'), resp.get('ccaddresses'), resp.get('senderDisplayName'), resp.get('subject'), resp.get('body'), attestation.Id, null);
        System.assertEquals(resp2.get('showToastMode'), 'success');
        
    }    
    

}