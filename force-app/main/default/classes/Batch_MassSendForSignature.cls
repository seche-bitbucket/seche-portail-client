/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : Batch_MassSendForSignature
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : Batch_MassSendForSignature
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 29-05-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
public with sharing class Batch_MassSendForSignature implements Queueable, Database.AllowsCallouts  {

    public Map<Id,SObject> objects = new Map<Id,SObject>();
    public List<SObject> objectsProcessed = new List<SObject>(); 
    private String contactField;
    private Schema.SObjectType objectApiName;

    public Batch_MassSendForSignature(List<SObject> objects,List<SObject> objectsProcessed){
        this.objects = new Map<Id,SObject>(objects);
        this.objectApiName = new List<Id>(this.objects.keySet())[0].getSobjectType();
        /*if(this.objectApiName == Quote.sobjectType) {
            this.contactField = 'ContactId';
        } else*/
        // if(this.objectApiName == Attestation__c.sobjectType) {
        //     this.contactField = 'Contact__c';
        // }
        if(objectsProcessed == null){
            this.objectsProcessed = new List<SObject>();
        }
        else{
            this.objectsProcessed = objectsProcessed;
        }
    }
    
    public void execute(QueueableContext context) {
        try{
            for(Id oId : objects.keySet()){
                String result = '';
                if(Test.isRunningTest()) {
                    result = 'OK';
                } else {
                    Id contactId;
                    if(this.objectApiName == Attestation__c.sobjectType) {
                        contactId = [SELECT Contact__c FROM Form__c WHERE ID = :(Id)objects.get(oId).get('Form__c')].Contact__c;
                    } else {
                        contactId = (Id)objects.get(oId).get(contactField);
                    }
                    PackageV4Wrapper pkg = ConnectiveV4ApiUtils.createPackage(oId, contactId);
                    result = ConnectiveV4ApiManager.createPackage(pkg, oId);
                    
                }
                if(result == 'OK'){
                    SObject obj = objects.get(oId);
                    this.objectsProcessed.add(obj);
                }
                objects.remove(oId);
                if(objects.size()>0){
                    System.enqueueJob(new Batch_MassSendForSignature(objects.values(),this.objectsProcessed));
                } else {
                    /*if(this.objectApiName == Quote.SObjectType) {
                     SignatureUtils.sendEmailGroup(this.objectsProcessed,false);
                     updaterecordTypeQuote(this.objectsProcessed);
                    } else*/ 
                    if(this.objectApiName == Attestation__c.SObjectType) {
                        updateAtestation(this.objectsProcessed);
                    }
                }
                break;
            }
        }
        catch(Exception ex){
            System.debug(Batch_MassSendForSignature.class.getName()+' // '+ex.getMessage()+' at '+ex.getLineNumber());
        }
    }

    private void updateAtestation(List<Attestation__c> attsToUpdate) {
        List<Attestation__c> atts = new List<Attestation__c>();
        for (Attestation__c att : attsToUpdate) {
            atts.add(new Attestation__c(Id = att.Id,
                                        Status__c = 'Envoyée pour signature'));
        }
        update atts;
    }

    // private void updaterecordTypeQuote(List<Quote> quoteToUpdate){
    //     List<Quote> quoteUpdated = new List<Quote>();
    //     RecordType rt=[SELECT Id, Name, DeveloperName, Description FROM RecordType Where DeveloperName='Devis_approuv_Read_Only' LIMIT 1];
    //     for(Quote q : quoteToUpdate){
    //         q.Tech_UID__c=GuidUtil.NewGuid();
    //         q.TECH_isApprouved__c=true;
    //         q.Status='Envoyé pour signature';
    //         q.RecordTypeId=rt.Id;
    //         quoteUpdated.add(q);		
    //     }
	// 	//Update quotes
	// 	if(quoteUpdated.size() > 0) {
	// 		updateLockedRecords updateQuoteRecords = new updateLockedRecords();
	// 		updateQuoteRecords.updateRecords(quoteUpdated);			
	// 	}	
    // }

    // // Class for updating locked record (without sharing to by pass locked record)
    // without sharing class updateLockedRecords {	  
    //     public void updateRecords(List<Quote> qt) { 		
    //         update qt; 
    //     }
    // }

}