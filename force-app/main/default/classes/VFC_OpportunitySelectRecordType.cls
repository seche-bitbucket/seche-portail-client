public class VFC_OpportunitySelectRecordType {
    public List<SelectOption> relationType{get;set;}
    public List<SelectOption> recordsTypesSelect{get;set;}
    public List<RecordType> recordsTypes{get;set;}
    public String relatedOpt{get;set;}
    public String recordType{get;set;}
    public Boolean displayRecordType{get;set;}
    public Boolean displayButton{get;set;}
    public Account acc;
    public Contact ctc;
    
    public VFC_OpportunitySelectRecordType(ApexPages.StandardController controller) {
        setRelationTypes();     
    }
    
    //Controller for Create Button on related list
        public VFC_OpportunitySelectRecordType(ApexPages.StandardSetController controller) {      
        //check if the creation is triggered from an Account or a Contact
        Boolean isAccount = testAccount();
            
            System.debug('.......isAccount : '+isAccount);
        
        //Get Account information
        if(isAccount){
            acc = getAccount();
            //Get Contact information and his Account related
        }else{
            ctc = getContact();
            acc = getContactAccount(ctc);
            System.debug('this is the contact'+ctc);
            System.debug('this is account for contact'+ acc);
        }
        
        setRelationTypes(); 
    }
    
    //Load Relation values in the picklist
    public void setRelationTypes(){
        
        relationType = new List<SelectOption>();
        
        Schema.DescribeFieldResult picklist = Schema.sObjectType.Opportunity.fields.TypeRelation__c .getSObjectField().getDescribe();
       
        relationType .add(new SelectOption('--Aucun--', '--Aucun--'));
        for(PicklistEntry ent:picklist.getpicklistvalues())
        {
            relationType .add(new SelectOption(ent.getValue(), ent.getLabel()));
        }
    }
    
    
    //Check the relation and load Recordtype if needed
    public PageReference updateRecordType(){
        
        //Check if the relation is : direct
        if (relatedOpt.contains('standard')){
            //Display the the picklist with the RecordType
            displayRecordType = true;
            //Load the RecordType picklist
            recordsTypes = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SobjectType='Opportunity' AND DeveloperName LIKE :'%'+relatedOpt ORDER BY Name ASC];
            recordsTypesSelect = new List<SelectOption>();
            recordsTypesSelect.add(new SelectOption('--Aucun--', '--Aucun--'));
            for(RecordType r : recordsTypes){
                recordsTypesSelect.add(new SelectOption(r.DeveloperName, r.Name));
            }
            //If not 'intermediaire' relation
        }else{
            //Don't display the RecordType Picklist and displau the validate button
            displayRecordType = false;
            displayButton = true;
        }
        return ApexPages.currentPage();
    }
    
    //On Validate button event, return the Opp Creation page with the selected recordtype
    public PageReference redirectRecordType(){
        //Check if the recordtype has been selected => Direct Relation
        if(relatedOpt.contains('standard')){
            for(RecordType r : recordsTypes){
                if(r.DeveloperName.equals(recordType))
                    return new PageReference('/006/e?RecordType=' + r.Id+'&nooverride=1');
            }
            //Else intermediaire Relation => Opport_Mere
        }else{
            RecordType rt = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SobjectType='Opportunity' AND DeveloperName = :'OPP_OpportuniteMere' Limit 1];
            return new PageReference('/006/e?RecordType=' + rt.Id+'&nooverride=1');
        }
        
        return null;
    }
    
    //Display the Validation button if a recordtype has been selected
    public void updateButton(){
        
        if (recordsTypesSelect != null){
            displayButton = true;
            
        }else{
            displayButton = false;
        }
    }
    
    public PageReference redirectToAccount(){
        
        if(relatedOpt.contains('standard')){
            for(RecordType r : recordsTypes){
                if(r.DeveloperName.equals(recordType)){
                    PageReference pgOpport= Page.VFP_CreateOppRecord;
                    pgOpport.getParameters().put('accountId', acc.Id);
                    pgOpport.getParameters().put('recordTypeId', r.Id);
                    pgOpport.setRedirect(true);
                    return pgOpport;
                }                   
                   
                  // return new PageReference('/006/e?lightning/cmp/c__LC_UrlCreateRecord?c__ObjectName=Opportunity&c__RecordTypeId='+r.Id+'&c__AccountId='+acc.ID+'&nooverride=1');
                   // return new PageReference('/lightning/cmp/c__LC_UrlCreateRecord?c__ObjectName=Opportunity&c__RecordTypeId='+r.Id+'&c__AccountId='+acc.ID);
                    

            }
            //Else intermediaire Relation => Opport_Mere
        }else{
                RecordType rt = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SobjectType='Opportunity' AND DeveloperName = :'OPP_OpportuniteMere' Limit 1];
                PageReference pgOpport= Page.VFP_CreateOppRecord;
                pgOpport.getParameters().put('accountId', acc.Id);
                pgOpport.getParameters().put('recordTypeId', rt.Id);
                pgOpport.setRedirect(true);
                return pgOpport;
           // return new PageReference('/006/e?RecordType=' +rt.Id+'&'+'opp4'+'='+acc.Name+'&'+'opp4'+'_lkid='+acc.ID+'&nooverride=1');
        }
        
        
        return null;
    }
    
    public PageReference redirectToContact(){
        
        
        if(relatedOpt.contains('standard')){
            for(RecordType r : recordsTypes){
                if(r.DeveloperName.equals(recordType)){
                    PageReference pgOpport= Page.VFP_CreateOppRecord;
                    pgOpport.getParameters().put('accountId', acc.Id);
                    pgOpport.getParameters().put('contactId', ctc.Id);
                    pgOpport.getParameters().put('recordTypeId', r.Id);                   
                    pgOpport.setRedirect(true);
                    return pgOpport;
                    // return new PageReference('/006/e?RecordType=' +r.Id+'&'+'opp4'+'='+acc.Name+'&'+'opp4'+'_lkid='+acc.ID+'&'+Label.OPP_Contact+'='+ctc.Name+'&'+ Label.OPP_Contact+'_lkid='+ctc.ID+'&nooverride=1');
                }
                   
            }
            //Else intermediaire Relation => Opport_Mere
        }else{
                RecordType rt = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SobjectType='Opportunity' AND DeveloperName = :'OPP_OpportuniteMere' Limit 1];
                PageReference pgOpport= Page.VFP_CreateOppRecord;
                pgOpport.getParameters().put('accountId', acc.Id);
                pgOpport.getParameters().put('contactId', ctc.Id);
                pgOpport.getParameters().put('recordTypeId', rt.Id);                
                pgOpport.setRedirect(true);
                return pgOpport;

            //return new PageReference('/006/e?RecordType=' +rt.Id+'&'+'opp4'+'='+acc.Name+'&'+'opp4'+'_lkid='+acc.ID+'&'+Label.OPP_Contact+'='+ctc.Name+'&'+ Label.OPP_Contact+'_lkid='+ctc.ID+'&nooverride=1');
        }
        
        return null;
    }
    
    // get Account parameter 
    public Account getAccount() { 
        Account accQuery = [SELECT Id, Name FROM Account where ID= :ApexPages.currentPage().getParameters().get('id') limit 1 ];
        
        return accQuery;
    }
    
    //Test if the Parameter is an Account or a Contact
    public Boolean testAccount() { 
        try{
            Account acc = [SELECT Id, Name FROM Account where ID= :ApexPages.currentPage().getParameters().get('id') limit 1 ];
            return true;
        }catch(Exception e){
            return false;
        }
    }
    
    // get Contact parameter
    public Contact getContact() {
        Contact c = [SELECT Id, Name, AccountID FROM Contact where ID= :ApexPages.currentPage().getParameters().get('id') limit 1 ];
 
        return c;
    }
    
    // get Account parameter from contact 
    public Account getContactAccount(Contact c) { 
        Account ac = [SELECT ID,Name FROM Account where ID= :c.AccountId limit 1 ];
        return ac;
    }
    
    
}