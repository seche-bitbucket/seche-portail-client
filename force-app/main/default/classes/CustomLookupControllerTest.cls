@isTest
public class CustomLookupControllerTest {

     @IsTest(SeeAllData=true) 
    public static void TestCustomLookupControllerTest(){
        //Set the current VF Page which use the ContactController
        PageReference pgOpportFille = Page.OPP_OpportuniteFilleLookUp;
        
        //Standard Set controller
        ApexPages.StandardSetController stdSetCtrl;
        
        //Standard controller
        ApexPages.StandardController stdCtrl;
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Chargé(e) d\'affaires' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    SubsidiaryCreationRight__c = 'Alcea',
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname');
        
        System.RunAs(currentUser) { 
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aucun', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try{
                insert ac;
            }catch(Exception e){
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '0606060606', Salesman__c = currentUser.ID);
            
            try{
                insert contactTest;
            }catch(Exception e1){
            }
            
            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];
            
            //Create Opportunity
            Opportunity opportMere = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', Filiere__c ='DASRI');
            Opportunity opportFille = new Opportunity(Name = 'TESTOPPORTFILLE', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', Filiere__c ='DASRI');
            insert opportMere;
            insert opportFille;
            
            Test.startTest();
            
            List<Opportunity> oppLst = new List<Opportunity>();
            oppLst.add(opportFille);
            // Set the currentPage to the test
            Test.setCurrentPage(pgOpportFille);
            //Set parameter 
            pgOpportFille.getParameters().put('ID', opportMere.Id );
            pgOpportFille.getParameters().put('lksrch', 'TEST' );
            
            CustomLookUpController customLookup = new CustomLookUpController(stdSetCtrl);
            Opportunity oppResult =  customLookup.results.get(0);
            
            System.assertEquals(opportFille.ID, oppResult.ID);
            
            PageReference pg = customLookup.redirectOppMere();
            
            
            customLookup = new CustomLookUpController(stdCtrl);
            customLookup.search();
            customLookup.getFormTag();
            customLookup.getTextBox();
            Test.stopTest();
            
        }
    }
        
}