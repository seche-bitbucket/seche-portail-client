@isTest
public with sharing class LC_EventControllerTest {

static testMethod void testGetEvent(){
	String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

	//Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;
	System.RunAs(director) {
		User u = [Select Id From User Limit 1];
		//RecordType rt = [Select Id From RecordType Where IsActive = true AND SobjectType = 'Account' Limit 1];
		Account a = new Account();
		a.Name = 'testAccName';
		a.CustomerNature__c = 'Administration';
		a.Industry = 'Aéronautique';
		a.CurrencyIsoCode = 'EUR';
		//a.RecordTypeId = rt.Id;
		a.Producteur__c = false;
		a.BillingPostalCode = '01000';
		a.NAF_Number__c = '1234A';
		insert a;

		Contact c = new Contact();
		c.LastName = 'testName';
		c.AccountId = a.Id;
		c.Salesman__c = u.Id;
		c.Email = 'aaa@yopmail.com';
		c.Phone = '03 00 00 00 00';
		insert c;
		Event e = new Event();
		e.WhatId = a.Id;
		e.WhoId = c.Id;
		e.Subject = 'test title';
		e.OwnerId = director.Id;
		e.StartDateTime = System.today();
		e.EndDateTime = System.today();
		e.ActivityDate = System.today();
		e.ActivityDateTime = System.today();
		e.Compte__c = a.Id;
		insert e;
		System.assertEquals(e.id, LC_EventController.getEvent(e.Id).Id);
	}
}

static testMethod void testGenerateVR()
{

	String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};
	//Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;
	System.RunAs(director) {
		User u = [Select Id From User Limit 1];
		//RecordType rt = [Select Id From RecordType Where IsActive = true AND SobjectType = 'Account' Limit 1];
		Account a = new Account();
		a.Name = 'testAccName';
		a.CustomerNature__c = 'Administration';
		a.Industry = 'Aéronautique';
		a.CurrencyIsoCode = 'EUR';
		//a.RecordTypeId = rt.Id;
		a.Producteur__c = false;
		a.BillingPostalCode = '01000';
		a.NAF_Number__c = '1234A';
		insert a;

		Contact c = new Contact();
		c.LastName = 'testName';
		c.AccountId = a.Id;
		c.Salesman__c = u.Id;
		c.Email = 'aaa@yopmail.com';
		c.Phone = '03 00 00 00 00';
		insert c;

		Event e = new Event();
		e.WhatId = a.Id;
		e.WhoId = c.Id;
		e.Subject = 'test title';
		e.OwnerId = director.Id;
		e.StartDateTime = System.today();
		e.EndDateTime = System.today();
		e.ActivityDate = System.today();
		e.ActivityDateTime = System.today();
		e.Compte__c = a.Id;
		insert e;

		// Cas nominal
		System.assert (!LC_EventController.generateVisitReport(e).equals('error1') && !LC_EventController.generateVisitReport(e).equals('error2'));

		// Cas d'erreur où l'évévement est déjà lié à un CR
		VisitReport__c vr = new VisitReport__c();
		vr.Name = 'test';
		vr.Account__c = a.Id;
		vr.Contact__c = c.Id;
		insert vr;
		e.Compte_rendu__c = vr.Id;
		update e;
		System.assert (LC_EventController.generateVisitReport(e).equals('error1'));

		// Cas d'erreur où le contact ou compte ne sont pas renseignés
		e.Compte_rendu__c = null;
		e.Compte__c = null;
		e.WhoId = null;
		update e;
		System.assert (LC_EventController.generateVisitReport(e).equals('error2'));
	}
}
}