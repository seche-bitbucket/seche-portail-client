public class SendSignatureQueueable Implements Queueable 
{
    private List<Quote> Quotes;
    public void execute(QueueableContext sc)
    {
        for(quote quoteTosend:Quotes){
            sendCreatedFlow(quoteTosend.Id);
        }
    }
    public SendSignatureQueueable(Quote q){
        this.Quotes=new List<Quote>();
        this.Quotes.add(q);
        System.debug(This.quotes.size());
    }
    public void sendCreatedFlow(Id quoteId){
        Quote quote=[Select Id,TECH_UnknownSignerInformation__c,QuoteCode__c, TECH_SendForSignature__c,Contact.FirstName,
                     Contact.LastName,Contact.Email,Contact.Phone FROM Quote Where Id=:quoteId];
        List<Attachment> att=[SELECT id, Name, body, ContentType,ParentId FROM Attachment WHERE ParentId=:quote.Id];
        List<ContentDocumentLink> cdl=[SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: quote.Id];		
        if(att.size()>0 || cdl.size() >0){
            if(quote.TECH_UnknownSignerInformation__c.contains('=')){
                List<String> information=quote.TECH_UnknownSignerInformation__c.split('=');                    
                if(!Test.isRunningTest()){
                    ConnectiveCallout.sendCreatedFlowWSUnknownSigner(quote.Id, information[0], information[1],information[3] , information[2]); 
                }
            }else{
                List<Contact> Contact=[SELECT Id,FirstName,LastName,Email,Phone From Contact WHERE ID=:quote.ContactId];
                if(!Test.isRunningTest()){
                    ConnectiveCallout.sendCreatedFlowWSUnknownSigner(quote.Id, quote.Contact.FirstName, quote.Contact.LastName, quote.Contact.Phone ,quote.Contact.Email); 
                }
            }
        }
    }
}