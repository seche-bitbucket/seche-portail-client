@isTest(SeeAllData=true)
public class AccountCreationCheckTriggerUtilTest {
    
    //FINISHED
    @isTest
    public static void CreateAccountTest(){
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Assistante commerciale' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname', SocieteF_t__c = true);
        
        System.RunAs(currentUser){
            Test.startTest();
            
            Account acc = new Account();
            acc.Name = 'Account1';
            acc.CustomerNature__c = 'Collecteur';
            acc.Industry = 'Aéronautique';
            acc.BillingCity = 'Ville-de-test';      
            
            List<Account> accs = new List<Account>();
			accs.add(acc);
            
            try{
                insert(acc);
            }
            catch(Exception e){
             
            }  
            
            AccountCreationCheckTriggerUtil.CreateAccount(accs);
            Test.stopTest();
            System.assertEquals(null, acc.Id);
        }
    }
}