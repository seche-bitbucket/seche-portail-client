/*
* ------------------------------------------------------------------------------------------------------------------
* -- Name : ConnectiveUpdateStatusBatchV4
* -- Author : PMB
* -- Company : Capgemini
* -- Purpose : ConnectiveUpdateStatusBatchV4
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 27-05-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
global class Batch_CheckDuplicateAccounts implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful{
    
    global List<Account> lstAcc {get; set;}
    
    global Database.querylocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator(
            'SELECT Id, Name, TECH_IsDuplicate__c, TECH_NotToMerge__c, TECH_IsPassed__c, SIRET_Number__c, ContractRoot__c, CustomerNature__c, Industry FROM Account WHERE TECH_IsPassed__c = false AND (SIRET_Number__c LIKE \'______________\') AND SIRET_Number__c != NULL AND (NOT CustomerNature__c IN (\'Particulier\', \'Groupe Séché\', \'Exutoire\')) ORDER BY SIRET_Number__c'
        );
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope) {
        
        this.lstAcc = scope;
        System.debug(this.lstAcc.size());
        
        List<Id> lstAccsIds = new List<Id>();

        if(lstAcc.size() > 0) {
            for(Account acc: lstAcc) {
                lstAccsIds.add(acc.Id);
            }
        }
        
        List<Contact> lstCts = Database.query('SELECT Id, AccountId FROM Contact WHERE AccountId IN :lstAccsIds');
        
        List<Id> lstCtsIds = new List<Id>();
        for(Contact ct: lstCts) {
            lstCtsIds.add(ct.Id);
        }
        
        Map<Id, List<Id>> mapRelToId = new Map<Id, List<Id>>();
        
        for(AccountContactRelation rela: Database.Query('SELECT AccountId, ContactId FROM AccountContactRelation WHERE ContactId IN :lstCtsIds OR AccountId IN :lstAccsIds')) {
            
            if(!mapRelToId.containsKey(rela.ContactId)) {
                mapRelToId.put(rela.ContactId, new List<Id>{rela.AccountId});
            } else {
                List<Id> ids = new List<Id>();
                ids = mapRelToId.get(rela.ContactId);
                ids.add(rela.AccountId);
                mapRelToId.put(rela.ContactId, ids);
            }
            
        }
        
        if(lstAccsIds.size() > 0) {
            
            Map<String, List<Account>> mapAccsWithSamesSiret = new Map<String, List<Account>>();
            
            for(Account acc: lstAcc) {
                if(!mapAccsWithSamesSiret.containsKey(acc.SIRET_Number__c)) {
                    mapAccsWithSamesSiret.put(acc.SIRET_Number__c, new List<Account>{acc});
                } else {
                    List<Account> accs = new List<Account>();
                    accs = mapAccsWithSamesSiret.get(acc.SIRET_Number__c);
                    accs.add(acc);
                    mapAccsWithSamesSiret.put(acc.SIRET_Number__c, accs);
                }
            }
            
            Set<Account> updateNotMerge = new Set<Account>();
            
            for(String siret: mapAccsWithSamesSiret.keySet()) {
                
                List<Account> tmpAccs = mapAccsWithSamesSiret.get(siret);
                String tempNature;
                String tempIndustry;
                
                if(tmpAccs.size() >= 2) {
                    
                    for(Account acc: tmpAccs) {
                        tempNature = (tempNature == null && acc.CustomerNature__c != null) ? acc.CustomerNature__c : null;
                        tempIndustry = (tempIndustry == null && acc.Industry != null && acc.Industry != 'Aucun') ? acc.Industry : null;
                        
                        if(tempIndustry != null || tempNature != null) {
                            break;
                        }
                        
                    }
                    
                    for(Account acc: tmpAccs) {
                        
                        system.debug(tmpAccs);
                        system.Debug(tempNature != acc.CustomerNature__c);
                        
                        if (acc.ContractRoot__c != null) {
                            updateNotMerge.addAll(tmpAccs);
                            break;
                        } else if ((tempNature != null && acc.CustomerNature__c != tempNature) || (tempIndustry != null && acc.Industry != tempIndustry && acc.Industry != 'Aucun')) {
                            system.Debug('83 : '+acc.Name+' '+acc.Id+' '+acc.SIRET_Number__c);
                            updateNotMerge.addAll(tmpAccs);
                            tempnature = null;
                            tempIndustry = null;
                            break;
                        } else {
                            
                            for(Id id: mapRelToId.keySet()) {
                                if(mapRelToId.get(id).contains(acc.Id)) {
                                    if(mapRelToId.get(id).size() > 1) {
                                        updateNotMerge.addAll(tmpAccs);
                                        break;
                                    }
                                }
                            }
                            
                            /*for(Contact ct: lstCts) {
                                if(ct.AccountId == acc.Id) {
                                    
                                    system.debug(ct);
                                    
                                    List<AccountContactRelation> lstRelations = new List<AccountContactRelation>();
                                    
                                    for(AccountContactRelation rela: lstRela) {
                                        if(rela.ContactId == ct.Id) {
                                            system.debug('rela => '+rela);
                                            lstRelations.add(rela);
                                        }
                                    }
                                    
                                    system.debug('lstRelations => '+lstRelations);
                                    
                                    if(lstRelations.size() > 1) {
                                        updateNotMerge.addAll(tmpAccs);
                                        break;
                                    }
                                    
                                }        
                            }*/
                        }
                        
                    }
                    
                }
                
            }
            
            List<Account> updateAccs = new List<Account>();
            
            for(String siret: mapAccsWithSamesSiret.keySet()) {
                if(mapAccsWithSamesSiret.get(siret).size() >= 2) {
                    updateAccs.addAll(mapAccsWithSamesSiret.get(siret));
                }
            }
            
            for(Account acc: updateAccs) {
                for(Account a: updateNotMerge) {
                    if(a.Id == acc.Id) {acc.TECH_NotToMerge__c = true; break;}
                }
                acc.TECH_IsDuplicate__c = true;
                acc.TECH_IsPassed__c = true;
            }
            
            update updateAccs;
            
        }
        
    }
    
    global void finish(Database.BatchableContext BC){ }
    
}