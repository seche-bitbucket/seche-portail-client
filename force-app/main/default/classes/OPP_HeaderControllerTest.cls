@isTest
public class OPP_HeaderControllerTest {
    
    static Opportunity oppHeader;
    static Opportunity oppInter;
    static Opportunity oppStd;
    static Opportunity oppSubInter;
    static Opportunity oppSubStd;
  
     static void init(){
        
        List<Opportunity> Opps = new List<Opportunity>();
        
        //Get a Record Type for Account
        RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
        
        //Create Account
        Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aucun', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
        
        try{
            insert ac;
        }catch(Exception e){
        }
        
        //Create Contact
        Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '03 00 00 00 00', Salesman__c = UserInfo.getUserId()  );
        
        try{
            insert contactTest;
        }catch(Exception e1){
        }
        
        //Get a Record Type for Opportunity
        
        Map<String, RecordType> RTs = new Map<String, RecordType>(); 
        for(RecordType rtOpp : [Select r.DeveloperName, r.Id From RecordType r  Where SobjectType='Opportunity'])
            RTs.put(rtOpp.DeveloperName, rtOpp);
        
        
        
        //Create Opportunity Header
        oppHeader = new Opportunity(Name = 'Opp_Header', RecordTypeId = RTs.get('OPP_OpportuniteMere').ID , AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
        //Opps.add(oppHeader);
        insert oppHeader;
        
        
        //Create Opportunity Inter 1
        oppInter = new Opportunity(Name = 'Opp_Inter', RecordTypeId = RTs.get('OPP_06_AutreOffreSecteurPriveIntermediaire').ID , Opportunit_mere__c = oppHeader.ID ,AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
        Opps.add(oppInter);
        
        //Create Opportunity Inter 1
        oppStd = new Opportunity(Name = 'Opp_Std', RecordTypeId = RTs.get('OPP_06_AutreOffreSecteurPriveStandard').ID , AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
        Opps.add(oppStd);
        
        //Create Opportunity Sub 1 of Inter 1
        oppSubInter = new Opportunity(Name = 'Opp_Sub1', RecordTypeId = RTs.get('OPP_OpportuniteIntraGroupe').ID , OpportuniteMereIntra__c = oppInter.ID ,AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
        Opps.add(oppSubInter);
        
        //Create Opportunity Sub 2 of Inter 1
        oppSubStd = new Opportunity(Name = 'Opp_Sub2', RecordTypeId = RTs.get('OPP_OpportuniteIntraGroupe').ID , OpportuniteMereIntra__c = oppStd.ID ,AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
        Opps.add(oppSubStd);
        
        
        try{
            insert Opps;
        }catch(Exception e2){
            System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
        }                  
    }
    
    static testMethod void Test_OPP_HeaderControllerHead(){
          init();
        //    Start TEST
        
        
        test.startTest();
        PageReference pageRef = Page.OPP_Header;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',oppHeader.id);
        ApexPages.StandardController sc = new ApexPages.standardController(oppHeader);
        OPP_HeaderController  controller = new OPP_HeaderController(sc);
        System.assertEquals(controller.getOppBackgroundColor(), '#006C51');
        
        test.stopTest();
    }
    
    static testMethod void Test_OPP_HeaderControllerInter(){
          init();
        //    Start TEST
        
        
        test.startTest();
        PageReference pageRef = Page.OPP_Header;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',oppInter.id);
        ApexPages.StandardController sc = new ApexPages.standardController(oppHeader);
        OPP_HeaderController  controller = new OPP_HeaderController(sc);
        System.assertEquals(controller.getOppBackgroundColor(), '#006C51');
        
        test.stopTest();
    }
    
     static testMethod void Test_OPP_HeaderControllerSubStd(){
          init();
        //    Start TEST
        
        
        test.startTest();
        PageReference pageRef = Page.OPP_Header;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',oppSubStd.id);
        ApexPages.StandardController sc = new ApexPages.standardController(oppHeader);
        OPP_HeaderController  controller = new OPP_HeaderController(sc);
        System.assertEquals(controller.getOppBackgroundColor(), '#DB7312');
        
        test.stopTest();
    }
    
     static testMethod void Test_OPP_HeaderControllerStd(){
          init();
        //    Start TEST
        
        
        test.startTest();
        PageReference pageRef = Page.OPP_Header;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',oppStd.id);
        ApexPages.StandardController sc = new ApexPages.standardController(oppHeader);
        OPP_HeaderController  controller = new OPP_HeaderController(sc);
        System.assertEquals(controller.getOppBackgroundColor(), '#DB7312');
        
        test.stopTest();
    }
    
    static testMethod void Test_OPP_HeaderControllerRT(){
          init();
        //    Start TEST
        
        
        test.startTest();
        PageReference pageRef = Page.OPP_Header;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',oppStd.id);
        ApexPages.StandardController sc = new ApexPages.standardController(oppHeader);
        OPP_HeaderController  controller = new OPP_HeaderController(sc);
        System.assertEquals(controller.getOppRTName(), '06 Autre offre au secteur privé (Standard)');
        
        test.stopTest();
    }
    static testMethod void Test_OPP_HeaderControllerFont(){
          init();
        //    Start TEST
        
        
        test.startTest();
        PageReference pageRef = Page.OPP_Header;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',oppStd.id);
        ApexPages.StandardController sc = new ApexPages.standardController(oppHeader);
        OPP_HeaderController  controller = new OPP_HeaderController(sc);
        System.assertEquals(controller.getOppFontColor(), 'white');
        
        test.stopTest();
    }


}