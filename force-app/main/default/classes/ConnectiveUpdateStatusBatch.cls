/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ConnectiveUpdateStatusBatch
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ConnectiveUpdateStatusBatch
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 27-05-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
global class ConnectiveUpdateStatusBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful{
    
    global Database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(
            'SELECT Id, Status__c, FlowId__c,TECH_ExternalId__c,OwnerId,Quote__c,Quote__r.OwnerId , Quote__r.QuoteCode__c, FIP_FIP__r.Name, Package__c, Package__r.ConnectivePackageId__c, Package__r.ConnectivePackageStatus__c'+
            ' FROM DocumentsForSignature__c WHERE Package__r.ConnectivePackageId__c != null AND (Quote__c != null OR FIP_FIP__c != null) AND (Status__c= \'PENDINGSIGNING\' OR Status__c=\'\' OR Status__c=\'PENDING\')'+
            'AND CreatedDate > 2020-09-08T10:00:00.000+0000'
        );
    }

    global void execute(Database.BatchableContext BC, List<DocumentsForSignature__c> scope) {
        PackageResponse.ResponseGetListPackage listPkg = ConnectiveApiManager.getBatch50Package(true,null,null);
        Map<PackageResponse.ResponseItemPackage,Boolean> pkgConnectiveProcessed = new Map<PackageResponse.ResponseItemPackage,Boolean>();
        try{
            ConnectiveAsyncUpdateStatus updateStatus = new ConnectiveAsyncUpdateStatus(scope,listPkg);
            ID jobID = System.enqueueJob(updateStatus);
        }
        catch (Exception e){
            System.debug(LoggingLevel.ERROR, e.getMessage());
        }
    }

    global void finish(Database.BatchableContext BC){}
}