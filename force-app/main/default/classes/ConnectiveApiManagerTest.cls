/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ConnectiveApiManagerTest
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ConnectiveApiManagerTest
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 13-05-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
@isTest
public class ConnectiveApiManagerTest {

    static Quote q;
    static Attachment Att;
    static Contact c;
    static Attachment Att2;
    static FIP_FIP__c fip;
    static DocumentsForSignature__c doc;
    static DocumentsForSignature__c doc2;
    
    public static void initAttachement(){
        Account a;
        Opportunity opp;

        Profile p = [select id from profile where name = :'Chargé(e) d\'affaires' limit 1];

        String testemail = 'currentUserTest@cap.com';

        User us = new User(profileId = p.id, username = testemail, email = testemail,
        SubsidiaryCreationRight__c = 'Alcea',
        emailencodingkey = 'UTF-8', localesidkey = 'en_US',
        languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
        alias='cspu', lastname='lastname');
        insert us;
        
        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        opp.Salesman__c = us.Id;
        insert opp;
        
        q = new Quote();
        //q.id='0Q09E0000000AhESAU';
        q.OpportunityId = opp.ID;
        q.Name = opp.Name;
        q.DateDevis__c = Date.today();
        q.QuoteCode__c=opp.Name;
        q.ContactId = opp.ContactName__c;
        q.Email = opp.ContactName__r.Email;
        q.Phone = opp.ContactName__r.Phone;
        q.Fax = opp.ContactName__r.Fax;
        q.BillingStreet = opp.Account.BillingStreet;
        q.BillingCity = opp.Account.BillingCity;
        q.BillingState = opp.Account.BillingState;
        q.BillingPostalCode = opp.Account.BillingPostalCode;
        q.BillingCountry = opp.Account.BillingCountry; 
        insert q;
        
        att = new Attachment();
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        att.body=bodyBlob;
        att.Name = q.Name;	
        att.ParentId = q.Id;             
        insert att;    
        fip = new FIP_FIP__c();
        fip.Collector__c = a.ID;
        fip.Contact__c = c.ID;
        insert fip;       
    }

    public testmethod static void createInstantPackageTest(){
        initAttachement();
        string pbefore = 'Testing base 64 encode';            
        Blob beforeblob = Blob.valueOf(pbefore);
        
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=fip.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveMockInstantPackage()); 
        ConnectiveMockInstantPackage.externalIdContact = c.Id;
        ConnectiveMockInstantPackage.externalIdDoc = testContent.Id;
        ConnectiveApiManager.createInstantPackage(fip.id);
        Test.stopTest(); 
    }

    public testmethod static void createInstantPackageTestErrorApex(){
        initAttachement();
        string pbefore = 'Testing base 64 encode';            
        Blob beforeblob = Blob.valueOf(pbefore);
        
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=fip.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveMockInstantPackage()); 
        ConnectiveMockInstantPackage.externalIdContact = c.Id;
        //ConnectiveMockInstantPackage.externalIdDoc = testContent.Id;
        ConnectiveApiManager.createInstantPackage(fip.id);
        Test.stopTest(); 
    }

    public testmethod static void updateStatusTest(){
        initAttachement();
        string pbefore = 'Testing base 64 encode';            
        Blob beforeblob = Blob.valueOf(pbefore);
        
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=fip.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;

        Package__c pkg = new Package__c(
            ConnectivePackageId__c='3bb6a2d1-35db-4a3a-a434-868c01bcca9d',
            ConnectivePackageStatus__c='Pending'
        );
        insert pkg;

        DocumentsForSignature__c doc=new DocumentsForSignature__c();
        doc.Name= cv.title;
        doc.SigningUrl__c='';
        doc.SentTo__c=c.Id;
        doc.TECH_ExternalId__c=cv.Id;
        doc.ConnectiveDocumentId__c = cv.Id;
        doc.Status__c = 'PENDING';
        doc.Quote__c=q.Id;
        doc.Package__c = pkg.Id;
        insert doc;
        List<DocumentsForSignature__c> lstDoc = new List<DocumentsForSignature__c>();
        lstDoc.add(doc);

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveMockGetStatus()); 
        ConnectiveMockGetStatus.externalIdContact = c.Id;
        ConnectiveMockGetStatus.externalIdDocument = testContent.Id;
        ConnectiveMockGetStatus.connectiveIdPackage = pkg.ConnectivePackageId__c;
        ConnectiveApiManager.updateStatus(lstDoc);
        Test.stopTest(); 
    }

    public testmethod static void updateStatusTestError(){
        initAttachement();
        string pbefore = 'Testing base 64 encode';            
        Blob beforeblob = Blob.valueOf(pbefore);
        
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=fip.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;

        Package__c pkg = new Package__c(
            ConnectivePackageId__c='3bb6a2d1-35db-4a3a-a434-868c01bcca9d',
            ConnectivePackageStatus__c='Pending'
        );
        insert pkg;

        DocumentsForSignature__c doc=new DocumentsForSignature__c();
        doc.Name= cv.title;
        doc.SigningUrl__c='';
        doc.SentTo__c=c.Id;
        doc.TECH_ExternalId__c=cv.Id;
        doc.ConnectiveDocumentId__c = cv.Id;
        doc.Status__c = 'PENDING';
        doc.Quote__c=q.Id;
        doc.Package__c = pkg.Id;
        insert doc;

        Test.startTest();
        ConnectiveApiManager.getStatusByConnectivePackageId('idpackageFake');
        Test.stopTest(); 
    }

    public testmethod static void updateStatusByFipQuoteTest(){
        initAttachement();
        string pbefore = 'Testing base 64 encode';            
        Blob beforeblob = Blob.valueOf(pbefore);
        
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=fip.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;
        Package__c pkg = new Package__c(
            ConnectivePackageId__c='3bb6a2d1-35db-4a3a-a434-868c01bcca9d',
            ConnectivePackageStatus__c='Pending'
        );
        insert pkg;

        DocumentsForSignature__c doc=new DocumentsForSignature__c();
        doc.Name= cv.title;
        doc.SigningUrl__c='';
        doc.SentTo__c=c.Id;
        doc.TECH_ExternalId__c=cv.Id;
        doc.ConnectiveDocumentId__c = cv.Id;
        doc.Status__c = 'PENDING';
        doc.Quote__c=q.Id;
        doc.Package__c = pkg.Id;
        insert doc;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveMockGetStatus()); 
        ConnectiveMockGetStatus.externalIdContact = c.Id;
        ConnectiveMockGetStatus.externalIdDocument = testContent.Id;
        ConnectiveMockGetStatus.connectiveIdPackage = pkg.ConnectivePackageId__c;
        ConnectiveApiManager.updateStatusByFipQuote(fip.Id);
        Test.stopTest(); 
    }

    public testmethod static void sendReminderTest(){
        initAttachement();
        string pbefore = 'Testing base 64 encode';            
        Blob beforeblob = Blob.valueOf(pbefore);
        
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=fip.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;
        Package__c pkg = new Package__c(
            ConnectivePackageId__c='3bb6a2d1-35db-4a3a-a434-868c01bcca9d',
            ConnectivePackageStatus__c='Pending'
        );
        insert pkg;

        DocumentsForSignature__c doc=new DocumentsForSignature__c();
        doc.Name= cv.title;
        doc.SigningUrl__c='';
        doc.SentTo__c=c.Id;
        doc.TECH_ExternalId__c=cv.Id;
        doc.ConnectiveDocumentId__c = cv.Id;
        doc.Status__c = 'PENDING';
        doc.Quote__c=q.Id;
        doc.Package__c = pkg.Id;
        insert doc;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveMockSendReminder()); 
        ConnectiveApiManager.sendRedminder(q.Id);
        Test.stopTest(); 
    }

    public testmethod static void sendReminderTestError(){
        initAttachement();
        string pbefore = 'Testing base 64 encode';            
        Blob beforeblob = Blob.valueOf(pbefore);
        
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=fip.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;
        Package__c pkg = new Package__c(
            ConnectivePackageId__c='3bb6a2d1-35db-4a3a-a434-868c01bcca9d',
            ConnectivePackageStatus__c='Pending'
        );
        insert pkg;

        DocumentsForSignature__c doc=new DocumentsForSignature__c();
        doc.Name= cv.title;
        doc.SigningUrl__c='';
        doc.SentTo__c=c.Id;
        doc.TECH_ExternalId__c=cv.Id;
        doc.ConnectiveDocumentId__c = cv.Id;
        doc.Status__c = 'PENDING';
        doc.Quote__c=q.Id;
        doc.Package__c = pkg.Id;
        insert doc;

        Test.startTest();
        ConnectiveApiManager.sendRedminder(q.Id);
        Test.stopTest(); 
    }

    public testmethod static void getBatch50PackageTest(){
        initAttachement();
        string pbefore = 'Testing base 64 encode';            
        Blob beforeblob = Blob.valueOf(pbefore);
        
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=fip.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;
        Package__c pkg = new Package__c(
            ConnectivePackageId__c='3bb6a2d1-35db-4a3a-a434-868c01bcca9d',
            ConnectivePackageStatus__c='Pending'
        );
        insert pkg;

        DocumentsForSignature__c doc=new DocumentsForSignature__c();
        doc.Name= cv.title;
        doc.SigningUrl__c='';
        doc.SentTo__c=c.Id;
        doc.TECH_ExternalId__c=cv.Id;
        doc.ConnectiveDocumentId__c = cv.Id;
        doc.Status__c = 'PENDING';
        doc.Quote__c=q.Id;
        doc.Package__c = pkg.Id;
        insert doc;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveMockGet50Status()); 
        //Boolean firstBatch,Integer continuationToken,Integer total
        ConnectiveApiManager.getBatch50Package(true,0,0);
        Test.stopTest(); 
    }

    public testmethod static void getBatch50PackageTestError(){
        initAttachement();
        string pbefore = 'Testing base 64 encode';            
        Blob beforeblob = Blob.valueOf(pbefore);
        
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=fip.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;
        Package__c pkg = new Package__c(
            ConnectivePackageId__c='3bb6a2d1-35db-4a3a-a434-868c01bcca9d',
            ConnectivePackageStatus__c='Pending'
        );
        insert pkg;

        DocumentsForSignature__c doc=new DocumentsForSignature__c();
        doc.Name= cv.title;
        doc.SigningUrl__c='';
        doc.SentTo__c=c.Id;
        doc.TECH_ExternalId__c=cv.Id;
        doc.ConnectiveDocumentId__c = cv.Id;
        doc.Status__c = 'PENDING';
        doc.Quote__c=q.Id;
        doc.Package__c = pkg.Id;
        insert doc;

        Test.startTest();
        //Boolean firstBatch,Integer continuationToken,Integer total
        ConnectiveApiManager.getBatch50Package(true,0,0);
        Test.stopTest(); 
    }

    public testmethod static void getResponsePackage(){
        
        PackageResponse.ResponseLocation resploc= new PackageResponse.ResponseLocation();
        resploc.Id = 'test';
        resploc.Label = 'test';
        resploc.PageNumber =1;

        PackageResponse.ResponsePackageDocument respDoc = new PackageResponse.ResponsePackageDocument();
        respDoc.DocumentId = '';
        respDoc.DocumentType = '';
        respDoc.ExternalDocumentReference = '';
        respDoc.DocumentName = ''; 
        respDoc.Locations = new List<PackageResponse.ResponseLocation>();

        PackageResponse.ResponseLocationActor respLocActo = new PackageResponse.ResponseLocationActor();
            respLocActo.Id = '';
            respLocActo.UsedSigningType = '';
        
        PackageResponse.ResponseActor respact = new  PackageResponse.ResponseActor();
            respact.Type = '';
            respact.Reason = '';
            respact.CompletedBy = '';
            respact.CompletedTimestamp = '';
            respact.Locations = new List< PackageResponse.ResponseLocationActor>();
            respact.ActorId = '';
            respact.ActionUrl = '';
            respact.ActorStatus = '';

        PackageResponse.ResponseStakeholder restk = new  PackageResponse.ResponseStakeholder();
            restk.Type = '';
            restk.EmailAddress = '';
            restk.ContactGroupCode = '';
            restk.ExternalStakeholderReference = '';
            restk.StakeholderId = '';
            restk.Actors = new List< PackageResponse.ResponseActor>();

        PackageResponse pkg = new PackageResponse();
            pkg.PackageId = 'test';
            pkg.PackageName = 'test';
            pkg.DocumentGroupCode = 'test';
            pkg.Initiator = 'test';
            pkg.ExpiryTimestamp = 'test';
            pkg.ExternalPackageReference = 'test';
            pkg.F2FSigningUrl = 'test';
            pkg.PackageStatus = 'test';
            pkg.PackageDocuments = new List<PackageResponse.ResponsePackageDocument>();
            pkg.Stakeholders = new List<PackageResponse.ResponseStakeholder>();
            pkg.CreationTimestamp = '';
            pkg.Warnings = new list<String>{'test'};
    }

    public testmethod static void getDocumentTest(){
        initAttachement();
        string pbefore = 'Testing base 64 encode';            
        Blob beforeblob = Blob.valueOf(pbefore);
        
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=fip.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;
        Package__c pkg = new Package__c(
            ConnectivePackageId__c='3bb6a2d1-35db-4a3a-a434-868c01bcca9d',
            ConnectivePackageStatus__c='Pending'
        );
        insert pkg;

        DocumentsForSignature__c doc=new DocumentsForSignature__c();
        doc.Name= cv.title;
        doc.SigningUrl__c='';
        doc.SentTo__c=c.Id;
        doc.TECH_ExternalId__c=cv.Id;
        doc.ConnectiveDocumentId__c = cv.Id;
        doc.Status__c = 'PENDING';
        doc.Quote__c=q.Id;
        doc.Package__c = pkg.Id;
        insert doc;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveMockDocument()); 
        //Boolean firstBatch,Integer continuationToken,Integer total
        ConnectiveApiManager.getDocument(q.Id,pkg.ConnectivePackageId__c,'');
        Test.stopTest();
    }

    public testmethod static void generateEmailTest(){
        initAttachement();

        DocumentsForSignature__c doc=new DocumentsForSignature__c();
        doc.Name= 'titre';
        doc.SigningUrl__c='';
        doc.SentTo__c=c.Id;
        doc.Status__c = 'PENDING';
        doc.Quote__c=q.Id;
        doc.TECH_ExternalId__c = q.Id;
        insert doc;

        Map<Quote,DocumentsForSignature__c> mapQDocSi = new Map<Quote,DocumentsForSignature__c>();
        mapQDocSi.put(q,doc);

        Test.startTest();
        ConnectiveApiManager.generateEmail(mapQDocSi);
        Test.stopTest();
    }
    
}