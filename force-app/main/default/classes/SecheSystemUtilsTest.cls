@Istest
public with sharing class SecheSystemUtilsTest {
    @isTest
    public static void testGUIDAndUploadAndDelete() {
        ByPassUtils.ByPass('QuoteTrigger');
            
        QteLineItemSyncTriggerV2Handler.stop();
        OppLineItemSyncTriggerV2Handler.stop();
        QuoteSyncTriggerV2Handler.stop();
        OppSyncTriggerV2Handler.stop();
        
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        //Quote Section
        Opportunity opp = TestFactory.generateOpport(acc, null);
        Form__c form = TestFactory.generateForm(opp, ct);

        insert new URL__c(SITE_Prefip__c='test@test.com');

        opp.GUID__c = 'TEST001';
        opp.ContactName__c = ct.Id;
        update opp;

        SObject opp1 = SecheSystemUtils.getRecordByGUID('TEST001', 'Opportunity', 'GUID__c');
        System.assert(opp1.get('Id') == opp.Id);

        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];

        Id RecordTypeId = [SELECT id, name from RecordType where Name like 'Attestation_Producteur_DND'].Id;

        User currentUser = new User(profileId = p.Id, username = 'uhuehdurfurhufhru@gmail.com', email = 'uhuehdurfurhufhru@gmail.com',
        emailencodingkey = 'UTF-8', localesidkey = 'en_US',
        languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
        alias='cspu', lastname='lastname', SocieteF_t__c = true);

        System.RunAs(currentUser){
            SecheSystemUtils.FormulaireId formId = new SecheSystemUtils.FormulaireId();
            formId.formulaireId = form.Id;
            Attestation__c att = new Attestation__c(Account__c = acc.Id, Sorted__c = true);
            att.Contact__c = ct.Id;
            att.Producer__c = true;
            att.Sorted__c = true;
            att.Mixed__c = false;
            att.RecordTypeId = RecordTypeId;
            att.WasteOwner__c = false;
            att.CAPNumber__c = '123456789';
            att.Renewal__c = 'Oui';
            insert att;
            Attestation__c att1 = new Attestation__c(Account__c = acc.Id);
            att1.Contact__c = ct.Id;
            att1.Producer__c = true;
            att1.Sorted__c = true;
            att1.Mixed__c = false;
            att1.RecordTypeId = RecordTypeId;
            att1.WasteOwner__c = false;
            insert att1;
            formId.prefips = new List<Attestation__c>{att, att1};
            List<SecheSystemUtils.FormulaireId> lst = new List<SecheSystemUtils.FormulaireId>{FormId};
    
            SecheSystemUtils.genGuid(lst);
        }

        
    }
}