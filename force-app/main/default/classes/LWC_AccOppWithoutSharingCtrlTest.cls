/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : LWC_AccOppWithoutSharingCtrlTest
* -- - Author : MGR
* -- - Company : Capgemini
* -- - Purpose : LWC_AccOppWithoutSharingCtrlTest
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 08-06-2020 MGR 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/ 
 @isTest
 global with sharing class LWC_AccOppWithoutSharingCtrlTest {

    @isTest
    global static void getOpportunitiesWithoutSharingByAccount_Test(){
        Account acc = TestDataFactory.createAccount('testAccount');
        Test.startTest();
        insert acc;
        Opportunity opp = TestDataFactory.createOpportunity('testOpp');
        opp.AccountId = acc.id;
        opp.OwnerId = UserInfo.getUserId();
        insert opp;
        System.debug('##########'+acc.Id);
        List<AggregateResult> lst = LWC_AccOppWithoutSharingCtrl.getOpportunitiesWithoutSharingByAccount(acc.Id);
        System.assertEquals(lst.size(),1);
        Test.stopTest();
    }
}