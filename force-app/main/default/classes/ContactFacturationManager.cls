public without sharing class ContactFacturationManager {
    
    @AuraEnabled(cacheable=true) 
    public static AutomationInputs__c getAutomationInputsSettings(){
        return AutomationInputs__c.getOrgDefaults();
    }

    @AuraEnabled
    public static Boolean createCase(Map<String, Object> fields) {
        Case c = new Case();
        c.RecordTypeId = (String)fields.get('RecordTypeId');
        c.Status = (String)fields.get('Status');
        c.Origin = (String)fields.get('Origin');
        c.Definition__c = (String)fields.get('Definition__c');
        c.ContactId = (String)fields.get('ContactId');
        c.Subject = (String)fields.get('Subject');
        c.Reason = (String)fields.get('Reason');
        c.Description = (String)fields.get('Description');
        c.SuppliedEmail = (String)fields.get('SuppliedEmail');
        c.Salesman__c = (String)fields.get('Salesman__c');
        c.OwnerId = (String)fields.get('OwnerId');
        c.BillingSiteCode__c = (String)fields.get('BillingSiteCode__c');
        ByPassUtils.ByPass('CaseTrigger');
        upsert c;

        string customerCode ='';
        Case insertedCase = [SELECT Id, Contact.Id, OwnerId, CaseNumber, Contact.ERP_CustomerCode__c FROM Case WHERE Id = :c.Id FOR UPDATE];
        Contact  contacts = [SELECT ID ,ERP_CustomerCode__c  FROM Contact WHERE ID =: insertedCase.Contact.Id] ;
        
        User u = [SELECT Name, FirstName, LastName, Email FROM User WHERE Id = :c.OwnerId];

        String orgURL = [SELECT Org_URL__c FROM URL__c].Org_URL__c;
        String caseLink = orgURL + 'lightning/r/Case/' + insertedCase.Id + '/view';
        if(contacts.ERP_CustomerCode__c != null) { customerCode =  'Code client '+contacts.ERP_CustomerCode__c + ' : ';}
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String owa = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = 'Séché Connect'].Id;
        //String email = [SELECT Email FROM User WHERE Id = :insertedCase.OwnerId].Email;
        String htmlBody = '<p>Bonjour ' + u.FirstName + ' ' + u.LastName + '.<br/>La requête N° ' + insertedCase.CaseNumber + ' vient de vous être attribuée.<br> ' + customerCode + '' + caseLink + '.</p>';
        mail.setOrgWideEmailAddressId(owa);
        //mail.setTargetObjectId(insertedCase.OwnerId);
        mail.setWhatId(insertedCase.Id);
        mail.setToAddresses(new String[] {u.Email});
        mail.setSubject('Nouvelle demande de modification des données de facturation');
        mail.setHtmlBody(htmlBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        return true;
    }

    @AuraEnabled
    public static Boolean updateContact(Id contactId, Map<String, Object> fields) {
        Contact c = [SELECT Id FROM Contact WHERE Id = :contactId];
        c.ERP_BillingPhone__c = (String)fields.get('ERP_BillingPhone__c');
        c.PrimaryBillingContactEmail__c = (String)fields.get('PrimaryBillingContactEmail__c');
        c.SecondaryBillingContactEmail__c = (String)fields.get('SecondaryBillingContactEmail__c');
        c.GUID__c='';
        c.BillingContactUpdateLink__c='';
        update c;
        return true;
    }
}