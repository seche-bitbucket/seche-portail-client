/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ToolsDataAccess
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ToolsDataAccess
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- ----------------------------------------------------------------------------------------------------------------
* -- 27-02-2020 PMB 1.0 - Initial version
* -- 18-03-2020 PMB 1.2 - getOpportunitiesWithoutSharringByAccount
* -- 09-04-2020 PMB 1.3 - getOpportunitiesWithoutSharringByAccount Update
* -- 08-06-2020 MGR 1.4 - Deletion of method getOpportunitiesWithoutSharringByAccount (which is now in the class LWC_AccOppWithoutSharingCtrl)
* --------------------------------------------------------------------------------------------------------------------------------------------
*/
public with sharing class ToolsDataAccess implements Database.AllowsCallouts{
    public static Id getRecordTypeIdByName(SObjectType obj, String developperName){  
        return obj.getDescribe().getRecordTypeInfosByDeveloperName().get(developperName).getRecordTypeId();
    }


    /*public static HttpResponse runREST_SF_API_DeleteMethod(String service,String param){  
        return runREST_SF_API(service,param,Constants.STR_TDA_METHOD_DELETE);
    }

    public static HttpResponse runREST_SF_API(String service,String param,String method){
        String token = ToolsDataAccess.getTokenAccess();
        String endPointURL = Label.ORG_URL+service+param;
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPointURL);
        req.setMethod(method);
        req.setHeader('Authorization','Bearer '+token);
        HttpResponse res = h.send(req);
        return res;
    }

     
    public static String getTokenAccess() {
        system.debug('FIPFormController.getTokenAccess = START');
        String endPointURL = Label.ORG_URL_LOGIN+'/services/oauth2/token';
        system.debug('endPointURL = '  + endPointURL);
        
        String tokenSF = '';
        Boolean found = false;
        String username = Label.USR_API_Portal;
        system.debug('username = '  + username);
        
        String password = Label.USR_API_Password;
        system.debug('password = '  + password);
        
        String clientID = Label.API_ClientID;
        system.debug('clientID = '  + clientID);
        
        String clientSecret = Label.API_Client_Secret;
        system.debug('clientSecret = '  + clientSecret);
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPointURL+'?grant_type=password&client_id='+clientID+'&client_secret='+clientSecret+'&username='+username+'&password='+password);
        req.setMethod('POST');
        HttpResponse res = h.send(req);
        system.debug('res2 = '  + res.getBody());
        
        
        if(res.getStatusCode() == 200){
            //Parse JSON to retrive Token
            System.JSONToken token;
            string text;
            JSONParser parser = JSON.createParser(res.getBody());
            System.debug('test restGetBody = '+res.getBody());
            parser.nextToken(); 
            while((token = parser.nextToken()) != null && !found) {
                
                text = parser.getText();
                if (token == JSONToken.FIELD_Name && text == 'access_token') {
                    token=parser.nextToken();
                    tokenSF = parser.getText();
                    found = true;
                }
            }
        }
        System.debug('test tokenSF = '+tokenSF);
        
        if(tokenSF != ''){
            system.debug('FIPFormController.getTokenAccess = END');
            return tokenSF;   
            
        }else{
            system.debug('FIPFormController.getTokenAccess = END2');
            return 'ERROR';
        }
        
    }*/
    
}