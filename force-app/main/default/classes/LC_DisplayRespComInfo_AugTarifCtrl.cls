public with sharing class LC_DisplayRespComInfo_AugTarifCtrl {
    @AuraEnabled 
    public static user fetchUserDetail(Id userId){
        return [Select id,Name,SmallPhotoUrl, FullPhotoUrl
                From User
                Where Id =: userId];
    }
}