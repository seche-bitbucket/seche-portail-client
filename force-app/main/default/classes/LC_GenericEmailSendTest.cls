@isTest(SeeAllData=false)
public class LC_GenericEmailSendTest {
    
    @isTest
    public static void testQte() {

        ByPassUtils.ByPass('QuoteTrigger');
            
        QteLineItemSyncTriggerV2Handler.stop();
        OppLineItemSyncTriggerV2Handler.stop();
        QuoteSyncTriggerV2Handler.stop();
        OppSyncTriggerV2Handler.stop();
        
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        ConnectiveV4Mock.c = ct;
        
        //Quote Section
        Opportunity opp = TestFactory.generateOpport(acc, null);

        Quote qte = TestFactory.generateQuote(ct, acc, opp);

        opp.SyncedQuoteId = qte.Id;
        update opp;

        Attachment att2 = TestFactory.linkAttachement(qte, 'testQuote - packageOK');

        ContentDocumentLink cdl2 = TestFactory.linkDoc(qte.Id, 'testQuote - packageOK');

        Map<String,Object> result2 = LC_GenericEmailSend.genericGetEmailPrev(qte.Id);
		System.debug('######122'+result2.get('result2'));
        System.assertEquals('true', result2.get('isShowForm'));
        
        System.debug('######125'+result2.get('contactId'));
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveV4Mock());
        Map<String,String> email_qte = LC_GenericEmailSend.genericCreatePackage((String)result2.get('contactId'),
                                                                                    (String)result2.get('contactName'),
                                                                                    (String)result2.get('replyTo'),
                                                                                    (String)result2.get('toAddresses'),
                                                                                    (String)result2.get('ccaddresses'),
                                                                                    (String)result2.get('senderDisplayName'),
                                                                                    (String)result2.get('subject'),
                                                                                    (String)result2.get('body'),
                                                                                    (String)qte.Id,
                                                                                    (List<String>)result2.get('fileId'),
                                                                               		null);

        System.debug(email_qte.get('message'));
		Test.stopTest();
    }
    
    @isTest
    public static void testQteNoFiles() {

        ByPassUtils.ByPass('QuoteTrigger');
            
        QteLineItemSyncTriggerV2Handler.stop();
        OppLineItemSyncTriggerV2Handler.stop();
        QuoteSyncTriggerV2Handler.stop();
        OppSyncTriggerV2Handler.stop();
        
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
                
        ConnectiveV4Mock.c = ct;
        
        //Quote Section
        Opportunity opp = TestFactory.generateOpport(acc, null);

        Quote qte = TestFactory.generateQuote(ct, acc, opp);

        opp.SyncedQuoteId = qte.Id;
        update opp;

        Attachment att2 = TestFactory.linkAttachement(qte, 'testQuote - packageOK');

        ContentDocumentLink cdl2 = TestFactory.linkDoc(qte.Id, 'testQuote - packageOK');

        Map<String,Object> result2 = LC_GenericEmailSend.genericGetEmailPrev(qte.Id);

        System.assertEquals('true', result2.get('isShowForm'));
        
        System.debug(result2.get('contactId'));
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveV4Mock());
        Map<String,String> email_qte = LC_GenericEmailSend.genericCreatePackage((String)result2.get('contactId'),
                                                                                    (String)result2.get('contactName'),
                                                                                    (String)result2.get('replyTo'),
                                                                                    (String)result2.get('toAddresses'),
                                                                                    (String)result2.get('ccaddresses'),
                                                                                    (String)result2.get('senderDisplayName'),
                                                                                    (String)result2.get('subject'),
                                                                                    (String)result2.get('body'),
                                                                                    (String)qte.Id,
                                                                                    null,
                                                                                	null);

        System.debug(email_qte.get('message'));
		Test.stopTest();
    } 
    
    @isTest
    public static void testMass() {
        
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        Opportunity opp1 = TestFactory.generateOpport(acc, 'Proposition commerciale');
        Quote qte1 = TestFactory.generateQuote(ct, acc, opp1);
        ContentDocumentLink cdl1 = TestFactory.linkDoc(qte1.Id, 'testQuote - packageOK');
        
        List<Quote> lstQte = new List<Quote>{qte1};
        
        String result = LC_GenericEmailSend.checkAndSendQuoteRecords(lstQte);
        System.assertEquals('L’envoi pour signature a été exécuté avec succès', result);
        
        Quote qte4 = TestFactory.generateQuoteWithUID(ct, acc, opp1);    
        List<Quote> lstQte3 = new List<Quote>{qte4};
            
        String result4 = LC_GenericEmailSend.checkAndSendQuoteRecords(lstQte3);
        System.assertEquals('Vous avez sélectionné un devis déjà envoyé pour signature', result4);
 
    }
    
    @isTest
    public static void testGetMass() {
        
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        Opportunity opp1 = TestFactory.generateOpport(acc, 'Proposition commerciale');
        Quote qte1 = TestFactory.generateQuote(ct, acc, opp1);
        opp1.SyncedQuoteId = qte1.id;
        update opp1;
        
        List<Quote> lstQte = LC_GenericEmailSend.getQuoteRecords();
        System.assertEquals(0, lstQte.size());
 
    }
    
    @isTest
    public static void testGetMassReminder() {
        
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        Opportunity opp1 = TestFactory.generateOpport(acc, 'Proposition commerciale');
        Quote qte1 = TestFactory.generateQuoteSent(ct, acc, opp1);
        opp1.SyncedQuoteId = qte1.id;
        update opp1;
        
        List<Quote> lstQte = LC_GenericEmailSend.getQuoteRecordsReminder();
        System.assertEquals(0, lstQte.size());
        
        String result = LC_GenericEmailSend.SendReminders(new List<Quote>{qte1});
        System.assertEquals('L’envoi de rappel a été exécuté avec succès', result);
 
    }

}