@isTest(SeeAllData=true)
public class TRANSFO_AddPriceBookEntriesControllerT {
    
    //FINISHED
    @isTest
    public static void getPriceBookEntriesTest(){
        Test.startTest();
        
         //create Waste_Statement
         WST_Waste_Statement__c vWst = TestDataFactory.createWasteStatement('AccountName1');
         
         //Insert Waste_Statement
         insert vWst;
        
        //call the method
        List<QuoteLine__c> vLquoteLine = new List<QuoteLine__c>();
        vLquoteLine = TRANSFO_AddPriceBookEntriesController.getPriceBookEntries(vWst.Id);

        
        //check the result
        System.debug('getPriceBookEntriesTest.vLquoteLine' + vLquoteLine);  
        System.assertNotEquals(null, vLquoteLine);

        Test.stopTest();

    }
    
    //FINISHED
    @isTest
    public static void  saveQuoteLinesTest(){

        Test.startTest(); 
        
        //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        
        //insert Waste_Statement
        insert vWst1;
        
        
        //Create Service Demande
        String name = 'AccountName1';
        Account acc = TestDataFactory.createAccountWithInsert(name);

        SD_ServiceDemand__c vServiceD = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Repairs');
        
        //insert Service Demande
        insert vServiceD;
        
        
        List<QuoteLine__c> vLquoteLine = new List<QuoteLine__c>();
        List<QuoteLine__c> vLquoteLine2 = new List<QuoteLine__c>();
        
         //Create QuoteLine  
        QuoteLine__c vQuoteline = new QuoteLine__c();
 
         
        //link QuoteLine to ServiceDemande
        vQuoteline.ServiceDemand__c = vServiceD.Id;
          
        //link QuoteLine to TECH_PriceBookEntryID__c  
        vQuoteline.TECH_PriceBookEntryID__c='01u9E00000A0sGcQAJ';
        
        vQuoteline.Quantity__c = 1;
        vQuoteline.Code__c='';
          
        
        insert vQuoteline;
        
        vLquoteLine.add(vQuoteline);
        update vLquoteLine;
        
        //call method
        List<QuoteLine__c> vResult = TRANSFO_AddPriceBookEntriesController.saveQuoteLines(JSON.serialize(vLquoteLine2),JSON.serialize(vLquoteLine),vWst1.id);
            
        //check the result
        System.debug('saveQuoteLinesTest.vResult = ' + vResult);
        System.assertNotEquals(null, vResult);
        
        Test.stopTest();
    }
    
    
     //FINISHED
    @isTest
    public static void  getQuoteLinesTest(){
         Test.startTest(); 
         
         //create Waste_Statement
         WST_Waste_Statement__c vWst = TestDataFactory.createWasteStatement('AccountName1');
         
         //Insert Waste_Statement
         insert vWst;
         System.debug('vWst.id = ' + vWst.id);
         
         //Create opportunity
		 Opportunity vOpportunity = TestDataFactory.createOpportunity('OpportunityName1');    
         
         //insert opportunity
         insert vOpportunity;
         
         //link Waste_Statement to opportunity
          vWst.Opportunity__c = vOpportunity.Id;
         
         //create Quote
         Quote vQuote =new Quote();
         
         vQuote.OpportunityId=vOpportunity.Id;
         vQuote.Status= 'Other';
         vQuote.Name='QuoteName';
         insert vQuote;
         System.debug('vQuote = ' + vQuote.Id);
	     System.debug('vQuote.Status = ' + vQuote.Status);
         
    
         //link opportunity to quote
         vOpportunity.SyncedQuoteId = vQuote.Id;
         
	     //Update Object
         update vOpportunity;
         update vWst;
         
         //check the result
         //call method
        Map<ID, QuoteLine__c> vResult = TRANSFO_AddPriceBookEntriesController.getQuoteLines(vWst.id);
          
        System.debug('getQuoteLinesTest.vResult = ' + vResult);
         Test.stopTest();
    }
    
    

}