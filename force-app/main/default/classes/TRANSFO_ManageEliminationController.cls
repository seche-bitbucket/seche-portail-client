public class TRANSFO_ManageEliminationController {
    
    private static String regionEnedis = '';
    private static Decimal grandTotal = 0;
    private static Decimal KVA = 0;
    private static String ServiceDemandTypeOrigin ='';
    private static ID WstRecordTypeID;
    private static WST_Waste_Statement__c WST_wst;
    private static WST_Waste_Statement__c wstToManage;
    private static SD_ServiceDemand__c newSd;
    
    public static boolean sendWasteInstanceToElimination(ID wstID){
       
        //Generate All object needed to transform SD to Elimination
        WST_wst= getAllDetails(wstID);
        ElimObject eo = getObjects(WST_wst);
        wstToManage = eo.wasteStatement;
        System.debug('////wstToManage 1 '+ wstToManage.KVA__c);
       
        
        
        //Case where WST is a depollution Recordtype
        //Action : Erase created opportunity & Quote
        //         Remove WST for its ServiceDemand
        //         Recreate
        //          a ServiceDemand with Elimination Type
        System.debug('////wstToManage.RecordTypeId : '+WstRecordTypeID);
        if(WstRecordTypeID == Label.RCT_RT_WST_Remediation){
            //Remove All Opportunity Line Items
            removeOpportunityLineItem(eo.Opp.ID);
            //Remove All QuoteLine__c of WST
            removeQuoteLine(eo.wasteStatement.ID);
            
            Opportunity oppResult = new Opportunity();
            Quote qResult = new Quote();
            
            newSd = generateNewElimSD(eo.serviceDemand);
            WST_Waste_Statement__c wst = updateWasteStatement( eo.wasteStatement , newSd);
            
            if(eo.Opp.ID != null){
                oppResult = updateOpportunity(eo.Opp, newSd);
                qResult = updateQuote(eo.qte);
            }
            
            if(oppResult != null && oppResult != null){
                return true;
            }else{
                return false;
            }
        }else{
            //Case where WST is a repairs Recordtype
            //Action : Erase created opportunity & Quote
            //         Remove WST for its ServiceDemand
            //         Recreate a ServiceDemand with Elimination Type
            
            //Remove All QuoteLine__c of WST
            removeQuoteLine(eo.wasteStatement.ID);
            
            if(eo.Opp.ID != null){
                //Remove All Diagnostic Opportunity Line Items
                removeDiagnosticOpportunityLineItem(eo.Opp.ID);
                
                //Edit OLI name to explain the reason
                renameTransportOpportunityLineItem(eo.Opp.ID, WST_wst);
                
                return updateOpportunityLineItemForRepairs(eo.Opp);
            }else{
                List<OpportunityLineItem> olisToInsert = new List<OpportunityLineItem>();
                List<WST_Waste_Statement__c> wstList = new List<WST_Waste_Statement__c>();
                wstList.add(WST_wst);
                List<Opportunity> opps = WST_ManageOpportunitiesController.createOpportunities(wstList, true);
                WST_ManageOpportunitiesController.createQuotes(wstList, opps);
                for(Opportunity opp : opps){
                    updateOpportunity(opp);               
                    List<OpportunityLineItem> olis = WST_ManageOpportunitiesController.generateGenericRepairsOLI(opp, wstToManage, false, true);
                    
                    insert olis;
                }
                
            }
            return true;
            
        }
        
        
        
        
    }
    
    private static WST_Waste_Statement__c getAllDetails(ID wstID){
        System.debug('////Function GATHER DETAILS');
        
        WST_Waste_Statement__c wst = [Select ID, Name, KVA__c, RecordTypeID, PriceBook__c, ServiceDemande__c,
                                      ElectricTestComments__c, 	CaracteristicComments__c,
                                      ServiceDemande__r.OwnerID, 
                                      ServiceDemande__r.ApplicantCenter__c,
                                      ServiceDemande__r.ApplicantCenter__r.Name,
                                      ServiceDemande__r.ApplicantCenter__r.OwnerID,
                                      ServiceDemande__r.ApplicantCenter__r.EnedisRegion__c,
                                      ServiceDemande__r.RemovalPostalCode__c,
                                      ServiceDemande__r.ConfirmedRemovalDate__c,
                                      ServiceDemande__r.FavoriteRemovalDate__c,
                                      ServiceDemande__r.Email__c,
                                      ServiceDemande__r.ContactEmail__c,
                                      ServiceDemande__r.Fax__c,
                                      ServiceDemande__r.ContactFax__c,
                                      ServiceDemande__r.PriceBook__c,
                                      ServiceDemande__r.IsSameLocalContact__c,
                                      ServiceDemande__r.BusinessNumberTREDI__c,
                                      ServiceDemande__r.OrderNumber__c,
                                      ServiceDemande__r.LocalContact__c,
                                      ServiceDemande__r.ResponsibleName__c,
                                      ServiceDemande__r.Mobile__c,
                                      ServiceDemande__r.ContactMobile__c,
                                      ServiceDemande__r.RemovalStreet__c,
                                      ServiceDemande__r.Status__c,
                                      ServiceDemande__r.Phone__c,
                                      ServiceDemande__r.ContactPhone__c,
                                      ServiceDemande__r.BilledAccount__c,
                                      ServiceDemande__r.RemovalCity__c,
                                      ServiceDemande__r.ResponsibleName__r.Email,
                                      ServiceDemande__r.ResponsibleName__r.Phone,
                                      ServiceDemande__r.ResponsibleName__r.Fax,
                                      ServiceDemande__r.ApplicantCenter__r.BillingStreet,
                                      ServiceDemande__r.ApplicantCenter__r.BillingCity,
                                      ServiceDemande__r.ApplicantCenter__r.BillingState,
                                      ServiceDemande__r.ApplicantCenter__r.BillingPostalCode,
                                      ServiceDemande__r.ApplicantCenter__r.BillingCountry,
                                      ServiceDemande__r.BilledAccount__r.BillingStreet,
                                      ServiceDemande__r.BilledAccount__r.BillingCity,
                                      ServiceDemande__r.BilledAccount__r.BillingState,
                                      ServiceDemande__r.BilledAccount__r.BillingPostalCode,
                                      ServiceDemande__r.BilledAccount__r.BillingCountry,
                                      Opportunity__c,
                                      Opportunity__r.Name,
                                      Opportunity__r.Informations__c,
                                      Opportunity__r.Pricebook2ID,
                                      Opportunity__r.SyncedQuoteID, 
                                      Opportunity__r.SyncedQuote.Status,
                                      Opportunity__r.SyncedQuote.OpportunityID,
                                      Opportunity__r.SyncedQuote.IsSyncing,
                                      Opportunity__r.SyncedQuote.OwnerID,
                                      Opportunity__r.SyncedQuote.Name,
                                      Opportunity__r.SyncedQuote.GrandTotal,
                                      Opportunity__r.SyncedQuote.DateDevis__c,
                                      Opportunity__r.SyncedQuote.Description,
                                      Opportunity__r.SyncedQuote.Quote_Type__c,
                                      Opportunity__r.SyncedQuote.ContactID,
                                      Opportunity__r.SyncedQuote.Email,
                                      Opportunity__r.SyncedQuote.Phone,
                                      Opportunity__r.SyncedQuote.Fax,
                                      Opportunity__r.SyncedQuote.BillingStreet,
                                      Opportunity__r.SyncedQuote.BillingPostalCode,
                                      Opportunity__r.SyncedQuote.BillingCity,
                                      Opportunity__r.SyncedQuote.BillingCountry,
                                      Opportunity__r.SyncedQuote.ShippingStreet,
                                      Opportunity__r.SyncedQuote.ShippingPostalCode,
                                      Opportunity__r.SyncedQuote.ShippingCity,
                                      Opportunity__r.SyncedQuote.ShippingCountry
                                      FROM WST_Waste_Statement__c WHERE ID = : wstID];
        
        WstRecordTypeID = wst.RecordTypeId;
        
        return wst;
    }
    
    private static ElimObject getObjects(WST_Waste_Statement__c wst){
        System.debug('////Function GENERATE OBJECT');
        ElimObject eo = new ElimObject();
        eo.serviceDemand = getCurrentSD(wst);   
        eo.wasteStatement = getCurrentWST(wst);
        eo.opp = getCurrentOpp(wst);
        eo.qte = getCurrentQuote(wst);
        grandTotal = wst.Opportunity__r.SyncedQuote.GrandTotal;
        return eo;
    }
    
    private static void removeOpportunityLineItem(ID oppID){
        System.debug('////Function DELETE OLI');
        List<OpportunityLineItem> olis = [Select ID FROM OpportunityLineItem WHERE OpportunityID =: oppID];
            
        if(olis.size() > 0){
            delete olis;
        }            

    }
    
    private static void removeDiagnosticOpportunityLineItem(ID oppID){
        System.debug('////Function DELETE DIAGNOSTIC OLIs');
        List<OpportunityLineItem> olis = [Select ID FROM OpportunityLineItem WHERE OpportunityID =: oppID AND Product2.Param_1_Num_Max__c =null];
            
        if(olis.size() > 0){
            delete olis;
        }            

    }
    
    private static void removeQuoteLine(ID wstID){
        System.debug('////Function DELTE QL');
        List<QuoteLine__c> qls = [Select ID FROM QuoteLine__c WHERE Transformateur__c =: wstID];
            
        if(qls.size() > 0){
            delete qls;
        }            

    }
    
    private static void renameTransportOpportunityLineItem(ID oppID, WST_Waste_Statement__c wstToElim){
        System.debug('////Function Rename OLI to pute destruction needed');
        List<OpportunityLineItem> olis = [Select ID,Name, Description, Product2.Name FROM OpportunityLineItem WHERE OpportunityID =: oppID AND Product2.Param_1_Num_Max__c !=null AND Nature__c = 'Traitement'];
        List<OpportunityLineItem> olisToUpdate = new  List<OpportunityLineItem>();
        String reason = getEliminationReason(wstToElim.CaracteristicComments__c, wstToElim.ElectricTestComments__c);
        for(OpportunityLineItem oli : olis){
            System.debug('OLI Name : '+oli.name);
            System.debug('OLI Name : '+oli.Product2.Name);
            oli.Description = oli.Product2.Name + ' Non réparable, à détruire.'+reason; 
            olisToUpdate.add(oli);
        }            
		update olisToUpdate;
    }
    
    private static String getEliminationReason(String carcCom, String elecCom){
        String reason = ' Raison : ';
        
        //Check if all comments are empty
        if(carcCom == '' && elecCom == ''){
            return '';
        //Check if all comments are filled 
        }else if(carcCom != '' && elecCom != ''){
            reason += carcCom+'. '+elecCom;
  		//Check if caracteristic comments are filled           
        }else if(carcCom == '' && elecCom != ''){
            reason += elecCom;
  		//Check if caracteristic comments are filled           
        }else{
            reason += carcCom;
        }
        
        return reason;
    }
    
   
    private static SD_ServiceDemand__c  getCurrentSD(WST_Waste_Statement__c wst){
        SD_ServiceDemand__c sd = new SD_ServiceDemand__c();
        
        //regionEnedis = ServiceDemande__r.ApplicantCenter__r.EnedisRegion__c;
        
        
        sd.OwnerID = wst.ServiceDemande__r.OwnerID; 
        sd.ApplicantCenter__c = wst.ServiceDemande__r.ApplicantCenter__c;
        sd.RemovalPostalCode__c = wst.ServiceDemande__r.RemovalPostalCode__c;
        sd.ConfirmedRemovalDate__c = wst.ServiceDemande__r.ConfirmedRemovalDate__c;
        sd.FavoriteRemovalDate__c = wst.ServiceDemande__r.FavoriteRemovalDate__c;
        sd.Email__c = wst.ServiceDemande__r.Email__c;
        sd.ContactEmail__c = wst.ServiceDemande__r.ContactEmail__c;
        sd.Fax__c = wst.ServiceDemande__r.Fax__c;
        sd.ContactFax__c = wst.ServiceDemande__r.ContactFax__c;
        sd.PriceBook__c = wst.ServiceDemande__r.PriceBook__c;
        sd.IsSameLocalContact__c = wst.ServiceDemande__r.IsSameLocalContact__c;
        sd.BusinessNumberTREDI__c = wst.ServiceDemande__r.BusinessNumberTREDI__c;
        sd.OrderNumber__c = wst.ServiceDemande__r.OrderNumber__c;
        sd.LocalContact__c = wst.ServiceDemande__r.LocalContact__c;
        sd.ResponsibleName__c = wst.ServiceDemande__r.ResponsibleName__c;
        sd.Mobile__c = wst.ServiceDemande__r.Mobile__c;
        sd.ContactMobile__c = wst.ServiceDemande__r.ContactMobile__c;
        sd.RemovalStreet__c = wst.ServiceDemande__r.RemovalStreet__c;
        sd.Status__c = wst.ServiceDemande__r.Status__c;
        sd.Phone__c = wst.ServiceDemande__r.Phone__c;
        sd.ContactPhone__c = wst.ServiceDemande__r.ContactPhone__c;
        sd.BilledAccount__c = wst.ServiceDemande__r.BilledAccount__c;
        sd.RemovalCity__c = wst.ServiceDemande__r.RemovalCity__c;
        
        return sd;
    }
    
    private static WST_Waste_Statement__c  getCurrentWST(WST_Waste_Statement__c wst){
       
        System.debug('/////KVA : '+wst.KVA__c);
        WST_Waste_Statement__c ws = new WST_Waste_Statement__c();
        ws.ID = wst.ID;
        ws.KVA__c = wst.KVA__c;
        ws.PriceBook__c = wst.PriceBook__c;
        ws.ServiceDemande__c = wst.ServiceDemande__c;
        
        return ws;
    }
    
    private static Opportunity  getCurrentOpp(WST_Waste_Statement__c wst){
        
        Opportunity opp = new Opportunity();
        opp.ID = wst.Opportunity__c;
        opp.Name = wst.Opportunity__r.Name;
        opp.Pricebook2ID = wst.Opportunity__r.Pricebook2ID;
        opp.SyncedQuoteID = wst.Opportunity__r.SyncedQuoteID;
        opp.Informations__c = wst.Opportunity__r.Informations__c;
        
        return opp;
    }
    
    private static Quote  getCurrentQuote(WST_Waste_Statement__c wst){
        
        Quote q = new Quote();
        q.ID = wst.Opportunity__r.SyncedQuoteID; 
        q.Status = wst.Opportunity__r.SyncedQuote.Status;
        q.OwnerID = wst.Opportunity__r.SyncedQuote.OwnerID;
        q.OpportunityID = wst.Opportunity__r.SyncedQuote.OpportunityID;
        q.Name = wst.Opportunity__r.SyncedQuote.Name;
        q.DateDevis__c = wst.Opportunity__r.SyncedQuote.DateDevis__c;
        q.Description = wst.Opportunity__r.SyncedQuote.Description;
        q.Quote_Type__c = wst.Opportunity__r.SyncedQuote.Quote_Type__c;
        q.ContactID = wst.Opportunity__r.SyncedQuote.ContactID;
        q.Email = wst.Opportunity__r.SyncedQuote.Email;
        q.Phone = wst.Opportunity__r.SyncedQuote.Phone;
        q.Fax = wst.Opportunity__r.SyncedQuote.Fax;
        q.BillingStreet = wst.Opportunity__r.SyncedQuote.BillingStreet;
        q.BillingPostalCode = wst.Opportunity__r.SyncedQuote.BillingPostalCode;
        q.BillingCity = wst.Opportunity__r.SyncedQuote.BillingCity;
        q.BillingCountry = wst.Opportunity__r.SyncedQuote.BillingCountry;
        q.ShippingStreet = wst.Opportunity__r.SyncedQuote.ShippingStreet;
        q.ShippingPostalCode = wst.Opportunity__r.SyncedQuote.ShippingPostalCode;
        q.ShippingCity = wst.Opportunity__r.SyncedQuote.ShippingCity;
        q.ShippingCountry = wst.Opportunity__r.SyncedQuote.ShippingCountry;
        
        return q;
    }
    
    private static SD_ServiceDemand__c generateNewElimSD(SD_ServiceDemand__c sd){
        System.debug('////Function GENERATE NEW SD');
        SD_ServiceDemand__c newSD = new SD_ServiceDemand__c();
        
        newSD.OwnerID = sd.OwnerID; 
        newSD.ApplicantCenter__c = sd.ApplicantCenter__c;
        newSD.RemovalPostalCode__c = sd.RemovalPostalCode__c;
        newSD.ConfirmedRemovalDate__c = sd.ConfirmedRemovalDate__c;
        newSD.FavoriteRemovalDate__c = sd.FavoriteRemovalDate__c;
        newSD.Email__c = sd.Email__c;
        newSD.ContactEmail__c = sd.ContactEmail__c;
        newSD.Fax__c = sd.Fax__c;
        newSD.ContactFax__c = sd.ContactFax__c;
        newSD.IsSameLocalContact__c = sd.IsSameLocalContact__c;
        newSD.BusinessNumberTREDI__c = sd.BusinessNumberTREDI__c;
        newSD.OrderNumber__c = sd.OrderNumber__c;
        newSD.LocalContact__c = sd.LocalContact__c;
        newSD.ResponsibleName__c = sd.ResponsibleName__c;
        newSD.Mobile__c = sd.Mobile__c;
        newSD.ContactMobile__c = sd.ContactMobile__c;
        newSD.RemovalStreet__c = sd.RemovalStreet__c;
        newSD.Status__c = sd.Status__c;
        newSD.Phone__c = sd.Phone__c;
        newSD.ContactPhone__c = sd.ContactPhone__c;
        newSD.BilledAccount__c = sd.BilledAccount__c;
        newSD.RemovalCity__c = sd.RemovalCity__c;
        newSD.RecordTypeId = Label.RCT_RT_DemandeService_Elimination;       
        if(regionEnedis == 'Nord'){
            newSD.PriceBook__c = Label.RCT_PB_Elimination_N;
        }else{
            newSD.PriceBook__c = Label.RCT_PB_Elimination_S;
        }
        
        
        insert newSD;
        
        return newSD;
    }
    
    private static WST_Waste_Statement__c  updateWasteStatement(WST_Waste_Statement__c wst, SD_ServiceDemand__c sd){
        System.debug('////Function GENERATE UPDATE WST');
       
        wst.RecordTypeID = Label.RCT_RT_WST_Elimination;
        wst.ServiceDemande__c = sd.ID;
        wst.TransfoFromRehab__c = true;
        wst.SendTransfoToElim__c = true;
        
        if(regionEnedis == 'Nord'){
            wst.PriceBook__c = Label.RCT_PB_Elimination_N;
        }else{
            wst.PriceBook__c = Label.RCT_PB_Elimination_S;
        }
        
        update wst; 
        
        return wst;
    }
    
    
    
     
    
    private static Opportunity  updateOpportunity(Opportunity opp, SD_ServiceDemand__c sd){
        System.debug('////Function GENERATE UPDATE OPP');
        String info ='';
        if(opp.Informations__c != null)
            info = opp.Informations__c;
        
        opp.Name = opp.Name + ' - Destruction'; 
        
        
        opp.Informations__c = 'Opportunité transformé en opportunité de desctruction par : '+ 
            UserInfo.getFirstName()+ ' '
            + UserInfo.getLastName() + '. Le '+
            Date.today() + '. Pour un montant total de '+ 
            grandTotal.format() +' €. '+
            info;
        
        opp.StageName = 'Proposition commerciale';
        opp.ServiceDemand__c = sd.ID;
        
        if(opp.PriceBook2ID == Label.RCT_PB_Reparation){
            opp.TransfoOpportunityFrom__c = 'Repairs';    
        }else if(opp.PriceBook2ID == Label.RCT_PB_Depollution){
            opp.TransfoOpportunityFrom__c = 'Remediation';    
        }
        
        
        if(regionEnedis == 'Nord'){
            opp.PriceBook2ID = Label.RCT_PB_Elimination_N;
        }else{
            opp.PriceBook2ID = Label.RCT_PB_Elimination_S;
        }
        
        update opp; 
        
        return opp;
    }
    
     private static Opportunity  updateOpportunity(Opportunity opp){
        System.debug('////Function GENERATE UPDATE OPP');
                
        opp.Name = opp.Name + ' - Destruction'; 
        
        
        opp.Informations__c = 'Tranformateur passé en desctruction par : '+ 
            UserInfo.getFirstName()+ 
            + UserInfo.getLastName() +'. Raison : '+ WST_wst.ElectricTestComments__c+'. Le '+
            Date.today();
         
         opp.StageName = 'Proposition commerciale';
         
         
         opp.TransfoOpportunityFrom__c = 'Repairs';    
         
         update opp; 
         return opp;
    }
    
    private static Boolean  updateOpportunityLineItemForRepairs(Opportunity opp){
        System.debug('////Function GENERATE UPDATE OLI Repairs');
        
        
        opp.StageName = 'Proposition commerciale';
       
     
        update opp; 
        
        return true;
    }
    
    private static Quote updateQuote(Quote q){
        System.debug('////Function GENERATE UPDATE Quote');
        q.Status = 'Révision nécessaire';
        
        update q; 
        
        return q;
    }
    
    
    public class ElimObject{
        public SD_ServiceDemand__c serviceDemand;
        public WST_Waste_Statement__c wasteStatement;
        public Opportunity opp;
        public Quote qte;
    }
    
}