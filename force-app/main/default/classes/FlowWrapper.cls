public class FlowWrapper {
    public String Document{get;set;}
    public String DocumentName{get;set;}
    public String DocumentLanguage{get;set;}
    public String DocumentGroupCode{get;set;}
    public String ExternalReference{get;set;}
    public String CallBackUrl{get;set;}
    public Boolean SigningSequence{get;set;}
    public DateTime ExpiryTimestamp{get;set;}
    public String Initiator{get;set;}
    public String LegalNoticeKey{get;set;}
    public String LegalNoticeCustomText{get;set;}
    public String SigningTemplateCode{get;set;}
    public String TargetType{get;set;}
    //public String PackageID{get;set;}
    public Map<String,String> Extradata{get;set;}
    public String QRConnectedFieldID{get;set;}
    public List <Signer> Signers{get;set;}
    public QRConnect QRConnect{get;set;}
    public List <Receiver> Receivers{get;set;}
    
    public class QRConnect {
        public Integer PageNumber{get;set;}
        public String Width{get;set;}
        public String Height{get;set;}
        public String Left{get;set;}
        public String Top{get;set;}
    }
    
    public class Signer {
        public String ExternalReference{get;set;}
        public String RedirectUrl{get;set;}
        public Boolean SendNotification{get;set;}
        public String EmailAddress{get;set;}
        public String FirstName{get;set;}
        public String LastName{get;set;}
        public String Title{get;set;}
        public String BirthDate{get;set;}
        public String PhoneNumber{get;set;}
        public String Language{get;set;}
        public String SigningType{get;set;}
        public String SigningFieldID{get;set;}
        public String MandatedSigner{get;set;}
        public String NationalSecurityNumber{get;set;}
        public SigningField Signingfield{get;set;}
    }
    
    public class SigningField {
        public String Width{get;set;}
        public String Height{get;set;}
        public String PageNumber{get;set;}
        public String Left{get;set;}
        public String Top{get;set;}
        public SigningField(){}
    }
    
    public class Receiver {
        public String EmailAddress{get;set;}
        public String FirstName{get;set;}
        public String LastName{get;set;}
        public String Title{get;set;}
        public String Language{get;set;}
        public Receiver(){}
    }
    
    public static sObject retrieveContact(Id objId){
        sObject returnObj = null;
        String sobjectType = objId.getSObjectType().getDescribe().getName();                 
        Schema.SObjectType convertType = Schema.getGlobalDescribe().get(sobjectType);
        if (convertType == Quote.sObjectType) {
            returnObj = [SELECT LastName, FirstName, Phone, Email, Title, Id FROM Contact c WHERE c.Id in (Select ContactId FROM Quote WHERE ID =: objId ) LIMIT 1];
        } else if (convertType == FIP_FIP__c.sObjectType){
            returnObj = [SELECT LastName, FirstName, Phone, Email, Title, Id FROM Contact c WHERE c.Id in (Select Contact__c FROM FIP_FIP__c WHERE ID =: objId ) LIMIT 1];
        }  else{
            System.debug('ERROR OBJECT NOT HANDLED BY CALLOUT');
        } 
        return returnObj;
    }
    
    public static Attachment retrieveAttachment(Id objId){
        Attachment att=new Attachment();
        if(ConnectiveCallout.retrievetype(objID)=='Quote'){
            try{
                att = [SELECT id, Name, body, ContentType FROM Attachment WHERE ParentId = : objId ORDER BY CreatedDate DESC LIMIT 1 ];
            } catch(Exception ex){
                att = null;}
        }else if(ConnectiveCallout.retrievetype(objID)=='FIP'){
            try{
                ContentDocumentLink cdl=[SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: objId];
                ContentVersion cv = [select PathOnClient, ContentLocation, Origin, OwnerId, Title, VersionData, ContentDocumentId from ContentVersion where ContentDocumentId =: cdl.ContentDocumentId];
                att.Name=cv.PathOnClient;
                att.OwnerId=cv.OwnerId;
                att.Body = cv.VersionData;
                System.debug(att);
            } catch(Exception ex){
                System.debug('error no file found');
                att=null;
            }
        }
        return att;
    }
    public static String populatejsonUnknownSigner(Id objId,Attachment att,String Firstname, String Lastname,String phone , String Email){
        FlowWrapper fl=new FlowWrapper();
        String jsonBody='';
        if(att!=null){
            String s = EncodingUtil.base64Encode(att.body);
            fl.Document= s;
            fl.DocumentName= att.Name;
            fl.SigningSequence= true;
            fl.Initiator=System.Label.FIP_ConnectiveUser;
            fl.TargetType='';
            fl.SigningTemplateCode='';
            fl.QRConnectedFieldID='';
            fl.LegalNoticeKey='OFF';
            fl.LegalNoticeCustomText='';
            fl.ExternalReference=objId;
            fl.DocumentLanguage='FR';
            fl.DocumentGroupCode='';
            fl.CallBackUrl=System.Label.ConnectiveRedirectUrl;            
            Signer signer=new Signer();
            //signer.ExternalReference='000';
            signer.SendNotification=True;
            signer.EmailAddress=Email;
            if(FirstName==null){
                FirstName='DefaultFirstName';
            }
            signer.FirstName=FirstName;
            signer.LastName=LastName;
            String phoneNumber;
            if(Phone!=Null){
                phoneNumber = ConnectiveCalloutUtils.reformatePhoneString(Phone);
                if(phoneNumber.substring(0,1)=='0' && phoneNumber.substring(1,2)!='0' ){
                    phoneNumber='0033'+phoneNumber;
                }
                if(phoneNumber.substring(0,1)=='+'){
                    phoneNumber=ConnectiveCalloutUtils.removePlusOnString(phoneNumber);
                }
            }else{
                phoneNumber='0033600000000';
            }
            signer.PhoneNumber=phoneNumber;
            signer.BirthDate='';
            signer.SigningType=System.Label.Connective_SigningType;
            signer.RedirectUrl=System.Label.ConnectiveRedirectUrl;   
            signer.MandatedSigner='';
            signer.Language='fr';
            /*SigningField sf=new SigningField();
            sf.Width='200';
            sf.Height='100';
            sf.PageNumber='2';
            sf.Left='120';
            sf.Top='280';       
            signer.Signingfield=sf;*/
            signer.SigningFieldId=System.Label.ConnectiveSigningFieldId;
            List<Signer> ListSigners = new List<Signer>();
            ListSigners.add(signer);
            fl.Signers=ListSigners;
            jsonBody = json.serialize(fl);
        }else{
            jsonBody= '';
        }      
        return jsonBody;           
    }
    public static String populatejson(Id objId){
        FlowWrapper fl=new FlowWrapper();
        String jsonBody='';
        Attachment att=retrieveAttachment(objId);
        if(att!=null){
            String s = EncodingUtil.base64Encode(att.body);
            fl.Document= s;
            fl.DocumentName= att.Name;
            fl.SigningSequence= true;
            
            fl.Initiator=System.Label.FIP_ConnectiveUser;
            fl.TargetType='';
            fl.SigningTemplateCode='';
            fl.QRConnectedFieldID='';
            fl.LegalNoticeKey='OFF';
            fl.LegalNoticeCustomText='';
            fl.ExternalReference=objId;
            fl.DocumentLanguage='FR';
            fl.DocumentGroupCode='';
            fl.CallBackUrl='';
            Contact c=(Contact)retrieveContact(objId);     
            
            Signer signer=new Signer();
            signer.ExternalReference= c.Id;
            signer.SendNotification=True;
            signer.EmailAddress=c.Email;
            String FirstName;
            if(c.FirstName==null){
                FirstName='DefaultFirstName';
            }else{
               FirstName=c.FirstName; 
            }
            signer.FirstName=FirstName;
            signer.LastName=c.LastName;
            String phoneNumber;
            if(c.Phone!=null){
                phoneNumber = ConnectiveCalloutUtils.reformatePhoneString(c.Phone);
                if(phoneNumber.substring(0,1)=='0' && phoneNumber.substring(1,2)!='0' ){
                    phoneNumber='0033'+phoneNumber;
                }
                if(phoneNumber.substring(0,1)=='+'){
                    phoneNumber=ConnectiveCalloutUtils.removePlusOnString(phoneNumber);
                }
            }
            signer.PhoneNumber=phoneNumber;
            signer.BirthDate='';
            signer.SigningType=System.Label.Connective_SigningType;
            signer.RedirectUrl=System.Label.ConnectiveRedirectUrl;  
            signer.MandatedSigner='';
            signer.Language='fr';
            signer.SigningFieldId=System.Label.ConnectiveSigningFieldId;
            List<Signer> ListSigners = new List<Signer>();
            ListSigners.add(signer);
            fl.Signers=ListSigners;
            jsonBody = json.serialize(fl);
        }else{
            jsonBody= '';
        }      
        return jsonBody;           
    }
    public static FlowWrapper deserializejson(String json){
        return (FlowWrapper) System.JSON.deserialize(json, FlowWrapper.class);
    }
}