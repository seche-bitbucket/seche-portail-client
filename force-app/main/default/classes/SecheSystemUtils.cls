/*
Class for all system methods to get data
*/
global without sharing class SecheSystemUtils {
    
    global class OpportunityId {
        @InvocableVariable
        global ID opportunityId;
        @InvocableVariable
        global Attestation__c[] prefips;
    }

    global class FormulaireId {
        @InvocableVariable
        global ID formulaireId;
        @InvocableVariable
        global Attestation__c[] prefips;
    }
    
    // @InvocableMethod(label='Set GUID' description='Set a GUID passcode for Opprtunity' category='Pré-Fip')
    // public static void genGuid(List<OpportunityId> request) {
    //     String preFipUrl = [SELECT SITE_Prefip__c FROM URL__c].SITE_Prefip__c;
    //     Opportunity opp = [SELECT ContactName__r.Email, Salesman__r.Name FROM Opportunity WHERE Id = :request[0].opportunityId];
    //     Blob b = Crypto.GenerateAESKey(128);
    //     String h = EncodingUtil.ConvertTohex(b);
    //     String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
    //     upsert new Opportunity(Id=request[0].opportunityId,
    //                                       GUID__c=guid);
    //     try{
    //         String owa = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = 'Séché Connect'].Id;
    //         String htmlBody = '<p>Cher client,</p><br><p>Pour compléter vos attestations producteurs en vue d\'une élimination en Installation de Stockage de Déchets Non Dangereux,</p><p>Veuillez cliquer sur le lien suivant : <a href="'+preFipUrl+'/s/?recordId='+guid+'">'+preFipUrl+'/s/?recordId='+guid+'</a></p><br><p>Après enregistrement de vos réponses, vous recevrez un mail de <strong>Connective</strong> vous invitant à signer électroniquement ces attestations.</p><br><p>'+opp.Salesman__r.Name+'</p>';
    //         List<Attestation__c> prefipsList = request[0].prefips;
    //         String email = [SELECT Email FROM Contact WHERE Id = :prefipsList[0].Contact__c].Email;
    //         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    //         mail.setOrgWideEmailAddressId(owa);
    //         mail.setTargetObjectId(prefipsList[0].Contact__c);
    //         mail.setWhatId(prefipsList[0].Id);
    //         mail.setToAddresses(new String[] {email});
    //         mail.setSubject('Attestations producteurs pour éliminitation ISDND');
    //         mail.setHtmlBody(htmlBody);
    //         if(prefipsList.size() > 1) {
    //             List<Task> tasksList = new List<Task>();
    //             for(Integer i = 1; i < prefipsList.size(); i++) {
    //                 Task t = LC_GenericEmailSend.createEmailActivity('Attestations producteurs pour éliminitation ISDND', htmlBody, null, prefipsList[i].Id);
    //                 tasksList.add(t);
    //             }
    //             insert tasksList;
    //         }
    //         Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    //     }catch(Exception ex){
    //         System.debug(ex.getMessage()+' '+ex.getLineNumber());
    //         return;
    //     }   
    //     return;
    // }

    @InvocableMethod(label='Set GUID' description='Set a GUID passcode for Formulaire' category='Pré-Fip')
    public static void genGuid(List<FormulaireId> request) {
        String preFipUrl = [SELECT SITE_Prefip__c FROM URL__c].SITE_Prefip__c;
        Form__c form = [SELECT Contact__r.Email, Opportunity__r.Salesman__r.Name, BusinessCase__c FROM Form__c WHERE ID = :request[0].formulaireId];
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        String businessCase = '';
        Map<String,String> renewal = new Map<String,String>();
        Map<String,String> CAP = new Map<String,String>();
        String tab = '';
        String tabBody = '';
        String staticResourceURL = (Test.isRunningTest()) ? 'test?' : PageReference.forResource('PreFip_Caracterisation_Template_FR').getUrl();
        staticResourceURL = (staticResourceURL.contains('?')) ? staticResourceURL.substring(0, staticResourceURL.indexOf('?')) : staticResourceURL;
        //Map<String,List<String>> renewalCAPMap = new Map<String,List<String>>();
        
        upsert new Form__c(Id=request[0].formulaireId, GUID__c=guid);
        try{
            List<Attestation__c> prefipsList = request[0].prefips;

            for (Attestation__c prefip : prefipsList) {
                List<String> renewalCAP = new List<String>();
                renewal.put(prefip.Id,'Renouvellement : '+ (prefip.Renewal__c != null ? prefip.Renewal__c : ''));
                CAP.put(prefip.Id,'N°CAP concerné : ' + (prefip.CAPNumber__c != null ? prefip.CAPNumber__c : ''));
            }

            for (Attestation__c prefip : prefipsList) {
                String renewalValue = '';
                String CAPValue = '';
                for (String attId : renewal.keySet()) {
                    if (attId == prefip.Id) {
                        renewalValue = '<td style="border: 1px solid black; padding: 8px;">' + renewal.get(attId) + '</td>';
                    }
                }
                for (String attId : CAP.keySet()) {
                    if (attId == prefip.Id) {
                        CAPValue = '<td style="border: 1px solid black; padding: 8px;">' + CAP.get(attId) + '</td>';
                    }
                }
                if (renewalValue!='' || CAPValue!='') {
                    tabBody = tabBody + '<tr> <td style="border: 1px solid black; padding: 8px;"> ' + prefip.Name + ' </td>';
                    if (renewalValue!='') {
                        tabBody += renewalValue;
                    } else {
                        tabBody += '<td style="border: 1px solid black; padding: 8px;"> </td>';
                    }
                    if (CAPValue!='') {
                        tabBody += CAPValue;
                    } else {
                        tabBody += '<td style="border: 1px solid black; padding: 8px;"> </td>';
                    }
                    tabBody += '</tr>';
                }
            }

            if (tabBody != '') {
                tab = '<table style="border-collapse: collapse;"> ' + tabBody + '</table>';
            }

            
            
            if(form.BusinessCase__c != null) {
                businessCase = 'Affaire / dossier : ' + form.BusinessCase__c + '</br>';
            }
            String guideLabel = System.Label.AttProd_Guide;
            String owa = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = 'Séché Connect'].Id;
            String htmlBody = '<p>Cher client,</p><p>Pour compléter vos attestations producteurs en vue d\'une élimination en Installation de Stockage de Déchets Non Dangereux,</p>' + businessCase + tab + '<p>Veuillez cliquer sur le lien suivant : <a href="'+preFipUrl+'/s/?recordId='+guid+'">'+preFipUrl+'/s/?recordId='+guid+'</a></p><br><p><span style="text-decoration: underline; background-color: #FFFF00"><strong>IMPORTANT :</strong></span> <br> Cette procédure est complétée en étape 3 par l’enregistrement de <strong>la caractérisation de votre déchet</strong>. <a href="'+preFipUrl+staticResourceURL+'">Un modèle type</a> est disponible. </p><p>Pour ceux concernés par cette caractérisation, il est recommandé de disposer en début de saisie de tous les éléments permettant cette caractérisation pour renseigner ce modèle type.</P><p>C’est une étape <strong>obligatoire</strong> pour finaliser l’attestation : il est <strong>indispensable</strong> de joindre cette caractérisation en fin de parcours pour que votre démarche soit prise en compte.</p><p><i>(Sans cette étape, vous serez bloqués en fin de parcours et vous serez amenés à refaire toute votre attestation)</i></p><br><p>Après enregistrement de vos réponses, vous recevrez un mail de <strong>Connective</strong> vous invitant à signer électroniquement ces attestations.</p><p>Pour vous aider dans cette démarche, consultez notre <a href="https://youtu.be/kEIXc_0_8bo">démo</a> et notre <a href="' + guideLabel + '">guide</a>.</p>';
            String email = [SELECT Email FROM Contact WHERE Id = :form.Contact__r.Id].Email;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setOrgWideEmailAddressId(owa);
            mail.setTargetObjectId(form.Contact__c);
            mail.setWhatId(prefipsList[0].Id);
            mail.setToAddresses(new String[] {email});
            mail.setSubject('Attestations producteurs pour élimination ISDND');
            mail.setHtmlBody(htmlBody); 
            if(prefipsList.size() > 1) {
                List<Task> tasksList = new List<Task>();
                for(Integer i = 1; i < prefipsList.size(); i++) {
                    Task t = LC_GenericEmailSend.createEmailActivity('Attestations producteurs pour élimination ISDND', htmlBody, null, prefipsList[i].Id);
                    tasksList.add(t);
                }
                insert tasksList;
            }
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }catch(Exception ex){
            System.debug(ex.getMessage()+' '+ex.getLineNumber());
            return;
        }   
        return;
    }
    
    @AuraEnabled
    public static SObject getRecordByGUID(String GUID, String objectApiName, String GUID_FIELD){
        if(GUID != null) {
        	try {
                List<String> fields = CAP_Global_SF_SysTools.getObjectFields(objectApiName);
                System.debug(GUID);
                return Database.query('SELECT '+String.join(fields, ', ')+' FROM '+objectApiName+' WHERE '+GUID_FIELD+' = :GUID');
            } catch (Exception e) {
                System.debug(e.getMessage()+' '+e.getLineNumber());
                return null;
            }
        } else {
            return null;
        }
    }
}