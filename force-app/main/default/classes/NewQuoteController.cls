/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Mohammed Amine IDRISSI TAGHKI <mohammed-amine.idrissi-taghki@capgemini.com>
 * @version        1.0
 * @created        2021-12-15
 * @systemLayer    Controller
 *
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Desciption: Lightning web component Controller for lwc_CreateNewQuote previously used for VFP QUO_NewQuoteVersion
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */


 public with sharing class NewQuoteController {

    private static ResponseWrp rsp = new ResponseWrp();

    @AuraEnabled
    public static string getSobjectType(Id recordId){
        try {
            return recordId.getSobjectType().getDescribe().getName();
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static string process(Id recordId, Boolean transferFIP){
        if(recordId.getSobjectType() == Opportunity.getSObjectType()) {
            return createQuote(recordId);
        } else {
            return newQuoteVersion(recordId, transferFIP);
        }
    }
    
    //public PageReference createQuote() - converted to auraEnabled for design purpose
    public static String createQuote(Id oppId) {

        Quote q = new Quote();
        
        try {
            
            Opportunity opp = [Select ID, Name, Account.Name, Filiere__c, SyncedQuoteId, TECH_QuoteCount__c, Pricebook2Id, ReferenceWasteOwner__c,
                            AccountID, Account.BillingStreet, Account.BillingCity, Account.BillingState, Account.BillingPostalCode, Account.BillingCountry,
                            ContactName__c, Amount, ContactName__r.Email, ContactName__r.Phone ,ContactName__r.Fax, FuelPriceIndexation__c,TECH_IsFromTarifIncrease__c, 
                            ConcernedWorks__c,MobilizedResources__c, OperatingMode__c
                            FROM Opportunity WHERE ID =: oppId /*ApexPages.currentPage().getParameters().get('id')*/ ];
            q.OpportunityId = opp.ID;
            q.Name = opp.Name +' V'+ (opp.TECH_QuoteCount__c != null && opp.TECH_QuoteCount__c > 0 ? opp.TECH_QuoteCount__c + 1 : 1);
            //String referenceWasteOwner = (opp.ReferenceWasteOwner__c == null ? '' : opp.ReferenceWasteOwner__c);
            //q.Name = opp.Account.Name + ' ' + referenceWasteOwner + ' '+ opp.Filiere__c +' V'+ (opp.TECH_QuoteCount__c != null && opp.TECH_QuoteCount__c > 0 ? opp.TECH_QuoteCount__c + 1 : 1);
            q.DateDevis__c = Date.today();
            q.ContactId = opp.ContactName__c;
            q.Email = opp.ContactName__r.Email;
            q.Phone = opp.ContactName__r.Phone;
            q.Fax = opp.ContactName__r.Fax;
            q.BillingStreet = opp.Account.BillingStreet;
            q.BillingCity = opp.Account.BillingCity;
            q.BillingState = opp.Account.BillingState;
            q.BillingPostalCode = opp.Account.BillingPostalCode;
            q.BillingCountry = opp.Account.BillingCountry;
            q.TotalPriceFromLastVersion__c = opp.Amount;
            q.ConcernedWorks__c = opp.ConcernedWorks__c;
            q.MobilizedResources__c = opp.MobilizedResources__c;
            q.OperatingMode__c = opp.OperatingMode__c;
            if(System.today().month() < 12) {
                q.ExpirationDate = date.newInstance(System.today().year(), 12, 31);
            } else if(System.today().month() == 12) {
                q.ExpirationDate = date.newInstance(System.today().year()+1, 12, 31);
            }

            q.FuelPriceIndexation__c = opp.FuelPriceIndexation__c;
            q.ReferenceWasteOwner__c = opp.ReferenceWasteOwner__c; 
            
            if(opp.ReferenceWasteOwner__c == null){
                q.ShippingStreet = opp.Account.BillingStreet;
            q.ShippingCity = opp.Account.BillingCity; 
            q.ShippingState = opp.Account.BillingState;
            q.ShippingPostalCode = opp.Account.BillingPostalCode;
            q.ShippingCountry = opp.Account.BillingCountry;
            }

            if(opp.TECH_IsFromTarifIncrease__c){
                q.Quote_Type__c = 'Convention';
            }else{
                q.Quote_Type__c = 'Proposition commerciale';
            }
            
            
            if(opp.Pricebook2Id != null)
                q.Pricebook2Id = opp.Pricebook2Id;
            
            ByPassUtils.ByPass('QuoteSyncTrigger');
            
            QteLineItemSyncTriggerV2Handler.stop();
            OppLineItemSyncTriggerV2Handler.stop();
            QuoteSyncTriggerV2Handler.stop();
            OppSyncTriggerV2Handler.stop();

            insert q;

            copyOLIToQLI(q);
            opp.SyncedQuoteId = q.ID;
            //update opp;
            updateLockedRecord updateLockedRecord = new updateLockedRecord();
        
            updateLockedRecord.updateRecord(opp);
            
            ByPassUtils.UndoByPass('QuoteSyncTrigger');
                   
            /*____________PREVIOUS________________*/
            // PageReference pr = new PageReference('/'+q.ID);
            // return pr;

            /*____________UPDATE_____________*/
            // returning a reponse object

            rsp.recordUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + q.Id;
            rsp.recordId = q.Id;
            rsp.success=true;
            rsp.message='Le devis a été créé avec succès.';

        } catch(Exception e) {
            errorHandling(e);
        }

        return JSON.serialize(rsp);
        
    }

    private static void errorHandling(Exception e) {

        String message = e.getMessage();
        // split message => message = apex message; system message  
        List<String> separatedMsg;
        // we will work with system message
        String systemError;
        String finalMessage;

        // message are like
        // System.DmlException: Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Le devis synchronisé n'est pas signé, vous ne pouvez pas passer en gagné l'opportunité : veuillez mettre à jour le statut du devis synchronisé.: []

        // first we split with ; if there is any
        if(message.contains(';')) {
            separatedMsg = message.split(';');
            // we delete the first one, and recreate the string with join
            separatedMsg.remove(0);

            systemError = String.join(separatedMsg, ';');
            // we recreate the msg to be sure not removing any custom message

            // recreated msg : first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Le devis synchronisé n'est pas signé, vous ne pouvez pas passer en gagné l'opportunité : veuillez mettre à jour le statut du devis synchronisé.: []
            // we need to remove first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, we use the same process
            if(systemError.contains(',')) {
                System.Debug(systemError);
                separatedMsg = systemError.split(',');

                // we delete the first one, and recreate the string with join
                separatedMsg.remove(0);

                systemError = String.join(separatedMsg, ',');
                // we recreate the msg to be sure not removing any custom message

                if(systemError.contains(':')) {
                    System.Debug(systemError);

                    separatedMsg = systemError.split(':');

                    // we delete the last and recreate the string with join
                    separatedMsg.remove(separatedMsg.size()-1);

                    finalMessage = String.join(separatedMsg, ':');
                    // we recreate the msg to be sure not removing any custom message
                
                } else {
                    finalMessage = systemError;
                }
            } else {
                finalMessage = systemError;
            }
        } else {
            finalMessage = message;
        }

        rsp.type = e.getTypeName();
        // system error message contains, System message type and system error detail
        // we split and take the system error detail
        rsp.message = finalMessage;
        rsp.dev_heads_up = 'NewQuoteController: ' + e.getMessage() + 'caused at line '+e.getLineNumber();

    }

    private class ResponseWrp {
        Boolean success {get; set;}
        String type {get; set;}
        String message {get; set;}
        String dev_heads_up {get; set;}
        Id recordId {get; set;}
        String recordUrl {get; set;}
    }
    
    //public PageReference newQuoteVersion() - converted to auraEnabled for design purpose
    public static String newQuoteVersion(Id quoId, Boolean transferFIP) {

        Quote q = new Quote();

        try {
            
            Quote OldQ = [Select ID, OpportunityId, DateDevis__c, ContactId, Email, Phone, Fax, BillingStreet, BillingCity, BillingState,
                            BillingPostalCode, TotalPrice, BillingCountry, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry,
                            EffectiveDate__c, ExpirationDate, Quote_Type__c, FuelPriceIndexation__c, ReferenceWasteOwner__c, VATRate__c, ConcernedWorks__c,
                            MobilizedResources__c, OperatingMode__c
                            FROM Quote WHERE ID = : quoId /*ApexPages.currentPage().getParameters().get('id')*/ ];

            Opportunity opp = [Select ID, Name, Account.Name, Filiere__c,ReferenceWasteOwner__c, SyncedQuoteId, TECH_QuoteCount__c, Pricebook2Id
                            FROM Opportunity WHERE ID =: OldQ.OpportunityID FOR UPDATE];
            
            q.OpportunityId = opp.ID;
            q.Name = opp.Name +' V'+ (opp.TECH_QuoteCount__c + 1);
            //q.Name = opp.Account.Name + ' '+ OldQ.ReferenceWasteOwner__c +' '+ opp.Filiere__c +' V'+  (opp.TECH_QuoteCount__c + 1);
            q.DateDevis__c = Date.today();
        
            q.ContactId = OldQ.ContactId;
            q.Email = OldQ.Email;
            q.Phone = OldQ.Phone;
            q.Fax = OldQ.Fax;
            q.BillingStreet = OldQ.BillingStreet;
            q.BillingCity = OldQ.BillingCity;
            q.BillingState = OldQ.BillingState;
            q.BillingPostalCode = OldQ.BillingPostalCode;
            q.BillingCountry = OldQ.BillingCountry;
            q.TotalPriceFromLastVersion__c = OldQ.TotalPrice;
            
            q.ShippingStreet = OldQ.ShippingStreet;
            q.ShippingCity = OldQ.ShippingCity;
            q.ShippingState = OldQ.ShippingState;
            q.ShippingPostalCode = OldQ.ShippingPostalCode;
            q.ShippingCountry = OldQ.ShippingCountry;

            q.EffectiveDate__c = OldQ.EffectiveDate__c;
            q.ExpirationDate = OldQ.ExpirationDate;
            q.Quote_Type__c = OldQ.Quote_Type__c;

            q.FuelPriceIndexation__c = OldQ.FuelPriceIndexation__c;
            q.ReferenceWasteOwner__c = OldQ.ReferenceWasteOwner__c;

            q.ConcernedWorks__c = OldQ.ConcernedWorks__c;
            q.MobilizedResources__c = OldQ.MobilizedResources__c;
            q.OperatingMode__c = OldQ.OperatingMode__c;
            
            if(opp.Pricebook2Id != null)
                q.Pricebook2Id = opp.Pricebook2Id;   

            ByPassUtils.ByPass('QuoteSyncTrigger');
            
            OppLineItemSyncTriggerV2Handler.stop();
            QuoteSyncTriggerV2Handler.stop();
            OppSyncTriggerV2Handler.stop();
            
            insert q;

            
            List<QuoteLineItem> newQlis = copyOLIToQLI(q);
            if(transferFIP == null) {
                transferFIP = false;
            }
            if(transferFIP) {
                List<FIP_FIP__c> fipsToUpdate = new List<FIP_FIP__c>();
                List<QuoteLineItem> qlis = [SELECT Id, OpportunityLineItemId FROM QuoteLineItem WHERE QuoteId = :quoId];
                List<FIP_FIP__c> fips = [SELECT Id, QuoteLineItem__c, Quote__c FROM FIP_FIP__c WHERE QuoteLineItem__c IN :qlis];
                
                Map<Id, Id> mapQliToFIP = new Map<Id, Id>();
                if(fips != null) {
                    for(FIP_FIP__c fip: fips) {
                        for(QuoteLineItem qli: qlis) {
                            if(fip.QuoteLineItem__c == qli.Id) {                               
                                mapQliToFIP.put(qli.OpportunityLineItemId, fip.Id);
                            }
                        }
                    }
                }
                
                for(QuoteLineItem newQli : newQlis) {
                    if(mapQliToFIP.get(newQli.OpportunityLineItemId)!= null){
                        fipsToUpdate.add(
                            new FIP_FIP__c( Id = mapQliToFIP.get(newQli.OpportunityLineItemId),
                            QuoteLineItem__c = newQli.Id,
                            Quote__c = newQli.QuoteId,
                            Contact__c = q.ContactId,
                            Collector__c = opp.Account.Id
                            )
                        );
                    }
                }
                    
                
                if(fipsToUpdate.size() > 0) {
                    update fipsToUpdate;
                }
                    
            }   
                
            opp.SyncedQuoteId = q.ID;
            //update opp;
            updateLockedRecord updateLockedRecord = new updateLockedRecord();
        
            updateLockedRecord.updateRecord(opp);
            
            ByPassUtils.UndoByPass('QuoteSyncTrigger');

            rsp.recordUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + q.Id;
            rsp.recordId = q.Id;
            rsp.success=true;
            rsp.message='Le devis a été créé avec succès.';

        } catch(Exception e) {
            errorHandling(e);
        }

        return JSON.serialize(rsp);
        
        // PageReference pr = new PageReference('/'+q.ID);
        // PageReference pr = new PageReference('/'+q.ID+'/e?retURL='+q.ID);
        // return pr;
    }
    
    public static List<QuoteLineItem> copyOLIToQLI(Quote q){
        
       List<QuoteLineItem> listQLI = new List<QuoteLineItem>();
       List<OpportunityLineItem> listOLI = [SELECT Id, Filiale__c, Description, Nature__c, PricebookEntryId, Quantity, Classe__c, EuropeanCode__c,  Features__c, 
                                            GE__c, IncludedTGAP__c, Sector__c, Label__c, Product2ID, Packaging__c, TransportType__c, Treatment__c, 
                                            UNCode__c, Unit__c, N_CAP__c, UnitPrice, Amendment__c, BuyingPrice__c, CommentaryAmendementsX3__c,
                                            FooterContractX3__c, HeaderContractX3__c, LabelArticleCode__c, Outlet__c, Planned_next_year__c,
                                            TECH_Communal_tax__c, TECH_Indexation_gasoil__c, TextSalesX3__c, TransportPrice__c, TreatmentPrice__c,
                                            FuelPart__c, GoLitrePrice__c
                                            FROM OpportunityLineItem WHERE OpportunityID =: q.opportunityID ];
        
        System.debug('listOLI : '+listOLI);
       
        for(OpportunityLineItem oli : listOLI){
            QuotelineItem qli = new QuoteLineItem();
            qli.OpportunityLineItemId = oli.Id;
            qli.Description = oli.Description;
            qli.Nature__c = oli.Nature__c;
            qli.Quantity = oli.Quantity;
            qli.Classe__c = oli.Classe__c;
            qli.EuropeanCode__c = oli.EuropeanCode__c;
            qli.Features__c = oli.Features__c;
            qli.GE__c = oli.GE__c;
            qli.Sector__c = oli.Sector__c;
            qli.Label__c = oli.Label__c;
            qli.Packaging__c = oli.Packaging__c;
            qli.Product2ID = oli.Product2ID;
            qli.TransportType__c = oli.TransportType__c;
            qli.Treatment__c = oli.Treatment__c;
            qli.UNCode__c = oli.UNCode__c;
            qli.Unit__c = oli.Unit__c;
            qli.N_CAP__c = oli.N_CAP__c;
            qli.UnitPrice = oli.UnitPrice;
            qli.QuoteId = q.Id;
            qli.PricebookEntryId = oli.PricebookEntryId;
            qli.Amendment__c = oli.Amendment__c;
            qli.BuyingPrice__c = oli.BuyingPrice__c;
            qli.CommentaryAmendementsX3__c = oli.CommentaryAmendementsX3__c;
            qli.FooterContractX3__c = oli.FooterContractX3__c;
            qli.HeaderContractX3__c = oli.HeaderContractX3__c;
            qli.includedTGAP__c = oli.includedTGAP__c;
            qli.LabelArticleCode__c = oli.LabelArticleCode__c;
            qli.Outlet__c = oli.Outlet__c;
            qli.Planned_next_year__c = oli.Planned_next_year__c;
            qli.TECH_Communal_tax__c = oli.TECH_Communal_tax__c;
            qli.TECH_Indexation_gasoil__c = oli.TECH_Indexation_gasoil__c;
            qli.TextSalesX3__c = oli.TextSalesX3__c;
            qli.TransportPrice__c = oli.TransportPrice__c;
            qli.TreatmentPrice__c = oli.TreatmentPrice__c;
            qli.Filiale__c = oli.Filiale__c;
            qli.FuelPart__c = oli.FuelPart__c;
            qli.GoLitrePrice__c = oli.GoLitrePrice__c;
            
            listQLI.add(qli);
        }
        System.debug('listQLI : '+listQLI);
        insertRecords insertRecords = new insertRecords();
        insertRecords.insertListQLI(listQLI);

        return listQLI;
    }

    // Class for updating locked record (without sharing to by pass locked record)
    without sharing class updateLockedRecord {	  
        public void updateRecord(Opportunity opp) { 		
            update opp; 
        }
    }
    
    without sharing class insertRecords {
        public void insertListQLI(List<QuoteLineItem> listQLI){
            insert listQLI;
        }
        
        public void updateListOLI(List<OpportunityLineItem> listOLI){
            update listOLI;
        }
  }
    
}