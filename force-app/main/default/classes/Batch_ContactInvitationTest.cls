@isTest(SeeAllData=false)
public class Batch_ContactInvitationTest {
    
    @isTest
    public static void test() {
        
        insert new List<Contacts_Invitation__c>{new Contacts_Invitation__c(Email__c='test@test.com'), new Contacts_Invitation__c(Email__c='test@test.com')};
            
        Test.startTest();
        
        Database.executeBatch(new Batch_ContactsInvitationCheckDuplicates());
        
        Test.stopTest();
        
        System.assertEquals(2, [SELECT Id FROM Contacts_Invitation__c WHERE IsDuplicate__c = true].size());
        
    }

}