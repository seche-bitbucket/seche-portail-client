@isTest(SeeAllData=true)
private class RegionTriggerBeforeInsertUpdateTest {
    @isTest(SeeAllData=true) static void RegionTriggerBeforeInsert(){
        RecordType r = [SELECT  Id FROM RecordType WHERE SobjectType='Account' AND Name='France'];
        
        //Regions__c Re = new Regions__c(code__c = '10',Name='R10',Country__c='C10');
        Account A = new Account(Name='a',BillingPostalCode ='10000',RecordTypeId = r.Id); 
        insert A;
       
        Account Ac = [Select Name,BillingState, BillingCountry from Account where Id =:A.id];
        //System.assertEquals('Alsace-Champagne-Ardenne-Lorraine',Ac.BillingState);
        //System.assertEquals('France',Ac.BillingCountry);
    }
    @isTest static void RegionTriggerBeforeUpdate(){
        RecordType r = [SELECT  Id FROM RecordType WHERE SobjectType='Account' AND Name='France'];
        
        //Regions__c Re = new Regions__c(code__c = '10',Name='R10',Country__c='C10');
        Account A = new Account(Name='a',BillingPostalCode ='10000',RecordTypeId = r.Id); 
        insert A;
        A.BillingPostalCode ='10000';
        update A;
       
        Account Ac = [Select Name,BillingState, BillingCountry from Account where Id =:A.id];
        //System.assertEquals('Alsace-Champagne-Ardenne-Lorraine',Ac.BillingState);
        //System.assertEquals('France',Ac.BillingCountry);
        
    }
    
}