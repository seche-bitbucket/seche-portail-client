@IsTest
public class VFP_QuoteRelatedItemsControllerTest {

@testSetup static void setup() {
	// Create common test Quotes and QuoteLinesItems
	List<Opportunity> oppList = New List<Opportunity>();

	List<Quote> lstQuote = new List<Quote>();

	//Get a Record Type for Account
	RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];

	ID priceBookId = Test.getStandardPricebookId();

	Product2 product = new product2(name = 'Test' ,TECH_ExternalID__c = '012345',Family = 'Traitement', isActive = true );

	insert product;

	PriceBookEntry pbe = new PriceBookEntry(
		priceBook2Id = priceBookId,
		product2Id = product.id,
		unitPrice = 5.00,
		isActive = true
		);

	insert pbe;

	Account acct = new Account(
		name = 'Test Account'
		);

	insert acct;

	Opportunity oppty = new Opportunity(
		accountId = acct.id,
		priceBook2Id = priceBookId,
		name = 'Test Opportunity',
		stageName = 'Decision',
		closeDate = Date.today().addDays( 30 )
		);

	insert oppty;

	OpportunityLineItem oli = new OpportunityLineItem(
		opportunityId = oppty.id,
		priceBookEntryId = pbe.id,
		quantity = 5,
        unitPrice = pbe.unitPrice
      
		);

	insert oli;

	Quote q = new Quote(
		opportunityId = oppty.id,
		priceBook2Id = priceBookId,
		name = 'Test Quote'
		);

	insert q;



	QuoteLineItem qli = new QuoteLineItem(
		quoteId = q.id,	
		priceBookEntryId = oli.priceBookEntryId,
		quantity = oli.quantity,
        unitPrice = oli.unitPrice,
        BuyingPrice__c = 10
		);

    insert qli;
    
    QuoteLineItem qlii = new QuoteLineItem(
		quoteId = q.id,		
		priceBookEntryId = oli.priceBookEntryId,
        quantity = oli.quantity,
        unitPrice = pbe.unitPrice,
        BuyingPrice__c = null
	
		);

	insert qlii;
}


@IsTest
    public static void testNoSelected ()
    {
    
        List<QuoteLineItem> quoteLineItemList  = new  List<QuoteLineItem>();
        
        PageReference pageRef = Page.VFP_IncreaseByPercent;           
            
            
            Test.setCurrentPage(pageRef);
            //Test sans QLI sélectionnée
            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(quoteLineItemList);
            stdSetController.setSelected(quoteLineItemList);
            VFP_QuoteRelatedItemsController myController = new VFP_QuoteRelatedItemsController(stdSetController);        
            myController.init(); 
            ApexPages.Message[] pageMessages= ApexPages.getMessages(); 
            
            system.assertEquals('Veuillez sélectionner un enregistrement avant de procéder à l\'augmentation !',pageMessages[0].getDetail() );

     }

     @IsTest 
    public static void testNoBuyingPrice ()
    {
    
        List<QuoteLineItem> quoteLineItemList  = [select id, UnitPrice, BuyingPrice__c  from QuoteLineItem];
        
        PageReference pageRef = Page.VFP_IncreaseByPercent;           
            
            
            Test.setCurrentPage(pageRef);
            //Test sans QLI sélectionnée
            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(quoteLineItemList);
            stdSetController.setSelected(quoteLineItemList);
            VFP_QuoteRelatedItemsController myController = new VFP_QuoteRelatedItemsController(stdSetController);        
            myController.init(); 
            ApexPages.Message[] pageMessages= ApexPages.getMessages(); 
            
            system.assertEquals('Prix d\'achat non renseigné ,veuillez renseigner le prix d\'achat',pageMessages[0].getDetail() );

     }

     @IsTest
     public static void testIncreasePercent ()
     {
     
         List<QuoteLineItem> quoteLineItemList  = [select id, UnitPrice, BuyingPrice__c  from QuoteLineItem WHERE BuyingPrice__c!=null];
         
         PageReference pageRef = Page.VFP_IncreaseByPercent;           
             
         Double uPrice = quoteLineItemList[0].UnitPrice;
             
             Test.setCurrentPage(pageRef);
             //Test sans QLI sélectionnée
             ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(quoteLineItemList);
             stdSetController.setSelected(quoteLineItemList);
             VFP_QuoteRelatedItemsController myController = new VFP_QuoteRelatedItemsController(stdSetController);    
             myController.lineItemsSelected = quoteLineItemList;  
             myController.qli = new  QuoteLineItem();
             myController.qli.UnitPrice = 30;   
             myController.inputValue = 30;      
             myController.save(); 
             ApexPages.Message[] pageMessages= ApexPages.getMessages();     
            
             system.assertEquals('Enregistrement(s) mise(nt) à jour avec succès !',pageMessages[0].getDetail() );
 
             myController.returnBack();
      }




}