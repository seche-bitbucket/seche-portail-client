@isTest
public with sharing class WithoutSharingUtilsTest {
    @testSetup static void setup() {
        // Create common test accounts
        List<Contract__c> testConts = new List<Contract__c>();
        for(Integer i=0;i<5;i++) {
            testConts.add(new Contract__c(Name = 'TestContract'+i, RegroupmentIndex__c=i));
        }
        insert testConts;        
    }

    @isTest static void testGetIndexMax() {       
     System.assertEquals(WithoutSharingUtils.getIndexMax(), 4);
    }
 
}