global class SendApprovalEmailBatch implements Database.Batchable<sObject>{
    
    global  string query;
    
    
    global SendApprovalEmailBatch()
    {
        query = 'SELECT Id, ProcessInstance.TargetObjectid, ActorId,Actor.Name,Actor.Email FROM ProcessInstanceWorkitem where ProcessInstance.targetObject.Type = \'Quote\'';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext info, List<ProcessInstanceWorkitem> scope){
        
        
        //List <ProcessInstanceWorkitem> procInstList2 = new List <ProcessInstanceWorkitem>();
        Map <Id,List<ProcessInstanceWorkitem>> processInstanceWIMap = new Map <Id,List<ProcessInstanceWorkitem>>();
        Map <Id,List<ProcessInstanceWorkitem>> procInstMap = new Map<Id, List<ProcessInstanceWorkitem>>();
        
        for (ProcessInstanceWorkitem s : scope) {
            List <ProcessInstanceWorkitem> procInstList = procInstMap.get(s.ActorId);
            if (procInstList == null) {
            	procInstList = new List<ProcessInstanceWorkitem>();
        	} 
            procInstList.add(s);  
        	procInstMap.put(s.ActorId, procInstList); 
        } 
        
        system.debug('procInstMap :'+procInstMap);
        
        for (id usrId : procInstMap.keySet())
        {
            ApprovalRecapEmails.SendRecapEmail(procInstMap.get(usrId));
        }
          
    } 
    global void finish(Database.BatchableContext info){    
        
    }
    
}