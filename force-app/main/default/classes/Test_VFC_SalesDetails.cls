/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-09-29
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for Sales Details Controller 
* 2018-09-29    Added more tests to increase test coverage

* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class Test_VFC_SalesDetails {
    public static PageReference myVfPage {get;set;}
    public static VFC_SalesDetails vfc{get;set;}  
    
    @isTest(SeeAllData=true) 
    static void TestVFC_SalesDetails(){
        Profile directp = [select id from profile where name = :'Directeur commercial' limit 1];
        
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = directp.id, username = testemail2, email = testemail2,emailencodingkey = 'UTF-8', localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        
        insert director;
        Profile p = [select id from profile where name = :'Responsable commercial' limit 1];
        
        String testemail = 'Commercial-_User_test@test.com';
        User Commercial = new User(profileId = p.id, username = testemail, email = testemail,emailencodingkey = 'UTF-8', localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                   alias='cs', lastname='lastname2', IsActive=true);
        
        insert Commercial;
        System.RunAs(director) {
            Account ac = new Account(Name = 'testAccName',CustomerNature__c = 'Administration', Industry = 'Trader', CurrencyIsoCode = 'EUR', Producteur__c = false, BillingPostalCode = '00000', NAF_Number__c = '1234A');
            insert ac;
            
            Account ac2 = new Account(Name = 'TestAcc2', CustomerNature__c = 'Administration', Industry = 'Trader', CurrencyIsoCode = 'EUR', Producteur__c = false, BillingPostalCode = '69006', NAF_Number__c = '1234A'  );
            insert ac2;
            
            List<Contract__c> contractsToInsert=new List<Contract__c>();
            for(integer i=0;i<20;i++){
                String conName =  'Contract Test'+ i ; 
                Contract__c c = new Contract__c(Account__c=ac.id,AssociatedSalesRep__c=Commercial.id, Filiale__c='Séché Eco Industries Changé', Filiere__c='Biocentre', RegroupmentIndex__c=null, name= conName );
                contractsToInsert.add(c);
            }
            for(integer i=0;i<20;i++){
                String conName2 =  'Contract Test'+ i ; 
                Contract__c c = new Contract__c(Account__c=ac2.id, AssociatedSalesRep__c=Commercial.id, Filiale__c='Drimm', Filiere__c='Stockage DND', RegroupmentIndex__c=null,  name= conName2 );
                contractsToInsert.add(c);
            }
            if(contractsToInsert.size()>0){
                insert contractsToInsert;
            }
            List<Amendment__c> Amendments = new List<Amendment__c>();
            Integer i=1;
            for(Contract__c contract:contractsToInsert){                
                Amendment__c am = new Amendment__c(Contrat2__c=contract.id, WasteLabel__c='conditionnement amiante', PriceCurrentYear__c=1*i, QuantityCurrentYear__c=3*i, Quantity2YearsAgo__c=33, QuantityPreviousYear__c=41,
                                                   Nature__c='Traitement', PricePreviousYear__c=200, Price2YearsAgo__c=100, PriceNextYear__c=2*i);
                Amendment__c am2 = new Amendment__c( Contrat2__c=contract.id, WasteLabel__c='Transport amiante', QuantityCurrentYear__c=3*i, Quantity2YearsAgo__c=33, QuantityPreviousYear__c=41, PriceCurrentYear__c=1*i, PriceNextYear__c=2*i);
                Amendment__c am3 = new Amendment__c( Contrat2__c=contract.id, WasteLabel__c='traitement amiante', PriceCurrentYear__c=1*i, Nature__c='Traitement', PricePreviousYear__c=20*i, Price2YearsAgo__c=10*i, QuantityCurrentYear__c=3*i, PriceNextYear__c=2*i);
                Amendments.add(am);
                Amendments.add(am2);
                Amendments.add(am3);
                i++;
            }        
            if(Amendments.size()>0){
                insert Amendments;
            }
            ApexPages.currentPage().getParameters().put('userid',Commercial.Id);
            ApexPages.currentPage().getParameters().put('name','DND_DRIMM');
            ApexPages.currentPage().getParameters().put('FilialeParam','Drimm');
            ApexPages.currentPage().getParameters().put('FiliereParam','Stockage DND');
            //ApexPages.currentPage().getParameters().put('FiliereParam','Centre de tri CS');
            test.startTest();            
            vfc = new VFC_SalesDetails(); 
            //Boolean accName=ac.Name.equalsIgnoreCase(vfc.ContractsByAccounts[1].Account);
           // Boolean accName2=ac2.Name.equalsIgnoreCase(vfc.ContractsByAccounts[0].Account);
           // System.assertEquals(true, accName);
           // System.assertEquals(true, accName2);
            vfc.redirectToListview();
            vfc.redirectToIndicators();
            
           
            List<Contract__c> ContractsToSubmit=new List<Contract__c>();
			         
            for(Contract__c c: contractsToInsert){
                c.Status__c='Soumis';
                ContractsToSubmit.add(c);
            }
            update ContractsToSubmit;
            
            vfc.Approuver();
            List<Contract__c> Contracts=[SELECT Id, Name, AssociatedSalesRep__c, Status__c FROM Contract__c Where Id IN:vfc.Contracts ];
            
           
            for(Contract__c c: Contracts){               
                System.assertEquals('Valide', c.Status__c);              
            }
            
            vfc = new VFC_SalesDetails(); 
            vfc.Refuser();
            Contracts=[SELECT Id, Name, AssociatedSalesRep__c, Status__c FROM Contract__c Where Id IN:Contracts ];
            for(Contract__c c: Contracts){
                System.assertEquals('Valide', c.Status__c);
            }
            
            list<Contract__c> contractsToUpdate = new list<Contract__c>();            
             for(Contract__c c: Contracts){               
                System.assertEquals('Valide', c.Status__c);   
                 c.Status__c = 'Soumis';
                 contractsToUpdate.add(c);
            }
            
            update contractsToUpdate;
            
            vfc = new VFC_SalesDetails(); 
            vfc.Refuser();
            Contracts=[SELECT Id, Name, AssociatedSalesRep__c, Status__c FROM Contract__c Where Id IN:Contracts ];
            for(Contract__c c: Contracts){
                System.assertEquals('Brouillon', c.Status__c);
            }
            test.stopTest();
        }
    } 
    

    
    
}