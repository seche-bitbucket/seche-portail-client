@isTest 
public class QuoteLineItemTriggerTest{
    
    @isTest
    static void TestQuoteLineItemTrigger(){
        
         //get standard pricebook
		ID  standardPb = Test.getStandardPricebookId();
            
        QuoteLineItem_Field__c f = new QuoteLineItem_Field__c(Name='INCOTERMS__c', OppLineSyncField__c='INCOTERMS__c');
        insert f;

        Product2 prd1 = new Product2 (Name='Test Product Entry 1',Description='Test Product Entry 1',productCode = 'ABC', isActive = true, TECH_ExternalID__c='zExternalID_Capgemini001',
                                     Classe__c = 'TEST',
                                     EuropeanCode__c = 'TEST',
                                     Features__c = 'TEST',
                                     GE__c = 'TEST',
                                     Label__c = 'TEST',
                                     Family = 'TEST',
                                     Container__c = 'TEST',
                                     TransportType__c = 'TEST',
                                     Treatment__c = 'TEST',
                                     UNCode__c = 'TEST',
                                     Unit__c = 'TEST');
        insert prd1;

        PricebookEntry pbe1 = new PricebookEntry (Product2ID=prd1.id,Pricebook2ID=standardPb ,UnitPrice=50, isActive=true);
        insert pbe1;
        
       
        
        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '01000';
        a.NAF_Number__c = '1234A';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        //c.Salesman__c = director.Id;
        c.Email = 'aaa@groupe-seche.com';
        c.Phone = '03 00 00 00 00';
        insert c;
       
        
        Opportunity opp = new Opportunity();
        opp.Name = 'test';
        //opp.Salesman__c = director.Id;
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        opp.Pricebook2Id = pbe1.Pricebook2Id;
        insert opp;
        
        Quote quo = new Quote();
        quo.OpportunityId = opp.ID;
        quo.name = 'Mon Devis Test';
        quo.Pricebook2Id =pbe1.Pricebook2Id;
        insert quo;

        Quote quo2 = new Quote();
        quo2.OpportunityId = opp.ID;
        quo2.name = 'Mon Devis Test 2';
        quo2.Pricebook2Id =pbe1.Pricebook2Id;
        insert quo2;
        
        opp.SyncedQuoteId = quo.Id;
        update opp;
        
        OpportunityLineItem oli = new OpportunityLineItem(INCOTERMS__c='FOB',OpportunityId=opp.Id, UnitPrice=10, Quantity=1, Discount=1, ServiceDate=System.today(), PricebookEntryId=pbe1.Id);
        insert oli;
        
        Test.startTest();
		System.debug('.....PBE : '+ pbe1);
        QuoteLineItem qli = new QuoteLineItem (QuoteID=quo2.id,PriceBookEntryID=pbe1.id, Quantity=4, UnitPrice=4, Product2Id=prd1.id, OpportunityLineItemId=oli.Id);

        insert qli;
        
        Test.stopTest();
        
        //QuoteLineItem r = [SELECT INCOTERMS__c FROM QuoteLineItem WHERE ID = :qli.Id];
        //System.assertEquals(r.INCOTERMS__c, oli.INCOTERMS__c);
        
        
    }
}