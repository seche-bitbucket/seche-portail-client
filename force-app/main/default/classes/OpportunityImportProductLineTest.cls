@isTest
public class OpportunityImportProductLineTest {

static testMethod void ImportOppLineItemsFromDaughtersTest(){

	//Get a profile to create User
	Profile p = [select id from profile where name = :'Directeur commercial' limit 1];

	String testemail = 'director_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail, email = testemail,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;

	String testemail2 = 'assistant_-_User_test@test.com';

	User assistant = new User(profileId = p.id, username = testemail2, email = testemail2,
	                          emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                          languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                          alias='cspu', lastname='lastname', IsActive=true);
	insert assistant;

	Equipe__c e = new Equipe__c();
	e.Directeur__c = director.ID;
	e.Assistant__c = assistant.ID;
	e.Filiere__c = 'Physico';

	insert e;

	Account ac = new Account();
	ac.Name = 'testAccName';
	ac.CustomerNature__c = 'Administration';
	ac.Industry = 'Aéronautique';
	ac.CurrencyIsoCode = 'EUR';
	ac.Producteur__c = false;
	ac.BillingPostalCode = '00000';
	ac.NAF_Number__c = '1234A';
	insert ac;

	Contact contactTest = new Contact();
	contactTest.LastName = 'testName';
	contactTest.AccountId = ac.Id;
	contactTest.Salesman__c = director.Id;
	contactTest.Email = 'aaa@yopmail.com';
	//contactTest.Phone = '0000000000';
	insert contactTest;

	//Insert PriceBooks
	//pricebook2 pbMere = [select id from pricebook2 where isstandard = true];

	Pricebook2 pbFille = new Pricebook2(Name='PricebookFille', isActive=true);
	insert pbFille;

	//Pricebook2 pbFille = new Pricebook2(Name='PricebookFille', isActive=true);
	//insert pbFille;

	//Insert Products
	Product2 prdMere = new product2(name = 'Test',TECH_ExternalID__c = 'DEFAUT');
	insert prdMere;

	/*Product2 prdFille = new product2(name = 'Absorbant',TECH_ExternalID__c = 'TEST2');
	   insert prdFille;*/

	//Insert priceBookEntry

	PricebookEntry pbeMere = new PricebookEntry(unitprice=0.01,Product2Id=prdMere.Id,Pricebook2Id=Test.getStandardPricebookId(),
	                                            isActive=true);
	insert pbeMere;

	PricebookEntry pbeFille = new PricebookEntry(unitprice=0.01,Product2Id=prdMere.Id,Pricebook2Id=pbFille.Id,
	                                             isActive=true,usestandardprice = true);
	insert pbeFille;

	//Get a Record Type for Opportunity
	RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_OpportuniteMere' LIMIT 1];

	RecordType rt3 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];

	//Create Opportunity Mother
	Opportunity opportMere = new Opportunity(Name = 'OPPMERE', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id, Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', PriceBook2Id = Test.getStandardPricebookId());

	insert opportMere;

	//Create Opportunity Daughter
	Opportunity opportFille = new Opportunity(Name = 'OPPFILLE', RecordTypeId = rt3.Id, AccountID= ac.Id, ContactName__c = contactTest.Id, Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', OpportuniteMereIntra__c = opportMere.Id,PriceBook2Id = pbFille.Id);

	insert opportFille;

	OpportunityLineItem OPplineitemFille = new OpportunityLineItem (Quantity=2, OpportunityId=opportFille.Id,UnitPrice=0.01,PriceBookEntryId=pbeFille.Id);
	insert OPplineitemFille;
	List<Id> OppIntraIds = new List<Id> ();
	OppIntraIds.add(opportFille.Id);
	OpportunityImportProductLine.ImportOppLineItemsFromDaughters(OppIntraIds,opportMere.Id);

	OpportunityLineItem OPplineitemMere = new OpportunityLineItem (Quantity=2, OpportunityId=opportMere.Id,UnitPrice=0.01,PriceBookEntryId=pbeMere.Id);
	insert OPplineitemMere;
	OpportunityImportProductLine.ImportOppLineItemsFromDaughters(OppIntraIds,opportMere.Id);

	Product2 prdMere2 = new product2(name = 'Test',TECH_ExternalID__c = 'TEST');
	insert prdMere2;

	PricebookEntry pbeMere2 = new PricebookEntry(unitprice=0.01,Product2Id=prdMere2.Id,Pricebook2Id=Test.getStandardPricebookId(),
	                                             isActive=true);
	insert pbeMere2;
	OpportunityImportProductLine.ImportOppLineItemsFromDaughters(OppIntraIds,opportMere.Id);

	OpportunityLineItem OPplineitemMere2 = new OpportunityLineItem (Quantity=2, OpportunityId=opportMere.Id,UnitPrice=0.01,PriceBookEntryId=pbeMere2.Id);
	insert OPplineitemMere2;
	OpportunityImportProductLine.processImportOppLineItemsFromDaughters(OppIntraIds,opportMere.Id);
	

}



static testMethod void ImportOppLineInitTest(){

	//Get a profile to create User
	Profile p = [select id from profile where name = :'Directeur commercial' limit 1];

	String testemail = 'director_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail, email = testemail,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;

	String testemail2 = 'assistant_-_User_test@test.com';

	User assistant = new User(profileId = p.id, username = testemail2, email = testemail2,
	                          emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                          languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                          alias='cspu', lastname='lastname', IsActive=true);
	insert assistant;

	Account ac = new Account();
	ac.Name = 'testAccName';
	ac.CustomerNature__c = 'Administration';
	ac.Industry = 'Aéronautique';
	ac.CurrencyIsoCode = 'EUR';
	ac.Producteur__c = false;
	ac.BillingPostalCode = '00000';
	ac.NAF_Number__c = '1234A';
	insert ac;

	Contact contactTest = new Contact();
	contactTest.LastName = 'testName';
	contactTest.AccountId = ac.Id;
	contactTest.Salesman__c = director.Id;
	contactTest.Email = 'aaa@yopmail.com';
	//contactTest.Phone = '0000000000';
	insert contactTest;

	//Insert Products
	Product2 prdMere = new product2(name = 'Test',TECH_ExternalID__c = 'DEFAUT');
	insert prdMere;

	/*Product2 prdFille = new product2(name = 'Absorbant',TECH_ExternalID__c = 'TEST2');
	   insert prdFille;*/

	//Insert priceBookEntry

	PricebookEntry pbeMere = new PricebookEntry(unitprice=0.01,Product2Id=prdMere.Id,Pricebook2Id=Test.getStandardPricebookId(),
	                                            isActive=true);
	insert pbeMere;
	//Get a Record Type for Opportunity
	RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_OpportuniteMere' LIMIT 1];

	RecordType rt3 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];

	List<Opportunity>oppList = new List<Opportunity>();
	//Create Opportunity Mother
	Opportunity opport1 = new Opportunity(Name = 'OPP1', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id, Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', PriceBook2Id = Test.getStandardPricebookId());
	Opportunity opport2 = new Opportunity(Name = 'OPP2', RecordTypeId = rt3.Id, AccountID= ac.Id, ContactName__c = contactTest.Id, Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', PriceBook2Id = Test.getStandardPricebookId());
	oppList.add(opport1);
	oppList.add(opport2);
	insert oppList;

	PageReference pageRef = Page.VFP_ImportOppIntraGroupLineItem;
	Test.setCurrentPage(pageRef);
	ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(oppList);
	stdSetController.setSelected(oppList);
	OpportunityImportProductLine myController = new OpportunityImportProductLine(stdSetController);

	myController.init();

	system.assertEquals(myController.isShowConfirmation,true );

}

static testMethod void ImportOppLineInitWithNoSelectedTest(){

	//Get a profile to create User
	Profile p = [select id from profile where name = :'Directeur commercial' limit 1];

	String testemail = 'director_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail, email = testemail,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;	

	System.RunAs(director) {
		List<Opportunity>oppList = new List<Opportunity>();

		PageReference pageRef = Page.VFP_ImportOppIntraGroupLineItem;
		Test.setCurrentPage(pageRef);
		ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(oppList);
		stdSetController.setSelected(oppList);
		OpportunityImportProductLine myController = new OpportunityImportProductLine(stdSetController);
        myController.init();
		ApexPages.Message[] pageMessages = ApexPages.getMessages();
		for(ApexPages.Message message : pageMessages) {

			system.assertEquals('Veuillez sélectionner au moins une opportunité dans la liste des Opportunités Sous-traitance avant de procéder !',message.getDetail() );
		}
	}

}


}