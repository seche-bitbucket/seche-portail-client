/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : LC_MassCreatedPackage_AugTarifCtrlTest
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : LC_MassCreatedPackage_AugTarifCtrlTest
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 27-05-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
@isTest
public class LC_MassCreatedPackage_AugTarifCtrlTest {
    
    @testSetup 
    static void setup() {
        Account a;
        Quote q;
        Attachment Att;
        Contact c;
        Attachment Att2;
        FIP_FIP__c fip;
        Opportunity opp;
        DocumentsForSignature__c doc;
        DocumentsForSignature__c doc2;

        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        //Create Assistant Commercial
        User assistantCo = TestDataFactory.createAssistantCommercialUser();

        //Create Resp Commercial
       User responsableCo = TestDataFactory.createResponsableCommercialUser(assistantCo);

        opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        opp.Salesman__c = responsableCo.Id;
        insert opp;
        
        q = new Quote();
        //q.id='0Q09E0000000AhESAU';
        q.OpportunityId = opp.ID;
        q.Name = opp.Name;
        q.DateDevis__c = Date.today();
        q.QuoteCode__c=opp.Name;
        q.ContactId = opp.ContactName__c;
        q.Email = opp.ContactName__r.Email;
        q.Phone = opp.ContactName__r.Phone;
        q.Fax = opp.ContactName__r.Fax;
        q.BillingStreet = opp.Account.BillingStreet;
        q.BillingCity = opp.Account.BillingCity;
        q.BillingState = opp.Account.BillingState;
        q.BillingPostalCode = opp.Account.BillingPostalCode;
        q.BillingCountry = opp.Account.BillingCountry; 
        insert q;
        
        att = new Attachment();
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        att.body=bodyBlob;
        att.Name = q.Name;	
        att.ParentId = q.Id;             
        insert att;    
        fip = new FIP_FIP__c();
        fip.Collector__c = a.ID;
        fip.Contact__c = c.ID;
        insert fip;       
    }

    public testmethod static void test(){
        String pbefore = 'Testing base 64 encode';
        List<Quote> quotes = [SELECT id, Name,ContactId,Opportunity.Salesman__c,Opportunity.Owner.Email, Opportunity.Owner.Name,Email, QuoteCode__c,TECH_UID__c,Opportunity.Filiale__c,Account.Name,TECH_Assistante__c FROM Quote];
        List<Contact> contacts = [SELECT id FROM Contact];                        
        Blob beforeblob = Blob.valueOf(pbefore);
        
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=quotes[0].id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveMockInstantPackage()); 
        ConnectiveMockInstantPackage.externalIdContact = contacts[0].Id;
        ConnectiveMockInstantPackage.externalIdDoc = testContent.Id;
        LC_MassCreatedPackage_AugTarifCtrl btch = new LC_MassCreatedPackage_AugTarifCtrl(quotes,null);
        System.enqueueJob(btch);
        Test.stopTest(); 
    }

}