/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-07-02
* @systemLayer    Controller         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Regroup Contracts Controller for the button that regroups 2 accounts
				to create one opportunity when batch is launched
*  2018-07-02	Removed different account grouping and added error shown for that
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class VFC_RegroupContracts {
    public List<Contract__c> ContractsList {get;set;}
    public Boolean differentAccount{get;set;}
    public VFC_RegroupContracts(ApexPages.StandardSetController controller){
        this.differentAccount=false;
        // retrieve selected contracts and check if he selected more than 1 contract
        if(controller.getSelected().size()>=2){
            this.ContractsList=[SELECT Id, Name, Account__c, Opportunity__c, RegroupmentIndex__c, Filiale__c, AssociatedSalesRep__c,RecordLocked__c FROM Contract__c WHERE ID IN:controller.getSelected() Order By RegroupmentIndex__c ASC NULLS First];
        }else{
            this.ContractsList = new List<Contract__c>();
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Veuillez selectionner au moins deux contract');
            ApexPages.addMessage(errorMessage);
        }
        //check if the contracts selected are from different accounts
        for(Contract__c c: this.ContractsList){
            if(c.Account__c!=this.ContractsList[0].Account__c){
                this.differentAccount=true;
            }
        }
    }
    public PageReference regroupContracts(){
        PageReference returnPage=ApexPages.currentPage();
        //If accounts are from the different accounts we return the user to an error page
        if(this.differentAccount){
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Les contrats sélectionnés proviennent de deux comptes différents et ne peuvent pas être fusionnés');
            ApexPages.addMessage(errorMessage);
            returnPage=null;
        }else{
            returnPage=regroupContractsIfSameAccount();
        }
        return ReturnPage;       
    }
    // Regroup Accounts if they are not locked and put the regroupment Index to a gradual number according to the data created
    public PageReference regroupContractsIfSameAccount(){
        PageReference returnPage=ApexPages.currentPage();
        List<Contract__c> ContractsToUpdate=new List<Contract__c>();
        String Status= 'Brouillon';
        List<Contract__c> AllContracts=[SELECT Id, Name, RegroupmentIndex__c FROM Contract__c Order By RegroupmentIndex__c ASC NULLS First];
        Decimal RegroupmentValue;
        RegroupmentValue=AllContracts[AllContracts.size()-1].RegroupmentIndex__c;
        if(RegroupmentValue==null){
            RegroupmentValue=0;
        }
        Boolean locked=false;
        If(this.ContractsList!=null && this.ContractsList.size()>0){
            for(Contract__c c: this.ContractsList){
                    c.RegroupmentIndex__c=RegroupmentValue+1;
                    ContractsToUpdate.add(c);
                if(c.RecordLocked__c){
                    locked=true;
                }
            }
        }
        If(ContractsToUpdate.size()>0 && !locked){
            Update ContractsToUpdate;
            ReturnPage= new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        }else{
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Le contrat sélectionné est soumis ou validé et ne peut pas être groupé');
            ApexPages.addMessage(errorMessage);
            ReturnPage=null;
        }
        return ReturnPage;
        
    }
}