/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : Batch_InitConnectivePackage
* -- - Author : BT
* -- - Company : Capgemini
* -- - Purpose : OnShot used for initialize Package connective to migrate current signing documents fro V3 to V5 
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 07-09-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
global with sharing class Batch_InitConnectivePackage implements Database.Batchable<sObject>{
    global Database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(
            'SELECT Id, Status__c, FlowId__c,TECH_ExternalId__c,OwnerId,Quote__c,Quote__r.OwnerId , Quote__r.QuoteCode__c, FIP_FIP__r.Name, CreatedDate'+
            ' FROM DocumentsForSignature__c WHERE  FlowId__c != null AND (Quote__c != null OR FIP_FIP__c != null) AND (Status__c= \'PENDINGSIGNING\' OR Status__c=\'\' OR Status__c=\'PENDING\')'
        );
    } 

    global void execute(Database.BatchableContext BC, List<DocumentsForSignature__c> scope) {
        List<Package__c> listPackage = new List<Package__c>();  
        List<DocumentsForSignature__c> listDocToUpdate = new List<DocumentsForSignature__c>();  
        Map<String, DocumentsForSignature__c> mapDocByFlowID = new  Map<String, DocumentsForSignature__c>();

        try{
          for(DocumentsForSignature__c d : scope ){
            Package__c p = new Package__c(ConnectivePackageId__c = d.FlowId__c);
            listPackage.add(p);
            mapDocByFlowID.put(d.FlowId__c,d);
          }

          insert listPackage;

          for(Package__c p : listPackage){
            DocumentsForSignature__c doc = mapDocByFlowID.get(p.ConnectivePackageId__c);
            doc.Package__c = p.Id;
            listDocToUpdate.add(doc);
          }
          update listDocToUpdate;
        }
        catch (Exception e){
            System.debug(LoggingLevel.ERROR, e.getMessage());
        }
    }

    global void finish(Database.BatchableContext BC){}
}