global class OpportunityCloseSubOppController {

    private static List<Opportunity> oppToUpdate = new List<Opportunity>();
        
    
    webService  static String CloseSubOpportunities(List<Id> subOppIds)
    {
        List<Opportunity> opps = [select Id,Name,StageName from Opportunity where ID IN:subOppIds];
        
        for(Opportunity opp : opps ){
            opp.StageName = 'Clôturée / Gagnée';
            oppToUpdate.add(opp);
        }
        
        try{
            update oppToUpdate;
            return 'Opportunité(s) mise(nt) à jour avec succès';
        }catch(Exception e){
            System.debug('Unable to update Opportunities : '+e);
            return 'Une erreur est survenue, impossible de mettre à jour les opportunité(s)';
        }
			
    }
    
  
    
     
}