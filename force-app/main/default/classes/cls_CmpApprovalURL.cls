public class cls_CmpApprovalURL    
{  
    public String quoteID {get; set;}  
    
    public String urlStr{  
        get  
        {
            return cls_createApprovalURL.generateApprovalURL(quoteID);  
        }
    }  
}