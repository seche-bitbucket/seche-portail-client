public with sharing class AVE_QueueOLIsUpdateSync implements Queueable {

    private List<Opportunity> opps;
    private List<QuoteLineItem> qlis;

    public AVE_QueueOLIsUpdateSync(List<Opportunity> opps, List<QuoteLineItem> qlis) {
        this.opps = opps;
        this.qlis = qlis;
    }

    public void execute(QueueableContext context) {
        try{
            ByPassUtils.ByPass('OpportunityLineItemTrigger');
            copyQLI2Oli(qlis);
        } catch (Exception e) {
            Log__c error = new Log__c(ApexJob__c= AVE_QueueOLIsUpdateSync.class.getName(),
                                    ErrorMessage__c=e.getMessage()+' at '+e.getLineNumber(),
                                    ExceptionType__c='INTERNAL');
            insert error;
        }
    }

    private void copyQLI2Oli(List<QuoteLineItem> qlis) {

        System.debug('Start copyQLI2Oli -------');
        System.debug('copyQLI2Oli qlis: '+qlis+' -------');

        try{

            List<String> fields_lst = new List<String>();
            List<QuoteLineItem_Field__c> lstSyncedFields = [SELECT Name, OppLineSyncField__c FROM QuoteLineItem_Field__c];
            List<OpportunityLineItem> listOlis = new List<OpportunityLineItem>();

            Map<String, Schema.SObjectField> fs = Schema.getGlobalDescribe().get('QuoteLineItem').getDescribe().fields.getMap();
            Map<String, Schema.SObjectField> fs_target = Schema.getGlobalDescribe().get('OpportunityLineItem').getDescribe().fields.getMap();

            Map<String, Schema.SObjectField> mapFsTtoName = new Map<String, Schema.SObjectField>();
            for(Schema.SObjectField f: fs_target.values()) {
                mapFsTtoName.put(f.getDescribe().getName(), f);
            }

            // re-get qlis
            List<Id> qliIds = new List<Id>();
            for(QuoteLineItem qli: qlis) {
                qliIds.add(qli.Id);
            }

            List<Id> oppIds = new List<Id>();
            for(Opportunity opp: this.opps) {
                oppIds.add(opp.Id);
            }

            List<QuoteLineItem> fresh_qlis = Database.query('SELECT '+String.join(new List<String>(fs.keySet()), ', ')+' FROM QuoteLineItem WHERE ID IN :qliIds');
            Map<Id, SObject> fresh_olis = new Map<Id, SObject>(Database.query('SELECT '+String.join(new List<String>(fs_target.keySet()), ', ')+' FROM OpportunityLineItem WHERE OpportunityId IN :oppIds'));

            Map<Id, OpportunityLineItem> mapOlisToQlis = new Map<Id, OpportunityLineItem>();
            for(QuoteLineItem qli: fresh_qlis) {
                mapOlisToQlis.put(qli.OpportunityLineItemId, (OpportunityLineItem)fresh_olis.get(qli.OpportunityLineItemId));
            }

            for(SObject qli: fresh_qlis) {
                
                // for(QuoteLineItem qli: fresh_qlis) {
                if(mapOlisToQlis.containsKey((Id)qli.get('OpportunityLineItemId'))) {

                    OpportunityLineItem oli = mapOlisToQlis.get((Id)qli.get('OpportunityLineItemId'));

                    if(qli.get('OpportunityLineItemId') == oli.Id) {

                        // add values for all standards fields
                        for(Schema.SObjectField f: fs.values()) {
                            // for(Schema.SObjectField f_t: fs_target.values()) {
                            if(mapFsTtoName.containsKey(f.getDescribe().getName())) {
                                Schema.SObjectField field = mapFsTtoName.get(f.getDescribe().getName());
                                if(field.getDescribe().getName() != 'TotalPrice' && field.getDescribe().isUpdateable()) {
                                    if(qli.get(field.getDescribe().getName()) != null) {
                                        oli.put(field.getDescribe().getName(), qli.get(field.getDescribe().getName()));
                                    }
                                }
                            }
                        }

                        // add values for all custom fileds
                        for(QuoteLineItem_Field__c qsf: lstSyncedFields) {
                            oli.put(qsf.OppLineSyncField__c, qli.get(qsf.Name));
                        }

                        // append to list
                        listOlis.add(oli);
                    }
                }
            }

            System.debug('copyQLI2Oli listOlis: '+listOlis.size()+' -------');

            upsert listOlis;

        } catch (Exception e) {
            System.debug('At Line '+e.getLineNumber()+' '+e.getMessage());
        }

        System.debug('Finished copyQLI2Oli -------');

    }

}