/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-11-13
* @modified       2018-11-13
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for mass send for signature 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class Test_VFC_MassSendForSignature {
    public static PageReference myVfPage {get;set;}
    public static ApexPages.StandardSetController stc {get;set;}
    public static VFC_MassSendForSignature vfc{get;set;}    
    @isTest static void TestMassSendForSignature(){
        Profile p = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];
        
        String testemail = 'Commercial-_User_test@test.com';
        User Commercial = new User(profileId = p.id, username = testemail, email = testemail,
                                   emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                   languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                   alias='cs', lastname='lastname2', IsActive=true);
        insert Commercial;
        PageReference ref = new PageReference('/001/o');
        Test.setCurrentPage(ref);
        ApexPages.currentPage().getParameters().put('retURL', '/001/o');
        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        opp.Salesman__c=Commercial.Id;
        insert opp;
        
        Quote q = new Quote();
        
        q.OpportunityId = opp.ID;
        q.Name = opp.Name;
        q.DateDevis__c = Date.today();
        q.ContactId = opp.ContactName__c;
        q.Email = Null;
        q.Phone = opp.ContactName__r.Phone;
        q.Fax = opp.ContactName__r.Fax;
        q.BillingStreet = opp.Account.BillingStreet;
        q.BillingCity = opp.Account.BillingCity;
        q.BillingState = opp.Account.BillingState;
        q.BillingPostalCode = opp.Account.BillingPostalCode;
        q.BillingCountry = opp.Account.BillingCountry; 
        q.TECH_UID__c=null;
        insert q;
        Attachment att = new Attachment();
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        att.body=bodyBlob;
        att.Name = q.Name+'- Devis.pdf';	
        att.ParentId = q.Id;             
        insert att; 
        q=[SELECT id,Email,TECH_UID__c, Name,Opportunity.Salesman__c,ContactId,(SELECT ID,Name FROM Attachments) FROM Quote LIMIT 1];
        List<Quote> Quotes=new List<Quote>();
        stc=new Apexpages.standardSetController(Quotes);
        stc.setSelected(Quotes);
        vfc = new VFC_MassSendForSignature(stc);
        System.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages()[0].getSeverity());
        System.assertEquals('Veuillez selectionner un devis', ApexPages.getMessages()[0].getSummary());
        Quotes.add(q);
        stc=new Apexpages.standardSetController(Quotes);
        stc.setSelected(Quotes);
        vfc = new VFC_MassSendForSignature(stc);
        System.assertEquals(true,vfc.Error); 
        System.assertEquals(1,vfc.QuotesWithError.size());
        q.TECH_UID__c='testUID';
        Update q;
        stc=new Apexpages.standardSetController(Quotes);
        stc.setSelected(Quotes);
        vfc = new VFC_MassSendForSignature(stc);
        System.assertEquals(true,vfc.Sent);
        System.assertEquals(1,vfc.QuotesSent.size());
        q.Email = 'email@email.fr';
        q.TECH_UID__c=Null;
        Update q;
        stc=new Apexpages.standardSetController(Quotes);
        stc.setSelected(Quotes);
        vfc = new VFC_MassSendForSignature(stc);
        System.assertEquals(1,vfc.QuotesTosend.size());
        vfc.init();
    }
}