public with sharing class emailSending {
    public static string toMail { get; set;}
    public static string ccMail { get; set;}
    public static string subject { get; set;}
    public static string body { get; set;}
    public static string saleMa { get; set;}
    public static string attachment  { get; set;}
    public static string quoteId { get; set;}
    public static String vContact { get; set;}
    public static String vContactID { get; set;}    
    List<String> vContacts  { get; set;}   
    List<Attachment> vAtt  { get; set;}
    public String selectedContactEmail {get;set;}
    public String selectedEmailContactName {get;set;}
    public String selectedObject {get;set;}
    public String selectedField {get;set;} 
    public list<selectOption> allFieldsForSelectedObject {get;set;}

    
    
    private ApexPages.StandardController stdCtrl;

    
    
    //Constructeur
    public  emailSending(ApexPages.StandardController controller) {
        allFieldsForSelectedObject = new list<selectOption>();
        stdCtrl=controller;
        //recuperation des informations du devis
        List<Quote> vQuote = getQuotes();
        quoteId=  String.valueof(vQuote.get(0).id); 
        String quoteName =String.valueof(vQuote.get(0).name); 
        String vQuoteWasteRef =String.valueof(vQuote.get(0).ReferenceWasteOwner__c); 
        String vQuoteAccount =String.valueof(vQuote.get(0).Account.NomUsuel__c); 
        
        //initialisation variable du mail
        toMail = String.valueof(vQuote.get(0).email); 
        
        List<Quote> vQuo  =[SELECT OpportunityID FROM Quote where id = :vQuote];
        List<Opportunity>  vOpp = [SELECT Salesman__c,Filiale__c FROM Opportunity where id = :vQuo.get(0).OpportunityID];
        String vIdSalesMan = String.valueof(vOpp.get(0).Salesman__c).substring(0, 15);
        User vUser  =[SELECT Email,name FROM User where id = :vIdSalesMan];
        saleMa=vUser.Email; 
        system.debug('saleMa ' +saleMa);
        selectedContactEmail =saleMa;
        

        

        
        Contact vContact  =[SELECT ID, Email,Name FROM Contact where id = :vQuote.get(0).contactId];
        System.debug('Contact : '+vContact );
        String vContactName = String.valueof(vContact.Name);     
        vContactID = vContact.ID;
        System.debug('vContactID : '+vContactID);
        
        vAtt = getAttachments(quoteId);
        for( Attachment att :vAtt){ 
            if (attachment ==null){
                attachment= att.Name;
            }
            else {
                attachment =  att.Name +' , '+ attachment ;
            }
        }
        
        
        if(vOpp.get(0).Filiale__c =='Tredi Hombourg' && vQuote.get(0).Quote_Type__c =='Convention'){
            subject ='Convention  ' +vQuoteAccount ;
            body ='<p>Bonjour '+ vContactName+',</p> '+
                '<p>Veuillez trouver en annexe notre convention 2018 concernant la prise en charge de vos déchets sur notre centre TREDI Hombourg.'+
                '<br /> '+vUser.name+'  reste à votre disposition pour toutes informations complémentaires.</p>'+
                '<p>Cordialement.</p>';        
        }
        else {
            if(vQuoteAccount != vQuoteWasteRef)
                subject ='Offre commerciale ' +vQuoteAccount+' - '+vQuoteWasteRef ;
            else
                subject ='Offre commerciale ' +vQuoteAccount ;
            if(vQuote.get(0).Quote_Type__c =='Proposition commerciale')
                body ='<font face="calibri" color="#1e4477"><p>Bonjour ,</p> <p><span>Comme convenu avec '+ vUser.name +
                ', vous trouverez ci-joint notre offre commerciale concernant la prise en charge de vos déchets, </span><span>ainsi'+
                ' qu’une copie de votre certificat d’acceptation</span>.</p> <p>Dans l’attente de recevoir votre " bon pour accord " afin de valider l’acceptation, '+
                'nous nous tenons à votre entière disposition pour toutes informations.</p> <p>Bonne réception.</p> <p>Cordialement.</p></font>';        
            else if(vQuote.get(0).Quote_Type__c =='Convention')
                body ='<font face="calibri" color="#1e4477"><p>Bonjour ,</p> <p><span>Comme convenu avec '+ vUser.name +
                ', vous trouverez ci-joint notre offre commerciale concernant la prise en charge de vos déchets, </span><span>ainsi'+
                ' qu’une copie de votre certificat d’acceptation</span>.</p>  <p>Dans l’attente du retour de la convention signée, paraphée et tamponnée afin de valider '+
                'l’acceptation, nous nous tenons à votre entière disposition pour toutes informations.</p> <p>Bonne réception.</p> <p>Cordialement.</p></font>';        
        }
        System.debug('body : '+ body);    
    }
    
    
    //Action envoie Email du formulaire
    public PageReference sendMail(){
        System.debug('vContactID : '+vContactID);       
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        System.debug('toMail : '+toMail);
        string[] to = new string[] {toMail};
        Quote vQuote = (Quote)stdCtrl.getRecord();
        quoteId=  String.valueof(vQuote.id); 
        String[] cc = selectedContactEmail.split(',');


        email.setBccSender(true);
        System.debug('to : '+ to);
        email.setToAddresses(to);
        System.debug('vContactID : '+ vContactID);
        email.setTargetObjectId(vContactID);
        System.debug('quoteId : '+ quoteId);
        email.setWhatId(quoteId);
        
        System.debug('vContactID : '+ vContactID);
        if( !String.isEmpty(cc.get(0)) || !String.isBlank(cc.get(0)) ){
            System.debug('cc  : '+ cc);
          email.setCcAddresses(cc);
         }

        email.setSubject(subject);
        
        // envoie email avec les pieces jointes
        List<Messaging.EmailFileAttachment> fileAttachments = new List<Messaging.EmailFileAttachment>();
        
        for (Attachment attachItr :vAtt)
        {
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(attachItr.Name);
            efa.setBody(attachItr.Body);
            fileAttachments.add(efa);            
        }  email.setFileAttachments(fileAttachments);
        
        email.setHtmlBody(body);
        
        email.setSaveAsActivity(true);
        
        try{
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }catch(exception e){            
            apexpages.addmessage(new apexpages.message(apexpages.severity.error,e.getMessage()));           
        }
        
        //nettoyage des parametres d'entrées.
        toMail = '';
        ccMail = '';
        subject = '';
        body = '';
        attachment = '';
        
        PageReference pageRef = new PageReference('/'+quoteId);
        pageRef.setRedirect(true); 
        return pageRef;       
    }   
    
    // Return a list of Quote 
    public List<Quote> getQuotes() {
        return [SELECT Id,name,ContactID,Email, OpportunityID, Quote_Type__c, ReferenceWasteOwner__c, Account.NomUsuel__c FROM Quote where id = :ApexPages.currentPage().getParameters().get('id')];
    }
    
    // Return a list of Attachment 
    public static  List<Attachment> getAttachments(String quoteID) {
        return [SELECT id, Name, body, ContentType FROM Attachment WHERE ParentId = :quoteId];
    }   
    
}