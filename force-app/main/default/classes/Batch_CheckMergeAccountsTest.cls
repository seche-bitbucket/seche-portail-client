@isTest(SeeAllData = false)
public class Batch_CheckMergeAccountsTest {

    private static Map<String, List<Account>> mapSiretAccs = new Map<String, List<Account>>();
    private static boolean wait = true;
    
    @testSetup
    static void setup() {
        
        delete [SELECT TECH_IsDuplicate__c, TECH_IsPassed__c, TECH_NotToMerge__c FROM Account];
        
        //prep data
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.AllowSave = true; 
        
        // all correct to be merged
        final integer STRING_LENGTH_NUMBERACC = 1;
        final integer STRING_LENGTH_SIRET = 14;
        
        //random acc number
        double MIN_ACCs = Math.pow(10,STRING_LENGTH_NUMBERACC -1);
        double MAX_ACCs = Math.pow(10,STRING_LENGTH_NUMBERACC) - 1;
        Integer accNumber = Math.Round(Math.Random() * (MAX_ACCs-MIN_ACCs) + MIN_ACCs);
        System.debug('rdm0 => '+accNumber);
        
        // random siret
        Double MIN = Math.pow(10,STRING_LENGTH_SIRET -1);
        Double MAX = Math.pow(10, STRING_LENGTH_SIRET)-1;
        String siret = String.valueOf(Math.roundToLong(Math.Random() * (MAX-MIN) + MIN));
        System.debug('srt0 => '+siret);
        
        // normal accounts
        for(Integer i = 0; i<=accNumber; i++) {
            if(mapSiretAccs.containsKey(siret)) {
                List<Account> tmp = mapSiretAccs.get(siret);
                tmp.add(TestFactory.generateAccount(siret, null, 'Aucun', null, null, null, null, null, null));
                mapSiretAccs.put(siret, tmp);
            } else {
                mapSiretAccs.put(siret, new List<Account>{TestFactory.generateAccount(siret, null, 'Aucun', null, 'test', 'test', 'test', 'test', 'test')});
            }
        }
        
        // re calculate randoms
        accNumber = Math.Round(Math.Random() * (MAX_ACCs-MIN_ACCs) + MIN_ACCs);
        System.debug('rdm1 => '+accNumber);
        siret = String.valueOf(Math.roundToLong(Math.Random() * (MAX-MIN) + MIN));
        System.debug('srt1 => '+siret);
        
        // accounts with root code (not to merge 1)
        for(Integer i = 0; i<=accNumber; i++) {
            if(mapSiretAccs.containsKey(siret)) {
                List<Account> tmp = mapSiretAccs.get(siret);
                tmp.add(TestFactory.generateAccount(siret, null, 'Aucun', null, null, null, null, null, null));
                mapSiretAccs.put(siret, tmp);
            } else {
                mapSiretAccs.put(siret, new List<Account>{TestFactory.generateAccount(siret, 'test', 'Industriel', null, 'test', 'test', 'test', 'test', 'test')});
            }
        }
        
        // re calculate randoms
        accNumber = Math.Round(Math.Random() * (MAX_ACCs-MIN_ACCs) + MIN_ACCs);
        System.debug('rdm2 => '+accNumber);
        siret = String.valueOf(Math.roundToLong(Math.Random() * (MAX-MIN) + MIN));
        System.debug('srt2 => '+siret);
        
        // accounts with different nature or industry (not to merge 1) -- no root code
        for(Integer i = 0; i<=accNumber; i++) {
            if(mapSiretAccs.containsKey(siret)) {
                List<Account> tmp = mapSiretAccs.get(siret);
                tmp.add(TestFactory.generateAccount(siret, null, 'Automobile', 'Industriel', null, null, null, null, null));
                mapSiretAccs.put(siret, tmp);
            } else {
                mapSiretAccs.put(siret, new List<Account>{TestFactory.generateAccount(siret, null, 'Industriel', 'Chimie', 'test', 'test', 'test', 'test', 'test')});
            }
        }
        
        // re calculate randoms
        accNumber = Math.Round(Math.Random() * (MAX_ACCs-MIN_ACCs) + MIN_ACCs);
        System.debug('rdm3 => '+accNumber);
        siret = String.valueOf(Math.roundToLong(Math.Random() * (MAX-MIN) + MIN));
        System.debug('srt3 => '+siret);
        
        Account nonDupAcc = TestFactory.generateAccount();
        Contact ct = TestFactory.generateContact();
        ct.AccountId = nonDupAcc.Id;
        update ct;
        
        Account accInserted;
        
        // accounts with multiple contact relation (not to merge 1) -- no root code -- no different industry or nature
        for(Integer i = 0; i<=accNumber; i++) {
            if(mapSiretAccs.containsKey(siret)) {
                List<Account> tmp = mapSiretAccs.get(siret);
                tmp.add(TestFactory.generateAccount(siret, null, 'Industriel', 'Chimie', null, null, null, null, null));
                mapSiretAccs.put(siret, tmp);
            } else {
                Account acc = TestFactory.generateAccount(siret, null, 'Industriel', 'Chimie', null, null, null, null, null);
                Database.insert(acc, dml);
                accInserted = acc;
                ct.AccountId = acc.Id;
                update ct;
                mapSiretAccs.put(siret, new List<Account>{TestFactory.generateAccount(siret, null, 'Industriel', 'Chimie', 'test', 'test', 'test', 'test', 'test')});
            }
        }
        
        List<Account> lstAccs = new List<Account>();
        
        for(List<Account> accs: mapSiretAccs.values()) {
            if(accs.contains(accInserted)) {
                for(Integer i = (accs.size()-1) ; i>= 0 ; i--) {
                    if(accs[i] == accInserted) {
                		accs.remove(i);
                        break;
                    }
                }
            } 
            lstAccs.addAll(accs);
        }
        
        Database.insert(lstAccs, dml);
        
    }
    
    static testMethod  void test() {
        
        Test.startTest();
        
        Database.executeBatch(new Batch_CheckDuplicateAccounts(), 150);
        
        Test.stopTest();
        
        for(Account acc: [SELECT TECH_IsDuplicate__c, TECH_IsPassed__c, TECH_NotToMerge__c FROM Account]) {
            system.debug(acc);
        }
        
        test2();
       
    }    
    
    private static void test2() {
        
        Database.executeBatch(new Batch_MergeDuplicateAccounts(), 150);
        
    }  
}