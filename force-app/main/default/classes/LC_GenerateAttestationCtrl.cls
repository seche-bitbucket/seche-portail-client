public with sharing class LC_GenerateAttestationCtrl {   

    @AuraEnabled
    public static Attestation__c getAttestation(Id attestationId){
        if(attestationId != null){
             return [SELECT Id, Name, Sorted__c, Mixed__c, Papercardboard__c,Plastic__c,Metal__c, Glass__c, Wood__c, Plaster__c, MineralFractions__c, Status__c FROM Attestation__c WHERE Id =:attestationId][0];  
        }else{
            return null;
        }
             
    }

    @AuraEnabled
    public static RequestResponse updateAttestation( Id attestationId){
        RequestResponse resp = new RequestResponse();
        Attestation__c att =  new Attestation__c();
        att.Id = attestationId;
        att.TECH_CongaAttestationToGenerate__c  = true;
        try {
            update att;
        } catch (DmlException e) {
            throw new AuraHandledException('Erreur pendant la mise à jour de l\' attesation');           
        }  
        
        resp.message ='Votre attestation est en cours de génération. Veuillez actualiser la page dans quelques instants pour accéder aux documents dans la liste des Fichiers.';
        resp.toastMode = Label.ToastMode_Success;
        return resp;
    
}

public class RequestResponse {
    @AuraEnabled
    public string message {get; set;}
    @AuraEnabled
    public string toastMode {get; set;}
    }

}