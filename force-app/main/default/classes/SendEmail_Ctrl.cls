public with sharing class SendEmail_Ctrl {
    public static Task createEmailActivity(String subject,String description, String contactId, String Id) {

		Task t = new Task(
		Subject = subject,
		Description = description.stripHtmlTags(),
		WhatId = Id,
		TaskSubtype = 'Email',
		ActivityDate = Date.today(),
		Status = 'Achevée');
		return t;
	}

	public static ContentDocumentLink linkAttToTask(String taskId, ContentVersion cversion) {
	// Create Related file
		ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = cversion.ContentDocumentId;
        cdl.LinkedEntityId = taskId;
        cdl.ShareType = 'V';     
		return cdl;	
	}
    
    /*public static void createEmailActivity(String subject,String description, String contactId, String Id, ContentVersion cversion) {
        Task t = new Task(Subject = subject, Description = description.escapeHtml4(), WhoId = contactId, WhatId = Id, 
                          TaskSubtype = 'Email', ActivityDate = Date.today(), Status = 'Completed');    
        insert t;
        
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = cversion.ContentDocumentId;
        cdl.LinkedEntityId = t.Id;
        cdl.ShareType = 'V';          
        insert cdl;           

  }*/
    
    public static RequestResponse getEmailValues(String Id){
        String userTheme = UserInfo.getUiThemeDisplayed();
        SendEmail_Ctrl.RequestResponse resp = new SendEmail_Ctrl.RequestResponse();
        resp.message = 'Nombre de pièces jointes non conforme';
        resp.showToastMode = 'warning';
        resp.mailStatus = false;
        resp.isShowForm = 'false';
        String returnValue='Wait';
        
        Attestation__c attestation = [Select Id, Year__c, Name,  AssociatedSalesRep__c , Status__c, Contact__c, Contact__r.Name , Contact__r.Email from Attestation__c where Id =:Id];       
	    List<ContentDocumentLink> files  = [SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLink where LinkedEntityId =:Id ];
        
        Messaging.Emailfileattachment fileAttachment = new Messaging.Emailfileattachment();
        String contactGreeting;
        String CommercialText;
        String secheIndustryText;
        String ConnectiveText;
        String link;
        String LinkBody;
        String secheConnect;
        String greeting;
        String notContact;
        if(attestation.Status__c == Constants.ATTESTATION_NOTCONCERNED_STATUS){
            resp.message = 'Impossible d\'envoyer car '+Constants.ATTESTATION_NOTCONCERNED_STATUS;
            resp.showToastMode = 'warning';
            resp.isShowForm = 'false';
        }else if(attestation.Status__c == Constants.ATTESTATION_SEND_STATUS){
            resp.message = 'Attestation déjà envoyée au client.';
            resp.showToastMode = 'warning';
            resp.isShowForm = 'false';
        }else if(files != null && files.size()>0) {             
                if(files.size()<2) {
                    if(attestation.Status__c != Constants.ATTESTATION_GENERATE_STATUS) {
                        resp.message = 'Vous ne pouvez pas envoyer une attestation si elle n\'est pas au statut '+Constants.ATTESTATION_GENERATE_STATUS;
                        resp.showToastMode = 'warning';
                        resp.isShowForm = 'false';
                    }else{                              	
                            User salesRep =[SELECT Id, Name,email,Signature, Assistant__c FROM User WHERE Id = :attestation.AssociatedSalesRep__c];
                            if(salesRep.Assistant__c!=null){
                                User Assistante =[SELECT Id, Name,email, Assistant__c FROM User WHERE Id = :salesRep.Assistant__c];
                                if(String.isNotBlank(Assistante.Email)){
                                    resp.ccaddresses = new String[] { Assistante.Email, salesRep.Email };
                                }
                            }
                            ContentVersion cversion =  [SELECT title, PathOnClient, FileType, versiondata, FileExtension FROM contentversion WHERE ContentDocumentId =: files[0].ContentDocumentId];
                            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                            
                            //Create Email content
                            resp.contactId = attestation.Contact__c;
                            resp.contactName = attestation.Contact__r.Name;
                            resp.toAddresses = new String[] { attestation.Contact__r.Email };
                            //resp.bccaddresses = new String[] {salesRep.Email};
                            resp.replyTo = new String[] {salesRep.Email};
                            resp.senderDisplayName = salesRep.Name;
                            resp.subject = 'Attestation de valorisation déchets - '+attestation.Name;
                            resp.htmlBody = Constants.massEmailBody(Integer.valueOf(attestation.Year__c));
                            resp.atts = new List<Messaging.Emailfileattachment>();
                            resp.atts.add(fileAttachment);
                            resp.isShowForm = 'true';
                            resp.fileId = files[0].ContentDocumentId;
                            resp.cversions = new List<ContentVersion>();
                            resp.cversions.add(cversion);   
                       
                    }
                }else{
                    resp.message = 'Vous ne pouvez pas envoyer une attestation avec plus d\'un fichier'; 
                    resp.showToastMode = 'warning';
                    resp.isShowForm = 'false';
                }
            
        }   
        resp.objectAttestation = attestation;
			
		return resp;       
    }   
    
    
    @AuraEnabled
    public static Map<String,String> getPreviewEmail(String Id){	
        Map<String,String> result = new Map<String,String>();
        Boolean isEmailAddressBounced = false;
        Attestation__c attestation = [SELECT Id, Status__c, Contact__r.Name, Contact__r.Email, Contact__r.IsEmailBounced FROM Attestation__c WHERE Id=:Id];
        String status = attestation.Status__c;
        //Check if Email address is bounced
        if(attestation.Contact__r.IsEmailBounced ==true) {
            isEmailAddressBounced = true;
            result.put('message',' L\'adresse mail suivante est incorrecte : '+attestation.Contact__r.Email+' ('+attestation.Contact__r.Name+')');
            result.put('isEmailAddressBounced', 'isEmailAddressBounced');
            result.put('initialStatus', Status);
            }else{
                SendEmail_Ctrl.RequestResponse resp = new  SendEmail_Ctrl.RequestResponse();
                resp = getEmailValues(Id);    
                result.put('message', resp.message !=null ? resp.message : '');
                result.put('contactId', resp.contactId);
                result.put('contactName', resp.contactName);
                result.put('toAddresses', resp.toAddresses!=null ? resp.toAddresses[0] : '');
                if(resp.ccaddresses!= null && String.isNotBlank(resp.ccaddresses[0])){
                    result.put('ccaddresses', resp.ccaddresses!=null ? String.join(resp.ccaddresses, ' ; ') : '');
                }else {
                    result.put('ccaddresses',UserInfo.getUserEmail());
                }
                result.put('bccaddresses', resp.bccaddresses!=null ? resp.bccaddresses[0] : '');
                result.put('replyTo', resp.replyTo!=null ? resp.replyTo[0] : '');
                result.put('senderDisplayName', resp.senderDisplayName!=null ? resp.senderDisplayName : '');
                result.put('subject', resp.subject!=null ? resp.subject : '');
                result.put('fileId', resp.fileId!=null ? resp.fileId : '');
                result.put('body', resp.htmlBody!=null ? resp.htmlBody : '');
                result.put('isShowForm', resp.isShowForm);
                result.put('status', status);               
        }
        
        return result;
    }
    
    @AuraEnabled
    public static Map<String,String> sendMailMethod(String contactId, String contactName, String fromAddresses, String toAddresses, String cciEmail, 
                                                    String senderDisplayName, String subject, String body, String Id , String tech_uid){

                                                        System.debug(cciEmail);
        
        Map<String,String> result = new Map<String,String>();
    
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
    
        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
        Messaging.Emailfileattachment fileAttachment = new Messaging.Emailfileattachment();
    
        ContentVersion cversion = new ContentVersion ();
    
        //RecordType rt=[SELECT Id, Name, DeveloperName, Description FROM RecordType Where DeveloperName='Devis_approuv_Read_Only' LIMIT 1];    
        result.put('message', 'Erreur lors de l\'envoi ');
        result.put('showToastMode', 'Warning');
                                                        
        if(Id != null) {
            List<Attachment> att=[SELECT id, Name, body, ContentType,ParentId FROM Attachment WHERE ParentId=:Id];
            List<ContentDocumentLink> files  = [SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLink where LinkedEntityId =:Id];
            if(files != null && files.size()>0) {//Salesforce Lightning UI running
                cversion =  [SELECT title, PathOnClient, FileType, versiondata, FileExtension, ContentDocumentId FROM contentversion WHERE ContentDocumentId =: files[0].ContentDocumentId];
                fileAttachment.setFileName(cversion.title);
                fileAttachment.setBody(cversion.versiondata);    
            } else if(att !=null && att.size()>0) { //Salesforce Classic UI running
                fileAttachment.setFileName(att[0].Name);
                fileAttachment.setBody(att[0].Body);
            }
    
            // Add to attachment file list
                       
            //message.setFileAttachments(new Messaging.Emailfileattachment[] {fileAttachment});
            message.setEntityAttachments(new List<ID>{cversion.id});
            message.setReplyTo(fromAddresses);
            message.toAddresses = new String[] {toAddresses};
            message.ccaddresses = cciEmail.trim().split(';');
            message.bccaddresses = new String[] {fromAddresses};
            message.subject = subject;
            message.setSenderDisplayName(senderDisplayName);
            message.htmlBody = body;
            message.setTargetObjectId(contactId);
            message.setSaveAsActivity(true);
    
            mails.add(message);
            Messaging.sendEmail(mails);
            
            //Update Attestation Status		
            Attestation__c attest = new  Attestation__c();           
            attest.Id = Id;	            
            attest.Status__c='Envoyée';
            Update attest;
            
           
           // createEmailActivity( subject, body, contactId, Id,cversion);
           
            //Create activity
            Task t = createEmailActivity( subject, body, contactId, Id);
            insert t;
            //Add the attachment on task
            insert linkAttToTask(t.Id, cversion);
            
            
            result.put('message', 'Votre attestation est envoyée');
        	result.put('showToastMode', 'success');
    
        }          
        return result;
    }
    

    
    
public class RequestResponse {
@AuraEnabled
public String message {get; set;}
@AuraEnabled
public String showToastMode {get; set;}
@AuraEnabled
public Attestation__c objectAttestation {get; set;}
@AuraEnabled
public List<String> toAddresses {get; set;}
@AuraEnabled
public List<String> ccaddresses {get; set;}
@AuraEnabled
public List<String> bccaddresses {get; set;}
@AuraEnabled
public List<String> replyTo {get; set;}
@AuraEnabled
public String senderDisplayName {get; set;}
@AuraEnabled
public String subject {get; set;}
@AuraEnabled
public String contactName {get; set;}
@AuraEnabled
public String contactId {get; set;}
@AuraEnabled
public String htmlBody {get; set;}
@AuraEnabled
public Boolean mailStatus {get; set;}
@AuraEnabled
public String isShowForm {get; set;}
@AuraEnabled
public List<ContentVersion> cversions {get; set;}
@AuraEnabled
public List<Messaging.Emailfileattachment> atts {get; set;}
@AuraEnabled
public String fileId {get; set;}
@AuraEnabled
public String status {get; set;}
}   

}