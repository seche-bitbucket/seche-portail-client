public with sharing class SectorCompetitorController {
    
    public List<Address> address {get; set;}
    public List<Sectors> sectors {get; set;}
    String selectedCompetitorID;
    public Sectors selectSector {get; set;}
    public String startDate {get; set;}
    public Date queryDate {get; set;}
    
    
    public void loadSectors(){
        address = new List<Address>();
        String str = ApexPages.currentpage().getparameters().get('Name');
        System.debug('Test : '+selectSector);
        System.debug('Test 2 : '+str);
        
        if(str == null && selectSector != null)
            str = selectSector.Name;
        
        System.debug('Test 3 : '+str);
        
        sectors = loadMainSectors(str);
        
        if(str != null){
            startDate = ApexPages.currentpage().getparameters().get('startDate');
            queryDate = date.parse(startDate);    
            address = findLostOpportunitiesWithSelectedSector(str);
            
            
        }else{
            
            address = new List<Address>();
            
            Address add = new Address();
            
            add.Name = 'Séché Environnement';
            add.BillingStreet = '33 Avenue du Maine';
            add.BillingCity = 'Paris 15';
            add.BillingPostalCode = '75015';
            add.BillingCountry = 'France';
            address.add(add);
            
            selectSector = new Sectors();
            selectSector.Name = 'Sélectionner une filière';
            selectSector.TurnoverFormatted = '-';
            selectSector.TonnageFormatted = '-';
            
            queryDate = Date.today();
            queryDate = Date.newInstance(queryDate.year(), 1, 1);
            startDate = queryDate.format();
            
        }
    }
    
    public PageReference loadOpportunities(){
        String str = ApexPages.currentpage().getparameters().get('getName');
        
        PageReference pr=Page.SectorCompetitionMap;
        pr.getParameters().put('Name',str);
        pr.getParameters().put('startDate',startDate);
        return pr;
    }
    
    public List<Sectors> loadMainSectors(String id){
        List<Sectors> lstS = new List<Sectors>();
        Map<String, Sectors> mapS = new Map<String, Sectors>();
        for(Opportunity opp : [Select Id, Name, AMOUNT, TONNAGEN__C, Filiere__c FROM Opportunity 
                               WHERE StageName = 'Clôturée / Perdue' AND AMOUNT != null AND TONNAGEN__C != null AND Recordtype.DeveloperName != 'OPP_OpportuniteMere' AND ATTRIBUTAIRE__C != null AND DateFinEngagement__c >=: queryDate]){
                                   
                                   if(mapS.containsKey(opp.Filiere__c)){
                                       Sectors s = mapS.get(opp.Filiere__c);
                                       s.Turnover += opp.Amount; 
                                       s.Tonnage += opp.tonnageN__c;
                                       mapS.put(s.Name, s);
                                   }else{
                                       Sectors s = new Sectors();
                                       s.Name = opp.Filiere__c; 
                                       s.Turnover = opp.Amount; 
                                       s.Tonnage = opp.tonnageN__c;
                                       mapS.put(s.Name, s);
                                   }
                                   
                                   
                               }
        
        selectSector = mapS.get(id);
        
        for(Sectors s: mapS.values()) {
            s.TurnoverFormatted = formatDecimal(s.Turnover);
            s.TonnageFormatted = formatDecimal(s.Tonnage);
            lstS.add(s);
        }
        
        return lstS; 
    }
    
    
    public List<Address> findLostOpportunitiesWithSelectedSector(String Name){
        List<Address> results = new List<Address>();
        
        Map<ID, Address> mapA = new Map<ID, Address>();
        
        for(Opportunity opp : [Select Id, AMOUNT, TONNAGEN__C, AccountId, Account.Name, Account.BillingStreet, Account.BillingCity, Account.BillingPostalCode, Account.BillingCountry, Attributaire__c, Attributaire__r.name, Attributaire__r.ParentID, Attributaire__r.Parent.Name  FROM Opportunity 
                               where StageName = 'Clôturée / Perdue' AND AMOUNT != null AND TONNAGEN__C != null AND DateFinEngagement__c >=: queryDate AND Recordtype.DeveloperName != 'OPP_OpportuniteMere' AND ATTRIBUTAIRE__C != null AND Filiere__c = : Name  LIMIT 50000]){
                                   
                                   //Query information about Competitor
                                   Competitors c = new Competitors();
                                   String competitorID ;
                                   String CompetitorName = '';
                                   
                                   if(opp.Attributaire__r.ParentID != null){
                                       c.ID = opp.Attributaire__r.ParentID;
                                       c.Name = opp.Attributaire__r.Parent.Name;
                                   }else{
                                       c.ID = opp.Attributaire__c;
                                       c.Name = opp.Attributaire__r.Name;
                                   }
                                   
                                   if(mapA.containsKey(opp.AccountId)){
                                       Address add = mapA.get(opp.AccountId);
                                       add.Turnover += opp.Amount;
                                       add.Tonnage += opp.TonnageN__c;
                                       
                                       if(add.competitors.containsKey(c.ID)){
                                           c = add.competitors.get(c.ID);
                                           c.Turnover += opp.Amount;
                                           c.Tonnage += opp.TonnageN__c;
                                           c.Count += 1;
                                           
                                       }else{
                                           c.Turnover = opp.Amount;
                                           c.Tonnage = opp.TonnageN__c;
                                           c.Count = 1;
                                       }
                                       
                                       add.competitors.put(c.ID,c);                                       
                                       
                                       mapA.put(add.ID, add);
                                   }else{
                                      
                                       
                                       Address add = new Address();
                                       add.ID = opp.AccountId;
                                       add.Name = opp.Account.Name;
                                       add.BillingStreet = opp.Account.BillingStreet;
                                       add.BillingCity = opp.Account.BillingCity;
                                       add.BillingPostalCode = opp.Account.BillingPostalCode;
                                       add.BillingCountry = opp.Account.BillingCountry;
                                       add.Turnover = opp.Amount;
                                       add.Tonnage = opp.TonnageN__c;
                                       add.competitors = new Map<Id,Competitors>();
                                       
                                       c.Turnover = opp.Amount;
                                       c.Tonnage = opp.TonnageN__c;
                                       c.Count = 1;
                                       
                                       add.competitors.put(c.ID,c); 
                                       mapA.put(add.ID, add);
                                   }
                               }
        
        for(Address a: mapA.values()) {
            
            a.TurnoverFormatted = formatDecimal(a.Turnover);
            a.TonnageFormatted = formatDecimal(a.Tonnage);
            a.CompetitorsList = '';
            for(Competitors c : a.competitors.values()){
                a.CompetitorsList += '<p><b><a href="/'+c.ID+'">'+c.Name+'<a/></b> -  CA : '+formatDecimal(c.Turnover)+' €/an - Tonnage : '+formatDecimal(c.Tonnage)+' tonnes/an pour '+c.Count+' opportunité(s)</p>';
                                
            }
            
            results.add(a);
        }
        
        return results;
        
    }    
    public String formatDecimal(Decimal deci){
        
        List<String> args = new String[]{'0','number','### ### ###,##'};
            return String.format(deci.format(), args);
        
    }
    
    
    
    
    public class Address{
        
        public String ID {get; set;}
        public String Name {get; set;}
        public String BillingStreet {get; set;}
        public String BillingCity {get; set;}
        public String BillingPostalCode {get; set;}
        public String BillingCountry {get; set;}
        public Decimal Turnover {get; set;}
        public String TurnoverFormatted {get; set;}
        public Decimal Tonnage {get; set;}
        public String TonnageFormatted {get; set;}
        public String CompetitorsList {get; set;}
        public Map<ID, Competitors> competitors {get; set;}
        
    }
    
    public class Sectors{
        
        public String Name {get; set;}
        public Decimal Turnover {get; set;}
        public String TurnoverFormatted {get; set;}
        public Decimal Tonnage {get; set;}
        public String TonnageFormatted {get; set;}
        
    }
    
    public class Competitors{
        
        public String ID {get; set;}
        public String Name {get; set;}
        public Decimal Turnover {get; set;}
        public Decimal Tonnage {get; set;}
        public Integer Count {get; set;}
        
    }
    
}