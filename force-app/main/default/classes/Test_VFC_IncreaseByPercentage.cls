/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-06-29
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for Increase By percentage on Amendment Button
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class Test_VFC_IncreaseByPercentage {
    public static PageReference myVfPage {get;set;}
    public static ApexPages.StandardSetController stc {get;set;}
    public static VFC_IncreaseByPercentage vfc{get;set;}    
    @isTest static void TestIncreasePercentage(){
        PageReference ref = new PageReference('/001/o');
        Test.setCurrentPage(ref);
        ApexPages.currentPage().getParameters().put('retURL', '/001/o');
        Contract__c c = new Contract__c();
        c.name='t';
        insert c;
        Amendment__c am = new Amendment__c();
        am.Contrat2__c=c.id;
        am.Nature__c='Conditionnement';
        am.PriceCurrentYear__c=120.12;
        Amendment__c am2 = new Amendment__c();
        am2.Contrat2__c=c.id;
        am2.Nature__c='Traitement';
        am2.PriceCurrentYear__c=120.72;
        Amendment__c am3 = new Amendment__c();
        am3.Contrat2__c=c.id;
        am3.Nature__c='Prestation';
        am3.PriceCurrentYear__c=120.00;
        List<Amendment__c> Amendments = new List<Amendment__c>();
        Amendments.add(am);
        Amendments.add(am2);
        Amendments.add(am3);
        insert Amendments;
        stc=new Apexpages.standardSetController(Amendments);
        stc.setSelected(Amendments);
        vfc = new VFC_IncreaseByPercentage(stc);
        test.startTest();
        vfc.save();
        System.assertEquals(ApexPages.Severity.WARNING, ApexPages.getMessages()[0].getSeverity());
        System.assertEquals('Veuillez saisir une valeur positive', ApexPages.getMessages()[0].getSummary());
        vfc = new VFC_IncreaseByPercentage(stc);
        vfc.inputValueConditionnement=20;
        vfc.save();
        test.stopTest();
        am=[SELECT PriceNextYear__c,PriceCurrentYear__c from Amendment__c Where Id=:am.Id];
        System.assertEquals(144.144, am.PriceNextYear__c);
        vfc = new VFC_IncreaseByPercentage(stc);
        vfc.inputValue=10;
        vfc.changeAllInput();
        vfc.save();
        Amendment__c amToTest=[SELECT PriceNextYear__c,PriceCurrentYear__c from Amendment__c Where Id=:am2.Id];
        vfc = new VFC_IncreaseByPercentage(stc);
        System.assertEquals(132.792, amToTest.PriceNextYear__c);
        vfc.inputValue=Null;        
        vfc.inputValueTransport=30;
        vfc.EntierHigh=true;
        vfc.save();
        Amendment__c amToTest2=[SELECT PriceNextYear__c,PriceCurrentYear__c from Amendment__c Where Id=:am3.Id];
        vfc = new VFC_IncreaseByPercentage(stc);
        System.assertEquals(156, amToTest2.PriceNextYear__c);
        vfc.inputValueTraitement=40;
        vfc.EntierAuto=true;
        vfc.save();
        Amendment__c amToTest3=[SELECT PriceNextYear__c,PriceCurrentYear__c from Amendment__c Where Id=:am2.Id];
        System.assertEquals(169, amToTest3.PriceNextYear__c);
        c.RecordLocked__c=true;
        Update c;
        vfc = new VFC_IncreaseByPercentage(stc);
    }
    @isTest static void TestExceptions(){
        List<Amendment__c> Amendments = new List<Amendment__c>();
        stc=new Apexpages.standardSetController(Amendments);
        stc.setSelected(Amendments);
        vfc = new VFC_IncreaseByPercentage(stc);
        System.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages()[0].getSeverity());
        System.assertEquals('Veuillez selectionner un avenant', ApexPages.getMessages()[0].getSummary());
    }
}