/*
 * ------------------------------------------------------------------------------------------------------------------
 * -- - Name : CRM_String_Helper
 * -- - Author : NLE
 * -- - Company 	: Capgemini
 * -- - Purpose : List of Strings used in other class (Picklist values, codes etc.)
 * --
 * -- Maintenance History:
 * --
 * -- Date 	Name Version Remarks
 * -- ----------- ---- ------- -------------------------------------------------------------------------------------
 * -- 06-08-2019 NLE 1.0 Initial version
 * --------------------------------------------------------------------------------------------------------------------
 */
public class String_Helper {
    
    //Contract Filiale
    public static final String CON_Filiale_Alcea = 'Alcea';
	public static final String CON_Filiale_Drimm = 'Drimm';
	public static final String CON_Filiale_DrimmLalande = 'Drimm Lalande';
    public static final String CON_Filiale_ECILaVraieCroix = 'ECI La Vraie Croix';
    
    //Contract Type de Convention
    public static final String CON_TypeDeConvention_DndTraitementTransport = 'DND TRAITEMENT TRANSPORT';
    public static final String CON_TypeDeConvention_DndTraitement = 'DND TRAITEMENT';
    
    //Contract Filiere
    public static final String CON_Filiere_CentreDeTriCS = 'Centre de tri CS';
    public static final String CON_Filiere_CentreDeTriDAE = 'Centre de tri DAE';
    
    //Contract status
    public static final String CON_Status_Brouillon = 'Brouillon';
    public static final String CON_Status_Soumis = 'Soumis';
	

}