@isTest
public class NewQuoteControllerTest {
    
    static Account a;
    static Contact c;
    static Opportunity opp;
    static Quote q;
    static OpportunityLineItem oli;
    static PricebookEntry pbe;
    
    @isTest
    static void init(){
        ByPassUtils.ByPass('QuoteTrigger');
            
        QteLineItemSyncTriggerV2Handler.stop();
        OppLineItemSyncTriggerV2Handler.stop();
        QuoteSyncTriggerV2Handler.stop();
        OppSyncTriggerV2Handler.stop();

        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Chimie';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        a.SIRET_Number__c = '61572033100112';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        q = new Quote();
        
        q.OpportunityId = opp.ID;
        q.Name = opp.Name;
        q.DateDevis__c = Date.today();
        q.ContactId = opp.ContactName__c;
        q.Email = opp.ContactName__r.Email;
        q.Phone = opp.ContactName__r.Phone;
        q.Fax = opp.ContactName__r.Fax;
        q.BillingStreet = opp.Account.BillingStreet;
        q.BillingCity = opp.Account.BillingCity;
        q.BillingState = opp.Account.BillingState;
        q.BillingPostalCode = opp.Account.BillingPostalCode;
        q.BillingCountry = opp.Account.BillingCountry; 
        insert q;

        opp.SyncedQuoteId = q.Id;
        update opp;
        
        Product2 prd = new product2(name = 'Test' ,TECH_ExternalID__c = '012345',Family = 'Traitement', isActive = true );
        insert prd;
        
        pbe = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=Test.getStandardPricebookId(),
                                 isActive=true);
        insert pbe;
        
        oli = new OpportunityLineItem (OpportunityID = opp.id,PriceBookEntryID=pbe.id, quantity=4, UnitPrice=4);
        System.debug('OLI DEBUG');
        System.debug(oli);
        insert oli;
    }
    
    @IsTest
    private static void test() {

        Boolean transferFIP = true;
        init();
        
        String jsonRsp = NewQuoteController.process(opp.Id,transferFIP);


        Map<String, Object> parsed = (Map<String, Object>)JSON.deserializeUntyped(jsonRsp);
        
        System.assertEquals(true, (Boolean)parsed.get('success'));
        String qid = (String)parsed.get('recordId');
        Quote qt = [Select ID, OpportunityId FROM Quote WHERE ID = : qid ];
        
        List<QuoteLineItem> listQLI  =  [SELECT Id,QuoteId,OpportunityLineItemId FROM QuoteLineItem WHERE QuoteId =: qt.Id ];
        //create fip, related qli,(dml, ID QuoteId =qid )
        List<FIP_FIP__c> fipsToUpdate = new List<FIP_FIP__c>();
        //FIP_FIP__c fipToUpdate = new FIP_FIP__c(QuoteLineItem__c = );
        for(QuoteLineItem newQli : listQLI) {
            if(newQli.OpportunityLineItemId!= null){
                FIP_FIP__c  fip  = new FIP_FIP__c( QuoteLineItem__c = newQli.Id,Quote__c = newQli.QuoteId, Collector__c = a.id);
                fipsToUpdate.add(fip);
            }
        }
        if(fipsToUpdate.size() > 0)  {
            //insert (fipsToUpdate);
        }

        String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

        //Get a profile to create User
        Profile p = [select id from profile where name in:profiLisList limit 1];
        
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);

        System.runAs(director){
            q.Status = 'Signé';
            update q;
        }

        String jsonRsp2 = NewQuoteController.process(opp.Id,false);

        jsonRsp = NewQuoteController.process(q.Id,transferFIP);
        parsed = (Map<String, Object>)JSON.deserializeUntyped(jsonRsp);

        System.assertEquals(true, (Boolean)parsed.get('success'));        
        
    }
    
}