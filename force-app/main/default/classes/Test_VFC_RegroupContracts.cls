/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-06-29
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for Regroup Contracts Controller 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class Test_VFC_RegroupContracts {
    public static PageReference myVfPage {get;set;}
    public static ApexPages.StandardSetController stc {get;set;}
    public static VFC_RegroupContracts vfc{get;set;}  
    @isTest static void TestVFC_RegroupContracts(){
        PageReference ref = new PageReference('/001/o');
        Test.setCurrentPage(ref);
        ApexPages.currentPage().getParameters().put('retURL', '/001/o');
        Profile p = [select id from profile where name = :'Directeur commercial' limit 1];
        
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        director.UserRoleId=[select Id from UserRole where Name='Direction Commerciale - Nicolas Rogeau'].Id;
        
        insert director;
        System.RunAs(director) {
            Account ac = new Account();
            ac.Name = 'testAccName';
            ac.CustomerNature__c = 'Administration';
            ac.Industry = 'Trader';
            ac.CurrencyIsoCode = 'EUR';
            ac.Producteur__c = false;
            ac.BillingPostalCode = '00000';
            ac.NAF_Number__c = '1234A';
            insert ac;
            Account ac2 = new Account();
            ac2.Name = 'TestAcc2';
            ac2.CustomerNature__c = 'Administration';
            ac2.Industry = 'Trader';
            ac2.CurrencyIsoCode = 'EUR';
            ac2.Producteur__c = false;
            ac2.BillingPostalCode = '69006';
            ac2.NAF_Number__c = '1234A';
            insert ac2;
            List<Contract__c> contractsToInsert=new List<Contract__c>();
            for(integer i=0;i<10;i++){
                Contract__c c = new Contract__c();
                c.Account__c=ac.id;
                c.Filiale__c='Séché Eco Industries Changé';
                c.RegroupmentIndex__c=null;
                c.status__c='Brouillon';
                c.name='Contract Test'+i;
                contractsToInsert.add(c);
            }
            insert contractsToInsert;
            test.startTest();            
            List<Contract__c> EmptyList=new List<Contract__c>(); 
            stc=new Apexpages.standardSetController(EmptyList);
            stc.setSelected(EmptyList);
            vfc = new VFC_RegroupContracts(stc); 
            System.assertEquals('Veuillez selectionner au moins deux contract', ApexPages.getMessages()[0].getSummary());
            System.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages()[0].getSeverity());
            stc=new Apexpages.standardSetController(contractsToInsert);
            System.debug('size: '+contractsToInsert.size());
            stc.setSelected(contractsToInsert);
            vfc = new VFC_RegroupContracts(stc); 
            vfc.regroupContracts();
            List<Contract__c> UpdatedList=[SELECT Id, Name, Account__c, Opportunity__c, RegroupmentIndex__c FROM Contract__c WHERE ID IN:contractsToInsert];
            for(Contract__c c:UpdatedList){
                System.assertEquals(1, c.RegroupmentIndex__c);
            }
            test.stopTest();
            List<Contract__c> contractsToInsert2=new List<Contract__c>();
            for(integer i=0;i<5;i++){
                Contract__c c = new Contract__c();
                c.Account__c=ac2.id;
                c.Filiale__c='Sodicome';
                c.status__c='Brouillon';
                c.RegroupmentIndex__c=1;
                c.name='Contract Test'+i;
                contractsToInsert2.add(c);
            }
            List<Contract__c> contractsToInsert3=new List<Contract__c>();
            for(integer i=0;i<5;i++){
                Contract__c c = new Contract__c();
                c.Account__c=ac.id;
                c.Filiale__c='Sodicome';
                c.RegroupmentIndex__c=2;
                c.status__c='Brouillon';
                c.name='Contract Test'+i;
                contractsToInsert3.add(c);
            }
            for(integer i=0;i<5;i++){
                Contract__c c = new Contract__c();
                c.Account__c=ac2.id;
                c.Filiale__c='Sodicome';
                c.status__c='Brouillon';
                c.RegroupmentIndex__c=null;
                c.name='Contract Test'+i;
                contractsToInsert2.add(c);
            }
            if(contractsToInsert2.size()>0){
                insert contractsToInsert2;
            }
            if(contractsToInsert3.size()>0){
                insert contractsToInsert3;
            }
            stc=new Apexpages.standardSetController(contractsToInsert3);
            stc.setSelected(contractsToInsert3);
            vfc = new VFC_RegroupContracts(stc);
            vfc.regroupContracts();
            vfc.regroupContractsIfSameAccount();
            List<Contract__c> UpdatedList2=[SELECT Id, Name, Account__c, Opportunity__c, RegroupmentIndex__c FROM Contract__c WHERE ID IN:contractsToInsert3];
            for(Contract__c c:UpdatedList2){
                System.assertEquals(4, c.RegroupmentIndex__c);
            }
            List<Contract__c> ContratsFromDiffAcc=new List<Contract__c>();
            ContratsFromDiffAcc.add(contractsToInsert2[0]);
            ContratsFromDiffAcc.add(contractsToInsert3[0]);
            stc=new Apexpages.standardSetController(ContratsFromDiffAcc);
            stc.setSelected(ContratsFromDiffAcc);
            vfc = new VFC_RegroupContracts(stc);
            vfc.regroupContracts();
            for(Contract__c contract:contractsToInsert3){
                contract.RecordLocked__c=true;
            }
            Update contractsToInsert3;
            stc=new Apexpages.standardSetController(contractsToInsert2);
            stc.setSelected(contractsToInsert2);
            vfc = new VFC_RegroupContracts(stc);
            vfc.regroupContracts();
        }
    }
}