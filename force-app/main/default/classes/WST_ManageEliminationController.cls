public class WST_ManageEliminationController {
  
    private static ApexPages.StandardController ctrl;
    
    //List of WST selected, with opportunities, without opporuntunites
    private static ID wstsToEliminate ;
   
    //Boolean to Control if everything is OK
    private static Boolean check = true;
    private static Exception exep;
    
    public WST_ManageEliminationController(ApexPages.StandardController controller){
        ctrl = controller;
        wstsToEliminate = ctrl.getRecord().ID;
     
    }
    
    public static PageReference manageWSTToEliminate(){
        
        PageReference retPage = new PageReference('/'+apexPages.CurrentPage().getParameters().get('id')); 
        
        if(wstsToEliminate != null){ 
            check = TRANSFO_ManageEliminationController.sendWasteInstanceToElimination(wstsToEliminate);
            if(check){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Transformateurs(s) envoyé avec succès en élimination'));
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Une erreur est survenue, contacter votre administrateur.'+ exep));
                
            }
        }
        
        return retPage;
    }
        
}