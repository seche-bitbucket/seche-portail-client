/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-07-16
* @systemLayer    Controller         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Controller class for button that submits the contracts for validation
2018-07-05		Changed so that the submission is done By filiale and if a group of contracts
the same filiale are submitted and are valid they are send for validation.
2018-07-16		Changed the algorithme to handle more contracts (cpu time limit ) 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class VFC_ContratsSubmittedForValidation {
    public List<Contract__c> ContractList {get;set;}
    public Set<Id> ContractsToSubmit{get;set;}
    public Id UserId{get;set;}
    public List<UserWrapper> DirectorWrapper{get;set;}
    public Boolean error {get;set;}
    public VFC_ContratsSubmittedForValidation(ApexPages.StandardSetController controller){
        this.error=false;
        this.DirectorWrapper =new List<UserWrapper>();
        // retrieve all the commercial directors that are active
        Profile directorProfile=[SELECT Id, Name FROM Profile WHERE Name=:'Directeur commercial'];
        List<User> Directors =[SELECT ID,Name,Email FROM USER WHERE ProfileId=:directorProfile.Id AND isActive=:true];
        for(User u:Directors){
            UserWrapper uw=new UserWrapper(u);
            this.DirectorWrapper.add(uw);
        }
        // retrieve all contracts from current User
        this.Userid=UserInfo.getUserId();
        this.ContractList=[SELECT Id,Name,Status__c,Filiale__c,AssociatedSalesRep__c,AssociatedSalesRep__r.Name FROM Contract__c WHERE
                           AssociatedSalesRep__c=:Userid AND Status__c='Brouillon'];   
        List<Contract__c> Contractsubmitted=[SELECT Id,Name,Status__c,Filiale__c,AssociatedSalesRep__c,AssociatedSalesRep__r.Name FROM
                                             Contract__c WHERE AssociatedSalesRep__c=:Userid AND Status__c='Soumis'];  
        List<Contract__c> ContractValidated=[SELECT Id,Name,Status__c,Filiale__c,AssociatedSalesRep__c,AssociatedSalesRep__r.Name FROM
                                             Contract__c WHERE AssociatedSalesRep__c=:Userid AND Status__c='Valide'];
        integer count = [SELECT count() FROM Contract__c WHERE AssociatedSalesRep__c=:Userid LIMIT 1];
        if(this.ContractList.size()<=0){
            if(Contractsubmitted.size()<=0 && ContractValidated.size()<=0){
                ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Votre utilisateur n\'est associé à aucun contrat ou n\'est pas autorisé à soumettre pour validation');
                ApexPages.addMessage(errorMessage);
            }else if(ContractValidated.size()>0){
                ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Les contrats sont validé');
                ApexPages.addMessage(errorMessage);
            }else{
                ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Les contrats sont soumis pour approbation');
                ApexPages.addMessage(errorMessage);
            }
            this.error=true;
        }else{
            List <Amendment__c> Amendments = new List<Amendment__c>();
            Map<String,List<Amendment__c>> AmendmentsByFiliale=new Map<String,List<Amendment__c>>();
            Amendments = [SELECT Id,ByPassContract__c,PriceCurrentYear__c, PriceNextYear__c, Contrat2__c,Contrat2__r.Status__c,
                          Contrat2__r.Type_de_convention__c, Contrat2__r.Filiale__c FROM Amendment__c WHERE 
                          Contrat2__c IN:ContractList Order By Contrat2__r.Filiale__c];
            Set<String> Filiales=new Set<String>();
            for(Contract__c a:[SELECT Id,Filiale__c FROM Contract__c WHERE AssociatedSalesRep__c=:Userid AND Id IN:ContractList Order By Filiale__c]){
                if(a.Filiale__c!=null){
                    Filiales.add(a.Filiale__c);
                }
            }
            List<String> ListFiliales= new List<String>(Filiales);
            Integer j=0;
            for(Integer i=0;i<ListFiliales.size();i++){
                List<Amendment__c> AmendmentList=new List<Amendment__c>();
                do{
                    AmendmentList.add(Amendments[j]);
                    j++;
                }
                while( Amendments.size()!=j && Amendments[j].Contrat2__r.Filiale__c==ListFiliales[i] );
                if(AmendmentList.size()>0 && !AmendmentList.IsEmpty() &&  AmendmentList.size()>0){
                    AmendmentsByFiliale.put(ListFiliales[i],AmendmentList);
                }
            }
            this.ContractsToSubmit=new Set<Id>();
            for(String filiale: AmendmentsByFiliale.keyset()){
                Integer Counter=0;
                Integer noTemplate=0;
                Set<Id> ContractCounter=new Set<Id>();
                Set<Id> ContractNoTemplate=new Set<Id>();
                Set<Id> ContractNotChanged=new Set<Id>();
                for(Amendment__c a:AmendmentsByFiliale.get(filiale)){
                    if(a.PriceNextYear__c==0 && a.ByPassContract__c==false && a.PriceCurrentYear__c!=null ){
                        counter++;
                        ContractCounter.add(a.Contrat2__c);
                    }else if(a.Contrat2__r.Type_de_convention__c==null){
                        noTemplate++;
                        ContractNoTemplate.add(a.Contrat2__c);
                    }
                    else{
                        ContractNotChanged.add(a.Contrat2__c);   
                    }
                }
                if(Counter!=0){
                    this.error=true;
                    ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.INFO,'Vous avez toujours '+ContractCounter.size() +' contrats, '+ Counter+' avenants non traités pour la filiale '+filiale+'. Veuillez les traiter pour soumettre pour approbation');
                    ApexPages.addMessage(errorMessage);
                }else if(noTemplate!=0){
                    this.error=true;
                    ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.INFO,'Le modèle de convention n\'est pas choisi pour '+ContractNoTemplate.size()+' contrats pour la filiale '+filiale);
                    ApexPages.addMessage(errorMessage);
                }else{
                    this.ContractsToSubmit.addall(ContractNotChanged); 
                }
            }
            if(this.ContractsToSubmit.size()>0){
                this.error=false;
            }
        }
    }
    
    // submits the contracts for validation and send an email to the chosen commercial director so he can validate the contracts
    public PageReference Submit(){
        PageReference ReturnPage;
        List<Contract__c> ContractToUpdate=new List<Contract__c>();
        for(Contract__c c:this.ContractList){
            c.Status__c='Soumis';
            ContractToUpdate.add(c);
        }
        ID DirectorId;
        Integer c=1;
        for(UserWrapper uw:this.DirectorWrapper ){
            if(uw.isSelected==true){
                DirectorId=uw.DirectorId;
                c++;
            }
        }
        // send an email to the commercial director so he can access the list of contracts and validate them
        if(c==2){
            User DC=[SELECT ID,Name,Email FROM USER WHERE Id=:DirectorId LIMIT 1];
            try{
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new String[] {DC.Email});
                mail.setReplyTo(DC.Email);
                mail.setSenderDisplayName('Salesforce Augmentation Tarifaire');
                mail.setSubject('Augmentations tarifaires pour '+ContractList[0].AssociatedSalesRep__r.Name);
                Integer Nextyear = system.today().year() + 1;
                mail.setHtmlBody('Bonjour '+DC.Name+', <br/>        '
                                 +' Vous avez demande de validation pour les augmentations tarifaires '+Nextyear
                                 +' <br/> pour les valider cliquer sur ce <a href="'+Url.getSalesforceBaseUrl().toExternalForm()
                                 +'/apex/VFP_SalesDetails?userid='+ContractList[0].AssociatedSalesRep__c+'">lien Salesforce</a>');
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }catch(Exception E){
                System.debug(E);
            }
            if(ContractToUpdate.size()>0){
                update ContractToUpdate;
                ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.Confirm,'Vos contrats sont soumis pour validation.');
                ApexPages.addMessage(errorMessage);
                this.error=true;
                String ObjectPrefix = Contract__c.sObjectType.getDescribe().getKeyPrefix();
                PageReference page = new PageReference('/'+ObjectPrefix); 
                page.setRedirect(true);
                ReturnPage=page;
            }
        }else if(c==1){    
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Veuillez choisir un directeur commercial');
            ApexPages.addMessage(errorMessage);
            ReturnPage=null;
        }else{
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Veuillez choisir un seul directeur commercial');
            ApexPages.addMessage(errorMessage);
            ReturnPage=null;
        }
        return ReturnPage;
    }
    public Pagereference cancel(){
        String ObjectPrefix = Contract__c.sObjectType.getDescribe().getKeyPrefix();
        PageReference page = new PageReference('/'+ObjectPrefix); 
        page.setRedirect(true);
        return  page;
    }
    public class UserWrapper {
        public Boolean isSelected {get;set;}
        public String Director {get;set;}
        public Id DirectorId {get;set;}
        public UserWrapper(User u){
            Director=u.Name;
            DirectorId=u.Id;
            isSelected=false;
        }
    }
}