@isTest
public with sharing class LC_SetQuoteStatusToSignedCtrlTest {
    @isTest
     public static void testGetQuote(){
        Profile p = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];        
        String testemail2 = 'assistanttest@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        director.UserRoleId=[select Id from UserRole where Name='Direction Commerciale - Nicolas Rogeau'].Id;
        insert director;
        System.RunAs(director) {
            Account a;
            Contact c;
            Opportunity opp;
            Quote q;
            a = new Account();
            a.Name = 'testAccName';
            a.CustomerNature__c = 'Administration';
            a.Industry = 'Trader';
            a.CurrencyIsoCode = 'EUR';
            a.Producteur__c = false;
            a.BillingPostalCode = '00000';
            a.NAF_Number__c = '1234A';
            a.BillingStreet = '12 rue Pré Gaudry';
            insert a;
            
            c = new Contact();
            c.LastName = 'testName';
            c.AccountId = a.Id;
            c.Email = 'sechetestsfdc@gmail.com';
            c.Phone = '000000000';
            insert c;
            
            opp = new Opportunity();
            opp.Name = 'Mon OPP Convention';
            opp.StageName = 'Contractualisation';
            opp.CloseDate = System.today().addDays(10);
            opp.AccountId = a.Id;
            opp.TonnageN__c = 0;
            opp.Filiale__c = 'Tredi Hombourg';
            opp.Filiere__c = 'Physico';
            opp.ContactName__c = c.Id;
            insert opp;
            
            q = new Quote();
            q.OpportunityId = opp.ID;
            q.Name = opp.Name;
            q.DateDevis__c = Date.today();
            q.QuoteCode__c=opp.Name+'test ';
            q.ContactId = opp.ContactName__c;
            q.Email = 'sechetestsfdc@gmail.com';
            q.Phone = opp.ContactName__r.Phone;
            q.Fax = opp.ContactName__r.Fax;
            q.BillingStreet = opp.Account.BillingStreet;
            q.BillingCity = opp.Account.BillingCity;
            q.BillingState = opp.Account.BillingState;
            q.BillingPostalCode = opp.Account.BillingPostalCode;
            q.BillingCountry = opp.Account.BillingCountry; 
            insert q;           
                     
            System.assertEquals(q.Id, LC_SetQuoteStatusToSignedCtrl.getQuote(q.Id).Id);
           }
    }

     @isTest
    public static void testrequestToSigned(){
        Profile p = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];        
        String testemail2 = 'assistanttest@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        director.UserRoleId=[select Id from UserRole where Name='Direction Commerciale - Nicolas Rogeau'].Id;
        insert director;
        System.RunAs(director) {
            Account a;
            Contact c;
            Opportunity opp;
            Quote q, q1;
            a = new Account();
            a.Name = 'testAccName';
            a.CustomerNature__c = 'Administration';
            a.Industry = 'Trader';
            a.CurrencyIsoCode = 'EUR';
            a.Producteur__c = false;
            a.BillingPostalCode = '00000';
            a.NAF_Number__c = '1234A';
            a.BillingStreet = '12 rue Pré Gaudry';
            insert a;
            
            c = new Contact();
            c.LastName = 'testName';
            c.AccountId = a.Id;
            c.Email = 'sechetestsfdc@gmail.com';
            c.Phone = '000000000';
            insert c;
            
            opp = new Opportunity();
            opp.Name = 'Mon OPP Convention';
            opp.StageName = 'Contractualisation';
            opp.CloseDate = System.today().addDays(10);
            opp.AccountId = a.Id;
            opp.TonnageN__c = 0;
            opp.Filiale__c = 'Tredi Hombourg';
            opp.Filiere__c = 'Physico';
            opp.ContactName__c = c.Id;
            insert opp;
            
            q = new Quote();
            q.OpportunityId = opp.ID;
            q.Name = opp.Name;
            q.DateDevis__c = Date.today();
            q.QuoteCode__c=opp.Name+'test ';
            q.ContactId = opp.ContactName__c;
            q.Email = 'sechetestsfdc@gmail.com';
            q.Phone = opp.ContactName__r.Phone;
            q.Fax = opp.ContactName__r.Fax;
            q.BillingStreet = opp.Account.BillingStreet;
            q.BillingCity = opp.Account.BillingCity;
            q.BillingState = opp.Account.BillingState;
            q.BillingPostalCode = opp.Account.BillingPostalCode;
            q.BillingCountry = opp.Account.BillingCountry; 
            insert q;

            q1 = new Quote();
            q1.OpportunityId = opp.ID;
            q1.Name = opp.Name;
            q1.DateDevis__c = Date.today();
            q1.QuoteCode__c=opp.Name+'test ';
            q1.ContactId = opp.ContactName__c;
            q1.Email = 'sechetestsfdc@gmail.com';
            q1.Phone = opp.ContactName__r.Phone;
            q1.Fax = opp.ContactName__r.Fax;
            q1.BillingStreet = opp.Account.BillingStreet;
            q1.BillingCity = opp.Account.BillingCity;
            q1.BillingState = opp.Account.BillingState;
            q1.BillingPostalCode = opp.Account.BillingPostalCode;
            q1.BillingCountry = opp.Account.BillingCountry; 
            insert q1;
          
            LC_SetQuoteStatusToSignedCtrl.setStatus(q, 'Signé');
            Quote quoteUpdate=[Select Id,Status FROM Quote Where ID=:q.ID];           
            System.assert(quoteUpdate.Status == 'Signé');

            c.Actif__c = false;
            update c;
            LC_SetQuoteStatusToSignedCtrl.setStatus(q1, 'Signé');

        }
    }
}