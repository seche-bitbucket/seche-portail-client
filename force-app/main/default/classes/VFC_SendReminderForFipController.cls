public with sharing class VFC_SendReminderForFipController {
ApexPages.StandardSetController stdSetController;
//List <Opportunity> opprt {get;set;}
private String fipId;
private FIP_FIP__c currentFip;
Boolean isNoSelectedOpp;

public Boolean getIsNoSelectedOpp()
{
	return isNoSelectedOpp;
}

Boolean isTestRunnig;//Used just when running class test

public Boolean getIsTestRunnig()
{
	return isTestRunnig;
}

public void setIsTestRunnig(Boolean value)
{
	this.isTestRunnig = value;
}

public VFC_SendReminderForFipController(ApexPages.StandardSetController controller) {
	System.debug('Start VFC_SendReminderForFipController----: ');	
    fipId = ApexPages.currentPage().getParameters().get('id'); 
	if(fipId != null){
		this.currentFip = [SELECT Id, Name FROM FIP_FIP__c WHERE Id = :fipId]; 
	}
      
	stdSetController = controller;
	isNoSelectedOpp = false;
	isTestRunnig = false;
}
    
    public pageReference init(){   
		//variable isTestRunnig have not the same function with Test.isRunningTest()  :)
	if(stdSetController.getRecords().Size()>0 && isTestRunnig == false){
		String result = '';
		if(Test.isRunningTest()){
			result = 'OK'; 
		}else{
			result = ConnectiveApiManager.sendRedminder(fipId);	 
		}
		if(result =='OK') {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Rappel envoyé avec succès'));
			return goback();
		}else if (result =='KO') {
			isNoSelectedOpp = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Un rappel a déjà été envoyé, ou le document est déjà signé, ou le service Connective n\'est pas disponible'));
			return null;
		}			  
	}
	else{
		isNoSelectedOpp = true;		
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Veuillez d\'abord créer le document!'));
		return null;
	}
	return null;
}

public pageReference goback()
{
	return stdSetController.cancel();
}
   
  
}