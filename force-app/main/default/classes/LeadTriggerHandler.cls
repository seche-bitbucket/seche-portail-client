/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : LeadTriggerHandler
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : LeadTriggerHandler trigger
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 27-02-2020 PMB 1.0 - RefactoringClass
* -- 24-03-2020 PMB 1.1 - update onAfterUpdate
* --------------------------------------------------------------------------------------------------------------------
*/
public class LeadTriggerHandler {
    
    public static List<OpportunityLineItem> oppLineItToInsert = new List<OpportunityLineItem>();
    public static List<Opportunity> oppToUpdate = new List<Opportunity>();
    
    public static void onBeforeInsert(List<Lead> listLeads){
        onBefore(listLeads,null);
    }

    public static void onBeforeUpdate(List<Lead> listLeads,Map<Id,Lead> mapOldLeads){
        onBefore(listLeads,mapOldLeads);
    }

    public static void onAfterUpdate(Map<Id,Lead> mapLeads,Map<Id,Lead> mapOldLeads){
        if(isLeadNeedConvertion(mapLeads.values())){
            leadConvert(mapLeads.values());
        }
        transfertFilesVisitReport(mapLeads);     
    }

    public static void onBefore(List<Lead> listLeads,Map<Id,Lead> mapOldLeads){
        manageUId(listLeads);
        // To Assign the Web to lead to the appropriate Subsidiary Site depending of the Zip Code Site
        setBranchForWebToLead(listLeads,mapOldLeads);
        CheckIfNameIsEmpty(listLeads);
    }

    public static void manageUId(List<lead> leadList){
        for(Lead l : leadList){       
            if (l.TECH_UID__c == null){
                l.TECH_UID__c = GuidUtil.NewGuid();
            }     
        }
    }

    public static void setBranchForWebToLead(List<Lead> leadList,Map<Id,Lead> mapOldLeads)
    {
        Map<String,Lead> leadDptCode = new Map<String,Lead>();
        String pc;

        for(Lead l : leadList){
            //checking the PostalCode for 
            //TRIADIS && SHC Lead
            if((l.WorkSiteZip__c!= NULL ||
                (mapOldLeads != null && l.WorkSiteZip__c != mapOldLeads.get(l.Id).WorkSiteZip__c)) &&
                l.WorkSiteZip__c.length()>=2 &&
                (l.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_TRIADIS_AMIANTE) || 
                l.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_SHC_DASRI))){
                
                pc = l.WorkSiteZip__c.substring(0,2);
                if (pc == Constants.STR_LED_PSTCODE_97 || pc == Constants.STR_LED_PSTCODE_98){
                    pc = l.WorkSiteZip__c.substring(0,3);
                }
                leadDptCode.put(pc,l);    
            }
            //DND && SHC Lead
            else if((l.PostalCode != NULL || 
                    (mapOldLeads != null && l.PostalCode != mapOldLeads.get(l.Id).PostalCode)) &&
                    l.PostalCode.length()>=2 && 
                    (l.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_DND) ||
                    l.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_SHC_DASRI))){
                pc = l.PostalCode.substring(0,2);
                if (pc == Constants.STR_LED_PSTCODE_97 || pc == Constants.STR_LED_PSTCODE_98){
                    pc = l.PostalCode.substring(0,3);
                }
                leadDptCode.put(pc,l);
            }
        }

        // getting the Triadis Branch linked to the postal code, with code__c in values of Work Site Zip Code
        Map<String,Regions__c> mapRegionByCode = new Map<String,Regions__c>();
        List<Regions__c> listRe = [SELECT Id,Code__c, Name, Country__c, FilialeTriadis__c, FiliereTriadis__c,  SHC_Subsidiary__c, FilialeDND__c, FiliereDND__c  FROM Regions__c WHERE Code__c in:leadDptCode.keySet()];
        if(listRe.size() > 0){
            for(Regions__c re : listRe){
                mapRegionByCode.put(re.Code__c,re);
            }
        }
        
        for(String dtp : leadDptCode.keySet()){
            Lead currLead = leadDptCode.get(dtp);
            if(mapRegionByCode.get(dtp) != null){
                if(currLead.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_TRIADIS_AMIANTE)){
                    currLead.WorkSiteCountry__c = mapRegionByCode.get(dtp).Country__c;
                    currLead.FilialeTriadis__c  = mapRegionByCode.get(dtp).FilialeTriadis__c ;
                    currLead.Filiale__c = mapRegionByCode.get(dtp).FilialeTriadis__c ;
                    currLead.Filiere__c = mapRegionByCode.get(dtp).FiliereTriadis__c;
                }
                else if(currLead.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_SHC_DASRI)){
                    currLead.Country = mapRegionByCode.get(dtp).Country__c;
                    currLead.Filiale__c = mapRegionByCode.get(dtp).SHC_Subsidiary__c ;
                    currLead.Filiere__c = Constants.STR_LED_FILIERE_DASRI;
                }
                else if(currLead.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_DND)){
                    currLead.Country = mapRegionByCode.get(dtp).Country__c;
                    currLead.Filiale__c = mapRegionByCode.get(dtp).FilialeDND__c ;
                    currLead.Filiere__c = mapRegionByCode.get(dtp).FiliereDND__c ;
                }
            }
        }
    }
      
    //Manage if the name isn't provided and set a default name
    public static void CheckIfNameIsEmpty(List<lead> leadList)
    {
        for(Lead l: leadList){
            if(l.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_TENDER)){
                if(l.LastName == null){
                    l.LastName = '[non fourni]';
                }
            }   
        }                      
    }
    
    public static Boolean isLeadNeedConvertion (List<lead> leads){
        
        for(Lead l : leads){
            if(l.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_TRIADIS_AMIANTE) || 
                l.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_SHC_DASRI) ||
                l.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_DND) || 
                l.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_TENDER) || 
                l.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_MISCELLANEOUS)){
                return true;
            }
        }
        
        return false;
    }
    
    public static void leadConvert (List<lead> leadNewList){
        List<lead> SHC_Leads = new List<lead>();
        List<lead> Triadis_Leads = new List<lead>();
        Map<Id,Lead> DND_LeadsWithOppID = new Map<Id,Lead>();
        List<lead> Tenders_Leads = new List<lead>();
        List<lead> Miscellaneous_Leads = new List<lead>();

        for(Lead l : leadNewList){
            if(l.isConverted && l.ConvertedOpportunityId != null){
                if(l.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_TRIADIS_AMIANTE)){
                    Triadis_Leads.add(l);
                }else if(l.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_SHC_DASRI)){
                    SHC_Leads.add(l);
                }else if(l.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_DND)){
                    DND_LeadsWithOppID.put(l.ConvertedOpportunityId,l);
                }else if(l.DeadlineSubmissionOffer__c != NULL && l.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_TENDER) ){
                    Tenders_Leads.add(l); 
                }else if(l.RecordTypeId == ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_MISCELLANEOUS)){
                    Miscellaneous_Leads.add(l);
                }   
            } 
        }

        if(Triadis_Leads.size() > 0){
            leadConvertTRIADIS(Triadis_Leads);
        }
        if(SHC_Leads.size() > 0){
            leadConvertSHC(SHC_Leads);
        }
        if(DND_LeadsWithOppID.size() > 0){
            leadConvertDND(DND_LeadsWithOppID);
        }
        if(Tenders_Leads.size() > 0){
            manageAOLead(Tenders_Leads);
        }
        if(Miscellaneous_Leads.size() > 0){
           manageMiscellaneousLead(Miscellaneous_Leads);
        }        
        try{
            if(oppLineItToInsert.size()>0){
                insert oppLineItToInsert;
            }
            if(oppToUpdate.size()>0){
                update oppToUpdate;
            }
        }
        catch(Exception ex){
            Log__c lg = new Log__c(ExceptionType__c=ex.getTypeName(),ApexJob__c='LeadTriggerHandler : leadConvert',ErrorMessage__c=ex.getMessage());
            insert lg;
        }    
    }
    
    public static void leadConvertTRIADIS (List<lead> leadNewList){
        Product2[] resultProductOther = [SELECT Id,Name FROM Product2 WHERE TECH_ExternalID__c =:Constants.STR_PRD_TECH_EXTERNALID_DEFAULT LIMIT 1];
        PriceBook2[] resultPrbPltfrm = [SELECT Id FROM PriceBook2 WHERE Name =:Constants.STR_PRB_NAME_PLATEFORME LIMIT 1];
        PriceBook2[] resultPrbStDD = [SELECT Id FROM PriceBook2 WHERE Name =:Constants.STR_PRB_NAME_STOCKAGE_DD LIMIT 1];
        
        Product2 productOther;
        PriceBook2 prbPltfrm;
        PriceBook2 prbStDD;
        if(resultProductOther.size()>0){
            productOther = resultProductOther[0];
        }
        if(resultPrbPltfrm.size()>0){
            prbPltfrm = resultPrbPltfrm[0];
        }
        if(resultPrbStDD.size()>0){
            prbStDD = resultPrbStDD[0];
        }

        PriceBookEntry pbeStDD; 
        PriceBookEntry pbePtfrm;
        if (prbStDD != null && productOther != null){
            pbeStDD = [select Id from PriceBookEntry where name =: productOther.name and pricebook2Id =: prbStDD.Id LIMIT 1];
        }
        if (prbPltfrm != null && productOther != null){
            pbePtfrm = [select Id from PriceBookEntry where name =: productOther.name and pricebook2Id =: prbPltfrm.Id LIMIT 1];
        }

        Id prbeId;
        String dimCond = '';
        Decimal qte = 1;
        for (Lead l : leadNewList){
            if (l.AsbestosPackagingSize__c !=null){
                dimCond = '('+l.AsbestosPackagingSize__c+')';
            }    
            if (l.Filiale__c == Constants.STR_LED_FILIALE_SECHE_ECO_INDUS_CHANGE && pbeStDD !=null){
                prbeId = pbeStDD.Id;
            }
            else{
                if(pbePtfrm != null){
                    prbeId = pbePtfrm.Id;
                }
            }

            if(l.Quantity__c != null) {
                qte = l.Quantity__c;
            }
            OpportunityLineItem oli= new OpportunityLineItem(Description =l.Waste__c+dimCond,
                                                             Packaging__c =l.AsbestosPackagingType__c,
                                                             Quantity = qte,
                                                             PriceBookEntryId = prbeId,
                                                             OpportunityId = l.ConvertedOpportunityId,
                                                             TotalPrice =0,
                                                             Unit__c = Constants.STR_UNIT_TONNE,
                                                             Nature__c=Constants.STR_NATURE_TRAITEMENT,
                                                             Sector__c=l.Filiere__c
                                                            );
            oppLineItToInsert.add(oli); 
        }
    }
    
    public static void leadConvertSHC (List<lead> leadNewList){
        PriceBook2 prbSHC = [SELECT Id FROM PriceBook2 WHERE Name =:Constants.STR_PRB_NAME_SHC];
        List<PriceBookEntry> pbes = [SELECT Id,  Pricebook2Id , UnitPrice, Product2.Id, Product2.Name, Product2.ProductCode, Product2.Family, Product2.Param_1_Num_Min__c, Product2.Param_3_Text__c, Product2.Param_4_Text__c FROM PricebookEntry WHERE Pricebook2Id =:prbSHC.Id AND (Product2.Param_4_Text__c !=null OR Product2.ProductCode =:Constants.STR_PRD_PRODUCTCODE_DEFAULT)];
        
        for (Lead l : leadNewList){
            if(l.Packaging_1__c != null){
                generateOLI(l.Packaging_1__c, l.Quantity_1__c, l.Volume_1__c, l.Filiale__c, l.ConvertedOpportunityId, pbes);
                generateOLI_Transport(l, pbes);
            }
            if(l.Packaging_2__c != null){
                generateOLI(l.Packaging_2__c, l.Quantity_2__c, l.Volume_2__c, l.Filiale__c, l.ConvertedOpportunityId, pbes);
            }
            if(l.Packaging_3__c != null){
                generateOLI(l.Packaging_3__c, l.Quantity_3__c, l.Volume_3__c, l.Filiale__c, l.ConvertedOpportunityId, pbes);
            }
            if(l.Packaging_4__c != null){
                generateOLI(l.Packaging_4__c, l.Quantity_4__c, l.Volume_4__c, l.Filiale__c, l.ConvertedOpportunityId, pbes);
            }    
            if(l.Packaging_5__c != null){
                generateOLI(l.Packaging_5__c, l.Quantity_5__c, l.Volume_5__c, l.Filiale__c, l.ConvertedOpportunityId, pbes);
            }
            
            oppToUpdate.add(new Opportunity(Id = l.ConvertedOpportunityId, PriceBook2ID = prbSHC.Id));              
        }
    }
    
    public static void leadConvertDND (Map<Id,Lead> leadWithOppId){
        PriceBook2 prbStDD = [SELECT Id FROM PriceBook2 WHERE Name =:Constants.STR_PRB_NAME_GESTION_DND LIMIT 1];
        List<PriceBookEntry> pbes = [SELECT Id,  Pricebook2Id , UnitPrice, Product2.Id, Product2.Name, Product2.ProductCode, Product2.Family, Product2.Param_3_Text__c FROM PricebookEntry WHERE Pricebook2Id = : prbStDD.Id AND isActive= true AND Product2.Param_3_Text__c != null];
        List<Opportunity> oppUpdateContact = new List<Opportunity>();
        for (Lead l : leadWithOppId.values()){
            if(l.Packaging_1__c != null){
                generateOLI_DND(l.Packaging_1__c, l.WasteType1__c, l.Volume_1__c, l.Estimated_Duration_Of_Commissioning__c, l.ConvertedOpportunityId, pbes);
            }
            if(l.Packaging_2__c != null){
                generateOLI_DND(l.Packaging_2__c, l.WasteType2__c, l.Volume_2__c, l.Estimated_Duration_Of_Commissioning2__c, l.ConvertedOpportunityId, pbes);
            }
            if(l.Packaging_3__c != null){
                generateOLI_DND(l.Packaging_3__c, l.WasteType3__c, l.Volume_3__c, l.Estimated_Duration_Of_Commissioning3__c, l.ConvertedOpportunityId, pbes);
            }
            if(l.Packaging_4__c != null){
                generateOLI_DND(l.Packaging_4__c, l.WasteType4__c, l.Volume_4__c, l.Estimated_Duration_Of_Commissioning4__c, l.ConvertedOpportunityId, pbes);
            }    
            if(l.Packaging_5__c != null){
                generateOLI_DND(l.Packaging_5__c, l.WasteType5__c, l.Volume_5__c, l.Estimated_Duration_Of_Commissioning5__c, l.ConvertedOpportunityId, pbes);
            }
            oppUpdateContact.add(new Opportunity(ID = l.ConvertedOpportunityId, PriceBook2ID = prbStDD.Id));
        }
        oppToUpdate.addAll(assignContact(leadWithOppId, oppUpdateContact));    
    }
    
    public static void manageAOLead (List<lead> leadNewList){
        Date dateDeadLine;
        for(Lead l : leadNewList){
            dateDeadLine = setStringToDateFormat(l.DeadlineSubmissionOffer__c);

            oppToUpdate.add(new Opportunity(ID = l.ConvertedOpportunityId,
                                     closeDate = dateDeadLine,
                                     Informations__c = 'Remise du dossier le : '+l.DeadlineSubmissionOffer__c,
                                    RecordTypeId = Label.OPP_RT_AO,
                                    StageName = Constants.STR_OPP_STAGENAME_PREPA_AO
                                   ));
        }
    }

    public static void manageMiscellaneousLead (List<lead> leadNewList){
        for(Lead l : leadNewList){
            oppToUpdate.add(new Opportunity(ID = l.ConvertedOpportunityId,
                                     Informations__c = l.ProspectRequirements__c
                                   ));
        }
    }
    
    public static void generateOLI_Transport (Lead l, List<PricebookEntry> pbes){
        OpportunityLineItem oli = new OpportunityLineItem();
        PriceBookEntry defaultPbEntry = new PriceBookEntry();
        for(PricebookEntry pbe : pbes){
            String dpt = Constants.STR_LED_PSTCODE_DEFAULT;
            if(l.PostalCode != null){
                dpt = (l.PostalCode).left(2);
            }    
            
            //select one price book entry with dtp field
            if(pbe.Product2 != null && pbe.Product2.Param_4_Text__c != null && pbe.Product2.Param_4_Text__c.contains(dpt) && pbe.Product2.Family == Constants.STR_PRD_FAMILY_PRESTATION){
                oppLineItToInsert.add(new OpportunityLineItem(PriceBookEntryId = pbe.ID,
                                                                Description = 'Transport département : '+dpt,
                                                                Quantity = 1,
                                                                Unit__c = Constants.STR_UNIT_FORFAIT,
                                                                OpportunityId = l.ConvertedOpportunityId,
                                                                TotalPrice = pbe.UnitPrice));
                break;
            }
            if(pbe.Product2.ProductCode == Constants.STR_PRD_PRODUCTCODE_DEFAULT){
                defaultPbEntry = pbe;
            }            
        }
        if(oli.PriceBookEntryId == null){
            getGenericPrestationPBE(l, defaultPbEntry);
        }
        oppLineItToInsert.add(oli);  
    }
    
    public static void generateOLI (String container, Decimal quantity, String volume, String filiale, ID oppID, List<PricebookEntry> pbes){   
        for(PricebookEntry pbe : pbes){
            Decimal vol = 0;
            if(volume == '06'){
                vol = 0.6;
            }else if (volume == '18'){
                vol = 1.8;
            }else{
                if(volume != null)
                	vol = Decimal.valueOf(volume);
            } 
            
            //Manage Treatment PriceBookEntry
            if (pbe.Product2 != null 
                && filiale != null
                && pbe.Product2.Param_4_Text__c != null
                && pbe.Product2.Param_4_Text__c.contains(filiale)
                && pbe.Product2.Param_3_Text__c == container
                && pbe.Product2.Param_1_Num_Min__c == vol
                && pbe.Product2.Family == Constants.STR_PRD_FAMILY_TRAITEMENT){
                    OpportunityLineItem oli = new OpportunityLineItem();
                    oli.PriceBookEntryId = pbe.ID;
                    oli.Description = container+' '+vol+' L';
                    oli.Quantity = quantity;
                    oli.Unit__c = Constants.STR_UNIT_UNITE;
                    oli.OpportunityId = OppID;
                    oli.TotalPrice = pbe.UnitPrice * quantity;
                    oppLineItToInsert.add(oli);
            }
                
            //Manage Container PriceBookEntry
            if(pbe.Product2 != null 
                && filiale != null
                && pbe.Product2.Param_4_Text__c != null
                && pbe.Product2.Param_4_Text__c.contains(filiale)
                && pbe.Product2.Param_3_Text__c == container
                && pbe.Product2.Param_1_Num_Min__c == vol
                && pbe.Product2.Family == Constants.STR_PRD_FAMILY_CONDITIONNEMENT ){
                    OpportunityLineItem oli = new OpportunityLineItem();
                    oli.Description = 'Traitement '+container+' '+vol+' L';
                    oli.PriceBookEntryId = pbe.ID;
                    oli.Quantity = quantity;
                    oli.Unit__c = Constants.STR_UNIT_UNITE;
                    oli.OpportunityId = OppID;
                    oli.TotalPrice = pbe.UnitPrice * quantity;
                    oppLineItToInsert.add(oli);
              }
        } 
    }
    
    public static void generateOLI_DND (String container, String waste, String volume, String duration, ID oppID, List<PricebookEntry> pbes){    
        for(PricebookEntry pbe : pbes){
            //Manage Treatment PriceBookEntry
            if (pbe.Product2.Param_3_Text__c == waste && pbe.Product2.Family == Constants.STR_PRD_FAMILY_TRAITEMENT){
                OpportunityLineItem oli = new OpportunityLineItem();
                oli.PriceBookEntryId = pbe.ID;
                oli.Quantity = 1;
                oli.Unit__c = Constants.STR_UNIT_TONNE;
                oli.OpportunityId = OppID;
                oli.TotalPrice = pbe.UnitPrice ;
                oppLineItToInsert.add(oli);
            }
            
            //Manage Container PriceBookEntry
            if(pbe.Product2.Param_3_Text__c == container && pbe.Product2.Family != Constants.STR_PRD_FAMILY_TRAITEMENT){
                OpportunityLineItem oli = new OpportunityLineItem();
                if(volume != null){
                    oli.Description = container+' '+volume+' pour '+waste;
                }else{
                    oli.Description = container+' pour '+waste;
                }
                oli.PriceBookEntryId = pbe.ID;
                if(duration == Constants.STR_DURATION_0_15){
                    oli.Quantity = 8;
                    oli.Unit__c = Constants.STR_UNIT_JOUR;
                }else if(duration == Constants.STR_DURATION_15_30){
                    oli.Quantity = 22;
                    oli.Unit__c = Constants.STR_UNIT_JOUR;
                }else if(duration == Constants.STR_DURATION_USAGE_UNIQUE){
                    oli.Quantity = 1;
                    oli.Unit__c = Constants.STR_UNIT_UNITE;
                }else{
                    oli.Quantity = 1;
                    oli.Unit__c = Constants.STR_UNIT_UNITE_MOIS;
                }
                oli.OpportunityId = OppID;
                oli.TotalPrice = pbe.UnitPrice;
                oppLineItToInsert.add(oli);
            }    
        } 
    }
    
    
    public static void getGenericPrestationPBE(Lead l, PricebookEntry pbe){      
        String dpt = Constants.STR_LED_PSTCODE_DEFAULT;
        if(l.PostalCode != null){
            dpt = (l.PostalCode).left(2);
        }
        oppLineItToInsert.add(new OpportunityLineItem(PriceBookEntryId = pbe.ID,
                                                Description = 'Transport département : '+dpt,
                                                Quantity = 1,
                                                Unit__c = Constants.STR_UNIT_FORFAIT,
                                                TotalPrice = 0,
                                                OpportunityId = l.ConvertedOpportunityId));           
    }
    
    
    public static List<Opportunity> assignContact(Map<Id,Lead> leadWithOppId, List<Opportunity> opps){
        List<Opportunity> oppsContactUpdate  = new List<Opportunity>();
        if(opps.size() > 0){
            for(Opportunity opp : opps){
                Lead l = leadWithOppId.get(opp.ID);
                opp.ContactName__c = l.ConvertedContactId;
                oppsContactUpdate.add(opp);
            }
        }
        return oppsContactUpdate;  
    }
    
    private static Date setStringToDateFormat(String myDate) {
        
        String[] myDateOnly = myDate.split(' ');
        String[] strDate = myDateOnly[0].split('/');
        Integer myIntDate = integer.valueOf(strDate[0]);
        Integer myIntMonth = integer.valueOf(strDate[1]);
        Integer myIntYear = integer.valueOf(strDate[2]);
        Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
        return d;
    }

    public static void transfertFilesVisitReport(Map<Id,Lead> mapLeads){
        List<ContentDocumentLink> listFile = [SELECT ContentDocumentId, LinkedEntityId FROM ContentDocumentLink where LinkedEntityId in :mapLeads.keySet()];
        List<VisitReport__c> listVisitReport = [SELECT Id,Contact__c,Lead__c FROM VisitReport__c WHERE Lead__c in :mapLeads.keySet()];

        List<ContentDocumentLink> listFileToUpdate = new List<ContentDocumentLink>();
        List<VisitReport__c> listVisitReportToUpdate = new List<VisitReport__c>();

        for(ContentDocumentLink documentLink : listFile){
            if(mapLeads.get(documentLink.LinkedEntityId)!=null){
                listFileToUpdate.add(new ContentDocumentLink(Id = documentLink.Id,LinkedEntityId = mapLeads.get(documentLink.LinkedEntityId).ConvertedContactId));
            }
        }

        for(VisitReport__c visit : listVisitReport){
            if(mapLeads.get(visit.Lead__c)!=null){
                visit.Contact__c = mapLeads.get(visit.Lead__c).ConvertedContactId;
                visit.Account__c = mapLeads.get(visit.Lead__c).ConvertedAccountId;
                listVisitReportToUpdate.add(visit);
            }
        }

        if(listFileToUpdate.size() > 0){
            update listFileToUpdate;
        }

        if(listVisitReportToUpdate.size() > 0){
            update listVisitReportToUpdate;
        }
    }
}