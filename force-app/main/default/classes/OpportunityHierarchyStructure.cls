public class OpportunityHierarchyStructure {
    
    public String currentID {get; set;}
    private String currentRTID;
    public Opportunity OpportunityHeader {get; set;}
    public List<Opportunity> IntermediaryOpportunities {get; set;}
    public List<Opportunity> SubOpportunities {get; set;}
    public String RecordType {get; set;}
    
    public OpportunityHierarchyStructure(ApexPages.StandardController controller){
        
        currentRTID = [SELECT RecordTypeID FROM Opportunity WHERE ID = : System.currentPageReference().getParameters().get( 'id' )].RecordTypeID;
        currentID = System.currentPageReference().getParameters().get( 'id' );
        initializeDate(currentRTID);     
    }    
    
    public void initializeDate(String RTId){
        
        //Opp Header
        if(RtID.startsWith('01258000000Ab0Y')){
            mainIsHeader(currentID);
            //Intermediary Opp
        }else if(
            RtID.startsWith('01258000000Ab0R') || RtID.startsWith('01258000000Ab0S') || 
            RtID.startsWith('01258000000Ab0T') || RtID.startsWith('01258000000Ab0U') || 
            RtID.startsWith('01258000000Ab0V') || RtID.startsWith('01258000000Ab0W')) {
                
                mainIsIntermediary();
            }
        
    }
    
    private void mainIsHeader(ID oppHeaderID){
        
        OpportunityHeader = [Select ID, Name, filiale__c, filiere__c, TonnageN__c, Amount FROM Opportunity Where ID = : oppHeaderID];
        IntermediaryOpportunities = [Select ID, Name, filiale__c, filiere__c, TonnageN__c,  Amount FROM Opportunity Where Opportunit_mere__c =: OpportunityHeader.Id];
        SubOpportunities = [Select ID, Name, filiale__c, TonnageN__c, filiere__c, Amount, OpportuniteMereIntra__c FROM Opportunity Where OpportuniteMereIntra__c in : IntermediaryOpportunities];
        
    }
    
    private void mainIsIntermediary(){
        Opportunity tmpOpp = [Select ID, Opportunit_mere__c FROM Opportunity Where ID =: currentID];
        mainIsHeader(tmpOpp.Opportunit_mere__c);
    }
    
    
    
}