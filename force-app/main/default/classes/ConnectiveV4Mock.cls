@isTest
global class ConnectiveV4Mock implements HttpCalloutMock{
    
    global Static String externalIdDocument;
    global Static Contact c;
    
    global HttpResponse respond(HttpRequest req) {
        
        HttpResponse res = new HttpResponse();

        System.debug('req = ' + req.getEndpoint()+' '+req.getMethod());
        String reqMethod = req.getMethod();
        String reqEndpoint = req.getEndpoint();

        if (reqEndpoint.contains('V4') && reqEndpoint.endsWith('packages/') && reqMethod == 'POST') {
            Map<String, Object> reqBody = (Map<String, Object>)JSON.deserializeUntyped(req.getBody());
            String name = (String)reqBody.get('Name');
            if(name.contains('packageOK')) {
                String resBody = TestFactory.getResponsePackage(c, null);
                res.setBody(resBody);
                res.setStatusCode(201);
            }
            //create package NOK test
            else if (name.contains('packageNOK')) {
                res.setStatusCode(400);
            }
        } else if (reqEndpoint.contains('V4') && reqEndpoint.endsWith('reminders') && reqMethod == 'POST') {
            String packId = reqEndpoint.substringBetween('packages/', '/reminders');
            try{
                //get status OK test
                if (packId == 'packageOK'){
                    res.setStatusCode(200);
                } else {
                    res.setStatusCode(400);
                }
            } catch(Exception e) {
                System.debug('exception: '+e);
            }
        } else if (reqEndpoint.contains('V4') && reqEndpoint.contains('packages/') && reqMethod == 'GET') {
            if(reqEndpoint.contains('packageOK')) {
                res.setBody(TestFactory.getResponseStatus('packageSigned', 'Finished', null, null));
                res.setStatusCode(200);
            } else if(reqEndpoint.contains('packageNOK')) {
                res.setBody(TestFactory.getResponseStatus('packageRejected', 'Rejected', 'test', null));
                system.debug(res.getBody());
                res.setStatusCode(200);
            }
        } else if (reqEndpoint.contains('V4') && reqEndpoint.contains('/download/') && reqMethod == 'GET') {
            Blob body = Blob.valueOf('TestPdfPDF');
            if(reqEndpoint.contains('packageOK')) {
            	res.setBodyAsBlob(body);
                res.setStatusCode(200);
            } else {
                res.setStatusCode(400);
            }
        }
        
        system.debug('res: '+res);

        return res;
    }
}