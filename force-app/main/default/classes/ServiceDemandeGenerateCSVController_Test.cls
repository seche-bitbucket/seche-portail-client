@isTest
public class ServiceDemandeGenerateCSVController_Test {
    
    @isTest
    public static void testServiceDemandeGenerateCSVController(){
        
        User u = TestDataFactory.createResponsableCommercialUser([SELECT Id FROM User WHERE Id = :UserInfo.getUserId()]);
        u.trigramme__c = 'SSOB';
        
        update u; 
        
        
        List<WST_Waste_Statement__c> wsts = new List<WST_Waste_Statement__c>();
        List<SD_ServiceDemand__c> sds = new List<SD_ServiceDemand__c>();
        Account acc = TestDataFactory.createAccount('Capgemini_PER');
        acc.BillingStreet = '34 rue Pre Gaudry';
        acc.BillingCity = 'Lyon';
        acc.BillingPostalCode = '69007';
        acc.BillingCountry = 'FRANCE';
        acc.TECH_X3SECHEID__c = 'FR690321';
        acc.X3AddressCode__c = 'AD1';
        acc.OwnerId = u.ID;
        insert acc;
        
        Contact ctc = TestDataFactory.createContact(acc);
        ctc.FirstName = 'Jean';
        ctc.LastName = 'Dupont';
        insert ctc;
        
        SD_ServiceDemand__c sd1 = TestDataFactory.createServiceDemand(acc, ctc, 'Remediation');
        sd1.FavoriteRemovalDate__c = date.newInstance(2018, 02, 02);
        sd1.ConfirmedRemovalDate__c = date.newInstance(2018, 02, 05);
        sd1.ResponsibleName__c = ctc.ID;
        sds.add(sd1);
        
        SD_ServiceDemand__c sd2 = TestDataFactory.createServiceDemand(acc, ctc, 'Remediation');
        sd2.FavoriteRemovalDate__c = date.newInstance(2018, 02, 02);
        sd2.ConfirmedRemovalDate__c = date.newInstance(2018, 02, 05);
        sd2.ResponsibleName__c = ctc.ID;
        sds.add(sd2);
        
        insert sds;
        for(SD_ServiceDemand__c sd : sds){
            for(WST_Waste_Statement__c wst : TestDataFactory.createWasteStatementList(sd, 2)){
                wst.TransfoFromRehab__c = false;
                wst.Brand__c = 'ALS';
                wst.KVA__c = 214;
                wst.Year__c = '1983';
                wst.Name = '45678909876';
                wst.TotalWeight__c = 1200;
                wst.LaboratoryRecordNumber__c = '1238';
                wst.PCBContent__c = 120.0; 
                wst.PCBAnalyzeResult__c = 124.2;
                
                wsts.add(wst);
            }
        }
        
        insert wsts;
        
        
        System.runAs(u){
            Test.startTest();
            
            /*
Test.setCurrentPage(Page.SD_GenerateCSV);
ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(wsts);
stdSetController.setSelected(wsts);
*/
            
            Test.setCurrentPage(Page.SD_GenerateCSV);
            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(sds);
            stdSetController.setSelected(sds);
            
            
            ServiceDemandeGenerateCSVController sdGenerateCSVController = new ServiceDemandeGenerateCSVController(stdSetController);
            
            
            Test.stopTest();
        }
        
    }
    
    @isTest
    public static void testGenerateHeader(){
        String expectedResult = 'ID_SF_RCT;ID_SF_DEMANDE;SF_DEMANDE;TYPE_SERVICE;NUM_CDE;MARCHE_NATIONAL;TIERS;TIERS_CODE_ADRESSE;TIERS_FAC;TIERS_FAC_CODE_ADRESSE;CONTACT_TIERS;DEMANDEUR;DEMANDEUR_CODE_ADRESSE;DEMANDEUR_CONTACT;RESP_CONTRAT;GESTIONNAIRE;REPRESENTANT;MARQUE;PUISSANCE;ANNEE;NUM_SERIE;POIDS_PLAQUE;POIDS_LIQUIDE;POIDS_LIQUIDE_PLAQUE;MASSE_METALLIQUE;NUM_LABO;TENEUR_PCB;ENLEVE;DATE_ENLEVEMENT;RUE_LIVRAISON;VILLE_LIVRAISON;ISSU_REHAB;NUM_DEVIS;CODE_TRANS;FILLER\n';
        
        Test.startTest();
        String result = ServiceDemandeGenerateCSVController.generateHeaderTransfo();
        ServiceDemandeGenerateCSVController.generateContentTriadis();
        ServiceDemandeGenerateCSVController.generateHeaderTriadis();
        
        Test.stopTest();
        System.debug('expectedResult'+expectedResult);
        System.debug('result'+result);
        System.assertEquals(expectedResult, result);
        
    }
    
    @isTest
    public static void testgenerateContent(){
        
        User u = TestDataFactory.createResponsableCommercialUser([SELECT Id FROM User WHERE Id = :UserInfo.getUserId()]);
        u.trigramme__c = 'SSOB';
        
        update u; 
        
        
        List<WST_Waste_Statement__c> wsts = new List<WST_Waste_Statement__c>();
        List<SD_ServiceDemand__c> sds = new List<SD_ServiceDemand__c>();
        
        Account acc = TestDataFactory.createAccount('Capgemini_PER');
        acc.BillingStreet = '34 rue Pre Gaudry';
        acc.BillingCity = 'Lyon';
        acc.BillingPostalCode = '69007';
        acc.BillingCountry = 'FRANCE';
        acc.TECH_X3SECHEID__c = 'FR690321';
        acc.X3AddressCode__c = 'AD1';
        acc.OwnerId = u.ID;
        insert acc;
        
        Contact ctc = TestDataFactory.createContact(acc);
        ctc.FirstName = 'Jean';
        ctc.LastName = 'Dupont';
        insert ctc;
        
        SD_ServiceDemand__c sd1 = TestDataFactory.createServiceDemand(acc, ctc, 'Remediation');
        sd1.FavoriteRemovalDate__c = date.newInstance(2018, 02, 02);
        sd1.ConfirmedRemovalDate__c = date.newInstance(2018, 02, 05);
        sd1.ResponsibleName__c = ctc.ID;
        sds.add(sd1);
        
        SD_ServiceDemand__c sd2 = TestDataFactory.createServiceDemand(acc, ctc, 'Remediation');
        sd2.FavoriteRemovalDate__c = date.newInstance(2018, 02, 02);
        sd2.ConfirmedRemovalDate__c = date.newInstance(2018, 02, 05);
        sd2.ResponsibleName__c = ctc.ID;
        sds.add(sd2);
        
        insert sds;
        for(SD_ServiceDemand__c sd : sds){
            for(WST_Waste_Statement__c wst : TestDataFactory.createWasteStatementList(sd, 2)){
                wst.TransfoFromRehab__c = false;
                wst.Brand__c = 'ALS';
                wst.KVA__c = 214;
                wst.Year__c = '1983';
                wst.Name = '45678909876';
                wst.TotalWeight__c = 1200;
                wst.LaboratoryRecordNumber__c = '1238';
                wst.PCBContent__c = 120.0; 
                wst.PCBAnalyzeResult__c = 124.2;
                
                wsts.add(wst);
            }
        }
        
        insert wsts;
        
        Test.setCurrentPage(Page.SD_GenerateCSV);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(sds);
        stdSetController.setSelected(sds);
        
        
        ServiceDemandeGenerateCSVController sdGenerateCSVController = new ServiceDemandeGenerateCSVController(stdSetController);        
        System.runAs(u){
            Test.startTest();
            String result = ServiceDemandeGenerateCSVController.generateContentTransfo();
            
            System.assertNotEquals('', result);
            Test.stopTest();
        }
    }
    
    @isTest
    public static void testGenerateCSV(){
        
        User u = TestDataFactory.createResponsableCommercialUser([SELECT Id FROM User WHERE Id = :UserInfo.getUserId()]);
        u.trigramme__c = 'SSOB';
        
        update u; 
        
        
        List<WST_Waste_Statement__c> wsts = new List<WST_Waste_Statement__c>();
        List<SD_ServiceDemand__c> sds = new List<SD_ServiceDemand__c>();
        
        Account acc = TestDataFactory.createAccount('Capgemini_PER');
        acc.BillingStreet = '34 rue Pre Gaudry';
        acc.BillingCity = 'Lyon';
        acc.BillingPostalCode = '69007';
        acc.BillingCountry = 'FRANCE';
        acc.TECH_X3SECHEID__c = 'FR690321';
        acc.X3AddressCode__c = 'AD1';
        acc.OwnerId = u.ID;
        insert acc;
        
        Contact ctc = TestDataFactory.createContact(acc);
        ctc.FirstName = 'Jean';
        ctc.LastName = 'Dupont';
        insert ctc;
        
        SD_ServiceDemand__c sd1 = TestDataFactory.createServiceDemand(acc, ctc, 'Remediation');
        sd1.FavoriteRemovalDate__c = date.newInstance(2018, 02, 02);
        sd1.ConfirmedRemovalDate__c = date.newInstance(2018, 02, 05);
        sd1.ResponsibleName__c = ctc.ID;
        sds.add(sd1);
        
        SD_ServiceDemand__c sd2 = TestDataFactory.createServiceDemand(acc, ctc, 'Remediation');
        sd2.FavoriteRemovalDate__c = date.newInstance(2018, 02, 02);
        sd2.ConfirmedRemovalDate__c = date.newInstance(2018, 02, 05);
        sd2.ResponsibleName__c = ctc.ID;
        sds.add(sd2);
        
        insert sds;
        for(SD_ServiceDemand__c sd : sds){
            for(WST_Waste_Statement__c wst : TestDataFactory.createWasteStatementList(sd, 2)){
                wst.TransfoFromRehab__c = false;
                wst.Brand__c = 'ALS';
                wst.KVA__c = 214;
                wst.Year__c = '1983';
                wst.Name = '45678909876';
                wst.TotalWeight__c = 1200;
                wst.LaboratoryRecordNumber__c = '1238';
                wst.PCBContent__c = 120.0; 
                wst.PCBAnalyzeResult__c = 124.2;
                
                wsts.add(wst);
            }
        }
        
        insert wsts;
        
        Test.setCurrentPage(Page.SD_GenerateCSV);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(sds);
        stdSetController.setSelected(sds);
        
        
        ServiceDemandeGenerateCSVController sdGenerateCSVController = new ServiceDemandeGenerateCSVController(stdSetController);        
        System.runAs(u){
            Test.startTest();
            ServiceDemandeGenerateCSVController.generateCSV();
            
            System.assertNotEquals('', ServiceDemandeGenerateCSVController.CSV);
            System.assertNotEquals('', ServiceDemandeGenerateCSVController.filename);
            Test.stopTest();
        }
    }
    
}