@isTest
public class GuidUtilTest {
    
    @isTest static void testGuidUtil(){
    	Test.startTest();
        
        String guid = GuidUtil.NewGuid();
        System.debug(guid);
        
        
        System.assert(guid.length() == 18);
        
        
        Test.stopTest();
    }
    

}