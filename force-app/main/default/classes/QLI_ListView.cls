public with sharing class QLI_ListView {
    
    public String QuoteId;
    public String UrlQuote { get; set;} 
    /**This is the list which will be passed to Visualforce Page and in turn passed to Flow from Visualforce**/
    public List < QuoteLineItem > LstSelectedQuoteLineItem {
        get;
        set;
    }
    
    public QLI_ListView(ApexPages.StandardSetController listcontroller) {
      
        Set < Id > QuoteLineItemSelectedIds = new set < Id > ();
        LstSelectedQuoteLineItem = new List <quoteLineItem >();
            
        for (QuoteLineItem c: (quoteLineItem[]) listcontroller.getSelected()) {
            QuoteLineItemSelectedIds.add(c.id);
        }

        
        /**Querying on the Selected records and fetching fields which are required in Flow **/
        LstSelectedQuoteLineItem = [select Id, PricebookEntryId, QuoteId, Product2Id, Description, LabelArticleCode__c, Nature__c, 
        BuyingPrice__c, UnitPrice, VATRate__c, Quantity, Unit__c, Packaging__c, 
        IncludedTGAP__c, N_CAP__c, Sector__c, TransportType__c, Outlet__c, Incoterms__c,
        IncotermsDesc__c, CollectRate__c from QuoteLineItem where id in: QuoteLineItemSelectedIds];

        QuoteId  = LstSelectedQuoteLineItem[0].QuoteId;
    
         String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        UrlQuote = baseUrl+'/lightning/r/Quote/'+QuoteId+'/view' ;
        System.debug('UrlQuote : '+UrlQuote);

    }
   
    
}