/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : LogBatchApexErrorEvent_Handler
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : Handler for the LogBatchApexErrorEvent trigger
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 27-02-2020 PMB 1.0 - Initial version (union of getApexJobName, createLogDetails)
* --------------------------------------------------------------------------------------------------------------------
*/
public class LogBatchApexErrorEvent_Handler {
    
    public static Map<Id,AsyncApexJob> jobs = new Map<Id,AsyncApexJob>();

    public static void onAfterInsert(Map<Id, BatchApexErrorEvent> newMapBatchApexErrorEvent){
        getApexJobName(newMapBatchApexErrorEvent.values());
        createLogDetails(newMapBatchApexErrorEvent.values());
    }

    public static void getApexJobName(List<BatchApexErrorEvent> batchEvents){
        Set<Id> asyncApexJobIds = new Set<Id>();
        for(BatchApexErrorEvent evt : batchEvents){
            asyncApexJobIds.add(evt.AsyncApexJobId);
        }

        jobs = new Map<Id,AsyncApexJob>(
            [SELECT id, ApexClass.Name FROM AsyncApexJob WHERE Id IN :asyncApexJobIds]
        );
    }

    public static void createLogDetails(List<BatchApexErrorEvent> newMapBatchApexErrorEvent){ 
        List<Log__c> logToInsert = new List<Log__c>();
        for(BatchApexErrorEvent evt : newMapBatchApexErrorEvent){
            String jobName = jobs.get(evt.AsyncApexJobId).ApexClass.Name;  
            Log__c a = new Log__c(
                ScopeId__c = evt.JobScope,
                ApexJob__c = jobName,
                ErrorMessage__c = evt.Message,
                ExceptionType__c = evt.ExceptionType,
                Phase__c = evt.Phase
            );
            logToInsert.add(a);
        }
        if(logToInsert.size()>0){
            insert logToInsert;
        }
    }
}