@isTest
public with sharing class FIP_LEXCSVExportControllerTest {


@isTest static void testInit() {

	Account acc = new Account();
	acc.Name = 'Test Account';
	acc.BillingPostalCode = '21000';
	acc.BillingStreet = '1 rue du test';
	insert acc;

	Contact ctc = new Contact();
	ctc.lastName = 'Test';
	ctc.AccountId = acc.ID;
	insert ctc;

	List<FIP_FIP__c> testFips = new List<FIP_FIP__c>();
	for(integer i=0; i<100; i++) {
		FIP_FIP__c fip = new FIP_FIP__c();
		fip.Collector__c = acc.ID;
		fip.Contact__c = ctc.ID;
		fip.WasteNamingCode__c = 'abc defghijklmopq';

		testFips.add(fip);
	}
	insert testFips;
	Test.StartTest();
	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
	PageReference pageRef = Page.FIP_LEXexportCSV;
	Test.setCurrentPage(pageRef);
	ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(testFips);
	FIP_LEXCSVExportController ctrl = new FIP_LEXCSVExportController(ssc);
	pageRef.getParameters().put('listid', 'ert322144');
	pageRef.getParameters().put('Object', 'FIP_FIP__c');
	pageRef.getParameters().put('listName', 'All Fip');
	ctrl.init();
	Test.StopTest();
	system.assertEquals(true,ctrl.isNoSelectedOpp);

}

@isTest static void testSave() {
	Account acc = new Account();
	acc.Name = 'Test Account';
	acc.BillingPostalCode = '21000';
	acc.BillingStreet = '1 rue du test';
	insert acc;

	Contact ctc = new Contact();
	ctc.lastName = 'Test';
	ctc.AccountId = acc.ID;
	insert ctc;

	List<FIP_FIP__c> testFips = new List<FIP_FIP__c>();
	for(integer i=0; i<100; i++) {
		FIP_FIP__c fip = new FIP_FIP__c();
		fip.Collector__c = acc.ID;
		fip.Contact__c = ctc.ID;
		fip.WasteNamingCode__c = 'abc defghijklmopq';
		testFips.add(fip);
	}
	insert testFips;
	Test.StartTest();
	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
	PageReference pageRef = Page.FIP_LEXexportCSV;
	Test.setCurrentPage(pageRef);
	ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(testFips);
	ssc.setSelected(testFips);
	FIP_LEXCSVExportController ctrl = new FIP_LEXCSVExportController(ssc);
	pageRef.getParameters().put('listid', 'ert322144');
	pageRef.getParameters().put('Object', 'FIP_FIP__c');
	pageRef.getParameters().put('listName', 'All Fip');
	ctrl.save();
	system.assertNotEquals(null,ctrl.CSV);
	PageReference exportCsvPage = Page.FIP_FipCsvDisplay;
	exportCsvPage.getParameters().put('csv', ctrl.CSV);
    FIP_FipCsvDisplayController crt = new FIP_FipCsvDisplayController();
	Test.setCurrentPage(exportCsvPage);
	Test.StopTest();


}
}