public with sharing class VFC_GetStatusQuoteController {
ApexPages.StandardSetController stdSetController;
private String quoteId;
private Quote currentQuote;
Boolean isNoSelectedOpp;

public Boolean getIsNoSelectedOpp()
{
	return isNoSelectedOpp;
}

Boolean isTestRunnig;//Used just when running class test for process else part in the init() method

public Boolean getIsTestRunnig()
{
	return isTestRunnig;
}

public void setIsTestRunnig(Boolean value)
{
	this.isTestRunnig = value;
}


public VFC_GetStatusQuoteController(ApexPages.StandardSetController controller) {
	System.debug('Start VFC_GetStatusQuoteController----: ');
	quoteId = ApexPages.currentPage().getParameters().get('id');	
	stdSetController = controller;
	isNoSelectedOpp = false;
	isTestRunnig = false;
}

public pageReference init(){
	System.debug('List Records : '+stdSetController.getSelected());
	if(stdSetController.getRecords().Size()>0 && isTestRunnig == false){
		String result = '';
		if(Test.isRunningTest()){
			result = 'OK';
		}
		else{
			//TST
			Boolean statusRes = ConnectiveApiUtils.updateAllStatus(quoteId);
			result = statusRes == true ? 'OK': 'KO';
			//End TST
			//result = ConnectiveApiManager.updateStatusByFipQuote(quoteId);
		}
		 
		if(String.isNotBlank(result) && result == 'OK') {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Status mis à jour'));
			return goback();
		}
		else{
			isNoSelectedOpp = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Erreur lors de la récupération du statut, veuillez  vérifier le service Connective'));
		}
		return null;
	}
	else{
		isNoSelectedOpp = true;
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Veuillez d\'abord créer le document!'));
	}
	return null;
}

public pageReference goback()
{
	return stdSetController.cancel();
}
}