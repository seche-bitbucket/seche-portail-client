@isTest(SeeAllData=true)
public class TRANSFO_DisplayQteForCustomerControllerT {
  
    //FINISHED CheckQuoteStatusTest1 Quote = 'Présenté' return true
     @isTest
     public static void CheckQuoteStatusTest(){
         Test.startTest(); 
         
         //create Waste_Statement
         WST_Waste_Statement__c vWst = TestDataFactory.createWasteStatement('AccountName1');
         
         //Insert Waste_Statement
         insert vWst;
         
         //Create opportunity
		 Opportunity vOpportunity = TestDataFactory.createOpportunity('OpportunityName1');    
         
         //insert opportunity
         insert vOpportunity;
         
         //link Waste_Statement to opportunity
          vWst.Opportunity__c = vOpportunity.Id;
         
         //create Quote
         Quote vQuote =new Quote();
         
         vQuote.OpportunityId=vOpportunity.Id;
         vQuote.Status= 'Présenté';
         vQuote.Name='QuoteName';
         insert vQuote;
         System.debug('vQuote = ' + vQuote.Id);
	     System.debug('vQuote.Status = ' + vQuote.Status);
         
         //link opportunity to quote
         vOpportunity.SyncedQuoteId = vQuote.Id;
         
	     //Update Object
         update vOpportunity;
         update vWst;
         
         //check the result
         Boolean vQuoteStatut = TRANSFO_DisplayQteForCustomerController.CheckQuoteStatus(vWst.Id);
          
         System.assertEquals(true, vQuoteStatut);
         Test.stopTest();

	}
    
      //FINISHED CheckQuoteStatusTest1 Quote = 'Other' return false
     @isTest
     public static void CheckQuoteStatusTest2(){
         Test.startTest(); 
         
         //create Waste_Statement
         WST_Waste_Statement__c vWst = TestDataFactory.createWasteStatement('AccountName1');
         
         //Insert Waste_Statement
         insert vWst;
         System.debug('vWst.id = ' + vWst.id);
         
         //Create opportunity
		 Opportunity vOpportunity = TestDataFactory.createOpportunity('OpportunityName1');    
         
         //insert opportunity
         insert vOpportunity;
         
         //link Waste_Statement to opportunity
          vWst.Opportunity__c = vOpportunity.Id;
         
         //create Quote
         Quote vQuote =new Quote();
         
         vQuote.OpportunityId=vOpportunity.Id;
         vQuote.Status= 'Other';
         vQuote.Name='QuoteName';
         insert vQuote;
         System.debug('vQuote = ' + vQuote.Id);
	     System.debug('vQuote.Status = ' + vQuote.Status);
         
         //link opportunity to quote
         vOpportunity.SyncedQuoteId = vQuote.Id;
         
	     //Update Object
         update vOpportunity;
         update vWst;
         
         //check the result
         Boolean vQuoteStatut = TRANSFO_DisplayQteForCustomerController.CheckQuoteStatus(vWst.Id);
          
         System.assertEquals(false, vQuoteStatut);
         Test.stopTest();

	}    

    
     //FINISHED
     @isTest
     public static void GetTotalPriceTest(){
         
        Test.startTest(); 
         
         //create Waste_Statement
         WST_Waste_Statement__c vWst = TestDataFactory.createWasteStatement('AccountName1');
         
         //Insert Waste_Statement
         insert vWst;         
         //Create opportunity
		 Opportunity vOpportunity = TestDataFactory.createOpportunity('OpportunityName1');    
         
         //insert opportunity
         insert vOpportunity;
         
         //link Waste_Statement to opportunity
          vWst.Opportunity__c = vOpportunity.Id;
         
         //create Quote
         Quote vQuote =new Quote();
         
         vQuote.OpportunityId=vOpportunity.Id;
         vQuote.Status= 'Other';
         vQuote.Name='QuoteName';

         insert vQuote;
         System.debug('vQuote = ' + vQuote.Id);
	     System.debug('vQuote.Status = ' + vQuote.Status);
         
         //link opportunity to quote
         vOpportunity.SyncedQuoteId = vQuote.Id;
         
	     //Update Object
         update vOpportunity;
         update vWst;
         
         //Check the Result
         System.debug('GetTotalPriceTest.vWst.id = ' + vWst.id);
         Decimal vQtyForCustomer = TRANSFO_DisplayQteForCustomerController.GetTotalPrice(vWst.Id);
         System.debug('GetTotalPriceTest.vQtyForCustomer = ' + vQtyForCustomer);
         System.assertEquals(0, vQtyForCustomer);
		 Test.stopTest();
	}
    
    //FINISHED
     @isTest
     public static void getQuoteTypeTest(){
         Test.startTest(); 

         
        //create Waste_Statement
        WST_Waste_Statement__c vWst = TestDataFactory.createWasteStatement('AccountName1');
        vWst.Status__c  ='Commande proposee';
        
        //insert Waste_Statement
        insert vWst;
        
         String vResult = TRANSFO_DisplayQteForCustomerController.getQuoteType(vWst.Id);
         
         //Check the Result
         System.debug('getQuoteTypeTest.vResult' + vResult );
         System.assertEquals('Remediation', vResult);
         Test.stopTest();

	}

    
    
    
        
    //FINISHED
     @isTest
     public static void     getQuoteLineItemsTest(){
         Test.startTest(); 
         
         //create Waste_Statement
         WST_Waste_Statement__c vWst = TestDataFactory.createWasteStatement('AccountName1');
         vWst.Status__c  ='Commande proposee';
         
         //insert Waste_Statement
         insert vWst;
         
         List<QuoteLine__c> vResult = TRANSFO_DisplayQteForCustomerController.getQuoteLineItems(vWst.Id);
         
         //Check the Result
         System.debug('getQuoteTypeTest.vResult' + vResult );
         System.assertNotEquals(null, vResult);
         Test.stopTest();
         
     }

    
    //FINISHED
     @isTest
     public static void ValidateQuoteTest(){
         Test.startTest();  

        //create Waste_Statement
        WST_Waste_Statement__c vWst = TestDataFactory.createWasteStatement('AccountName1');
        vWst.Status__c  ='Commande proposee';
        
        //insert Waste_Statement
        insert vWst;
         
         List<QuoteLine__c> vResult = TRANSFO_DisplayQteForCustomerController.ValidateQuote(vWst.Id);
         
         //Check the Result
         System.debug('ValidateQuoteTest.vResult' + vResult );
         System.assertNotEquals(null, vResult);
         Test.stopTest();
	}
    
 
    //FINISHED
     @isTest
     public static void AcceptQuoteControllerTest(){
         
         Test.startTest();  
          //create Waste_Statement
         WST_Waste_Statement__c vWst = TestDataFactory.createWasteStatement('AccountName1');
         
         //Insert Waste_Statement
         insert vWst;
          
         //Create opportunity
		 Opportunity vOpportunity = TestDataFactory.createOpportunity('OpportunityName1');    
         
         //insert opportunity
         insert vOpportunity;
         
         //link Waste_Statement to opportunity
          vWst.Opportunity__c = vOpportunity.Id;
         
         //create Quote
         Quote vQuote =new Quote();
         
         //link quote to opportunity
         vQuote.OpportunityId=vOpportunity.Id;
         vQuote.Status= 'Accepté';
         vQuote.Name='QuoteName';
         insert vQuote;
         System.debug('vQuote = ' + vQuote.Id);
         
         //link opportunity to quote
         vOpportunity.SyncedQuoteId = vQuote.Id;
         
	     //Update Object
         update vOpportunity;
         update vWst;

         Boolean vResult = TRANSFO_DisplayQteForCustomerController.AcceptQuoteController(vWst.Id);
		 System.debug('vResult' + vResult );
         System.assertEquals(true, vResult);         
         Test.stopTest();

	}
	

    //FINISHED at 95%
     @isTest
     public static void DeclineQuoteControllerTest(){
                 
         Test.startTest();  
          //create Waste_Statement
         WST_Waste_Statement__c vWst = TestDataFactory.createWasteStatement('AccountName1');
         
         //Insert Waste_Statement
         insert vWst;          
         //Create opportunity
		 Opportunity vOpportunity = TestDataFactory.createOpportunity('OpportunityName1');    
         
         //insert opportunity
         insert vOpportunity;
         
         //link Waste_Statement to opportunity
          vWst.Opportunity__c = vOpportunity.Id;
         
         //create Quote
         Quote vQuote =new Quote();
         
         //link quote to opportunity
         vQuote.OpportunityId=vOpportunity.Id;
         vQuote.Status= 'Refusé';
         vQuote.Name='QuoteName';
         insert vQuote;
         System.debug('vQuote = ' + vQuote.Id);
         
         //link opportunity to quote
         vOpportunity.SyncedQuoteId = vQuote.Id;
         
	     //Update Object
         update vOpportunity;
         update vWst;

         Boolean vResult = TRANSFO_DisplayQteForCustomerController.DeclineQuoteController(vWst.Id);
		 System.debug('vResult' + vResult );
         System.assertEquals(true, vResult);         
         Test.stopTest();
	}


    //  FINISHED
     @isTest
     public static void SendToEliminationControllerTest(){
         
         Test.startTest();  
         //create Waste_Statement
         WST_Waste_Statement__c vWst = TestDataFactory.createWasteStatement('AccountName1');
         vWst.RecordTypeId =  Label.RCT_RT_WST_Remediation;
         
         //Insert Waste_Statement
         insert vWst;
         System.debug('vWst.id = ' + vWst.id);
         
         //call method
         Boolean vIsSentToElimination =  TRANSFO_DisplayQteForCustomerController.SendToEliminationController(vWst.Id);
         System.debug('vIsSentToElimination = ' + vIsSentToElimination);
         
         //check the result
         System.assertEquals(true, vIsSentToElimination);
         Test.stopTest();

	}
    
	
    
}