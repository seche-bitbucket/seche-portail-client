/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-09-26
* @modified       2018-09-26
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for Batch that resets all contracts and opportunities after our courrier stage
				so that the convention stage starts
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class Test_CNT_BatchResetContractsAndOpps {
 @istest static void TestExecute(){
        Profile directp = [select id from profile where name = :'Directeur commercial' limit 1];
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = directp.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        
        insert director;
        Profile p = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];
        
        String testemail = 'Commercial-_User_test@test.com';
        User Commercial = new User(profileId = p.id, username = testemail, email = testemail,
                                   emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                   languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                   alias='cs', lastname='lastname2', IsActive=true);
        insert Commercial;
        System.RunAs(Commercial) {
            
            Account ac = new Account();
            ac.Name = 'testAccName';
            ac.CustomerNature__c = 'Administration';
            ac.Industry = 'Trader';
            ac.CurrencyIsoCode = 'EUR';
            ac.Producteur__c = false;
            ac.BillingPostalCode = '00000';
            ac.NAF_Number__c = '1234A';
            insert ac;
            Account ac2 = new Account();
            ac2.Name = 'testAccName';
            ac2.CustomerNature__c = 'Groupe Séché';
            ac2.Industry = 'Trader';
            ac2.CurrencyIsoCode = 'EUR';
            ac2.Producteur__c = false;
            ac2.BillingPostalCode = '00000';
            ac2.NAF_Number__c = '1234A';
            insert ac2;
            Contact cont = new Contact(FirstName='Test',
                                       LastName='Test',
                                       Salesman__c = Commercial.ID,
                                       Accountid= ac.id);
            insert cont;
            List<Contract__c> contractsToInsert=new List<Contract__c>();
            for(integer i=0;i<50;i++){
                Contract__c c = new Contract__c();
                c.Account__c=ac.id;
                c.Etat__c='Convention';
                c.Filiale__c='Tredi Salaise 1';
                c.ContactRelated__c=cont.Id;
                c.AssociatedSalesRep__c=Commercial.Id;
                c.name='Contract Test'+i;
                c.Status__c='Valide';
                c.Etat__c='Courrier';
                contractsToInsert.add(c);
            }
            for(integer i=0;i<20;i++){
                Contract__c c = new Contract__c();
                c.Account__c=ac2.id;
                c.Etat__c='Convention';
                c.Filiale__c='Tredi Salaise 3';
                c.ContactRelated__c=cont.Id;
                c.AssociatedSalesRep__c=Commercial.Id;
                c.name='Contract Test'+i;
                c.Status__c='Valide';
                c.Etat__c='Courrier';
                contractsToInsert.add(c);
            }
            if(contractsToInsert.size()>0){
                insert contractsToInsert;
            }
            List<Amendment__c> Amendments = new List<Amendment__c>();
            for(Contract__c contract:contractsToInsert){
                Amendment__c am = new Amendment__c();
                am.Contrat2__c=contract.id;
                am.WasteLabel__c='conditionnement amiante';
                am.Nature__c='Conditionnement';
                am.PriceCurrentYear__c=120;
                am.QuantityCurrentYear__c=10;
                Amendment__c am2 = new Amendment__c();
                am2.Contrat2__c=contract.id;
                am2.WasteLabel__c='Transport amiante';
                am2.PriceCurrentYear__c=120;
                am2.QuantityCurrentYear__c=20;
                am2.Nature__c='Prestation';
                Amendment__c am3 = new Amendment__c();
                am3.Contrat2__c=contract.id;
                am3.Nature__c='Traitement';
                am3.WasteLabel__c='traitement amiante';
                am3.PriceCurrentYear__c=140;
                am3.QuantityCurrentYear__c=40;
                Amendments.add(am);
                Amendments.add(am2);
                Amendments.add(am3);
            }        
            if(Amendments.size()>0){
                insert Amendments;
            }
            Map<Contract__c,ID> ContractMap=new Map<Contract__c,ID>();
            List<Opportunity> OppToInsert =new List<Opportunity>();
            for(Contract__c contract:contractsToInsert){
                Opportunity opp=new Opportunity(Name='Courrier'+' 2019 - '+contract.Account__r.Name,
                                                AccountId=contract.Account__c,CloseDate = date.today(),
                                                StageName ='Renouvellement',TECH_Contrat2__c=contract.Id, 
                                                ContactName__c=contract.ContactRelated__c,Salesman__c=contract.AssociatedSalesRep__c,
                                                Filiale__c=contract.Filiale__c);
                OppToInsert.add(opp);
            }
            
            if(OppToInsert.size()>0){
                insert OppToInsert;
            }
            for(Contract__c contract:contractsToInsert){
                for(Opportunity opp:OppToInsert){
                    if(contract.Id==opp.TECH_Contrat2__c){
                        Contract.Opportunity__c=opp.Id;
                        break;
                    }
                }
            }
            Update contractsToInsert;
            Test.startTest();

            CNT_BatchResetContractsAndOpps batch = new CNT_BatchResetContractsAndOpps (); 
            Id batchId = Database.executeBatch(batch);
            Test.stopTest();

            //List<Opportunity> Opportunities =[SELECT Id,Name FROM Opportunity ];
            //System.assertEquals(0, Opportunities.size());
        }
        
    }
}