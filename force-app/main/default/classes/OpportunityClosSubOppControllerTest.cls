@IsTest (seeAllData=true)
public class OpportunityClosSubOppControllerTest {

    static Opportunity oppHeader;
    static Opportunity oppInter1 = new Opportunity();
    static Opportunity oppInter2 = new Opportunity();
    static Opportunity oppSub1_1 = new Opportunity();
    static Opportunity oppSub1_2 = new Opportunity();
    
    static void init(){
        
        List<Opportunity> Opps = new List<Opportunity>();
        
        //Get a Record Type for Account
        RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
        
        //Create Account
        Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aucun', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
        
        try{
            insert ac;
        }catch(Exception e){
        }
        
        //Create Contact
        Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '03 00 00 00 00', Salesman__c = UserInfo.getUserId()  );
        
        try{
            insert contactTest;
        }catch(Exception e1){
        }
        
        //Get a Record Type for Opportunity
        
        Map<String, RecordType> RTs = new Map<String, RecordType>(); 
        for(RecordType rtOpp : [Select r.DeveloperName, r.Id From RecordType r  Where SobjectType='Opportunity'])
            RTs.put(rtOpp.DeveloperName, rtOpp);
        
        

        //Create Opportunity Inter 1
        oppInter1 = new Opportunity(Name = 'Opp_Inter1', RecordTypeId = RTs.get('OPP_06_AutreOffreSecteurPriveStandard').ID , AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
        Opps.add(oppInter1);
        
        //Create Opportunity Inter 1
        oppInter2 = new Opportunity(Name = 'Opp_Inter2', RecordTypeId = RTs.get('OPP_06_AutreOffreSecteurPriveStandard').ID , AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
        Opps.add(oppInter2);
        
        //Create Opportunity Sub 1 of Inter 1
        oppSub1_1 = new Opportunity(Name = 'Opp_Sub1', RecordTypeId = RTs.get('OPP_OpportuniteIntraGroupe').ID , OpportuniteMereIntra__c = oppInter1.ID ,AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
        Opps.add(oppSub1_1);
        
        //Create Opportunity Sub 2 of Inter 1
        oppSub1_2 = new Opportunity(Name = 'Opp_Sub2', RecordTypeId = RTs.get('OPP_OpportuniteIntraGroupe').ID , OpportuniteMereIntra__c = oppInter1.ID ,AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea');
        Opps.add(oppSub1_2);
        
        
        try{
            insert Opps;
        }catch(Exception e2){
            System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
        }                  
    }
    
    
    @isTest(seeAllData=true)
    static void TestCloseWithOpportunities(){
        
        init();
        
        //    Start TEST
        List<ID> lstSubOpp = new List<ID>();
        lstSubOpp.add(oppSub1_1.ID);
        lstSubOpp.add(oppSub1_2.ID);
         
        //    Start TEST
         Test.startTest();
             
        String result = OpportunityCloseSubOppController.CloseSubOpportunities(lstSubOpp);
        
        oppSub1_1 = [Select ID, StageName FROM Opportunity Where ID = : oppSub1_1.id];
        oppSub1_2 = [Select ID, StageName FROM Opportunity Where ID = : oppSub1_2.id];
        
        System.assertEquals(oppSub1_1.StageName, 'Clôturée / Gagnée');
        System.assertEquals(oppSub1_2.StageName, 'Clôturée / Gagnée');
        
         System.assertEquals(result, 'Opportunité(s) mise(nt) à jour avec succès');
        
        Test.stopTest();
       
        
    }
      
    
}