@isTest
public with sharing class VFC_CreateOppRecordTest {
static testMethod void setParamsTest() {
	String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

	//Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;


	System.RunAs(director) {
		PageReference pgOpport = Page.VFP_CreateOppRecord;
		Test.setCurrentPage(pgOpport);
		pgOpport.getParameters().put('accountId', '123');
		pgOpport.getParameters().put('contactId', '456');
		pgOpport.getParameters().put('recordTypeId', '789');	
		 
		VFC_CreateOppRecord myController = new VFC_CreateOppRecord();

		System.assertEquals('123', myController.accountId);
		System.assertEquals('456', myController.contactId);
        System.assertEquals('789', myController.recordTypeId);

	}

}

}