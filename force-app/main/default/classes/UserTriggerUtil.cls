with sharing  public class UserTriggerUtil {
    
    //Generate TECH_UID on account if UID does not exist
   @future
	public static void  Generate_Tech_UID(ID contactId){      

        Contact  cont= [SELECT Id, AccountId, Account.TECH_UID__c FROM Contact WHERE Id = :contactId ]; 

        if(cont != null){     
            if(cont.Account.TECH_UID__c == null){
               Account  vAccount = new Account();
               vAccount.Id = cont.accountId;
                vAccount.TECH_UID__c= GuidUtil.NewGuid();
                update vAccount;
            }
        }
    }
}