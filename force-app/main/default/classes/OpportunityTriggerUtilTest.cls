/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Yassir MERHDER <yassir.merhder@capgemini.com>
* @modifiedBy     Yassir MERHDER <yassir.merhder@capgemini.com>
* @maintainedBy   Yassir MERHDER <yassir.merhder@capgemini.com>
* @version        1.0
* @created        2018-09-27
* @modified       
* @systemLayer    Test class         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test of OpportunityTriggerUtil class
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
private class OpportunityTriggerUtilTest {
    @isTest static void getOpportunityTeamsTest(){
        //Create Opportunities list
        Opportunity opp = new Opportunity (Filiere__c='Biocentre');
        System.debug('opp.Filiere__c= '+opp.Filiere__c);
        List<Opportunity> opps = new List<Opportunity>();
        insert opps;
        //Create an Equipe__c
        Equipe__c eq = new Equipe__c(Filiere__c='Biocentre');
        insert eq;
        Map<String, Equipe__c> results = OpportunityTriggerUtil.getOpportunityTeams(opps); 
        //system.assertEquals(results.get('Test'), eq);
    }
    
     @isTest static void getOpportunityTeamToInsertTest(){
        //Create Opportunities list
        Opportunity opp = new Opportunity (Filiere__c='Biocentre', Filiale__c='Alcea');
        System.debug('opp.Filiere__c= '+opp.Filiere__c);
        List<Opportunity> opps = new List<Opportunity>();
        opps.add(opp);
        //Create a Map<String, Equipe__c> equipes
        //Create an Equipe__c
        Equipe__c eq = new Equipe__c(Filiere__c='Biocentre', Exceptions_Site_Filiale__c='Alcea');
        Map<String, Equipe__c> equipes = new Map<String, Equipe__c>();
        equipes.put(opp.Filiere__c, eq);
        List<OpportunityTeamMember> results = OpportunityTriggerUtil.getOpportunityTeamToInsert(opps,equipes);
        system.assertEquals(0, results.size());
        equipes.get(opp.Filiere__c).Exceptions_Site_Filiale__c=null;
        results = OpportunityTriggerUtil.getOpportunityTeamToInsert(opps,equipes);
        system.assertEquals(1, results.size());
        //Random ref for equipe__c.Assitant__c
        User ref = [SELECT id from User where id!=null LIMIT 1];
        equipes.get(opp.Filiere__c).Assistant__c=ref.Id;
        //System.assertEquals(null,equipes.get(opp.Filiere__c).Assistant__c);
        results = OpportunityTriggerUtil.getOpportunityTeamToInsert(opps,equipes);
        system.assertEquals(2, results.size());
    }
    
  /*  @isTest static void manageUserToUnsubscribeTest(){
        //Create a User
        User usr = [Select ID From User Where Id = : UserInfo.getUserId()];
        //Create Opportunities list
        List<Opportunity> opps = new List<Opportunity>();
        opps.add(new Opportunity (Name='Testt',CloseDate=Date.newInstance(1960, 2, 17),StageName='Clôturée / Gagnée'));
        //insert opps;
        //system.assertEquals(usr.id, opps[0].Id);
        EntitySubscription ess = new EntitySubscription(SubscriberId=usr.id,ParentId=opps[0].Id);
        insert ess;
        List<EntitySubscription> esss = OpportunityTriggerUtil.manageUserToUnsubscribe(opps);
        //system.assertEquals(1, esss.size());
    } */
    
    @isTest static void manageOpportunityRecordTypeTest(){
        //Create Opportunities list
        List<Opportunity> opps = new List<Opportunity>();
        Opportunity oppMother = new Opportunity(Name ='Mom',CloseDate=Date.newInstance(1960, 2, 17),StageName='Clôturée / Gagnée');
        insert oppMother;
        /*RecordType rt1 = new RecordType(DeveloperName='OPP_01_OffreSpeichimStandard');insert rt1;
        RecordType rt2 = new RecordType(DeveloperName='OPP_02_OffresOGGDStandard');insert rt2;
        RecordType rt3 = new RecordType(DeveloperName='OPP_03_AppelOffrePubliqueStandard');insert rt3;
        RecordType rt4 = new RecordType(DeveloperName='OPP_04_OffrePrestationSiteStandard');insert rt4;
        RecordType rt5 = new RecordType(DeveloperName='OPP_05_AutreOffreSecteurPublicStandard');insert rt5;
        RecordType rt6 = new RecordType(DeveloperName='OPP_06_AutreOffreSecteurPriveStandard');insert rt6;
        RecordType rt01 = new RecordType(DeveloperName='OPP_01_OffreSpeichimIntermediaire');insert rt01;
        RecordType rt02 = new RecordType(DeveloperName='OPP_02_OffresOGGDIntermediaire');insert rt02;
        RecordType rt03 = new RecordType(DeveloperName='OPP_03_AppelOffrePubliqueIntermediaire');insert rt03;
        RecordType rt04 = new RecordType(DeveloperName='OPP_04_OffrePrestationSiteIntermediaire');insert rt04;
        RecordType rt05 = new RecordType(DeveloperName='OPP_05_AutreOffreSecteurPublicIntermediaire');insert rt05;
        RecordType rt06 = new RecordType(DeveloperName='OPP_06_AutreOffreSecteurPriveIntermediaire');insert rt06;
        
        rts.add(rt1); rts.add(rt2); rts.add(rt3); rts.add(rt4); rts.add(rt5); rts.add(rt6); rts.add(rt01); rts.add(rt02); rts.add(rt03); rts.add(rt04);
        rts.add(rt05); rts.add(rt06);*/
        List<RecordType> rts = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN('OPP_01_OffreSpeichimStandard','OPP_02_OffresOGGDStandard', 
                               'OPP_03_AppelOffrePubliqueStandard','OPP_04_OffrePrestationSiteStandard','OPP_05_AutreOffreSecteurPublicStandard', 
                               'OPP_06_AutreOffreSecteurPriveStandard','OPP_01_OffreSpeichimIntermediaire','OPP_02_OffresOGGDIntermediaire', 
                               'OPP_03_AppelOffrePubliqueIntermediaire','OPP_04_OffrePrestationSiteIntermediaire','OPP_05_AutreOffreSecteurPublicIntermediaire',
                               'OPP_06_AutreOffreSecteurPriveIntermediaire')];
        opps.add(new Opportunity (Name='Test1',Opportunit_mere__c=oppMother.Id,CloseDate=Date.newInstance(1960, 2, 17),RecordTypeId=rts[0].Id,StageName='Clôturée / Gagnée'));
        opps.add(new Opportunity (Name='Test1',Opportunit_mere__c=oppMother.Id,CloseDate=Date.newInstance(1960, 2, 17),RecordTypeId=rts[1].Id,StageName='Clôturée / Gagnée'));
        opps.add(new Opportunity (Name='Test1',Opportunit_mere__c=oppMother.Id,CloseDate=Date.newInstance(1960, 2, 17),RecordTypeId=rts[2].Id,StageName='Clôturée / Gagnée'));
        opps.add(new Opportunity (Name='Test1',Opportunit_mere__c=oppMother.Id,CloseDate=Date.newInstance(1960, 2, 17),RecordTypeId=rts[3].Id,StageName='Clôturée / Gagnée'));
        opps.add(new Opportunity (Name='Test1',Opportunit_mere__c=oppMother.Id,CloseDate=Date.newInstance(1960, 2, 17),RecordTypeId=rts[4].Id,StageName='Clôturée / Gagnée'));
        opps.add(new Opportunity (Name='Test1',Opportunit_mere__c=oppMother.Id,CloseDate=Date.newInstance(1960, 2, 17),RecordTypeId=rts[5].Id,StageName='Clôturée / Gagnée'));
        insert opps;
        OpportunityTriggerUtil.manageOpportunityRecordType(opps,rts);
      //  for(Integer i=0;i<5;i++)
      //  system.assertEquals(rts[i+5].Id, opps[i].RecordTypeId);
    }

    
    
    
}