public with sharing class OPP_ChoosePriceBook_Ext {
private List<Opportunity> opps;
private id OppId;
private id QuoteId ;
public string selectedPricebook {get; set;}
public OPP_ChoosePriceBook_Ext() {	
	opps = new List<Opportunity>();
	OppId = ApexPages.currentPage().getParameters().Get('OppId');
	QuoteId = ApexPages.currentPage().getParameters().Get('QuoteId');

	if(oppId != null){
			opps = [select Name, pricebook2Id from opportunity where id =: OppId];
	}else if(QuoteId != null){
		OppId = [SELECT Id,OpportunityId FROM QUOTE WHERE Id = :QuoteId ].OpportunityId;		
		if(OppId != null){
			opps = [select Name, pricebook2Id from opportunity where id =: OppId];
		}		
	}

if(opps.size() > 0){
	//opps = [select Id,name,pricebook2Id from opportunity where id =: OppId Limit 1];
	List<Pricebook2> pricebooks = [SELECT Id, Name from Pricebook2 where IsActive = true];

	if(pricebooks.size() > 0 && opps[0].Pricebook2id != null) {
		selectedPricebook = opps[0].Pricebook2id;
	}
}
	
}


public PageReference savePriceBook(){
	//Check if Opportunity is synced with a quote
	//List<Quote> listQuotes = [SELECT Id, Name FROM QUOTE WHERE OpportunityId = :oppId];	

	Opportunity opp = [Select (select id, name from Quotes),(select id, name from OpportunityLineItems) From Opportunity WHERE Id = :oppId ];
	/*if(opp.Quotes.size() > 0) {
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Vous ne pouvez pas modifier le catalogue de prix pour une opportunité contenant des produits d´opportunité si  l\'opportunité est synchronisée avec un devis. Vous devez d \'abord supprimer les devis.'));
	}else{ */
		if(selectedPricebook != null && opp.OpportunityLineItems.size() > 0) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Vous ne pouvez pas modifier le catalogue de prix pour une opportunité contenant des produits d´opportunité si  l\'opportunité est synchronisée avec un devis. Vous devez d \'abord supprimer les produits d\'opportunité.'));
		}else if(selectedPricebook == null && opp.OpportunityLineItems.size() > 0){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Vous ne pouvez pas supprimer le catalogue de prix pour une opportunité contenant des produits d´opportunité si  l\'opportunité est synchronisée avec un devis. Vous devez d \'abord supprimer les produits d\'opportunité.'));
		}		
		else{
			opps[0].Pricebook2id=selectedPricebook;
			update opps[0];
			if(selectedPricebook != null) {
				PageReference pageRef3 = new PageReference('/apex/OLI_ProductSearch?OppId='+oppId+'&OppName='+opps[0].Name);
				pageRef3.setRedirect(false);
				return pageRef3;
			}else{
				return new PageReference('/'+OppId);
			}
		}
	//}

	return null;
}

public PageReference cancelProcess(){
	return new PageReference('/'+OppId);
}

/**********/

public static List<SelectOption> getPricebookOptions(){

	Pricebook2[] Records = [SELECT Name, IsStandard FROM Pricebook2 WHERE IsActive = true AND IsStandard != true ORDER BY Name ASC];

	SelectOption[] Options = new SelectOption[] {};

	Set<Id> pricebookIDs = new set<Id>();
	for(Pricebook2 R : Records) {
		pricebookIDs.add(R.Id);
	}

	List<UserRecordAccess> listUserRecordAccess = [SELECT RecordId, HasReadAccess, HasEditAccess, HasDeleteAccess FROM UserRecordAccess WHERE UserId=:UserInfo.getUserId() AND RecordId IN: pricebookIDs];

	map<Id,boolean> accessMap = new map<Id,boolean>();

	for(UserRecordAccess recAccess : listUserRecordAccess) {
		accessMap.put(recAccess.RecordId, recAccess.HasReadAccess);
	}


	Options.add(new selectOption( '', '--None--'));
	for(Pricebook2 R : Records) {
		if(accessMap.get(R.Id)) {
			Options.add(new SelectOption(R.Id, R.Name));
		}
	}

	if(Options.size() == 1) {

		return null;
	}
	else {
		return Options;
	}


}

}