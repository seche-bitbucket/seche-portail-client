@isTest
public with sharing class DeleteContactInvitationsBatchTest {
    

    @isTest
    public static void testBatch() {

        Contacts_Invitation__c conInv = new Contacts_Invitation__c(Email__c = 'test@testCap.fr', TECH_ToDelete__c = true);
        insert conInv;

        String sch = '0 0 23 * * ?';
        ScheduleDeleteContactInvitationsBatch schedule = new ScheduleDeleteContactInvitationsBatch();
        Id jobId = System.schedule('Test Scheduled Batch', sch, schedule);

        DeleteContactInvitationsBatch batch = new DeleteContactInvitationsBatch();
        Integer batchSize = 200;
        Database.executeBatch(batch, batchSize);
    }

    

}