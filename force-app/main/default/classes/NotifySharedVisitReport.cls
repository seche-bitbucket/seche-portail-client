global class NotifySharedVisitReport implements Database.Batchable<sObject>{
    
    global NotifySharedVisitReport(){        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        //return Database.getQueryLocator('SELECT ParentId, Id, UserOrGroup.Type, Parent.Account__r.Name, UserOrGroupId, AccessLevel, RowCause, LastModifiedDate, LastModifiedById, IsDeleted FROM VisitReport__Share Where LastModifiedDate = TODAY AND HOUR_IN_DAY(LastModifiedDate) > 1');
        return Database.getQueryLocator('SELECT ParentId, Id, UserOrGroup.Type, Parent.Account__r.Name, Parent.CreatedByID, Parent.CreatedDate, UserOrGroupId, AccessLevel, RowCause, LastModifiedDate, LastModifiedById, IsDeleted FROM VisitReport__Share Where LastModifiedDate = TODAY AND HOUR_IN_DAY(LastModifiedDate) > 1');
    }
    
    global void execute(Database.BatchableContext BC, List<VisitReport__Share> scope){
        List<FeedItem> listeFI = new List<FeedItem>();
        List<Task> listeTask = new List<Task>();
        for(VisitReport__Share vr : scope)
        {
            if(vr.LastModifiedDate != vr.Parent.createdDate && vr.UserOrGroupId != vr.Parent.CreatedByID){
                if (vr.UserOrGroup.Type.equals('User'))
                {
                    FeedItem fi = new FeedItem();
                    fi.Title = 'Compte rendu partagé';
                    fi.Body = 'Un compte rendu de '+ vr.Parent.Account__r.Name +' vous a été partagé';
                    fi.ParentId = vr.ParentId;
                    fi.Status = 'Published';
                    fi.CreatedById = vr.UserOrGroupId;
                    listeFI.add(fi);
                    
                    Task t = new Task();
                    t.Subject = 'Autre';
                    t.Status = 'Non démarrée';
                    t.Priority = 'Normal';
                    t.Description = 'Un compte rendu de ' + vr.Parent.Account__r.Name + ' vous a été partagé et doit être consulté';
                    t.OwnerId = vr.UserOrGroupId;
                    t.WhatId = vr.ParentId;
                    listeTask.add(t);
                }
                else
                {
                    // Public Groups
                    List<GroupMember> groupe = [Select Id, UserOrGroupId From GroupMember Where GroupId =: vr.UserOrGroupId];
                    for (GroupMember gm : groupe)
                    {
                        FeedItem fi = new FeedItem();
                        fi.Title = 'Compte rendu partagé';
                        fi.Body = 'Un compte rendu de ' + vr.Parent.Account__r.Name + ' vous a été partagé';
                        fi.ParentId = vr.ParentId;
                        fi.Status = 'Published';
                        fi.CreatedById = gm.UserOrGroupId;
                        listeFI.add(fi);
                        
                        Task t = new Task();
                        t.Subject = 'Partage de CR';
                        t.Status = 'Non démarrée';
                        t.Priority = 'Normal';
                        t.Description = 'Un compte rendu de ' + vr.Parent.Account__r.Name + ' vous a été partagé et doit être consulté';
                        t.OwnerId = gm.UserOrGroupId;
                        t.WhatId = vr.ParentId;
                        listeTask.add(t);
                    }
                }
            }
        }
        insert listeFI;
        insert listeTask;
    }
    
    global void finish(Database.BatchableContext BC){
    }
}