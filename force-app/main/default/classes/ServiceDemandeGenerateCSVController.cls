public class ServiceDemandeGenerateCSVController {
    
    
    ApexPages.StandardSetController stdSetController;
    public static String CSV {get;set;}
    public static String fileName {get;set;}
    private static String separator = ';';
    private static String returnLine = '\n';
    
    private static List <WST_Waste_Statement__c> wsts = New List <WST_Waste_Statement__c>();
    private static List <SD_ServiceDemand__c> sdLists = New List <SD_ServiceDemand__c>();
    private static list <QuoteLine__c> qls= New List <QuoteLine__c>();    
    private static List<RecordType> rt=New List <RecordType>();
    private static RecordType rtTriadisDemand = new RecordType();
    private static SD_ServiceDemand__c sdRTCheck = new SD_ServiceDemand__c();
    private static Map<ID,QuoteLineItem> qlisMap = new Map<ID,QuoteLineItem>();
    
    public ServiceDemandeGenerateCSVController(ApexPages.StandardSetController controller) {  
        
        rtTriadisDemand =[SELECT Id FROM RecordType WHERE DeveloperName = 'Triadisdemand' LIMIT 1];
        
        //Init Controller
        stdSetController = controller;
        //Query Recortype to check wether it's TRIADIS Demand or not
        sdRTCheck = (SD_ServiceDemand__c) stdSetController.getSelected().Get(0);
        sdLists = stdSetController.getSelected();
        
        //If it's a TRANSFO Record Type
        if(sdRTCheck.RecordTypeId != rtTriadisDemand.Id){
            
            wsts = [Select ID, ServiceDemande__c, TransfoFromRehab__c, OilWeight__c, OrderNumber__c, SampleNumber__c,
                    ServiceDemande__r.Name,
                    ServiceDemande__r.ServiceTypeCode__c, ServiceDemande__r.PriceBook__r.Name,
                    ServiceDemande__r.ResponsibleName__r.Name,
                    ServiceDemande__r.LocalContact__c,
                    ServiceDemande__r.BilledAccount__r.TECH_X3SECHEID__c,
                    ServiceDemande__r.BilledAccount__r.X3AddressCode__c,
                    ServiceDemande__r.BilledAccount__r.ChargedX3Code__c,
                    ServiceDemande__r.BilledAccount__r.X3ChargedAddressCode__c,
                    ServiceDemande__r.ApplicantCenter__r.TECH_X3SECHEID__c,
                    ServiceDemande__r.ApplicantCenter__r.X3AddressCode__c,
                    ServiceDemande__r.ApplicantCenter__r.Owner.trigramme__c,
                    ServiceDemande__r.ApplicantCenter__r.Owner.Assistant__r.trigramme__c,
                    ServiceDemande__r.ShippingCity__c, 
                    ServiceDemande__r.ShippingPostalCode__c,
                    ServiceDemande__r.ShippingStreet__c,
                    ServiceDemande__r.OrderNumber__c,
                    Opportunity__r.SyncedQuoteID,
                    Brand__c, Removed__c, KVA__c, Year__c, Name,
                    TotalWeight__c, DielectricWeight__c, MetallicWeight__c, 
                    LaboratoryRecordNumber__c,  PCBContent__c,
                    PCBAnalyzeResult__c, ServiceDemande__r.FavoriteRemovalDate__c, ServiceDemande__r.ConfirmedRemovalDate__c
                    FROM WST_Waste_Statement__c
                    WHERE   ServiceDemande__c IN:stdSetController.getSelected() AND ServiceDemande__r.isExported__c = false  ORDER BY ServiceDemande__r.Name, Removed__c, KVA__c, PCBAnalyzeResult__c ASC];
            
            try{
                List<QuoteLineItem> qlisTransport = [Select ID, QuoteID, Quote.CodeDevis__c, Product2.X3WasteCode__c FROM QuoteLineItem WHERE Quote.Opportunity.ServiceDemand__c IN:stdSetController.getSelected() AND (Product2.X3WasteCode__c = 'FTR1B' OR Product2.X3WasteCode__c = 'FTR1P' OR Product2.X3WasteCode__c = 'FTR1' OR Product2.X3WasteCode__c = 'FTR1Q' OR Product2.X3WasteCode__c = 'FTR1D')];
                System.debug('qlisTransport : '+qlisTransport);
                generateQLIMap(qlisTransport);
            }catch(Exception e){
                System.debug('Error : '+e);
            }
            
          
                        
            WST_Waste_Statement__c wst = new WST_Waste_Statement__c();
            Account a = new Account();
            
            //If it's a Triadis Demand
        }else if(sdRTCheck.RecordTypeId == rtTriadisDemand.Id){
            
            qls = [Select ID,ServiceDemand__c, 
                   ServiceDemand__r.Name,
                   ServiceDemand__r.CustomerIdentification__c,
                   ServiceDemand__r.ConfirmedRemovalDate__c,
                   ServiceDemand__r.LocalContact__c,
                   ServiceDemand__r.OrderNumber__c,
                   ServiceDemand__r.ApplicantCenter__r.Name,
                   ServiceDemand__r.ApplicantCenter__r.TECH_X3SECHEID__c,
                   ServiceDemand__r.ApplicantCenter__r.BillingPostalCode,
                   //ServiceDemand__r.CreatedBy__r.ContactId__r.AccountId__r.Name,
                   CreatedBy.Contact.Account.Name,
                   label__c,
                   quantity__c,
                   TECH_Code__c,
                   ServiceDemand__r.Exploitation_Center__r.Name
                   
                   
                   FROM QuoteLine__c
                   WHERE ServiceDemand__c IN:stdSetController.getSelected()];
            system.debug('qls '+qls);
            QuoteLine__c ql = new QuoteLine__c();
            Account a = new Account();
        }
        
    }
    
    
    
    public static String generateHeaderTransfo(){
        
        
        String header = 'ID_SF_RCT'+separator+
            'ID_SF_DEMANDE'+separator+
            'SF_DEMANDE'+separator+
            'TYPE_SERVICE'+separator+
            'NUM_CDE'+separator+
            'MARCHE_NATIONAL'+separator+
            'TIERS'+separator+
            'TIERS_CODE_ADRESSE'+separator+
            'TIERS_FAC'+separator+
            'TIERS_FAC_CODE_ADRESSE'+separator+
            'CONTACT_TIERS'+separator+
            'DEMANDEUR'+separator+
            'DEMANDEUR_CODE_ADRESSE'+separator+
            'DEMANDEUR_CONTACT'+separator+
            'RESP_CONTRAT'+separator+
            'GESTIONNAIRE'+separator+
            'REPRESENTANT'+separator+
            'MARQUE'+separator+
            'PUISSANCE'+separator+
            'ANNEE'+separator+
            'NUM_SERIE'+separator+
            'POIDS_PLAQUE'+separator+
            'POIDS_LIQUIDE'+separator+
            'POIDS_LIQUIDE_PLAQUE'+separator+
            'MASSE_METALLIQUE'+separator+
            'NUM_LABO'+separator+
            'TENEUR_PCB'+separator+
            'ENLEVE'+separator+
            'DATE_ENLEVEMENT'+separator+
            'RUE_LIVRAISON'+separator+
            'VILLE_LIVRAISON'+separator+
            'ISSU_REHAB'+separator+
            'NUM_DEVIS'+separator+
            'CODE_TRANS'+separator+
            'FILLER'+returnLine;
        return header;
        
    }
    
    public static String generateContentTransfo(){
        String content = '';
        
        for(WST_Waste_Statement__c wst : wsts){
            
            String intWeek = '0';
            String orderNum = '';
            Decimal totalWeight = 0;
            String quoteCode = '';
            String transportCode = '';
            String AssistantCode = '';
            
            if(wst.ServiceDemande__r.ConfirmedRemovalDate__c != null){
                intWeek = DateTime.newInstanceGmt(Date.newInstance(wst.ServiceDemande__r.ConfirmedRemovalDate__c.year(), 
                                                                   wst.ServiceDemande__r.ConfirmedRemovalDate__c.month(), 
                                                                   wst.ServiceDemande__r.ConfirmedRemovalDate__c.day()).toStartOfWeek(), Time.NewInstance(12,0,0,0)).format('w');
            }
            
            if(wst.ServiceDemande__r.OrderNumber__c != null){
                orderNum = wst.ServiceDemande__r.OrderNumber__c;
            }else{
                orderNum = wst.OrderNumber__c;
            }
            
            if(wst.TotalWeight__c != null){
                totalWeight = wst.TotalWeight__c;
            }else{
                totalWeight = wst.MetallicWeight__c + wst.DielectricWeight__c;
            }
            //GETTING Quote Code & QLI Transport X3 Code
            if(wst.Opportunity__r.SyncedQuoteID != null){
                QuoteLineItem qli = qlisMap.get(wst.Opportunity__r.SyncedQuoteID);
                if(qli != null){
                    System.debug('QLI : '+qli);
                    quoteCode = qli.Quote.CodeDevis__c ;
                    transportCode = qli.Product2.X3WasteCode__c ;
                }else{
                    quoteCode = '' ;
                    transportCode = '' ;
                }
                
            }            
            //Avoid Null Assistant Code
            if( wst.ServiceDemande__r.ApplicantCenter__r.Owner.Assistant__r.trigramme__c != null){
                assistantCode =  wst.ServiceDemande__r.ApplicantCenter__r.Owner.Assistant__r.trigramme__c;
            }
            content += wst.ID +separator+
                wst.ServiceDemande__c +separator+
                wst.ServiceDemande__r.Name +separator+
                wst.ServiceDemande__r.ServiceTypeCode__c +separator+
                orderNum +separator+
                wst.ServiceDemande__r.PriceBook__r.Name +separator+
                wst.ServiceDemande__r.BilledAccount__r.TECH_X3SECHEID__c +separator+
                wst.ServiceDemande__r.BilledAccount__r.X3AddressCode__c +separator+   
                wst.ServiceDemande__r.BilledAccount__r.ChargedX3Code__c +separator+
                wst.ServiceDemande__r.BilledAccount__r.X3ChargedAddressCode__c +separator+
                wst.ServiceDemande__r.LocalContact__c +separator+
                wst.ServiceDemande__r.ApplicantCenter__r.TECH_X3SECHEID__c +separator+
                wst.ServiceDemande__r.ApplicantCenter__r.X3AddressCode__c +separator+  
                wst.ServiceDemande__r.ResponsibleName__r.Name +separator+
                // u.Trigramme__c+separator+
                assistantCode+separator+
                assistantCode+separator+
                wst.ServiceDemande__r.ApplicantCenter__r.Owner.trigramme__c +separator+
                wst.Brand__c +separator+
                wst.KVA__c +separator+
                wst.Year__c +separator+
                wst.Name +separator+
                totalWeight +separator+
                wst.DielectricWeight__c +separator+
                wst.OilWeight__c +separator+
                wst.MetallicWeight__c +separator+
                wst.LaboratoryRecordNumber__c +separator+
                wst.PCBContent__c +separator+
                wst.Removed__c +separator+
                intWeek+separator+
                wst.ServiceDemande__r.ShippingStreet__c +separator+
                wst.ServiceDemande__r.ShippingPostalCode__c + ' ' + wst.ServiceDemande__r.ShippingCity__c +separator+
                wst.TransfoFromRehab__c +separator+
                quoteCode+separator+
                transportCode+separator+
                ';'+returnLine;
            
        }
        return content;
        
    }
    public static String generateHeaderTriadis(){
        
        string header = 'LIGNE_DE_DEVIS'+separator+
            'NUMERO_COMMANDE'+separator+
            'IDENTIFIANT_CLIENT'+separator+
            'RAISON_SOCIALE_CLIENT'+separator+
            'IDENTIFIANT_DECHETERIE'+separator+
            'NOM_DECHETERIE'+separator+
            //'CODE_DECHET'+separator+ //A définir
            'LIBELLE_DECHET'+separator+
            'QUANTITE_COLLECTEE'+separator+
            'CODE_CONDITIONNEMENT'+separator+  
            //'QUANTITE_DEPOSEE'+separator+      //A definir
            //'CODE_SITE_TRAITANT'+separator+   //A definir
            'LIBELLE_SITE_TRAITANT'+separator+
            //'CODE...'+separator+          //A definir
            'DEPARTEMENT'+returnLine;
           //'STATUT'+separator+              //A definir
        
        return header;
        
    }
    
    
    
    
    public static String generateContentTriadis(){
        String content = '';
        string dep ='';
        for(QuoteLine__c ql : qls){
            string cd=ql.ServiceDemand__r.ApplicantCenter__r.BillingPostalCode;
            if(cd!=null){
                dep='Département '+cd.substring(0,2);
                
            }
            
            content+=ql.id+separator+
                ql.ServiceDemand__r.OrderNumber__c+separator+
                ql.ServiceDemand__r.CustomerIdentification__c+separator+
                ql.ServiceDemand__r.ApplicantCenter__r.Name+separator+
                ql.ServiceDemand__r.ApplicantCenter__r.TECH_X3SECHEID__c+separator+
                ql.ServiceDemand__r.LocalContact__c+separator+
                //code déchet
                ql.label__c+separator+
                ql.quantity__c+separator+
                ql.TECH_Code__c+separator+
                //quantite deposee
                //code site traitant
                ql.ServiceDemand__r.Exploitation_Center__r.Name+separator+
                //code..
                dep+returnLine;
            //statut

            
        }
        System.debug('contentTriadis'+ content);
        return content;
        
    }
    
    public static void setToExportesSDs(List <SD_ServiceDemand__c> sdLists){
        List <SD_ServiceDemand__c> sdsToUpdate = New List <SD_ServiceDemand__c>();
        System.debug('sdLists '+sdLists);
        if(sdLists.size() > 0){
            for(SD_ServiceDemand__c sd : sdLists){
                SD_ServiceDemand__c sdNew = new SD_ServiceDemand__c(Id = sd.ID, IsExported__c = true);
                sdsToUpdate.add(sdNew);
            }
        }
        try{
            update sdsToUpdate;
        }catch(Exception e){
            System.debug('Error : '+e.getMessage());
        }
        
        
        
    }
    
    public static void generateQLIMap(List<QuoteLineItem> qlis){
        
        if(qlis.size()>0){
            for(QuoteLineItem qli : qlis){
                qlisMap.put(qli.QuoteId, qli);
            }
        }
        System.debug('Map : '+qlisMap);
    }
    
    public static void generateCSV(){
        
        if(sdRTCheck.RecordTypeId!= rtTriadisDemand.Id){
            String header = generateHeaderTransfo();
            String content = generateContentTransfo();
            CSV = header + content;
            
            //Update All SD Exported
            setToExportesSDs(sdLists);
            
        }else if(sdRTCheck.RecordTypeId== rtTriadisDemand.Id){
            String header = generateHeaderTriadis();
            String content = generateContentTriadis();
            CSV = header + content;
        }
    }
}