@isTest
public class AttachmentTriggerUtilTest {
    
    @isTest 
    static void testDeleteCase() {
        
        String result = '';
        
        RecordType r = [Select Id, name FROM Recordtype WHERE SObjectType = 'Quote' AND Name = 'Devis non approuvé CongaEmail'];
        
        Opportunity opp = TestDataFactory.createOpportunity('Opportunity Test');
        insert opp;
        Quote q = TestDataFactory.createQuote(opp);
        q.Status = 'Approuvé';
        q.RecordTypeId = r.ID;
        
        insert q;
        
        Blob bloby = Blob.valueOf('IAZJDSQJ NDOQD09 A21DSQ');
        
        Attachment a = new Attachment(Name='Attachmnet Test', Body=bloby, ParentID = q.ID);
        
        insert a;
        
        Test.startTest();
        try{
            delete a;
        }catch(Exception e){
           result = e.getMessage();
            System.debug('result '+result);
            
        }
        System.assertEquals(false, result.contains('first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Vous ne pouvez pas supprimer un devis qui a été validé:'));
        
        Test.stopTest();
        
    }
    
    @isTest 
    static void testNotDeleteCase() {
        
        String result = '';
        
        RecordType r = [Select Id, name FROM Recordtype WHERE SObjectType = 'Quote' AND Name != 'Devis non approuvé CongaEmail' LIMIT 1];
        
        Opportunity opp = TestDataFactory.createOpportunity('Opportunity Test');
        insert opp;
        Quote q = TestDataFactory.createQuote(opp);
        q.Status = 'Présenté';
        q.RecordTypeId = r.ID;
        
        insert q;
        
        Blob bloby = Blob.valueOf('IAZJDSQJ NDOQD09 A21DSQ');
        
        Attachment a = new Attachment(Name='Attachmnet Test', Body=bloby, ParentID = q.ID);
        
        insert a;
        
        Test.startTest();
            delete a;
       
        System.assertNotEquals(null, a.ID);
        
        Test.stopTest();
        
    }
    
}