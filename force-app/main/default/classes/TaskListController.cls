public without sharing class TaskListController {

    private integer counter=0;  //keeps track of the offset
    private integer list_size=20; //sets the page size or number of rows
    public integer total_size; //used to show user the total size of the list

    public TaskListController() {
        total_size = [select count() from Task where CreatedById =: UserInfo.getUserId() and ownerId !=:UserInfo.getUserId()]; //set the total size in the constructor
    }
    
    public Task[] getTasks() {

        Task[] taskList = [select Id,Subject, Status, Description, Who.Name,What.Name,Account.Name,Owner.name,ActivityDate
                           from Task 
                           where status = 'En cours' and CreatedById =: UserInfo.getUserId() and ownerId !=:UserInfo.getUserId()
                           order by ActivityDate
                           limit :list_size ];                   
        return taskList;
          
    }
}