/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Ramatoulaye GUEYE  <ramatoulaye.gueye@capgemini.com>
* @modifiedBy     Ramatoulaye GUEYE  <ramatoulaye.gueye@capgemini.com>
* @maintainedBy   Ramatoulaye GUEYE  <ramatoulaye.gueye@capgemini.com>
* @version        1.0
* @created        2018-03-02
* @modified       
     
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class UploadData {
    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    public String[] fileLines = new String[]{};
        public List<QuoteLine__c> quoteLineToUpload;
    public  List<SD_ServiceDemand__c> dstoupload;
    public List<SET_Settings__c> objs;
    public List<CAP__c> caps;
    SD_ServiceDemand__c e = new SD_ServiceDemand__c();
    
    
    /***This function reads the CSV file and inserts records into the custum object. ***/
    public Pagereference ReadFile()
    {
       system.debug('ReadFile START ');

        //try{
        //Convert the uploaded file which is in BLOB format into a string
        nameFile =blobToString( contentFile,'ISO-8859-1');
        //Now sepatate every row of the excel file
        fileLines = nameFile.split('\n');  
        system.debug('fileLines  '+fileLines);
        //Iterate through every line and create an Entete record for each row
        dstoupload= new List<SD_ServiceDemand__c>();
        String[] values = new String[]{};
            String[] val= new String[]{};
                
                // Assign the user.contact.account to the field exploitation_center   
                Id userId=system.userInfo.getUserId();
        system.debug('userID'+userId);
        List<Contact> conts = new List<Contact>();
        conts = [select Id,name, accountId from contact where id IN (select ContactId from user where id=:userId)];
        system.debug('contacts.size()'+conts.size());
        
        if(conts.size()>0){
            for(Contact cont : conts){
                system.debug('contact.Name'+cont.Name);
                e.Exploitation_Center__c=cont.accountId;
                system.debug('centre de traitement'+e.Exploitation_Center__c);
            }  
        }

     

        values = fileLines[2].split(';');
        e.LocalContact__c=values[1];
        system.debug('Service'+e.LocalContact__c);
        e.CustomerIdentification__c=values[11];
        system.debug('idclient'+values[11]);
        
        values = fileLines[3].split(';');
        system.debug('adress'+values[1]);
        e.RemovalStreet__c=values[1];
        system.debug('adress'+e.RemovalStreet__c);
        
       //Adress treatement///
        system.debug('values ='+values);
        //system.debug('adressSplit.size() test 1 '+adressSplit.size());
        string respName= values[12];
        system.debug('Resp Name'+values[12]);
        
        List<Contact> contacts = new List<Contact>();
        if(respname!=''){
            contacts = [SELECT Id, Name, FirstName, Email, AccountId FROM contact WHERE Name = :respname limit 1];
            if(contacts.size()!=0){
            e.ResponsibleName__c = contacts[0].Id;
        	}
        }
        system.debug('responsiblename'+e.ResponsibleName__c);
        values = filelines[4].split(';');
        system.debug('values___'+values[1]+'__');
        date dte= date.parse(values[1]);
        system.debug('date'+dte);
        system.debug('date too '+date.parse(values[1]).format());
        datetime now=System.now();
        e.ConfirmedRemovalDate__c=dte;
        system.debug('e.ConfirmedRemovalDate__c'+e.ConfirmedRemovalDate__c);
        
        
        
        values = filelines[0].split(';');
        system.debug('values'+values);
        system.debug('values'+values.size());
        system.debug('Market number'+values[10]);
        e.MarketNumber__c=values[10];
        system.debug('values[1]'+values[1]);
        String accName=values[1];
        system.debug('compte'+accName);
        List<Account> accountsA =[SELECT Name FROM Account WHERE Name = :accName Limit 1];
        string streetB;
        
        List<Account> accountsB =new List<Account>();
        system.debug('e.RemovalStreet__c'+e.RemovalStreet__c);
        
        if (e.RemovalStreet__c!=null){
            streetB=e.RemovalStreet__c;
            streetB=streetB.trim();
            system.debug('streetTest '+streetB);
            accountsB =[SELECT BillingStreet, BillingPostalCode,BillingCity FROM Account WHERE BillingStreet=:streetB limit 1];
            system.debug('taille liste account'+accountsA.size());
            system.debug('taille liste accountB'+accountsB.size());
            // Boolean accountIdFound = false;
            if(accountsB.size()>0){
                for(Account acntB : accountsB){
                    
                    if(streetB==acntB.BillingStreet ){
                        system.debug('id apcenter '+ acntB.Id);
                        e.ApplicantCenter__c = acntB.Id;
                        system.debug('e.ApplicantCenter__c '+acntB.Id);
                    }
                }
            }
        }
        
        if(accountsA.size()>0){
            for(Account acntA : accountsA){
                if(accName == acntA.Name){
                    e.ApplicantCenter__c = acntA.Id;
                    //accountIdFound=true;
                }
            }
        } 
        
        if( accountsA.size()==0 && accountsB.size()==0 &&contacts.size()>0 ){
            e.ApplicantCenter__c = contacts[0].AccountId;
        }                  
        
        
        
        List<RecordType> recordTypes =[SELECT Id FROM RecordType WHERE DeveloperName = 'Triadisdemand' LIMIT 1];
        e.RecordtypeId= recordTypes.get(0).Id;
        String lineEtiq= 'Information de collecte';
        String[] infcolLine = new String[]{};
            for (Integer nI=0;nI<fileLines.size();nI++) { 
                Boolean result1=fileLines[nI].contains(lineEtiq);
                if ( result1==true){
                    infcolLine=fileLines[nI].split(';');
                    e.RemovalInformation__c=infcolLine[1]; 
                }
            }
      
        insert e;
        
        String lineLim0= 'ENLEVEMENT DES DECHETS CHIMIQUES';
        Integer lim0;
        string delim0= '';
        Integer contt0=0;
        for (Integer b=0;b<fileLines.size();b++) { 
            Boolean result=fileLines[b].contains(lineLim0);
            if ( result==true){
                lim0=b;
            }
        }
        
        String lineLim= 'COMMANDES EMBALLAGES / ETIQUETTES';
        Integer lim=0;
        string delim = '';
        Integer cont0=0;
        for (Integer s=0;s<fileLines.size();s++) { 
            Boolean result=fileLines[s].contains(lineLim);
            if ( result==true){
                lim=s;
            }
        }
       
        ///////////////////////////////////////////CAP ///////////////////////////////////////////////////
        caps=[SELECT Id, Name, Account__c, WasteDetailsAndService__c FROM CAP__c where Account__c=:e.ApplicantCenter__c];
        //caps=[SELECT WasteDetailsAndService__c , Name FROM CAP__c where Account__c=:e.ApplicantCenter__c]
        //Map<String, String> capMap = new Map<String , String>([SELECT WasteDetailsAndService__c , Name,Account__c FROM CAP__c where Account__c=:e.ApplicantCenter__c]);
        Map<String, String> capMap = new Map<String , String>();
        system.debug('les caps '+caps);
        for(Integer i=0;i<caps.size();i++){
            capMap.put(caps[i].WasteDetailsAndService__c,caps[i].Name);
           // System.debug('map de cap'+capMap);
            
        }
        //_ql:e2____________________________________________________________________________________________________________________
        objs=[select Object__c, Code__c, Category__c, ExternalID__c from SET_Settings__c where Category__c='Conditionnement'];
        //system.debug('conditionnements'+objs);
        //system.debug('cond'+objs.get(0).Object__c);
        integer lqt=lim0+1;
       
        integer lqt1=lim0+2;
         system.debug('lqt ='+ lqt);
        system.debug('lqt1 ='+lqt1);
        quoteLineToUpload = new List<QuoteLine__c>();
        for (Integer i=lqt1;i<lim;i++)  //for lines
        {    
            values = filelines[i].split(';');   // lines
            val= fileLines[lqt].split(';');       // collums
            
            system.debug('values '+values);
            system.debug('values.size() '+values.size());
            system.debug('val ='+val);
             system.debug('val.size() ='+val.size());
            for (integer k=1;k<val.size()-1;k++){
                System.debug('valeur de k'+values[k]);
                 //System.debug('valeur produit '+values[0]);
                if ((values[k]!='') && (values[0] !=null)){
                      if((values[k]=='x') || (values[k]=='X') ){
                        values[k]='1';
					}
                    string quantity;
                    if(values[k].isNumeric()==false){
                        integer vals=values[k].length();
                        for(integer q=0;q<vals;q++){
                            quantity=values[k].substring(0,q);
                            if(quantity.isNumeric()==true){
                               string quantExtract=+quantity;
                                system.debug('quantity');
                            }
                        }
                       values[k]=quantity;
                    }
                  	                    
                    system.debug('les quantités: '+values[k]);
                    QuoteLine__c e2 = new QuoteLine__c();
                    e2.label__c= values[0].trim(); 
                    e2.contenant__c = val[K];
                    String tmp = values[k];
                    e2.ServiceDemand__c=e.id;
                    e2.Applicant_Center__c=e.ApplicantCenter__c;
                    e2.CAP__c=capMap.get(values[0].trim());  //line for CAP
                    tmp = tmp.replace('\r\n', '');
                    tmp = tmp.replace('\n', '');
                    tmp = tmp.replace('\r', '');
                    //system.debug('|'+tmp+'|'); 
                    
                    if(tmp != ''){
                        try{
                            e2.quantity__c = Integer.valueof(tmp);
                            //quoteLineToUpload.add(e2);
                        }catch(Exception ex){
                        }
                    }

                    for(Integer j=0;j<objs.size();j++){
                        if(objs.get(j).Object__c==val[k]){
                            //system.debug('code'+objs.get(j).Code__c);
                            e2.TECH_Code__c=objs.get(j).Code__c;
                            e2.ExternalIDcontionnement__c = objs.get(j).ExternalID__c;
                        } 
                    }
                 quoteLineToUpload.add(e2);

                    
                } 
            }
        }
        //quoteLine:e3_____________________________________________________________________________________________________________________
        integer lim1 = 0;

        integer Li1=Lim+2;
        for (Integer a=Li1;a<fileLines.size();a++) { 
            String[] line = new String[]{};
                line =fileLines[a].split(';');
            system.debug('line size'+line.size());
            system.debug('line'+line);
            //system.debug('line0 '+line[0]);
            //system.debug('line1 '+line[1]);
            
            if (line[0]=='' && line[1]==''){
                //if (line.IsEmpty()){       //for test
                lim1=a;
                a=fileLines.size();  
            }
        }
        integer l=lim+2;
        integer L1=Lim+1;
        integer L2=lim1+1;
        integer cont=cont0+l;
        for( integer f=l; f<L2-1; f++){
            values = fileLines[f].split(';');   //line
            val= fileLines[L1].split(';');
            for (integer k=1;k<val.size()-1;k++){
                if ((values[k]!='') && (values[0] !='')){
                      if((values[k]=='x') || (values[k]=='X') ){
                        values[k]='1';
					} string quantity;
                    if(values[k].isNumeric()==false){
                        integer vals=values[k].length();
                        for(integer q=0;q<vals;q++){
                            quantity=values[k].substring(0,q);
                            if(quantity.isNumeric()==true){
                               string quantExtract=+quantity;
                                system.debug('quantity');
                            }
                        }
                       values[k]=quantity;
                    }
                    QuoteLine__c e3 = new QuoteLine__c();
                    e3.label__c= values[0].trim(); 
                    
                    system.debug('avec trim...'+values[0].trim()+'|');
                    system.debug('sans trim...'+values[0]+'|');
                    e3.contenant__c = val[K];
                    String tmp = values[k];
                    e3.ServiceDemand__c=e.id;
                     e3.Applicant_Center__c=e.ApplicantCenter__c;
                    e3.CAP__c=capMap.get(values[0].trim());
                    tmp = tmp.replace('\r\n', '');
                    tmp = tmp.replace('\n', '');
                    tmp = tmp.replace('\r', '');
                    //system.debug('|'+tmp+'|'); 
                    if(tmp != ''){
                        // try{
                        e3.quantity__c = Integer.valueof(tmp);
                        //system.debug('e3 : '+e3);
                        //}catch(Exception ex){
                        // System.debug('Exception : '+ex);
                        //}
                    }
                    
                    for(Integer j=0;j<objs.size();j++){
                        if(objs.get(j).Object__c==values[0]+val[k]){
                            e3.TECH_Code__c=objs.get(j).Code__c;
                            e3.ExternalIDcontionnement__c = objs.get(j).ExternalID__c;
                        } 
                    }
                    quoteLineToUpload.add(e3);
                }
            }
            cont= cont+1;       
        }
        //Quoteline:e4_____________________________________________________________________________________________________________________
        integer cont1=cont+2;
        integer lim2 = 0;
        for (Integer a=cont1;a<fileLines.size();a++) { 
            String[] line = new String[]{};
                line =fileLines[a].split(';');
            if ( line[0]=='' && line[1]==''){
                //if ( line.IsEmpty()){        //for test
                lim2=a;
                a=fileLines.size();
            }
        }
        for( integer st=cont; st<lim2; st++){
            values = fileLines[st].split(';'); 
            for (integer k=1;k<values.size()-1;k++){
                if (values[k]!=''){
                      if((values[k]=='x') || (values[k]=='X') ){
                        values[k]='1';
					}
                     string quantity;
                    if(values[k].isNumeric()==false){
                        integer vals=values[k].length();
                        for(integer q=0;q<vals;q++){
                            quantity=values[k].substring(0,q);
                            if(quantity.isNumeric()==true){
                               string quantExtract=+quantity;
                                system.debug('quantity');
                            }
                        }
                       values[k]=quantity;
                    }
                    QuoteLine__c e4 = new QuoteLine__c();
                    e4.label__c= values[0]; 
                   
                    e4.ServiceDemand__c=e.id;
                     e4.Applicant_Center__c=e.ApplicantCenter__c;
                    e4.CAP__c=capMap.get(values[0].trim());
                    String tmp = values[k];
                    tmp = tmp.replace('\r\n', '');
                    tmp = tmp.replace('\n', '');
                    tmp = tmp.replace('\r', '');
                    if(tmp != ''){
                        try{
                            e4.quantity__c = Integer.valueof(tmp);
                        }catch(Exception ex){
                            //System.debug('Exception : '+ex);
                        }
                        
                    }
                    
                    for(Integer j=0;j<objs.size();j++){
                        if(objs.get(j).Object__c==values[0]){
                            e4.TECH_Code__c=objs.get(j).Code__c;
                            e4.ExternalIDcontionnement__c = objs.get(j).ExternalID__c;
                        } 
                    }
                    quoteLineToUpload.add(e4);  
                }
            }
        }  
        //e5 etiquettes_____________________________________________________________________________________________________________________
        String lineLimEtiq= 'Information de collecte';
        Integer limEtiq;
        for (Integer s=lim2;s<fileLines.size();s++) { 
            Boolean result=fileLines[s].contains(lineLimEtiq);
            if ( result==true){
                limEtiq=s;
            }
        }
        integer startEtiq=lim2+1;
        for(integer h=startEtiq;h<limEtiq;h++){
            values = fileLines[h].split(';'); 
            integer stop= values.size()-3;
            for (integer k=-1;k<stop;k++){
                integer step1=k+2;
                integer step2=step1+1;
                if (values[step2]!='' && math.mod(k, 2)!=0){
                    QuoteLine__c e5 = new QuoteLine__c();
                    e5.label__c= values[step1].trim(); 
                    e5.ServiceDemand__c=e.id;
                     e5.Applicant_Center__c=e.ApplicantCenter__c;
                    String tmp = values[step2];
                    tmp = tmp.replace('\r\n', '');
                    tmp = tmp.replace('\n', '');
                    tmp = tmp.replace('\r', '');
                    if(tmp != ''){
                        try{
                            e5.quantity__c = Integer.valueof(tmp);
                            quoteLineToUpload.add(e5);
                        }catch(Exception ex){
                            
                        }
                        
                    }
                } 
            }
        }
        insert quoteLineToUpload;    
        //}
        /*catch(Exception e){
ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured reading the CSV file'+e.getMessage());
System.debug('Error : '+errormsg);
}    */   
        return null;
    }
    /**
This function convers the input CSV file in BLOB format into a string
@param input    Blob data representing correct string in @inCharset encoding
@param inCharset    encoding of the Blob data (for example 'ISO 8859-1')
*/
    public static String blobToString(Blob input, String inCharset){
        String hex = EncodingUtil.convertToHex(input);
        System.assertEquals(0, hex.length() & 1);
        final Integer bytesCount = hex.length() >> 1;
        String[] bytes = new String[bytesCount];
        for(Integer i = 0; i < bytesCount; ++i)
            bytes[i] =  hex.mid(i << 1, 2);
        return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
    }     
    /**** This function sends back to the visualforce page the DS that were inserted ****/ 
    public QuoteLine__c getuploadedQuoteLine()
    {
        if (quoteLineToUpload!= NULL){
            if (quoteLineToUpload.size() > 0){
                
                return quoteLineToUpload.get(0);
            }
            else{
                return null;  
            } 
        }                
        else{
            return null;
        }
    }
    
    
}