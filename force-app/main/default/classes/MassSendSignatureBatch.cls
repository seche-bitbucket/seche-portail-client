global class MassSendSignatureBatch  implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {	
        String query = 'SELECT ID,Name From Quote Where TECH_MassSignature__c=true';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Quote> scope)
    {
        List<Quote> quotes=[SELECT id, Name,ContactId,Opportunity.Salesman__c,Opportunity.Owner.Email,
                            Opportunity.Owner.Name,Email, QuoteCode__c,TECH_UID__c,Opportunity.Filiale__c,Account.Name,TECH_Assistante__c FROM Quote WHERE Id IN:scope];
        RecordType rt=[SELECT Id, Name, DeveloperName, Description FROM RecordType Where DeveloperName='Devis_approuv_Read_Only' LIMIT 1];
        List<Attachment> atts=[SELECT id, Name, body, ContentType,ParentId FROM Attachment WHERE ParentId IN:quotes];
        Map<Id,Id> ContactsID=new Map<Id,Id>();
        Map<Id,Id> SalesmanID=new Map<Id,Id>();
        Map<Id,Id> AssistantesID=new Map<Id,Id>();
        
        for(Quote q:Quotes){
            ContactsID.put(q.Id,q.ContactId);
            SalesmanID.put(q.Id,q.Opportunity.Salesman__c);
            AssistantesID.put(q.Id,q.TECH_Assistante__c);
        }
        List<Contact> contacts=[SELECT ID,Name,FirstName,LastName,Email,Phone From Contact Where Id IN:ContactsID.Values()];
        List<User> Assistantes=[SELECT Id, Name,email FROM User WHERE Id IN:AssistantesID.Values()];
        Integer NextYear = System.today().year()+1;
        List<User> Users=[SELECT Id, Name,Signature,email FROM User WHERE Id IN:SalesmanID.Values()];
        for(Quote q:Quotes){
            User u=new User();
            User assistante=new User();
            Contact contact=new Contact();
            attachment attTosend=new Attachment();
            q.Tech_UID__c=GuidUtil.NewGuid();
            q.TECH_isApprouved__c=true;
            q.Status='Envoyé pour signature';
            q.RecordTypeId=rt.Id;
            q.TECH_MassSignature__c=false;
            update q;
            for(User salesman:Users){
                if(SalesmanID.get(q.id)==salesman.id){
                    u=salesman;
                    break;
                }
            }
            for(User ass:Assistantes){
                if(AssistantesID.get(q.id)==ass.id){
                    assistante=ass;
                    break;
                }
            }
            for(Contact con:contacts){
                if(ContactsID.get(q.id)==con.id){
                    contact=con;
                    break;
                }
            }
            for(Attachment att:atts){
                if(q.Id==att.ParentId){
                    attTosend=att;
                }
            }
            String userSignature ='<pre>'+u.Signature+'</pre>';
            String userName=u.Name;
            if(q.Name.Contains('Convention')){
                String contactGreeting ='Cher client,<br><br>';
                String CommercialText= u.Name+' vous prie de trouver ci-joint votre convention '+NextYear+'. ';
                String ConnectiveText='<br><br>'+q.Opportunity.Filiale__c+' vous invite, via Connective, à signer votre convention en cliquant sur le lien ci-dessous :<br>';
                String link='<a href="'+label.CommunityORGURL+'/SignatureDevis/s/sendsignatureform?UID='+q.TECH_UID__c+'&ID='+q.Id+'">';
                String LinkBody='Portail Signature de la Convention '+NextYear+' - '+q.QuoteCode__c+' </a>'+'<br>';
                String secheConnect='<br>Ce lien vous redirige sur un formulaire Séché Connect pré-rempli avec vos éléments, si toutefois vous n\'êtes pas le signataire de la convention vous pouvez remplacer vos coordonnées par celles dudit signataire.<br><br> Une fois ce formulaire rempli, vous recevrez un mail de la part de "no-reply@connective.eu", service de signature électronique sécurisée choisi par Séché Environnement dans le cadre de son programme de transformation digitale.';
                String greeting= '<br><br>Cordialement,'; 
                String notContact='<br><br><i> Séché Environnement collecte et traite des données à caractère personnel dans le cadre de la gestion des contrats et des obligations légales et réglementaires s’y rattachant. Elles sont uniquement destinées à nos services internes et à nos filiales habilitées. Conformément au Règlement Général sur la Protection des Données, le Groupe met à disposition des droits d’accès, de rectification et d’opposition sur les données à caractère personnel. Il est possible d\'exercer ces droits en contactant notre Délégué à la Protection des Données par courrier électronique adressé à <a href="dp@groupe-seche.com">dp@groupe-seche.com</a> , qui traitera votre demande. Pour plus de détails sur l’engagement du Groupe :  <a href="https://www.groupe-seche.com/fr/mentions-legales">RGPD</a>';
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new String[] { q.Email };
                    message.ccaddresses=new String[] { assistante.Email };
                        message.bccaddresses=new String[] {u.Email};
                            message.subject = 'Convention '+NextYear+' - '+q.Opportunity.Filiale__c+' - '+q.Account.Name;
                message.setReplyTo(u.Email);
                message.setSenderDisplayName(u.Name);
                message.htmlBody = contactGreeting+CommercialText+ConnectiveText+link+LinkBody+secheConnect+greeting+userSignature + notContact;
                Messaging.SingleEmailMessage[] EmailsToSend = new Messaging.SingleEmailMessage[] { message };
                    // Add to attachment file list
                    Messaging.Emailfileattachment fileAttachment = new Messaging.Emailfileattachment();
                fileAttachment.setFileName(attTosend.Name);
                fileAttachment.setBody(attTosend.Body);
                message.setFileAttachments(new Messaging.Emailfileattachment[] { fileAttachment });
                Messaging.sendEmail(EmailsToSend);
            }else{
                String contactGreeting ='Cher client,<br><br>';
                String CommercialText= u.Name+' vous prie de trouver ci-joint votre devis '+NextYear+'. ';
                String ConnectiveText='<br><br>'+q.Opportunity.Filiale__c+' vous invite, via Connective, à signer votre devis en cliquant sur le lien ci-dessous :<br>';
                String link='<a href="'+label.CommunityORGURL+'/SignatureDevis/s/sendsignatureform?UID='+q.TECH_UID__c+'&ID='+q.Id+'">';
                String LinkBody='Portail Signature du devis '+NextYear+' - '+q.QuoteCode__c+' </a>'+'<br>';
                String secheConnect='<br>Ce lien vous redirige sur un formulaire Séché Connect pré-rempli avec vos éléments, si toutefois vous n\'êtes pas le signataire du devis vous pouvez remplacer vos coordonnées par celles dudit signataire.<br><br> Une fois ce formulaire rempli, vous recevrez un mail de la part de "no-reply@connective.eu", service de signature électronique sécurisée choisi par Séché Environnement dans le cadre de son programme de transformation digitale.';
                String greeting= '<br><br>Cordialement,'; 
                String notContact='<br><br><i> Séché Environnement collecte et traite des données à caractère personnel dans le cadre de la gestion des contrats et des obligations légales et réglementaires s’y rattachant. Elles sont uniquement destinées à nos services internes et à nos filiales habilitées. Conformément au Règlement Général sur la Protection des Données, le Groupe met à disposition des droits d’accès, de rectification et d’opposition sur les données à caractère personnel. Il est possible d\'exercer ces droits en contactant notre Délégué à la Protection des Données par courrier électronique adressé à <a href="dp@groupe-seche.com">dp@groupe-seche.com</a> , qui traitera votre demande. Pour plus de détails sur l’engagement du Groupe :  <a href="https://www.groupe-seche.com/fr/mentions-legales">RGPD</a>';
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new String[] { q.Email };
                    message.ccaddresses=new String[] { assistante.Email };
                        message.bccaddresses=new String[] {u.Email};
                            message.subject = 'Devis '+NextYear+' - '+q.Opportunity.Filiale__c+' - '+q.Account.Name;
                message.setReplyTo(u.Email);
                message.setSenderDisplayName(u.Name);
                message.htmlBody = contactGreeting+CommercialText+ConnectiveText+link+LinkBody+secheConnect+greeting+userSignature + notContact;
                Messaging.SingleEmailMessage[] EmailsToSend = new Messaging.SingleEmailMessage[] { message };
                    // Add to attachment file list
                    Messaging.Emailfileattachment fileAttachment = new Messaging.Emailfileattachment();
                fileAttachment.setFileName(attTosend.Name);
                fileAttachment.setBody(attTosend.Body);
                message.setFileAttachments(new Messaging.Emailfileattachment[] { fileAttachment });
                Messaging.sendEmail(EmailsToSend);
            } 
        }
    }
    global void finish(Database.BatchableContext BC)
    {  
    }
}