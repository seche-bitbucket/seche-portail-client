// this class is to be executed once a day to retrieve the status of all the documents sent for signing
global class SignatureStatusRefresh Implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        SignatureStatusMassRetrieve  batch = new SignatureStatusMassRetrieve(); 
        Id batchId = Database.executeBatch(batch,50);        
    }
}