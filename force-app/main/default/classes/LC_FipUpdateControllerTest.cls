@isTest
public with sharing class LC_FipUpdateControllerTest {    
    
static testMethod void setToExploitationTest() {
     
    String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

    //Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;    
	
	System.RunAs(director) {						
	    Account a = new Account();
        a.Name = 'TESTFIPAMIANTE';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Aéronautique';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingStreet = '12 rue du test';
        a.BillingPostalCode = '39000';
        a.BillingCity = 'Lyon';
        a.NAF_Number__c ='1234A';
        a.ChargedX3Code__c = 'FR972139821';
        a.X3ChargedAddressCode__c = 'GEN';
        a.FavoriteSubsidiaryAsbestos__c = 'Tredi Salaise 1';
        a.SIRET_Number__c = '00092183013812';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName1';
        c.AccountId = a.Id;
        c.Email = 'aaaa@groupe-seche.com';
        c.Phone = '03 00 00 00 00';
        insert c;
        
        FIP_FIP__c fip = new FIP_FIP__c();
        system.debug('Collector__c = '+ a.ID);
        fip.Collector__c = a.ID;
        system.debug('Contact__c = '+ c.ID);
        fip.Contact__c = c.ID;
        fip.State__c = 'Refusée'; 
        fip.GenerateFIPPDF__c = true;
        insert fip;
        System.assertEquals('En attente de exploitation', LC_FipUpdateController.setToExploitation(fip).objectFip.State__c);
	}
}

static testMethod void notSetToExploitationTest() {
     
    String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

    //Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;    
	
	System.RunAs(director) {						
	    Account a = new Account();
        a.Name = 'TESTFIPAMIANTE';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Aéronautique';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingStreet = '12 rue du test';
        a.BillingPostalCode = '39000';
        a.BillingCity = 'Lyon';
        a.NAF_Number__c ='1234A';
        a.ChargedX3Code__c = 'FR972139821';
        a.X3ChargedAddressCode__c = 'GEN';
        a.FavoriteSubsidiaryAsbestos__c = 'Tredi Salaise 1';
        a.SIRET_Number__c = '00092183013812';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName1';
        c.AccountId = a.Id;
        c.Email = 'aaaa@groupe-seche.com';
        c.Phone = '03 00 00 00 00';
        insert c;
        
        FIP_FIP__c fip = new FIP_FIP__c();
        system.debug('Collector__c = '+ a.ID);
        fip.Collector__c = a.ID;
        system.debug('Contact__c = '+ c.ID);
        fip.Contact__c = c.ID;
        fip.State__c = 'En attente de exploitation'; 
        fip.GenerateFIPPDF__c = true;
        insert fip;
        System.assertEquals('Vous ne pouvez pas modifier une FIP qui est en attente de l\'exploitation ou approuvée', LC_FipUpdateController.setToExploitation(fip).message);
	}
}

static testMethod void setToSeiTest() {
     
    String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

    //Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;    
	
	System.RunAs(director) {						
	    Account a = new Account();
        a.Name = 'TESTFIPAMIANTE';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Aéronautique';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingStreet = '12 rue du test';
        a.BillingPostalCode = '39000';
        a.BillingCity = 'Lyon';
        a.NAF_Number__c ='1234A';
        a.ChargedX3Code__c = 'FR972139821';
        a.X3ChargedAddressCode__c = 'GEN';
        a.FavoriteSubsidiaryAsbestos__c = 'Tredi Salaise 1';
        a.SIRET_Number__c = '00092183013812';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName1';
        c.AccountId = a.Id;
        c.Email = 'aaaa@groupe-seche.com';
        c.Phone = '03 00 00 00 00';
        insert c;
        
        FIP_FIP__c fip = new FIP_FIP__c();
        system.debug('Collector__c = '+ a.ID);
        fip.Collector__c = a.ID;
        system.debug('Contact__c = '+ c.ID);
        fip.Contact__c = c.ID;
        fip.State__c = 'En attente de qualification'; 
        fip.GenerateFIPPDF__c = true;
        insert fip;
        System.assertEquals('Soumise a SEI', LC_FipUpdateController.setToSei(fip).objectFip.State__c);
	}
}

static testMethod void notSetToSeiTest() {
     
    String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

    //Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;    
	
	System.RunAs(director) {						
	    Account a = new Account();
        a.Name = 'TESTFIPAMIANTE';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Aéronautique';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingStreet = '12 rue du test';
        a.BillingPostalCode = '39000';
        a.BillingCity = 'Lyon';
        a.NAF_Number__c ='1234A';
        a.ChargedX3Code__c = 'FR972139821';
        a.X3ChargedAddressCode__c = 'GEN';
        a.FavoriteSubsidiaryAsbestos__c = 'Tredi Salaise 1';
        a.SIRET_Number__c = '00092183013812';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName1';
        c.AccountId = a.Id;
        c.Email = 'aaaa@groupe-seche.com';
        c.Phone = '03 00 00 00 00';
        insert c;
        
        FIP_FIP__c fip = new FIP_FIP__c();
        system.debug('Collector__c = '+ a.ID);
        fip.Collector__c = a.ID;
        system.debug('Contact__c = '+ c.ID);
        fip.Contact__c = c.ID;
        fip.State__c = 'Soumise a SEI'; 
        fip.GenerateFIPPDF__c = true;
        insert fip;
        System.assertEquals('Vous ne pouvez pas modifier une FIP qui est en attente du collecteur, de l\'exutoire ou de l\'exploitation', LC_FipUpdateController.setToSei(fip).message);
	}
}

static testMethod void testSendFipRemider() {
     
    String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

    //Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;    
	
	System.RunAs(director) {	
        Test.startTest();   
       
	    Account a = new Account();
        a.Name = 'TESTFIPAMIANTE';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Aéronautique';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingStreet = '12 rue du test';
        a.BillingPostalCode = '39000';
        a.BillingCity = 'Lyon';
        a.NAF_Number__c ='1234A';
        a.ChargedX3Code__c = 'FR972139821';
        a.X3ChargedAddressCode__c = 'GEN';
        a.FavoriteSubsidiaryAsbestos__c = 'Tredi Salaise 1';
        a.SIRET_Number__c = '00092183013812';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName1';
        c.AccountId = a.Id;
        c.Email = 'aaaa@groupe-seche.com';
        c.Phone = '03 00 00 00 00';
        insert c;
        
        FIP_FIP__c fip = new FIP_FIP__c();
        system.debug('Collector__c = '+ a.ID);
        fip.Collector__c = a.ID;
        system.debug('Contact__c = '+ c.ID);
        fip.Contact__c = c.ID;
        fip.State__c = 'Soumise a SEI'; 
        fip.GenerateFIPPDF__c = true;
        insert fip;
        Test.setMock(HttpCalloutMock.class, new WS001_ConnectiveMock()); 
        WS001_ConnectiveMock.externalId=fip.id;
        LC_FipUpdateController.sendFipReminder(fip);
        System.assertEquals('Aucun document envoyé pour signature', LC_FipUpdateController.sendFipReminder(fip).message);
	   Test.stopTest();
    }
}

static testMethod void testGetFipStatus() {
     
    String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

    //Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;    
	
	System.RunAs(director) {	
        Test.startTest();        
	    Account a = new Account();
        a.Name = 'TESTFIPAMIANTE';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Aéronautique';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingStreet = '12 rue du test';
        a.BillingPostalCode = '39000';
        a.BillingCity = 'Lyon';
        a.NAF_Number__c ='1234A';
        a.ChargedX3Code__c = 'FR972139821';
        a.X3ChargedAddressCode__c = 'GEN';
        a.FavoriteSubsidiaryAsbestos__c = 'Tredi Salaise 1';
        a.SIRET_Number__c = '00092183013812';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName1';
        c.AccountId = a.Id;
        c.Email = 'aaaa@groupe-seche.com';
        c.Phone = '03 00 00 00 00';
        insert c;
        
        FIP_FIP__c fip = new FIP_FIP__c();
        system.debug('Collector__c = '+ a.ID);
        fip.Collector__c = a.ID;
        system.debug('Contact__c = '+ c.ID);
        fip.Contact__c = c.ID;
        fip.State__c = 'Soumise a SEI'; 
        fip.GenerateFIPPDF__c = true;
        insert fip;
        Test.setMock(HttpCalloutMock.class, new WS001_ConnectiveMock()); 
        WS001_ConnectiveMock.externalId=fip.id;
        LC_FipUpdateController.sendFipReminder(fip);
        System.assertEquals('Aucun document trouvé', LC_FipUpdateController.getFipStatus(fip).message);
	   Test.stopTest();
    }
}


static testMethod void getFipTest() {
     
    String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

    //Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;   
	
	System.RunAs(director) {						
	  Account a = new Account();
        a.Name = 'TESTFIPAMIANTE';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Aéronautique';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingStreet = '12 rue du test';
        a.BillingPostalCode = '39000';
        a.BillingCity = 'Lyon';
        a.NAF_Number__c ='1234A';
        a.ChargedX3Code__c = 'FR972139821';
        a.X3ChargedAddressCode__c = 'GEN';
        a.FavoriteSubsidiaryAsbestos__c = 'Tredi Salaise 1';
        a.SIRET_Number__c = '00092183013812';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName1';
        c.AccountId = a.Id;
        c.Email = 'aaaa@groupe-seche.com';
        c.Phone = '03 00 00 00 00';
        insert c;
        
        FIP_FIP__c fip = new FIP_FIP__c();
        system.debug('Collector__c = '+ a.ID);
        fip.Collector__c = a.ID;
        system.debug('Contact__c = '+ c.ID);
        fip.Contact__c = c.ID;
        fip.State__c = 'Refusée'; 
        fip.GenerateFIPPDF__c = true;
        insert fip;
        System.assertEquals(fip.id, LC_FipUpdateController.getFip(fip.id).Id);
        
	}
}

@isTest
static void testgetFipListview() {    
    String listViewDeveloperName = 'FIPToApprouve';
    ListView litView = LC_FipUpdateController.getListViewByName(listViewDeveloperName);

    System.assertNotEquals(null, litView.Id);
}


}