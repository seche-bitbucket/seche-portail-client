/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-06-29
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for Increase Face To Face on Amendment Button
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@IsTest
public class Test_VFC_IncreaseFaceToFace {
    public static PageReference myVfPage {get;set;}
    public static ApexPages.StandardSetController stc {get;set;}
    public static VFC_IncreaseFaceToFace vfc{get;set;}    
    @isTest static void TestIncreaseFaceToFace(){
        PageReference ref = new PageReference('/001/o');
        Test.setCurrentPage(ref);
        ApexPages.currentPage().getParameters().put('retURL', '/001/o');
        Contract__c c = new Contract__c();
        c.name='t';
        insert c;
        Amendment__c am = new Amendment__c();
        am.Contrat2__c=c.id;
        am.PriceCurrentYear__c=120;
        insert am;
        List<Amendment__c> Amendments = new List<Amendment__c>();
        Amendments.add(am);
        stc=new Apexpages.standardSetController(Amendments);
        stc.setSelected(Amendments);
        vfc = new VFC_IncreaseFaceToFace(stc);
        test.startTest();
        vfc.increase();
        test.stopTest();
        am=[SELECT PriceNextYear__c,PriceCurrentYear__c,ByPassContract__c from Amendment__c Where Id=:am.Id];
        System.assertEquals(Null, am.PriceNextYear__c);
        System.assertEquals(true, am.ByPassContract__c);    
        c.RecordLocked__c=true;
        Update c;
        vfc = new VFC_IncreaseFaceToFace(stc);
        vfc.increase();
    }
    @isTest static void TestExceptions(){
        List<Amendment__c> Amendments = new List<Amendment__c>();
        stc=new Apexpages.standardSetController(Amendments);
        stc.setSelected(Amendments);
        vfc = new VFC_IncreaseFaceToFace(stc);
        System.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages()[0].getSeverity());
        System.assertEquals('Veuillez selectionner un avenant', ApexPages.getMessages()[0].getSummary());
    }
}