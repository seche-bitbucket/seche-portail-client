@isTest
public with sharing class VFC_SendReminderForQuoteControllerTest {

@TestSetup
static void setup(){
	Account ac = new Account();
	ac.Name = 'testAccName';
	ac.CustomerNature__c = 'Administration';
	ac.Industry = 'Trader';
	ac.CurrencyIsoCode = 'EUR';
	ac.Producteur__c = false;
	ac.BillingPostalCode = '00000';
	ac.NAF_Number__c = '1234A';
	insert ac;

	Contact contactTest = new Contact();
	contactTest.LastName = 'testName';
	contactTest.AccountId = ac.Id;
	contactTest.Email = 'aaa@yopmail.com';
	//contactTest.Phone = '0000000000';
	insert contactTest;

    
	Pricebook2 pb = new Pricebook2(Name='PricebookFille', isActive=true);
	insert pb;

	Product2 prd = new product2(name = 'Test',TECH_ExternalID__c = '012345',Family = 'Traitement');
	insert prd;

	PricebookEntry pbeMere = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=Test.getStandardPricebookId(),
	                                            isActive=true);
	insert pbeMere;

	PricebookEntry pbe = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=pb.Id,
	                                        isActive=true,usestandardprice = true);
	insert pbe;
	//Get a Record Type for Opportunity
	RecordType rt = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_OpportuniteMere' LIMIT 1];

	//Create Opportunity Mother
	Opportunity opport = new Opportunity(Name = 'OPPtest', RecordTypeId = rt.Id, AccountID= ac.Id, ContactName__c = contactTest.Id, Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', PriceBook2Id = pb.Id);

	insert opport;
	system.debug('opport '+opport.ID);
	Quote qu = new Quote(name='01010', opportunityId=opport.Id);
	insert qu;

	ContentVersion cv = new ContentVersion();
	cv.title = 'test content trigger';      
	cv.PathOnClient ='test';           
	cv.VersionData =Blob.valueOf('Unit Test Attachment Body');          
	insert cv;         
	
	ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
	
	ContentDocumentLink contentlink=new ContentDocumentLink();
	contentlink.LinkedEntityId=qu.id;
	contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
	contentlink.ShareType = 'V';
	insert contentlink;

	Package__c pkg = new Package__c(
		ConnectivePackageId__c='3bb6a2d1-35db-4a3a-a434-868c01bcca9d',
		ConnectivePackageStatus__c='Pending'
	);
	insert pkg;

	DocumentsForSignature__c doc=new DocumentsForSignature__c();
	doc.Name= 'titre';
	doc.SigningUrl__c='';
	doc.SentTo__c=contactTest.Id;
	doc.TECH_ExternalId__c=cv.Id;
	doc.ConnectiveDocumentId__c = cv.Id;
	doc.Status__c = 'PENDING';
	doc.Quote__c=qu.Id;
	doc.Package__c = pkg.Id;
	insert doc;
}

static testMethod void initTest() {
     
    String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

    //Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;
	
	System.RunAs(director) {
		List<Quote> quoteList = [SELECT Name FROM Quote LIMIT 20];				
		PageReference pageRef = Page.VFP_SendReminderForQuote;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',quoteList[0].Id);
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(quoteList);
        VFC_SendReminderForQuoteController myController = new VFC_SendReminderForQuoteController(ssc); 
		Test.setMock(HttpCalloutMock.class, new ConnectiveMockSendReminder());     
		myController.setIsTestRunnig(false);
        myController.init(); 
		myController.goback();        
        System.assertEquals(false, myController.getIsNoSelectedOpp());
		System.assertEquals(false, myController.getIsTestRunnig());
       
	}
}

static testMethod void initTestWithNoSelected() {
     
   String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

    //Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;
	
	System.RunAs(director) {
		List<Quote> quoteList = [SELECT Name FROM Quote LIMIT 20];				
		PageReference pageRef = Page.VFP_SendReminderForQuote;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',quoteList[0].Id);
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(quoteList);      
        VFC_SendReminderForQuoteController myController = new VFC_SendReminderForQuoteController(ssc);
		Test.setMock(HttpCalloutMock.class, new ConnectiveMockSendReminder());

        myController.setIsTestRunnig(true);
        myController.init();        
        System.assertEquals(true, myController.getIsNoSelectedOpp());        
       
	}
}
static testMethod void initTestWithNoRecord() {
     
	String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};
 
	 //Get a profile to create User
	 Profile p = [select id from profile where name in:profiLisList limit 1];
 
	 String testemail2 = 'assistant_-_User_test@test.com';
 
	 User director = new User(profileId = p.id, username = testemail2, email = testemail2,
							  emailencodingkey = 'UTF-8', localesidkey = 'en_US',
							  languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
							  alias='cspu', lastname='lastname', IsActive=true);
	 insert director;
	 
	 System.RunAs(director) {
		 List<Quote> quoteList = [SELECT Name FROM Quote LIMIT 20];
		 List<Quote> quoteListEmpty = new List<Quote>();				
		 PageReference pageRef = Page.VFP_SendReminderForQuote;
		 Test.setCurrentPage(pageRef);
		 pageRef.getParameters().put('id',quoteList[0].Id);
		 ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(quoteListEmpty);      
		 VFC_SendReminderForQuoteController myController = new VFC_SendReminderForQuoteController(ssc);
		 Test.setMock(HttpCalloutMock.class, new ConnectiveMockSendReminder());
 
		 myController.setIsTestRunnig(false);
		 myController.init();        
		 System.assertEquals(true, myController.getIsNoSelectedOpp());        
		
	 }
 }
}