@isTest
public class ContactControllerTest{
    
  @IsTest(SeeAllData=true) 
    public static void TestContactController(){
        //Set the current VF Page which use the ContactController
        PageReference pgNouveauCRContact = Page.CR_NouveauCRContact;
        
        //Standard Set controller
        ApexPages.StandardSetController stdCtrl;
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Assistante commerciale' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname');
        
        System.RunAs(currentUser) { 
           
            //Get a Record Type
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];

            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, NAF_Number__c='1234A', CustomerNature__c = 'Administration', Industry = 'Aucun', BillingPostalCode = '69000', Producteur__c = false);
            
            try{
                insert ac;
            }catch(Exception e){
                System.debug('.............EXCEPTION : '+e);
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '03 06 06 06 06', Salesman__c = currentUser.ID);
            
             try{
                insert contactTest;
            }catch(Exception e1){
            }
                        
            //    Start TEST
            
            Test.startTest();
            
            // Set the currentPage to the test
            Test.setCurrentPage(pgNouveauCRContact);
           //Set parameter 
            pgNouveauCRContact.getParameters().put('ID', contactTest.Id );
          
           //Initialize Controller
            ContactController ctrl = new ContactController(stdCtrl);
            
            System.assertEquals(ctrl.Account.Name, ac.Name);
            System.assertEquals(ctrl.Contact.Name, contactTest.LastName);

            Test.stopTest();
            
            //    End TEST
                      
        }
    }
    
}