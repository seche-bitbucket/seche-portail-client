/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-06-29
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for Commercial Director Table Controller 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class Test_CommercialDirectorTableController {
    public static PageReference myVfPage {get;set;}
    public static ApexPages.StandardController stc {get;set;}
    public static VFC_CommercialDirectorTableController vfc{get;set;}  
    @isTest static void TestCommercialDirectorTableController(){
        //Insert users to test the visualforce page
        List<User> users= new List<User>();
        Profile p = [select id from profile where name = :'Assistante de direction' limit 1];
        
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        director.UserRoleId=[select Id from UserRole where Name='Direction Commerciale - Nicolas Rogeau'].Id;
        
        users.add(director);
        String testemail = 'Commercial-_User_test@test.com';
        User Commercial = new User(profileId = p.id, username = testemail, email = testemail,
                                   emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                   languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                   alias='cs', lastname='lastname2', IsActive=true);
        Commercial.UserRoleId=[select Id from UserRole where Name='Grands comptes - Responsable commercial'].Id;
        
        users.add(Commercial);
        String testemail3 = 'Commercial3-_User_test@test.com';
        User Commercial2 = new User(profileId = p.id, username = testemail3, email = testemail3,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cs', lastname='lastname3', IsActive=true);
        Commercial2.UserRoleId=[select Id from UserRole where Name='Grands comptes - Responsable commercial'].Id;
        
        users.add(Commercial2);
        insert users;
        System.RunAs(director) {
            Account ac = new Account();
            ac.Name = 'testAccName';
            ac.CustomerNature__c = 'Administration';
            ac.Industry = 'Trader';
            ac.CurrencyIsoCode = 'EUR';
            ac.Producteur__c = false;
            ac.BillingPostalCode = '00000';
            ac.NAF_Number__c = '1234A';
            insert ac;
            Account ac2 = new Account();
            ac2.Name = 'TestAcc2';
            ac2.CustomerNature__c = 'Administration';
            ac2.Industry = 'Trader';
            ac2.CurrencyIsoCode = 'EUR';
            ac2.Producteur__c = false;
            ac2.BillingPostalCode = '69006';
            ac2.NAF_Number__c = '1234A';
            insert ac2;
            List<Contract__c> contractsToInsert=new List<Contract__c>();
            for(integer i=0;i<10;i++){
                Contract__c c = new Contract__c();
                c.Account__c=ac.id;
                c.AssociatedSalesRep__c=Commercial.id;
                c.Filiale__c='Séché Eco Industries Changé';
                c.Filiere__c='Biocentre';
                c.RegroupmentIndex__c=null;
                c.name='Contract Test'+i;
                c.Status__c ='Soumis';
                contractsToInsert.add(c);
            }
            for(integer i=0;i<10;i++){
                Contract__c c = new Contract__c();
                c.Account__c=ac.id;
                c.AssociatedSalesRep__c=Commercial.id;
                c.Filiale__c='Sodicome';
                c.RegroupmentIndex__c=null;
                c.name='Contract Test'+i;
                contractsToInsert.add(c);
            }
            for(integer i=0;i<20;i++){
                Contract__c c = new Contract__c();
                c.Account__c=ac2.id;
                c.AssociatedSalesRep__c=Commercial2.id;
                c.Filiale__c='Sogad';
                c.RegroupmentIndex__c=1;
                c.name='Contract Test'+i;
                contractsToInsert.add(c);
            }
            if(contractsToInsert.size()>0){
                insert contractsToInsert;
            }
            test.startTest();
            vfc = new VFC_CommercialDirectorTableController();
            //checking the visualforce controller elements that will be shown in the VFP
            System.assertEquals('lastname2',vfc.ContractSalesreps[0].salesRepName); 
            System.assertEquals('Séché Eco Industries Changé',vfc.ContractSalesreps[0].Filiales[0].Filiale);
          
            test.stopTest();
        }
    }
}