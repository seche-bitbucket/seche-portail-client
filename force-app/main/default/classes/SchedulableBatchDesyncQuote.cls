/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2019-03-13
* @modified       2019-03-18
* @systemLayer    Batch Class         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Schedulable batch class to desync quotes 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global class SchedulableBatchDesyncQuote implements Schedulable{
    global void execute(SchedulableContext sc) {
        BatchDeSyncQuote  batchToExecute = new  BatchDeSyncQuote(); 
        database.executebatch(batchToExecute);
    }
}