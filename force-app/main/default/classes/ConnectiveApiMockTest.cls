@isTest
global class ConnectiveApiMockTest implements HttpCalloutMock{
    
    global Static String externalIdDocument;
    global Static Contact c;
    
    global HttpResponse respond(HttpRequest req) {
        
        HttpResponse res = new HttpResponse();

        System.debug('req = ' + req);
        String reqMethod = req.getMethod();
        String reqEndpoint = req.getEndpoint();

        if (reqEndpoint.endsWith('packages') && reqMethod == 'POST'){
            Map<String, String> reqBody = (Map<String, String>)JSON.deserialize(req.getBody(), Map<String,String>.class);
            //create package OK test
            if ((String)reqBody.get('PackageName') == 'packTest'){
                Map<String, String> resBody = new Map<String, String>{
                    'PackageId' => 'idTestOk'
                };
                res.setBody(JSON.serialize(resBody));
                res.setStatusCode(201);
            }
            //create package NOK test
            else if ((String)reqBody.get('PackageName') == 'packTestNOK'){
                Map<String, String> resBody = new Map<String, String>{
                    'PackageId' => 'idTestNOK'
                };
                res.setBody(JSON.serialize(resBody));
                res.setStatusCode(400);
            }
            else if(reqBody.get('PackageName').contains('Final')) {
                Map<String, String> resBody = new Map<String, String>{
                    'PackageId' => 'idTestOK'
                };
                res.setBody(JSON.serialize(resBody));
                res.setStatusCode(201);
            }
            //if not both then body will be null and trigger the apex exception
        }
        else if (reqEndpoint.endsWith('documents') && reqMethod == 'POST'){
            String packId = reqEndpoint.substringBetween('packages/', '/documents');
            //add document OK test
            if (packId == 'idTestOk'){
                ConnectiveApiUtils.RespAddDoc resBody = new ConnectiveApiUtils.RespAddDoc();
                resBody.DocumentId = 'idRespOk';
                ConnectiveApiUtils.RespLocation loc = new ConnectiveApiUtils.RespLocation();
                loc.Id = 'idLocOk';
                resBody.Locations = new List<ConnectiveApiUtils.RespLocation>{loc};
                res.setBody(JSON.serialize(resBody));
                res.setStatusCode(200);
            }
            //add document NOK test
            else if (packId == 'idTestNOK'){
                res.setBody('error');
                res.setStatusCode(400);
            }
            //if not both then body will be null and trigger the apex exception
        }
        else if (reqEndpoint.endsWith('process') && reqMethod == 'PUT'){
            String packId = reqEndpoint.substringBetween('packages/', '/process');
            //process OK test
            if (packId == 'idTestOk'){
                res.setStatusCode(200);
            }
            //process NOK test
            else if (packId == 'idTestNOK'){
                res.setStatusCode(400);
            }
            //if not both then body will be null and trigger the apex exception
        }
        else if (reqEndpoint.endsWith('status') && reqMethod == 'PUT'){
            String packId = reqEndpoint.substringBetween('packages/', '/status');
            //set status OK test
            if (packId == 'idTestOk'){
                res.setStatusCode(200);
            }
            //set status NOK test
            else if (packId == 'idTestNOK'){
                res.setStatusCode(400);
            }
            //if not both then body will be null and trigger the apex exception
        }
        else if (reqEndpoint.endsWith('status') && reqMethod == 'GET'){
            String packId = reqEndpoint.substringBetween('packages/', '/status');
            system.debug('working here: '+packId);
            try{
                //get status OK test
                if (packId == 'idTestOk'){
                    /*PackageResponse resBody = new PackageResponse();
                    resBody.PackageStatus = 'FINISHED';
                    PackageResponse.ResponseActor act = new PackageResponse.ResponseActor();
                    act.Type = 'Receiver';
                    act.ActionUrl = 'testUrl';
                    PackageResponse.ResponseStakeholder sth = new PackageResponse.ResponseStakeholder();
                    sth.Actors = new List<PackageResponse.ResponseActor>{act};
                    resBody.Stakeholders = new List<PackageResponse.ResponseStakeholder>{sth};
                    res.setBody(JSON.serialize(resBody));*/
                    system.debug(c.Id);
                    res.setStatusCode(200);
                    res.setBody('{"PackageName": "package-docu1.pdf","Initiator": "signer@gmail.com","ExpiryTimestamp": null,"ExternalPackageReference": "idTestOk","F2FSigningUrl": "http://myserver/signinit?packageSignId=6bc402eb-6cbd-423a-bf00-1157e8d68f37&f2f=True","PackageStatus": "FINISHED","PackageDocuments": [{"DocumentId": "dc2691d8-e3c0-470b-9715-e55b489ea493","DocumentType": "application/pdf","ExternalDocumentReference": "'+externalIdDocument+'","DocumentName": "docu1"}],"Stakeholders": [{"Type": "PersonGroup","EmailAddress": null,"ContactGroupCode": null,"ExternalStakeholderReference": "'+String.valueOf(c.Id)+'","StakeholderId": "6b2cda0c-ab81-4984-9e16-159fe20d983f","Actors": [{"Type": "Signer","Reason": null,"CompletedBy": null,"CompletedTimestamp": null,"Locations": [{"Id": "24ab070a-29e4-496e-9b79-e66c0edcced7","UsedSigningType": null}],"ActorId": "2806f94d-2a45-4168-8667-cbd4ce4ce090","ActionUrl": "testUrl","ActionUrls": [{"EmailAddress": "john.smith@mail.com","Url": "https://MyURL.com"},{"EmailAddress": "jane.jefferson@mail.com","Url": "https://HerURL.com"}],"ActorStatus": "Available"},{"Type": "Receiver","ActorId": "e3da1877-fac9-43c4-9949-bacef76718fa","ActionUrl": null,"ActionUrls": [],"ActorStatus": ""}],"PersonGroupName": "APIGroup"},{"Type": "Person","EmailAddress": "signer@gmail.com","ContactGroupCode": null,"ExternalStakeholderReference": "'+c.Id+'","StakeholderId": "490e242f-43c4-4bc0-a1b4-e2d67dc1fdac","Actors": [{"Type": "Receiver","ActorId": "0903c863-9197-4abf-93f4-694fddde9d99","ActionUrl": null,"ActionUrls": [],"ActorStatus": ""}],"PersonGroupName": null}],"CreationTimestamp": "2019-10-23T13:34:12Z"}');
                }
            } catch(Exception e) {
                System.debug('exception: '+e);
            }
        }
        
        system.debug('res: '+res);

        return res;
    }
}