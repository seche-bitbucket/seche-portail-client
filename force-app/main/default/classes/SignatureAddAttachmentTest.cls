/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-08-08
* @modified       2018-08-08
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@IsTest
public class SignatureAddAttachmentTest {
    @Istest static void getAttachmentFromStatus(){
        Account a;
        Contact c;
        Opportunity opp;
        
        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        Quote q = new Quote();
        q.OpportunityId = opp.ID;
        q.Name = opp.Name;
        q.DateDevis__c = Date.today();
        q.QuoteCode__c=opp.Name;
        q.ContactId = opp.ContactName__c;
        q.Email = opp.ContactName__r.Email;
        q.Phone = opp.ContactName__r.Phone;
        q.Fax = opp.ContactName__r.Fax;
        q.BillingStreet = opp.Account.BillingStreet;
        q.BillingCity = opp.Account.BillingCity;
        q.BillingState = opp.Account.BillingState;
        q.BillingPostalCode = opp.Account.BillingPostalCode;
        q.BillingCountry = opp.Account.BillingCountry; 
        insert q;
        Attachment att = new Attachment();
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        att.body=bodyBlob;
        att.Name = q.Name+'- Devis.pdf';	
        att.ParentId = q.Id;             
        insert att; 
        DocumentsForSignature__c doc=new DocumentsForSignature__c();
        doc.Name=q.Name+'.pdf';
        doc.FlowId__c='a02c9f7a-cfd0-46d2-8ba3-465a5bd79fce';
        doc.Document__c='https://i.pinimg.com/736x/ba/03/23/ba03237a6d6499f0e2633314826e1526--cutest-animals-baby-animals.jpg';
        doc.Quote__c=q.id;
        doc.Status__c='SIGNED';
        doc.TECH_ExternalId__c=q.id;
        insert doc;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WS003_ConnectiveMock());
        WS003_ConnectiveMock.externalId=q.id;
        WS003_ConnectiveMock.status='SIGNED';
        SchedulableContext sc = null;
        SignatureAddAttachment tsc = new SignatureAddAttachment();
        tsc.execute(sc);
        Test.stopTest();
        List<Attachment> atts = [SELECT id,Name FROM Attachment WHERE Name like:'%Signée%' ORDER BY CreatedDate];
        System.assertEquals(1,atts.size() );
        
    }
}