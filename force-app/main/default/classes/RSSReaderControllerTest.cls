@isTest
public class RSSReaderControllerTest {

    static testMethod void testGenerateVR()
    { 
        RSSReaderController.getRSSReaderController();
        
        String xmlContent = '<?xml version="1.0" encoding="iso-8859-1"?><rss version="2.0"><channel><title>Seche Environnement</title><link>http://www.groupe-seche.com</link><description></description><item><title>Lancement du projet NanoWet </title><link>http://www.groupe-seche.com/actu-_117-actualite.html</link><guid isPermaLink="true">http://www.groupe-seche.com/actu-_117-actualite.html</guid><description>NanoWet, un programme de recherche pour une meilleure maîtrise des émissions potentielles de nanoparticules lors de l \'incinération de nanomatériaux en fin de vie  Lancé cette année, le projet NanoWet est coordonné par MINES NANTES Laboratoire GEPEA et réalisé en collaboration avec ...</description><pubDate>Mon, 07 Nov 2016 10:34:00 +0100</pubDate><image>http://www.groupe-seche.com//image,/images/actualites/117_Logo-NanoWet-RVB.jpg,l,120,90.jpg</image></item></channel></rss>';
        
		RSSReaderController.parse(xmlContent);  
            
    }
}