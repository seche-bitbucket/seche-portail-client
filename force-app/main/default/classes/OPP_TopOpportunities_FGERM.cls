public class OPP_TopOpportunities_FGERM{
    
    public List<SelectOption> nbOpps{get;set;}
    public Integer nbOpp;
    public List<String> lstFiliale = new List<String>();
    
    public List<Opportunity> oppRCEY{get;set;}
    public List<Opportunity> oppFBUL{get;set;}
    public List<Opportunity> oppFGIO{get;set;}
    public List<Opportunity> oppYPIE{get;set;}
    public List<Opportunity> oppMRUF{get;set;}
    
    
    public OPP_TopOpportunities_FGERM() {      
        nbOpp = 5;
        
        lstFiliale.add('Tredi Salaise 1');
        lstFiliale.add('Tredi Salaise 2');
        lstFiliale.add('Tredi Salaise 3');
        lstFiliale.add('Tredi Salaise 4');
        
        setTopOpportunity();
    }
    
    public void setTopOpportunity(){
        
        oppRCEY = [Select ID, Name, AccountID, Account.Name, Amount  FROM Opportunity WHERE Salesman__c = '00558000000jrE2' AND filiale__c in : lstFiliale ORDER BY AMOUNT DESC LIMIT : nbOpp];
    	oppFBUL = [Select ID, Name, AccountID, Account.Name, Amount  FROM Opportunity WHERE Salesman__c = '00558000000jrZl' AND filiale__c in : lstFiliale ORDER BY AMOUNT DESC LIMIT : nbOpp];
    	oppFGIO = [Select ID, Name, AccountID, Account.Name, Amount  FROM Opportunity WHERE Salesman__c = '00558000000jrca' AND filiale__c in : lstFiliale ORDER BY AMOUNT DESC LIMIT : nbOpp];
        oppYPIE = [Select ID, Name, AccountID, Account.Name, Amount  FROM Opportunity WHERE Salesman__c = '00558000000jrcL' AND filiale__c in : lstFiliale ORDER BY AMOUNT DESC LIMIT : nbOpp];
        oppMRUF = [Select ID, Name, AccountID, Account.Name, Amount  FROM Opportunity WHERE Salesman__c = '00558000000jrc6' AND filiale__c in : lstFiliale ORDER BY AMOUNT DESC LIMIT : nbOpp];
    }
    
}