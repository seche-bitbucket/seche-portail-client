/* -------------
  Auth : Baïla TOURE
  Date : 24/02/2020
*/
@isTest
public class LC_GenerateAttestationCtrlTest {
    
    @testSetup static void setup() {
        // Create common test accounts
       List<Account> listAccount = TestDataFactory.createAccountList('TestName',5); 
       insert listAccount;

       //Create Contacts 
       List<Contact> listContact = new List<Contact>();
       for(Account acc : listAccount){
           listContact.add(TestDataFactory.createContact(acc));
       } 
       insert listContact;

       //Create Assistant Commercial
       User assistantCo = TestDataFactory.createAssistantCommercialUser();

        //Create Resp Commercial
       User responsableCo = TestDataFactory.createResponsableCommercialUser(assistantCo);

       //Create Attestations 
       // params : TestDataFactory.createAttestation(Contact c, User responsableCo, Boolean isWood, Boolean isMixed, Boolean isPaperCardboard, Boolean isPlastic, Boolean isGlass, Boolean isPlaster, Boolean isMineralFractions, Boolean isSorted, Boolean isTechCongaAttestationToGenerate, String status)
       List<Attestation__c> listAttestation = new List<Attestation__c>();
       listAttestation.add(TestDataFactory.createAttestation(listContact[0], responsableCo, false, false, false,false,false, true, false, true, true, true, Constants.ATTESTATION_DRAFT_STATUS));       
       insert listAttestation;       
    
    }

    
@isTest static void testGetAttestationById() {
    PageReference pageRef = Page.VFP_GenerateAttestation;      
    List<Attestation__c> listAttestation = [SELECT Id, Name, Status__c FROM Attestation__c];   
    Test.StartTest();
    Attestation__c att = LC_GenerateAttestationCtrl.getAttestation(listAttestation[0].Id);
    Test.StopTest();
    System.assertEquals(listAttestation[0].Id, att.Id);
}

@isTest static void testUpdateAttestationById() {
    PageReference pageRef = Page.VFP_GenerateAttestation;      
    List<Attestation__c> listAttestation = [SELECT Id, Name, Status__c,TECH_CongaAttestationToGenerate__c FROM Attestation__c];   
    Test.StartTest();
    Id attId = listAttestation[0].Id;
    LC_GenerateAttestationCtrl.RequestResponse resp  = LC_GenerateAttestationCtrl.updateAttestation(attId);
    Attestation__c attUpdate = [SELECT Id, Name, Status__c,TECH_CongaAttestationToGenerate__c FROM Attestation__c WHERE Id = :attId ]; 
    Test.StopTest();
    System.assertEquals(resp.message,'Votre attestation est en cours de génération. Veuillez actualiser la page dans quelques instants pour accéder aux documents dans la liste des Fichiers.');
}

}