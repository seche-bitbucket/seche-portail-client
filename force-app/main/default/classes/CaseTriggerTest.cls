/** * @author Thomas Prouvot * @date Creation 16.09.2016 * @description CaseTriggerTest – TriggerTestClass */
@isTest
public class CaseTriggerTest {
    
    @isTest(seeAllData=true)
    static void testCase(){
        //TODO test trigger to be bulkified
         Account ac = new Account(Name='TestAccount',
                                       Producteur__c = false,
                                       BillingPostalCode='69800',
                                       TVA_Number__c = '2434234',
                                        NAF_Number__c = '1234B');
                                       //SIRET_Number__c = 345436);
        insert ac;
        
       
        User commercial = new User(Alias = 'standt',
                          Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8',
                          LastName='Testing',
                          LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US',
                          ProfileId = UserInfo.getProfileId(), 
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName='monuser@montest.org');   
        insert commercial;
        
         Contact cont = new Contact(FirstName='Test',
                        LastName='Test',
                        Salesman__c = commercial.ID,
                        Accountid= ac.id);
        insert cont;

        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'CASE_RT_Standard'];
        
        Test.startTest();
        Case ca = new Case(Salesman__c=commercial.Id,
                          AccountId=ac.Id,
                          Definition__c = 'Information',
                          ContactId=cont.Id,
                          RecordTypeId = rt.Id,
                          Status='Working',
                          Origin = 'Phone');
        insert ca;
        
        //reload to get case id
        ca = [SELECT Id, OwnerId FROM CASE WHERE Id=:ca.Id];
        System.assertEquals(commercial.Id , ca.OwnerId);
        
        CaseShare cs = [Select Id, CaseId, UserOrGroupId, CaseAccessLevel FROM CaseShare WHERE CaseId=: ca.Id AND CaseAccessLevel='Edit'];
        System.assertEquals(cs.UserOrGroupId , UserInfo.getUserId());        
        
        Test.stopTest();        
    }
}