public with sharing class EmailSendController {
    @AuraEnabled
    public String ID {get; set;}
    
    @AuraEnabled
    public String UID {get; set;}
    
    
    @AuraEnabled  
    public static void sendMailMethod(String mMail ){
        system.debug('test sendMailMethod ' + mMail); 
        
        id userId = UserInfo.getUserId();
        system.debug('userId   ' + userId);

        User u = [select id, contactId  from User where id = : userId];
        id getContactId = u.contactId;
        system.debug('getContactId   ' + getContactId);
        
        Contact vContact =[select id, accountID from contact where id=:getContactId];
        
        //system.debug('test vContact.accountID =' +vContact.accountID);
        //system.debug('test vContact.id =' + vContact.id);
        // Contact vContact =[select accountID from contact where OwnerId=:userId];
        string vAccountId = vContact.AccountID;
        system.debug(' vAccountId   ' +  vAccountId);
        //la MAJ de utilisateur permet la generation de TECH_UID__c
        update u;

        Account vAccount = [Select TECH_UID__c,name,ID From Account Where ID=:vAccountId];
        system.debug(' vAccount.TECH_UID__c   ' +  vAccount.TECH_UID__c);

         
        String mSubject = 'FIP Amiante en ligne pour ' +vAccount.name;
        
            String mbody = '<p>Bonjour,</p><p>&nbsp;</p><p>Suite &agrave; votre demande, la soci&eacute;t&eacute; '+vAccount.name+' vous invite &agrave; renseigner une FIP (Fiche d\'Identification Préalable) dans le but d\'établir un CAP Amiante.</p>'+
            +'<p>Merci de compl&eacute;ter la fiche &agrave; cette adresse :<a href ="'+Label.URL_ORG+'/amiante/s/fip?UID='+vAccount.TECH_UID__c+'&ID='+vAccount.ID+'&CID='+vContact.ID+'">Cliquer sur le lien</a>.</p>'+
            +'<p>Merci de conserver l\'email pour de future demande FIP Amiante.</p>'
            +'<p>&nbsp;</p>'+
            +'<p>Vous trouverez ici le :<a href ="'+label.GuideUtilisateurFIP+'">Guide utilisateur</a></p>' ;
        
        
        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();     
        
        // Step 1: Create a new Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        // Step 2: Set list of people who should get the email
        List<String> sendTo = new List<String>();
        sendTo.add(mMail);
        mail.setToAddresses(sendTo);
        
        // Step 3: Set who the email is sent from
       // mail.setReplyTo('noreply@gmail.com'); // change it with your mail address.
        mail.setSenderDisplayName('Séché connect solution amiante'); 
        
        // Step 4. Set email contents - you can use variables!
        mail.setSubject(mSubject);
        mail.setHtmlBody(mbody);
        
        // Step 5. Add your email to the master list
        mails.add(mail);
        
        // Step 6: Send all emails in the master list
        Messaging.sendEmail(mails);
    }   
}