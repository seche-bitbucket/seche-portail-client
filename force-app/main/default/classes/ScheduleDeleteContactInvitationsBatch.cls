public with sharing class ScheduleDeleteContactInvitationsBatch implements Schedulable{
    
    public void execute(SchedulableContext context) {
        DeleteContactInvitationsBatch batch = new DeleteContactInvitationsBatch();

        Integer batchSize = 200;

        Database.executeBatch(batch, batchSize);
    }
}