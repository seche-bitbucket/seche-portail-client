@isTest
global class WS002_ConnectiveMock implements  HTTPCalloutMock{
    global Static String externalId;
    global Static String status;
	global HTTPResponse respond(HTTPRequest req){
        HttpResponse res = new HTTPResponse();
        res.setHeader('Content-Type', 'application/JSON');
        res.setBody('{"ExternalReference":"'+externalId+'","Status":"'+status+'","DownloadURL":"https://google.com","F2FSigningURL":"https://portaldemo3.connective.eu/signinit?id=a02c9f7a-cfd0-46d2-8ba3-465a5bd79fce","Signers":[{"SignerId":"eef3c466-ce15-487d-9883-3bfb19209ba0","ExternalReference":"0039E00000LBf6yQAD","EmailAddress":"sechecrm69@gmail.com","Uri":"https://portaldemo3.connective.eu/signinit?id=eef3c466-ce15-487d-9883-3bfb19209ba0","Status":"PENDINGSIGNING","RejectReason":null,"SignatureTimestamp":null,"SignatureSigner":null,"RedirectURL":""}],"ExpiryTimestamp":null,"ErrorLog":[]}');
        req.setMethod('GET');
        res.setStatusCode(200);
        return res;
    }
}