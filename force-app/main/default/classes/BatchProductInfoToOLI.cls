global class BatchProductInfoToOLI implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Id, Name,TECH_CatalogueMatch__c, Features__c,N_CAP__c  FROM OpportunityLineItem Where TECH_CatalogueMatch__c !=null';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<OpportunityLineItem> scope)
    { 	
        Set<String> CatalogCodes=new Set<String>();
        for(OpportunityLineItem oli : scope)
        {   
            CatalogCodes.add(oli.TECH_CatalogueMatch__c);
        }
        List<Product2> ProductsInfo=[SELECT Id,Name,Description,Features__c,ProductCode From Product2 Where ProductCode IN:CatalogCodes];
        List<OpportunityLineItem> olis=new List<OpportunityLineItem>();
        for(OpportunityLineItem oli : scope)
        {   
            for(Product2 p2:ProductsInfo){
                if(p2.ProductCode==oli.TECH_CatalogueMatch__c){
                    oli.features__c=p2.features__c;
                    oli.TECH_CatalogueMatch__c=null;
                    olis.add(oli);
                    break;
                }     
            }
        }
        if(olis.size()>0){
            Update olis;
        }
    }
    global void finish(Database.BatchableContext BC)
    { 
        if(!Test.isRunningTest()){
            CourrierConventionBatch batch =new CourrierConventionBatch();
            id BatchId= Database.executeBatch(batch,100);
        }
    }
}