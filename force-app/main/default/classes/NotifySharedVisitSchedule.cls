global class NotifySharedVisitSchedule  implements Schedulable{
    global void execute(SchedulableContext sc) {
        
        NotifySharedVisitReport vcr = new NotifySharedVisitReport();        
        
        Database.executebatch(vcr);
    }
}