@isTest
public with sharing class INV_SendEmailTest {
    
    @isTest 
    public static void test() {
    
        Id emailTemplateId = [SELECT Id, FolderName, DeveloperName FROM emailTemplate WHERE DeveloperName = 'ATT_Signed_Client_Notification'].Id;
        
        ContentVersion cv = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.pdf',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert cv;

        ContentVersion cv2 = new ContentVersion(
            Title = 'Test - Signée',
            PathOnClient = 'Test - Signée.pdf',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert cv2;
        List<Id> documentsId = new List<Id>();
        for(ContentDocument cd : [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument]) {
            documentsId.add(cd.Id);
        }

        Id owaId = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = 'Séché Connect'].Id;

        Contact c = TestFactory.generateContact();

        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;

        Opportunity opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        Product2 prd = new product2(name = 'Test' ,TECH_ExternalID__c = '012345',Family = 'Traitement', isActive = true );
        insert prd;
        
        PricebookEntry pbe = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=Test.getStandardPricebookId(),
                                 isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem (OpportunityID = opp.id,PriceBookEntryID=pbe.id, quantity=4, UnitPrice=4);
            insert oli;
            
        Id rtId = [SELECT Id FROM RecordType WHERE DeveloperName LIKE '%Attestation_Producteur_DND%'][0].Id;
        Attestation__c att = new Attestation__c(Account__c = a.Id, RecordTypeId = rtId, OpportunityLineItem__c = oli.Id);
        insert att;

        INV_SendEmail.EmailParameters params = new INV_SendEmail.EmailParameters();
        params.attachementsId = documentsId;
        params.toId = c.Id;
        params.emailTemplateId = emailTemplateId;
        params.ccAdresses = new List<String>{'test@test.com'};
        params.owa = owaId;
        params.whatId = att.Id;
        
        INV_SendEmail.sendEmail(new List<INV_SendEmail.EmailParameters>{params});

        params.ccAdresses = new List<String>{'Error'};
        INV_SendEmail.sendEmail(new List<INV_SendEmail.EmailParameters>{params});

    }

}