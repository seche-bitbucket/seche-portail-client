Global with sharing class ConnectiveCallout {

/* Function retrieveName : used to retrieve the Name that will be
 * added as the name of the document sent for signing
 */
public static String retrieveName(Id objId){
	String returnValue = '';
	System.debug(objId);
	String sobjectType = objId.getSObjectType().getDescribe().getName();
	Schema.SObjectType convertType = Schema.getGlobalDescribe().get(sobjectType);
	if (convertType == Quote.sObjectType) {
		returnValue = [Select Id,QuoteCode__c FROM Quote Where Id= : objId LIMIT 1].QuoteCode__c;
	} else if (convertType == FIP_FIP__c.sObjectType) {
		returnValue = [Select Id,Name FROM FIP_FIP__c Where Id= : objId LIMIT 1].Name;
	}  else{
		System.debug('ERROR OBJECT NOT HANDLED BY CALLOUT');
	}
	return returnValue;
}
/* Function retrieveType : used to retrieve the type of the object
 * to see where to use it
 * @param ObjId : id of the object (quote or fip)
 */
public static String retrievetype(Id objId){
	String returnValue = '';
	String sobjectType = objId.getSObjectType().getDescribe().getName();
	Schema.SObjectType convertType = Schema.getGlobalDescribe().get(sobjectType);
	if (convertType == Quote.sObjectType) {
		returnValue = 'Quote';
	} else if (convertType == FIP_FIP__c.sObjectType) {
		returnValue = 'FIP';
	}  else{
		System.debug('ERROR OBJECT NOT HANDLED BY CALLOUT');
	}
	return returnValue;
}
/* Function sendCreatedFlow : used in CDLTrigger for object FIP
 * it retrieves the FIP document and sends it to the client for signing
 * @param ObjId : id of the object (quote or fip)
 */
@future (callout=true)
global static void sendCreatedFlow(Id objId) {
	String DocName= retrieveName(objId)+'.pdf';
	Boolean temp=false;
	if(retrievetype(objID)=='FIP') {
		List<DocumentsForSignature__c> docs = new List<DocumentsForSignature__c>();
		docs = [SELECT Id, Status__c, FlowId__c, FIP_FIP__c FROM DocumentsForSignature__c WHERE FIP_FIP__c= :objId Limit 1];
		if(docs.isEmpty()==True) {
			temp=false;
		}else{
			temp=true;
			System.debug('NO FIP FILE FOUND');
		}
	}
	If(temp==false){
		HttpRequest req = new HttpRequest();
		HttpResponse res = new HttpResponse();
		Http http = new Http();
		req.setHeader('Content-Type','application/json');
		req.setEndpoint('callout:ConnectiveApi/flows/');
		req.setMethod('POST');
		req.setTimeout(120000);
		String query=FlowWrapper.populatejson(objId);
		System.debug(query);
		FlowWrapper fl=FlowWrapper.deserializejson(query);
		req.setBody(query);
		if(req.getBody()!='') {
			try {
				res = http.send(req);
				System.debug(res.getBody());
				if(res.getStatusCode() == 200) {
					FlowResponse flres=FlowResponse.deserializejson(res.getBody());
					DocumentsForSignature__c doc=new DocumentsForSignature__c();
					doc.Name=fl.DocumentName;
					doc.SigningUrl__c=flres.F2FSigningURL;
					doc.SentTo__c=fl.Signers[0].ExternalReference;
					doc.TECH_ExternalId__c=flres.ExternalReference;
					doc.FIP_FIP__c=flres.ExternalReference;
					doc.document__c=flres.DownloadURL;
					doc.FlowId__c=flres.FlowId;
					insert doc;
					System.debug('OK');
				}else{
					System.debug('Error'+res.getStatusCode());
				}
			} catch(System.CalloutException e) {
				System.debug('Callout error: '+ e);
				System.debug(res.toString());
				System.debug('Error');
			}
		}
		else{
			System.debug('Attachment Null');
		}
	}else{
		System.debug('Attachment exists');
	}
}


/*public static void attachementToFiles(List<Attachment> attachs){

        List<ContentVersion> listContentVersionToInsert = new List<ContentVersion>();
        List<ContentDocumentLink> listContentDocumentLinkToInsert = new List<ContentDocumentLink>();
        Map <Integer, Id> mapContentVersionToAttachementParentId = new Map <Integer, Id>();
        //Insert ContentVersion
        Integer AttachementRank = 0;
        for(Attachment attach : attachs) {
                ContentVersion cVersion = new ContentVersion();
                cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
                cVersion.PathOnClient = attach.Name;//File name with extention
                cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.
                cVersion.Title = attach.Name;//Name of the file
                cVersion.VersionData = attach.Body;//File content

                listContentVersionToInsert.add(cVersion);
                mapContentVersionToAttachementParentId.put( AttachementRank,  attach.ParentId);
                AttachementRank++;
        }

        Insert listContentVersionToInsert;
        Integer ContentVersionRank = 0;
        for(ContentVersion conDocument : listContentVersionToInsert) {
                //Insert ContentDocumentLink
                ContentDocumentLink cDocLink = new ContentDocumentLink();
                cDocLink.ContentDocumentId = conDocument.ContentDocumentId;//Add ContentDocumentId
                cDocLink.LinkedEntityId = mapContentVersionToAttachementParentId.get(ContentVersionRank);//Add attachment parentId(Number Attachement = Number Contentconversion)
                cDocLink.ShareType = 'I';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
                cDocLink.Visibility = 'InternalUsers';//AllUsers, InternalUsers, SharedUsers

                listContentDocumentLinkToInsert.add(cDocLink);
                ContentVersionRank++;
        }


        Insert listContentDocumentLinkToInsert;
   }*/

/* Function sendCreatedFlowWSUnknownSigner : Used in salesforce community for unknown signers to
 * create connective flow and sign documents
 * @param ObjId : id of the object Quote
   @param FirstName : first Name of the unknown signer
   @param LastName :  last Name of the unknown signer
   @param Phone :  Name of the unknown signer
   @param Email :  email of the unknown signer
 */

//Becarefull beetween that : we note have access to files in the apex code when user is on classic mode and
//not have acces to the attachement when he is on Lightning mode. Even if you see fils and attachement on th IHM
@future (callout=true)
global static void sendCreatedFlowWSUnknownSigner(Id objId, String Firstname, String Lastname,String phone, String Email) {
	Attachment att = new Attachment();
	List<Attachment> atts =[SELECT id, Name, body, ContentType,ParentId FROM Attachment WHERE ParentId=:objId LIMIT 1];

	if(atts.size() >  0) {//This means we run in Saleforce Classic(Attachments are not availible in Lightning)
		att = atts[0];
	}

//This means we run in Saleforce Lightning(Attachments are not availible in Lightning)
//So we create temporary attachment using the file
	if(att.id == null) {
		List<ContentDocumentLink> cdls=[SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: objId];
		ContentDocumentLink cdl = new ContentDocumentLink();
		if(cdls.size() >  0) {
			cdl = cdls[0];
		}

		if(cdl.Id != null) {
			ContentVersion cv = [select PathOnClient, ContentLocation, Origin, OwnerId, Title, VersionData, ContentDocumentId from ContentVersion where ContentDocumentId =: cdl.ContentDocumentId];
			att.Name=cv.PathOnClient;
			att.OwnerId=cv.OwnerId;
			att.Body = cv.VersionData;
			att.ParentId = objId;
		}

	}

	Boolean AlreadyExists=false;
	if(att.Name !=null) {
		HttpRequest req = new HttpRequest();
		HttpResponse res = new HttpResponse();
		Http http = new Http();
		req.setHeader('Content-Type','application/json');
		req.setEndpoint('callout:ConnectiveApi/flows/');
		req.setMethod('POST');
		req.setTimeout(120000);
		String query=FlowWrapper.populatejsonUnknownSigner( objId, att, Firstname, Lastname, phone, Email);
		FlowWrapper fl=FlowWrapper.deserializejson(query);
		req.setBody(query);
		if(req.getBody()!='') {
			try {
				res = http.send(req);
				if(res.getStatusCode() == 200) {
					FlowResponse flres=FlowResponse.deserializejson(res.getBody());
					DocumentsForSignature__c doc=new DocumentsForSignature__c();
					Quote q=[Select ID, QuoteCode__c,AccountId, TECH_UnknownSignerInformation__c,TECH_SendForSignature__c FROM Quote Where Id=:objId];
					if(q.TECH_UnknownSignerInformation__c!=null && q.TECH_UnknownSignerInformation__c.contains('=')) {
						try{
							Contact c=new Contact(FirstName=FirstName,LastName=Lastname,Phone=phone,Email=email,
							                      SourceContact__c='Signataire de conventions et devis',Description='Signataire de conventions et devis',AccountId=q.AccountId);
							insert c;
							doc.SentTo__c=c.id;
						}catch(Exception e) {
							System.debug('Exception on inserting new contact'+e);
						}
					}else{
						doc.SentTo__c=q.TECH_UnknownSignerInformation__c;
					}
					doc.Name=fl.DocumentName;
					doc.SigningUrl__c=flres.F2FSigningURL;
					doc.Quote__c=flres.ExternalReference;
					doc.TECH_ExternalId__c=flres.ExternalReference;
					doc.document__c=flres.DownloadURL;
					doc.FlowId__c=flres.FlowId;
					insert doc;
					q.TECH_UnknownSignerInformation__c=null;
					Update q;
				}else{
					System.debug('Callout body'+res.getBody()+' Status Code: '+res.getStatusCode());
				}
			} catch(System.CalloutException e) {
				System.debug('Callout error: '+ e);
				System.debug(res.toString());
			}
		}
	}
}


/* Function retrieveFlowStatus : used to retrieve the status of the signature on buttons for quote and FIP
 * @param ObjId : id of the object (quote or fip)
 */
webservice static String retrieveFlowStatus(Id objId) {
	HttpResponse res = new HttpResponse();
	HttpRequest req = new HttpRequest();
	Http http = new Http();
	DocumentsForSignature__c doc=[SELECT Id, Status__c, FlowId__c,TECH_ExternalId__c,OwnerId,Quote__r.OwnerId , Quote__r.QuoteCode__c, FIP_FIP__r.Name FROM DocumentsForSignature__c WHERE TECH_ExternalId__c= :objId Limit 1];
	String FlowId= doc.FlowId__c;
	req.setHeader('Content-Type','application/json');
	req.setEndpoint('callout:ConnectiveApi/flows/'+FlowId);
	req.setMethod('GET');
	req.setTimeout(120000);
	if(FlowId!=null) {
		try {
			res = http.send(req);
			System.debug(res.getBody());
			FlowStatusResponse flst=FlowStatusResponse.deserializejson(res.getBody());
			System.debug(flst);
			String DocName= retrieveName(flst.ExternalReference);
			// this part adds the document as an attachment in the quote or the FIP
			List<Attachment> atts = [SELECT id,Name FROM Attachment WHERE ParentId = : flst.ExternalReference ORDER BY CreatedDate];
			Boolean temp=false;
			if(atts != null) {
				for(Attachment at: atts) {
					if(at.Name==DocName+' -  Signée.pdf') {
						temp=true;
					}
				}
			}

			List<ContentDocumentLink> files  = [SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLink where LinkedEntityId =:  flst.ExternalReference ];
			if(files != null) {
				for(ContentDocumentLink fi: files) {
					if(fi.ContentDocument.title==DocName+' -  Signée.pdf') {
						temp=true;
					}
				}
			}

			If(flst.Status=='SIGNED'&& temp==false){
				PageReference pageRef = new PageReference(flst.DownloadURL);
				Blob ret;
				if(Test.isRunningTest()) {
					ret =Blob.valueof('UNIT-TEST');
				}else{
					ret = pageRef.getContentAsPDF();
				}

				Attachment Att=new Attachment();
				att.Body = ret;
				att.Name = DocName+' -  Signée.pdf';
				att.ParentId = flst.ExternalReference;
				att.OwnerId= doc.Quote__r.OwnerId;
				insert att;

				List<Attachment> listTemp = new List<Attachment>();

			}else{
				System.debug('Attachment already exists');
			}
			//this part updates the status of the document
			doc.Status__c=flst.Status;
			update doc;
			return 'OK';
		} catch(System.CalloutException e) {
			System.debug('Callout error: '+ e);
			System.debug(res.toString());
			return 'Error';
		}
	}
	else{
		return 'FlowId Null';
	}
}
/* Function retrieveMassFlowStatus : used to retrieve the status of the signature on buttons for quote and FIP
 * @param ObjId : id of the object (quote or fip)
 */
webservice static String retrieveMassFlowStatus(List<DocumentsForSignature__c> docs) {
	String Result='';
	Map<DocumentsForSignature__c,String> DocumentsWithString=new Map<DocumentsForSignature__c,String>();
	List<Id> ParentIds=new List<Id>();
	for(DocumentsForSignature__c doc:docs) {
		ParentIds.add(doc.TECH_ExternalId__c);
		Id externalId=Id.ValueOf(doc.TECH_ExternalId__c);
		String sobjectType = externalId.getSObjectType().getDescribe().getName();
		Schema.SObjectType convertType = Schema.getGlobalDescribe().get(sobjectType);
		if (convertType == Quote.sObjectType) {
			DocumentsWithString.put(doc,doc.Quote__r.QuoteCode__c);
		}else{
			DocumentsWithString.put(doc,doc.FIP_FIP__r.Name);
		}
	}
	List<Attachment> atts = [SELECT id,Name,ParentId FROM Attachment WHERE ParentId IN:ParentIds ORDER BY CreatedDate];
	List<Attachment> AttachmentsToInsert=new List<Attachment>();

	List<ContentDocumentLink> files  = [SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLink where LinkedEntityId IN:ParentIds ];

	List<DocumentsForSignature__c> DocumentsToUpdate=new List<DocumentsForSignature__c>();

	for(DocumentsForSignature__c doc:DocumentsWithString.keyset()) {
		HttpResponse res = new HttpResponse();
		HttpRequest req = new HttpRequest();
		Http http = new Http();
		String FlowId= doc.FlowId__c;
		req.setHeader('Content-Type','application/json');
		req.setEndpoint('callout:ConnectiveApi/flows/'+FlowId);
		req.setMethod('GET');
		req.setTimeout(120000);
		if(FlowId!=null) {
			try {
				res = http.send(req);
				System.debug(res.getBody());
				FlowStatusResponse flst=FlowStatusResponse.deserializejson(res.getBody());
				System.debug(flst);
				String DocName=DocumentsWithString.get(doc);
				// this part adds the document as an attachment in the quote or the FIP
				Boolean temp=false;

			/*	if(atts != null) {
					for(Attachment at: atts) {
						if(at.Name==DocName+' -  Signée.pdf') {
							temp=true;
						}
					}
				} */
				if(files.size() > 0) {
					for(ContentDocumentLink fi: files) {
						if(fi.ContentDocument.title==DocName+' -  Signée.pdf') {
							temp=true;
						}
					}
				}
				If(flst.Status=='SIGNED'&& temp==false){
					PageReference pageRef = new PageReference(flst.DownloadURL);
					Blob ret;
					if(Test.isRunningTest()) {
						ret =Blob.valueof('UNIT-TEST');
					}else{
						ret = pageRef.getContentAsPDF();
					}
					Attachment Att=new Attachment();
					att.Body = ret;
					att.Name = DocName+' -  Signée.pdf';
					att.ParentId = flst.ExternalReference;
					att.OwnerId= doc.Quote__r.OwnerId;
					System.debug(att.Name);
					AttachmentsToInsert.add(att);
				}else{
					System.debug('Attachment already exists');
				}
				//this part updates the status of the document
				doc.Status__c=flst.Status;
				DocumentsToUpdate.add(doc);
				System.debug(doc.Status__c);
				Result= 'OK';
			} catch(System.CalloutException e) {
				System.debug('Callout error: '+ e);
				System.debug(res.toString());
				Result= 'Error';
			}
		}else{
			Result= 'FlowId Null';
		}
	}
	try{
		Set<Id> listId = new Set<Id>();
        Map <String,String> mapAttParentId = new  Map <String,String>();
		if(AttachmentsToInsert.size()>0) {			
			Insert AttachmentsToInsert;
			for (Attachment att : AttachmentsToInsert) {
				mapAttParentId.put(att.Name,att.ParentId);
			}
			//Convert attachments to ContentVersions
			List <ContentVersion> lstConv = converttAttsToContentVers(AttachmentsToInsert);
			insert lstConv;

			for(ContentVersion c :lstConv){
				listId.add(c.Id);
			}

		}		
			
		//Create DocumentLinks
		addDocumentLink([SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id IN :listId],mapAttParentId);

		Update DocumentsToUpdate;

		delete AttachmentsToInsert;
	}catch(System.Exception e) {
		System.debug('Insert error: '+ e);
		Result= 'Error';
	}
	return Result;
}
/* Function retrieveAllFlows : used to retrieve the status of all documents sent for signing
 *   NOT USED FOR THE MOMENT
 */ /*
   webservice static List<DocumentsForSignature__c> retrieveAllFlows() {
   Datetime LastMonth=datetime.now()-30;
   String LastMonthString=LastMonth.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
   HttpResponse res = new HttpResponse();
   HttpRequest req = new HttpRequest();
   Http http = new Http();
   req.setHeader('Content-Type','application/json');
   req.setEndpoint('callout:ConnectiveApi/flows'+'?MaxQuantity=50&createdBeforeDate='+LastMonthString+'&ContinuationToken=');
   req.setMethod('GET');
   List<DocumentsForSignature__c> docs=new List<DocumentsForSignature__c>();
   DocumentsForSignature__c doc=new DocumentsForSignature__c();
   try {
   res = http.send(req);
   System.debug(res.getBody());
   FlowListWrapper flw=FlowListWrapper.deserializejson(res.getBody());
   System.debug(flw.items.size());
   for(Integer j=0;j<flw.items.size()-1;j++){
   if(flw.Items[j].ExternalReference!='Test Reference Manual' && flw.Items[j].ExternalReference!='Test Reference Digital'){
   List<DocumentsForSignature__c> docRetrieved=[SELECT Id, Name, Status__c, SentTo__c, TECH_ExternalId__c, FlowId__c, Document__c, SigningUrl__c, CreatedById FROM DocumentsForSignature__c WHERE TECH_ExternalId__c= :flw.Items[j].ExternalReference];
   System.debug(doc+'documents all of them');
   if(docRetrieved.size()>0 && docRetrieved.size()<2){
   docRetrieved[0].Status__c=flw.Items[j].FlowStatus;
   docs.add(docRetrieved[0]);
   }
   }
   }
   update docs;
   return docs;
   }
   catch(System.CalloutException e) {
   System.debug('Callout error: '+ e);
   System.debug(res.toString());
   return null;
   }
   }*/
/* Function deleteFlow : used to change status of a flow
 * example action: REVOKE ,DELETE
 *  @param FlowId : Id of the document sent to signature in the connective server
 *  @param Action : Action required for that flow
 *  @param SendNot : Send notification to sender and receiver of the document
 */
webservice static void deleteFlow(String FlowId, String Action,Boolean SendNot) {
	HttpResponse res = new HttpResponse();
	HttpRequest req = new HttpRequest();
	Http http = new Http();
	req.setHeader('Content-Type','application/json');
	req.setEndpoint('callout:ConnectiveApi/flows/'+FlowId);
	req.setMethod('PUT');
	if(FlowId!=null) {
		String query=FlowStatusChange.populatejson(FlowId,Action,SendNot);
		FlowStatusChange fs=FlowStatusChange.deserializejson(query);
		req.setBody(query);
		if(req.getBody()!='') {
			try {
				res = http.send(req);
				System.debug(res.getBody());
			}catch(System.CalloutException e) {
				System.debug('Callout error: '+ e);
				System.debug(res.toString());
			}
		}
	}
}
/* Function sendReminderFlow : used to send a reminder for the document sent for signing
 * @param ObjId : id of the object (Quote)
 */
webservice static String sendReminderFlow(String objId) {
	HttpResponse res = new HttpResponse();
	HttpRequest req = new HttpRequest();
	Http http = new Http();
	DocumentsForSignature__c doc=[SELECT Id, Status__c, FlowId__c,TECH_ExternalId__c,OwnerId,Quote__r.OwnerId , Quote__r.QuoteCode__c, FIP_FIP__r.Name FROM DocumentsForSignature__c WHERE Quote__c= :objId Limit 1];
	String FlowId= doc.FlowId__c;
	req.setHeader('Content-Type','application/json');
	req.setHeader('Content-Length', '0');
	req.setEndpoint('callout:ConnectiveApi/flows/'+FlowId+'/reminders');
	req.setMethod('POST');
	try {
		res = http.send(req);
		System.debug(res.getBody());
		if(res.getStatusCode()==200) {
			return 'OK';
		}else{
			return 'ERROR';
		}
	}catch(System.CalloutException e) {
		System.debug('Callout error: '+ e);
		System.debug(res.toString());
		return 'ERROR';
	}
}


public static String sendReminder(SObject obj) {
	System.debug('Start sendReminder----: ');
	HttpResponse res = new HttpResponse();
	HttpRequest req = new HttpRequest();
	Http http = new Http();
	List<DocumentsForSignature__c> docs = new List<DocumentsForSignature__c>();
	String ObjectName = String.valueOf(obj.Id.getSObjectType());
	String FlowId ='';
	if(ObjectName.equals('Quote')) {
		System.debug('sendReminder for Quote----: ');
		docs=[SELECT Id, Status__c, FlowId__c,TECH_ExternalId__c,OwnerId,Quote__r.OwnerId , Quote__r.QuoteCode__c, FIP_FIP__r.Name FROM DocumentsForSignature__c WHERE Quote__c = :obj.Id Limit 1];
	} else if(ObjectName.equals('FIP_FIP__c')) {
		System.debug('sendReminder for FIP_FIP__c----: ');
		docs=[SELECT Id, Status__c, FlowId__c, FIP_FIP__c FROM DocumentsForSignature__c WHERE FIP_FIP__c= :obj.Id Limit 1];
	}

	if(docs.size() > 0) {
		FlowId = docs[0].FlowId__c;
	}

	req.setHeader('Content-Type','application/json');
	req.setHeader('Content-Length', '0');
	req.setEndpoint('callout:ConnectiveApi/flows/'+FlowId+'/reminders');
	req.setMethod('POST');
	try {
		res = http.send(req);
		System.debug(res.getBody());
		if(res.getStatusCode()==200) {
			return 'OK';
		}else{
			return 'ERROR';
		}

	}catch(System.CalloutException e) {
		System.debug('Callout error: '+ e);
		System.debug(res.toString());
		return 'ERROR';
	}
}
/* Function sendReminderFlowFIP :  used to send a reminder for the document sent for signing
 * @param ObjId : id of the object (fip)
 */
webservice static String sendReminderFlowFIP(String objId) {
	HttpResponse res = new HttpResponse();
	HttpRequest req = new HttpRequest();
	Http http = new Http();
	DocumentsForSignature__c doc=[SELECT Id, Status__c, FlowId__c,TECH_ExternalId__c,OwnerId,Quote__r.OwnerId , Quote__r.QuoteCode__c, FIP_FIP__r.Name FROM DocumentsForSignature__c WHERE FIP_FIP__c= :objId Limit 1];
	String FlowId= doc.FlowId__c;
	req.setHeader('Content-Type','application/json');
	req.setHeader('Content-Length', '0');
	req.setEndpoint('callout:ConnectiveApi/flows/'+FlowId+'/reminders');
	req.setMethod('POST');
	try {
		res = http.send(req);
		System.debug(res.getBody());
		if(res.getStatusCode()==200) {
			return 'OK';
		}else{
			return 'ERROR';
		}
	}catch(System.CalloutException e) {
		System.debug('Callout error: '+ e);
		System.debug(res.toString());
		return 'ERROR';
	}
}

public static String sendReminderFIP(String objId) {
	System.debug('sendReminderFIP Start ----: ');
	String FlowId;

	try {
		HttpResponse res = new HttpResponse();
		HttpRequest req = new HttpRequest();
		Http http = new Http();
		DocumentsForSignature__c[] docs = [SELECT Id, Status__c, FlowId__c, FIP_FIP__c FROM DocumentsForSignature__c WHERE FIP_FIP__c= :objId];
		if (docs.size() > 0) {
			FlowId= docs[0].FlowId__c;
			req.setHeader('Content-Type','application/json');
			req.setHeader('Content-Length', '0');
			req.setEndpoint('callout:ConnectiveApi/flows/'+FlowId+'/reminders');
			req.setMethod('POST');
			res = http.send(req);
			if(res.getStatusCode()==200) {
				return 'OK';
			}else{
				return 'ERROR';
			}
		}  else{
			return 'NoDocument';
		}

	}catch(System.CalloutException e) {
		System.debug('Callout error: '+ e);
		return 'ERROR';
	}
}

public static String retrieveStatus(Id objId) {
	String FlowId;
	HttpResponse res = new HttpResponse();
	HttpRequest req = new HttpRequest();
	Http http = new Http();
	DocumentsForSignature__c[] docs = [SELECT Id, Status__c, FlowId__c,  Quote__r.OwnerId, OwnerId FROM DocumentsForSignature__c WHERE TECH_ExternalId__c= :objId];
	if (docs.size() > 0) {
		DocumentsForSignature__c doc = docs[0];
		FlowId= docs[0].FlowId__c;
		req.setHeader('Content-Type','application/json');
		req.setEndpoint('callout:ConnectiveApi/flows/'+FlowId);
		req.setMethod('GET');
		req.setTimeout(120000);
		try {
			res = http.send(req);
			System.debug(res.getBody());
			FlowStatusResponse flst=FlowStatusResponse.deserializejson(res.getBody());
			System.debug(flst);
			String DocName= retrieveName(flst.ExternalReference);
			// this part adds the document as an attachment in the quote or the FIP
			List<Attachment> atts = [SELECT id,Name, OwnerId, ParentId FROM Attachment WHERE ParentId = : flst.ExternalReference ORDER BY CreatedDate];
			List<ContentDocumentLink> files  = [SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLink where LinkedEntityId  = : flst.ExternalReference  ];

			Boolean temp=false;
			Boolean isNoFile=false;
			Attachment att=new Attachment();

			//Attacchments not used in LEX
			/*if(atts != null) {
				for(Attachment at: atts) {
					if(at.Name==DocName+' -  Signée.pdf') {
						temp=true;
					}
				}
			} */

			System.debug('files---- ' +files.size());
			if(files.size() > 0) {
				for(ContentDocumentLink fi: files) {
					if(fi.ContentDocument.title==DocName+' -  Signée.pdf') {
						temp=true;
					}
				}
			}else {
				isNoFile = true;
			}

			If(flst.Status=='SIGNED'&& temp==false){
				PageReference pageRef = new PageReference(flst.DownloadURL);
				Blob ret;
				if(Test.isRunningTest()) {
					ret =Blob.valueof('UNIT-TEST');
				}else{
					ret = pageRef.getContentAsPDF();
				}

			//attachment not used in LEX	
				att.Body = ret;
				att.Name = DocName+' -  Signée.pdf';
				att.ParentId = flst.ExternalReference;
				//att.OwnerId= doc.OwnerId;
				insert att;
				//
				converttAttToFIle(att);
				delete att;

			}else{				
				System.debug('Attachment already exists');
				
			}
			//this part updates the status of the document
			doc.Status__c=flst.Status;
			update doc;
			return 'OK';
		} catch(System.CalloutException e) {
			System.debug('Callout error: '+ e);
			System.debug(res.toString());
			return 'Error';
		}
	}
	else{
		return 'FlowIdNull';
	}
}

public static void converttAttToFIle(Attachment attach ) {
//Insert ContentVersion
	ContentVersion cVersion = new ContentVersion();
	cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
	cVersion.PathOnClient = attach.Name;//File name with extention
	cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.
	cVersion.OwnerId = userinfo.getUserId();//Owner of the file
	cVersion.Title = attach.Name;//Name of the file
	cVersion.VersionData = attach.Body;//File content
	cVersion.firstPublishLocationId= userinfo.getUserId();
	Insert cVersion;

//After saved the Content Verison, get the ContentDocumentId
	Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;

//Insert ContentDocumentLink
	ContentDocumentLink cDocLink = new ContentDocumentLink();
	cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
	cDocLink.LinkedEntityId = attach.ParentId;//Add attachment parentId
	cDocLink.ShareType = 'I';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
	cDocLink.Visibility = 'InternalUsers';//AllUsers, InternalUsers, SharedUsers
	Insert cDocLink;
}

public static List<ContentVersion> converttAttsToContentVers(List<Attachment> attachs ) {
	List<ContentVersion> lstConv = new List<ContentVersion>(); 
	//Insert ContentVersion
	for(Attachment attach : attachs){
		ContentVersion cVersion = new ContentVersion();
		cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
		cVersion.PathOnClient = attach.Name;//File name with extention
		cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.
		cVersion.OwnerId =userinfo.getUserId();//Owner of the file
		cVersion.Title = attach.Name;//Name of the file
		cVersion.VersionData = attach.Body;//File content
		cVersion.firstPublishLocationId= userinfo.getUserId();
		lstConv.add(cVersion);
	}
	return lstConv;

}

global static void addDocumentLink(List <ContentVersion> lstCvs,   Map <String,String> mapAttParentId){
    
	List <ContentDocumentLink> lstLinkDoc = new List <ContentDocumentLink>();	
	for(ContentVersion c : lstCvs) {		
		String TECH_ExternalId = mapAttParentId.get(c.Title);
		ContentDocumentLink contDocLink = new ContentDocumentLink(ContentDocumentId = c.ContentDocumentId, LinkedEntityId = TECH_ExternalId, ShareType = 'V' );
		lstLinkDoc.add(contDocLink);
	}

	if(lstLinkDoc.size()>0) {
		insert lstLinkDoc;

	}

}

}