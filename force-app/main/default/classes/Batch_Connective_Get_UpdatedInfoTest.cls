/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : Batch_Connective_Get_Document
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : Batch_Connective_Get_Document
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 13-08-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
@isTest
public class Batch_Connective_Get_UpdatedInfoTest {
    @testSetup 
    static void setup() {

        Account a;
        Contact c;
        Quote q;
        Opportunity opp;
        
        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        q = new Quote();
        //q.id='0Q09E0000000AhESAU';
        q.OpportunityId = opp.ID;
        q.Name = opp.Name;
        q.DateDevis__c = Date.today();
        q.QuoteCode__c=opp.Name;
        q.ContactId = opp.ContactName__c;
        q.Email = opp.ContactName__r.Email;
        q.Phone = opp.ContactName__r.Phone;
        q.Fax = opp.ContactName__r.Fax;
        q.BillingStreet = opp.Account.BillingStreet;
        q.BillingCity = opp.Account.BillingCity;
        q.BillingState = opp.Account.BillingState;
        q.BillingPostalCode = opp.Account.BillingPostalCode;
        q.BillingCountry = opp.Account.BillingCountry; 
        insert q;

        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =Blob.valueOf('Unit Test Attachment Body');          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=q.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;

        Package__c pkg = new Package__c(
            ConnectivePackageId__c='3bb6a2d1-35db-4a3a-a434-868c01bcca9d',
            ConnectivePackageStatus__c='Pending'
        );
        insert pkg;

        DocumentsForSignature__c doc=new DocumentsForSignature__c();
        doc.Name= 'titre';
        doc.SigningUrl__c='';
        doc.SentTo__c=c.Id;
        doc.TECH_ExternalId__c=cv.Id;
        doc.ConnectiveDocumentId__c = cv.Id;
        doc.Status__c = 'PENDING';
        doc.Quote__c=q.Id;
        doc.Package__c = pkg.Id;
        insert doc;
        
        FIP_FIP__c fip = new FIP_FIP__c();
        fip.Collector__c = a.Id;
        insert fip;
        
        DocumentsForSignature__c doc2=new DocumentsForSignature__c();
        doc2.Name= 'titre';
        doc2.SigningUrl__c='';
        doc2.SentTo__c=c.Id;
        doc2.TECH_ExternalId__c=cv.Id;
        doc2.ConnectiveDocumentId__c = cv.Id;
        doc2.Status__c = 'PENDING';
        doc2.FIP_FIP__c=fip.Id;
        doc2.Package__c = pkg.Id;
        insert doc2;
    }
    static testmethod void testBatch() {        
        Test.startTest();
        List<DocumentsForSignature__c> docslist = [SELECT Id FROM DocumentsForSignature__c];
        Test.setMock(HttpCalloutMock.class, new ConnectiveMockGetStatus()); 
        Batch_Connective_Get_UpdatedInfo cbt = new Batch_Connective_Get_UpdatedInfo(docslist);
        System.enqueueJob(cbt);
        Test.stopTest();
    }
    
    @isTest
    static void manageGetUpdateInfoTest(){
        Test.startTest();
        List<DocumentsForSignature__c> docslist = [SELECT Id, Quote__c, FIP_FIP__c FROM DocumentsForSignature__c];
        Test.setMock(HttpCalloutMock.class, new ConnectiveMockGetStatus()); 
        Batch_Connective_Get_UpdatedInfo cbt = new Batch_Connective_Get_UpdatedInfo(docslist);
        cbt.manageGetUpdateInfo(docslist[0]);
        cbt.manageGetUpdateInfo(docslist[1]);
        Test.stopTest();
    }
}