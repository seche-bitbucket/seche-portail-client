public with sharing class QUO_TraiteTransporteController {
    public List<quotelineWrapper> QuoteLineItemList {get;set;}
    public QuoteLineItem Transport{get;set;}
    public Quote Quote {get;set;}
    public Double PriceOfTransport{get;set;}
    public ID quoteID;
    public QUO_TraiteTransporteController( ApexPages.StandardSetController stdController ){
        quoteID = apexpages.currentpage().getparameters().get('id');
        this.QuoteLineItemList=new List<quotelineWrapper>();
        if(stdController.getSelected()!=null){
            if(stdController.getSelected().size()!=0){
                List<QuoteLineItem> QuoteLineItems=[SELECT ID, Product2Id,Nature__c,TreatmentPrice__c, QuoteId, UnitPrice,Unit__c FROM QuoteLineItem WHERE ID IN:stdController.getSelected()];
                for(QuoteLineItem QL:QuoteLineItems){
                    QuoteLineWrapper QLW=new quotelineWrapper();
                    QLW.QLI=QL;
                    if(QL.TreatmentPrice__c!=null){
                        QLW.QLI.UnitPrice=QL.TreatmentPrice__c;
                    }
                    QLW.isTransport=false;
                    this.QuoteLineItemList.add(QLW);
                }
                if(This.QuoteLineItemList.size()>0){
                    Quote q=[SELECT Name From Quote Where Id=:QuoteLineItemList[0].QLI.QuoteId LIMIT 1];
                    this.Quote=q;
                }
                this.transport=new QuoteLineItem();
                this.transport.Unit__c=QuoteLineItems[0].Unit__c;
                QuoteLineWrapper QLW=new quotelineWrapper();
                QLW.QLI=transport;
                QLW.isTransport=true;
                this.QuoteLineItemList.add(QLW);
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Veuillez sélectionner une ligne de devis avant de procéder !'));   
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Veuillez sélectionner une ligne de devis avant de procéder !'));   
        }
    }
    public PageReference returnToPrevious(){
        try {
            PageReference returnPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL')); 
            returnPage.setRedirect(true);
            return returnPage;
        } catch (Exception e) {
            PageReference returnPage = new PageReference('/' + quoteID); 
            returnPage.setRedirect(true);
            return returnPage;
        }
    }
    public class QuoteLineWrapper{
        public QuoteLineItem QLI{get;set;}
        public Boolean IsTransport {get;set;}
    }
    public PageReference save(){
        List<QuoteLineItem> ListToUpdate=new List<QuoteLineItem>();
        for(QuoteLineWrapper QLW:QuoteLineItemList){
            if(QLW.IsTransport){
                This.PriceOfTransport=QLW.QLI.UnitPrice;
            }
        }
        for(QuoteLineWrapper QLW:QuoteLineItemList){
            if(!QLW.IsTransport){
                QLW.QLI.TreatmentPrice__c=QLW.QLI.UnitPrice;
                QLW.QLI.TransportPrice__c=this.PriceOfTransport;
                QLW.QLI.UnitPrice =QLW.QLI.UnitPrice+This.PriceOfTransport;
                QLW.QLI.Nature__c='TT';
                ListToUpdate.add(QLW.QLI);
            }
        }
        Update ListToUpdate;
        String IdtoReturn=QuoteLineItemList[0].QLI.QuoteId;
        PageReference page = new PageReference('/'+IdtoReturn); 
        page.setRedirect(true);
        return page;
    }
    public PageReference cancelProcess(){
        String IdtoReturn=QuoteLineItemList[0].QLI.QuoteId;
        PageReference page = new PageReference('/'+IdtoReturn); 
        page.setRedirect(true);
        return page;
    }
}