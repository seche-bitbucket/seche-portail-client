@IsTest
public class SendApprovalEmailBatchTest {
    @isTest
    static  User createUserTest(){
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Administrateur système' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname');
        insert currentUser ;  
        return currentUser ;        
    }
    
    @IsTest 
    Static void ApprovalRecapEmailTest ()
    {
        List<Quote> quoList = new list<Quote>();
        User currentUser = createUserTest();
        System.RunAs(currentUser){
            
            Account a = new Account();
            a.Name = 'testAccName';
            a.CustomerNature__c = 'Administration';
            a.Industry = 'Trader';
            a.CurrencyIsoCode = 'EUR';
            a.Producteur__c = false;
            a.BillingPostalCode = '01000';
            a.NAF_Number__c = '1234A';
            insert a;
            
            Contact c = new Contact();
            c.LastName = 'testName';
            c.AccountId = a.Id;
            //c.Salesman__c = director.Id;
            c.Email = 'aaa@groupe-seche.com';
            c.Phone = '03 00 00 00 00';
            insert c;
            
            Opportunity opp1 = new Opportunity();
            opp1.Name = 'test';
            //opp.Salesman__c = director.Id;
            opp1.StageName = 'Contractualisation';
            opp1.CloseDate = System.today().addDays(10);
            opp1.AccountId = a.Id;
            opp1.TonnageN__c = 0;
            opp1.Filiale__c = 'Tredi Hombourg';
            opp1.Filiere__c = 'Physico';
            opp1.Amount = 100000;
            opp1.ContactName__c = c.Id;
            insert opp1;
            
            Quote quo1 = new Quote();
            quo1.OpportunityId = opp1.ID;
            quo1.name = 'Mon Devis Test';
            insert quo1;
            quoList.add(quo1);
            
            Opportunity opp2 = new Opportunity();
            opp2.Name = 'test';
            //opp.Salesman__c = director.Id;
            opp2.StageName = 'Contractualisation';
            opp2.CloseDate = System.today().addDays(10);
            opp2.AccountId = a.Id;
            opp2.TonnageN__c = 0;
            opp2.Filiale__c = 'Tredi Hombourg';
            opp2.Filiere__c = 'Physico';
            opp2.Amount = 120000;
            opp2.ContactName__c = c.Id;
            insert opp2;
            
            Quote quo2 = new Quote();
            quo2.OpportunityId = opp2.ID;
            quo2.name = 'Mon Devis Test2';
            insert quo2;
            quoList.add(quo2);
            Approval.ProcessResult result ;
            for (integer i =0;i<quoList.size();i++)
            {
                Quote q = quoList[i];
                ProcessDefinition pd = [select id from ProcessDefinition where developerName = 'QUO_AP_Tredi_Hombourg'];
                
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setComments('Submitting request for approval.');
                req1.setObjectId(q.id);
                req1.setSubmitterId(currentUser.Id); 
                req1.setProcessDefinitionNameOrId('QUO_AP_Tredi_Hombourg');
                req1.setSkipEntryCriteria(true);
                result = Approval.process(req1);
            }
            
            List<processinstanceworkitem> piwiList = [select ActorId from processinstanceworkitem where processInstance.TargetObjectId IN:quoList];
            List<processinstanceworkitem> piwiListUpd = new List<processinstanceworkitem>();
            for (processinstanceworkitem piwi : piwiList )
            {
            	piwi.actorid = currentuser.id;
                piwiListUpd.add(piwi);
            }
            update piwiListUpd;
            
            SendApprovalEmailBatch batch = new SendApprovalEmailBatch();
			database.executebatch(batch);
            
            System.assert(result.isSuccess());    
        }
    }
}