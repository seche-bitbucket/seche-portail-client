/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Mohammed Amine IDRISSI TAGHKI <mohammed-amine.idrissi-taghki@capgemini.com>
 * @version        1.0
 * @created        2022-02-21
 * @systemLayer    Extend Base Model
 *
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Desciption: Child class extends base model
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */
public without sharing class CB_OpportunityLineItem extends PolyMorphic_BaseClass {

    override
    public virtual Object VGetRecord(String recordId){

        System.debug('Alex debug 1');
        
        
        String field = 'ID';
        String query = '';
        List<String> f = new List<String>(Schema.getGlobalDescribe().get('OpportunityLineItem').getDescribe().fields.getMap().keySet());
        
        if(recordId.length() > 18) {
            System.debug('Alex debug 2');
            List<Form__c> form = [SELECT Id FROM Form__c WHERE GUID__c = :recordId];
            List<Attestation__c> attLst = [SELECT OpportunityLineItem__c FROM Attestation__c WHERE Form__c = :form];
            List<Id> oliIdsList = new List<Id>();
            for(Attestation__c att : attLst) {
                oliIdsList.add(att.OpportunityLineItem__c);
            }
            query = 'SELECT ' + String.join(f, ', ') + ' FROM OpportunityLineItem WHERE ' + field + ' IN :oliIdsList';
        } else {
            if(Id.valueOf(recordId).getSobjectType() == Opportunity.SObjectType) {
                field = 'OpportunityId';
            }
            query = 'SELECT '+String.join(f, ', ')+' FROM OpportunityLineItem WHERE '+field+' = :recordId AND Sector__c = \'Stockage DND\' AND Nature__c = \'Traitement\'';
        }


        List<SObject> rc = Database.query(query);

        System.debug('SELECT '+String.join(f, ', ')+' FROM OpportunityLineItem WHERE '+field+' = :recordId');
        System.debug(recordId);
        
        return rc;
    }
    
}