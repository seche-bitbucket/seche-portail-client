public class OpportunityTriggerUtil {
    
    //Get All Existing "Equipe" and map the with the subsidiary as ID
    public static Map<String, Equipe__c> getOpportunityTeams(List<Opportunity> opps){
        Map<String, Equipe__c> results = new Map<String, Equipe__c>();
        List<String> subs = new List<String>();
        Set<String> subsSet = new Set<String>();
        
        for(Opportunity opp : opps){
            subs.add(opp.Filiere__c);
        }
        //remove Duplicates
        subsSet.addAll(subs);
        subs.clear();
        subs.addAll(subsSet);
        
        for(Equipe__c e : [Select Id, Directeur__c, Assistant__c, Exceptions_Site_Filiale__c, Filiere__c From Equipe__c Where Filiere__c in : subs]){
            results.put(e.Filiere__c, e);
        }   
        
        return results;       
        
    }
    
    //Retrive all OpportunityTeamMember linked to Teams
    public static Map<ID, List<OpportunityTeamMember>> getAllOpportunityTeams(List<Opportunity> opps){
        Map<ID, List<OpportunityTeamMember>> results = new Map<ID, List<OpportunityTeamMember>>();
        
        
        //Get all OTM with all Opp ID
        for(OpportunityTeamMember otm : [Select Id, UserId, OpportunityId From OpportunityTeamMember Where OpportunityId in : opps AND (TeamMemberRole = 'Directeur commercial' OR TeamMemberRole = 'Assistant(e)')]){ 
            List<OpportunityTeamMember> otmTmp = new  List<OpportunityTeamMember>();
            if(results.containsKey(otm.OpportunityId)){
                otmTmp = results.get(otm.OpportunityId);
            }
            otmTmp.add(otm);
            results.put(otm.OpportunityId, otmTmp);
        }   
        return results;       
        
    }
    
    public static List<OpportunityTeamMember> getOpportunityTeamToInsert(List<Opportunity> opps, Map<String, Equipe__c> equipes){
        Equipe__c e = new Equipe__c();
        Boolean isException = false;
        
        List<OpportunityTeamMember> otmToinsert =  new List<OpportunityTeamMember>();
        
        for(Opportunity opp : opps){
            e = equipes.get(opp.Filiere__c);
            
            if(e != null){
                if(e.Exceptions_Site_Filiale__c != null ){
                    if(e.Exceptions_Site_Filiale__c.contains(opp.Filiale__c)){
                        isException = true;
                    }
                }
                if(!isException){
                    //Create the director in the team
                    OpportunityTeamMember dir = new OpportunityTeamMember();
                    dir.UserId = e.Directeur__c;
                    dir.TeamMemberRole = 'Directeur Commercial ';
                    dir.OpportunityId = opp.id;
                    dir.OpportunityAccessLevel = 'Edit';
                    dir.Tech_Equipe__c = e.Id;
                    otmToinsert.add(dir);
                    
                    //Create the assistante in the team
                    if(e.Assistant__c != null ){
                        OpportunityTeamMember assistante = new OpportunityTeamMember();
                        assistante.UserId = e.Assistant__c;
                        assistante.TeamMemberRole = 'Assistant(e)';
                        assistante.OpportunityId = opp.id;
                        assistante.OpportunityAccessLevel = 'Edit';
                        assistante.Tech_Equipe__c = e.Id;
                        otmToinsert.add(assistante);
                        
                    }
                }
            }
        } 
        
        return otmToinsert;
        
    }
    
    
    public static List<OpportunityTeamMember> getOpportunityTeamToDelete(List<Opportunity> opps, Map<ID, List<OpportunityTeamMember>> otms){
        
        List<OpportunityTeamMember> otmToDelete =  new List<OpportunityTeamMember>();
        
        for(Opportunity opp : opps){
            
            //Retrive the old team
            List<OpportunityTeamMember> otmlist = new List<OpportunityTeamMember>();
            otmlist = otms.get(opp.ID);
            //Delete the old team
            if(otmlist != null)
                otmToDelete.addAll(otmlist);            
        } 
        
        return otmToDelete;
    }
    
    
    //Change the Recordtype of an opportunity to Interrediary when the Opportunity mother field is filled
    public static void manageOpportunityRecordType(List<Opportunity> opps, List<RecordType> rts){
        
        for(Opportunity opp : opps){    
            if(opp.Opportunit_mere__c !=  null){
                //Load RT with DeveloperName and ID as Key in map
                Map<String,RecordType> oppRTDeveloperName = new Map<String,RecordType>();
                Map<String,RecordType> oppRTID = new Map<String,RecordType>();
                for(RecordType rt : rts){
                    
                    oppRTDeveloperName.put(rt.DeveloperName, rt);
                    oppRTID.put(rt.ID, rt);
                }               
                
                RecordType newRt = new RecordType();
                String rt = oppRTID.get(opp.RecordTypeId).DeveloperName;
                //Get the correct new one
                if(rt =='OPP_01_OffreSpeichimStandard'){
                    opp.RecordTypeId = oppRTDeveloperName.get('OPP_01_OffreSpeichimIntermediaire').ID;
                }else if(rt =='OPP_02_OffresOGGDStandard'){
                    opp.RecordTypeId = oppRTDeveloperName.get('OPP_02_OffresOGGDIntermediaire').ID;
                }else if(rt =='OPP_03_AppelOffrePubliqueStandard'){
                    opp.RecordTypeId = oppRTDeveloperName.get('OPP_03_AppelOffrePubliqueIntermediaire').ID;
                }else if(rt =='OPP_04_OffrePrestationSiteStandard'){
                    opp.RecordTypeId = oppRTDeveloperName.get('OPP_04_OffrePrestationSiteIntermediaire').ID;
                }else if(rt =='OPP_05_AutreOffreSecteurPublicStandard'){
                    opp.RecordTypeId = oppRTDeveloperName.get('OPP_05_AutreOffreSecteurPublicIntermediaire').ID;
                }else if(rt =='OPP_06_AutreOffreSecteurPriveStandard'){
                    opp.RecordTypeId = oppRTDeveloperName.get('OPP_06_AutreOffreSecteurPriveIntermediaire').ID;
                }   
            }
        }       
    }
    
    public static List<Opportunity> manageOppMotherWithInitialization(List<Opportunity> opps, Map<Id, Opportunity> oppsData){
        List<Opportunity> oppChapeauToUpdate = new List<Opportunity>();
        
        for(Opportunity opp : opps){
            
            //Update Opp Mere Amount with its lowest Daughter Opportunity Amount while Mother or daughter aren't won or lost
            if( opp.Opportunit_mere__c != null && (opp.StageName != 'Clôturée / Gagnée' || opp.StageName != 'Clôturée / Perdue' || opp.StageName != 'Abandonnée')){
                Opportunity o = oppsData.get(opp.ID);
                if(o != null){
                    //If the mother isn't closed and its amount is greater than its daughter then, copy the daughter's amount in the mother
                    if((o.Opportunit_mere__r.StageName != 'Clôturée / Gagnée' &&
                        o.Opportunit_mere__r.StageName != 'Clôturée / Perdue' && 
                        o.Opportunit_mere__r.StageName != 'Abandonnée') && 
                       (o.Opportunit_mere__r.Amount > opp.Amount || 
                        o.Opportunit_mere__r.Amount == null)
                       && o.Opportunit_mere__c != null){
                           Opportunity oChap = new Opportunity(ID = opp.Opportunit_mere__c);
                           oChap.Amount = opp.Amount;
                           if(opp.TonnageN__c != null){
                               oChap.TonnageN__c = 	opp.TonnageN__c;
                           }
                           oppChapeauToUpdate.add(oChap);          
                       }
                }
            }
        }
        //Remove duplicates
        Set<sobject> myset = new Set<sobject>();
        List<sobject> result = new List<sobject>();
        for (sobject s : oppChapeauToUpdate) {
            if (myset.add(s)) {
                result.add(s);
            }
        }
        return oppChapeauToUpdate;
    }
    
    public static List<Opportunity> manageOppMotherWithWonDaughter(List<Opportunity> opps, Map<Id, Opportunity> oppsData){
        
        List<Opportunity> oppChapeauToUpdate = new List<Opportunity>();
        oppChapeauToUpdate.addAll(manageOppMotherWithInitialization(opps, oppsData));
        System.debug('Opps');
        for(Opportunity opp : opps)
            if(opp.Opportunit_mere__c != null && opp.StageName == 'Clôturée / Gagnée'  && !opp.TECH_AmoutAddedToMother__c){
                Opportunity o = oppsData.get(opp.ID);

               
                If(o != null && o.Opportunit_mere__r.StageName != null && o.Opportunit_mere__r.Amount != null){
                    Opportunity oChap = new Opportunity(ID = opp.Opportunit_mere__c);
                    System.debug('oChap'+oChap);
                    if(o.Opportunit_mere__r.StageName == 'Clôturée / Gagnée'){
                        oChap.Amount = o.Opportunit_mere__r.Amount + opp.Amount;
                        oChap.TonnageN__c = o.Opportunit_mere__r.TonnageN__c + opp.TonnageN__c;
                    }else{
                        oChap.Amount = opp.Amount;
                        oChap.TonnageN__c = opp.TonnageN__c;
                    }
                    opp.TECH_AmoutAddedToMother__c = true;
                    oppChapeauToUpdate.add(oChap);
                }
                
            }
        //Remove duplicates
        Set<sobject> myset = new Set<sobject>();
        List<sobject> result = new List<sobject>();
        for (sobject s : oppChapeauToUpdate) {
            if (myset.add(s)) {
                result.add(s);
            }
        }
        return oppChapeauToUpdate;
    }
    
    public static void manageSubOppName(List<Opportunity> opps){
        for(Opportunity opp : opps){
            String tmpID =  opp.RecordTypeId;
            if(tmpID.startsWith('01258000000Ab0XAAS')){
                //Override opportunity name for Sub Opportunity
                Boolean startWithST = false;
                
                // Instatiate Pattern that match ST/ or ST- or ST - or ST _ ....
                Pattern startWithST1 = Pattern.compile('ST\\W');
                Pattern startWithST2 = Pattern.compile('ST\\W\\W');
                
                // Match with the opportunity name
                Matcher hasST1 = startWithST1.matcher((opp.Name).toUpperCase());
                Matcher hasST2 = startWithST2.matcher((opp.Name).toUpperCase());
                
                //If nothing match or the name start by the filiale St Vulbas
                //Add 'ST -' in front of the name
                if((!hasST1.find() && !hasST2.find()) || ((opp.Name).toUpperCase()).startsWith('ST VULBAS')){
                    opp.name = 'ST - ' + opp.name;
                }
            }
        }
    }
    
    public static List<EntitySubscription> manageUserSubscriptions(List<Opportunity> opps){
        List<EntitySubscription> ess = new List<EntitySubscription>();
        for(Opportunity opp : opps){
            EntitySubscription esCreator = new EntitySubscription();
            esCreator.ParentId = opp.ID;
            esCreator.SubscriberId = opp.CreatedById;
            ess.add(esCreator);
            if(opp.CreatedById != opp.Salesman__c && opp.salesman__c != null){
                
                EntitySubscription esOwner = new EntitySubscription();
                esOwner.ParentId = opp.ID;
                esOwner.SubscriberId = opp.Salesman__c;
                ess.add(esOwner);
            }
        }
        return ess;
    }
    
    
    public static List<EntitySubscription> manageUserToUnsubscribe(List<Opportunity> opps){
        List<EntitySubscription> ess = new List<EntitySubscription>();
        Set<Id> oppToUnsubscribe = new Set<ID>();
        for(Opportunity opp : opps){
            if(opp.StageName == 'Clôturée / Gagnée' || opp.StageName == 'Clôturée / Perdue' || opp.StageName == 'Abandonnée' || opp.StageName == 'Non répondu'){
                oppToUnsubscribe.add(opp.ID);
            }
        }
        ess = [Select ID From EntitySubscription Where ParentID in: oppToUnsubscribe LIMIT 1000];
        return ess;
    }
    
    /*public static Boolean checkUserCreation(List<Opportunity> opps, Map<ID, Opportunity> oldOppsMap){
        
        if(CheckRecursive.runforRights()){
            User usr = [Select ID, profileID, SubsidiaryCreationRight__c From User Where Id = : UserInfo.getUserId()];
            
            for(Opportunity opp : opps){
                
                //Check for Assistante and Responsable Co profile
                
                if((usr.ProfileId == Label.Prf_AssitanteCo || usr.ProfileId == Label.Prf_ResponsableCo) && opp.RecordTypeID != Label.OPP_RT_IntraGroupe){
                    List<GroupMember> grpMember = [Select ID, userOrGroupID  From GroupMember Where GroupID = : [Select ID, Name From Group Where name  LIKE :Opp.Filiale__c+'%'] AND userOrGroupID =: UserInfo.getUserId() ];
                    
                    if(Trigger.isInsert && opp.RecordTypeID != Label.OPP_RT_IntraGroupe ){
                        if(grpMember.size() == 0){
                            opp.addError('Vous ne pouvez pas créer des opportunités pour des filiales auxquelles vous n\'êtes pas rattaché');
                        }
                    }else if(Trigger.isUpdate && opp.RecordTypeID != Label.OPP_RT_IntraGroupe){
                        if( grpMember.size() == 0 && oldOppsMap.get( opp.ID ).Filiale__c != oldOppsMap.get( opp.ID ).Filiale__c ){
                            opp.addError('Vous ne pouvez pas créer des opportunités pour des filiales auxquelles vous n\'êtes pas rattaché');
                        }
                    }
                    
                }else if(usr.ProfileId == Label.Prf_ChargeAffaire  && opp.RecordTypeID != Label.OPP_RT_IntraGroupe){
                    Boolean isAuthorized = false;
                    
                    List<String> lstFiliales = usr.SubsidiaryCreationRight__c.split(';');
                    
                    for(String filiale : lstFiliales){
                        if(Opp.Filiale__c == filiale){
                            isAuthorized = true;
                            break;
                        }
                    }
                    
                    if(!isAuthorized)
                        opp.addError('Vous ne pouvez pas créer des opportunités pour des filiales auxquelles vous n\'êtes pas rattaché');
                }
            }
        }
        
        return true;
    }*/

   
}