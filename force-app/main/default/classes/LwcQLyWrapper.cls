/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Baïla TOURE <baila.toure@capgemini.com>
 * @version        1.0
 * @created        2020-11-13
 * @systemLayer    Manager
 *
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Desciption: we use this class to parse the QLY informations
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

public with sharing class LwcQLyWrapper {

    public class Form {
        public String formId;
        public String name;
        public String address;
        public String companyId;
        public String companyPhone;
        public String companyAddressText;
        public String removalAddressText;
        public String contactId;
        public String phone;
        public String email;
        public String company;
        public String haveCollectThisYear;
        public String wantChangeAddress;
        public String isSameAddressForWaste; 
        public String priceBookId;
        public List<Day> days;
        public List<String> selectedDays;
        public String comment;
        public String isAccepted; 
        public String isNotify; 
        public String firstCollect;
        public String secondCollect;  
        public List<LwcQLyWrapper.Service> services; 
        public List<LwcQLyWrapper.Waste> wastes; 
        public List<LwcQLyWrapper.Container> containers; 
        
    }   
  
    public class Waste {
        public String id;       
        public String productId;
        public String name;       
        public List<WasteItem> rows;     
        public List<file> files;                 
    }    
    
    public class WasteItem {   
        public String wasteId;    
        public String containerId;   
        public Integer index;
        public String description;
        public String packaging;
        public String nbrPackaging;
        public String quantity;
        public Double unitPrice;
        public Boolean addContainer;
        public String numbContainer;
        public String originTreatmentService;
        public String peroxyName;
        public String classification; 
        public String peroxydeQuantity;
        public String photoQuantity;
        public String photoListQuantity;
        public List<file> files;
        public String adrIcon;
        public String clpIcon;
        public String features;
        public Boolean isSelected;
        public String displayStyle;   
        }

         public class file {
           public String fileId;
           public String filename;
           public String base64;
           public String recordId;
         }
  
    public class Service {       
        public String id; 
        public String productId;
        public String name;
        public Double unitPrice;
        public String serviceIcon;
        public Boolean isSelected;                 
    } 

 
    public class Container {       
        public String id;
        public String containerId;
        public String productId;
        public String name;
        public Double unitPrice;
        public String numberContainer;
        public Boolean isSelected;        
        public Boolean addContainer;
        public Boolean havePlug;
        public String numberPlug;                    
    }      

    public class Day {
        public Integer rank;     
        public String name;
        public Morning morning = new Morning('Matin',true);
        public Afternoon afternoon = new Afternoon('Après-midi', true);
    }
    
    public class Morning {
        public String morning ;   
        public Boolean selected;
    
    //contructeur
    public Morning(String morning, Boolean selected) {
          morning =  morning;    
          selected = selected;
        }
    }
    
    
    public class Afternoon {
        public String morning;    
        public Boolean selected;
    
        //contructeur
        public Afternoon(String afternoon , Boolean selected) {
          afternoon = afternoon;    
          selected = selected;
        }
    }
}