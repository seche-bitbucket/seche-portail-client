public with sharing class VFC_SendReminderForQuoteController {
ApexPages.StandardSetController stdSetController;
private String quoteId;
private Quote currentQuote;
Boolean isNoSelectedOpp;

public Boolean getIsNoSelectedOpp()
{
	return isNoSelectedOpp;
}

Boolean isTestRunnig;//Used just when running class test for process else part in the init() method

public Boolean getIsTestRunnig()
{
	return isTestRunnig;
}

public void setIsTestRunnig(Boolean value)
{
	this.isTestRunnig = value;
}

public VFC_SendReminderForQuoteController(ApexPages.StandardSetController controller) {
	System.debug('Start VFC_SendReminderForQuoteController----: ');
	quoteId = ApexPages.currentPage().getParameters().get('id');
	this.currentQuote = [SELECT Id, Name,NbSendReminder__c,Opportunity.Filiale__c,Account.Name,ContactId FROM Quote WHERE Id = :quoteId];
	stdSetController = controller;
	isNoSelectedOpp = false;
	isTestRunnig = false;
}

public pageReference init(){	
	if(stdSetController.getRecords().Size()>0 && isTestRunnig == false ){
		String result = '';
		//Integer NextYear = System.today().year()+1;
		Integer NextYear = 2021;
		result = ConnectiveApiManager.sendRedminder(quoteId);
		if( result=='OK' || Test.isRunningTest()) {
			this.currentQuote.NbSendReminder__c = this.currentQuote.NbSendReminder__c+1;
			//update this.currentQuote;
			updateLockedRecord updateLockedRecord = new updateLockedRecord();
			updateLockedRecord.updateRecord(this.currentQuote);
			String subject = 'RELANCE : Convention '+NextYear+' - '+this.currentQuote.Opportunity.Filiale__c+' - '+this.currentQuote.Account.Name;
			Task t = LC_EmailSend_AugTarifCtrl.createEmailActivity(subject,'Relance envoyee au client',this.currentQuote.contactId,this.currentQuote.Id);
			insert t;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Rappel envoyé avec succès'));
			return goback();
		}else if (result =='KO') {
			isNoSelectedOpp = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Un rappel a déjà été envoyé, ou le document est déjà signé, ou le service Connective n\'est pas disponible'));
			return null;
		}		 	
	}
	else{
		isNoSelectedOpp = true;
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Veuillez d\'abord créer le document!'));
		return null;
	}
	return null;
}

	public pageReference goback()
	{
		return stdSetController.cancel();
	}

	without sharing class updateLockedRecord {	  
        public void updateRecord(Quote q) { 		
            update q; 
        }
    }
}