@isTest
public class ServiceDemandTrigger_Test {
    
    @isTest
    public static void testUpdateDetailsBeforeInsert(){
        
        Account acc = TestDataFactory.createAccountWithInsert('ENEDIS_MOULINS');
        Contact ctc = TestDataFactory.createContactWithInsert(acc);
        
        User communityUser  = TestDataFactory.createCommmunityUser(acc, ctc);
        
        Test.startTest();
        System.runAs(communityUser){
            
            SD_ServiceDemand__c sd = TestDataFactory.createServiceDemand(acc, ctc, 'Repairs');
            sd.BilledAccount__c = acc.ID;
            try{
                insert sd;
                
                sd = [Select ID, ApplicantCenter__c, ResponsibleName__c FROM SD_ServiceDemand__c WHERE ID = : sd.ID];
                
                System.debug('Acc.id : '+acc.ID);
                System.debug('sd.ApplicantCenter__c : '+sd.ApplicantCenter__c);
                System.debug('ctc.id : '+ctc.ID);
                System.debug('sd.ResponsibleName__c : '+sd.ResponsibleName__c);
                
                System.assertEquals(acc.ID, sd.ApplicantCenter__c);
                System.assertEquals(ctc.ID, sd.ResponsibleName__c);
                
                
            }catch(Exception e){
                System.debug('Exeption : '+e.getMessage());
            }
            
        }
        
        Test.stopTest();
    }
        
    @isTest
    public static void testUpdateLocalContactBeforeInsert(){
        
        Account acc = TestDataFactory.createAccountWithInsert('ENEDIS_MOULINS');
        Contact ctc = TestDataFactory.createContactWithInsert(acc);
        
        User communityUser  = TestDataFactory.createCommmunityUser(acc, ctc);
        Test.startTest();
        System.runAs(communityUser){
            
            SD_ServiceDemand__c sd = TestDataFactory.createServiceDemand(acc, ctc, 'Repairs');
            
            sd.BilledAccount__c = acc.ID;
            sd.Phone__c = ctc.Phone;
            sd.Mobile__c = ctc.MobilePhone;
            sd.Fax__c = ctc.Fax;
            sd.Email__c = ctc.Email;
            
            try{
                insert sd;
                sd = [Select ID, ContactPhone__c, ContactMobile__c, ContactFax__c, ContactEmail__c FROM SD_ServiceDemand__c WHERE ID = : sd.ID];
                
                System.debug('ctc.Phone : '+ctc.Phone);
                System.debug('sd.ContactPhone__c : '+sd.ContactPhone__c);
                System.debug('ctc.MobilePhone : '+ctc.MobilePhone);
                System.debug('sd.ContactMobile__c : '+sd.ContactMobile__c);
                System.debug('ctc.Fax : '+ctc.Fax);
                System.debug('sd.ContactFax__c : '+sd.ContactFax__c);
                
                System.assertEquals(ctc.Phone, sd.ContactPhone__c);
                System.assertEquals(ctc.MobilePhone, sd.ContactMobile__c);
                System.assertEquals(ctc.Fax, sd.ContactFax__c);
            }catch(Exception e){
                System.debug('Exeption : '+e.getMessage());
            }
            
            
        }
        
        Test.stopTest();
        
    }
    
    @isTest
    public static void testUpdateAddressBeforeInsert(){
        
        
         Account acc = TestDataFactory.createAccountWithInsert('ENEDIS_MOULINS');
        Contact ctc = TestDataFactory.createContactWithInsert(acc);
        
        
        User communityUser  = TestDataFactory.createCommmunityUser(acc, ctc);
        Test.startTest();
        System.runAs(communityUser){
            
            SD_ServiceDemand__c sd = TestDataFactory.createServiceDemand(acc, ctc, 'Repairs');
            
            sd.BilledAccount__c = acc.ID;
            
            
            try{
                insert sd;
                
                sd = [Select ID, RemovalStreet__c, RemovalPostalCode__c, RemovalCity__c, ShippingStreet__c, ShippingPostalCode__c, ShippingCity__c  FROM SD_ServiceDemand__c WHERE ID = : sd.ID];
                
                System.assertEquals(acc.BillingStreet, sd.RemovalStreet__c);
                System.assertEquals(acc.BillingPostalCode, sd.RemovalPostalCode__c);
                System.assertEquals(acc.BillingCity, sd.RemovalCity__c);
                System.assertEquals(acc.BillingStreet, sd.ShippingStreet__c);
                System.assertEquals(acc.BillingPostalCode, sd.ShippingPostalCode__c);
                System.assertEquals(acc.BillingCity, sd.ShippingCity__c);
                
            }catch(Exception e){
                System.debug('Exeption : '+e.getMessage());
            }
            
            
        }
        
        Test.stopTest();
        
    }
    
    
        
    @isTest
    public static void testupdatePriceBook(){
             
        List<Account> accs = new List<Account>();
        List<SD_ServiceDemand__c> sds = new List<SD_ServiceDemand__c>();
        
        Account accApplicantN = TestDataFactory.createAccount('ENEDIS_MOULINS_Nord');
        accApplicantN.EnedisRegion__c = 'Nord';
        accs.add(accApplicantN);
        
        Account accApplicantS = TestDataFactory.createAccount('ENEDIS_MOULINS_Sud');
        accApplicantN.EnedisRegion__c = 'Sud';
        accs.add(accApplicantS);
		
        insert accs;
        
		Contact ctc = TestDataFactory.createContactWithInsert(accApplicantN);
        
       
        SD_ServiceDemand__c sdReme = TestDataFactory.createServiceDemand(accApplicantN, ctc, 'Remediation');
        sds.add(sdReme);
        SD_ServiceDemand__c sdRep = TestDataFactory.createServiceDemand(accApplicantN, ctc, 'Repairs');
        sds.add(sdRep);
        SD_ServiceDemand__c sdElimN = TestDataFactory.createServiceDemand(accApplicantN, ctc, 'Elimination');
        sdElimN.ApplicantCenter__c = accApplicantN.ID;
        sds.add(sdElimN);
        SD_ServiceDemand__c sdElimS = TestDataFactory.createServiceDemand(accApplicantS, ctc, 'Elimination');
        sds.add(sdElimS);
        SD_ServiceDemand__c sdElimSF = TestDataFactory.createServiceDemand(accApplicantN, ctc, 'EliminationSF6');
        sds.add(sdElimSF);
        SD_ServiceDemand__c sdWas = TestDataFactory.createServiceDemand(accApplicantN, ctc, 'Waste');
        sds.add(sdWas);
        
        insert sds;
        
        
        Test.startTest();
        sds = [Select ID, Name, PriceBook__c, ApplicantCenter__r.EnedisRegion__c, Recordtype.DeveloperName FROM  SD_ServiceDemand__c WHERE ID in: sds];
        
        for(SD_ServiceDemand__c sd : sds){
            if(sd.Recordtype.DeveloperName == 'Remediation'){
                System.assertEquals(sd.PriceBook__c, Label.RCT_PB_Depollution);
            }else  if(sd.Recordtype.DeveloperName == 'Repairs'){
                System.assertEquals(sd.PriceBook__c, Label.RCT_PB_Reparation);
            }else if(sd.Recordtype.DeveloperName == 'Elimination'){
                if(sd.ApplicantCenter__r.EnedisRegion__c == 'Nord'){
                    System.assertEquals(sd.PriceBook__c, Label.RCT_PB_Destruction_Nord);
                }else{
                    System.assertEquals(sd.PriceBook__c, Label.RCT_PB_Destruction_Sud);
                }
            }else if(sd.Recordtype.DeveloperName == 'EliminationSF6'){
                System.assertEquals(sd.PriceBook__c, Label.RCT_PB_Elimination_SF6);
            }else{
                System.assertEquals(sd.PriceBook__c, Label.RCT_PB_TPC);
            }
        }
        
        
        Test.stopTest();
        
    }
    
    
    @isTest
    public static void testManageWSTStatus(){
        Test.startTest();
        List<WST_Waste_Statement__c> wsts = new List<WST_Waste_Statement__c>();
        Account acc = TestDataFactory.createAccountWithInsert('Account_Test');
      
        SD_ServiceDemand__c sd = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Remediation');
      	insert sd;
        
        
      
        wsts = TestDataFactory.createWasteStatementList(sd, 10);
        insert wsts;
       
        sd.Status__c = 'Envoyee a TREDI';
        update sd;
        Test.stopTest();
        System.assertEquals(10, wsts.size());
        
    }
    
    @isTest
    public static void testgetWSTMap(){
        
        Test.startTest();
        List<WST_Waste_Statement__c> wsts = new List<WST_Waste_Statement__c>();
        Account acc = TestDataFactory.createAccountWithInsert('Account_Test');
      
        SD_ServiceDemand__c sd = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Remediation');
        sd.Status__c = 'Envoyee a TREDI';
      	insert sd;
        
        
      
        wsts = TestDataFactory.createWasteStatementList(sd, 10);
        insert wsts;
       
        Map<ID, List<WST_Waste_Statement__c>>  mapTest = ServiceDemandTriggerUtil.getWSTMap(wsts);
        
        List<WST_Waste_Statement__c> result = mapTest.get(sd.ID);
        Test.stopTest();
        System.assertEquals(10, result.size());
        
        
        
        
    }
    
    @isTest
    public static void testgetMyWstToUpdate(){
        
        Test.startTest();
        List<WST_Waste_Statement__c> wsts = new List<WST_Waste_Statement__c>();
        List<SD_ServiceDemand__c> sds = new List<SD_ServiceDemand__c>();
        Account acc = TestDataFactory.createAccountWithInsert('Account_Test');
        
        SD_ServiceDemand__c sd = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Remediation');
        sd.Status__c = 'Envoyee a TREDI';
      	insert sd;
        sds.add(sd);
        
        wsts = TestDataFactory.createWasteStatementList(sd, 10);
        insert wsts;
        
        List<WST_Waste_Statement__c> results = ServiceDemandTriggerUtil.getMyWstToUpdate(ServiceDemandTriggerUtil.getWSTMap(wsts), sds);
        
        
        Test.stopTest();
        
        for(WST_Waste_Statement__c wst : results){
            System.assertEquals('Demande effectuee', wst.Status__c);
        }
        
    }
    
    @isTest
    public static void testgetMyWstToUpdate2(){
        
        Test.startTest();
        List<WST_Waste_Statement__c> wsts = new List<WST_Waste_Statement__c>();
        List<SD_ServiceDemand__c> sds = new List<SD_ServiceDemand__c>();
        Account acc = TestDataFactory.createAccountWithInsert('Account_Test');
        
        SD_ServiceDemand__c sd1 = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Remediation');
        sd1.Status__c = 'Envoyee a TREDI';
        sds.add(sd1);
        
        SD_ServiceDemand__c sd2 = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Remediation');
        sd2.Status__c = 'Logistique';
        sds.add(sd2);
        
        SD_ServiceDemand__c sd3 = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Remediation');
        sd3.Status__c = 'Recu';
        sds.add(sd3);
        
        SD_ServiceDemand__c sd4 = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Remediation');
        sd4.Status__c = 'En livraison';
        sds.add(sd4);
        
        insert sds;
        
        wsts.addAll(TestDataFactory.createWasteStatementList(sd1, 10));
        wsts.addAll(TestDataFactory.createWasteStatementList(sd2, 10));
        wsts.addAll(TestDataFactory.createWasteStatementList(sd3, 10));
        wsts.addAll(TestDataFactory.createWasteStatementList(sd4, 10));
        
        insert wsts;
        
        List<WST_Waste_Statement__c> results = ServiceDemandTriggerUtil.getMyWstToUpdate(ServiceDemandTriggerUtil.getWSTMap(wsts), sds);
        
        
        Test.stopTest();
        
        
        System.assertEquals(40, results.size());
        
    }
    
 
    
}