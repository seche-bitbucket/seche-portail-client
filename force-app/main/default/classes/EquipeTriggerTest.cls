@isTest
public class EquipeTriggerTest {
    
    @IsTest(SeeAllData=true) 
    private static void TestEquipeTrigger() {
        
        //Récupération du profile Standard Salesforce
        Profile p = [select id from profile where name = :'Chargé(e) d\'affaires' limit 1];
        
        
        String testemail = 'director_-_User_test@test.com';
        String testemail2 = 'assistant_-_User_test@test.com';
        String testemail3 = 'director2_-_User_test@test.com';
        String testemail4 = 'assistant2_-_User_test@test.com';
        
        
        User director = new User(profileId = p.id, username = testemail, email = testemail,
                                 SubsidiaryCreationRight__c = 'Tredi Hombourg',
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        insert director;
        
        User director2 = new User(profileId = p.id, username = testemail3, email = testemail3,
                                  SubsidiaryCreationRight__c = 'Tredi Hombourg',
                                  emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                  languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                  alias='cspu', lastname='lastname', IsActive=true);
        insert director2;
        
        
        
        User assistant = new User(profileId = p.id, username = testemail2, email = testemail2,
                                  SubsidiaryCreationRight__c = 'Tredi Hombourg',
                                  emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                  languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                  alias='cspu', lastname='lastname', IsActive=true);
        insert assistant;
        
        User assistant2 = new User(profileId = p.id, username = testemail4, email = testemail4,
                                   SubsidiaryCreationRight__c = 'Tredi Hombourg',
                                   emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                   languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                   alias='cspu', lastname='lastname', IsActive=true);
        insert assistant2;
        
        Equipe__c e = new Equipe__c();
        e.Name = 'Physico';
        e.Directeur__c = director.ID;
        e.Assistant__c = assistant.ID;
        e.Filiere__c = 'Physico';
        insert e;
        
        
        //e = [Select ID, Directeur__c, Assistant__c, Filiere__c FROM Equipe__c WHERE Filiere__c = 'Physico'];
        
        
        
        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Salesman__c = director.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        System.debug('..TOP Opportunité crée');
        Opportunity opp = new Opportunity();
        opp.Name = 'test';
        opp.Salesman__c = director.Id;
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        System.debug('..Opportunité crée');
        opp.Name = 'Test equipe';
        update opp;
        System.debug('..Opportunité mise à jour');
        
        e.Directeur__c = director2.ID;
        e.Assistant__c = assistant2.ID;
        update e;
        
        //e = [select Directeur__c, Assistant__c from Equipe__c where Filiere__c = 'Physico' ];
        
        opp.Name = 'Test equipe 1';
        update opp;
        
        e.Directeur__c = director.ID;
        e.Assistant__c = assistant.ID;
        update e;
        
        
        opp.Name = 'Test equipe 2';
        update opp;        
        Test.startTest();
        
        
        
        for(OpportunityTeamMember otm : [select UserId from OpportunityTeamMember where OpportunityId = : opp.ID]){
            System.debug('.......OTM : '+otm);
            System.debug('....... director.ID : '+ director.ID);
            System.debug('....... assistant.ID : '+ assistant.ID);
            System.debug('.......director2.ID : '+director2.ID);
            System.debug('.......assistant2.ID : '+assistant2.ID);
            
            if(otm.UserID == director2.ID){
                //System.assertEquals(otm.UserID, director2.ID);
            }else{
                //System.assertEquals(otm.UserID, assistant2.ID);
            }
            
        }
        
        //Test just the assistant has change
        e.Assistant__c = assistant.ID;       
        update e;
        
        
        
       /* OpportunityTeamMember otm = [select UserId from OpportunityTeamMember where OpportunityId = : opp.ID and TeamMemberRole = 'Assistant(e)'];
        System.assertEquals(otm.UserID, assistant.ID);
        
        //Test without assistant
        e.Assistant__c = null;
        e.Directeur__c = director.ID;
        update e;
        
        otm = [select UserId from OpportunityTeamMember where OpportunityId = : opp.ID AND TeamMemberRole = 'Directeur commercial'];
        //System.assertEquals(otm.UserID, director.ID);          
        */
        
        Test.stopTest();
        
        
        
    }
    
}