/**
 * @description       : 
 * @author            : MAIT - @moidriss
 * @group             : 
 * @last modified on  : 07-24-2023
 * @last modified by  : MAIT - @moidriss
**/
public with sharing class ScanPollutec {
    
    @AuraEnabled
    public static String CodeValidation(String url){
        try {
            System.debug('je passe ici '+url);
            // String recordId = url.substring(url.length() - 15);
            System.debug('recordId codeValid : ' + url);
            Map<String, Object> mapBarCode = (Map<String, Object>)JSON.deserializeUntyped(url);
            String recordId = (String)mapBarCode.get('value');
            //Contacts_Invitation__c conInv = [SELECT Id, Date_Time_Arrival_at_party__c FROM Contacts_Invitation__c WHERE Email__c = :recordId];
            Contacts_Invitation__c conInv = [SELECT Id, Date_Time_Arrival_at_party__c FROM Contacts_Invitation__c WHERE GUID__c = :recordId];
            if (conInv.Date_Time_Arrival_at_party__c != null) {
                return conInv.Date_Time_Arrival_at_party__c.format('dd-MM-yyyy à HH:mm:ss');
            }
            System.debug('conInv : ' + conInv);
            conInv.Date_Time_Arrival_at_party__c = Datetime.now();
            update conInv;
            return 'true';  
        } catch(Exception e) {
            System.debug(e.getMessage()+' '+e.getLineNumber());
            return 'false';
        }
    }
}