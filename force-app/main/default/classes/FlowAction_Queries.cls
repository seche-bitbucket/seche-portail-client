/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Mohammed Amine IDRISSI TAGHKI <mohammed-amine.idrissi-taghki@capgemini.com>
 * @version        1.0
 * @created        2022-02-22 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Desciption: Flow Action for queries
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */
global with sharing class FlowAction_Queries {
    
    global class Parameter {
        @InvocableVariable(Required=true)
        global String[] listValues;

        @InvocableVariable(Required=true)
        global String objectApiName;
 
        @invocableVariable
        global String field;

        @InvocableVariable
        global String fieldsToRetrieve;
    }

    // private class Condition {
    //     private Integer Index;
    //     private String Field;
    //     private String Operator;
    //     private Object Value;
    // }

    // private static String constructConditions(Condition[] conditions, String rule) {
    //     String constructed = '';
    //     for(Condition  cdt: conditions) {
    //         String postRule;
    //         if(rule.length() > 3) {
    //             String subStringRule = rule.right(rule.length() - rule.indexOf(cdt.Index+' '));
    //             postRule = subStringRule.left(subStringRule.indexOf(' '));
    //         } else {
    //             postRule = rule;
    //         }
    //         if(constructed == null) {
    //             constructed += cdt.Field+' '+cdt.Operator+' '+cdt.Value;
    //         } else {
    //             constructed += postRule+' '+cdt.Field+' '+cdt.Operator+' '+cdt.Value;
    //         }
    //     }
    //     return constructed;
    // }

    // future editor configurationEditor='c-lwc_-sql-invocable-editor'
    @InvocableMethod(label='SOQL from flow' description='Apex method to query data from SF using list of Ids' category='Flow Tools')
    public static List<List<SObject>> getRecords(Parameter[] parameters) {

        String[] recordIds = parameters[0].listValues;
        String objectApiName = parameters[0].objectApiName;
        String fields = parameters[0].fieldsToRetrieve;
        String lookupField = (parameters[0].field != null) ? parameters[0].field : 'Id';

        String query;
        if(fields != null) {
            query = 'SELECT '+fields+' FROM '+objectApiName+' WHERE '+lookupField+' IN :recordIds';
        } else { // fetch record fields
            List<String> fields_list = new List<String>();
            for(String f: Schema.getGlobalDescribe().get(objectApiName).getDescribe().fields.getMap().keySet()) {
                fields_list.add(f);
            }
            query = 'SELECT '+string.join(fields_list, ', ')+' FROM '+objectApiName+' WHERE '+lookupField+' IN :recordIds';
        }
        return new List<List<SObject>>{Database.query(query)};

    }

}