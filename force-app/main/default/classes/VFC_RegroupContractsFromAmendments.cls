/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-07-03
* @modified       2018-09-29
* @systemLayer    Controller         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Regroup Contracts Controller for the button that regroups 2 accounts
				to create one opportunity when batch is launched
* 2018-09-29    Added more tests to increase test coverage

* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class VFC_RegroupContractsFromAmendments {
    public List<Amendment__c> AmendmentList {get;set;}
    public Boolean differentAccount{get;set;}
    public Boolean SameContract{get;set;}
    public VFC_RegroupContractsFromAmendments(ApexPages.StandardSetController controller){
        this.differentAccount=false;
        this.SameContract=false;
        // retrieve selected Amendments and check if he selected more than 1 Amendment
        if(controller.getSelected().size()>=2){
            this.AmendmentList=[SELECT Id, Name, Contrat2__c,Contrat2__r.Account__c,Contrat2__r.RecordLocked__c,Contrat2__r.RegroupmentIndex__c FROM Amendment__c WHERE ID IN:controller.getSelected()];
        }else{
            this.AmendmentList = new List<Amendment__c>();
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Veuillez selectionner au moins deux avenants');
            ApexPages.addMessage(errorMessage);
        }
        //check if the contracts Linked to the selected amendments are from different accounts
        for(Amendment__c c: this.AmendmentList){
            if(c.Contrat2__r.Account__c!=this.AmendmentList[0].Contrat2__r.Account__c){
                this.differentAccount=true;
            }
        }
        for(Integer i=1;i<this.AmendmentList.size();i++){
            if(this.AmendmentList[i].Contrat2__c==this.AmendmentList[0].Contrat2__c){
                this.SameContract=true;
            }
        }
    }
    public PageReference regroupContracts(){
        PageReference returnPage=ApexPages.currentPage();
        //If accounts are from the different accounts we return the user to an error page
        if(this.differentAccount){
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Les contrats liés aux avenants sélectionnés proviennent de deux comptes différents et ne peuvent pas être fusionnés');
            ApexPages.addMessage(errorMessage);
            returnPage=null;
        }else{
            if(this.SameContract){
                ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Les avenants sélectionnés proviennent du même contrat, veuillez choisir un avenant par contrat différent');
                ApexPages.addMessage(errorMessage);
                returnPage=null;
            }else{
                returnPage=regroupContractsIfSameAccount();
            }
        }
        return ReturnPage;       
    }
    // Regroup Accounts if they are not locked and put the regroupment Index to a gradual number according to the data created
    public PageReference regroupContractsIfSameAccount(){
        PageReference returnPage=ApexPages.currentPage();
        Set<Contract__c> ContractsToUpdate=new Set<Contract__c>();
        String Status= 'Brouillon';
        List<Contract__c> AllContracts=[SELECT Id, Name, RegroupmentIndex__c FROM Contract__c Order By RegroupmentIndex__c ASC NULLS First];
        Decimal RegroupmentValue;
        RegroupmentValue=AllContracts[AllContracts.size()-1].RegroupmentIndex__c;
        if(RegroupmentValue==null){
            RegroupmentValue=0;
        }
        Boolean locked=false;
        If(this.AmendmentList!=null && this.AmendmentList.size()>0){
            for(Amendment__c c: this.AmendmentList){
                c.Contrat2__r.RegroupmentIndex__c=RegroupmentValue+1;
                ContractsToUpdate.add(c.Contrat2__r);
                if(c.Contrat2__r.RecordLocked__c){
                    locked=true;
                }
            }
        }
        If(ContractsToUpdate.size()>0 && !locked){
            List<Contract__c> ContractsListUpdate= new List<Contract__c>(ContractsToUpdate);
            Update ContractsListUpdate;
            ReturnPage= new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        }else{
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Le contrat sélectionné est soumis ou validé et ne peut pas être groupé');
            ApexPages.addMessage(errorMessage);
            ReturnPage=null;
        }
        return ReturnPage;
        
    }
}