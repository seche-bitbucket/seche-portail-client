public Class checkRecursive{
    private static boolean run = true;
    private static boolean runBefore = true;
    private static boolean runAfter = true;
    private static boolean runRights = true;
    private static boolean runLoads = true;
    public static boolean avoidRun = false;
    
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
            
        }
    }
    
    public static boolean runOnceBefore(){
        if(runBefore){
            runBefore=false;
            return true;
        }else{
            return runBefore;
            
        }
    }
    
    public static boolean runOnceAfter(){
        if(runAfter){
            runAfter=false;
            return true;
        }else{
            return runAfter;
            
        }
    }
    
    public static boolean runforRights(){
        if(runRights){
            runRights=false;
            return true;
        }else{
            return runRights;
            
        }
    }
    
    public static boolean runOnceTriggerLoads(){
        if(runLoads){
            runLoads=false;
            return true;
        }else{
            return runLoads;
            
        }
    }
    
    //Use to bypass trigger
    public static void setTrue(){
        avoidRun = true;
    }
    
    //Use to bypass trigger
    public static void setFalse(){
        avoidRun = false;
    }
    
    public static void overrideVals() {
        run = true;
		runBefore = true;
		runAfter = true;
        runRights = true;
        runLoads = true;
    }
}