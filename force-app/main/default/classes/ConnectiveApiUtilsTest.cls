@isTest
public with sharing class ConnectiveApiUtilsTest {
    
    @TestSetup
    static void dataSetup(){
        Contact cntct = new Contact(
            FirstName = 'fNameTest',
            LastName = 'lNameTest',
            Email = 'testemail@mail.com'
        );
        insert cntct;

        Profile p = [SELECT Id FROM Profile WHERE Name = 'Chargé(e) d\'affaires' LIMIT 1];
        User testUser  = new User(
            ProfileId = p.Id,
            Username = 'testusertest@mail.com',
            FirstName = 'fNameUser',
            LastName = 'lNameUser',
            Email = 'testusertest@mail.com',
            EmailEncodingKey = 'UTF-8', LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US', TimeZoneSidKey = 'America/Los_Angeles',
            Alias='userTest'
        );
        insert testUser;

        Opportunity testOpp = new Opportunity(
            Name = 'testOpp',
            StageName = 'En Cours',
            CloseDate = System.Today()
        );
        insert testOpp;
        Quote testQuote = new Quote(
            Name = 'testQuote',
            ContactId = cntct.Id,
            OpportunityId = testOpp.Id
        );
        insert testQuote;

        ContentVersion cv=new Contentversion(
            Title='packTest',
            PathOnClient ='test',
            VersionData=EncodingUtil.base64Decode('Unit Test Attachment Body')
        );
        
        insert cv;

        Id contentDocId = [SELECT ContentDocumentId FROM ContentVersion LIMIT 1].ContentDocumentId;
        ContentDocumentLink cdl = new ContentDocumentLink(
            ContentDocumentId = contentDocId,
            LinkedEntityId = testQuote.Id
        );
        insert cdl;

        Package__c pack = new Package__c(
            ConnectivePackageId__c = 'idTestOk'
        );
        insert pack;

        DocumentsForSignature__c doc = new DocumentsForSignature__c(
            Package__c = pack.Id,
            Quote__c = testQuote.Id,
            TECH_ExternalId__c = 'textExt'
        );
        insert doc;
    }

    @isTest
    static void createRequestTest(){
        HttpRequest testReq = ConnectiveApiUtils.createRequest('endpointTest', 'PUT');
        System.assertEquals('PUT', testReq.getMethod());
    }

    @isTest
    static void getSignerStakeholderFromContactTest(){
        Contact cntct = [SELECT Id, FirstName, LastName, Email FROM Contact LIMIT 1];
        PackageWrapper.Stakeholder sth = ConnectiveApiUtils.getSignerStakeholderFromContact(cntct, new List<String>());
        
        System.assertEquals('testemail@mail.com', sth.EmailAddress);
        List<PackageWrapper.Actor> actLs = sth.Actors;
        System.assert(actLs.size() > 0);
        System.assertEquals('Signer', actLs[0].Type);
    }

    @isTest
    static void getReceiverStakeholderTest(){
        PackageWrapper.Stakeholder sth = ConnectiveApiUtils.getReceiverStakeholder();
        System.assertEquals('Person', sth.Type);
        List<PackageWrapper.Actor> actLs = sth.Actors;
        System.assert(actLs.size() > 0);
        System.assertEquals('Receiver', actLs[0].Type);
    }

    @isTest
    static void getReceiverStakeholderFromUser(){
        User testUser =  [SELECT Id, FirstName, LastName, Email FROM User WHERE FirstName = 'fNameUser' LIMIT 1];
        PackageWrapper.Stakeholder sth = ConnectiveApiUtils.getReceiverStakeholderFromUser(testUser);
        System.assertEquals('testusertest@mail.com', sth.EmailAddress);
        List<PackageWrapper.Actor> actLs = sth.Actors;
        System.assert(actLs.size() > 0);
        System.assertEquals('Receiver', actLs[0].Type);
    }

    @isTest
    static void createPackageTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveApiMockTest());
        String packageIdOk = ConnectiveApiUtils.createPackage('packTest');
        System.assert(packageIdOk != null);

        String packageIdNok = ConnectiveApiUtils.createPackage('packTestNOK');
        System.assertEquals(null, packageIdNok);
        String packageIdNull = ConnectiveApiUtils.createPackage('packTestNULL');
        System.assertEquals(null, packageIdNull);
        List<Log__c> logLs = [SELECT Id FROM Log__c];
        System.assertEquals(2, logLs.size()); //size should be because error on packageNok & on package null

        Test.stopTest();
    }

    @isTest
    static void addDocumentToPackageTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveApiMockTest());


        ContentVersion cv = [SELECT PathOnClient, ContentLocation, Origin, OwnerId, Title, 
                                VersionData, ContentDocumentId 
                            FROM ContentVersion LIMIT 1];
        Map<String, Object> res = ConnectiveApiUtils.addDocumentToPackage('idTestOk', cv);
        System.assertEquals('idRespOk', (String)res.get('docId'));
        List<String> locIdResp = (List<String>)res.get('locIdLs');
        System.assertEquals('idLocOk', locIdResp[0]);

        Map<String, Object> resNOK = ConnectiveApiUtils.addDocumentToPackage('idTestNOK', cv);
        System.assertEquals(null, resNOK);
        Map<String, Object> resNull = ConnectiveApiUtils.addDocumentToPackage('idTestNull', cv);
        System.assertEquals(null, resNull);
        List<Log__c> logLs = [SELECT Id FROM Log__c];
        System.assertEquals(2, logLs.size());

        Test.stopTest();
    }

    @isTest
    static void addStaticToPackageTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveApiMockTest());

        StaticResource sr = [SELECT Id, Name, Body FROM StaticResource WHERE Name='CGV_Test']; //default is always supposed to exist

        Map<String, Object> res = ConnectiveApiUtils.addStaticToPackage('idTestOk', sr);
        System.assertEquals('idRespOk', (String)res.get('docId'));
        List<String> locIdResp = (List<String>)res.get('locIdLs');
        System.assertEquals('idLocOk', locIdResp[0]);

        Map<String, Object> resNOK = ConnectiveApiUtils.addStaticToPackage('idTestNOK', sr);
        System.assertEquals(null, resNOK);
        Map<String, Object> resNull = ConnectiveApiUtils.addStaticToPackage('idTestNull', sr);
        System.assertEquals(null, resNull);
        List<Log__c> logLs = [SELECT Id FROM Log__c];
        System.assertEquals(2, logLs.size());

        Test.stopTest();
    }

    @isTest
    static void processPackageTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveApiMockTest());

        Id quoteId = [SELECT Id FROM Quote LIMIT 1].Id;
        Boolean res = ConnectiveApiUtils.processPackage('idTestOk', quoteId, new List<String>{'locId'});
        System.assert(res);

        Boolean resNok = ConnectiveApiUtils.processPackage('idTestNOK', quoteId, new List<String>{'locId'});
        System.assert(!resNok);

        Boolean resNull = ConnectiveApiUtils.processPackage('idTestNull', quoteId, new List<String>{'locId'});
        System.assert(!resNull);

        List<Log__c> logLs = [SELECT Id FROM Log__c];
        System.assertEquals(2, logLs.size());
        Test.stopTest();
    }

    @isTest
    static void setPackageStatusTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveApiMockTest());

        Id quoteId = [SELECT Id FROM Quote LIMIT 1].Id;
        //might as well test the specific status methods
        Boolean res = ConnectiveApiUtils.setPackagePending('idTestOk');
        System.assert(res);

        Boolean resNok = ConnectiveApiUtils.setPackageStatus('idTestNOK', 'nok');
        System.assert(!resNok);

        Boolean resNull = ConnectiveApiUtils.setPackageRevoked('idTestNull');
        System.assert(!resNull);

        List<Log__c> logLs = [SELECT Id FROM Log__c];
        System.assertEquals(2, logLs.size());
        Test.stopTest();
    }

    @isTest
    static void parseStatusRespToDocListTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveApiMockTest());

        PackageResponse bodyRes = new PackageResponse();
        bodyRes.F2FSigningURL = 'urlTest';
        bodyRes.PackageStatus = 'Pending';
        PackageResponse.ResponsePackageDocument packDoc = new PackageResponse.ResponsePackageDocument();
        packDoc.DocumentName = 'CGVtest';
        packDoc.ExternalDocumentReference = 'extDocTest';
        packDoc.DocumentId = 'docIdTest';
        bodyRes.PackageDocuments = new List<PackageResponse.ResponsePackageDocument>{packDoc};
        PackageResponse.ResponseStakeholder sth = new PackageResponse.ResponseStakeholder();
        sth.Type = 'Person';
        Id contactId = [SELECT Id FROM Contact LIMIT 1].Id;
        sth.ExternalStakeholderReference = contactId;
        bodyRes.Stakeholders = new List<PackageResponse.ResponseStakeholder>{sth};

        Quote qte = [SELECT Id, QuoteCode__c FROM Quote LIMIT 1];
        Package__c packTest = new Package__c();
        insert packTest;
        List<DocumentsForSignature__c> res = ConnectiveApiUtils.parseStatusRespToDocList(bodyRes, packTest.Id, qte.Id);
        System.assertEquals(1, res.size());
        String docNameTest = 'CGVtest '+qte.QuoteCode__c;
        System.assertEquals(docNameTest, res[0].Name);
        System.assertEquals(contactId, res[0].SentTo__c);
        System.assertEquals(qte.Id, res[0].Quote__c);

        Test.stopTest();
    }

    @isTest
    static void setupPackFromQuoteIdTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveApiMockTest());
        Id quoteId = [SELECT Id FROM Quote LIMIT 1].Id;
        String packId = ConnectiveApiUtils.setupPackFromQuoteId(quoteId);
        System.assertNotEquals(null, packId);
        Test.stopTest();
    }

    @isTest
    static void getStaticResourceNameFromFilialeTest(){
        //the problem here is we can't really test others cause the metadata can't be created or updated for tests only
        String resDefault = ConnectiveApiUtils.getStaticResourceNameFromFiliale('none');
        System.assertEquals('CGV_Default', resDefault);        
    }

    @isTest
    static void updateAllStatusTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveApiMockTest());
        Id quoteId = [SELECT Id FROM Quote LIMIT 1].Id;
        Boolean res = ConnectiveApiUtils.updateAllStatus(quoteId);
        System.assert(res);
        String quoteStatus = [SELECT Status FROM Quote LIMIT 1].Status;
        System.assertEquals('Signé', quoteStatus);
        String packStatus = [SELECT ConnectivePackageStatus__c FROM Package__c LIMIT 1].ConnectivePackageStatus__c;
        System.assertEquals('FINISHED', packStatus);
        String docStatus = [SELECT Status__c FROM DocumentsForSignature__c LIMIT 1].Status__c;
        System.assertEquals('FINISHED', docStatus);
        Test.stopTest();
    }
}