@isTest(SeeAllData=false)
public class QLY_UtilsTest {
	@isTest
	public static void getEmailWithAttachmentTest() {
        
        Account account = new Account(
            Name = 'test Name1'
        );
        insert account;
        
        Contact contact = new Contact(
            Email = 'test1@test.com',
            LastName = 'lastName1'
        );
        insert contact;
        
        QLY_SchoolForm__c qly = new QLY_SchoolForm__c(
            DocumentStatus__c = 'Non Généré',
            Contact__c = Contact.Id,
            Account__c = account.Id,
            Name = 'Test Name1'
        );
        insert qly;
        
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test4',
            PathOnClient = 'Test4.pdf',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        List<ContentVersion> contentVersions = new List<ContentVersion>();
        contentVersions.add(contentVersionInsert);
        insert contentVersions;
 
        // Test INSERT
        List<ContentVersion> contentVersionSelect = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id IN (:contentVersionInsert.Id) LIMIT 2];
        List<ContentDocument> documents = [SELECT Id, Title, CreatedDate FROM ContentDocument];
        
        ContentDocumentLink cdl = New ContentDocumentLink(
            LinkedEntityId = qly.id,
            ContentDocumentId = documents[0].Id,
            shareType = 'V'
        );
        insert cdl;
        
        List<Messaging.SingleEmailMessage> emailList = QLY_Utils.getEmailWithAttachment(new QLY_SchoolForm__c[]{qly});
        String mailSubject = emailList[0].Subject;
        String attachmentName = emailList[0].fileattachments[0].getFileName();
        
        System.assertEquals('[TRIADIS] Formulaire collecte des déchets. - ' + qly.AccountName__c, mailSubject);
        System.assertEquals('Test4', attachmentName);
        
    }
    
    @isTest
    private static void SendLatestContentDocumentToContactByEmailTest(){
        Account account = new Account(
            Name = 'test Name'
        );
        insert account;
        
        Contact contact = new Contact(
            Email = 'test@test.com',
            LastName = 'lastName'
        );
        insert contact;
        
        QLY_SchoolForm__c qly = new QLY_SchoolForm__c(
            DocumentStatus__c = 'Non Généré',
            Contact__c = Contact.Id,
            Account__c = account.Id,
            Name = 'Test Name'
        );
        insert qly;
        
        QLY_Utils.SendLatestContentDocumentToContactByEmail(new List<QLY_SchoolForm__c>{qly});
    }
    
    @isTest
    private static void uploadFileTest()
    {
        Account account = new Account(
            Name = 'test Name'
        );
        insert account;
        
        Contact contact = new Contact(
            Email = 'test@test.com',
            LastName = 'lastName'
        );
        insert contact;
        
        QLY_SchoolForm__c qly = new QLY_SchoolForm__c(
            DocumentStatus__c = 'Non Généré',
            Contact__c = Contact.Id,
            Account__c = account.Id,
            Name = 'Test Name'
        );
        insert qly;
        
        Product2 prod2 = new Product2(Name ='prodtest', TECH_ExternalID__c='test');
        insert prod2;
        
        ContentVersion doc2 = QLY_Utils.uploadFileToRecord('this is a test', 'test2.pdf', qly.ID,  prod2.ID);
        System.debug('doc2Id '+doc2.Id);
        System.assertNotEquals(doc2.Id, null);
        
        ContentVersion doc = QLY_Utils.uploadFile('this is a test', 'test.pdf', prod2.ID);
        System.debug('docId '+doc.Id);
        System.assertNotEquals(doc.Id, null);
        
    }
    
    @isTest
    private static void getPiklistValuesTest(){
        
        List < Map < String, String >> values = QLY_Utils.getPiklistValues('QLY_SchoolForm__c', 'Contact');
        System.debug('values: '+values);
        System.assertEquals(values.size(),0);
        List < Map < String, String >> values1 = QLY_Utils.getPiklistValues('QLY_SchoolForm__c', 'SelectedCollections__c');
		System.debug('values1: '+values1);
        System.assertNotEquals(values1, null);
    }
    
    private static void sleep(integer milliseconds) 
    {
        Long timeDiff = 0;
        DateTime firstTime = System.now();
        do
        {
            timeDiff = System.now().getTime() - firstTime.getTime();
        }
        while(timeDiff <= milliseconds);      
    }
}