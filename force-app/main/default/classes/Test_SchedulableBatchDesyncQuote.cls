/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-03-19
* @modified       2018-03-19
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for schedulable batch to desync quotes that are created in the past month
				,are locked (approved or pending approval) with the opportunity in order to avoid  	
				Errors while modifying locked records by assistants and sales team
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@IsTest
public class Test_SchedulableBatchDesyncQuote {
    static Account a;
    static Contact c;
    static Opportunity opp;
    static Quote q;
    public static testMethod void testschedule() {
        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        q = new Quote();
        
        q.OpportunityId = opp.ID;
        q.Name = opp.Name;
        q.DateDevis__c = Date.today();
        q.ContactId = opp.ContactName__c;
        q.Email = opp.ContactName__r.Email;
        q.Phone = opp.ContactName__r.Phone;
        q.Fax = opp.ContactName__r.Fax;
        q.Status='Approuvé';
        q.BillingStreet = opp.Account.BillingStreet;
        q.BillingCity = opp.Account.BillingCity;
        q.BillingState = opp.Account.BillingState;
        q.BillingPostalCode = opp.Account.BillingPostalCode;
        q.BillingCountry = opp.Account.BillingCountry; 
        insert q;
        Approval.lock(q);
        opp.SyncedQuoteId=q.Id;
        Update opp;
        Test.StartTest();
                
        SchedulableContext sc = null;
        SchedulableBatchDesyncQuote sh1 = new SchedulableBatchDesyncQuote();
        sh1.execute(sc);
        Test.stopTest();
        Quote quo = [Select ID,IsSyncing, name,Status FROM QUOTE WHERE ID = : q.Id];
        System.assertEquals(false,quo.IsSyncing);
    }
}