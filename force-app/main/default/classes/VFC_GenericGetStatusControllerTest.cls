@isTest(SeeAllData=false)
public class VFC_GenericGetStatusControllerTest {
    
    @isTest
    public static void testOk() {
        
        String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

            //Get a profile to create User
            Profile p = [select id from profile where name in:profiLisList limit 1];
        
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        insert director;
        
        Lead ld = TestFactory.generateLead();
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();

        ByPassUtils.ByPass('QuoteTrigger');
            
        QteLineItemSyncTriggerV2Handler.stop();
        OppLineItemSyncTriggerV2Handler.stop();
        QuoteSyncTriggerV2Handler.stop();
        OppSyncTriggerV2Handler.stop();
        
        //Quote Section
        Opportunity opp = TestFactory.generateOpport(acc, null);

        Quote qte = TestFactory.generateQuote(ct, acc, opp);

        opp.SyncedQuoteId = qte.Id;
        update opp;
        
        ld.OwnerId = UserInfo.getUserId();
        update ld;
        
        T5F_PreFip__c prefip = TestFactory.generatePreFip(ld);
        
        T5F_PreFip__c prefip2 = TestFactory.generatePreFip(ld);
        
        Package__c pkgSign = new Package__c(ConnectivePackageId__c = 'packageOK');
        Package__c pkgRejected = new Package__c(ConnectivePackageId__c = 'idTestNOk');
    	
        insert new List<Package__c>{pkgRejected, pkgSign};
        
        DocumentsForSignature__c docSign = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign.Id, TECH_ExternalId__c='Signed', T5F_PreFip__c=prefip.Id);
        DocumentsForSignature__c docRejected = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgRejected.Id, TECH_ExternalId__c='Rejected', T5F_PreFip__c=prefip2.Id);
        
        insert new List<DocumentsForSignature__c>{docSign, docRejected};
            
        ConnectiveV4Mock.c = ct;
        
        System.RunAs(director) {
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new ConnectiveV4Mock());
            qte.Status = 'Signé';
            update qte;
            PageReference pf = Page.VFP_GenericGetStatus;
            Test.setCurrentPage(pf);
            ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<Quote>{qte});
            ApexPages.Currentpage().getParameters().put('id',qte.Id);
            VFC_GenericGetStatusController gsr = new VFC_GenericGetStatusController(ssc);
            gsr.init();
            gsr.goback();
            System.debug(gsr.getIsNoSelectedOpp());
            Test.stopTest();
        }
        
    }
    
    @isTest
    public static void testNOk2() {
        
        String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

            //Get a profile to create User
            Profile p = [select id from profile where name in:profiLisList limit 1];
        
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        insert director;
        
        Lead ld = TestFactory.generateLead();
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();

        ByPassUtils.ByPass('QuoteTrigger');
            
        QteLineItemSyncTriggerV2Handler.stop();
        OppLineItemSyncTriggerV2Handler.stop();
        QuoteSyncTriggerV2Handler.stop();
        OppSyncTriggerV2Handler.stop();
        
        //Quote Section
        Opportunity opp = TestFactory.generateOpport(acc, null);

        Quote qte = TestFactory.generateQuote(ct, acc, opp);

        opp.SyncedQuoteId = qte.Id;
        update opp;
        
        ld.OwnerId = UserInfo.getUserId();
        update ld;
        
        T5F_PreFip__c prefip = TestFactory.generatePreFip(ld);
        
        T5F_PreFip__c prefip2 = TestFactory.generatePreFip(ld);
        
        Package__c pkgSign = new Package__c(ConnectivePackageId__c = 'packageOK');
        Package__c pkgRejected = new Package__c(ConnectivePackageId__c = 'packageNOK');
    	
        Insert new List<Package__c>{pkgRejected, pkgSign};
        
        DocumentsForSignature__c docSign = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign.Id, TECH_ExternalId__c='Signed', T5F_PreFip__c=prefip.Id);
        DocumentsForSignature__c docRejected = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgRejected.Id, TECH_ExternalId__c='Rejected', T5F_PreFip__c=prefip2.Id);
        
        Insert new List<DocumentsForSignature__c>{docSign, docRejected};
            
        ConnectiveV4Mock.c = ct;
        
        System.RunAs(director) {
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new ConnectiveV4Mock());
            
            PageReference pf = Page.VFP_GenericGetStatus;
            Test.setCurrentPage(pf);
            ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<Quote>());
            ApexPages.Currentpage().getParameters().put('id',qte.Id);
            VFC_GenericGetStatusController gsr = new VFC_GenericGetStatusController(ssc);
            gsr.init();
            gsr.goback();
            System.debug(gsr.getIsNoSelectedOpp());
            Test.stopTest();
        }
        
    }
    
    @isTest
    public static void testNOk() {
        
        String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

            //Get a profile to create User
            Profile p = [select id from profile where name in:profiLisList limit 1];
        
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        insert director;
        
        Lead ld = TestFactory.generateLead();
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();

        ByPassUtils.ByPass('QuoteTrigger');
            
        QteLineItemSyncTriggerV2Handler.stop();
        OppLineItemSyncTriggerV2Handler.stop();
        QuoteSyncTriggerV2Handler.stop();
        OppSyncTriggerV2Handler.stop();
        
        //Quote Section
        Opportunity opp = TestFactory.generateOpport(acc, null);

        Quote qte = TestFactory.generateQuote(ct, acc, opp);

        opp.SyncedQuoteId = qte.Id;
        update opp;
        
        ld.OwnerId = UserInfo.getUserId();
        update ld;
        
        T5F_PreFip__c prefip = TestFactory.generatePreFip(ld);
        
        T5F_PreFip__c prefip2 = TestFactory.generatePreFip(ld);
        
        Package__c pkgSign = new Package__c(ConnectivePackageId__c = 'packageOK');
        Package__c pkgRejected = new Package__c(ConnectivePackageId__c = 'packageNOK');
    	
        Insert new List<Package__c>{pkgRejected, pkgSign};
        
        DocumentsForSignature__c docSign = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign.Id, TECH_ExternalId__c='Signed', T5F_PreFip__c=prefip.Id);
        DocumentsForSignature__c docRejected = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgRejected.Id, TECH_ExternalId__c='Rejected', T5F_PreFip__c=prefip2.Id);
        
        Insert new List<DocumentsForSignature__c>{docSign, docRejected};
            
        ConnectiveV4Mock.c = ct;
        
        System.RunAs(director) {
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new ConnectiveV4Mock());
            
            PageReference pf = Page.VFP_GenericGetStatus;
            Test.setCurrentPage(pf);
            ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<Quote>{qte});
            ApexPages.Currentpage().getParameters().put('id',qte.Id);
            VFC_GenericGetStatusController gsr = new VFC_GenericGetStatusController(ssc);
            gsr.init();
            gsr.goback();
            System.debug(gsr.getIsNoSelectedOpp());
            Test.stopTest();
        }
        
    }

}