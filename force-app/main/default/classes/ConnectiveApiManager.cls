/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ConnectiveApiManager
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ConnectiveApiManager
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 13-05-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
Global class ConnectiveApiManager implements Database.AllowsCallouts,Database.Stateful {
    
    /* Send Signiature by Quote or FIP object */
    webservice static String createInstantPackage(Id objId){
        Boolean fipCreatedPackage = true;
        if(objId.getSObjectType() == FIP_FIP__c.sObjectType){
            List<DocumentsForSignature__c> docs = new List<DocumentsForSignature__c>();
            docs = [SELECT Id, Status__c, FlowId__c, FIP_FIP__c FROM DocumentsForSignature__c WHERE FIP_FIP__c= :objId Limit 1];
            if(docs.isEmpty()!=True){
                fipCreatedPackage = false;
            }
        }
        if(fipCreatedPackage){
            HttpResponse res = new HttpResponse();
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            req.setHeader('Content-Type','application/json');
            req.setEndpoint('callout:ConnectiveApiV3/packages/instant');
            req.setMethod('POST');
            req.setTimeout(120000);
            try{
                String query=PackageWrapper.populatejson(objId);
                PackageWrapper fl=PackageWrapper.deserializejson(query);
                req.setBody(query);
                res = http.send(req);
                if(res.getStatusCode() == 201){
                    PackageResponse bodyRes = PackageResponse.deserializeResponsejson(res.getBody());
                    System.debug('bodyRes : '+ bodyRes);
                    Package__c pkg = new Package__c(
                        ConnectivePackageId__c=bodyRes.PackageId,
                        ConnectivePackageStatus__c=bodyRes.PackageStatus
                    );
                    insert pkg;
                    Id idFIP;
                    Id idQuote;
                    if(objId.getSObjectType() == FIP_FIP__c.sObjectType){
                        idFIP=objId;
                    }
                    else if(objId.getSObjectType() == Quote.sObjectType){
                        idQuote=objId;
                    }
                    DocumentsForSignature__c doc=new DocumentsForSignature__c();
                    doc.Name= PackageResponse.getFirstDocumentName(bodyRes);
                    doc.SigningUrl__c=bodyRes.F2FSigningURL;
                    doc.SentTo__c=PackageResponse.getFirstSignerId(bodyRes);
                    doc.TECH_ExternalId__c=PackageResponse.getFirstDocumentIdSalesforce(bodyRes);
                    doc.ConnectiveDocumentId__c = PackageResponse.getFirstDocumentId(bodyRes);
                    doc.Status__c = 'PENDING';
                    doc.FIP_FIP__c=idFIP;
                    doc.Quote__c=idQuote;
                    doc.Package__c = pkg.Id;
                    insert doc;
                    return 'OK';
                }else{
                    fl.Document ='';
                    Log__c error = new Log__c(
                        ApexJob__c= ConnectiveApiManager.class.getName(),
                        ErrorMessage__c='request : '+ fl + 'response :' + res.getBody(),
                        ExceptionType__c='HTTP'
                    );
                    insert error;
                    return 'KO';
                }
            }
            catch(Exception ex){
                Log__c error = new Log__c(
                        ApexJob__c= ConnectiveApiManager.class.getName(),
                        ErrorMessage__c=ex.getMessage(),
                        ExceptionType__c='Error APEX'
                    );
                insert error;
                return 'KO : '+ ex.getMessage();
            }
        }
        return 'Not processed';
    }

    @future (callout=true)
    webservice static void createInstantPackageFIPTrigger(Id fipId){
        String result = '';
        result = createInstantPackage(fipId);
    }

    webservice static String getStatusByConnectivePackageId(String connectiveIdPackage){
        String resultStatus;
        HttpResponse res = new HttpResponse();
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        req.setHeader('Content-Type','application/json');
        req.setEndpoint('callout:ConnectiveApiV3/packages/'+connectiveIdPackage+'/status');
        req.setMethod('GET');
        req.setTimeout(120000);
        try{
            res = http.send(req);
            if(res.getStatusCode() == 200){
                resultStatus =res.getBody();        
            }
            else{
                Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiManager.class.getName(),
                    ErrorMessage__c='response :' + res.getBody(),
                    ExceptionType__c='HTTP'
                );
                insert error;
            }
        }
        catch(Exception ex){
            Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiManager.class.getName(),
                    ErrorMessage__c=ex.getMessage(),
                    ExceptionType__c='Error APEX'
                );
            insert error;
        }
        return resultStatus;
    }

    //same as getDocument 3 params but check if docName contains CGV to modify doc title
    webservice static void getDocumentByName(Id objId, String connectiveIdPackage, String documentId, String docName){
        HttpResponse res = new HttpResponse();
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        req.setHeader('Content-Type','application/json');
        req.setEndpoint('callout:ConnectiveApiV3/packages/'+connectiveIdPackage+'/download/'+documentId);
        req.setMethod('GET');
        req.setTimeout(120000);
        try{
            res = http.send(req);
            if(res.getStatusCode() == 200){
                Blob result =res.getBodyAsBlob();
                String titleDoc = '';
                titleDoc = retrieveName(objId);
                //if doc is CGV then add CGV to the name
                if (docName.contains('CGV')){
                    titleDoc += ' - CGV';
                }
                insertFile(result,titleDoc,objId);
            }
            else{
                Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiManager.class.getName(),
                    ErrorMessage__c='response :' + res.getBody(),
                    ExceptionType__c='HTTP'
                );
                insert error;
            }
        }
        catch(Exception ex){
            Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiManager.class.getName(),
                    ErrorMessage__c=ex.getMessage(),
                    ExceptionType__c='Error APEX'
                );
            insert error;
        }
    }

    webservice static void getDocument(Id objId, String connectiveIdPackage, String documentId){
        HttpResponse res = new HttpResponse();
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        req.setHeader('Content-Type','application/json');
        req.setEndpoint('callout:ConnectiveApiV3/packages/'+connectiveIdPackage+'/download/'+documentId);
        req.setMethod('GET');
        req.setTimeout(120000);
        try{
            res = http.send(req);
            if(res.getStatusCode() == 200){
                Blob result =res.getBodyAsBlob();
                String titleDoc = retrieveName(objId);
                insertFile(result,titleDoc,objId);
            }
            else{
                Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiManager.class.getName(),
                    ErrorMessage__c='response :' + res.getBody(),
                    ExceptionType__c='HTTP'
                );
                insert error;
            }
        }
        catch(Exception ex){
            Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiManager.class.getName(),
                    ErrorMessage__c=ex.getMessage(),
                    ExceptionType__c='Error APEX'
                );
            insert error;
        }
    }

    public static String updateStatus(List<DocumentsForSignature__c> docs) {
        Map<Id,String> pkgStatus = new Map<Id,String>();
        List<Package__c> pkgsToUpdate = new List<Package__c>();
        List<DocumentsForSignature__c> docToDownload = new List<DocumentsForSignature__c>();
        String simpleReturn = ''; //use for the button get status
        Set<Id> docId = new Set<Id>();
        Map<Package__c,DocumentsForSignature__c> pkgDoc = getPackageDoc(docs);
        for(Package__c pkg : pkgDoc.keyset()){
            String result =  getStatusByConnectivePackageId(pkg.ConnectivePackageId__c);
            System.debug('result : '+result);
            if(result !=null){
                PackageResponse.ResponseGetPackageStatus jsonTransform = PackageResponse.deserializeResponseGetPackageStatusjson(result);
                if(String.isNotBlank(jsonTransform.PackageStatus)){
                    updateDocumentForSigQuote(jsonTransform,pkg.ConnectivePackageId__c);
                    pkgStatus.put(pkg.Id,jsonTransform.PackageStatus);
                    simpleReturn = 'OK';
                }
                else{
                    simpleReturn = 'KO';
                }
                docId.add(pkgDoc.get(pkg).Id);
            }
        }

        for(Package__c pkg : pkgDoc.keySet()){
            Package__c pkgToUpdate = new Package__c(Id=pkg.Id);
            pkgToUpdate.ConnectivePackageStatus__c = pkgStatus.get(pkg.Id);
            pkgsToUpdate.add(pkgToUpdate);
            if(pkgStatus.get(pkg.Id) == 'FINISHED'){
                docToDownload.add(pkgDoc.get(pkg));
            }
        }

        if(docToDownload.size()>0){
            Batch_Connective_Get_Document batch = new Batch_Connective_Get_Document(docToDownload,null);
            system.enqueueJob(batch);
        }
        
        try{
            update pkgsToUpdate;
        }
        catch(Exception ex){
            Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiManager.class.getName(),
                    ErrorMessage__c=ex.getMessage(),
                    ExceptionType__c='Error APEX'
                );
            insert error;
            simpleReturn = 'KO';
        }
        return simpleReturn;
    }

    webservice static String updateStatusByFipQuote(Id objId) {
        List<DocumentsForSignature__c> docs = new List<DocumentsForSignature__c>();
        if(objId.getSObjectType() == FIP_FIP__c.sObjectType){
            docs = [SELECT Id, Status__c, FlowId__c, FIP_FIP__c,Package__c FROM DocumentsForSignature__c WHERE FIP_FIP__c= :objId Limit 1];
        }
        else{
            docs = [SELECT Id, Status__c, FlowId__c, FIP_FIP__c,Package__c FROM DocumentsForSignature__c WHERE Quote__c= :objId Limit 1];
        }
        return updateStatus(docs);
    }

    webservice static String sendRedminder(Id objId) {
        DocumentsForSignature__c doc;
        if(objId.getSObjectType() == FIP_FIP__c.sObjectType){
            doc = [SELECT Id, Status__c, FlowId__c, FIP_FIP__c,Package__r.ConnectivePackageId__c FROM DocumentsForSignature__c WHERE FIP_FIP__c= :objId Limit 1];
        }
        else{
            doc = [SELECT Id, Status__c, FlowId__c, FIP_FIP__c,Package__r.ConnectivePackageId__c FROM DocumentsForSignature__c WHERE Quote__c= :objId Limit 1];
        }
        String packageID = doc.Package__r.ConnectivePackageId__c;
        if(String.IsNotBlank(packageID)){
            HttpResponse res = new HttpResponse();
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            req.setHeader('Content-Type','application/json');
            req.setEndpoint('callout:ConnectiveApiV3/packages/'+packageID+'/reminders');
            req.setMethod('POST');
            req.setTimeout(120000);
            try{
                String query=PackageWrapper.populatejsonSendReminders(packageID);
                PackageWrapper fl=PackageWrapper.deserializejson(query);
                req.setBody(query);
                res = http.send(req);
                if(res.getStatusCode() == 200){
                    return 'OK';
                }else{
                    fl.Document ='';
                    Log__c error = new Log__c(
                        ApexJob__c= ConnectiveApiManager.class.getName(),
                        ErrorMessage__c='request : '+ fl + 'response :' + res.getBody(),
                        ExceptionType__c='HTTP'
                    );
                    insert error;
                    return 'KO';
                }
            }
            catch(Exception ex){
                Log__c error = new Log__c(
                        ApexJob__c= ConnectiveApiManager.class.getName(),
                        ErrorMessage__c=ex.getMessage(),
                        ExceptionType__c='Error APEX'
                    );
                insert error;
                return 'KO : '+ ex.getMessage();
            }
        }
        return 'Not processed';
    }

    private static void insertFile(Blob resultpdf, String docName, Id parent){
    //Insert ContentVersion
        ContentVersion cVersion = new ContentVersion();
        cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
        cVersion.PathOnClient = docName + ' - Signée.pdf';//File name with extention
        cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.
        cVersion.OwnerId = userinfo.getUserId();//Owner of the file
        cVersion.Title = docName + ' - Signée';//Name of the file
        cVersion.VersionData = resultpdf;//File content
        cVersion.firstPublishLocationId= userinfo.getUserId();
        Insert cVersion;
    
    //After saved the Content Verison, get the ContentDocumentId
        Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
    
    //Insert ContentDocumentLink
        ContentDocumentLink cDocLink = new ContentDocumentLink();
        cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
        cDocLink.LinkedEntityId = parent;//Add attachment parentId
        cDocLink.ShareType = 'V';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
        cDocLink.Visibility = 'AllUsers';//AllUsers, InternalUsers, SharedUsers
        Insert cDocLink;
    }
    
    /* BEGIN Region Batch Method */
    public static PackageResponse.ResponseGetListPackage getBatch50Package(Boolean firstBatch,Integer continuationToken,Integer total){
        Integer maxQuantity = 50;
        Integer limitContinuationToken = 1;
        String urlService='';
        if(!firstBatch){
            limitContinuationToken = Integer.valueOf(Math.round(total/maxQuantity)+1);
            urlService = 'callout:ConnectiveApiV3/packages?MaxQuantity='+maxQuantity+'&ContinuationToken='+continuationToken;
        }
        else{
            urlService = 'callout:ConnectiveApiV3/packages?MaxQuantity='+maxQuantity;
        }
        PackageResponse.ResponseGetListPackage result;
        HttpResponse res = new HttpResponse();
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        req.setHeader('Content-Type','application/json');
        req.setEndpoint(urlService);
        req.setMethod('GET');
        req.setTimeout(120000);
        try{
            if(firstBatch || ContinuationToken<=limitContinuationToken){
                if(test.isrunningtest()){
                    result = PackageResponse.deserializeResponseGetListPackagejson('{"ContinuationToken": "1","Items": [{"PackageId": "3bb6a2d1-35db-4a3a-a434-868c01bcca9d","PackageStatus": "Finished","ExternalPackageReference": "0Q01i000000l0V2CAI"}],"MaxQuantity": 50,"Total": 21476}');
                }
                else{
                    res = http.send(req);
                    if(res.getStatusCode() == 200){
                        result = PackageResponse.deserializeResponseGetListPackagejson(res.getBody());
                        System.debug(result);
                    }
                    else{
                        Log__c error = new Log__c(
                            ApexJob__c= ConnectiveApiManager.class.getName(),
                            ErrorMessage__c='Code Status :'+res.getStatusCode()+' '+res.getBody(),
                            ExceptionType__c='HTTP'
                        );
                        insert error;
                    }
                }
            }
            return result;
        }
        catch(Exception ex){
            Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiManager.class.getName(),
                    ErrorMessage__c=ex.getMessage(),
                    ExceptionType__c='Error APEX'
                );
            insert error;
            return result;
        }
    }
    /* END Region Batch Method */
    
    /* BEGIN Region generic method */
    public static Map<Package__c,DocumentsForSignature__c> getPackageDoc(List<DocumentsForSignature__c> docs){
        Map<Id,DocumentsForSignature__c> pkgIdDoc = new Map<Id,DocumentsForSignature__c>();
        Map<Package__c,DocumentsForSignature__c> pkgDoc = new Map<Package__c,DocumentsForSignature__c>();

        for(DocumentsForSignature__c doc:docs){
            pkgIdDoc.put(doc.Package__c,doc);
        }
        List<Package__c> pkgs = [SELECT Id,ConnectivePackageId__c,ConnectivePackageStatus__c FROM Package__c WHERE Id =:pkgIdDoc.keySet()];
        for(Package__c pkg : pkgs){
            pkgDoc.put(pkg,pkgIdDoc.get(pkg.Id));
        }
        return pkgDoc;
    }
    /* END Region generic method */

    public static void updateQuoteStatus(Id idQuote, String Status, Boolean eSigned){
        try{
            Quote quo = new Quote(Id=idQuote,Status=Status,ESigned__c=eSigned);
            //update quo;
            updateLockedRecord updateLockedRecord = new updateLockedRecord();
            updateLockedRecord.updateRecord(quo);
        }catch (Exception ex){
            Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiManager.class.getName(),
                    ErrorMessage__c=ex.getMessage(),
                    ExceptionType__c='Error APEX'
                );
            insert error;
        }
    }
    
    public static void updateDocumentForSigQuote(PackageResponse.ResponseGetPackageStatus resultStatus,String connectiveIdPackage){
        String status = resultStatus.PackageStatus;
        List<Messaging.SingleEmailMessage> lstMsg = new List<Messaging.SingleEmailMessage>();
        DocumentsForSignature__c doc = [SELECT Id,Status__c,ConnectiveDocumentId__c,ReasonRejected__c,DownloadSignedURL__c,Quote__c FROM DocumentsForSignature__c WHERE Package__r.ConnectivePackageId__c =:connectiveIdPackage Limit 1];
        doc.Status__c = status;
        if(status == 'FINISHED' || status == 'REJECTED'){
            List<PackageResponse.ResponseActor> lstActor = new List<PackageResponse.ResponseActor>();
            for(PackageResponse.ResponseStakeholder sth : resultStatus.Stakeholders){
                lstActor.addAll(sth.Actors);
            }
            String urlDownload ='';
            String reason ='';
            for(PackageResponse.ResponseActor act : lstActor){
                if(act.Type == 'Receiver'){
                    urlDownload =  act.ActionUrl;
                }
                if(act.Type == 'Signer'){
                    reason = act.Reason;
                }
            }
            doc.DownloadSignedURL__c = urlDownload;
            doc.ReasonRejected__c = reason;
            doc.ConnectiveDocumentId__c = PackageResponse.getFirstDocumentId(resultStatus);
        }
        if(doc.Quote__c != null){
            String qStatus = '';
            Boolean qESigned = false;
            if(status == 'FINISHED'){
                qStatus = 'Signé';
                qESigned = true;
                Quote qt = [SELECT Id,Name,NameCustomer__c,Opportunity.Salesman__r.Email,Opportunity.Salesman__r.Assistant__r.Email,Quote_Type__c,QuoteNumber FROM Quote WHERE Id =:doc.Quote__c];
                Messaging.SingleEmailMessage msg = generateContentEmail(qt,qStatus,doc);
                lstMsg.add(msg);
            }
            else if(status == 'Rejected'){
                qStatus = 'Refusé';
                Quote qt = [SELECT Id,Name,NameCustomer__c,Opportunity.Salesman__r.Email,Opportunity.Salesman__r.Assistant__r.Email,Quote_Type__c,QuoteNumber FROM Quote WHERE Id =:doc.Quote__c];
                Messaging.SingleEmailMessage msg = generateContentEmail(qt,qStatus,doc);
                lstMsg.add(msg);
            }
            if(String.isNotBlank(qStatus)){
                updateQuoteStatus(doc.Quote__c,qStatus,qESigned);
            }
        }
        update doc;
        Messaging.sendEmail(lstMsg);
    }

    /* Function retrieveName : used to retrieve the Name that will be
    * added as the name of the document sent for signing
    */
    public static String retrieveName(Id objId){
        String returnValue = '';
        System.debug(objId);
        String sobjectType = objId.getSObjectType().getDescribe().getName();
        Schema.SObjectType convertType = Schema.getGlobalDescribe().get(sobjectType);
        if (convertType == Quote.sObjectType) {
            returnValue = [Select Id,QuoteCode__c FROM Quote Where Id= : objId LIMIT 1].QuoteCode__c;
        } else if (convertType == FIP_FIP__c.sObjectType) {
            returnValue = [Select Id,Name FROM FIP_FIP__c Where Id= : objId LIMIT 1].Name;
        }  else{
            System.debug('ERROR OBJECT NOT HANDLED BY CALLOUT');
        }
        return returnValue;
    }

    public static List<Messaging.SingleEmailMessage> generateEmail(Map<Quote,DocumentsForSignature__c> mapQtDoc){        
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
        for(Quote qt : mapQtDoc.keySet()){
            allmsg.add(generateContentEmail(qt,'Refusé',mapQtDoc.get(qt)));
        }
        return allmsg;
    }

    public static Messaging.SingleEmailMessage generateContentEmail(Quote qt,String qStatus,DocumentsForSignature__c doc){        
        List<string> toAddress = new List<string>();
        List<string> ccAddress = new List<string>();
        String conventionOrDevis;
        String signedOrRejected;
        String reasonRejected;

        if(qStatus == 'Signé'){
            if(qt.Quote_Type__c == 'Convention'){
                conventionOrDevis = 'Convention';
                signedOrRejected = 'signée';
            }else{
                conventionOrDevis = 'Devis';
                signedOrRejected = 'signé';
            }
            reasonRejected = '';
        }else{
            if(qt.Quote_Type__c == 'Convention'){
                conventionOrDevis = 'Convention';
                signedOrRejected = 'refusée';
            }else{
                conventionOrDevis = 'Devis';
                signedOrRejected = 'refusé';
            }
            reasonRejected = '</br>Motif du rejet : '+ doc.ReasonRejected__c;
        }
        
        String subject = conventionOrDevis + ' ' + qt.QuoteNumber + ' ' + signedOrRejected + ' par le client';
        String header = 'Bonjour,'+
                        '</br>'+
                        '</br>';
        String core = 'Votre '+conventionOrDevis.toLowerCase()+' n° '+qt.QuoteNumber+' ('+qt.Name+') a été '+signedOrRejected+' par '+qt.NameCustomer__c+'.'+
                reasonRejected +
                '</br>' +
                'Vous pouvez consulter l\'enregistrement sur Salesforce directement à partir du lien suivant : '+
                +'<a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+qt.Id+'">Cliquez ici.</a>'+
                +'</br>'+
                '</br>';
        String footer = 'Séché Environnement';
        String body = header+core+footer;
        
        toAddress.add(qt.Opportunity.Salesman__r.Email);
        if (qt.Opportunity.Salesman__r != null && String.isNotBlank(qt.Opportunity.Salesman__r.Assistant__r.Email)){
            ccAddress.add(qt.Opportunity.Salesman__r.Assistant__r.Email);
        }
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.htmlBody=body;
            mail.setSubject(subject);
            mail.setToAddresses(toAddress);
            mail.setCcAddresses(ccAddress);
            mail.setWhatId(qt.Id);
            mail.setSaveAsActivity(false);
            mail.setUseSignature(false);
        return mail;
    }

    without sharing class updateLockedRecord {	  
        public void updateRecord(Quote q) { 		
            update q; 
        }
    }
}