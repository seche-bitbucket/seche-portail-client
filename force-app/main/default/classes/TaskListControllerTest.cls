@isTest
public class TaskListControllerTest {
    
    public static testMethod void taskListMethod() {
        TaskListController tskListCo = new TaskListController();

        Task tsk = new Task(Subject = 'test');
        insert tsk;
         //Test ownerId is a current user
        list<Task> tskList = tskListCo.getTasks();
        system.assertEquals(0, tskList.size());
        //Test Delegate Task to another user
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Directeur commercial' limit 1];
        User usr = new User(Alias = 'stdu', Email='stduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='stduser@testorg.com');
        insert usr;
        Task tskUpdate = [select Id, OwnerId from Task where Subject = 'test'];
        tskUpdate.OwnerId = usr.Id;
        update tskUpdate;
     
        tskList = tskListCo.getTasks();
        system.assertEquals(1, tskList.size());
        
    }
    

}