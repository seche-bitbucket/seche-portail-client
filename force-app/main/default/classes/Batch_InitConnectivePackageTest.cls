@isTest
public with sharing class Batch_InitConnectivePackageTest {
    @testSetup 
    static void setup() {

        Account a;
        Contact c;
        Quote q;
        Opportunity opp;
        
        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        q = new Quote();
        //q.id='0Q09E0000000AhESAU';
        q.OpportunityId = opp.ID;
        q.Name = opp.Name;
        q.DateDevis__c = Date.today();
        q.QuoteCode__c=opp.Name;
        q.ContactId = opp.ContactName__c;
        q.Email = opp.ContactName__r.Email;
        q.Phone = opp.ContactName__r.Phone;
        q.Fax = opp.ContactName__r.Fax;
        q.BillingStreet = opp.Account.BillingStreet;
        q.BillingCity = opp.Account.BillingCity;
        q.BillingState = opp.Account.BillingState;
        q.BillingPostalCode = opp.Account.BillingPostalCode;
        q.BillingCountry = opp.Account.BillingCountry; 
        insert q;

        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =Blob.valueOf('Unit Test Attachment Body');          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=q.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;

        DocumentsForSignature__c doc=new DocumentsForSignature__c();
        doc.Name= 'titre';
        doc.SigningUrl__c='';
        doc.SentTo__c=c.Id;
        doc.TECH_ExternalId__c=cv.Id;
        doc.ConnectiveDocumentId__c = cv.Id;
        doc.Status__c = 'PENDINGSIGNING';
        doc.Quote__c=q.Id; 
        doc.FlowId__c ='3bb6a2d1-35db-4a3a-a434-868c01bcca9d';
        insert doc;
    }
    static testmethod void test() {        
        Test.startTest();       
        Batch_InitConnectivePackage cbt = new Batch_InitConnectivePackage();
        Id batchId = Database.executeBatch(cbt);
        Test.stopTest();
        DocumentsForSignature__c doc = [SELECT  Id, Status__c, FlowId__c, Package__c FROM DocumentsForSignature__c Limit 1];
        Package__c pack = [SELECT Id, ConnectivePackageId__c FROM Package__c Limit 1];
        // after the testing stops, assert records were updated properly
        System.assertEquals(doc.FlowId__c,pack.ConnectivePackageId__c);
       
    }
}