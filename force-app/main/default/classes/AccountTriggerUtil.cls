public class AccountTriggerUtil {
    
    public static Map<Id, Integer> getMaisonMeres(List<Account> accounts){
        Map<Id, Integer> counts = new Map<Id, Integer>();
        for(Account a : accounts){
            if(a.Maison_mere__c != null){                
                //on va compter le nombre de maison mère 
                Integer count = counts.get(a.Maison_mere__c);
                if (count == null) 
                    counts.put(a.Maison_mere__c, 1);
                else
                    counts.put(a.Maison_mere__c, count + 1);                
            }
        }
        return counts;
    }
    
    public static void updateMaisonMere(List<Account> accounts, String CRUD){
        Map<Id, Integer> maisonMeres = getMaisonMeres(accounts);
        List<Account> parentAcc = new List<Account>();
        Integer coef = 0;
        if(CRUD.equals('INSERT')){
            coef = 1;
        }
        else if(CRUD.equals('DELETE')){
            coef = -1;
        }
        for(Account a : [SELECT Id,TECH_NbComptesEnfants__c FROM Account WHERE Id in :maisonMeres.keySet()]){
                if(a.TECH_NbComptesEnfants__c != null)
                    a.TECH_NbComptesEnfants__c = a.TECH_NbComptesEnfants__c + (maisonMeres.get(a.Id) * coef);
                else
                    a.TECH_NbComptesEnfants__c = maisonMeres.get(a.Id) * coef;
                parentAcc.add(a);            
        }
        update parentAcc;
    }      
}