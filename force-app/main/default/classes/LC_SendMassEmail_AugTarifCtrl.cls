public with sharing class LC_SendMassEmail_AugTarifCtrl implements Queueable {
private List<Quote> quotes;
private List<Quote> quotesToUpdate;
private List<Task> taskToCreate;
private Map<Id,ContentDocumentLink> mapFileByQuoteId = new  Map<Id,ContentDocumentLink>();
private Map<Id,ContentVersion> mapConversionByFileContentDocumentId = new  Map<Id,ContentVersion>();
private Boolean isThereMissingFile = false;


public LC_SendMassEmail_AugTarifCtrl(List<Quote> quotes) {
	this.quotes = quotes;
}

private void createMapFilesByQuoteId(Set<Id> quotIds){
	system.debug('LC_SendMassEmail_AugTarifCtrl.createMapFilesByQuoteId Start, Params LstQuoteIds --- >   '+quotIds);
	List<ID> lstquoteId = new List<ID>();
	List<ID> lstFileContentDocumentId = new List<ID>();

	// List<Quote> lstquote=[SELECT id, Name,ContactId,Opportunity.Salesman__c,Opportunity.OwnerId,Opportunity.Owner.Email,
	//                       Opportunity.Owner.Name,Email, QuoteCode__c,TECH_UID__c,Opportunity.Filiale__c,Account.Name FROM Quote WHERE Id IN:quotIds];

	Integer filesSize = 0;
	for(ContentDocumentLink f : [SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLink where LinkedEntityId IN : quotIds]) {
		lstFileContentDocumentId.add(f.ContentDocumentId);
		mapFileByQuoteId.put(f.LinkedEntityId, f);
		filesSize ++;
	}

	
	for(ContentVersion c : [SELECT title, versiondata, FileExtension, ContentDocumentId FROM contentversion WHERE ContentDocumentId IN :lstFileContentDocumentId]) {
		mapConversionByFileContentDocumentId.put(c.ContentDocumentId,c);
	}

//Check if all quotes have a file
	if(quotIds.size() != filesSize) {
		isThereMissingFile = true;
	}

	system.debug('LC_SendMassEmail_AugTarifCtrl.createMapFilesByQuoteId.mapFileByQuoteId --- >   '+mapFileByQuoteId);
	system.debug('LC_SendMassEmail_AugTarifCtrl.createMapFilesByQuoteId.mapConversionByFileContentDocumentId --- >   '+mapConversionByFileContentDocumentId);

}

//-----------The main method to send------------------//
public void execute(QueueableContext context) {

	system.debug('LC_SendMassEmail_AugTarifCtrl.execute --- > Start ');
	List<Quote> quotesToUpdate = new  List<Quote>();
	List<Task> taskToCreate = new  List<Task>();
	List<Id> lstContactId = new List<Id>();
	List<Id> lstFilesId = new List<Id>();
	Map<Id,ContentVersion> mapContentVersionByQuoteId = new  Map<Id,ContentVersion>();
	List<ContentVersion> ContentVersionToCreate = new  List<ContentVersion>();
	Map<Id,Quote> mapQuoteById = new  Map<Id,Quote>();
	Map<Id,Id> mapSalesmanByQuoteId = new  Map<Id,Id>();
	Map<Id,Id> mapassistantesByQuoteId = new  Map<Id,Id>();
	Map<Id,User> mapSalesmanById = new  Map<Id,User>();
	Map<Id,User> mapassistantesById = new  Map<Id,User>();
	Map<Id,List<ContentVersion> > mapFileToSendByContactId = new  Map<Id,List<ContentVersion> >();
	Map<Id,List<String> > mapLinkToSendByContactId = new  Map<Id,List<String> >();
	Map<Id,List<String> > mapConnectiveTextToSendByContactId = new  Map<Id,List<String> >();
	Map<Id,List<String> > mapLinkBodyToSendByContactId = new  Map<Id,List<String> >();
	//Map<Id,Activity> mapLinkQuoteActivity = new Map<Id,Activity>();
	Set<Id> quotesIds = new Set<Id>();
	List<Messaging.SingleEmailMessage> EmailsToSend =  new List<Messaging.SingleEmailMessage>();
	Integer numberSelectedQuote;
	//Integer NextYear = System.today().year()+1;
	Integer NextYear = 2020; //à remplacer par la ligne au-dessus. Modif faite lors du passage en 2020.
	RecordType rt=[SELECT Id, Name, DeveloperName, Description FROM RecordType Where DeveloperName='Devis_approuv_Read_Only' LIMIT 1];
	numberSelectedQuote = quotes.size();
	system.debug('LC_SendMassEmail_AugTarifCtrl.execute --- > quotes size '+quotes.size());
	User Assistante=[SELECT Id, Name,email,Signature FROM User WHERE Id = :UserInfo.getUserId()];

	if(numberSelectedQuote == 1) {
		Quote q = quotes[0]; 
		//create Email content
		Contact cont = [SELECT Id, Name,email FROM Contact WHERE Id = :q.ContactId];
		//Opportunity opp = [SELECT Id, Name, Account.Name, Filiale__c FROM Opportunity WHERE Id = :q.Opportunity.Id];
		mapQuoteById.put(q.Id,q);
		createMapFilesByQuoteId(mapQuoteById.keySet());
		String subject ='Convention '+NextYear+ ' - '+q.Opportunity.Filiale__c+' - '+q.Account.Name;

		if((mapFileByQuoteId.get(q.Id) != null)){
			mapContentVersionByQuoteId.put(q.Id,mapConversionByFileContentDocumentId.get(mapFileByQuoteId.get(q.Id).ContentDocumentId));
		}
		
		ContentVersion cversion = mapContentVersionByQuoteId.get(q.Id);
		User salesMan  = [SELECT Id, Name,Signature,email FROM User WHERE Id = :q.Opportunity.Salesman__c];		

		//Update quote
		q.Tech_UID__c=GuidUtil.NewGuid();
		q.TECH_isApprouved__c=true;
		q.Status='Envoyé pour signature';
		q.RecordTypeId=rt.Id;		
		quotesToUpdate.add(q);

		//Update quotes
		if(quotesToUpdate.size() > 0) {
			updateLockedRecords updateQuoteRecords = new updateLockedRecords();
			updateQuoteRecords.updateQuoteRecords(quotesToUpdate);			
		}		
		
		Messaging.SingleEmailMessage email = createSimpleMail(cont,Assistante, salesMan,  subject,  cversion,  q,  NextYear );
		EmailsToSend.add(email);

	//create task
		Task t = createEmailActivity(email.subject,email.htmlBody,cont.Id,q.Id);
		taskToCreate.add(t);

	//Create Task activitie
		if(taskToCreate.size() > 0) {
			insert taskToCreate;
		}
		if(cversion != null) {
			 ContentDocumentLink ctLink = linkQuoteFileToTask(taskToCreate[0].Id, cversion);
		 	insert ctLink;
		}
		
	
		//Send Emails
		if(EmailsToSend.size() > 0) {			
			Messaging.sendEmail(EmailsToSend);
		}

	}else if(numberSelectedQuote > 1) { //Package Email
		for (Quote q : quotes) {
			mapQuoteById.put(q.Id,q);
			mapSalesmanByQuoteId.put(q.Id,q.Opportunity.Salesman__c);
			mapassistantesByQuoteId.put(q.Id,q.TECH_Assistante__c);
			lstContactId.add(q.ContactId);
		}

		List<Contact> contacts = [SELECT Name,Email, (SELECT Name,Quote_Type__c,TECH_UID__c,QuoteCode__c,Email,OpportunityId, Opportunity.Filiale__c,AccountId,Account.Name,Commercial__c,ContactId,Contact.Name,TotalPrice,Status,Opportunity.Salesman__c, IsSyncing From Quotes WHERE Id IN :mapQuoteById.keySet()) From contact Where Id IN:lstContactId];
		//Map All contact's Quotes Files
		createMapFilesByQuoteId(mapQuoteById.keySet());  //Get the Assistante

		system.debug('LC_SendMassEmail_AugTarifCtrl.execute.Assistantes --- > Assistantes size '+Assistante);

		List<User> Users=[SELECT Id, Name,Signature,email FROM User WHERE Id IN:mapSalesmanByQuoteId.Values()];

		system.debug('LC_SendMassEmail_AugTarifCtrl.execute --- > Users size '+Users.size());
		for(User u : Users) {
			mapSalesmanById.put(u.Id, u);
		}

		system.debug('LC_SendMassEmail_AugTarifCtrl.execute --- > mapFileByQuoteId size '+mapFileByQuoteId.size());
		system.debug('LC_SendMassEmail_AugTarifCtrl.execute --- > mapConversionByFileContentDocumentId size '+mapConversionByFileContentDocumentId.size());
		system.debug('LC_SendMassEmail_AugTarifCtrl.execute --- > contacts size '+contacts.size());
		system.debug('LC_SendMassEmail_AugTarifCtrl.execute --- > Assistantes size '+quotes.size());
		system.debug('LC_SendMassEmail_AugTarifCtrl.execute --- > Users size '+Users.size());

		for(User u : Users) {
			mapSalesmanById.put(u.Id, u);
		}

		system.debug('LC_SendMassEmail_AugTarifCtrl.execute --- > mapFileByQuoteId size '+mapFileByQuoteId.size());
		system.debug('LC_SendMassEmail_AugTarifCtrl.execute --- > mapConversionByFileContentDocumentId size '+mapConversionByFileContentDocumentId.size());
		system.debug('LC_SendMassEmail_AugTarifCtrl.execute --- > contacts size '+contacts.size());

		for(contact c : contacts) {
			List<ContentVersion> tempFileTosend = new List<ContentVersion>();
			List<String> tempLinkTosend = new List<String>();		

			system.debug('LC_SendMassEmail_AugTarifCtrl.execute --- >  c.quotes.size() size '+ c.quotes.size());
			String subject ='Conventions '+NextYear;

			if( c.quotes.size() > 1) {				
				for(Quote q : c.quotes) {
					//Map the content with quote ID
					//Map<Id,ContentVersion>  test = mapConversionByFileContentDocumentId.get()
					ContentDocumentLink mapFileByQuote =  mapFileByQuoteId.get(q.Id);
					if(mapFileByQuote != null)      {
						mapContentVersionByQuoteId.put(q.Id,mapConversionByFileContentDocumentId.get(mapFileByQuoteId.get(q.Id).ContentDocumentId));
					}

					//Update quote
					q.Tech_UID__c=GuidUtil.NewGuid();
					q.TECH_isApprouved__c=true;
					q.Status='Envoyé pour signature';
					q.RecordTypeId=rt.Id;
                    
					String link='<a href="'+label.CommunityORGURL+'/SignatureDevis/s/sendsignatureform?UID='+q.TECH_UID__c+'&ID='+q.Id+'">';
					String LinkBody='Portail Signature de la convention  - '+q.QuoteCode__c+' </a>'+'<br>';
					//Connective Links
					tempLinkTosend.add(link+LinkBody);                    
                   
					//Liste files Attachement to send
					if((mapFileByQuoteId.get(q.Id).ContentDocumentId) != null) {
                        
                        system.debug('***In the If');
						tempFileTosend.add(mapConversionByFileContentDocumentId.get(mapFileByQuoteId.get(q.Id).ContentDocumentId));
					}


				
					Task t = createEmailActivity(subject,'Conventions envoyées par package...',c.Id,q.Id);
					taskToCreate.add(t);

					

					quotesToUpdate.add(q);
				}
                    //Add the current contact package email			
                Messaging.SingleEmailMessage email = createPackageMail(c, assistante, subject, tempLinkTosend, tempFileTosend, NextYear );
                EmailsToSend.add(email);
            }
            else if(c.quotes.size() == 1){
                Quote q = c.quotes[0];                
                String subj ='Convention '+NextYear+ ' - '+q.Opportunity.Filiale__c+' - '+q.Account.Name;
        
                if((mapFileByQuoteId.get(q.Id) != null)){
                    mapContentVersionByQuoteId.put(q.Id,mapConversionByFileContentDocumentId.get(mapFileByQuoteId.get(q.Id).ContentDocumentId));
                }
                
                ContentVersion cversion = mapContentVersionByQuoteId.get(q.Id);
                User salesMan  = [SELECT Id, Name,Signature,email FROM User WHERE Id = :q.Opportunity.Salesman__c];		
        
                //Update quote
                q.Tech_UID__c=GuidUtil.NewGuid();
                q.TECH_isApprouved__c=true;
                q.Status='Envoyé pour signature';
                q.RecordTypeId=rt.Id;		
                quotesToUpdate.add(q);       
               
                
                Messaging.SingleEmailMessage email = createSimpleMail(c,Assistante, salesMan,  subj,  cversion,  q,  NextYear );
                EmailsToSend.add(email);
        
            	//create task
                Task t = createEmailActivity(email.subject,email.htmlBody,c.Id,q.Id);
                taskToCreate.add(t);       
            	
                /*if(cversion != null) {
                     ContentDocumentLink ctLink = linkQuoteFileToTask(taskToCreate[0].Id, cversion);
                    insert ctLink;
                }*/        
            }
			
		}	

		//Update quotes
		if(quotesToUpdate.size() > 0) {
            for(Quote q: quotesToUpdate)
            {System.debug('Quotes to Update List : '+ quotesToUpdate);}
			updateLockedRecords updateQuoteRecords = new updateLockedRecords();
			updateQuoteRecords.updateQuoteRecords(quotesToUpdate);
		}

		//Create Task activitie
		if(taskToCreate.size() > 0) {
			insert taskToCreate;
		}

		//Send Emails
		if(EmailsToSend.size() > 0) {
			Messaging.sendEmail(EmailsToSend);
		}

		//Add Quotes files to related tasks
	/*	for(Task t : taskToCreate ) {
			ContentVersionToCreate.add(mapContentVersionByQuoteId.get(t.WhatId));
		}

		//Create ContentLinks
		if(ContentVersionToCreate.size() > 0) {
			insert ContentVersionToCreate;
		} */

	}	


	system.debug('LC_SendMassEmail_AugTarifCtrl.execute --- > End ');
}



private Messaging.SingleEmailMessage createSimpleMail(Contact cont, User ass, User salesMan, String subject, ContentVersion cversion, Quote q, Integer NextYear ){

	String contactGreeting ='Cher client,<br><br>';
	String CommercialText= salesMan.Name+' vous prie de trouver ci-joint votre convention. ';        //+NextYear+'. ';
	String ConnectiveText='<br><br>'+q.Opportunity.Filiale__c+' vous invite, via Connective, à signer votre convention en cliquant sur le lien ci-dessous :<br>';
	String link='<a href="'+label.CommunityORGURL+'/SignatureDevis/s/sendsignatureform?UID='+q.TECH_UID__c+'&ID='+q.Id+'">';
	String secheConnect='<br>Ce lien vous redirige sur un formulaire Séché Connect pré-rempli avec vos éléments; si toutefois vous n\'êtes pas le signataire de la convention vous pouvez remplacer vos coordonnées par celles dudit signataire.<br><br> Une fois ce formulaire rempli, vous recevrez un mail de la part de "no-reply@connective.eu", service de signature électronique sécurisée choisi par Séché Environnement dans le cadre de son programme de transformation digitale.';
	String LinkBody='Portail Signature de la Convention '+NextYear+' - '+q.QuoteCode__c+' </a>'+'<br>';
	String notContact='<br><br><i> Séché Environnement collecte et traite des données à caractère personnel dans le cadre de la gestion des contrats et des obligations légales et réglementaires s’y rattachant. Elles sont uniquement destinées à nos services internes et à nos filiales habilitées. Conformément au Règlement Général sur la Protection des Données, le Groupe met à disposition des droits d’accès, de rectification et d’opposition sur les données à caractère personnel. Il est possible d\'exercer ces droits en contactant notre Délégué à la Protection des Données par courrier électronique adressé à <a href="dp@groupe-seche.com">dp@groupe-seche.com</a> , qui traitera votre demande. Pour plus de détails sur l’engagement du Groupe :  <a href="https://www.groupe-seche.com/fr/mentions-legales">RGPD</a>';
	String greeting= '<br><br>Cordialement,<br><br>';
	String userSignature;
	/*if(salesMan.Signature!=Null) {
		userSignature ='<pre>'+salesMan.Signature+'</pre>';
	}else{
		userSignature=' ';
	} */

	String body = contactGreeting+CommercialText+ConnectiveText+link+LinkBody+secheConnect+greeting;//+userSignature;

	Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();

	List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
	Messaging.Emailfileattachment fileAttachment = new Messaging.Emailfileattachment();

	if(cversion != null){
		fileAttachment.setFileName(cversion.title+'.'+cversion.FileExtension);
		fileAttachment.setBody(cversion.versiondata);
		message.setFileAttachments(new Messaging.Emailfileattachment[] {fileAttachment});
	}
	

	String fromAddresses = ass.Email;
	String toAddresses = cont.Email;
	String cciEmail = salesMan.Email;

	system.debug('LC_SendMassEmail_AugTarifCtrl.execute.createSimpleMail.fromAddresses --- >  '+fromAddresses);
	system.debug('LC_SendMassEmail_AugTarifCtrl.execute.createSimpleMail.toAddresses --- >  '+toAddresses);
	system.debug('LC_SendMassEmail_AugTarifCtrl.execute.createSimpleMail.cciEmail --- >  '+cciEmail);
	//String bccaddresses = salesMan.Email;
	// Add to attachment file list	
	message.setReplyTo(fromAddresses);
	message.toAddresses = new String[] {toAddresses};
	message.ccaddresses = new String[] {cciEmail};
	message.bccaddresses = new String[] {ass.Email};
	message.subject = subject;
	message.setSenderDisplayName(salesMan.Name);
	message.htmlBody = body;
	message.setTargetObjectId(cont.Id);
	message.setSaveAsActivity(true);

	return message;
}

private Messaging.SingleEmailMessage createPackageMail(Contact cont, User ass, String subject, List<String> tempLinkTosend, List<ContentVersion> tempFileTosend, Integer NextYear){
	System.debug('***LinkPackage  +'+tempLinkTosend);
	String contactGreeting;
	String CommercialText;
	String secheIndustryText;
	String link ='';
	String secheConnect;
	String greeting;

	contactGreeting ='Cher client,<br><br>';
	CommercialText='Nous vous prions de bien vouloir trouver ci-joint vos conventions ';//+NextYear+'. ';
	secheIndustryText='et vous invitons, via Connective, à les signer en cliquant sur les liens ci-dessous :<br>';

	system.debug('LC_SendMassEmail_AugTarifCtrl.execute.createPackageMail.tempLinkTosend --- >  '+tempLinkTosend);
	for(String li : tempLinkTosend) {
		link=link+li;
	}

	secheConnect='<br>Ces liens vous redirigent sur un formulaire Séché Connect pré-rempli avec vos éléments; si toutefois vous n\'êtes pas le signataire des conventions vous pouvez remplacer vos coordonnées par celles dudit signataire.<br><br> Une fois ce formulaire rempli, vous recevrez un mail de la part de "no-reply@connective.eu", service de signature électronique sécurisée choisi par Séché Environnement dans le cadre de son programme de transformation digitale.';
	greeting= '<br><br>Cordialement,<br><br>';
	String userSignature;
	/*if(ass.Signature!=Null) {
		userSignature ='<pre>'+ass.Signature+'</pre>';
	}else{
		userSignature=' ';
	} */

	String body = contactGreeting+CommercialText+secheIndustryText+link+secheConnect+greeting;//+userSignature;

	Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();

	List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
	List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
	system.debug('LC_SendMassEmail_AugTarifCtrl.execute.createPackageMail.tempFileTosend --- >  '+tempFileTosend.size());


	for(ContentVersion cversion : tempFileTosend) {
		Messaging.EmailFileAttachment fileAttachment = new Messaging.EmailFileAttachment();			
		fileAttachment.setFileName(cversion.title+'.'+cversion.FileExtension);
		fileAttachment.setBody(cversion.versiondata);
		// Add to attachment file list
		fileAttachments.add(fileAttachment);
	
	}

	message.setFileAttachments(fileAttachments);

	String fromAddresses = ass.Email;
	String toAddresses = cont.Email;
	String cciEmail = ass.Email; 

	message.setReplyTo(fromAddresses);
	message.toAddresses = new String[] {toAddresses};
	message.ccaddresses = new String[] {cciEmail};
	message.subject = subject;
	message.setSenderDisplayName(ass.Name);
	message.htmlBody = body;
	message.setTargetObjectId(cont.Id);
	message.setSaveAsActivity(true);

	return message;
}

private Task createEmailActivity(String subject,String description, String contactId, String quoteId) {
	Task t = new Task(
		Subject = subject,
		Description = description.escapeHtml4(),
		WhoId = contactId,
		WhatId = quoteId,
		TaskSubtype = 'Email',
		ActivityDate = Date.today(),
		Status = 'Completed');

	return t;
}


private ContentDocumentLink linkQuoteFileToTask(String taskId, ContentVersion cversion) {
	// Create Related file
	ContentDocumentLink cdl = new ContentDocumentLink();
	cdl.ContentDocumentId = cversion.ContentDocumentId;
	cdl.LinkedEntityId = taskId;
	cdl.ShareType = 'V';
	return cdl;
}

// Class for updating locked record (without sharing to by pass locked record)
  without sharing class updateLockedRecords {	  
    public void updateQuoteRecords(List<Quote> lstQuotes) { 		
		update lstQuotes; 
	}
  }


}