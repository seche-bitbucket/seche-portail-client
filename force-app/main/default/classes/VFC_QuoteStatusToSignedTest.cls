/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-03-18
* @modified       2018-03-18
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for quote status to signed step controller
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@IsTest
public class VFC_QuoteStatusToSignedTest {
    static Account a;
    static Contact c;
    static Opportunity opp;
    static Quote q;
    static OpportunityLineItem oli;
    static  PricebookEntry pbe;
    @isTest static void TestupdateQuote(){
        a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        insert a;
        
        c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        q = new Quote();
        
        q.OpportunityId = opp.ID;
        q.Name = opp.Name;
        q.DateDevis__c = Date.today();
        q.ContactId = opp.ContactName__c;
        q.Email = opp.ContactName__r.Email;
        q.Phone = opp.ContactName__r.Phone;
        q.Fax = opp.ContactName__r.Fax;
        q.Status='Approuvé';
        q.BillingStreet = opp.Account.BillingStreet;
        q.BillingCity = opp.Account.BillingCity;
        q.BillingState = opp.Account.BillingState;
        q.BillingPostalCode = opp.Account.BillingPostalCode;
        q.BillingCountry = opp.Account.BillingCountry; 
        insert q;
        Test.startTest();
        
        
        PageReference pageRef = Page.VFP_QuoteStatusToSigned;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',q.id);
        ApexPages.StandardController sc = new ApexPages.standardController(q);
        VFC_QuoteStatusToSigned  stdController = new VFC_QuoteStatusToSigned(sc);
        PageReference pageResult =  stdController.updateQuote();
        Test.stopTest();
        Quote quo = [Select ID, name,Status FROM QUOTE WHERE ID = : q.Id];
        System.debug(quo.Status);
        System.assertEquals('Signé', quo.Status);
    }
}