@IsTest
public class LeadTriggerHandlerTest {
    
     @testSetup
    static void createTestData() {
        
        Account ac = new Account();
	ac.Name = 'testAccName';
	ac.CustomerNature__c = 'Administration';
	ac.Industry = 'Aéronautique';
	ac.CurrencyIsoCode = 'EUR';
	ac.Producteur__c = false;
	ac.BillingPostalCode = '00000';
	ac.NAF_Number__c = '1234A';
	insert ac;

	Contact contactTest = new Contact();
	contactTest.LastName = 'testName';
	contactTest.AccountId = ac.Id;	
	contactTest.Email = 'aaa@yopmail.com';
	//contactTest.Phone = '0000000000';
	insert contactTest;

        // Get the pricebook id
        Id pricebookId = Test.getStandardPricebookId();
        
        //Create your product
        Product2 prod = new Product2(
             Name = 'Product X',
             ProductCode = 'Pro-X',
             TECH_ExternalID__c = '012345',
             isActive = true
        );
        insert prod;
        
        //Create your pricebook entry
        PricebookEntry pbEntry = new PricebookEntry(
             Pricebook2Id = pricebookId,
             Product2Id = prod.Id,
             UnitPrice = 100.00,
             IsActive = true
        );
        insert pbEntry;
        
        Opportunity opp = new Opportunity(
            Name = 'Test Opp',
            CloseDate = System.today(),
            StageName = 'Closed Won',
            AccountId =ac.Id
        );
        insert opp;
        
        //create your opportunity line item.  
        OpportunityLineItem oli = new OpportunityLineItem(
        
             OpportunityId = opp.Id,
             Quantity = 200,
             PricebookEntryId = pbEntry.Id,
             TotalPrice = 5 * pbEntry.UnitPrice,
             Packaging__c = 'Palette',
             Sector__c ='Biocentre',
             Classe__c ='test'
             
        );
        insert oli;

        List<Regions__c> listReg = new List<Regions__c>();
        listReg.add(new Regions__c(Code__c='970',FilialeTriadis__c='Séché Eco Industries Changé', FiliereTriadis__c='Biocentre',SHC_Subsidiary__c='Alcea',Name='Test'));
         listReg.add(new Regions__c(Code__c='980',FilialeTriadis__c='Séché Eco Industries Changé', FiliereTriadis__c='Biocentre',SHC_Subsidiary__c='Alcea',Name='Test'));
          insert listReg;
    }

    static  User createUserTest(){
        //Get a profile to create User
        Profile p = [select id from profile where Name IN ('System Administrator', 'Administrateur Système')  limit 1];
        
        //Create a User for the Test 
        String testemail = 'current_-_User_7_test@seche.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'Europe/Paris',
                                    alias='cspu', lastname='lastname', IsActive=true);
        insert currentUser;
        return currentUser ;
        
    } 
 
    static PriceBook2 createPriceBookStkDD(){
        
        //Create PriceBook Stockage DD
        Pricebook2 pbkStDD= new Pricebook2(Name=Constants.STR_PRB_NAME_GESTION_DND, isActive=true);
        insert pbkStDD;
        Return pbkStDD;
    }

    static PriceBook2 createPriceBookSHC(){
        
        //Create PriceBook Stockage DD
        Pricebook2 pbkStDD= new Pricebook2(Name=Constants.STR_PRB_NAME_SHC, isActive=true);
        insert pbkStDD;
        Return pbkStDD;
    }
        
    
    static PriceBook2 createPriceBookPtfm(){
        
        //Create PriceBook Plateforme
        Pricebook2 pbkPtfm= new Pricebook2(Name='Plateforme', isActive=true);
        insert pbkPtfm;
        Return pbkPtfm;
    }
    
    
    static product2 createDefaultProduct(){
        
        //Create Product
        Product2 prd = new product2(name = 'Test' ,TECH_ExternalID__c = 'DEFAULT');
        insert prd;
        Return prd;
        
    }
    
    static product2 createProductTraitement(){
        
        //Create Product
        Product2 prd = new product2(name = 'Test' ,Param_3_Text__c='test', Family=Constants.STR_PRD_FAMILY_TRAITEMENT,TECH_ExternalID__c = 'TRAITEMENT');
        insert prd;
        Return prd;
        
    }

    static product2 createProductSHC(){
        
        //Create Product
        Product2 prd = new product2(name = 'Test' ,Param_3_Text__c='BidonJaune',Param_1_Num_Min__c=20,Param_4_Text__c='Alcea', Family=Constants.STR_PRD_FAMILY_TRAITEMENT,TECH_ExternalID__c = 'TRAITEMENT');
        insert prd;
        Return prd;
        
    }
    
    static PriceBookEntry createStandardPriceBookEntry(Product2 prd){
        
        //Create a pricebookentry for a standard pricebook
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prd.Id, UnitPrice = 1000, IsActive = true);
        insert standardPBE;
        Return standardPBE;
        
    }
    
    static PriceBookEntry createPriceBookEntry (product2 prd, priceBook2 pbk){
        
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pbk.Id, Product2Id = prd.Id, UnitPrice = 1000, IsActive = true);
        insert pbe;
        return pbe;
    }
    
    
    
    @IsTest
    public static void testLeadConvertGestionDD(){
        
        User currentUser = createUserTest(); 
        System.RunAs(currentUser){
            //Create Lead
            Lead lea = new Lead (LastName = 'Test Last Name', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Internet', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_DND),PostalCode = '69009',
                                 Waste__c = 'Déchet Amiante',AsbestosPackagingType__c = 'Palette',Quantity__c = 200,AsbestosPackagingSize__c = '3*4', Filiale__c = 'Séché Eco Industries Changé',Filiere__c = 'Biocentre',
                                 Packaging_1__c = 'Benne', WasteType1__c = 'test', Volume_1__c = '25 L', Estimated_Duration_Of_Commissioning__c = Constants.STR_DURATION_15_30,
                                 CustomerNature__c = 'Industriel', Industry = 'Aéronautique');
            
            Insert lea;
            
            PriceBook2 pbkStDD = createPriceBookStkDD();
            
            PriceBook2 pbkPtfm = createPriceBookPtfm();
            
            Product2 prd = createDefaultProduct();

            Product2 prd2 = createProductTraitement();
            
            PricebookEntry standardPBE = createStandardPriceBookEntry(prd);

            PricebookEntry standardPBE2 = createStandardPriceBookEntry(prd2);
            
            PricebookEntry pbeStDD = createPriceBookEntry(prd2,pbkStDD);
            
            PricebookEntry pbePtfm = createPriceBookEntry(prd,pbkPtfm);

            test.startTest();
            
            conversionLead(lea);
            
            List <opportunityLineItem> opli = [select Packaging__c,Quantity,Sector__c from opportunityLineItem];
            
            System.assertEquals(2, opli.size());
            System.assertEquals('Palette', opli[0].Packaging__c);
            System.assertEquals(200, opli[0].Quantity);
            System.assertEquals('Biocentre', opli[0].Sector__c);
            
            test.stopTest();
            
        }        
        
    }
    @IsTest
    public static void testLeadConvertTRIADIS(){
        
        User currentUser = createUserTest(); 
        System.RunAs(currentUser){           
            //Create Lead
            Lead lea = new Lead (LastName = 'Test Last Name', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Internet', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_TRIADIS_AMIANTE),PostalCode = '69009',
                                 Waste__c = 'Déchet Amiante',AsbestosPackagingType__c = 'Palette',Quantity__c = 200,AsbestosPackagingSize__c = '3*4', Filiale__c = 'Alcea',Filiere__c = 'DASRI',
                                 CustomerNature__c = 'Industriel', Industry = 'Aéronautique');
            
            Insert lea;
            
            PriceBook2 pbkStDD = createPriceBookStkDD();
            
            PriceBook2 pbkPtfm = createPriceBookPtfm();
            
            Product2 prd = createDefaultProduct();
            
            PricebookEntry standardPBE = createStandardPriceBookEntry(prd);
            
            PricebookEntry pbeStDD = createPriceBookEntry(prd,pbkStDD);
            
            PricebookEntry pbePtfm = createPriceBookEntry(prd,pbkPtfm);

            test.startTest();
            
            conversionLead(lea);         
            List <opportunityLineItem> opli = [select Packaging__c,Quantity,Sector__c from opportunityLineItem];
            
            System.assertEquals(2, opli.size());
            System.assertEquals('Palette', opli[0].Packaging__c);
            System.assertEquals(200, opli[0].Quantity);
            System.assertEquals('Biocentre', opli[0].Sector__c);
            
            test.stopTest();
            
        }        
        
    }
    @IsTest
    public static void testLeadConvertSHC(){
        
        User currentUser = createUserTest(); 
        System.RunAs(currentUser){          
            //Create Lead
            Lead lea = new Lead (LastName = 'Test Last Name', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Internet', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_SHC_DASRI),PostalCode = '69009',
                                 Waste__c = 'Déchet Amiante',AsbestosPackagingType__c = 'Palette',Quantity__c = 200,AsbestosPackagingSize__c = '3*4', Filiale__c = 'Alcea',Filiere__c = 'DASRI',
                                 Packaging_1__c = 'BidonJaune', Quantity_1__c = 2, Volume_1__c = '20', CustomerNature__c = 'Industriel', Industry = 'Aéronautique');
            
            Insert lea;
            
            PriceBook2 pbkSHC = createPriceBookSHC();
            
            PriceBook2 pbkPtfm = createPriceBookPtfm();
            
            Product2 prd = createDefaultProduct();

            Product2 prd2 = createProductSHC();
            
            PricebookEntry standardPBE = createStandardPriceBookEntry(prd);

            PricebookEntry standardPBE2 = createStandardPriceBookEntry(prd2);
            
            PricebookEntry pbeSHC = createPriceBookEntry(prd2,pbkSHC);
            
            PricebookEntry pbePtfm = createPriceBookEntry(prd,pbkPtfm);

            test.startTest();
            
            conversionLead(lea);         
            List <opportunityLineItem> opli = [select Packaging__c,Quantity,Sector__c from opportunityLineItem];
            
            
            test.stopTest();
            
        }        
        
    }

    @IsTest
    public static void testLeadConvertTender(){
        
        User currentUser = createUserTest(); 
        System.RunAs(currentUser){          
            //Create Lead
            Lead lea = new Lead (LastName = 'Test Last Name', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Internet', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_TENDER),PostalCode = '69009',
                                 Waste__c = 'Déchet Amiante',DeadlineSubmissionOffer__c=DateTime.now().format('dd/MM/yyyy'),AsbestosPackagingType__c = 'Palette',Quantity__c = 200,AsbestosPackagingSize__c = '3*4', Filiale__c = 'Alcea',Filiere__c = 'DASRI',
                                 CustomerNature__c = 'Industriel', Industry = 'Aéronautique');
            
            Insert lea;
            
            test.startTest();
            
            conversionLead(lea);         
            Lead leadConverted = [SELECT ConvertedOpportunityId FROM Lead WHERE Id=:lea.Id];
            Opportunity opp = [SELECT Id,closeDate FROM Opportunity WHERE Id =:leadConverted.ConvertedOpportunityId];         
            
            System.assertEquals(Date.today(), opp.closeDate);
            
            test.stopTest();
            
        }        
        
    }

    @IsTest
    public static void testLeadConvertMiscellaneous(){
        
        User currentUser = createUserTest(); 
        System.RunAs(currentUser){           
            //Create Lead
            Lead lea = new Lead (LastName = 'Test Last Name', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Internet', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_MISCELLANEOUS),PostalCode = '69009',
                                 Waste__c = 'Déchet Amiante',ProspectRequirements__c='test demande',AsbestosPackagingType__c = 'Palette',Quantity__c = 200,AsbestosPackagingSize__c = '3*4', Filiale__c = 'Alcea',Filiere__c = 'DASRI',
                                 CustomerNature__c = 'Industriel', Industry = 'Aéronautique');
            
            Insert lea;
            
            test.startTest();
            
            conversionLead(lea);         
            Lead leadConverted = [SELECT ConvertedOpportunityId FROM Lead WHERE Id=:lea.Id];
            Opportunity opp = [SELECT Id,Informations__c FROM Opportunity WHERE Id =:leadConverted.ConvertedOpportunityId];         
            
            System.assertEquals(lea.ProspectRequirements__c, opp.Informations__c);
            test.stopTest();
        }        
        
    }

    //YMR : test of LeadTriggerHandler.CheckIfNameIsEmpty 
     @IsTest
    public static void testCheckIfNameIsEmpty(){      
        //Create a Lead 
          Lead lea = new Lead (LastName=null, Company = 'CAP',Status = 'Ouverte',LeadSource = 'Internet', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_TENDER),PostalCode = '69009',
          Waste__c = 'Déchet Amiante',AsbestosPackagingType__c = 'Palette',Quantity__c = 200,AsbestosPackagingSize__c = '3*4', Filiale__c = 'Séché Eco Industries Changé',Filiere__c = 'Biocentre');
       
        //Create a Lead list
         List<Lead> leadList = new List<Lead>();
         leadList.add(lea);
         LeadTriggerHandler.CheckIfNameIsEmpty(leadList);
         //insert leadList;
         for(Lead l: leadList){
         system.assertEquals(ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_TENDER), l.RecordTypeId);    
         system.assertEquals('[non fourni]', l.LastName);
             }
    }  
  

     @IsTest
    public static void testMethods(){

        Account ac = [SELECT Id, Name FROM Account LIMIT 1];
          Opportunity opp = [SELECT Id, StageName, CloseDate, AccountId FROM Opportunity LIMIT 1];
        //Get a Lead Record Type
         List<Lead> listLead = new List<Lead>();
         Map<Id,Lead>LeadOldMap = new   Map<Id,Lead>();
        //Create a Lead 
          Lead lea = new Lead (LastName=null, Company = 'CAP',Status = 'Ouverte',LeadSource = 'Internet', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_TENDER),PostalCode = '69009',
          Waste__c = 'Déchet Amiante',AsbestosPackagingType__c = 'Palette',Quantity__c = 200,AsbestosPackagingSize__c = '3*4', Filiale__c = 'Séché Eco Industries Changé',Filiere__c = 'Biocentre',ConvertedOpportunityId = opp.Id, Packaging_1__c='Carton',isConverted = true,ConvertedAccountId =ac.Id,
          CustomerNature__c='Industriel', Industry='Aéronautique');
       
        //Create a Lead list
         List<Lead> leadList = new List<Lead>();
         leadList.add(lea);
         listLead.add(lea);
         
          listLead.add(new Lead(FirstName ='Jean',LastName = 'Martin',Company = 'Capgemini',Status = 'Ouverte',LeadSource = 'Autre',RecordTypeId=ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_TRIADIS_AMIANTE),
         WorkSiteZip__c='123'));
        
         listLead.add(new Lead (FirstName ='Jeane',LastName = 'Martine', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Internet', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_SHC_DASRI),PostalCode = '97000',
          Waste__c = 'Déchet Amiante',AsbestosPackagingType__c = 'Palette',Quantity__c = 200,AsbestosPackagingSize__c = '3*4', Filiale__c = 'Séché Eco Industries Changé',Filiere__c = 'Biocentre'));

           listLead.add(new Lead (FirstName ='Jea',LastName = 'Marte', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Internet', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_DND),PostalCode = '69009',
          Waste__c = 'Déchet Amiante',AsbestosPackagingType__c = 'Palette',Quantity__c = 200,AsbestosPackagingSize__c = '3*4', Filiale__c = 'Séché Eco Industries Changé',Filiere__c = 'Biocentre',ConvertedOpportunityId =Null,isConverted =false,DeadlineSubmissionOffer__c ='test',CustomerNature__c='Industriel', Industry='Chimie'));
           listLead.add(new Lead (FirstName ='Jea',LastName = 'Marte', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Internet', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_MISCELLANEOUS),PostalCode = '69009',
          Waste__c = 'Déchet Amiante',AsbestosPackagingType__c = 'Palette',Quantity__c = 200,AsbestosPackagingSize__c = '3*4', Filiale__c = 'Séché Eco Industries Changé',Filiere__c = 'Biocentre',ConvertedOpportunityId =Null,isConverted =false,DeadlineSubmissionOffer__c ='test',ProspectRequirements__c='test demande',CustomerNature__c='Industriel', Industry='Chimie'));
          listLead.add(new Lead (FirstName ='Jea',LastName = 'Marte', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Internet', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_TENDER),PostalCode = '69009',
          Waste__c = 'Déchet Amiante',AsbestosPackagingType__c = 'Palette',Quantity__c = 200,AsbestosPackagingSize__c = '3*4', Filiale__c = 'Séché Eco Industries Changé',Filiere__c = 'Biocentre',ConvertedOpportunityId =Null,isConverted =false,DeadlineSubmissionOffer__c ='test',CustomerNature__c='Industriel', Industry='Aéronautique'));

         LeadTriggerHandler.setBranchForWebToLead(listLead,null);
         
         insert listLead;
         for(Lead l: leadList){
         system.assertEquals(ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_TENDER), l.RecordTypeId);          
             }

        for(Lead l: listLead){
            if(l.ConvertedOpportunityId != null){
               // l.IsConverted = false;
                l.Packaging_2__c ='Carton';
                l.Packaging_3__c ='Unité';
                l.Packaging_4__c  ='Palette';
                l.Packaging_5__c ='Boite';
            }
                  LeadOldMap.put(l.Id, l);
             }    
    }
    
    public static void conversionLead(Lead lead){
        Database.LeadConvert leaC = new database.LeadConvert();
        leaC.setLeadId(lead.id);
        
        leaC.setDoNotCreateOpportunity(false);
        leadStatus convertStatus = [SELECT Id, ApiName FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        leaC.setConvertedStatus(convertStatus.ApiName);
        
        Database.LeadConvertResult lcr = Database.convertLead(leaC);   
    }

    @IsTest
    public static void testTransfertFilesVisitReport(){
        User currentUser = createUserTest(); 
        System.RunAs(currentUser){           
            //Create Lead
            Lead lea = new Lead (LastName = 'Test Last Name', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Internet', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_MISCELLANEOUS),PostalCode = '69009',
                                 Waste__c = 'Déchet Amiante',ProspectRequirements__c='test demande',AsbestosPackagingType__c = 'Palette',Quantity__c = 200,AsbestosPackagingSize__c = '3*4', Filiale__c = 'Alcea',Filiere__c = 'DASRI',
                                 CustomerNature__c = 'Industriel', Industry = 'Aéronautique');
            
            Insert lea;
            
            test.startTest();
            createFile(lea);
            createVisitReport(lea);
            conversionLead(lea);         
            Lead leadConverted = [SELECT ConvertedOpportunityId FROM Lead WHERE Id=:lea.Id];
            Opportunity opp = [SELECT Id,Informations__c FROM Opportunity WHERE Id =:leadConverted.ConvertedOpportunityId];         
            
            System.assertEquals(lea.ProspectRequirements__c, opp.Informations__c);
            test.stopTest();
        }
    }

    private static void createFile(Lead lea){
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = lea.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
    }

    private static void createVisitReport(Lead lea){
        VisitReport__c vr = new VisitReport__c(CR__c='Le compte rendu',Name='courte description',Lead__c=lea.Id);
        insert vr;
    }
}