/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Lucas DORMOY <lucas.dormoy@capgemini.com>
 * @version        1.0
 * @created        2020-12-11
 * @systemLayer    Manager
 *
 * @update         2020-12-21 by BTO adds creating files methods
 *
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Description: This class contains the utils methods for Questionnaire Lycée
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

public  without sharing class QLY_Utils {
@InvocableMethod(label='Send Latest QLY Document to Contact by Email' description='For the specified SchoolForms, sends the latest ContentDocument linked to the linked Contact by Email.')
public static void SendLatestContentDocumentToContactByEmail(List<QLY_SchoolForm__c> qlyList){
	List<Messaging.SingleEmailMessage> emailList = getEmailWithAttachment(qlyList);
	if(emailList.size() > 0) {
		Messaging.sendEmail(emailList);
	}
}

public static List<Messaging.SingleEmailMessage> getEmailWithAttachment(List<QLY_SchoolForm__c> qlyList){
	List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();

	for(QLY_SchoolForm__c qly : qlyList) {
		List<ContentDocumentLink> contentDocumentLinks = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :qly.Id];
		if(contentDocumentLinks.size() > 0) {
			List<Id> contentDocumentIds = new List<Id>();
			for (ContentDocumentLink contentDocumentLink : contentDocumentLinks) {
				contentDocumentIds.add(contentDocumentLink.ContentDocumentId);
			}
			ContentDocument LatestDoc = [SELECT Id, CreatedDate, Title FROM ContentDocument WHERE Id IN :contentDocumentIds ORDER BY CreatedDate DESC LIMIT 1];
			ContentVersion LatestDocVersion = [SELECT Id, Title, FileType, VersionData, isLatest, ContentDocumentId FROM ContentVersion WHERE isLatest = true AND ContentDocumentId=:latestDoc.Id LIMIT 1];

			Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
			attachment.setBody(LatestDocVersion.VersionData);
			string fileName = LatestDocVersion.Title;
			attachment.setFileName(fileName);
			attachment.setContentType('application/pdf');
			attachment.setInline(false);

			Contact contact = [SELECT Id, Email FROM Contact WHERE Id = :qly.Contact__c LIMIT 1];

			Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
			//email.setTemplateId(emailTemplateId);
			email.setToAddresses(new String[] {contact.Email});
			email.setFileAttachments(new Messaging.EmailFileAttachment[] {attachment});
			email.setSubject('[TRIADIS] Formulaire collecte des déchets. - ' + qly.AccountName__c );
			email.setHtmlBody('Bonjour,</br>'+
								'</br> Vous trouverez joint à ce mail le formulaire que vous avez saisi au format PDF, concernant l'+'\''+'établissement '+qly.AccountName__c+'.</br>'+
								'Nous vous remercions et vous souhaitons une bonne journée  </br>'+

								'</br> TRIADIS Services');

			emailList.add(email);
		}
	}
	return emailList;
}

/*
 * @method uploadFile()
 * @desc Creates a content version from a given file's base64 and name
 *
 * @param {String} base64 - base64 string that represents the file
 * @param {String} filename - full file name with extension, i.e. 'products.csv'
 * @param {String} recordId - Id of the record you want to attach this file to
 *
 * @return {ContentVersion} - returns the created ContentDocumentLink Id if the
 *   upload was successful, otherwise returns null
 */

public static ContentVersion uploadFileToRecord(String base64, String filename, String recordId, String productId) {
	ContentVersion cv = createContentVersion(base64, filename, productId);
	ContentDocumentLink cdl = createContentLink(cv.Id, recordId);
	if (cv == null || cdl == null) { return null; }
	return cv;
}

public static ContentVersion uploadFile(String base64, String filename, String productId) {
	ContentVersion cv = createContentVersion(base64, filename, productId);
	//cv.QLY_Product2__c = productId;
	if (cv == null) { return null; }
	return cv;
}
/*
 * @method createContentVersion() [private]
 * @desc Creates a content version from a given file's base64 and name
 *
 * @param {String} base64 - base64 string that represents the file
 * @param {String} filename - full file name with extension, i.e. 'products.csv'
 *
 * @return {ContentVersion} - returns the newly created ContentVersion, or null
 *   if there was an error inserting the record
 */
private static ContentVersion createContentVersion(String base64, String filename, String productId) {
	ContentVersion cv = new ContentVersion();
	cv.VersionData = EncodingUtil.base64Decode(base64);
	cv.Title = filename;
	cv.PathOnClient = filename;
	cv.QLY_Product2__c = productId;
	try {
		insert cv;
		return cv;
	} catch(DMLException e) {
		System.debug(e);
		return null;
	}
}

/*
 * @method createContentLink() [private]
 * @desc Creates a content link for a given ContentVersion and record
 *
 * @param {String} contentVersionId - Id of the ContentVersion of the file
 * @param {String} recordId - Id of the record you want to attach this file to
 *
 * @return {ContentDocumentLink} - returns the newly created ContentDocumentLink,
 *   or null if there was an error inserting the record
 */
private static ContentDocumentLink createContentLink(String contentVersionId, String recordId) {
	if (contentVersionId == null ) { return null; }
	ContentDocumentLink cdl = new ContentDocumentLink();
	cdl.ContentDocumentId = [
		SELECT ContentDocumentId
		FROM ContentVersion
		WHERE Id =: contentVersionId
	].ContentDocumentId;
	cdl.LinkedEntityId = recordId;
	// ShareType is either 'V', 'C', or 'I'
	// V = Viewer, C = Collaborator, I = Inferred
	cdl.ShareType = 'V';
	try {
		insert cdl;
		return cdl;
	} catch(DMLException e) {
		System.debug(e);
		return null;
	}
}

public static List < Map < String, String >> getPiklistValues(String objApiName, String fieldName) {
    List < Map < String, String >> options = new List < Map < String, String >> ();
    Map < String, Schema.SObjectType > objGlobalMap = Schema.getGlobalDescribe();
    Schema.SObjectType objType = Schema.getGlobalDescribe().get(objApiName);
    if (objType == null) {
        return options;
    }
    Schema.DescribeSObjectResult sobjectDescribe = objType.getDescribe();
    Map < String, Schema.SObjectField > fieldMap = sobjectDescribe.fields.getMap();
    if (fieldMap.get(fieldName) == null) {
        return options;
    }
    List < Schema.PicklistEntry > pickListValues = fieldMap.get(fieldName).getDescribe().getPickListValues();
    for (Schema.PicklistEntry f: pickListValues) {
        Map < String, String > values = new Map < String, String > {
            'label' => f.getLabel(),
            'value' => f.getValue()
        };
        options.add(values);
    }
    return options;
}   

}