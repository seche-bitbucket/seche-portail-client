public class OpportunityController {
    public Contact contact {get; set;}
    public Account account {get; set;}
    public Opportunity opportunity {get; set;}
    public String url {get; set;}

    public OpportunityController(ApexPages.StandardSetController controller){
    opportunity = [SELECT Name, AccountId, ContactName__c, Id FROM Opportunity where ID= :ApexPages.currentPage().getParameters().get('id') limit 1 ];
    
    if(opportunity.ContactName__c != null){
        contact = [SELECT Name FROM Contact where ID= :opportunity.ContactName__c limit 1 ];
    }
    if(opportunity.AccountId != null){
        account = [SELECT Name FROM Account where ID= :opportunity.AccountId limit 1 ];
    }
   }
    public OpportunityController(ApexPages.StandardController controller){
    opportunity = [SELECT Name, AccountId, ContactName__c, Id FROM Opportunity where ID= :ApexPages.currentPage().getParameters().get('id') limit 1 ];
    if(opportunity.ContactName__c != null){
        contact = [SELECT Name FROM Contact where ID= :opportunity.ContactName__c limit 1 ];
    }
    if(opportunity.AccountId != null){
        account = [SELECT Name FROM Account where ID= :opportunity.AccountId limit 1 ];
    }
   }
    public PageReference generateURL() {
       if (opportunity.ContactName__c != null) {
            url = '/' + Label.CR_Object + '/e?' + Label.CR_Compte + '=' + account.Name + '&' + Label.CR_Compte + '_lkid=' + account.ID + '&' + Label.CR_Contact + '=' + contact.Name + '&' + Label.CR_Contact + '_lkid=' + contact.ID + '&' + Label.CR_Opportunite + '=' + opportunity.Name + '&' + Label.CR_Opportunite+ '_lkid=' + opportunity.ID + '&retURL=/' + opportunity.Id;
        } else {
            url = '/' + Label.CR_Object + '/e?' + Label.CR_Compte + '=' + account.Name + '&' + Label.CR_Compte + '_lkid=' + account.ID + '&' + Label.CR_Opportunite + '=' + opportunity.Name + '&' + Label.CR_Opportunite+ '_lkid=' + opportunity.ID + '&retURL=/' + opportunity.Id;
        }
        return new Pagereference(url);
        
    }
    
}