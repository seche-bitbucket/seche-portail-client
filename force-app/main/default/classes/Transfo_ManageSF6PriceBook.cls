public class Transfo_ManageSF6PriceBook{
    
    public static  List<QuoteLine__c> genericQuoteLines = new  List<QuoteLine__c>();
    
    @AuraEnabled
    public static List<QuoteLine__c> getPriceBookEntries(ID serviceDemandID){
        List<QuoteLine__c> results = new List<QuoteLine__c>();
        SD_ServiceDemand__c sd = [Select ID, PriceBook__c FROM SD_ServiceDemand__c WHERE ID = : serviceDemandID];
        List<PricebookEntry> pbes = [Select Id, Name, ProductCode FROM PricebookEntry WHERE PriceBook2ID = : sd.PriceBook__c AND IsActive = true];
        Map<ID, QuoteLine__c> qls = new Map<ID, QuoteLine__c>();
        
        qls = getQuoteLines(serviceDemandID);
        
        for(PricebookEntry pbe : pbes){
            QuoteLine__c ql = new QuoteLine__c();
            if(pbe.ProductCode != 'DEFAUT'){// Avoid to add Generic product in the list
                
                if(qls != null && qls.containsKey(pbe.ID)){
                    ql = qls.get(pbe.ID);
                }else{
                    //ql.ID = null;
                    ql.TECH_PriceBookEntryID__c = pbe.ID;
                    ql.Label__c = pbe.Name;
                    ql.Code__c = pbe.ProductCode;
                    ql.Dimensions__c = '';
                    ql.Tonnage__c = Null;
                }
                results.add(ql);
            }
            
        }
        
        if(genericQuoteLines.size() > 0){
            results.addAll(genericQuoteLines);
        }
        
        genericQuoteLines.clear();
      
        
        return results;
    }
    
    @AuraEnabled
    public static List<QuoteLine__c> saveQuoteLines(String qls, ID serviceDemandID){
        
        List<QuoteLine__c> qlTest = (List<QuoteLine__c>)JSON.deserialize(qls, List<QuoteLine__c>.class);
        
        List<QuoteLine__c> qlToUpdate = new List<QuoteLine__c>();    
        List<QuoteLine__c> qlToRemove = new List<QuoteLine__c>();
        
        for(QuoteLine__c ql : qlTest){
            if(ql.Quantity__c != null && ql.Quantity__c != 0){
                if( ql.ID == null){
                    ql.ServiceDemand__c = serviceDemandID;
                }if(ql.Code__c == ''){
                    ql.Code__c = 'DEFAUT';
                    ql.TECH_PriceBookEntryID__c = Label.PBE_GenericProductID;
                }                
                qlToUpdate.add(ql);
            }else if((ql.Quantity__c == null || ql.Quantity__c == 0) && ql.ID != null ){
                qlToRemove.add(ql);
            }
        }
        
        upsert qlToUpdate;
        delete qlToRemove;
        
        return getPriceBookEntries(serviceDemandID);
    }
    
    
    @AuraEnabled
    public static Boolean isEditableServiceDemand(ID serviceDemandID){
        
        SD_ServiceDemand__c sd = [Select ID, Status__c FROM SD_ServiceDemand__c WHERE ID = : serviceDemandID];
       
        if(sd.Status__c != 'En construction'){
            return false;
        }else{
            return true;
        }
        
        
    }

    
    
    public static Map<ID, QuoteLine__c> getQuoteLines(ID serviceDemandID){
        Map<ID, QuoteLine__c> qls = new Map<ID, QuoteLine__c>();
        List<QuoteLine__c> existingQL = [SELECT ID, Code__c, Label__c, Quantity__c, Dimensions__c, Tonnage__c, TECh_PricebookEntryID__c, ServiceDemand__r.PriceBook__c FROM QuoteLine__c WHERE ServiceDemand__c = : serviceDemandID];
        
        for(QuoteLine__c ql :existingQL){
            if(ql.Code__c != 'DEFAUT'){
                qls.put(ql.TECh_PricebookEntryID__c ,ql);
            }else{
                genericQuoteLines.add(ql);
            }
        }
        
        
        return qls;
        
    }
    
}