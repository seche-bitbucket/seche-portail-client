public with sharing class LC_FipUpdateController {
@AuraEnabled
public String message {get; set;}
@AuraEnabled
public FIP_FIP__c objectFip {get; set;}
@AuraEnabled
public String showToastMode {get; set;}


@AuraEnabled
public static FIP_FIP__c getFip(Id fipId){
	//make your own SOQL here from your desired object where you want to place your lightnign action
	return [SELECT Name, Id, State__c, GenerateFIPPDF__c FROM FIP_FIP__c
	        WHERE Id = : fipId ];
}

@AuraEnabled
public static LC_FipUpdateController setToExploitation(FIP_FIP__c fip){
	LC_FipUpdateController wrapper = new LC_FipUpdateController();
	if(fip.State__c.equals('En attente de qualification') || fip.State__c.equals('Refusée') || fip.State__c.equals('Soumise a SEI')) {
		fip.State__c = 'En attente de exploitation';
		fip.GenerateFIPPDF__c = true;
		upsert fip;
		wrapper.objectFip = fip;
		wrapper.message = 'Soumis à l\'exploitation';
		wrapper.showToastMode = 'success';

	}else{
		wrapper.objectFip = fip;
		wrapper.message = 'Vous ne pouvez pas modifier une FIP qui est en attente de l\'exploitation ou approuvée';
		wrapper.showToastMode = 'warning';
	}
	return wrapper;
}

@AuraEnabled
public static LC_FipUpdateController setToSei(FIP_FIP__c fip){
	LC_FipUpdateController wrapper = new LC_FipUpdateController();
	if(fip.State__c.equals('En attente de qualification')) {
		fip.State__c = 'Soumise a SEI';		
		upsert fip;
		wrapper.objectFip = fip;
		wrapper.message = 'Soumis à l\'exutoire';
		wrapper.showToastMode = 'Success';

	}else{
		wrapper.objectFip = fip;
		wrapper.message = 'Vous ne pouvez pas modifier une FIP qui est en attente du collecteur, de l\'exutoire ou de l\'exploitation';
		wrapper.showToastMode = 'warning';
	}
	return wrapper;
}

@AuraEnabled
public static LC_FipUpdateController sendFipReminder(FIP_FIP__c fip){
	LC_FipUpdateController wrapper = new LC_FipUpdateController();
    String rep = ConnectiveCallout.sendReminderFIP(fip.Id);
	if(rep.equals('OK') ) {		
		wrapper.objectFip = fip;
		wrapper.message = 'Rappel envoyé avec succès';
		wrapper.showToastMode = 'success';

	}else if(rep.equals('NoDocument')){        
		wrapper.objectFip = fip;
		wrapper.message = 'Aucun document envoyé pour signature';
		wrapper.showToastMode = 'warning';
	}
    else{        
		wrapper.objectFip = fip;
		wrapper.message = 'Failed to check status , please contact your administrator';
		wrapper.showToastMode = 'error';
	}
	return wrapper;
}

@AuraEnabled
public static LC_FipUpdateController getFipStatus(FIP_FIP__c fip){
	LC_FipUpdateController wrapper = new LC_FipUpdateController();
    String rep = ConnectiveCallout.retrieveStatus(fip.Id);
	if(rep.equals('OK') ) {		
		wrapper.objectFip = fip;
		wrapper.message = 'Statut envoyé avec succès';

	}else if(rep.equals('FlowIdNull')){        
		wrapper.objectFip = fip;
		wrapper.message = 'Aucun document trouvé';
		wrapper.showToastMode = 'warning';
	}
    else{        
		wrapper.objectFip = fip;
		wrapper.message = 'Failed to check status , please contact your administrator';
		wrapper.showToastMode = 'error';
	}
	return wrapper;
}

@AuraEnabled
public static ListView getListViewByName(String listViewDeveloperName) {    
      return   [SELECT Id, Name FROM ListView WHERE SobjectType = 'FIP_FIP__c' AND DeveloperName = :listViewDeveloperName];    
   
}

}