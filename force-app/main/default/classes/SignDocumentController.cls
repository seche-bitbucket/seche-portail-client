public with sharing class SignDocumentController {
    @AuraEnabled
    public static String retrieveUrl(String q){
        DocumentsForSignature__c doc=[SELECT SigningUrl__c FROM DocumentsForSignature__c Where quote__c=: q];
        String Url= doc.SigningUrl__c;
        System.debug('url is '+ Url);
        return Url;    
    }
}