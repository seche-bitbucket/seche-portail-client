@isTest
public with sharing class SchedulableBatchCreateOppQuoteTest {
   static testMethod void myTestMethod() {        
         test.starttest();
         SchedulableBatchCreateOppQuote myClass = new SchedulableBatchCreateOppQuote ();   
         String chron = '0 20 17 * * ?';        
         system.schedule('Test Sched', chron, myClass);
         test.stopTest();
    }
}