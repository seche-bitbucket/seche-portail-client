@IsTest
public with sharing class AP_AugmentationTarifmanager_Test {
    @isTest
      	static void testSubmitAMEForValidation(){
        AP_AugmentationTarifmanager ctrl = new AP_AugmentationTarifmanager();
        List<Contract__c> listC = new  List<Contract__c>();
        List<Amendment__c> listA = new  List<Amendment__c>();

        Contract__c c1 = new Contract__c();   
        c1.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
        c1.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport;       
        
        Contract__c c2 = new Contract__c();   
        c2.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c2.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 

        Contract__c c3 = new Contract__c();   
        c3.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c3.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c3.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 

       	listC.add(c1); listC.add(c2);  listC.add(c3);
       	insert listC;
		
		Amendment__c a1 = new Amendment__c();
        a1.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;
        a1.Contrat2__c = listC[0].Id ;
        a1.PriceNextYear__c = 10;
        
        Amendment__c a2 = new Amendment__c();
        a2.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
        a2.Contrat2__c = listC[1].Id ;
        a2.PriceNextYear__c = 10; 

        Amendment__c a3 = new Amendment__c();
        a3.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a3.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
        a3.Contrat2__c = listC[2].Id ;
        a3.PriceNextYear__c = 10;

        listA.add(a1); listA.add(a2); listA.add(a3);
        insert listA;
            
	 	String filiale = 'Drimm';
        String filiere = 'Stockage DND'; 
        AP_AugmentationTarifmanager.RequestResponse response = AP_AugmentationTarifmanager.submitAMEForValidation(listC,filiale,filiere);
        
       // System.assertEquals(response.message , Label.ContractsSubmittedForValidation);
        System.assertEquals(response.toastMode , Label.ToastMode_Success);

    }

    @isTest
    static void testSubmitAMEForValidationError(){
        AP_AugmentationTarifmanager ctrl = new AP_AugmentationTarifmanager();
        List<Contract__c> listC = new  List<Contract__c>();
        List<Amendment__c> listA = new  List<Amendment__c>();

        Contract__c c1 = new Contract__c();   
        c1.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;               
        
        Contract__c c2 = new Contract__c();   
        c2.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c2.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 

        Contract__c c3 = new Contract__c();   
        c3.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c3.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c3.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 

       	listC.add(c1); listC.add(c2);  listC.add(c3);
       	insert listC;
		
		Amendment__c a1 = new Amendment__c();
        a1.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;
        a1.Contrat2__c = listC[0].Id ;
        a1.PriceNextYear__c = 10;
        
        Amendment__c a2 = new Amendment__c();
        a2.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
        a2.Contrat2__c = listC[1].Id ;
        a2.PriceNextYear__c = 10; 

        Amendment__c a3 = new Amendment__c();
        a3.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a3.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
        a3.Contrat2__c = listC[2].Id ;
        
        listA.add(a1); listA.add(a2); listA.add(a3);
        insert listA;

        String filiale = 'Drimm';
        String filiere = 'Stockage DND'; 
         AP_AugmentationTarifmanager.RequestResponse response = AP_AugmentationTarifmanager.submitAMEForValidation(listC,filiale,filiere);
        
        System.debug(response);
       

    }
    
     @isTest
    static void testUpdateSa(){
         Contract__c c = new Contract__c();
        c.Filiale__c = 'Tredi Hombourg';
        c.name='Name Contract';
        c.Etat__c = 'Courrier';
        c.Type_de_convention__c ='PF avec transport';
        insert c;      

       
        AP_AugmentationTarifmanager.updateContractsA(c.Id,'PF avec transport');  
        
        c = [select Id, Name, Type_de_convention__c, Etat__c from Contract__c Limit 1];
        
        System.assertEquals(c.Type_de_convention__c, 'PF avec transport');
        
    }

    @isTest
    static void testSetUnderContract() {
    List<Amendment__c> listA = new List<Amendment__c>();
    List<Contract__c> listC = new  List<Contract__c>();

    Contract__c c1 = new Contract__c();   
    c1.Filiale__c = String_Helper.CON_Filiale_Drimm;       
    c1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
    c1.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport;       
    
    Contract__c c2 = new Contract__c();   
    c2.Filiale__c = String_Helper.CON_Filiale_Drimm;       
    c2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
    c2.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 

    Contract__c c3 = new Contract__c();   
    c3.Filiale__c = String_Helper.CON_Filiale_Drimm;       
    c3.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
    c3.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 

    listC.add(c1);
    listC.add(c2); 
    listC.add(c3);
    insert listC;

    Amendment__c a1 = new Amendment__c();
    a1.Filiale__c = String_Helper.CON_Filiale_Drimm;
    a1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;
    a1.Contrat2__c = listC[0].Id ;
    a1.PriceNextYear__c = 10;
    
    Amendment__c a2 = new Amendment__c();
    a2.Filiale__c = String_Helper.CON_Filiale_Drimm;
    a2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
    a2.Contrat2__c = listC[1].Id ;
    a2.PriceNextYear__c = 10; 

    Amendment__c a3 = new Amendment__c();
    a3.Filiale__c = String_Helper.CON_Filiale_Drimm;
    a3.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
    a3.Contrat2__c = listC[2].Id ;
    a3.PriceNextYear__c = 10;

    listA.add(a1);
    listA.add(a2);
    listA.add(a3);
    insert listA;

    List<Amendment__c> returnedListA = AP_AugmentationTarifManager.setAmendmentsUnderContract(listA, true);
    System.assertEquals(3, returnedListA.size());

    AP_AugmentationTarifManager.setContractsUnderContract(listC);

    System.assertEquals(true, AP_AugmentationTarifManager.doAmendmentsHaveNextYearPrice(listA));
    }
    
     @isTest
    static void testupdateContractsConv(){
         Contract__c c = new Contract__c();
        c.Filiale__c = 'Tredi Hombourg';
        c.name='Name Contract';
        c.Etat__c = 'Courrier';
        c.Type_de_convention__c ='PF avec transport';
        insert c;      

       
        AP_AugmentationTarifmanager.updateContractsConv(new List<Contract__c>{c},'PF avec transport');  
        
        c = [select Id, Name, Type_de_convention__c, Etat__c from Contract__c Limit 1];
        
        System.assertEquals(c.Type_de_convention__c, 'PF avec transport');
        
    }

    @isTest
    static void testincreaseAMEFaf(){
        AP_AugmentationTarifmanager ctrl = new AP_AugmentationTarifmanager();
        List<Amendment__c> listA = new list<Amendment__c>();
        List<Contract__c> listC = new list<Contract__c>();  

        Contract__c c1 = new Contract__c();   
        c1.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
        c1.RecordLocked__c=false; 

        listC.add(c1);        
        insert listC ;
		
		Amendment__c a1 = new Amendment__c();
        a1.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;
        a1.Contrat2__c = listC[0].Id ;          

        listA.add(a1);
        insert listA;
        String comment = 'Test Commentaire';
        Test.startTest();
        AP_AugmentationTarifmanager.RequestResponse response = AP_AugmentationTarifmanager.increaseContractFaf(listC,comment,listA);
        Contract__c ctr =[SELECT Face_Face__c,IncreaseComment__c from Contract__c Limit 1];
        Test.stopTest();   
        System.debug(response);
        System.assertEquals(comment , ctr.IncreaseComment__c);
        System.assertEquals(true , ctr.Face_Face__c);

    }


    @isTest 
    static void testIncreaseAMEFixedPrice(){
        
        Contract__c c = new Contract__c();
        c.name='t';
        insert c;
        Amendment__c am1 = new Amendment__c();
        am1.Contrat2__c=c.id;
        am1.Nature__c='Conditionnement';
        am1.PriceCurrentYear__c=120;
        am1.TECH_Excluded__c=false;
        Amendment__c am2 = new Amendment__c();
        am2.Contrat2__c=c.id;
        am2.Nature__c='Traitement';
        am2.PriceCurrentYear__c=120;
        am2.TECH_Excluded__c=false;
        Amendment__c am3 = new Amendment__c();
        am3.Contrat2__c=c.id;
        am3.Nature__c='Prestation';
        am3.PriceCurrentYear__c=120;
        am3.TECH_Excluded__c=false;
        List<Amendment__c> amendments = new List<Amendment__c>();
        Amendments.add(am1);
        Amendments.add(am2);
        Amendments.add(am3);
        insert amendments;

        String comment = 'Test Commentaire';
        
        Test.startTest();
        AP_AugmentationTarifmanager.RequestResponse response = AP_AugmentationTarifmanager.increaseAMEFixedPrice(amendments, 130, 0, 0, 0,comment);       
        
        list<Amendment__c> am=[SELECT PriceNextYear__c,PriceCurrentYear__c,CompletedAmadement__c, Contrat2__r.RecordLocked__c, Nature__c,TECH_Excluded__c from Amendment__c];
        System.assertEquals(130, am[0].PriceNextYear__c);
        System.assertEquals(130, am[1].PriceNextYear__c);
        System.assertEquals(130, am[2].PriceNextYear__c);
        
        response = AP_AugmentationTarifmanager.increaseAMEFixedPrice(am, 0, 150, 0, 0,comment);    
        list<Amendment__c> amTraitement=[SELECT PriceNextYear__c,CompletedAmadement__c,PriceCurrentYear__c, Nature__c, Contrat2__r.RecordLocked__c,TECH_Excluded__c from Amendment__c where Nature__c='Traitement'];       
        System.assertEquals(130, amTraitement[0].PriceNextYear__c);
        
        response = AP_AugmentationTarifmanager.increaseAMEFixedPrice(am, 0, 0, 0, 150,comment);    
	    list<Amendment__c> amConditionnement =[SELECT PriceNextYear__c,CompletedAmadement__c,PriceCurrentYear__c, Nature__c, Contrat2__r.RecordLocked__c,TECH_Excluded__c from Amendment__c where Nature__c='Conditionnement'];
        System.assertEquals(130, amConditionnement[0].PriceNextYear__c);
        
        response = AP_AugmentationTarifmanager.increaseAMEFixedPrice(am, 0, 0, 150, 0,comment);    
        list<Amendment__c> amPrestation =[SELECT PriceNextYear__c,CompletedAmadement__c,PriceCurrentYear__c, Nature__c, Contrat2__r.RecordLocked__c,TECH_Excluded__c from Amendment__c where Nature__c='Prestation'];
        System.assertEquals(130, amPrestation[0].PriceNextYear__c);
        Test.stopTest(); 
        Contract__c cAfterinsert=[SELECT Id,IncreaseComment__c  from Contract__c where id =: c.Id];
        System.assertEquals(comment, cAfterinsert.IncreaseComment__c);
                        
    }
    
        @isTest 
    static void testIncreaseAMEFixedPriceError(){
        
        Contract__c c = new Contract__c();
        c.name='t';
        insert c;
        Amendment__c am1 = new Amendment__c();
        am1.Contrat2__c=c.id;
        am1.Nature__c='Conditionnement';
        am1.PriceCurrentYear__c=120;
        Amendment__c am2 = new Amendment__c();
        am2.Contrat2__c=c.id;
        am2.Nature__c='Traitement';
        am2.PriceCurrentYear__c=120;
        Amendment__c am3 = new Amendment__c();
        am3.Contrat2__c=c.id;
        am3.Nature__c='Prestation';
        am3.PriceCurrentYear__c=120;
        List<Amendment__c> amendments = new List<Amendment__c>();
        Amendments.add(am1);
        Amendments.add(am2);
        Amendments.add(am3);
        insert amendments;
        String comment = '';
        AP_AugmentationTarifmanager.RequestResponse response = AP_AugmentationTarifmanager.increaseAMEFixedPrice(amendments, 0, 0, 0, 0,comment);               
        
        System.assertEquals(response.toastMode, Label.ToastMode_Warning);

                        
    }
    
    @isTest 
    static void testIncreaseAMEByValue(){
        
        Contract__c c = new Contract__c();
        c.name='t';
        insert c;
        Amendment__c am1 = new Amendment__c();
        am1.Contrat2__c=c.id;
        am1.Nature__c='Conditionnement';
        am1.PriceCurrentYear__c=120;
        am1.TECH_Excluded__c=false;
        Amendment__c am2 = new Amendment__c();
        am2.Contrat2__c=c.id;
        am2.Nature__c='Traitement';
        am2.PriceCurrentYear__c=120;
        am2.TECH_Excluded__c=false;
        Amendment__c am3 = new Amendment__c();
        am3.Contrat2__c=c.id;
        am3.Nature__c='Prestation';
        am3.PriceCurrentYear__c=120;
        am3.TECH_Excluded__c=false;
        List<Amendment__c> amendments = new List<Amendment__c>();
        Amendments.add(am1);
        Amendments.add(am2);
        Amendments.add(am3);
        insert amendments;
        String comment = 'Test Commentaire';
        Test.startTest();
        AP_AugmentationTarifmanager.RequestResponse response = AP_AugmentationTarifmanager.increaseAMEByValue(amendments, 10, 0, 0, 0,comment);       
        
        list<Amendment__c> am=[SELECT PriceNextYear__c,CompletedAmadement__c,PriceCurrentYear__c, Contrat2__r.RecordLocked__c,TECH_Excluded__c, Nature__c from Amendment__c];
        System.assertEquals(130, am[0].PriceNextYear__c);
        System.assertEquals(130, am[1].PriceNextYear__c);
        System.assertEquals(130, am[2].PriceNextYear__c);
        
        response = AP_AugmentationTarifmanager.increaseAMEByValue(am, 0, 20, 0, 0,comment);    
        list<Amendment__c> amTraitement=[SELECT PriceNextYear__c,CompletedAmadement__c,PriceCurrentYear__c, Nature__c, Contrat2__r.RecordLocked__c,TECH_Excluded__c from Amendment__c where Nature__c='Traitement'];       
        System.assertEquals(130, amTraitement[0].PriceNextYear__c);
        
        response = AP_AugmentationTarifmanager.increaseAMEByValue(am, 0, 0, 0, 20,comment);    
	    list<Amendment__c> amConditionnement =[SELECT PriceNextYear__c,CompletedAmadement__c,PriceCurrentYear__c, Nature__c, Contrat2__r.RecordLocked__c,TECH_Excluded__c from Amendment__c where Nature__c='Conditionnement'];
        System.assertEquals(130, amConditionnement[0].PriceNextYear__c);
        
        response = AP_AugmentationTarifmanager.increaseAMEByValue(am, 0, 0, 20, 0,comment);    
        list<Amendment__c> amPrestation =[SELECT PriceNextYear__c,CompletedAmadement__c,PriceCurrentYear__c, Nature__c, Contrat2__r.RecordLocked__c,TECH_Excluded__c from Amendment__c where Nature__c='Prestation'];
        System.assertEquals(130, amPrestation[0].PriceNextYear__c);
        Test.stopTest(); 
        Contract__c cAfterinsert=[SELECT Id,IncreaseComment__c  from Contract__c where id=:c.Id];
        System.assertEquals(comment, cAfterinsert.IncreaseComment__c);
                        
    }
    
    @isTest 
    static void testIncreaseAMEByValueError(){
        
        Contract__c c = new Contract__c();
        c.name='t';
        insert c;
        Amendment__c am1 = new Amendment__c();
        am1.Contrat2__c=c.id;
        am1.Nature__c='Conditionnement';
        am1.PriceCurrentYear__c=120;
        am1.TECH_Excluded__c=false;
        Amendment__c am2 = new Amendment__c();
        am2.Contrat2__c=c.id;
        am2.Nature__c='Traitement';
        am2.PriceCurrentYear__c=120;
        am2.TECH_Excluded__c=false;
        Amendment__c am3 = new Amendment__c();
        am3.Contrat2__c=c.id;
        am3.Nature__c='Prestation';
        am3.PriceCurrentYear__c=120;
        am3.TECH_Excluded__c=false;
        List<Amendment__c> amendments = new List<Amendment__c>();
        Amendments.add(am1);
        Amendments.add(am2);
        Amendments.add(am3);
        insert amendments;

        String comment = '';
        AP_AugmentationTarifmanager.RequestResponse response = AP_AugmentationTarifmanager.increaseAMEByValue(amendments, 0, 0, 0, 0,comment); 
        System.assertEquals(response.toastMode, Label.ToastMode_Warning);
    }
    
    @isTest 
    static void testDontIncreaseAME(){
        
        Contract__c c = new Contract__c();
        c.name='t';
        insert c;
        Amendment__c am1 = new Amendment__c();
        am1.Contrat2__c=c.id;
        am1.Nature__c='Conditionnement';
        am1.PriceCurrentYear__c=120;
        Amendment__c am2 = new Amendment__c();
        am2.Contrat2__c=c.id;
        am2.Nature__c='Traitement';
        am2.PriceCurrentYear__c=120;
        Amendment__c am3 = new Amendment__c();
        am3.Contrat2__c=c.id;
        am3.Nature__c='Prestation';
        am3.PriceCurrentYear__c=120;
        List<Amendment__c> amendments = new List<Amendment__c>();
        Amendments.add(am1);
        Amendments.add(am2);
        Amendments.add(am3);
        insert amendments;
        String comment = 'Test Commentaire';
        Test.startTest();
        AP_AugmentationTarifmanager.RequestResponse response = AP_AugmentationTarifmanager.dontIncreaseAME(amendments,comment);  
        
        list<Amendment__c> am=[SELECT PriceNextYear__c,PriceCurrentYear__c, Contrat2__r.RecordLocked__c, Nature__c from Amendment__c];
        System.assertEquals(am[0].PriceCurrentYear__c, am[0].PriceNextYear__c);
        Test.stopTest();
        Contract__c cAfterinsert=[SELECT Id,IncreaseComment__c  from Contract__c where id =:c.Id];
        System.assertEquals(comment, cAfterinsert.IncreaseComment__c);    
        
    }

    @isTest 
    static void testRegroupContracts(){
        AP_AugmentationTarifmanager ctrl = new AP_AugmentationTarifmanager();
        List<Contract__c> listC = new  List<Contract__c>();      

        Account acc = new Account();
        acc.Name = 'Test';
        acc.SIRET_Number__c = '11223344556677';
        insert acc;

        list<Account> accList = [select Id from Account Limit 1] ;

        Contract__c c1 = new Contract__c();   
        c1.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;   
        c1.Account__c = accList[0].Id;        
        
        Contract__c c2 = new Contract__c();   
        c2.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c2.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 
        c2.Account__c = accList[0].Id;        
        
       	listC.add(c1); listC.add(c2);  
       	insert listC;

        listC = [select Id , RegroupmentIndex__c, Account__c, RecordLocked__c , Contrat_face_face__c, IncreaseRatioNumber__c,ContactRelated__c,Type_de_convention__c from Contract__c Limit 2]; 
		
        AP_AugmentationTarifmanager.RequestResponse response = AP_AugmentationTarifmanager.regroupContracts(listC);
        
        List<Contract__c> regroupIndex =[SELECT Id, RegroupmentIndex__c FROM Contract__c ];  
        System.assertEquals(regroupIndex[0].RegroupmentIndex__c , regroupIndex[1].RegroupmentIndex__c);

    }

     @isTest 
    static void testRegroupContractsErrorSameAccount(){
       AP_AugmentationTarifmanager ctrl = new AP_AugmentationTarifmanager();
        List<Contract__c> listC = new  List<Contract__c>();
        List<Amendment__c> listA = new  List<Amendment__c>();

        Account acc1 = new Account();
        acc1.Name = 'Test';
        acc1.SIRET_Number__c = '11323344556677';
        
        Account acc2 = new Account();
        acc2.Name = 'Test';
        acc2.SIRET_Number__c = '11223344556670';
        
        list<Account> accList = new List<Account>();
        accList.add(acc1); accList.add(acc2);

        insert accList;
        
        accList = [select Id from Account Limit 2] ;

        Contract__c c1 = new Contract__c();   
        c1.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;   
        c1.Account__c = accList[0].Id;        
        
        Contract__c c2 = new Contract__c();   
        c2.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c2.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 
        c2.Account__c = accList[1].Id;        
        
       	listC.add(c1); listC.add(c2);  
       	insert listC;

        listC = [select Id , RegroupmentIndex__c, Account__c, RecordLocked__c , Contrat_face_face__c, IncreaseRatioNumber__c,ContactRelated__c,Type_de_convention__c from Contract__c Limit 2]; 
		
        AP_AugmentationTarifmanager.RequestResponse response = AP_AugmentationTarifmanager.regroupContracts(listC);
        
        List<Contract__c> regroupIndex =[SELECT Id, RegroupmentIndex__c FROM Contract__c ];  
        System.assertEquals(response.toastMode, Label.ToastMode_Warning);

    }
  
   
    @isTest 
    static void testAddPercentage(){
        AP_AugmentationTarifmanager ctrl = new AP_AugmentationTarifmanager();        
      
        Contract__c c1 = new Contract__c();   
        c1.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;   
        c1.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport;    
        
		list<Contract__c> listC = new List<Contract__c>();
       	listC.add(c1);
        
       	insert listC;
        listC = [select Id , Type_de_convention__c  from Contract__c]; 	
        
        Amendment__c a1 = new Amendment__c();
        a1.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;
        a1.Contrat2__c = listC[0].Id ;               
        a1.PriceCurrentYear__c = 10;      
        
        insert a1;
        
        List<Amendment__c> listA = new List<Amendment__c>();

        listA = [select Filiale__c, Filiere__c,PriceCurrentYear__c  from Amendment__c ];
		Amendment__c ame = AP_AugmentationTarifmanager.addpercentage(listA[0],10, 'noRounding');
        
        System.assertEquals(ame.PriceNextYear__c, 11 );
        
		ame = AP_AugmentationTarifmanager.addpercentage(ame,10, 'supp');
        
        System.assertEquals(ame.PriceNextYear__c, ame.PriceNextYear__c.round(System.RoundingMode.CEILING) );
        
        ame = AP_AugmentationTarifmanager.addpercentage(ame,10, 'auto');
        
        System.assertEquals(ame.PriceNextYear__c, ame.PriceNextYear__c.round() );

    }
    
    @isTest 
    static void testIncreaseByValue(){
        
        Contract__c c = new Contract__c();
        c.name='t';
        insert c;
        Amendment__c am1 = new Amendment__c();
        am1.Contrat2__c=c.id;
        am1.Nature__c='Conditionnement';
        am1.PriceCurrentYear__c=10;
        am1.TECH_Excluded__c = false;
        Amendment__c am2 = new Amendment__c();
        am2.Contrat2__c=c.id;
        am2.Nature__c='Traitement';
        am2.PriceCurrentYear__c=10;
        am2.TECH_Excluded__c = false;
        Amendment__c am3 = new Amendment__c();
        am3.Contrat2__c=c.id;
        am3.Nature__c='Prestation';
        am3.PriceCurrentYear__c=10;
        am3.TECH_Excluded__c = false;
        Amendment__c am4 = new Amendment__c();
        am4.Contrat2__c=c.id;
        am4.Nature__c='Conditionnement';
        am4.PriceCurrentYear__c=20;
        am4.TECH_Excluded__c = false;
        Amendment__c am5 = new Amendment__c();
        am5.Contrat2__c=c.id;
        am5.Nature__c='Traitement';
        am5.PriceCurrentYear__c=20;
        am5.TECH_Excluded__c = false;
        Amendment__c am6 = new Amendment__c();
        am6.Contrat2__c=c.id;
        am6.Nature__c='Prestation';
        am6.PriceCurrentYear__c=20;
        am6.TECH_Excluded__c = false;
        List<Amendment__c> amendments = new List<Amendment__c>();
        Amendments.add(am1);
        Amendments.add(am2);
        Amendments.add(am3);
        insert amendments;

        String comment = 'Test Commentaire';
        Test.startTest();
        String response = AP_AugmentationTarifmanager.increaseValues(amendments, 10, 0, 0, 0,'noRounding','noRounding','noRounding','noRounding',comment);       
        
        list<Amendment__c> am=[SELECT PriceNextYear__c,PriceCurrentYear__c, Contrat2__r.RecordLocked__c, Nature__c,TECH_Excluded__c,CompletedAmadement__c from Amendment__c];
        System.assertEquals(11, am[0].PriceNextYear__c);
        System.assertEquals(11, am[1].PriceNextYear__c);
        System.assertEquals(11, am[2].PriceNextYear__c);
        
        List<Amendment__c> amendments2 = new List<Amendment__c>();
        amendments2.add(am4);
        amendments2.add(am5);
        amendments2.add(am6);
        insert amendments2;

        list<Amendment__c> amend=[SELECT PriceNextYear__c,PriceCurrentYear__c, Contrat2__r.RecordLocked__c, Nature__c,TECH_Excluded__c,CompletedAmadement__c from Amendment__c WHERE PriceCurrentYear__c=20];
        response = AP_AugmentationTarifmanager.increaseValues(amend, 0, 10, 0, 0,'supp','supp','supp','supp',comment);    
        list<Amendment__c> amTraitement=[SELECT PriceNextYear__c,PriceCurrentYear__c, Nature__c, Contrat2__r.RecordLocked__c,TECH_Excluded__c from Amendment__c where Nature__c='Traitement' AND PriceCurrentYear__c=20];       
        System.assertEquals(22, amTraitement[0].PriceNextYear__c);
        
        response = AP_AugmentationTarifmanager.increaseValues(amend, 0, 0, 0, 10,'supp','supp','supp','supp',comment);    
	    list<Amendment__c> amConditionnement =[SELECT PriceNextYear__c,PriceCurrentYear__c, Nature__c, Contrat2__r.RecordLocked__c,TECH_Excluded__c from Amendment__c where Nature__c='Conditionnement' AND PriceCurrentYear__c=20];
        System.assertEquals(22, amConditionnement[0].PriceNextYear__c);
        
        response = AP_AugmentationTarifmanager.increaseValues(amend, 0, 0, 20, 0 , 'supp','supp','supp','supp',comment);    
        list<Amendment__c> amPrestation =[SELECT PriceNextYear__c,PriceCurrentYear__c, Nature__c, Contrat2__r.RecordLocked__c,TECH_Excluded__c from Amendment__c where Nature__c='Prestation' AND PriceCurrentYear__c=20];
        System.assertEquals(24, amPrestation[0].PriceNextYear__c);    
        Test.stopTest();
        Contract__c cAfterinsert=[SELECT Id,IncreaseComment__c  from Contract__c where id=:c.Id];
        System.assertEquals(comment, cAfterinsert.IncreaseComment__c);              
    }
    
     @isTest 
    static void testUpdateContract(){
                    
        Contract__c c = new Contract__c();
        c.name='Name Contract';
        c.Etat__c = 'Courrier';
        insert c;
        
        c = [select Id, Name, Etat__c from Contract__c Limit 1];
        
        c.Etat__c = 'Convention';
        
        List<Contract__c> listC = new List<Contract__c>();
        listC.add(c);
       
        AP_AugmentationTarifmanager.updateContract(listC);  
        
        c = [select Id, Name, Etat__c from Contract__c Limit 1];
        
        System.assertEquals(c.Etat__c, 'Convention');        
                        
    }

    @isTest 
    static void testResetAmendmentIncrease(){
                    
        Contract__c c = new Contract__c();
        c.name='t';
        insert c;
        List<Contract__c> contracts = new List<Contract__c>();
        contracts.add(c);
        Amendment__c am1 = new Amendment__c();
        am1.Contrat2__c=c.id;
        am1.Nature__c='Conditionnement';
        am1.PriceCurrentYear__c=120;
        am1.PriceNextYear__c=120;
        Amendment__c am2 = new Amendment__c();
        am2.Contrat2__c=c.id;
        am2.Nature__c='Traitement';
        am2.PriceCurrentYear__c=120;
        am2.PriceNextYear__c=120;
        Amendment__c am3 = new Amendment__c();
        am3.Contrat2__c=c.id;
        am3.Nature__c='Prestation';
        am3.PriceCurrentYear__c=120;
        am3.PriceNextYear__c=120;
        List<Amendment__c> amendments = new List<Amendment__c>();
        Amendments.add(am1);
        Amendments.add(am2);
        Amendments.add(am3);
        insert amendments;
        String comment = 'Test Commentaire';
        Test.startTest();
        AP_AugmentationTarifmanager.RequestResponse response = AP_AugmentationTarifmanager.resetAmendmentIncrease(amendments);  
        
        list<Amendment__c> am=[SELECT PriceNextYear__c,PriceCurrentYear__c, Contrat2__r.RecordLocked__c, Nature__c from Amendment__c];
        System.assertEquals(am[0].PriceNextYear__c, null);
        Test.stopTest();                        
    }

    @isTest 
    static void testResetContractIncrease(){
                    
        Contract__c c = new Contract__c();
        c.name='t';
        c.IncreaseComment__c = 'Test commentaire';
        insert c;
        List<Contract__c> contracts = new List<Contract__c>();
        contracts.add(c);
       
        Test.startTest();
        AP_AugmentationTarifmanager.RequestResponse response = AP_AugmentationTarifmanager.resetContractIncrease(contracts);  
        
        list<Contract__c> cont=[SELECT IncreaseComment__c from Contract__c];
        System.assertEquals(cont[0].IncreaseComment__c, null);
        Test.stopTest();                        
    }

    @isTest 
    static void testExcluded(){
        Contract__c c = new Contract__c();
        c.name='t';
        insert c;
        List<Contract__c> contracts = new List<Contract__c>();
        contracts.add(c);
        Amendment__c am1 = new Amendment__c();
        am1.Contrat2__c=c.id;
        am1.Nature__c='Conditionnement';
        am1.PriceCurrentYear__c=120;
        am1.PriceNextYear__c=120;
        Amendment__c am2 = new Amendment__c();
        am2.Contrat2__c=c.id;
        am2.Nature__c='Traitement';
        am2.PriceCurrentYear__c=120;
        am2.PriceNextYear__c=120;
        Amendment__c am3 = new Amendment__c();
        am3.Contrat2__c=c.id;
        am3.Nature__c='Prestation';
        am3.PriceCurrentYear__c=120;
        am3.PriceNextYear__c=120;
        List<Amendment__c> amendments = new List<Amendment__c>();
        Amendments.add(am1);
        Amendments.add(am2);
        Amendments.add(am3);
        insert amendments;
        String comment = 'Test Commentaire';
        Test.startTest();
        //AP_AugmentationTarifmanager.RequestResponse response = AP_AugmentationTarifmanager.excluded(amendments,contracts,'test exclusion');  
        AP_AugmentationTarifmanager.RequestResponse response = AP_AugmentationTarifmanager.excluded_c(contracts,'test exclusion');  
        AP_AugmentationTarifmanager.RequestResponse response2 = AP_AugmentationTarifmanager.excluded_am(amendments,'test exclusion');  
        
        list<Amendment__c> am=[SELECT PriceNextYear__c,ByPassContract__c, Contrat2__r.RecordLocked__c,TECH_Face_Face__c,Contrat2__r.Face_Face__c,Contrat2__r.IncreaseComment__c, Nature__c,ExclusionComment__c,TECH_Excluded__c from Amendment__c];
        System.assertEquals(am[0].PriceNextYear__c, null);
        System.assertEquals(am[0].ByPassContract__c, false);
        System.assertEquals(am[0].TECH_Face_Face__c, false);
        System.assertNotEquals(am[0].ExclusionComment__c, null);
        System.assertEquals(am[0].TECH_Excluded__c, true);
        System.assertEquals(am[0].Contrat2__r.Face_Face__c, false);
        Test.stopTest();                        
    }
}