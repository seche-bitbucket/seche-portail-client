@IsTest
public class LeadTriggerTest {

@IsTest
public static void testLeadTrigger(){

	//l.RecordTypeId == Label.LED_RT_LED_WEB_TO_LEAD_TRIADIS_AMIANTE

	Test.startTest();

	List<Lead> listLead = new List<Lead>();
	Lead l = new Lead();
	l.FirstName ='Jean';
	l.LastName = 'Martin';
	l.Company = 'Capgemini';
	l.Status = 'Ouverte';
	l.LeadSource = 'Autre';
	listLead.add(l);

	listLead.add(new Lead(FirstName ='Jean',LastName = 'Martin',Company = 'Capgemini',Status = 'Ouverte',LeadSource = 'Autre',RecordTypeId=Label.LED_RT_LED_WEB_TO_LEAD_SHC_DASRI,
	                      WorkSiteZip__c='123'));

	listLead.add(new Lead(FirstName ='Jean',LastName = 'Martin',Company = 'Capgemini',Status = 'Ouverte',LeadSource = 'Autre',RecordTypeId=Label.LED_RT_LED_WEB_TO_LEAD_TRIADIS_AMIANTE,
	                      WorkSiteZip__c='123'));
	listLead.add(new Lead (FirstName ='Jean',LastName = 'Martin', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Internet', WorkSiteZip__c = '97000', RecordTypeId = Label.LED_RT_LED_WEB_TO_LEAD_SHC_DASRI,PostalCode = '97000',
	                       Waste__c = 'Déchet Amiante',AsbestosPackagingType__c = 'Palette',Quantity__c = 200,AsbestosPackagingSize__c = '3*4', Filiale__c = 'Séché Eco Industries Changé',Filiere__c = 'Biocentre'));

	listLead.add(new Lead (FirstName ='Jean',LastName = 'Martin', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Internet', WorkSiteZip__c = '97000', RecordTypeId = Label.LED_WEB_TO_LEAD_OPALE,PostalCode = '69009',
	                       Waste__c = 'Déchet Amiante',AsbestosPackagingType__c = 'Palette',Quantity__c = 200,AsbestosPackagingSize__c = '3*4', Filiale__c = 'Séché Eco Industries Changé',Filiere__c = 'Biocentre'));

	insert listLead;

	/*  l = [Select Id, Name, TECH_UID__c
	       FROM Lead Where ID =: l.ID];
	   System.assert(l.TECH_UID__c != null);*/

	Test.stopTest();

}

}