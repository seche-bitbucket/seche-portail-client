/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-11-26
* @modified       2018-11-26
* @systemLayer    Trigger Handler        
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Class that updates all the opportunities inked to signed quotes to won
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class DocumentForSignatureTriggerHandler {

    public static void DocumentForSignatureAfterUpdate(List<DocumentsForSignature__c> DocsForSignature){
        List<ID> quotesID = new List<ID>();
        List<ID> rejectedQuotesID=new List<ID>();
        for (DocumentsForSignature__c Doc: DocsForSignature){
            if(doc.Quote__c!=Null && doc.Status__c=='SIGNED' && doc.FIP_FIP__c==Null){
                quotesID.add(doc.Quote__c);
            }
            if(doc.Quote__c!=Null && doc.Status__c=='REJECTED' && doc.FIP_FIP__c==Null){
                rejectedQuotesID.add(doc.Quote__c);
            }
        }
        List<Quote> quoteToUpdate = [Select ID, OpportunityId FROM Quote WHERE ID in : quotesID];
        List<Quote> rejectedQuotes = [Select ID, OpportunityId FROM Quote WHERE ID in : rejectedQuotesID];
        List<ID> OpportunitiesID = new List<ID>();
        for(Quote q:quoteToUpdate){
            q.Status='Signé';
            OpportunitiesID.add(q.OpportunityId);
        }
         for(Quote q:rejectedQuotes){
            q.Status='Refusé';
        }
        List<Opportunity> OppsToUpdate=[Select Id,StageName FROM Opportunity WHERE ID IN:OpportunitiesID];
        for(Opportunity opp:OppsToUpdate){
            opp.StageName='Clôturée / Gagnée';
        }
        If(OppsToUpdate.size()>0){
            Update OppsToupdate;
        }
        if(quoteToUpdate.size()>0){
            Update quoteToUpdate;
        }
        if(rejectedQuotes.size()>0){
            Update rejectedQuotes;
        }
    }

    /*public static void SendEmailNotif(List<DocumentsForSignature__c> newDocs, Map<Id, DocumentsForSignature__c> oldMap) {

        Map<Id, DocumentsForSignature__c> mapPreFipToDoc = new Map<Id, DocumentsForSignature__c>();
        Map<Id, DocumentsForSignature__c> mapQuoteToDoc = new Map<Id, DocumentsForSignature__c>();

        for(DocumentsForSignature__c newDoc: newDocs) {
            if(newDoc.TECH_UpdatedWithApex__c == true && (newDoc.Status__c == 'REJECTED' || newDoc.Status__c == 'FINISHED') && oldMap.get(newDoc.Id).Status__c != 'FINISHED' && oldMap.get(newDoc.Id).Status__c != 'FINISHED') {
                
                System.debug('newDoc: '+newDoc);

                System.debug('newDoc.Status__c: '+newDoc.Status__c);

                if(newDoc.T5F_PreFip__c != null && !mapPreFipToDoc.containsKey(newDoc.T5F_PreFip__c)) {
                    mapPreFipToDoc.put(newDoc.T5F_PreFip__c, newDoc);
                } else if(newDoc.Quote__c != null && !mapQuoteToDoc.containsKey(newDoc.Quote__c)) {
                    mapQuoteToDoc.put(newDoc.Quote__c, newDoc);
                }

            }
        }

        System.debug('mapPreFipToDoc: '+mapPreFipToDoc);
        System.debug('mapQuoteToDoc: '+mapQuoteToDoc);
        
        List<Messaging.SingleEmailMessage> lstMsg = new List<Messaging.SingleEmailMessage>();

        Map<Id, User> mapPreFipToUser = new Map<Id, User>();
        List<T5F_PreFip__c> lstPreFip = [SELECT ID, Name, Status__c, Contact__r.Name, Lead__r.Owner.Email, Lead__r.OwnerId FROM T5F_PreFip__c WHERE ID IN :mapPreFipToDoc.keySet()];
        List<Quote> lstQuote = [SELECT Id,Name,Status,NameCustomer__c,Opportunity.Salesman__r.Email,Opportunity.Salesman__r.Assistant__r.Email,Quote_Type__c,QuoteNumber FROM Quote WHERE ID IN :mapQuoteToDoc.keySet()];

        List<Id> lstOwnersId = new List<Id>();
        for(T5F_PreFip__c prefip: lstPreFip) {
            if(!lstOwnersId.contains(prefip.Lead__r.OwnerId)) {
                lstOwnersId.add(prefip.Lead__r.OwnerId);
            }
        }

        System.debug('lstOwnersId: '+lstOwnersId);

        for(User user: [SELECT ID, Assistant__r.Email, Assistant__c FROM User WHERE ID = :lstOwnersId]) {
            for(T5F_PreFip__c prefip: lstPreFip) {
                if(user.Id == prefip.Lead__r.OwnerId && !mapPreFipToUser.containsKey(prefip.Id)) {
                    mapPreFipToUser.put(prefip.Id, user);
                }
            }
        }

        System.debug('mapPreFipToUser: '+mapPreFipToUser);

        for(T5F_PreFip__c prefip: lstPreFip) {
            Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
            msg = ConnectiveApiManager.generateContentEmailPreFip(prefip,prefip.Status__c,mapPreFipToDoc.get(prefip.Id), mapPreFipToUser.get(prefip.Id));
            lstMsg.add(msg);
        }

        for(Quote qt: lstQuote) {
            Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
            msg = ConnectiveApiManager.generateContentEmail(qt,qt.Status,mapQuoteToDoc.get(qt.Id));
            lstMsg.add(msg);
        }

        System.debug(lstMsg);

        Messaging.sendEmail(lstMsg);

    }*/

}