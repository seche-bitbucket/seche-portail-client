/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Mohammed Amine IDRISSI TAGHKI <mohammed-amine.idrissi-taghki@capgemini.com>
 * @version        1.0
 * @created        2021-07-27
 * @systemLayer    Manager
 *
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Desciption: Pollutec Class mANAGER
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */
public without sharing class Pollutec_CL_FlowAction {

    public class Inputs {
        @InvocableVariable(label='Record Id' required=true)
        public Id recordId;
    }

    @InvocableMethod(label='Pollutec Flow Action' description='Detect SObjectType From Id')
    public static List<String> detect(List<Inputs> inputs) {
        return new List<String>{inputs[0].recordId.getSObjectType().getDescribe().getName()}; 
    }
}