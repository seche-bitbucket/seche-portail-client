/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ConnectiveMockInstantPackage
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ConnectiveMockInstantPackage
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 16-06-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
@isTest
global class ConnectiveMockInstantPackage implements  HTTPCalloutMock{
        global Static String externalIdContact;
        global Static String externalIdDoc;
        global Static String body = '{"PackageId": "3bb6a2d1-35db-4a3a-a434-868c01bcca9d","PackageName": "instant package","Initiator": "john.smith@mail.com","ExpiryTimestamp": null,"ExternalPackageReference": "'+externalIdDoc+'","F2FSigningUrl": "https://yourFTFSignUrl","PackageStatus": "Pending","PackageDocuments": [{"DocumentId": "97cfc102-70c2-4c17-9fb7-74b29360d53f","DocumentType": "application/pdf","ExternalDocumentReference": "'+externalIdDoc+'","DocumentName": "instant package","Locations": [{"Id": "e3ba68ac-e737-4e15-b61f-229034cf0797","Label": "d28d483a-824d-43c9-93d1-026117d8ebaf","PageNumber": 2}]}],"Stakeholders": [{"Type": "Person","EmailAddress": "john.smith@mail.com","ContactGroupCode": null,"ExternalStakeholderReference": "'+externalIdContact+'","StakeholderId": "7aa76f03-7981-44c1-8b91-598256edf260","Actors": [{"Type": "Signer","Reason": null,"CompletedBy": null,"CompletedTimestamp": null,"Locations": [{"Id": "e3ba68ac-e737-4e15-b61f-229034cf0797","UsedSigningType": null}],"ActorId": "db4a380f-fbd8-4bd7-b9e3-23507e986f66","ActionUrl": "https://ActionUrl","ActionUrls": [],"ActorStatus": "Available"}],"PersonGroupName": null}],"CreationTimestamp": "2019-10-24T11:20:55Z","Warnings": []}';
	global HTTPResponse respond(HTTPRequest req){
        
        System.assertEquals('callout:ConnectiveApiV3/packages/instant', req.getEndpoint());
        HttpResponse res = new HTTPResponse();
        res.setHeader('Content-Type', 'application/JSON');
        res.setBody('{"PackageId": "3bb6a2d1-35db-4a3a-a434-868c01bcca9d","PackageName": "instant package","Initiator": "john.smith@mail.com","ExpiryTimestamp": null,"ExternalPackageReference": "'+externalIdDoc+'","F2FSigningUrl": "https://yourFTFSignUrl","PackageStatus": "Pending","PackageDocuments": [{"DocumentId": "97cfc102-70c2-4c17-9fb7-74b29360d53f","DocumentType": "application/pdf","ExternalDocumentReference": "'+externalIdDoc+'","DocumentName": "instant package","Locations": [{"Id": "e3ba68ac-e737-4e15-b61f-229034cf0797","Label": "d28d483a-824d-43c9-93d1-026117d8ebaf","PageNumber": 2}]}],"Stakeholders": [{"Type": "Person","EmailAddress": "john.smith@mail.com","ContactGroupCode": null,"ExternalStakeholderReference": "'+externalIdContact+'","StakeholderId": "7aa76f03-7981-44c1-8b91-598256edf260","Actors": [{"Type": "Signer","Reason": null,"CompletedBy": null,"CompletedTimestamp": null,"Locations": [{"Id": "e3ba68ac-e737-4e15-b61f-229034cf0797","UsedSigningType": null}],"ActorId": "db4a380f-fbd8-4bd7-b9e3-23507e986f66","ActionUrl": "https://ActionUrl","ActionUrls": [],"ActorStatus": "Available"}],"PersonGroupName": null}],"CreationTimestamp": "2019-10-24T11:20:55Z","Warnings": []}');
        req.setMethod('POST');
        res.setStatusCode(201);
        return res;
    }
}