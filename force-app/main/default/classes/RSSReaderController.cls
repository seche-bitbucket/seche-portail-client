public class RSSReaderController {
    
    static HTTPResponse res = new HTTPResponse();
    
    static String url = 'http://www.groupe-seche.com/new.rss';
    static Map<Integer, Post> news = new Map<Integer, Post>();
        
    static Integer counter = 10;
    static Integer i = 0;
    
    
    @AuraEnabled
    public static Map<Integer, Post> getRSSReaderController() {
        try {
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            req.setMethod('GET');    
            req.setEndpoint(url);
            res = http.send(req);
            System.debug(res.getBody());
            
            parse(res.getBody()); 
            return news;
        } catch(Exception ex){
            System.debug(ex.getMessage());
            return new Map<Integer, Post>();
        }
    }
    
    public static void parse(String xmlFile){
        
        Dom.Document doc = new Dom.Document();
        doc.load(xmlFile);
        
        //Retrieve the root element for this document.
        Dom.XMLNode root = doc.getRootElement();
        
        // Alternatively, loop through the child elements.
        // This prints out all the elements of the address
        for(Dom.XMLNode node1 : root.getChildElements()) {
            for(Dom.XMLNode node : node1.getChildElements()) {
                
                if(node.getName() == 'item'){
                    Post pst = buildPost(node);
                    news.put(pst.id, pst);
                }
                                
                //Stop load X news
                if(i == counter){
                    break;
                }     
            }
        }
    }
    
    public static Post buildPost(Dom.XMLNode item){
        Post pst = new Post();
        
        for(Dom.XmlNode node :item.getChildElements()){
           
            if(node.getName() == 'title'){
                pst.title = node.getText();
            }else if(node.getName() == 'link'){
                pst.link = node.getText();
            }
            else if(node.getName() == 'description'){
                pst.description = node.getText();
            }
            else if(node.getName() == 'image'){
                pst.image = node.getText();
                //Go find the secure http to display image
                //Answer to salesforce security policy
                pst.image = (pst.image).replace('http', 'https');
            }
            else if(node.getName() == 'pubDate'){
                pst.datePub = node.getText().substring(4, 16);
            }
        }
        
        pst.id = i++;
        return pst;
    }

    
    
    public class Post {
        
        @AuraEnabled
        public integer id { get; set; }
        
        @AuraEnabled
        public String title { get; set; }
        
        @AuraEnabled
        public String description { get; set; }
        
        @AuraEnabled
        public String link { get; set; }
        
        @AuraEnabled
        public String datePub { get; set; }
        
        @AuraEnabled
        public String image { get; set; }
    }
    
    
}