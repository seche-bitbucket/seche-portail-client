@isTest
public class ProductSearchExtTest {
    
    static testMethod void testSearchProducts(){
        
        Id priceBookId;
        integer size=200;
        Integer noOfRecords;

        String[] profiLisList = new String[]{'Administrateur système', 'System Administrator'};

        //Get a profile to create User
        Profile p = [select id from profile where name in:profiLisList limit 1];
        
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        insert director;
        System.RunAs(director) {
            Account ac = new Account();
            ac.Name = 'testAccName';
            ac.CustomerNature__c = 'Administration';
            ac.Industry = 'Trader';
            ac.CurrencyIsoCode = 'EUR';
            ac.Producteur__c = false;
            ac.BillingPostalCode = '00000';
            ac.NAF_Number__c = '1234A';
            ac.SIRET_Number__c = '11111111111112';
            insert ac;
            
            Contact contactTest = new Contact();
            contactTest.LastName = 'testName';
            contactTest.AccountId = ac.Id;
            contactTest.Salesman__c = director.Id;
            contactTest.Email = 'aaa@yopmail.com';
            //contactTest.Phone = '0000000000';
            insert contactTest;
            
            Pricebook2 pb = new Pricebook2(Name='PricebookFille', isActive=true);
            insert pb;
            
            Product2 prd = new product2(name = 'Test' ,TECH_ExternalID__c = '012345',Family = 'Traitement');
            insert prd;
            
            PricebookEntry pbeMere = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=Test.getStandardPricebookId(),
                                                        isActive=true);
            insert pbeMere;
            
            PricebookEntry pbe = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=pb.Id,
                                                    isActive=true,usestandardprice = true);
            insert pbe;
            //Get a Record Type for Opportunity
            RecordType rt = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_OpportuniteMere' LIMIT 1];
            
            //Create Opportunity Mother
            Opportunity opport = new Opportunity(Name = 'OPPtest', RecordTypeId = rt.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', PriceBook2Id = pb.Id);
            
            insert opport;
            system.debug('opport '+opport.ID);
            PageReference pageRef = Page.OLI_ProductSearch;
            pageRef.getParameters().put('OppId',opport.Id);
            id OppId = pageRef.getParameters().get('OppId');
            Test.setCurrentPage(pageRef);
            Opportunity opp  = [select pricebook2Id from opportunity where id =: OppId]; 
            priceBookId = opp.pricebook2Id;
            ProductSearchExt myController = new ProductSearchExt();
            
            ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                [select Product2.Name,Product2.Description,Product2.ProductCode,UnitPrice,Product2.Family,Name,LogoNature__c,Product2.EuropeanCode__c  from pricebookentry where Product2.Name != null and pricebook2Id =:priceBookId  order by Product2.Name]));
            System.debug('PricebookId ' + priceBookId);
            setCon.setPageSize(size);  
            noOfRecords = setCon.getResultSize();
            system.debug('setcon '+setcon);
            setCon.getRecords();
			myController.getProduct();
            myController.refreshPageSize();
           	myController.processSelected();
            myController.saveProcess();
            PageReference pageRef2 = Page.OLI_ProductSearch;
			pageRef.getParameters().put('productFamily', 'Traitement');
            pageRef.getParameters().put('keyword', 'test');
            string productFamily = pageRef.getParameters().get('productFamily');
            string keyword = pageRef.getParameters().get('keyword');
            system.debug(keyword);
            
            Test.startTest();
			myController.search();
            Test.stopTest();
            myController.initCon([select Product2.Name,Product2.Description,Product2.ProductCode,UnitPrice,Product2.Family,Name,LogoNature__c,Product2.EuropeanCode__c  from pricebookentry where Product2.Name != null and pricebook2Id =:priceBookId  order by Product2.Name]);
            myController.cancelProcess();
            
        }
        
    }

      static testMethod void testSearchProductsForQuote(){
        
        Id priceBookId;
        integer size=200;
        Integer noOfRecords;

        String[] profiLisList = new String[]{'Administrateur système', 'System Administrator'};

        //Get a profile to create User
        Profile p = [select id from profile where name in:profiLisList limit 1];
        
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        insert director;
        System.RunAs(director) {
            Account ac = new Account();
            ac.Name = 'testAccName';
            ac.CustomerNature__c = 'Administration';
            ac.Industry = 'Trader';
            ac.CurrencyIsoCode = 'EUR';
            ac.Producteur__c = false;
            ac.BillingPostalCode = '00000';
            ac.NAF_Number__c = '1234A';
            ac.SIRET_Number__c = '11111111111113';
            insert ac;
            
            Contact contactTest = new Contact();
            contactTest.LastName = 'testName';
            contactTest.AccountId = ac.Id;
            contactTest.Salesman__c = director.Id;
            contactTest.Email = 'aaa@yopmail.com';
            //contactTest.Phone = '0000000000';
            insert contactTest;
            
            Pricebook2 pb = new Pricebook2(Name='PricebookFille', isActive=true);
            insert pb;
            
            Product2 prd = new product2(name = 'Test' ,TECH_ExternalID__c = '012345',Family = 'Traitement');
            insert prd;
            
            PricebookEntry pbeMere = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=Test.getStandardPricebookId(),
                                                        isActive=true);
            insert pbeMere;
            
            PricebookEntry pbe = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=pb.Id,
                                                    isActive=true,usestandardprice = true);
            insert pbe;
            //Get a Record Type for Opportunity
            RecordType rt = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_OpportuniteMere' LIMIT 1];
            
            //Create Opportunity Mother
            Opportunity opport = new Opportunity(Name = 'OPPtest', RecordTypeId = rt.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', PriceBook2Id = pb.Id);
            
            insert opport;
            system.debug('opport '+opport.ID);
            Quote qu = new Quote(name='01010', opportunityId=opport.Id);
            insert qu;
            PageReference pageRef = Page.OLI_ProductSearch;
            pageRef.getParameters().put('QuoteId',qu.Id);
            id OppId = opport.Id;// pageRef.getParameters().get('OppId');
            Test.setCurrentPage(pageRef);
            Opportunity opp  = [select pricebook2Id from opportunity where id =: OppId]; 
            priceBookId = opp.pricebook2Id;
            ProductSearchExt myController = new ProductSearchExt();
            
            ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                [select Product2.Name,Product2.Description,Product2.ProductCode,UnitPrice,Product2.Family,Name,LogoNature__c,Product2.EuropeanCode__c  from pricebookentry where Product2.Name != null and pricebook2Id =:priceBookId  order by Product2.Name]));
            setCon.setPageSize(size);  
            noOfRecords = setCon.getResultSize();
            setCon.getRecords();
            system.debug('setcon '+setcon);
            myController.isPriceBookSet = true; 
            myController.selectPriceBook();
			myController.getProduct();
            myController.refreshPageSize();
           	myController.processSelected();
            myController.saveProcess();
            PageReference pageRef2 = Page.OLI_ProductSearch;
			pageRef.getParameters().put('productFamily', 'Traitement');
            pageRef.getParameters().put('keyword', 'test');
            string productFamily = pageRef.getParameters().get('productFamily');
            string keyword = pageRef.getParameters().get('keyword');
            system.debug(keyword);
            
            Test.startTest();
			myController.search();
            Test.stopTest();
            myController.initCon([select Product2.Name,Product2.Description,Product2.ProductCode,UnitPrice,Product2.Family,Name,LogoNature__c,Product2.EuropeanCode__c  from pricebookentry where Product2.Name != null and pricebook2Id =:priceBookId  order by Product2.Name]);
            myController.cancelProcess();
            
        }
        
    }
    
}