global class GenerateVisitReportFromEvent {

   public static String requestVisitReport(Id eventId){
   return generateVisitReport(eventId);
 } 


    webService static String generateVisitReport(Id eventId)
    {
        Event e = [Select Id, Description, Compte_Rendu__c, Subject, Compte__c, StartDateTime, WhatId, What.Type, WhoId, Who.Type From Event Where Id =: eventId];
        
        // L'évévement ne doit pas déjà être lié à un CR
        if ((e.WhatId != null && e.What.Type.equals('VisitReport__c')) || e.Compte_Rendu__c != null)
        {
            return 'error1';
        }
        
        // Contact et compte renseigné
        if (e.WhoId != null && e.Who.Type.equals('Contact') && e.Compte__c != null)
        {
            VisitReport__c vr = new VisitReport__c();
            vr.Name = e.Subject;
            vr.CR__c = e.Description;
            vr.VisitDate__c = Date.newinstance(e.StartDateTime.year(), e.StartDateTime.month(), e.StartDateTime.day());            
            vr.Contact__c = e.WhoId;
            vr.Account__c = e.Compte__c;
            insert vr;
            
            e.Compte_Rendu__c = vr.Id;
            update e;
            return vr.id;
        }
        else
        {
            return 'error2';
        }
    }
}