public class QuoteSyncTriggerV2Handler {
    
    private static Boolean stop = false;
    
    public static void stop() {
        stop = true;
    }
    
    Public static void execute(List<Quote> newLst, Map<Id, Quote> oldMap) {
        if(!stop) {
            SyncHandler.SyncHandler(newLst, oldMap, 'Quote');
        }
    }

}