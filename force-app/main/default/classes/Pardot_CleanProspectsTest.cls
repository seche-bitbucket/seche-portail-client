/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Tiphaine VIANA  <tiphaine.viana@capgemini.com>
* @version        1.0
* @created        2023-01-17
* @modified        
* @systemLayer    Test Class of Pardot_CleanProspects      
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes      
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

@isTest
public with sharing class Pardot_CleanProspectsTest {
    
    @TestSetup
    static void createTestData() {
        Account acc = TestDataFactory.createAccountWithInsert('Test Account');

        Contact con = TestDataFactory.createContactWithInsert(acc);

        Opportunity opp = TestDataFactory.createOpportunity('Opportunity Name');
        opp.CreatedDate = date.parse('01/01/2000');
        opp.LastModifiedDate = date.parse('01/01/2001');
        opp.ContactId = con.Id;
        insert opp;

    }

    @isTest
    public static void GetContactIdsTest() {
        Pardot_CleanProspects.GetContactIds(2, 'SELECT Id, (SELECT Id, ContactId , pi_LastActivityDate__c FROM Opportunities ORDER BY CreatedDate LIMIT 1) FROM Contact');
        Contact contact = [SELECT Id, IsToBeDeletedOnPardot__c FROM Contact LIMIT 1];
        System.assertEquals(false, contact.IsToBeDeletedOnPardot__c);

    }
}