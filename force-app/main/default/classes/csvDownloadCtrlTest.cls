@isTest
public with sharing class csvDownloadCtrlTest {
  static testMethod void testfetchFip() {

	Account acc = new Account();
	acc.Name = 'Test Account';
	acc.BillingPostalCode = '21000';
	acc.BillingStreet = '1 rue du test';
	insert acc;

	Contact ctc = new Contact();
	ctc.lastName = 'Test';
	ctc.AccountId = acc.ID;
	insert ctc;

	List<FIP_FIP__c> testFips = new List<FIP_FIP__c>();
	for(integer i=0; i<100; i++) {
		FIP_FIP__c fip = new FIP_FIP__c();
		fip.Collector__c = acc.ID;
		fip.Contact__c = ctc.ID;

		testFips.add(fip);
	}
	insert testFips;
	Test.StartTest();

    String [] listIds = new List<String>();
    for(FIP_FIP__c f : [Select Id FROM FIP_FIP__c]){
        listIds.add(f.Id);
    }
	
	list <FIP_FIP__c> listFips = csvDownloadCtrl.fetchFip(listIds);
	Test.StopTest();
	system.assertNotEquals(0, listFips.size());

}

static testMethod void testDownloadCsv() {
	Account acc = new Account();
	acc.Name = 'Test Account';
	acc.BillingPostalCode = '21000';
	acc.BillingStreet = '1 rue du test';
	insert acc;

	Contact ctc = new Contact();
	ctc.lastName = 'Test';
	ctc.AccountId = acc.ID;
	insert ctc;

	List<FIP_FIP__c> testFips = new List<FIP_FIP__c>();
	for(integer i=0; i<100; i++) {
		FIP_FIP__c fip = new FIP_FIP__c();
		fip.Collector__c = acc.ID;
		fip.Contact__c = ctc.ID;
		testFips.add(fip);
	}
	insert testFips;
	Test.StartTest();	
	String csv = csvDownloadCtrl.generateCSV([Select Id, FIPType__c, PreviousPACNumber__c,Exutoire__c, ContractRoot__c, AdressCode__c, TECH_Subsidiary_Key__c, AcceptedBy__r.Trigramme__c, PlateformeTransit__c,
                WasteName__c, WasteNamingCode__c, AnnualQuantity__c, AsbestosType__c, PackagingType__c, UNCode__c, Class__c, PackagingGroup__c,
                Collector__r.Name, Collector__r.BillingStreet, Collector__r.BillingPostalCode, Collector__r.BillingCity, CreatedBy.FirstName, CreatedBy.LastName, CreatedBy.Phone, CreatedBy.Email, Collector__r.SIRET_Number__c,
                ProductorBusinessName__c, ProductorCompanyIdentificationSystem__c, ProductorAdress1__c, ProductorAdress2__c, ProductorAdress3__c,ProductorPostalCode__c, ProductorCity__c, ConstructionSiteAdress__c,
                ChargedBusinessName__c, ChargedCompanyIdentificationSystem__c, ChargedAdress__c, ChargedPostalCode__c, ChargedCity__c, FrenchTaxeID__c,
                ProductorX3Code__c, ProductorX3AddressCode__c, ChargedX3Code__c, ChargedX3AddressCode__c, AskerCodeX3__c, AskerX3AddressCode__c, ContractNumber__c
                FROM FIP_FIP__c ]);
	system.assertNotEquals(null,csv);	
	Test.StopTest();


}

}