/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Baïla TOURE <baila.toure@capgemini.com>
 * @version        1.0
 * @created        2018-08-07
 * @systemLayer    Controller of the Lightning components : LC_ContractsList_AugTarifController,LC_InputModalForm_AugTarif
 * @Update :28/04/2023, author Tiphaine VIANA, Modif: AreStandardContractsSubmitted
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Desciption: This class contains methods to handle the increase price of contracts and amendements
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

public with sharing class LC_ContractsList_AugTarifCtrl {

  @AuraEnabled// (cacheable = true)
  public static List<Contract__c> fetchContracts(String filiale, String filiere, string natureCompte, String status){
    System.debug('LC_ContractsList_AugTarifCtrl.fetchContracts ---Start : filiale ='+ filiale);
    System.debug('LC_ContractsList_AugTarifCtrl.fetchContracts ---Start : natureCompte ='+ natureCompte);
    return AP_AugmentationTarifManager.retrieveUserContracts(UserInfo.getUserId(),filiale,filiere,natureCompte,status);
  }

  @AuraEnabled
  public static String percentAugmentation(List<Amendment__c> lstAmend, Double inputValue,Double inputValueTraitement,
                Double inputValueTransport,Double inputValueConditionnement,String RoundingType, String RoundingTypeTrait, String RoundingTypePrest, String RoundingTypeCondi,String comment){
    System.debug('LC_ContractsList_AugTarifCtrl.percentAugmentation --> Start');
    return AP_AugmentationTarifManager.increaseValues(lstAmend, inputValue, inputValueTraitement, inputValueTransport, inputValueConditionnement, RoundingType, RoundingTypeTrait, RoundingTypePrest, RoundingTypeCondi, comment);

  }

  @AuraEnabled
  //Method to update a list of contracts
  public static void updateContract(List<Contract__c> lstCont ){
    AP_AugmentationTarifManager.updateContract(lstCont);
  }

  @AuraEnabled
  public static AP_AugmentationTarifManager.RequestResponse validate_FaF_Contracts(list<Contract__c> contractsList, String filiale, String filiere){
    System.debug('Apex called');
    return AP_AugmentationTarifManager.submitAMEForValidation(contractsList,filiale,filiere);
  }

  @AuraEnabled
  public static List<Object> getMetadata(String filiale, String filiere, String natureCompte, String status){
    try {

      List<Object> objs = new List<Object>();

      objs.add(getColumnsLabels());
      
      List<SObject> ctrs = new List<SObject>();
      ctrs.addAll(AP_AugmentationTarifManager.retrieveUserContracts(UserInfo.getUserId(),filiale,filiere, natureCompte,status));
      objs.add(ctrs);

      objs.add(getDefinitions());
      return objs;

    } catch (Exception e) {
      System.debug(e.getMessage()+' '+e.getLineNumber());
    }
    
    return null;
  }

  public static List<cDef__mdt> getDefinitions(){
      List<String> fields_lst = new List<String>(Schema.getGlobalDescribe().get('cDef__mdt').getDescribe().fields.getMap().keySet());
      return Database.query('SELECT '+String.join(fields_lst, ', ')+' FROM cDef__mdt');
  }

  public static List<Custom_Column__mdt> getColumnsLabels(){
      return [SELECT Label, Filiale__c, Filiere__c, Columns_Contracts__c, Column_Avenant__c FROM Custom_Column__mdt];
  }

  @AuraEnabled
  public static Boolean AreStandardContractsSubmitted(String filiale, String filiere){
    return AP_AugmentationTarifManager.AreUserStandardContractsSubmitted(UserInfo.getUserId(), filiale, filiere);
  }


   
}