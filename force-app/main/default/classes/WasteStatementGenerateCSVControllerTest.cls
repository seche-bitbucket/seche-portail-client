@isTest
public class WasteStatementGenerateCSVControllerTest {
    
    
    @isTest
    public static void WasteStatementGenerateCSVControllerTest(){
        
        List<WST_Waste_Statement__c> wsts = new List<WST_Waste_Statement__c>();
        List<SD_ServiceDemand__c> sds = new List<SD_ServiceDemand__c>();
        List<Opportunity> Opps = TestDataFactory.createOpportunityListWithInsert('oppWSTGenerateCSV', 2);
        List<Quote> quotes = TestDataFactory.createQuoteListWithInsert(Opps);
        TestDataFactory.manageSyncedOpportunityList(Opps, quotes);
        
        Integer i = 0;
        
        
        Account acc = TestDataFactory.createAccount('Capgemini_PER');
        insert acc;
        
        Contact ctc = TestDataFactory.createContactWithInsert(acc);
        
        SD_ServiceDemand__c sd = TestDataFactory.createServiceDemand(acc, ctc, 'Remediation');
        insert sd;
        
        
        for(WST_Waste_Statement__c wst : TestDataFactory.createWasteStatementList(sd, 2)){
            wst.TransfoFromRehab__c = false;
            wst.Brand__c = 'ALS';
            wst.KVA__c = 214;
            wst.Year__c = '1983';
            wst.Name = '45678909876';
            wst.CAP_Number__c = i +'65434578';
            wst.DSNumber__c = '3456789098'+i; 
            wst.Opportunity__c = Opps.get(i).ID;
            wsts.add(wst);
            i++;
        }
        
        insert wsts;
        
        List<QuoteLineItem> vQuoteLineItem = TestDataFactory.createQuoteLineItemPerQuotes(quotes, 2);
        
        Test.startTest();
        
        
        
        Test.setCurrentPage(Page.WST_GenerateCSV);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(wsts);
        stdSetController.setSelected(wsts);
        
        
        WasteStatementGenerateCSVController sdGenerateCSVController = new WasteStatementGenerateCSVController(stdSetController);
        System.assertNotEquals(null, wsts);
        
        Test.stopTest();
        
    }
    
    
    @isTest
    public static void generateHeaderTest(){
        
        String expected = 'NUM_DS;ID_SF_TRANSFO;TYPE_SERVICE;NUM_SERIE_TRANSFO;NUM_CAP;CODE_PRESTA;DESIGNATION;QUANTITE;PRIX_UNITAIRE;CODET;CODE_DEVIS;NUM_COMMANDE;DESTRUCTION;POIDS_HUILE;POIDS_METAL;FILLER\n';
        String result = WasteStatementGenerateCSVController.generateHeader();
        Test.startTest();
        
        System.assertEquals(expected, result);
        
        
        Test.stopTest();
        
    }
    
	@IsTest
    public static void generateContentTest(){
        
        List<WST_Waste_Statement__c> wsts = new List<WST_Waste_Statement__c>();
        List<SD_ServiceDemand__c> sds = new List<SD_ServiceDemand__c>();
        List<Opportunity> Opps = TestDataFactory.createOpportunityListWithInsert('oppWSTGenerateCSV', 2);
        List<Quote> quotes = TestDataFactory.createQuoteListWithInsert(Opps);
        TestDataFactory.manageSyncedOpportunityList(Opps, quotes);
        
        Integer i = 0;
        
        
        Account acc = TestDataFactory.createAccount('Capgemini_PER');
        insert acc;
        
        Contact ctc = TestDataFactory.createContactWithInsert(acc);
        
        SD_ServiceDemand__c sd = TestDataFactory.createServiceDemand(acc, ctc, 'Remediation');
        insert sd;
        
        
        for(WST_Waste_Statement__c wst : TestDataFactory.createWasteStatementList(sd, 2)){
            wst.TransfoFromRehab__c = false;
            wst.Brand__c = 'ALS';
            wst.KVA__c = 214;
            wst.Year__c = '1983';
            wst.Name = '45678909876';
            wst.CAP_Number__c = i +'65434578';
            wst.DSNumber__c = '3456789098'+i; 
            wst.Opportunity__c = Opps.get(i).ID;
            wsts.add(wst);
            i++;
        }
        
        insert wsts;
        
        List<QuoteLineItem> vQuoteLineItem = TestDataFactory.createQuoteLineItemPerQuotes(quotes, 2);
        System.debug('QLs'+vQuoteLineItem);
        Test.startTest();
        
        
        
        Test.setCurrentPage(Page.WST_GenerateCSV);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(wsts);
        stdSetController.setSelected(wsts);
        
        
        WasteStatementGenerateCSVController sdGenerateCSVController = new WasteStatementGenerateCSVController(stdSetController);
        String result = WasteStatementGenerateCSVController.generateContent();
     
        System.assertNotEquals(null, result);

        System.assertEquals(4, (result.split('\n')).size());
        Test.stopTest();
        
    }
    
    
    @IsTest
    public static void generateCSVTest(){
        
        List<WST_Waste_Statement__c> wsts = new List<WST_Waste_Statement__c>();
        List<SD_ServiceDemand__c> sds = new List<SD_ServiceDemand__c>();
        List<Opportunity> Opps = TestDataFactory.createOpportunityListWithInsert('oppWSTGenerateCSV', 2);
        List<Quote> quotes = TestDataFactory.createQuoteListWithInsert(Opps);
        TestDataFactory.manageSyncedOpportunityList(Opps, quotes);
        
        Integer i = 0;
        
        
        Account acc = TestDataFactory.createAccount('Capgemini_PER');
        insert acc;
        
        Contact ctc = TestDataFactory.createContactWithInsert(acc);
        
        SD_ServiceDemand__c sd = TestDataFactory.createServiceDemand(acc, ctc, 'Remediation');
        insert sd;
        
        
        for(WST_Waste_Statement__c wst : TestDataFactory.createWasteStatementList(sd, 2)){
            wst.TransfoFromRehab__c = false;
            wst.Brand__c = 'ALS';
            wst.KVA__c = 214;
            wst.Year__c = '1983';
            wst.Name = '45678909876';
            wst.CAP_Number__c = i +'65434578';
            wst.DSNumber__c = '3456789098'+i; 
            wst.Opportunity__c = Opps.get(i).ID;
            wsts.add(wst);
            i++;
        }
        
        insert wsts;
        
        List<QuoteLineItem> vQuoteLineItem = TestDataFactory.createQuoteLineItemPerQuotes(quotes, 2);
        System.debug('QLs'+vQuoteLineItem);
        Test.startTest();
        
        
        
        Test.setCurrentPage(Page.WST_GenerateCSV);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(wsts);
        stdSetController.setSelected(wsts);
        
        
        WasteStatementGenerateCSVController sdGenerateCSVController = new WasteStatementGenerateCSVController(stdSetController);
        sdGenerateCSVController.generateCSV();
        
        String CSV = sdGenerateCSVController.CSV;
        String Name = sdGenerateCSVController.fileName;
        System.assertEquals(null, Name);
        
        System.assertNotEquals(null, CSV);
        
        System.assertEquals(5, (CSV.split('\n')).size());
        
        Test.stopTest();
        
    }
}