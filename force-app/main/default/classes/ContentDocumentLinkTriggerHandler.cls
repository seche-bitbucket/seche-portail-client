/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ContentDocumentLinkTriggerHandler
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ContentDocumentLinkTriggerHandler trigger
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 23-03-2020 PMB 1.0 - Initial version
* -- 07-07-2020 PMB 1.0 - add blockContentDocumentLinkSizeTitle
* --------------------------------------------------------------------------------------------------------------------
*/
public with sharing class ContentDocumentLinkTriggerHandler {
    public static void onAfterInsert(Map<Id,ContentDocumentLink> mapContentDocumentLink,Map<Id,ContentDocumentLink> mapOldContentDocumentLink){
        blockContentDocumentLinkSizeTitle(mapContentDocumentLink);

        // get all cdl parent which are QLY_SchoolForm__c
        List<Id> lstParentId = new List<Id>();
        for(ContentDocumentLink cdl: mapContentDocumentLink.values()) {
            if(cdl.LinkedEntityId.getsObjectType() == QLY_SchoolForm__c.sobjectType) {
                lstParentId.add(cdl.LinkedEntityId);
            }
        }

        if(lstParentId.size() > 0) {
            // get all cdls from linkedentityid list
            List<ContentDocumentLink> lstCdls = [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN :lstParentId AND Contentdocument.Title Like 'QLY_%'];

            List<Id> docIds = new List<Id>();
            for(ContentDocumentLink cdl: lstCdls) {
                docIds.add(cdl.ContentDocumentId);
            }
            Map<Id, ContentDocument> mapDocs = new Map<Id, ContentDocument>([SELECT Id, CreatedDate FROM ContentDocument WHERE Id IN :docIds]);

            // regroup by linkedentityid
            Map<Id, List<ContentDocument>> mapCd2Parent = new Map<Id, List<ContentDocument>>();
            for(ContentDocumentLink cdl: lstCdls) {
                if(mapCd2Parent.containsKey(cdl.LinkedEntityId)) {
                    List<ContentDocument> tmp = mapCd2Parent.get(cdl.LinkedEntityId);
                    tmp.add(mapDocs.get(cdl.ContentDocumentId));
                    mapCd2Parent.put(cdl.LinkedEntityId, tmp);
                } else {
                    mapCd2Parent.put(cdl.LinkedEntityId, new List<ContentDocument>{mapDocs.get(cdl.ContentDocumentId)});
                }
            }

            List<List<ContentDocument>> lstOfRecordToprocess = new List<List<ContentDocument>>();
            for(Id key: mapCd2Parent.keySet()) {
                if(mapCd2Parent.get(key).size() > 0) {
                    lstOfRecordToprocess.add(mapCd2Parent.get(key));
                }
            }

            List<String> idsToDelete = new List<String>();
            // check if we need to process
            if(lstOfRecordToprocess.size() > 0) {
                for(List<ContentDocument> lst: lstOfRecordToprocess) {
                    Integer noToDelete = null; // force it to be null
                    // loop to get the recent doc
                    for(Integer i = 0; i < lst.size(); i++) {
                        if(noToDelete == null) {
                            noToDelete = i;
                        } else {
                            if(lst[i].CreatedDate > lst[noToDelete].CreatedDate) {
                                noToDelete = i;
                            }
                        }
                    }
                    // we remove the recent pdf
                    lst.remove(noToDelete);
                    // we delete all the rest
                    for(ContentDocument cd: lst) {
                        idsToDelete.add(cd.Id);
                    }
                }
            }

            // delete all
            if(idsToDelete.size() > 0) {
                List<ContentDocumentLink> cdlsToDelete = [SELECT Id FROM ContentDocumentLink WHERE ContentDocumentId IN :idsToDelete];
                List<ContentVersion> vToDelete = [SELECT Id FROM ContentVersion WHERE ContentDocumentId IN :idsToDelete];
                List<ContentDocument> dToDelete = [SELECT Id FROM ContentDocument WHERE Id IN :idsToDelete];

                //delete cdlsToDelete;
                delete dToDelete;
            }
        }

    }

    public static void blockContentDocumentLinkSizeTitle(Map<Id,ContentDocumentLink> mapContentDocumentLink){
        Set<Id> docId = new Set<Id>();
        Map<Id,Id> docIdIdcdl = new Map<Id,Id>();
        List<ContentDocumentLink> lstErrorCdl = new List<ContentDocumentLink>();
        for(Id idContentLink : mapContentDocumentLink.keySet()){
            docId.add(mapContentDocumentLink.get(idContentLink).ContentDocumentId);
            docIdIdcdl.put(mapContentDocumentLink.get(idContentLink).ContentDocumentId,idContentLink);
        }
        List<ContentVersion> lstcv = [SELECT Id,Title,ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN:docId];
        for(ContentVersion cv : lstcv){
            if(cv.Title.length()>75){
                lstErrorCdl.add(mapContentDocumentLink.get(docIdIdcdl.get(cv.ContentDocumentId)));
            }
        }
        if(lstErrorCdl.size()>0){
            for(ContentDocumentLink cdl : lstErrorCdl){
                cdl.addError(Constants.STR_CVE_ERRORMESSAGE_SIZE_TITLE);
            }
        }
    }
}