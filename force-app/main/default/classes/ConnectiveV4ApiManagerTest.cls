/**
 * @description       : 
 * @author            : MAIT - @moidriss
 * @group             : 
 * @last modified on  : 11-20-2023
 * @last modified by  : MAIT - @moidriss
**/
@isTest
public class ConnectiveV4ApiManagerTest {
    
    @testSetup
    public static void makeData() {
        ConnectiveData__c data = new ConnectiveData__c();
        data.SigningUrl__c = 'https://sechegroup-dev.connective.eu/esig/public/actioninit?';
        insert data;
    }
    
    private static CAP__c getCapWithDoc(){
        CAP__c cap = TestFactory.generateCap(null, null);
        ContentDocumentLink cdl2 = TestFactory.linkDoc(cap.Id, 'testCap');
        return cap;
    }
    
    @isTest
    public static void test() {
        
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        Lead lead = TestFactory.generateLead();
        
        ConnectiveV4Mock.c = ct;
        
        FIP_FIP__c fip = TestFactory.generateFip(ct, acc);
        
        ContentDocumentLink cdl = TestFactory.linkDoc(fip.Id, 'testFip - packageOK');
        
        CAP__c cap = getCapWithDoc();
        TestFactory.linkDoc(lead.Id, 'tesLead');
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveV4Mock());
        
        ConnectiveV4ApiManager.createPackage(ConnectiveV4ApiUtils.createPackage(fip.Id, null), fip.Id);
        ConnectiveV4ApiManager.createPackage(ConnectiveV4ApiUtils.createPackage(cap.Id, ct.Id), cap.Id);
        ConnectiveV4ApiManager.createPackage(ConnectiveV4ApiUtils.createPackage(lead.Id, ct.Id), lead.Id);
        
        //DocumentsForSignature__c doc = [SELECT Id, Name, Status__c FROM DocumentsForSignature__c LIMIT 1];
        
        //System.assertEquals('ttest001', doc.Name);
        Test.stopTest();
        
    }
    
    @isTest
    public static void testNOk() {
        
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        ConnectiveV4Mock.c = ct;
        
        FIP_FIP__c fip = TestFactory.generateFip(ct, acc);
        
        ContentDocumentLink cdl = TestFactory.linkDoc(fip.Id, 'testFip - packageNOK');
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveV4Mock());
        
        ConnectiveV4ApiManager.createPackage(ConnectiveV4ApiUtils.createPackage(fip.Id, null), fip.Id);
        
        Test.stopTest();
        
    }
    
    @isTest
    public static void testStatus() {
        
        Contact ct = TestFactory.generateContact();
        ConnectiveV4Mock.c = ct;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveV4Mock());
        
        String bodyOK = ConnectiveV4ApiManager.getStatusByConnectivePackageId('packageOK');
        String bodyNOK = ConnectiveV4ApiManager.getStatusByConnectivePackageId('idTestNOk');
        
        System.assertEquals(null, bodyNOk);
        System.assertNotEquals(null, bodyOk);
        
        Test.stopTest();
        
    }
    
    @isTest
    public static void testRezminder() {

        ByPassUtils.ByPass('QuoteTrigger');
            
        QteLineItemSyncTriggerV2Handler.stop();
        OppLineItemSyncTriggerV2Handler.stop();
        QuoteSyncTriggerV2Handler.stop();
        OppSyncTriggerV2Handler.stop();
        
        Lead ld = TestFactory.generateLead();
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        ld.OwnerId = UserInfo.getUserId();
        update ld;
        
        Opportunity opp1 = TestFactory.generateOpport(acc, null);
        Quote qte = TestFactory.generateQuote(ct, acc, opp1);
        
        FIP_FIP__c fip = TestFactory.generateFip(ct, acc);
        
        ContentDocumentLink cdl = TestFactory.linkDoc(fip.Id, 'testFip - packageOK');
        
        PackageV4Wrapper pkg = ConnectiveV4ApiUtils.createPackage(fip.Id, null);
        
        System.assertEquals('testFip - packageOK', pkg.Name);
        
        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        a.SIRET_Number__c = '11111111111113';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        Product2 prd = new product2(name = 'Test' ,TECH_ExternalID__c = '012345',Family = 'Traitement', isActive = true );
        insert prd;
        
        PricebookEntry pbe = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=Test.getStandardPricebookId(),
                                 isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem (OpportunityID = opp.id,PriceBookEntryID=pbe.id, quantity=4, UnitPrice=4);
            insert oli;

        OpportunityLineItem oli2 = new OpportunityLineItem (OpportunityID = opp1.id,PriceBookEntryID=pbe.id, quantity=4, UnitPrice=4);
        insert oli2;
            
        Id rtId = [SELECT Id FROM RecordType WHERE DeveloperName LIKE '%Attestation_Producteur_DND%'][0].Id;
        Attestation__c prefip = new Attestation__c(Filiale__c='Séché Eco Industries La Dominelais', Account__c = acc.Id, RecordTypeId = rtId, OpportunityLineItem__c = oli2.Id);
        insert prefip;
        
        Attestation__c prefip2 = new Attestation__c(Filiale__c='Opale Environnement', Account__c = a.Id, RecordTypeId = rtId, OpportunityLineItem__c = oli.Id);
        insert prefip2;
        
        Package__c pkgSign = new Package__c(ConnectivePackageId__c = 'packageOK');
        Package__c pkgRejected = new Package__c(ConnectivePackageId__c = 'idTestNOk');
    	
        Insert new List<Package__c>{pkgRejected, pkgSign};
            
        CAP__c cap = getCapWithDoc();
        
        DocumentsForSignature__c docSign = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign.Id, TECH_ExternalId__c='Signed', Attestation__c=prefip.Id);
        DocumentsForSignature__c docSign2 = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign.Id, TECH_ExternalId__c='Rejected', Quote__c=qte.Id);
        DocumentsForSignature__c docRejected = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgRejected.Id, TECH_ExternalId__c='Rejected', Attestation__c=prefip2.Id);
        DocumentsForSignature__c docSignCap = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign.Id, TECH_ExternalId__c='Signed', CAP__c=cap.Id);
        DocumentsForSignature__c docSignLead = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign.Id, TECH_ExternalId__c='Signed', Lead__c=ld.Id);
        
        Insert new List<DocumentsForSignature__c>{docSign, docSign2, docRejected, docSignCap, docSignLead};
            
        ConnectiveV4Mock.c = ct;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveV4Mock());
        
        String str = ConnectiveV4ApiManager.sendRedminder(prefip.Id);
        String str2 = ConnectiveV4ApiManager.sendRedminder(prefip2.Id);
        String str1 = ConnectiveV4ApiManager.sendRedminder(qte.Id);
        ConnectiveV4ApiManager.sendRedminder(cap.Id);
        ConnectiveV4ApiManager.sendRedminder(ld.Id);

        System.debug([SELECT ID, ErrorMessage__c FROM Log__c]);
        
        System.assertEquals('OK', str);
        
        Test.stopTest();
        
    }
    
    @isTest
    public static void testRezminder1() {
        
        Lead ld = TestFactory.generateLead();
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        ld.OwnerId = UserInfo.getUserId();
        update ld;
        
        Opportunity opp = TestFactory.generateOpport(acc, null);
        Quote qte = TestFactory.generateQuote(ct, acc, opp);
        
        FIP_FIP__c fip = TestFactory.generateFip(ct, acc);
        
        ContentDocumentLink cdl = TestFactory.linkDoc(fip.Id, 'testFip - packageOK');
        
        PackageV4Wrapper pkg = ConnectiveV4ApiUtils.createPackage(fip.Id, null);
        
        System.assertEquals('testFip - packageOK', pkg.Name);
        
        Package__c pkgSign = new Package__c(ConnectivePackageId__c = 'packageOK');
        Package__c pkgRejected = new Package__c(ConnectivePackageId__c = 'idTestNOk');
    	
        Insert new List<Package__c>{pkgRejected, pkgSign};
        DocumentsForSignature__c docSign = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign.Id, TECH_ExternalId__c='Rejected', Quote__c=qte.Id);
        
        Insert new List<DocumentsForSignature__c>{docSign};
            
        ConnectiveV4Mock.c = ct;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveV4Mock());
        
        String str1 = ConnectiveV4ApiManager.sendRedminder(qte.Id);

        System.debug([SELECT ID, ErrorMessage__c FROM Log__c]);
        
        System.assertEquals('OK', str1);
        
        Test.stopTest();
        
    }

    @isTest
    public static void testRezminder1_bis() {
        
        Lead ld = TestFactory.generateLead();
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        ld.OwnerId = UserInfo.getUserId();
        update ld;
        
        Opportunity opp = TestFactory.generateOpport(acc, null);
        Quote qte = TestFactory.generateQuote(ct, acc, opp);
        
        FIP_FIP__c fip = TestFactory.generateFipAsbestos(ct, acc);
        
        ContentDocumentLink cdl = TestFactory.linkDoc(fip.Id, 'testFip - packageOK');
        
        PackageV4Wrapper pkg = ConnectiveV4ApiUtils.createPackage(fip.Id, null);
        
        System.assertEquals('testFip - packageOK', pkg.Name);
        
        Package__c pkgSign = new Package__c(ConnectivePackageId__c = 'packageOK');
        Package__c pkgRejected = new Package__c(ConnectivePackageId__c = 'idTestNOk');
    	
        Insert new List<Package__c>{pkgRejected, pkgSign};
        
        DocumentsForSignature__c docSign2 = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign.Id, TECH_ExternalId__c='Rejected', Quote__c=qte.Id);
        
        Insert new List<DocumentsForSignature__c>{docSign2};
            
        ConnectiveV4Mock.c = ct;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveV4Mock());
        
        String str1 = ConnectiveV4ApiManager.sendRedminder(qte.Id);

        System.debug([SELECT ID, ErrorMessage__c FROM Log__c]);
        
        System.assertEquals('OK', str1);
        
        Test.stopTest();
        
    }
    
    @isTest
    public static void testRezminder2() {
        
        Lead ld = TestFactory.generateLead();
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        ld.OwnerId = UserInfo.getUserId();
        update ld;
        
        Opportunity opp = TestFactory.generateOpport(acc, null);
        Quote qte = TestFactory.generateQuote(ct, acc, opp);
        
        FIP_FIP__c fip = TestFactory.generateFip(ct, acc);
        
        ContentDocumentLink cdl = TestFactory.linkDoc(fip.Id, 'testFip - packageOK');
        
        PackageV4Wrapper pkg = ConnectiveV4ApiUtils.createPackage(fip.Id, null);
        
        System.assertEquals('testFip - packageOK', pkg.Name);

        Product2 prd = new product2(name = 'Test' ,TECH_ExternalID__c = '012345',Family = 'Traitement', isActive = true );
        insert prd;
        
        PricebookEntry pbe = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=Test.getStandardPricebookId(),
                                 isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem (OpportunityID = opp.id,PriceBookEntryID=pbe.id, quantity=4, UnitPrice=4);
        insert oli;
            
        Id rtId = [SELECT Id FROM RecordType WHERE DeveloperName LIKE '%Attestation_Producteur_DND%'][0].Id;
        Attestation__c prefip = new Attestation__c(Filiale__c='Séché Eco Industries La Dominelais', Account__c = acc.Id, RecordTypeId = rtId, OpportunityLineItem__c = oli.Id);
        insert prefip;
        
        Package__c pkgSign = new Package__c(ConnectivePackageId__c = 'packageOK');
        Package__c pkgRejected = new Package__c(ConnectivePackageId__c = 'idTestNOk');
    	
        Insert new List<Package__c>{pkgRejected, pkgSign};
        
        DocumentsForSignature__c docSign2 = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign.Id, TECH_ExternalId__c='Rejected', Attestation__c=prefip.Id);
        
        Insert new List<DocumentsForSignature__c>{docSign2};
            
        ConnectiveV4Mock.c = ct;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveV4Mock());
        
        String str2 = ConnectiveV4ApiManager.sendRedminder(prefip.Id);

        System.debug([SELECT ID, ErrorMessage__c FROM Log__c]);
        
        System.assertEquals('OK', str2);
         
        Test.stopTest();
        
    }
    
    @isTest
    public static void updatedoc() {
        
        Lead ld = TestFactory.generateLead();
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        ld.OwnerId = UserInfo.getUserId();
        update ld;
        
        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        a.SIRET_Number__c = '11111111111112';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        Product2 prd = new product2(name = 'Test' ,TECH_ExternalID__c = '012345',Family = 'Traitement', isActive = true );
        insert prd;
        
        PricebookEntry pbe = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=Test.getStandardPricebookId(),
                                 isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem (OpportunityID = opp.id,PriceBookEntryID=pbe.id, quantity=4, UnitPrice=4);
            insert oli;
            
        Id rtId = [SELECT Id FROM RecordType WHERE DeveloperName LIKE '%Attestation_Producteur_DND%'][0].Id;
        Attestation__c prefip = new Attestation__c(Filiale__c='Opale Environnement', Contact__c = c.Id, Account__c = a.Id, RecordTypeId = rtId, OpportunityLineItem__c = oli.Id);
        insert prefip;
        
        Package__c pkgSign = new Package__c(ConnectivePackageId__c = 'packageOK');
    	
        Insert new List<Package__c>{pkgSign};
        
        DocumentsForSignature__c docSign = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign.Id, TECH_ExternalId__c='Signed', Attestation__c=prefip.Id);
        
        insert docSign;
        
        ConnectiveV4Mock.c = ct;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConnectiveV4Mock());
        
        String str2 = ConnectiveV4ApiManager.updateStatus(prefip.Id);
        ConnectiveV4ApiManager.updateStatus(getCapWithDoc().Id);
        ConnectiveV4ApiManager.updateStatus(ld.Id);
        ConnectiveV4ApiManager.updateStatus(ct.Id);
        
        System.assertEquals('OK', str2);
         
        Test.stopTest();
        
    }
    
    @isTest
    public static void callBack() {
        
        Lead ld = TestFactory.generateLead();
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        ld.OwnerId = UserInfo.getUserId();
        update ld;
        
        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        a.SIRET_Number__c = '11111111111115';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        opp.Assistant__c = UserInfo.getUserId();
        opp.Salesman__c = UserInfo.getUserId();
        insert opp;

        Quote qte = TestFactory.generateQuote(c, a, opp);
        
        Product2 prd = new product2(name = 'Test' ,TECH_ExternalID__c = '012345',Family = 'Traitement', isActive = true );
        insert prd;
        
        PricebookEntry pbe = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=Test.getStandardPricebookId(),
                                 isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem (OpportunityID = opp.id,PriceBookEntryID=pbe.id, quantity=4, UnitPrice=4);
            insert oli;
            
        Id rtId = [SELECT Id FROM RecordType WHERE DeveloperName LIKE '%Attestation_Producteur_DND%'][0].Id;
        Attestation__c prefip = new Attestation__c(Filiale__c='Opale Environnement', Account__c = a.Id, RecordTypeId = rtId, OpportunityLineItem__c = oli.Id);
        insert prefip;
        
        Package__c pkgSign = new Package__c(ConnectivePackageId__c = 'packageOK');
        Package__c pkgSign2 = new Package__c(ConnectivePackageId__c = 'packageOK_Quote');
    	
        Insert new List<Package__c>{pkgSign};
        
        DocumentsForSignature__c docSign = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign.Id, TECH_ExternalId__c='Signed', Attestation__c=prefip.Id);
        DocumentsForSignature__c docSign2 = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign2.Id, TECH_ExternalId__c='Signed', Quote__c=qte.Id);

        insert docSign;
        insert docSign2;
        
        String body = TestFactory.getResponseStatus('packageOK', 'Finished', null, null);
        String body2 = TestFactory.getResponseStatus('packageOK_Quote', 'Finished', null, null);
        
        Connective_CallBack__e ev = new Connective_CallBack__e();
        ev.JSON_Body__c = body;

        Connective_CallBack__e ev2 = new Connective_CallBack__e();
        ev2.JSON_Body__c = body2;
        
        Test.startTest();
        
        // Publish test event
        Database.SaveResult sr = EventBus.publish(ev);
            
        Test.stopTest();
        
        System.assertEquals(true, sr.isSuccess());

        docSign = [SELECT Status__c FROM DocumentsForSignature__c WHERE Id = :docSign.Id];

        //System.assertEquals('FINISHED', docSign.Status__c);
        
    }

    @isTest
    public static void callBack2() {
        
        Lead ld = TestFactory.generateLead();
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        ld.OwnerId = UserInfo.getUserId();
        update ld;
        
        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        a.SIRET_Number__c = '11111111111114';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        opp.Assistant__c = UserInfo.getUserId();
        opp.Salesman__c = UserInfo.getUserId();
        insert opp;

        Quote qte = TestFactory.generateQuote(c, a, opp);
        
        Product2 prd = new product2(name = 'Test' ,TECH_ExternalID__c = '012345',Family = 'Traitement', isActive = true );
        insert prd;
        
        PricebookEntry pbe = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=Test.getStandardPricebookId(),
                                 isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem (OpportunityID = opp.id,PriceBookEntryID=pbe.id, quantity=4, UnitPrice=4);
            insert oli;
            
        Id rtId = [SELECT Id FROM RecordType WHERE DeveloperName LIKE '%Attestation_Producteur_DND%'][0].Id;
        Attestation__c prefip = new Attestation__c(Filiale__c='Séché Eco Industries La Dominelais', Account__c = a.Id, RecordTypeId = rtId, OpportunityLineItem__c = oli.Id);
        insert prefip;
        
        Package__c pkgSign = new Package__c(ConnectivePackageId__c = 'packageOK');
        Package__c pkgSign2 = new Package__c(ConnectivePackageId__c = 'packageOK_Quote');
        Package__c callbackEvent = new Package__c(ConnectivePackageId__c = 'package123');
    	
        insert new List<Package__c>{pkgSign2, callbackEvent};
        
        CAP__c cap = getCapWithDoc();
        TestFactory.linkDoc(ld.Id, 'tesLead');
        
        DocumentsForSignature__c docSign = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign.Id, TECH_ExternalId__c='Signed', Attestation__c=prefip.Id);
        DocumentsForSignature__c docSign2 = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = pkgSign2.Id, TECH_ExternalId__c='Signed', Quote__c=qte.Id);
        DocumentsForSignature__c docSignCap = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = callbackEvent.Id, TECH_ExternalId__c='Signed', CAP__c=cap.Id);
        DocumentsForSignature__c docSignLead = new DocumentsForSignature__c(Status__c = 'PENDING', Package__c = callbackEvent.Id, TECH_ExternalId__c='Signed', Lead__c=ld.Id);

        insert new List<DocumentsForSignature__c>{docSign, docSign2, docSignCap, docSignLead};
        
        String body = TestFactory.getResponseStatus('packageOK', 'Finished', null, null);
        String body2 = TestFactory.getResponseStatus('packageOK_Quote', 'Finished', null, null);
        String bodyRejected = TestFactory.getResponseStatus('package123', 'Rejected', null, null);

        Connective_CallBack__e ev2 = new Connective_CallBack__e();
        ev2.JSON_Body__c = body2;
        
        Connective_CallBack__e eventRejected = new Connective_CallBack__e(JSON_Body__c=bodyRejected);
        
        Test.startTest();
        
        // Publish test event
        Database.SaveResult sr = EventBus.publish(ev2);
        Database.SaveResult srRejected = EventBus.publish(eventRejected);
            
        Test.stopTest();
        
        System.assertEquals(true, sr.isSuccess());
        System.assertEquals(true, srRejected.isSuccess());

        docSign2 = [SELECT Status__c FROM DocumentsForSignature__c WHERE Id = :docSign2.Id];

        //System.assertEquals('FINISHED', docSign2.Status__c);
        
    }

    @isTest
    public static void testRetrieveNameLead() {

        List<Lead> leads = new List<Lead>();

        Lead leadUnderContract = new Lead (LastName = 'Test Last Name', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Urgence Assainissement', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),'LED_WEB_TO_LEAD_ASSAINISSEMENT'),PostalCode = '69009',
                                 Quantity__c = 200, Filiale__c = 'Séché Eco Industries Changé',
                                 CustomerNature__c = 'Industriel', Industry = 'Chimie', Email = 'Test@Test.com', IsUnderContract__c = true);

        leads.add(leadUnderContract);

        Lead leadNotUnderContract = new Lead (LastName = 'Test Last NUC', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Urgence Assainissement', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),'LED_WEB_TO_LEAD_ASSAINISSEMENT'),PostalCode = '69009',
                                 Quantity__c = 200, Filiale__c = 'Séché Eco Industries Changé',
                                 CustomerNature__c = 'Industriel', Industry = 'Chimie', Email = 'Test@Test.com', IsUnderContract__c = false);
        
        leads.add(leadNotUnderContract);
        
        Lead leadDevisFlashGarage = new Lead (LastName = 'Test Last NUC', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Devis Flash Garage', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),'LED_WEB_TO_LEAD_ASSAINISSEMENT'),PostalCode = '69009',
                                 Quantity__c = 200, Filiale__c = 'Séché Eco Industries Changé',
                                 CustomerNature__c = 'Industriel', Industry = 'Chimie', Email = 'Test@Test.com');
        
        leads.add(leadDevisFlashGarage);
        
        insert leads;
                
        String nameLeadUC = ConnectiveV4ApiManager.retrieveName(leadUnderContract.Id);
        String nameLeadNUC = ConnectiveV4ApiManager.retrieveName(leadNotUnderContract.Id);
        String nameLeadDFG = ConnectiveV4ApiManager.retrieveName(leadDevisFlashGarage.Id);
        String nameCap = ConnectiveV4ApiManager.retrieveName(getCapWithDoc().Id);
        
        Contact contact = TestFactory.generateContact();
        Account account = TestFactory.generateAccount();
        Opportunity opp = TestFactory.generateOpport(account, 'Stage Name Test');
        
        FIP_FIP__c fip = TestFactory.generateFip(contact, account);
        Quote quote = TestFactory.generateQuote(contact, account, opp);
        Attestation__c attestation = new Attestation__c(
            Name='N° Attestion', 
            Status__c='Brouillon', 
            Account__c=account.Id, 
            AssociatedSalesRep__c=UserInfo.getUserId(),
            Sorted__c=true, 
            Producer__c = true, 
            Contact__c = contact.Id
            
        );
        insert attestation;
        
        ConnectiveV4ApiManager.retrieveName(fip.Id);
        ConnectiveV4ApiManager.retrieveName(quote.Id);
        ConnectiveV4ApiManager.retrieveName(attestation.Id);
    }
}