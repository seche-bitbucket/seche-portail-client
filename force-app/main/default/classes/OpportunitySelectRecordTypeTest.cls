@isTest
public class OpportunitySelectRecordTypeTest {
    
    @IsTest(SeeAllData=true) 
    public static void TestOpportunityControllerT(){
        //Set the current VF Page which use the ContactController
        PageReference pgOpportCompte = Page.OPP_NouvelleOpportuniteDepuisCompte;
        
        PageReference pgOpportContact = Page.OPP_NouvelleOpportuniteDepuisContact   ;
        
        //Standard Set controller
        ApexPages.StandardSetController stdSetCtrl;
        
        //Standard controller
        ApexPages.StandardController stdCtrl;
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Directeur commercial' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname');
        
        System.RunAs(currentUser) { 
            
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aucun', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try{
                insert ac;
            }catch(Exception e){
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '03 00 00 00 00', Salesman__c = currentUser.ID);
            
            try{
                insert contactTest;
            }catch(Exception e1){
                System.Debug('..............Exception Contact : '+e1);
            }
            
                          
            //    Start TEST
            
            Test.startTest();
            
            // Set the currentPage to the test
            Test.setCurrentPage(pgOpportCompte);
            //Set parameter 
            //System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! id Opport: ' + opportTest.Id);
            pgOpportCompte.getParameters().put('ID', ac.Id );
            
            //Initialize Controller
            VFC_OpportunitySelectRecordType ctrl = new VFC_OpportunitySelectRecordType(stdSetCtrl);
            ctrl.relatedOpt = 'standard';
            ctrl.updateRecordType();
            ctrl.recordType = [SELECT DeveloperName FROM RecordType WHERE SobjectType='Opportunity' AND DeveloperName LIKE :'%Standard' LIMIT 1].DeveloperName;
            ctrl.redirectRecordType();
            ctrl.redirectToAccount();
            
            ctrl.relatedOpt = 'Intermediaire';
            ctrl.updateRecordType();
            ctrl.redirectRecordType();
            ctrl.updateButton();
            
            
            
            Test.setCurrentPage(pgOpportContact);
            pgOpportContact.getParameters().put('ID', contactTest.Id );
            ctrl = new VFC_OpportunitySelectRecordType(stdSetCtrl);
            ctrl.relatedOpt = 'standard';
            ctrl.recordType = [SELECT DeveloperName FROM RecordType WHERE SobjectType='Opportunity' AND DeveloperName LIKE :'%Standard' LIMIT 1].DeveloperName;
            ctrl.updateRecordType();           
            ctrl.redirectRecordType();
            ctrl.redirectToContact();
           
            
            //Initialize Controller
            ctrl = new VFC_OpportunitySelectRecordType(stdCtrl);
            
            Test.stopTest();
            
            //    End TEST
        }
    }
}