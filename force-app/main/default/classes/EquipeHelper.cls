public class EquipeHelper {
    public static List<OpportunityTeamMember> getNewOpportunityTeamMemberDirector(Id eId, ID newUser, List<OpportunityTeamMember> otmRole){
        
        List<OpportunityTeamMember> otmNewRole = new List<OpportunityTeamMember>();
        
        for(OpportunityTeamMember otm : otmRole){
            OpportunityTeamMember otmNew = new OpportunityTeamMember();
            otmNew.UserId = newUser;
            otmNew.TeamMemberRole = 'Directeur Commercial';
            otmNew.OpportunityId = otm.OpportunityId;
            otmNew.OpportunityAccessLevel = 'Edit';
            otmNew.Tech_Equipe__c = eId;
            
            otmNewRole.add(otmNew);
        }
        
        return otmNewRole;
    }
    
    public static List<OpportunityTeamMember> getNewOpportunityTeamMemberAssistant(Id eId, ID newUser, List<OpportunityTeamMember> otmDirectors){
        
        
        List<OpportunityTeamMember> otmNewRole = new List<OpportunityTeamMember>();
        
        for(OpportunityTeamMember otm : otmDirectors){
            OpportunityTeamMember otmNew = new OpportunityTeamMember();
            otmNew.UserId = newUser;
            otmNew.TeamMemberRole = 'Assistant(e)';
            otmNew.OpportunityId = otm.OpportunityId;
            otmNew.OpportunityAccessLevel = 'Edit';
            otmNew.Tech_Equipe__c = eId;
            
            otmNewRole.add(otmNew);
        }
        
        return otmNewRole;
    }
    
}