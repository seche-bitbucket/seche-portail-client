/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : PackageV4Wrapper
* -- - Author : MOIDRISS
* -- - Company : Capgemini
* -- - Purpose : PackageV4Wrapper
* --
* -- Maintenance History:
* --
* -- Date Name  Ver  Remarks
* -- ---------- ---  ---------------------------------------------------------------------------------------------
* -- 15-03-2021 1.0  Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
public class PackageV4Wrapper {

    public String Name;		
    public String Initiator;
    public String Status;
    public String DocumentGroupCode;
    public String ExpiryDate;
    public List<Document> Documents;
    public List<Stakeholder> Stakeholders;
    public DefaultLegalNotice DefaultLegalNotice ;
    public String ThemeCode ;
    public String CallBackUrl ;
    public String NotificationCallBackUrl ;
    public String F2fRedirectUrl ;
    public Boolean IsUnsignedContentDownloadable ;
    public Boolean IsReassignEnabled ;
    public String ActionUrlExpirationPeriodInDays ;
    public String ProofCorrelationId ;
    public String TargetType ;
    public AutomaticReminder AutomaticReminder;

    public class AutomaticReminder {
        public Boolean IsSendAutomaticRemindersEnabled;
        public Boolean IsRepeatRemindersEnabled;
        public Decimal DaysBeforeFirstReminder;
        public Decimal RepeatReminders;
    }
    
    public class Document {
        public String Name;
        public String Language;
        public String ExternalReference;
        public List<Element> Elements;
        public String ProofCorrelationId;
        public DocumentOptions DocumentOptions;
        public RepresentationOptions RepresentationOptions;
    }

    public class Element {
        public String Type;
        public String ExternalReference;
        public String Marker;
        public Integer DocumentIndex;
        public Location Location;
        public Dimensions Dimensions;
        public List<String> SigningMethods;
        public LegalNotice LegalNotice;
    }

    public class Location {
        public String Page;
        public String Left;
        public String Top;
    }

    public class Dimensions {
        public String Width;
        public String Height;
        
    }

    public class DocumentOptions {
        public String TargetType;
        public PdfOptions PdfOptions;
        public String Base64data;
        public String ContentType;
    }

    public class PdfOptions {
        public String TargetFormat;
        public String PdfErrorHandling;
    }

    public class RepresentationOptions {
        public String Base64data;
        public String ContentType;
    }

    public class Stakeholder {
        public String FirstName;
        public String LastName;
        public String EmailAddress;
        public String Language;
        public String Type;
        public String ExternalReference;
        public List<Actor> Actors;
        public Boolean SendNotifications;
        public String BirthDate;
        public String PhoneNumber;
        public String AdditionalProperties;
    }
    
    public class Actor {
        public String Type;
        public List<Element> Elements;
        public String RedirectURL;
        public String RedirectType;
        public String BackButtonUrl;
        public Integer ProcessStep;
    }

    public class DefaultLegalNotice {
        public String Text;
    }

    public class LegalNotice {
        public String Text;
    }

}