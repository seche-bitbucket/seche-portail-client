@isTest
public class LC_Indicator_AugTarifCtrlTest {
    
    @isTest(SeeAllData=true) 
     static void testGetReportData(){
        List<String> result = new List<String>();        
        result.add('Amiante_Stockage_DD');        
        result = LC_Indicator_AugTarifCtrl.getReportData(result , String_Helper.CON_Filiale_Drimm, String_Helper.CON_Filiere_CentreDeTriDAE); 
        
        system.assertEquals(String.isEmpty(result[0]),false);
        
    }

}