public class wasteStatementTriggerUtil {
    
    
    //Change the price book of the WST if the WST isn't sales contrat
    //If the WST is checked as "TPC" it will have a different Pricebook
    public static void managePricebookAfterInsert(List<WST_Waste_Statement__c> wsts){
        Map<ID,WST_Waste_Statement__c> wstsMap = new Map<Id,WST_Waste_Statement__c>([Select ID, ServiceDemande__r.PriceBook__c FROM WST_Waste_Statement__c WHERE ID in : wsts]);
        List<WST_Waste_Statement__c> wstsToUPdate = new List<WST_Waste_Statement__c>();
        
        for(WST_Waste_Statement__c wst : wsts){
            WST_Waste_Statement__c  wstTMP = wstsMap.get(wst.ID);
            
            if(wst.isTPC__c){
                wstTMP.PriceBook__c = Label.RCT_PB_TPC;
            }else{
                wstTMP.PriceBook__c = wstTmp.ServiceDemande__r.PriceBook__c;
            }
            
            wstsToUPdate.add(wstTmp);
        }
        
        
        update wstsToUPdate;
    }
  	
    
    
    //Check the WST Type
    public static void manageWSTType(List<WST_Waste_Statement__c> wsts){
        
        for(WST_Waste_Statement__c wst : wsts){
            if(wst.Type__c != null){
                if((wst.Type__c).toUpperCase()== 'CABINE' || (wst.Type__c).toUpperCase()== 'CAB'){
                    wst.Type__c = 'Cabine';
                }else if((wst.Type__c).toUpperCase()== 'H61' || (wst.Type__c).toUpperCase()== 'POTEAU'){
                    wst.Type__c = 'Poteau';
                }
                
                
            }
            
        }
        
        
    }
    
    //Change the price book of the WST if the WST isn't sales contrat
    //If the WST is checked as "TPC" it will have a different Pricebook
    //Used when the Employee detect that the WST is a TPC
    public static void managePricebookBeforeUpdate(List<WST_Waste_Statement__c> wsts){
        
        Map<ID,WST_Waste_Statement__c> wstsMap = new Map<Id,WST_Waste_Statement__c>([Select ID, PriceBook__c, ServiceDemande__r.PriceBook__c FROM WST_Waste_Statement__c WHERE ID in : wsts]);
        Set<ID> toDeleteQuoteLine = new Set<ID>();
        System.debug('////Function TRIGGER WST');
        for(WST_Waste_Statement__c wst : wsts){
            WST_Waste_Statement__c  wstTMP = wstsMap.get(wst.ID);
            if(wst.isTPC__c){
                wst.PriceBook__c = Label.RCT_PB_TPC;
            }else{
                wst.PriceBook__c = wstTmp.ServiceDemande__r.PriceBook__c;
                System.debug('////Function TRIGGER WST :'+wst.PriceBook__c);
            }
            
            if(wstTMP.PriceBook__c != wst.PriceBook__c){
                toDeleteQuoteLine.add(wst.ID);
            }
            if(toDeleteQuoteLine.size() > 0){
                removeQuoteline(toDeleteQuoteLine);
            }
            
        }
    }
    
    
    public static void removeQuoteline(Set<ID> wsts){
        List<QuoteLine__c> qls = [SELECT ID FROM QuoteLine__c WHERE Transformateur__c = : wsts];
        delete qls;
        
    }
    
    public static void manageStateStatus(List<WST_Waste_Statement__c> wsts){
       
        for(WST_Waste_Statement__c wst : wsts){
            if(wst.Status__c == 'Demande effectuee' || wst.Status__c == 'Logistique'){
                wst.Etat__c = 'Demande effectuee';
            }else if(wst.Status__c == 'Recu' || wst.Status__c == 'Diagnostic effectue' || wst.Status__c == 'Commande proposee'){
                wst.Etat__c = 'Recu';
            }else if(wst.Status__c == 'Commande acceptee' || wst.Status__c == 'Devis accepte'){
                wst.Etat__c = 'Devis accepte';
            }else if(wst.Status__c == 'En reparation' || wst.Status__c == 'En peinture' || wst.Status__c == 'Pret'){
                wst.Etat__c = 'En reparation';
            }else if(wst.Status__c == 'En livraison'){
                wst.Etat__c = 'En livraison';
            }else if(wst.Status__c == 'Detruit'){
                wst.Etat__c = 'Detruit';
            }
               
        }
        
    }
    
    public static void assignSampleNumber(List<WST_Waste_Statement__c> wsts){
        
        Map<ID, Integer> SD_SampleNumber = getMapOfWSTbySD(wsts);
        
        for(WST_Waste_Statement__c wst : wsts){
            
            Integer i = SD_SampleNumber.get(wst.ServiceDemande__c);
            if(i != null){
                wst.SampleNumber__c = i;
                i++;
            }else{
                wst.SampleNumber__c = 1;
                i = 2;
            }
            SD_SampleNumber.put(wst.ServiceDemande__c, i);
        }
        
    }
    
     public static void manageQuoteStatus(List<WST_Waste_Statement__c> wsts){
       	List<Quote> qToUpdate = new List<Quote>();
         Set<ID> quoteToUpdate = new Set<ID>();
         List<Quote> quotes = new List<Quote>();
         
        for(WST_Waste_Statement__c wst : wsts){
            if(wst.Status__c == 'Commande proposee'){
                quoteToUpdate.add(wst.Opportunity__c);
            }
        }
         
         if(quoteToUpdate.size() > 0){
             quotes = [SELECT ID, Status FROM QUOTE 
                       WHERE Status != 'Rejeté' AND Status != 'Accepté' 
                       AND Status != 'Soumis pour approbation'
                       AND Status != 'Présenté' AND OpportunityID in : quoteToUpdate];
         }
         
         if(quotes.size() > 0){
             for(Quote q : quotes){
                 q.status = 'Présenté';
                 qToUpdate.add(q);
             }
             update qToUpdate;
         }
        
    }
    
    private static Map<ID, Integer> getMapOfWSTbySD(List<WST_Waste_Statement__c> wsts){
        
        Map<ID, Integer> SD_SampleNumber = new Map<ID, Integer>();
		Set<ID> sdIDs = new Set<ID>();
        
        for(WST_Waste_Statement__c wst : wsts){
            sdIDs.add(wst.ServiceDemande__c);
        }

		        
        List<SD_ServiceDemand__c> sds = [Select Id, Total__c FROM SD_ServiceDemand__c Where ID in : sdIDs ];
        
        for(SD_ServiceDemand__c sd : sds){ 
            SD_SampleNumber.put(sd.Id, (Integer) sd.Total__c + 1);
           
        }
        return SD_SampleNumber;
    }
}