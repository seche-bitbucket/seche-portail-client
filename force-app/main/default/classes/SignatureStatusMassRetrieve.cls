global class SignatureStatusMassRetrieve implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.RaisesPlatformEvents{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {	// retrieve contracts that don't have an opportunity linked to them
        String query = 'SELECT Id, Status__c, FlowId__c,TECH_ExternalId__c,OwnerId,Quote__r.OwnerId , Quote__r.QuoteCode__c, FIP_FIP__r.Name FROM DocumentsForSignature__c WHERE Status__c= \'PENDINGSIGNING\' OR Status__c=\'\'';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<DocumentsForSignature__c> scope)
    {	
        ConnectiveCallout.retrieveMassFlowStatus(scope);
    }
    global void finish(Database.BatchableContext BC)
    {  
        
    }
}