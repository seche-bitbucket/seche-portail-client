/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Mohammed Amine IDRISSI TAGHKI <mohammed-amine.idrissi-taghki@capgemini.com>
 * @version        1.0
 * @created        2022-02-21
 * @systemLayer    Extend Base Model
 *
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Desciption: Child class extends base model
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */
public with sharing class CB_Contact extends PolyMorphic_BaseClass {

    private class ErrorException extends Exception {}
    
    override
    public Object VInsert(String data, String apiName){
        try {  

            if(test.isRunningTest() && data == 'Error') {
                throw new ErrorException('Error');
            }

            SObject sobj = (SObject)JSON.deserialize(data, Type.forName('Schema.'+apiName));

            insert sobj;

            return (String)sobj.get('Id');

        } catch (Exception e) {
            Map<String, Object> errorMap = new Map<String, Object>();
            errorMap.put('message', e.getMessage());
            errorMap.put('line', e.getLineNumber());
            errorMap.put('source', 'CB_Case');
            errorMap.put('inputs', new Map<String, String>{'data' => data,
                                                           'apiName' => apiName});
            return errorMap;
        }
    }

}