@isTest (seeAllData=true)
public class OpportunityLineItemTriggerTest{
    
    @isTest(seeAllData=true)
    static void TestOpportunityLineItemTrigger(){
        
         //get standard pricebook
		Pricebook2  standardPb = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];

        Product2 prd1 = new Product2 (Name='Test Product Entry 1',Description='Test Product Entry 1',productCode = 'ABC', isActive = true, TECH_ExternalID__c='zExternalID_Capgemini');
        insert prd1;

        PricebookEntry pbe1 = new PricebookEntry (Product2ID=prd1.id,Pricebook2ID=standardPb.id,UnitPrice=50, isActive=true);
        insert pbe1;
        
        //Get a profile to create User
        Profile p = [select id from profile where name like :'standard user' limit 1];
        
        String testemail = 'director_-_User_test@test.com';
        
        User director = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname', IsActive=true);
        insert director;
        
         Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '01000';
        a.NAF_Number__c = '1234A';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Salesman__c = director.Id;
        c.Email = 'aaa@groupe-seche.com';
        c.Phone = '03 00 00 00 00';
        insert c;
       
        
        Opportunity opp = new Opportunity();
        opp.Name = 'test';
        opp.Salesman__c = director.Id;
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        opp.Pricebook2Id = pbe1.Pricebook2Id;
        insert opp;
        
        Test.startTest();

        OpportunityLineItem oli = new OpportunityLineItem (OpportunityID=opp.id,PriceBookEntryID=pbe1.id, quantity=4, totalprice=200);

        insert oli;
        
        Test.stopTest();
        
        
    }
}