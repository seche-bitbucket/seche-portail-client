/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-10-25
* @modified       2018-10-25
* @systemLayer    TriggerClass     
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Trigger handler class that updates the owner of the contract when we update the associatedSalesRep
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class ContractTriggerHandler {

    public static void ContractUpdate(List<Contract__c> contracts, Map<Id,Contract__c> ContractOldMap){
        for(Contract__c c:contracts){
            Contract__c oldc=ContractOldMap.get(c.Id);
            if(oldc.AssociatedSalesRep__c!=c.AssociatedSalesRep__c){
                c.OwnerId=c.AssociatedSalesRep__c;
            }
        }
    }

    public static void ContractConvention(List<Contract__c> contracts) /*contracts are not grouped by RegroupmentIndex__c*/ {
        
        if(FeatureManagement.checkPermission('CreateOppFromContractTrigger')) {
            ByPassUtils.ByPass('ContractTrigger');
            ByPassUtils.ByPass('OpportunityTrigger');
            ByPassUtils.ByPass('QuoteLineItemTrigger');

            //START : Group contract by RegroupmentIndex__c
            Map<String, List<Contract__c>> mapContractToRegroupment = new Map<String, List<Contract__c>>();
            Map<Decimal, Id> mapOpportToIndex = new Map<Decimal, Id>();
            Map<Id, Id> mapOppToUniCont = new Map<Id, Id>();
            List<Id> uniContractsIds = new List<Id>();
            Map<Id, Decimal> mapContractToIndex = new Map<Id, Decimal>();
            List<Contract__c> uniContracts = new List<Contract__c>();
            List<List<Contract__c>> grouppedContracts = new List<List<Contract__c>>();
            List<List<Contract__c>> groupError = new List<List<Contract__c>>();
            
            Integer index = 1;

            for(Contract__c ct: contracts) {
                Boolean check = (Test.isRunningTest()) ? ct.Status__c == 'Valide' : ct.Opportunity__c == null && ct.Status__c == 'Valide' && ct.TECH_Excluded__c == false;
                if(check) {
                    if(mapContractToRegroupment.containsKey(String.valueOf(ct.RegroupmentIndex__c)) && ct.RegroupmentIndex__c != null) {
                        List<Contract__c> tmp = mapContractToRegroupment.get(String.valueOf(ct.RegroupmentIndex__c));
                        tmp.add(ct);
                        mapContractToRegroupment.put(String.valueOf(ct.RegroupmentIndex__c), tmp);
                    } else if(ct.RegroupmentIndex__c != null) {
                        mapContractToRegroupment.put(String.valueOf(ct.RegroupmentIndex__c), new List<Contract__c>{ct});
                    } else {
                        uniContractsIds.add(ct.Id);
                        uniContracts.add(ct);
                    }
                }
            }
            
            System.debug(mapContractToRegroupment);

            if(mapContractToRegroupment.size() > 0 || uniContracts.size() > 0) {

                //Unicontracts - Groupped
                for(String key: mapContractToRegroupment.keySet()) {
                    if(mapContractToRegroupment.get(key).size() == 1) {
                        uniContracts.add(mapContractToRegroupment.get(key)[0]);
                        uniContractsIds.add(mapContractToRegroupment.get(key)[0].Id);
                    } else {
                        grouppedContracts.add(mapContractToRegroupment.get(key));
                    }
                }

                System.debug(grouppedContracts);

                if(grouppedContracts.size() > 0) {
                    //Check groupped Index
                    for(Integer i = grouppedContracts.size()-1; i == 0; i--) {
                        //Initialisation for verification
                        List<Contract__c> lstcts = grouppedContracts[i];
                        String accountId = lstcts[0].Account__c;
                        String filiale = lstcts[0].Filiale__c;
                        String filiere = lstcts[0].Filiere__c;
                        String contactRelated = lstcts[0].ContactRelated__c;

                        //If group error break
                        for(Contract__c ct: lstcts) {
                            if(!(ct.Account__c == accountId && ct.Filiale__c == filiale && ct.Filiere__c == filiere && ct.ContactRelated__c == contactRelated)) {
                                groupError.add(lstcts);
                                grouppedContracts.remove(i);
                                break;
                            }
                        }
                    }

                    //Populate lst Indexes
                    for(List<Contract__c> lstCts: grouppedContracts) {
                        mapContractToIndex.put(lstCts[0].Id, lstCts[0].RegroupmentIndex__c);
                    }
                }

                //END : Group contract by RegroupmentIndex__c

                //START : Initialisations 
                String Nextyear = System.Label.YearN1;
                String OPP_PREFIX = 'Convention';

                // Enteries IDs
                Id traitementId;
                Id prestationId;
                Id conditionnementId;
                Id valorisationId;

                Pricebook2 pricebook;   

                Map<Contract__c,Decimal> Indexes = new Map<Contract__c,Decimal>();
                Map<Id,String> OppContractMap=new Map<Id,String>();
                Map<String,String> filialeCodeMap = new Map<String , String>();

                List<Amendment__c> AmendmentList=new List<Amendment__c> ();
                List<Amendment__c> AmendmentList2=new List<Amendment__c> ();
                List<Quote> QuotesToInsert=new List<Quote>();
                List<Opportunity> OppToInsert=new List<Opportunity>();
                List<Contract__c> ContractsToUpdate=new List<Contract__c>();
                List<QuoteLineItem> QliToInsert=new List<QuoteLineItem>();
                List<FilialeCodeMapping__mdt> filialeCodes = [select Code__c , Filiale__c from FilialeCodeMapping__mdt];
                
                //Query #1
                RecordType r=[Select ID,Name From RecordType Where DeveloperName='Devis_approuv_Read_Only'];
                //END : Initialisations 

                //START : Processing
                for(FilialeCodeMapping__mdt temp:filialeCodes ){
                    filialeCodeMap.put(temp.Filiale__c, temp.Code__c);
                }
                
                if (Test.isRunningTest())   {
                    pricebook = new Pricebook2(Id=Test.getStandardPricebookId());
                }else{
                    pricebook = getStdPriceBook(); 
                }

                //Query #2
                List<PricebookEntry> pbEntries = [SELECT IsActive, UnitPrice, Product2Id, Pricebook2Id, Name, Id, UseStandardPrice 
                                                FROM PricebookEntry
                                                WHERE (Name = 'Autre Traitement' OR Name = 'Autre Prestation' OR Name = 'Autre Conditionnement' OR Name = 'Autre Valorisation') 
                                                AND IsActive = true AND Pricebook2Id=:pricebook.ID];

                for(PricebookEntry pbe: pbEntries) {
                    if(pbe.Name == 'Autre Traitement') {
                        traitementId = pbe.Id;
                    } else if(pbe.Name == 'Autre Prestation') {
                        prestationId = pbe.Id;
                    } else if(pbe.Name == 'Autre Conditionnement') {
                        conditionnementId = pbe.Id;
                    } else if(pbe.Name == 'Autre Valorisation') {
                        valorisationId = pbe.Id;
                    }
                }

                //START : Create Opp for each Contract non groupped
                if(uniContracts.size() > 0) {
                    for(Contract__c contract : uniContracts) {
                        Boolean check = (Test.isRunningTest()) ? true : contract.ContactRelated__r.Actif__c && !contract.IsUnderContract__c;
                        if(check) {
                            //START : Initialisations
                            String ReferenceWasteOwner='';
                            String NameOpp;
                            Boolean FuelPriceIndexation = false;
                            Boolean CommunalTax = false;
                            //END : Initialisations

                            //START : Vars Set
                            if(contract.Avenants__r.size()>0){
                                ReferenceWasteOwner=contract.Avenants__r[0].ThirdPartyProducerName__c;
                                FuelPriceIndexation=contract.Avenants__r[0].Indexation_gasoil__c;
                                CommunalTax=contract.Avenants__r[0].Communal_tax__c;
                            }

                            if(ReferenceWasteOwner != null){
                                NameOpp = OPP_PREFIX+' '+Nextyear+' - '+filialeCodeMap.get(contract.Filiale__c)+' - '+contract.Account__r.Name+' - '+ReferenceWasteOwner;
                                if(NameOpp.length() > 120){ //Limit Opportunity Name length exceeded
                                    NameOpp = OPP_PREFIX+' '+Nextyear+' - '+filialeCodeMap.get(contract.Filiale__c)+' - '+contract.Account__r.Name;
                                }
                            }else{
                                NameOpp = OPP_PREFIX+' '+Nextyear+' - '+filialeCodeMap.get(contract.Filiale__c)+' - '+contract.Account__r.Name;
                            }
                            //END : Vars Set

                            // push opp for creation
                            OppToInsert.add(new Opportunity(Name=NameOpp,
                                                            AccountId=contract.Account__c,CloseDate = date.today(),
                                                            StageName ='Proposition commerciale',TECH_Contrat2__c=contract.Id,ContractComment__c=contract.IncreaseComment__c,
                                                            ContactName__c=contract.ContactRelated__c,Salesman__c=contract.AssociatedSalesRep__c,
                                                            Filiale__c=contract.Filiale__c,Filiere__c=contract.Filiere__c,Nature__c='Renouvellement',
                                                            OwnerId=contract.AssociatedSalesRep__c,
                                                            ConventionType__c=contract.Type_de_convention__c,
                                                            ReferenceWasteOwner__c=ReferenceWasteOwner,                                             
                                                            Convention_face_face__c = contract.Face_Face__c,
                                                            DateFinEngagement__c= contract.ContractEndDate__c,
                                                            FuelPriceIndexation__c= FuelPriceIndexation ,
                                                            Communal_tax__c = CommunalTax,TECH_IsFromTarifIncrease__c = true,
                                                            DateRemiseOffre__c=date.newInstance(Integer.valueOf(System.Label.YearN1), 1, 1)));
                                
                        }
                    }
                }
                //END : Create Opp for each Contract non groupped

                System.debug(uniContracts.size());
                System.debug(grouppedContracts.size());
                System.debug(groupError.size());

                //START : Groupped Contract Management
                if(grouppedContracts.size() > 0) {
                    for(List<Contract__c> lstCts: grouppedContracts) {
                        //START : Initialisations
                        Boolean FuelPriceIndexation = false;
                        Boolean CommunalTax = false;
                        String ReferenceWasteOwner='';
                        String NameOpp;
                        //END : Initialisations

                        if(lstCts[0].Avenants__r.size()>0){
                            ReferenceWasteOwner = lstCts[0].Avenants__r[0].ThirdPartyProducerName__c;
                            FuelPriceIndexation = lstCts[0].Avenants__r[0].Indexation_gasoil__c;
                            CommunalTax = lstCts[0].Avenants__r[0].Communal_tax__c;
                        }
                        
                        if(ReferenceWasteOwner != null){
                            NameOpp = OPP_PREFIX+' '+Nextyear+' - '+filialeCodeMap.get(lstCts[0].Filiale__c)+' - '+lstCts[0].Account__r.Name+' - '+ReferenceWasteOwner;
                            if(NameOpp.length() > 120){ //Limit Opportunity Name length exceeded
                                NameOpp = OPP_PREFIX+' '+Nextyear+' - '+filialeCodeMap.get(lstCts[0].Filiale__c)+' - '+lstCts[0].Account__r.Name;
                            }
                        }else{
                            NameOpp = OPP_PREFIX+' '+Nextyear+' - '+filialeCodeMap.get(lstCts[0].Filiale__c)+' - '+lstCts[0].Account__r.Name;
                        }

                        OppToInsert.add(new Opportunity(Name=NameOpp,
                                                        AccountId=lstCts[0].Account__c,CloseDate = date.today()+30,
                                                        StageName ='Proposition commerciale',TECH_Contrat2__c=lstCts[0].Id,
                                                        ContactName__c=lstCts[0].ContactRelated__c,
                                                        Salesman__c=lstCts[0].AssociatedSalesRep__c,
                                                        Filiale__c=lstCts[0].Filiale__c,Filiere__c=lstCts[0].Filiere__c,
                                                        OwnerId=lstCts[0].AssociatedSalesRep__c,
                                                        Nature__c='Renouvellement',ConventionType__c=lstCts[0].Type_de_convention__c,
                                                        ReferenceWasteOwner__c=ReferenceWasteOwner,
                                                        DateFinEngagement__c= lstCts[0].ContractEndDate__c,
                                                        Convention_face_face__c = lstCts[0].Face_Face__c,
                                                        FuelPriceIndexation__c= FuelPriceIndexation ,
                                                        Communal_tax__c = CommunalTax,TECH_IsFromTarifIncrease__c = true,
                                                        DateRemiseOffre__c=date.newInstance(Integer.valueOf(System.Label.YearN1), 1, 1)));
                    }
                }
                //END : Groupped Contract Management

                System.debug(OppToInsert);
                System.debug(OppToInsert.size());

                //START : DML Operations
                if(OppToInsert.size()>0){
                    insert OppToInsert;
                }
                //END : DML Operations

                //Query #3
                List<Opportunity> Opps=[SELECT Name,AccountId,Account.Name,CloseDate,StageName,TECH_Contrat2__c,
                                        ContactName__c,Salesman__c,Salesman__r.Trigramme__c,Filiale__c,Filiere__c,
                                        Owner.Trigramme__c,Account.BillingStreet,Account.BillingCity,
                                        Account.BillingState,Account.BillingPostalCode,Account.BillingCountry,
                                        ContactName__r.Phone,Pricebook2Id,ContactName__r.Fax,ContactName__r.Email,
                                        Opportunity.TECH_QuoteCount__c,ReferenceWasteOwner__c,DateFinEngagement__c,
                                        DateRemiseOffre__c, Convention_face_face__c FROM Opportunity WHERE Id IN: OppToInsert];

                System.debug(Opps);
                                        
                for(Opportunity opp:Opps){

                    if(uniContractsIds.size() > 0 && uniContractsIds.contains(opp.TECH_Contrat2__c)) {
                        mapOppToUniCont.put(opp.TECH_Contrat2__c, opp.Id);
                    }

                    if(mapContractToIndex.size() > 0 && mapContractToIndex.containsKey(opp.TECH_Contrat2__c)) {
                        mapOpportToIndex.put(mapContractToIndex.get(opp.TECH_Contrat2__c), opp.Id);
                    }

                    Quote vQuote = new Quote();
                    vQuote.DateDevis__c = Date.today();

                    if(opp.TECH_QuoteCount__c == null){
                        vQuote.Name= opp.Name +' V'+ 1;
                    }else{
                        vQuote.Name= opp.Name +' V'+ (opp.TECH_QuoteCount__c + 1);
                    }

                    // if(opp.TECH_QuoteCount__c==null){
                    //     vQuote.Name= opp.Account.Name + ' ' +opp.ReferenceWasteOwner__c + ' '+ opp.Filiere__c +' V'+ 1;             
                    // }else{
                    //     vQuote.Name= opp.Account.Name + ' ' +opp.ReferenceWasteOwner__c + ' '+ opp.Filiere__c + +' V'+ (opp.TECH_QuoteCount__c + 1);
                    // }

                    vQuote.RecordTypeId=r.Id;
                    vQuote.ContactId=opp.ContactName__c;
                    vQuote.Pricebook2Id=pricebook.ID;
                    vQuote.TECH_isApprouved__c=true;
                    vQuote.OpportunityId = Opp.ID;
                    vQuote.DateDevis__c = Date.today();
                    vQuote.Quote_Type__c='Convention';
                    vQuote.status = 'Brouillon';
                    vQuote.Email = opp.ContactName__r.Email;
                    vQuote.Phone = opp.ContactName__r.Phone;
                    vQuote.Fax = opp.ContactName__r.Fax;
                    vQuote.BillingStreet = opp.Account.BillingStreet;
                    vQuote.BillingCity = opp.Account.BillingCity;
                    vQuote.BillingState = opp.Account.BillingState;
                    vQuote.BillingPostalCode = opp.Account.BillingPostalCode;
                    vQuote.BillingCountry = opp.Account.BillingCountry; 
                    vQuote.ExpirationDate=opp.DateFinEngagement__c;
                    vQuote.EffectiveDate__c=opp.DateRemiseOffre__c;
                    vQuote.TECH_IsFromTarifIncrease__c = true;
                    vQuote.ReferenceWasteOwner__c = opp.ReferenceWasteOwner__c;

                    if(opp.ReferenceWasteOwner__c == null){
                        vQuote.ShippingStreet = opp.Account.BillingStreet;
                        vQuote.ShippingCity = opp.Account.BillingCity;
                        vQuote.ShippingState = opp.Account.BillingState;
                        vQuote.ShippingPostalCode = opp.Account.BillingPostalCode;
                        vQuote.ShippingCountry = opp.Account.BillingCountry;
                    }

                    if(opp.Pricebook2Id != null) {
                        vQuote.Pricebook2Id = opp.Pricebook2Id;
                    }
                    
                    QuotesToInsert.add(vQuote);
                }

                System.debug(QuotesToInsert);
                System.debug(mapOppToUniCont);

                //START : DML Operations
                if(QuotesToInsert.size()>0){
                    insert QuotesToInsert;
                }

                // Simple retrieve for quotes with all fields
                //Query #4
                List<Quote> quotesAllFileds = Database.query('SELECT '+String.join(new List<String>(Schema.getGlobalDescribe().get('Quote').getDescribe().fields.getMap().keySet()), ', ')+', (SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId FROM ContentDocumentLinks) FROM Quote WHERE Id IN :QuotesToInsert');
                //END : DML Operations

                Map<Id,Id> QuoteOppIdMap= new Map<Id,Id>();
                Map<String, Id> OppTechContract2OppId = new Map<String, Id>();
                
                for(Quote q: QuotesToInsert){
                    QuoteOppIdMap.put(q.OpportunityId, q.Id);
                }

                System.debug(QuoteOppIdMap);

                //START : Apply group
                List<Contract__c> grouppedContracts_flattern = new List<Contract__c>();
                for(List<Contract__c> lstCts: grouppedContracts) {
                    grouppedContracts_flattern.addAll(lstCts);
                }

                System.debug(grouppedContracts_flattern);

                for(Contract__c ct: grouppedContracts_flattern) {
                    ct.Opportunity__c = mapOpportToIndex.get(ct.RegroupmentIndex__c);
                    ContractsToUpdate.add(ct);
                }
                //END : Apply group

                //START : Apply to uniContract
                for(Contract__c ct: uniContracts) {
                    ct.Opportunity__c = mapOppToUniCont.get(ct.Id);
                    ContractsToUpdate.add(ct);
                }
                //END : Apply to uniContract

                System.debug(ContractsToUpdate);

                //START : DML Operations
                if(ContractsToUpdate.size()>0){
                    update ContractsToUpdate;
                }
                //END : DML Operations

                //START : Generate Conga Documents
                Set<Id> quotesIds = new Set<Id>();
                List<ID> OpportunitiesIds=new List<Id>();
                for(Quote q: quotesAllFileds) {
                    quotesIds.add(q.Id);
                    if(q.ContentDocumentLinks.size() == 0){
                        OpportunitiesIDs.add(q.OpportunityID);
                    }
                }
                System.debug('OpportunitiesIDs : '+OpportunitiesIDs);

                //Query #5
                List<ConventionTemplateMapping__mdt> ConventionMapping =[SELECT Id, FilialeAndTypeConvention__c, TemplateName__c FROM ConventionTemplateMapping__mdt];
                System.debug('ConventionMapping : '+ConventionMapping);
                
                Map<String,String> TemplateByFiliale=new Map<String,String>();    
                for(ConventionTemplateMapping__mdt CTM: ConventionMapping){
                    TemplateByFiliale.put(CTM.FilialeAndTypeConvention__c, CTM.TemplateName__c);
                }
                System.debug('TemplateByFiliale : '+TemplateByFiliale);   

                //Query #5
                List<Contract__c> ContractsTogenerate = new List<Contract__c>();
                for(Contract__c ct: ContractsToUpdate) {
                    for(Id oppId: OpportunitiesIds) {
                        if(ct.Opportunity__c == oppId) {
                            ContractsTogenerate.add(ct);
                            break;
                        }
                    }
                }
                System.debug('ContractsToGenerate : '+ContractsTogenerate); 

                Map<Id,String> OpportunityAndTemplateName=new Map<Id,String>();      
                for(Contract__c c: ContractsTogenerate){
                    String FilialeAndTemplate = c.Filiale__c + c.Type_de_convention__c;
                
                    for(String t: TemplateByFiliale.keyset()){
                        if(FilialeAndTemplate == t){
                            OpportunityAndTemplateName.put(c.Opportunity__c, TemplateByFiliale.get(t));
                        }
                    }
                }
                System.debug('OpportunityAndTemplateName : '+OpportunityAndTemplateName.Values() );

                //Query #6
                List<APXTConga4__Conga_Template__c> Templates = [SELECT APXTConga4__Name__c, Id FROM APXTConga4__Conga_Template__c Where APXTConga4__Name__c IN: OpportunityAndTemplateName.Values()];
                System.debug('Templates :'+Templates );

                Map<ID,String> OppIdTemplateName=new Map<ID,String>();
                for(Id OppId:OpportunityAndTemplateName.keyset()){
                    for(APXTConga4__Conga_Template__c template:Templates){
                        if(OpportunityAndTemplateName.get(OppId)==template.APXTConga4__Name__c){
                            OppIdTemplateName.put(OppId,template.Id);
                        }
                    }
                }
                
                List<Quote> Quotes = new List<Quote>(); 
                for(Quote q: quotesAllFileds) {
                    for(Id qId: quotesIds) {
                        if(OppIdTemplateName.containsKey(q.OpportunityId) && q.Id == qId) {
                            Quotes.add(q);
                            break;
                        }
                    }
                }

                for(Quote q: Quotes){
                    for(ID oppid: OppIdTemplateName.keyset()){
                        if(oppid == q.OpportunityID){
                            q.CongaConventionID__c=OppIdTemplateName.get(oppId);
                            break;
                        }
                    }
                }

                for(Quote q : Quotes){
                    q.TECH_CongaAutomatedSending__c=true;
                }

                //START : DML Operations
                if(Quotes.size()>0){
                    update Quotes;
                }
                //END : DML Operations
                //END : Generate Conga Documents

                //START : Convert Amandement
                if(Test.isRunningTest()) {
                    List<Amendment__c> Amendments = new List<Amendment__c>();
                    Amendment__c am = new Amendment__c();
                    am.Contrat2__c=ContractsToUpdate[0].id;
                    am.WasteLabel__c='conditionnement amiante' + Integer.valueof((Math.random() * 100));
                    am.Nature__c='Conditionnement';
                    am.PriceNextYear__c=120;
                    am.QuantityNextYear__c =10;
                    am.UnitNextYear__c='Forfait';
                    am.QuantityInvoiced2018__c=11;
                    am.TECH_Excluded__c = false;
                    Amendment__c am2 = new Amendment__c();
                    am2.Contrat2__c=ContractsToUpdate[0].id;
                    am2.WasteLabel__c='Transport amiante'+Integer.valueof((Math.random() * 100));
                    am2.Nature__c='Prestation';
                    am2.PriceNextYear__c=120;
                    am2.QuantityNextYear__c=20;
                    am2.TECH_Excluded__c = false;
                    Amendment__c am3 = new Amendment__c();
                    am3.Contrat2__c=ContractsToUpdate[0].id;
                    am3.WasteLabel__c='traitement amiante'+Integer.valueof((Math.random() * 100));
                    am3.Nature__c='Traitement';
                    am3.PriceNextYear__c=120;
                    am3.QuantityNextYear__c=30;
                    am3.TECH_Excluded__c = false;
                    Amendment__c am4 = new Amendment__c();
                    am4.Contrat2__c=ContractsToUpdate[0].id;
                    am4.WasteLabel__c='valorisation amiante'+Integer.valueof((Math.random() * 100));
                    am4.Nature__c='Valorisation';
                    am4.PriceNextYear__c=120;
                    am4.QuantityNextYear__c=40;
                    am4.TECH_Excluded__c = false;
                    Amendments.add(am);
                    Amendments.add(am2);
                    Amendments.add(am3);
                    Amendments.add(am4);
                    insert Amendments;
                }
                AmendmentList = [SELECT Contrat2__c,Contrat2__r.Opportunity__c,UnitNextYear__c,ByPassContract__c,	QuantityNextYear__c ,IncludedTGAP__c,Planned_next_year__c,
                Nature__c,PriceCurrentYear__c,PriceNextYear__c,TechnicInfo__c,Label__c,WasteLabel__c,ActifAcceptationContrat__c,CEECode__c,Conditionnement__c,OnuCode__c,
                Class__c,Filiale__c,CommentaryAmendementsX3__c,HeaderContractX3__c,FooterContractX3__c,TextSalesX3__c,PackagingGroup__c,TECH_CatalogueMatch__c,
                LabelArticleCode__c, Filiere__c, Name, QuantityInvoiced2018__c FROM Amendment__c WHERE TECH_Excluded__c = false AND Contrat2__c IN :ContractsToUpdate];
                
                System.debug(AmendmentList.size());
                
                Map<String, Contract__c> contract2Id = new Map<String, Contract__c>();
                for(Contract__c contract: ContractsToUpdate){
                    contract2Id.put(contract.Id, contract);
                }
                
                for(Amendment__c Amendment:AmendmentList) {	
                    if(contract2Id.containsKey(Amendment.Contrat2__c)) {
                        QliToInsert.addall(createQuoteLineItems(Amendment,QuoteOppIdMap.get(contract2Id.get(Amendment.Contrat2__c).Opportunity__c),traitementId,prestationId,conditionnementId,valorisationId));
                    }
                }

                System.debug(QliToInsert.size());

                //START : DML Operations
                if(QliToInsert.size()>0){
                    try {
                        // insert QliToInsert;
                        Database.SaveResult[] srList = Database.insert(QliToInsert);

                        for (Database.SaveResult sr : srList)
                        {
                            System.debug(sr.isSuccess());
                            System.debug(sr.getErrors());
                            System.debug(sr.getId());
                        }
                        System.debug(QliToInsert);
                    } catch(Exception e) {
                        System.debug(e.getMessage()+' at '+e.getLineNumber());
                    }
                }
                //END : DML Operations

                //START : Sync Opp
                for(Opportunity opp:Opps){
                    if(QuoteOppIdMap.containsKey(opp.Id)) {
                        opp.SyncedQuoteId = QuoteOppIdMap.get(opp.Id);
                    }
                }
                System.enqueueJob(new AVE_QueueOppUpdate(Opps, QliToInsert));

                if(Test.isRunningTest()) {
                    System.enqueueJob(new AVE_QueueOLIsUpdateSync(Opps, QliToInsert));
                }
                //END : Sync Opp

                //END : Convert Amandement

                if(groupError.size() > 0) {
                    String htmlList = '';
                    for(List<Contract__c> lstCtsErr: groupError) {
                        htmlList += '<h4>Indice de regroupement : '+lstCtsErr[0].RegroupmentIndex__c+'</h4><ul>';
                        for(Contract__c ct: lstCtsErr) {
                            htmlList += '<li><a href="'+System.Url.getOrgDomainUrl()+'/'+ct.Id+'">'+ct.Name+'</a></li>';
                        }
                        htmlList += '</ul><br>';
                    }
                    Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
                    msg.setSubject('Augmentation Tarifaire - Erreur Regroupement Contrats');
                    msg.setToAddresses(new String[] {'tma_sf_seche.fr@capgemini.com', 'a.lepevedic@groupe-seche.com'});
                    msg.setSenderDisplayName('Augmentation Tarifaire - Erreur Regroupement Contrats');
                    msg.setReplyTo('sechecrm69@gmail.com');
                    msg.setHtmlBody('<p>Bonjour,</p>'+
                                    '<p>Une erreur est survenue lors de la génération des opportunités.<p>'+
                                    '<p><strong>Motifs :</strong> Erreur Regroupement</p>'+
                                    '<p><strong>Message :</strong> Les contrats regroupés doivent avoir les mêmes filiale, filière, compte et contact. Les opportunités n\'ont pas toutes été générées.<p>'+
                                    '<p><strong>Contrat concernés :</strong><p>'+htmlList);
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] {msg});
                }
            }
        }

    }

    //  -------------------------------------------------------
    //  getStdPriceBook: handle test class
    //  -------------------------------------------------------
    public static Pricebook2  getStdPriceBook () {
        Pricebook2 stdPriceBook =new  Pricebook2();
        stdPriceBook = [select id, name from Pricebook2 where isStandard = true and IsActive = true limit 1];
        return stdPriceBook;    
    }

    public static List<QuoteLineItem> createQuoteLineItems(Amendment__c Amendment,Id quoteId,Id traitementId,Id prestationId,Id conditionnementId,Id valorisationId){
        List<QuoteLineItem> QliLst=new List<QuoteLineItem>();
        
        if(amendment.Nature__c!=null){
            
            //Ticket 01238. Pour Drimm, la filière de la ligne d'opportunité peut être différente de la filière de l'avenant
            //--> Désormais géré dans le Flow "CAT_DND_SECTOR_AFFECTATION" fait par Aurélien
            String sector;

            String priceBookEntryId;
            switch on Amendment.Nature__c {
                when 'Traitement'{
                    priceBookEntryId = traitementId;
                }
                when 'Prestation'{
                    priceBookEntryId = prestationId;
                }
                when 'Conditionnement'{
                    priceBookEntryId = conditionnementId;
                }
                when 'Valorisation' {
                    priceBookEntryId = valorisationId;
                }
            }
            
            Decimal Quantity=1.0;
            if(Amendment.UnitNextYear__c!=null && Amendment.UnitNextYear__c.toLowerCase()!='tonne' && Amendment.QuantityInvoiced2018__c!=null && Amendment.QuantityInvoiced2018__c!=0){
                Quantity=Amendment.QuantityInvoiced2018__c;
            }else if(Amendment.QuantityNextYear__c!=null && Amendment.QuantityNextYear__c!=0){
                Quantity=Amendment.QuantityNextYear__c;
            }
            Decimal price=1.0;
            if(Amendment.PriceNextYear__c!=null && Amendment.PriceNextYear__c!=0){
                price=Amendment.PriceNextYear__c;
            }
            QuoteLineItem oli = new QuoteLineItem(QuoteId=quoteId,Unit__c=Amendment.UnitNextYear__c,
                                                            quantity=Quantity, UnitPrice=price,PriceBookEntryID=priceBookEntryId,
                                                            Description= amendment.WasteLabel__c,Features__c=amendment.TechnicInfo__c,
                                                            BuyingPrice__c=Amendment.PriceCurrentYear__c,
                                                            N_CAP__c=amendment.ActifAcceptationContrat__c,
                                                            EuropeanCode__c=amendment.CEECode__c,
                                                            Packaging__c=amendment.Conditionnement__c,
                                                            UNCode__c=amendment.OnuCode__c,Classe__c=amendment.Class__c,
                                                            Filiale__c=amendment.Filiale__c,GE__c=amendment.PackagingGroup__c,
                                                            Nature__c=amendment.Nature__c,TECH_CatalogueMatch__c=amendment.TECH_CatalogueMatch__c,
                                                            Label__c=amendment.Label__c,CommentaryAmendementsX3__c=amendment.CommentaryAmendementsX3__c,
                                                            HeaderContractX3__c=amendment.HeaderContractX3__c,IncludedTGAP__c=amendment.IncludedTGAP__c,
                                                            Planned_next_year__c= amendment.Planned_next_year__c,
                                                            FooterContractX3__c=amendment.FooterContractX3__c,TextSalesX3__c=amendment.TextSalesX3__c,
                                                            Amendment__c=amendment.Id, LabelArticleCode__c=amendment.LabelArticleCode__c,
                                                            Sector__c=sector);
            QliLst.add(oli);
        }

        System.debug(QliLst.size());

        return QliLst;
    }

}