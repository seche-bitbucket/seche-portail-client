/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-06-29
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Commercial Director Table controller 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing  class VFC_CommercialDirectorTableController {
    public Transient List<Contract__c> Contracts {get;set;}   
    public List<ContractWrapper> ContractSalesreps {get;set;} 
    public String filiale {get;set;} 
    public String filiere {get;set;} 
    
    public VFC_CommercialDirectorTableController(){      
	
        this.filiale = System.currentPageReference().getParameters().get('filiale');
        this.filiere = System.currentPageReference().getParameters().get('filiere');
        
        String target = ' ';
        String replacement = '_';
        Map<Id,List<String>> MapFilialeFiliereByContractId = new  Map<Id,List<String>>();
        Map<String,String> MapDeveloperNameByFilialeFiliere = new  Map<String,String>();
        this.ContractSalesreps=new List<ContractWrapper>();
        // retrieve all contracts that have an associated User
        if(filiale!= null && filiere != null){
             this.Contracts = [SELECT Id, Name, AssociatedSalesRep__c, AssociatedSalesRep__r.Name,
                          Filiale__c, Filiere__c, Account__c, Opportunity__c, RegroupmentIndex__c FROM Contract__c
                          Where AssociatedSalesRep__c!=null And Status__c ='Soumis' and Filiale__c=:filiale and Filiere__c=:filiere Order By AssociatedSalesRep__c ASC];

        }else{
             this.Contracts = [SELECT Id, Name, AssociatedSalesRep__c, AssociatedSalesRep__r.Name,
                          Filiale__c, Filiere__c, Account__c, Opportunity__c, RegroupmentIndex__c FROM Contract__c
                          Where AssociatedSalesRep__c!=null And Status__c ='Soumis' Order By AssociatedSalesRep__c ASC];

        }
       
         List<String> FilialeList = new List<String>(); 
         List<String> FiliereList = new List<String>();
         //Map contract's filiale & filiere                 
        for(Contract__c c : this.Contracts){
            System.debug('c.Name :'+c.Name+ 'c.Filiale__c :'+c.Filiale__c+ 'c.Filiere__c : '+c.Filiere__c+ ' c.AssociatedSalesRep__c :'+c.AssociatedSalesRep__c);          
            FilialeList.add(c.Filiale__c);
            FiliereList.add(c.Filiere__c);

            List<String> tempList = new List<String>();
            tempList.add(c.Filiale__c);
            tempList.add(c.Filiere__c);
            MapFilialeFiliereByContractId.put(c.Id, tempList);
        }

         List<Approbation_user__mdt> lstFilialeFiliereMdt  = [SELECT DeveloperName,Filiale__c,Filiere__c FROM Approbation_user__mdt WHERE Filiale__c IN:FilialeList and Filiere__c IN:FiliereList];
          for(Approbation_user__mdt mdt : lstFilialeFiliereMdt){ 
              MapDeveloperNameByFilialeFiliere.put(mdt.Filiale__c+mdt.Filiere__c, mdt.DeveloperName);
          }

        Integer j=0;
        for(Integer i=0;i<this.Contracts.size();i++){
            List<FilialeWrapper> Filiales=new List<FilialeWrapper>();
            String filiale='';
            do{
                if(this.contracts[j].Filiale__c!=null && this.contracts[j].Filiere__c!=null && !filiale.contains(this.contracts[j].Filiale__c)){
                    if(filiale==''){
                        filiale = this.contracts[j].Filiale__c;
                    }else{
                        filiale = filiale+' , '+ this.contracts[j].Filiale__c;
                        System.debug('there is 2 of us here'+ filiale);
                    }
                    FilialeWrapper fw=new FilialeWrapper();
                    fw.Filiale=this.contracts[j].Filiale__c;
                    List<String> lstContractFilialeFilere = MapFilialeFiliereByContractId.get(contracts[j].Id);
                    System.debug('contract.lstContractFilialeFilere '+contracts[j].Name+' '+lstContractFilialeFilere);
                    String contractFiliale = lstContractFilialeFilere[0];
                    String contractFilere = lstContractFilialeFilere[1];                   
                    String developerName = MapDeveloperNameByFilialeFiliere.get(contractFiliale+contractFilere);
                    System.debug('Contract developerName '+contracts[j].Name+' '+developerName);
                   
                    fw.FilialeLink='/apex/VFP_SalesDetails?userid='+this.contracts[j].AssociatedSalesRep__c+'&name='+developerName+'&FilialeParam='+this.contracts[j].Filiale__c.replace(target, replacement);
                    Filiales.add(fw);
                }
                j++;
            }while(j<this.Contracts.size() && this.contracts[i].AssociatedSalesRep__c==this.contracts[j].AssociatedSalesRep__c);
            i=j-1;
             if(this.contracts[i].Filiale__c!=null && this.contracts[i].Filiere__c!=null){
             List<String> listeContractFilialeFilere = MapFilialeFiliereByContractId.get(contracts[i].Id);
             System.debug('contract[i].listeContractFilialeFilere '+contracts[i].Name+' '+listeContractFilialeFilere);
             String contFiliale = listeContractFilialeFilere[0];
             String contFilere = listeContractFilialeFilere[1];                   
             String contDeveloperName = MapDeveloperNameByFilialeFiliere.get(contFiliale+contFilere);

            ContractWrapper CW= new ContractWrapper();
            CW.salesRepLink='/apex/VFP_SalesDetails?userid='+this.contracts[i].AssociatedSalesRep__c+'&name='+contDeveloperName+'&FilialeParam='+this.contracts[i].Filiale__c.replace(target, replacement)+'&FiliereParam='+this.contracts[i].Filiere__c.replace(target, replacement);
            CW.salesRepName=this.contracts[i].AssociatedSalesRep__r.Name;
            CW.FilialeLink='/apex/VFP_SalesDetails?userid='+this.contracts[i].AssociatedSalesRep__c+'&name='+contDeveloperName+'&FilialeParam='+this.contracts[i].Filiale__c.replace(target, replacement);
            CW.Filiale=this.contracts[i].Filiale__c;
            CW.Filiales=Filiales;
            this.ContractSalesreps.add(CW);
             }
        }
    }
    // Wrapper to show the table of all salesreps and their filiale
    public class ContractWrapper{
        public String salesRepLink{get;set;}
        public String salesRepName{get;set;}
        public String FilialeLink{get;set;}
        public String Filiale{get;set;}
        public List<FilialeWrapper> Filiales{get;set;}
    }
    public class FilialeWrapper{
        public String Filiale{get;set;}
        public String FilialeLink{get;set;}
    }
}