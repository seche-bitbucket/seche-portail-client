public class ApprovalRecapEmails {
    
    public static void SendRecapEmail (List<ProcessInstanceWorkitem> prInWorkItems)
    {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
        String[] toAddresses;
        List<Quote> quoteList = New List <Quote>();
        List<Id> quoteIds = new list<Id>();
        String emailBody = 'Bonjour '+prInWorkItems[0].actor.name +',\n\n';
        string domain = URL.getSalesforceBaseUrl().toExternalForm();
     	string TargetLink ='';
        
        for (ProcessInstanceWorkitem piwi : prInWorkItems)
        {	
            toAddresses = new String[] {piwi.Actor.Email}; 
            quoteIds.Add(piwi.ProcessInstance.TargetObjectid);     
        }
        
        quoteList = [select id, account.Name ,QuoteNumber__c, GrandTotal,Opportunity.TonnageN__c,Opportunity.Filiere__c,Opportunity.Salesman__r.Name from Quote where id IN:quoteIds];
        
        For (quote q : quoteList)
        {
            TargetLink = domain + '/' + string.valueof(q.id);
            emailBody += 'Un devis associé au commercial '+q.Opportunity.Salesman__r.Name+', d\'un montant de '+q.GrandTotal +' €'+', de filière '+q.Opportunity.Filiere__c+', et de tonnage = '+q.Opportunity.TonnageN__c+'.\n';
            emailBody += 'Pour le compte : '+q.account.name+' est soumis à votre approbation.\n';
            emailBody += 'Veuillez cliquer sur le lien pour voir le devis : '+TargetLink+'\n\n';
        }
        
        system.debug(emailBody);
        email.setToAddresses(toAddresses);
        email.setPlainTextBody(emailBody);
        Messaging.SingleEmailMessage[] EmailsToSend = new Messaging.SingleEmailMessage[] { email };
    	Messaging.sendEmail(EmailsToSend);
    }
    
}