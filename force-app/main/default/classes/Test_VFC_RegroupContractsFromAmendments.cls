/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-07-17
* @modified       2018-09-29
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for Regroup Contracts Controller 
* 2018-09-29    Added more tests to increase test coverage
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class Test_VFC_RegroupContractsFromAmendments {
    public static PageReference myVfPage {get;set;}
    public static ApexPages.StandardSetController stc {get;set;}
    public static VFC_RegroupContractsFromAmendments vfc{get;set;}  
    @isTest static void TestVFC_RegroupContractsFromAmendments(){
        PageReference ref = new PageReference('/001/o');
        Test.setCurrentPage(ref);
        ApexPages.currentPage().getParameters().put('retURL', '/001/o');
        Profile p = [select id from profile where name = :'Directeur commercial' limit 1];
        
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        director.UserRoleId=[select Id from UserRole where Name='Direction Commerciale - Nicolas Rogeau'].Id;
        
        insert director;
        System.RunAs(director) {
            Account ac = new Account();
            ac.Name = 'testAccName';
            ac.CustomerNature__c = 'Administration';
            ac.Industry = 'Trader';
            ac.CurrencyIsoCode = 'EUR';
            ac.Producteur__c = false;
            ac.BillingPostalCode = '00000';
            ac.NAF_Number__c = '1234A';
            insert ac;
            Account ac2 = new Account();
            ac2.Name = 'TestAcc2';
            ac2.CustomerNature__c = 'Administration';
            ac2.Industry = 'Trader';
            ac2.CurrencyIsoCode = 'EUR';
            ac2.Producteur__c = false;
            ac2.BillingPostalCode = '69006';
            ac2.NAF_Number__c = '1234A';
            insert ac2;
            List<Contract__c> contractsToInsert=new List<Contract__c>();
            for(integer i=0;i<10;i++){
                Contract__c c = new Contract__c();
                c.Account__c=ac.id;
                c.Filiale__c='Séché Eco Industries Changé';
                c.RegroupmentIndex__c=null;
                c.Status__c='Brouillon';
                c.name='Contract Test'+i;
                contractsToInsert.add(c);
            }
            if(contractsToInsert.size()>0){
                insert contractsToInsert;
            }
            List<Amendment__c> Amendments = new List<Amendment__c>();
            for(Contract__c contract:contractsToInsert){
                Amendment__c am = new Amendment__c();
                am.Contrat2__c=contract.id;
                am.WasteLabel__c='conditionnement amiante';
                am.PriceNextYear__c=1;
                am.ByPassContract__c=false;
                Amendments.add(am);
            }        
            insert Amendments;
            test.startTest();             
            List<Amendment__c> EmptyList=new List<Amendment__c>(); 
            stc=new Apexpages.standardSetController(EmptyList);
            stc.setSelected(EmptyList);
            vfc = new VFC_RegroupContractsFromAmendments(stc); 
            System.assertEquals('Veuillez selectionner au moins deux avenants', ApexPages.getMessages()[0].getSummary());
            System.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages()[0].getSeverity());
            test.stopTest();

            stc=new Apexpages.standardSetController(Amendments);
            stc.setSelected(Amendments);
            vfc = new VFC_RegroupContractsFromAmendments(stc); 
            vfc.regroupContracts();
            List<Contract__c> UpdatedList=[SELECT Id, Name, Account__c, Opportunity__c, RegroupmentIndex__c FROM Contract__c];
            for(Contract__c c:UpdatedList){
                System.debug(c.RegroupmentIndex__c);
                System.assertEquals(1, c.RegroupmentIndex__c);
            }
            List<Contract__c> contractsToInsert2=new List<Contract__c>();
            for(integer j=0;j<5;j++){
                Contract__c c = new Contract__c();
                c.Account__c=ac2.id;
                c.Filiale__c='Sodicome';
                c.RegroupmentIndex__c=1;
                c.name='Contract Test'+j;
                contractsToInsert2.add(c);
            }
            List<Contract__c> contractsToInsert3=new List<Contract__c>();
            for(integer j=0;j<5;j++){
                Contract__c c = new Contract__c();
                c.Account__c=ac.id;
                c.Filiale__c='Sodicome';
                c.RegroupmentIndex__c=null;
                c.name='Contract Test'+j;
                contractsToInsert3.add(c);
            }
            for(integer j=0;j<5;j++){
                Contract__c c = new Contract__c();
                c.Account__c=ac.id;
                c.Filiale__c='Sodicome';
                c.RegroupmentIndex__c=null;
                c.name='Contract Test'+j;
                contractsToInsert3.add(c);
            }  
            if(contractsToInsert2.size()>0){
                insert contractsToInsert2;
            }
            if(contractsToInsert3.size()>0){
                insert contractsToInsert3;
            }
            
            List<Amendment__c> AmendmentToInsert=new List<Amendment__c>(); 
            Integer i=1;
            for(Contract__c contract:contractsToInsert2){
                Amendment__c am = new Amendment__c();
                am.Contrat2__c=contract.id;
                am.WasteLabel__c='conditionnement amiante';
                am.PriceNextYear__c=1*i;
                am.ByPassContract__c=false;
                Amendment__c am2 = new Amendment__c();
                am2.Contrat2__c=contract.id;
                am2.WasteLabel__c='Transport amiante';
                am2.PriceNextYear__c=1*i;
                am2.ByPassContract__c=false;
                Amendment__c am3 = new Amendment__c();
                am3.Contrat2__c=contract.id;
                am3.WasteLabel__c='traitement amiante';
                am3.PriceNextYear__c=1*i;
                am3.ByPassContract__c=false;
                AmendmentToInsert.add(am);
                AmendmentToInsert.add(am2);
                AmendmentToInsert.add(am3);
                i++;
            }   
            i=1;
            List<Amendment__c> AmendmentToInsert2=new List<Amendment__c>(); 
            for(Contract__c contract:contractsToInsert3){
                Amendment__c am = new Amendment__c();
                am.Contrat2__c=contract.id;
                am.WasteLabel__c='conditionnement amiante';
                am.PriceNextYear__c=1*i;
                am.ByPassContract__c=false;
                Amendment__c am2 = new Amendment__c();
                am2.Contrat2__c=contract.id;
                am2.WasteLabel__c='Transport amiante';
                am2.PriceNextYear__c=1*i;
                am2.ByPassContract__c=false;
                Amendment__c am3 = new Amendment__c();
                am3.Contrat2__c=contract.id;
                am3.WasteLabel__c='traitement amiante';
                am3.PriceNextYear__c=1*i;
                am3.ByPassContract__c=false;
                AmendmentToInsert2.add(am);
                AmendmentToInsert2.add(am2);
                AmendmentToInsert2.add(am3);
                i++;
            } 
            insert AmendmentToInsert;
            insert AmendmentToInsert2;
            stc=new Apexpages.standardSetController(AmendmentToInsert2);
            stc.setSelected(AmendmentToInsert2);
            vfc = new VFC_RegroupContractsFromAmendments(stc);
            vfc.regroupContracts();
            vfc.regroupContractsIfSameAccount();
            List<Contract__c> UpdatedList2=[SELECT Id, Name, Account__c, Opportunity__c, RegroupmentIndex__c FROM Contract__c WHERE ID IN:contractsToInsert3];
            for(Contract__c c:UpdatedList2){
                System.assertEquals(2, c.RegroupmentIndex__c);
            }
            List<Amendment__c> AmendmentsFromDiffAcc=new List<Amendment__c>();
            AmendmentsFromDiffAcc.add(AmendmentToInsert[0]);
            AmendmentsFromDiffAcc.add(AmendmentToInsert2[0]);
            stc=new Apexpages.standardSetController(AmendmentsFromDiffAcc);
            stc.setSelected(AmendmentsFromDiffAcc);
            vfc = new VFC_RegroupContractsFromAmendments(stc);
            vfc.regroupContracts();
            for(Contract__c contract:contractsToInsert3){
                contract.RecordLocked__c=true;
            }
            Update contractsToInsert3;
            stc=new Apexpages.standardSetController(AmendmentToInsert2);
            stc.setSelected(AmendmentToInsert2);
            vfc = new VFC_RegroupContractsFromAmendments(stc);
            vfc.regroupContracts();

        }
    }
}