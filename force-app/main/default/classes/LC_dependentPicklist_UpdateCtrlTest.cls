@isTest
public class LC_dependentPicklist_UpdateCtrlTest {
        @isTest
    static void testGetDependentMap(){
        
        Map<String, List<String>> dependentMap = LC_dependentPicklist_UpdateCtrl.getDependentMap(new Contract__c(), 'Filiale__c','Type_de_convention__c');
        List<String> filiale = dependentMap.get(String_Helper.CON_Filiale_Drimm);
        System.assertEquals(filiale.contains(String_Helper.CON_TypeDeConvention_DndTraitement), true);
        
    }

}