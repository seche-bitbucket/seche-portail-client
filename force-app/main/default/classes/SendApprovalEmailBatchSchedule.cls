global class SendApprovalEmailBatchSchedule implements Schedulable {
    
    global void execute(SchedulableContext sc) {
        
        SendApprovalEmailBatch batch = new SendApprovalEmailBatch();
        database.executebatch(batch);
        
        
    }
    
}