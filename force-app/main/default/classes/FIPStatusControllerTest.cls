@isTest(SeeAllData=true)
private class FIPStatusControllerTest {
    
    static testmethod void testFIPApprouved() {
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.BillingPostalCode = '21000';
        acc.BillingStreet = '1 rue du test';
        insert acc;
        
        Contact ctc = new Contact();
        ctc.lastName = 'Test';
        ctc.AccountId = acc.ID;
        insert ctc;
        
        
        FIP_FIP__c fip = new FIP_FIP__c();
        fip.Collector__c = acc.ID;
        fip.Contact__c = ctc.ID;
        insert fip;
        Test.StartTest();
        
        FIPStatusController.setAccepted(fip.ID);
        
        fip = [Select ID, State__c, AcceptedBy__c FROM FIP_FIP__c WHERE ID = : fip.ID];
        
        Test.StopTest();
        system.assertEquals('Approuvée',fip.State__c);
        system.assertEquals(UserInfo.getUserId(),fip.AcceptedBy__c);
        
    }
    
    static testmethod void testFIPRefused() {
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.BillingPostalCode = '21000';
        acc.BillingStreet = '1 rue du test';
        insert acc;
        
        Contact ctc = new Contact();
        ctc.lastName = 'Test';
        ctc.AccountId = acc.ID;
        insert ctc;
        
        
        FIP_FIP__c fip = new FIP_FIP__c();
        fip.Collector__c = acc.ID;
        fip.Contact__c = ctc.ID;
        insert fip;
        Test.StartTest();
        
        FIPStatusController.setRefused(fip.ID);
        
        fip = [Select ID, State__c FROM FIP_FIP__c WHERE ID = : fip.ID];
        
        Test.StopTest();
        system.assertEquals('Refusée',fip.State__c);
        
    }
    
    static testmethod void testFIPListView() {
        
       
        Test.StartTest();
        
     //    ListView listview = FIPStatusController.getListView();
          
        
        Test.StopTest();
        //system.assertEquals('FIPToApprouve',listview.Name);
        
    }
    
}