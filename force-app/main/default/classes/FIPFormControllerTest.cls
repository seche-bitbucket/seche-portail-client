@isTest
private class FIPFormControllerTest {
    
    @isTest static void test(){
        System.debug('FIPFormControllerTest START' );
        
      //String URL = 'https://preprod-seche.cs85.force.com/amiante/s/fip?UID=bbcdab6984cc47e68e&ID=0016E00000Uz9o1QAB&CID=0036E00000NgA8jQAF';
        String URL = 'https://seche.my.salesforce.com/amiante/s/fip?UID=5bd63bfafb2c4a63bc&ID=a141i0000008VMjAAM&CID=0035800001Ix6dn';
//  String URL = 'https://preprod-seche.cs85.force.com/amiante/s/fip?UID=bbcdab6984cc47e68e&ID=0016E00000Uz9o1QAB&CID=0036E00000NgA8jQAF';
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new mockupHttpResponseFIPGenerator());
        
        FIPFormController lfc = FIPFormController.find_FIPById(URL);
        
        System.debug('FIPFormControllerTest ' +lfc);
        String JSONString = JSON.serialize(lfc);
        System.debug(JSONString);
        String NONUCG ='2590 - III - 9';
        
        if(JSONString != null){
         String result = FIPFormController.saveContents(JSONString,NONUCG );
        }
        
        Test.StopTest();

        System.debug('FIPFormControllerTest END' );
        
    }
    
}