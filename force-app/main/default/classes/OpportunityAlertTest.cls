@isTest
private class OpportunityAlertTest {
    
    static testMethod void testBatch()
    {
        User u = [Select Id From User Limit 1];
        //RecordType rt = [Select Id From RecordType Where IsActive = true AND SobjectType = 'Account' Limit 1];
        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        //a.RecordTypeId = rt.Id;
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Salesman__c = u.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '03 00 00 00 00';
        insert c;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'test';
        opp.AccountId = a.Id;
        //opp.Salesman__c = u.Id;
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.ContactName__c = c.Id;
        opp.TonnageN__c = 0;
        insert opp;
        
        Test.startTest();
        OpportunityAlertTaskBatch b = new OpportunityAlertTaskBatch('Select Id, OwnerId From Opportunity Where CloseDate > TODAY'); 
        Database.executebatch(b);
        Test.stopTest();

    }
}