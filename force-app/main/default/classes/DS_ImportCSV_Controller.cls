public class DS_ImportCSV_Controller {
    
    public String documentName {get;set;}
    public Blob csvFileBody{get;set;}
    public String[] contactDataLines {get;set;}
    public List <WST_Waste_Statement__c> lstWST {get;set;}
    public boolean readSuccess {get;set;}
    public List<String> lstFieldNames{get;set;}
    Map <String, FieldWrapper> fieldNamesMap = new Map<String, FieldWrapper>();
    WST_Waste_Statement__c wstObj;
    String contactDataAsString;
    String fieldValue;
    Integer fieldNumber;
    public ID parentID {get;set;}
    public ID wstRecordType {get;set;}
    Map < String, Integer > fieldNumberMap = new Map < String, Integer > ();
	public string callfunc{get;set;}	
	
    
    public DS_ImportCSV_Controller(ApexPages.StandardController controller) {
        parentID = controller.getId();
        wstRecordType = defineRecordTypeID(parentID);
        documentName = '';
        readSuccess = FALSE;
        contactDataLines = new String[] {};
            lstWST = new List <WST_Waste_Statement__c> ();
        lstFieldNames = new List<String>();
    }
    
    
    public void readFromFile(){
        
        //contactDataAsString = csvFileBody.toString();
        contactDataAsString = blobToString( csvFileBody,'ISO-8859-1');
        generateMapDescribe();
        readCSVFile();
        if(readSuccess){
            saveData();
        }
        /*PageReference pr = new PageReference('/'+stdController.getId());
        pr.setRedirect(true);
        return pr;*/
        
        
        callfunc='<script> func(); </script>';
        
    }
    
    public void readCSVFile() {
        lstWST.clear();
        contactDataLines = contactDataAsString.split('\r\n');
        string[] csvFieldNames = contactDataLines[0].split(';');
        for (Integer i = 0; i < csvFieldNames.size(); i++) {
            fieldNumberMap.put(csvFieldNames[i], i);
            lstFieldNames.add(csvFieldNames[i].trim());
        }
        
        for (Integer i = 1; i < contactDataLines.size(); i++) {
            wstObj = new WST_Waste_Statement__c();
            string[] csvRecordData = contactDataLines[i].split(';');
            for (String fieldName: csvFieldNames) {
                fieldNumber = fieldNumberMap.get(fieldName);
                try{
                    fieldValue = csvRecordData[fieldNumber];
                    FieldWrapper fw = fieldNamesMap.get(fieldName.trim());
                    System.debug('fw.fieldName : '+fw.fieldName);
                    System.debug('fw.fieldType : '+fw.fieldType);
                    if(fw.fieldAPIName == 'TransfoSerialNumber__c'){
                        if(fieldValue.trim() != ''){
                            wstObj.put(fw.fieldAPIName,  fieldValue.trim());
                            wstObj.put('Name',  fieldValue.trim());
                        }
                        //BOOLEAN, DATE, DATETIME, DOUBLE, ID, MULTIPICKLIST, PICKLIST, REFERENCE, STRING, TEXTAREA
                    }else if(fw.fieldType == 'DOUBLE' ){
                        wstObj.put(fw.fieldAPIName,  Decimal.valueOf(fieldValue.trim()));
                    }else{
                        wstObj.put(fw.fieldAPIName, fieldValue.trim());
                    }
                }catch(Exception e){
                    System.debug('Error : '+e.getMessage());
                }
                
                
            }
            wstObj.put(	'ServiceDemande__c', ParentId);
            wstObj.put(	'RecordtypeID', wstRecordType);
            lstWST.add(wstObj);                
        }
        if(lstWST.size() > 0){
            readSuccess = TRUE;
            lstWST  = manageWasteInElimination(lstWST);
        }            
    }
    
    public void saveData() {
        
        try {
            INSERT lstWST;
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.INFO, 'Transformateur(s) importé(s) avec succès');
            ApexPages.addMessage(errorMessage);
            
        } catch (Exception e) {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR, 'Une erreur est survenue');
            ApexPages.addMessage(errorMessage);
        }
        
    }
    
    private static ID defineRecordTypeID(Id parentId){
		
        Id rtParentId = [Select RecordTypeID FROM SD_ServiceDemand__c WHERE ID = : parentId].RecordTypeID;
        Id result; 
        
        if(rtParentId == Label.RCT_RT_DemandeService_EliminationSF6){
            result = Label.RCT_RT_WST_EliminationSF6;
        }else if(rtParentId == Label.RCT_RT_DemandeService_Remediation){
            result = Label.RCT_RT_WST_Remediation;
        }else if(rtParentId == Label.RCT_RT_DemandeService_Repairs){
            result = Label.RCT_RT_WST_Repairs;
        }else{
            result = Label.RCT_RT_WST_Elimination;
        }
            
            return result;
        
    }    
    
    public static String blobToString(Blob input, String inCharset){
        String hex = EncodingUtil.convertToHex(input);
        System.assertEquals(0, hex.length() & 1);
        final Integer bytesCount = hex.length() >> 1;
        String[] bytes = new String[bytesCount];
        for(Integer i = 0; i < bytesCount; ++i)
            bytes[i] =  hex.mid(i << 1, 2);
        return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
    }    
    
    public void generateMapDescribe() {
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get('WST_Waste_Statement__c').getDescribe().fields.getMap();
        
        
        for(Schema.SObjectField sfield : fieldMap.Values())
        {
            schema.describefieldresult dfield = sfield.getDescribe();
            FieldWrapper fw = new FieldWrapper();
            fw.fieldName = dfield.getLabel();
            fw.fieldAPIName = dfield.getname();
            fw.fieldType = dfield.getType()+'';
            fieldNamesMap.put(dfield.getLabel(), fw);
            
        }
    }
    
    public List<WST_Waste_Statement__c> manageWasteInElimination(List<WST_Waste_Statement__c> lstWSTToManage) {
        
        List<WST_Waste_Statement__c> lstResult = new List<WST_Waste_Statement__c>();
        
        for(WST_Waste_Statement__c wst : lstWSTToManage){
            if(wst.Name == null && wst.Type__c != ''){
                wst.Name = wst.Type__c;
            }else if(wst.Name == null && wst.Brand__c != ''){
                 wst.Name = wst.Brand__c;
            }
            lstResult.add(wst);
        }
        
        return lstResult;
    }
    
    public class FieldWrapper
    {
        public String fieldName {get; set;}
        public String fieldAPIName {get; set;}
        public String fieldType {get; set;}
    }
}