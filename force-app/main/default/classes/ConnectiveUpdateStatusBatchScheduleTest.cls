/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ConnectiveUpdateStatusBatchScheduleTest
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ConnectiveUpdateStatusBatchScheduleTest
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 27-05-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
@isTest
public class ConnectiveUpdateStatusBatchScheduleTest {

    public static testMethod void testschedule() {
        ConnectiveUpdateStatusBatchSchedule sh1 = new ConnectiveUpdateStatusBatchSchedule();
        Test.StartTest();
        String sch = '0 0 23 * * ?'; 
        String jobId = system.schedule('Connective UpdateStatusBatch', sch, sh1); 
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                              FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(sch,ct.CronExpression);

        Test.stopTest();   
    }
}