/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Lucas DORMOY <lucas.dormoy@capgemini.com>
 * @version        1.0
 * @created        2020-12-11
 * @systemLayer    Manager
 *
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Description: This class contains the trigger handler for Questionnaire Lycée
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

public with sharing class QLYTriggerHandler {
	
    public static void onAfterUpdate(Map<Id, QLY_SchoolForm__c> oldMap, Map<Id, QLY_SchoolForm__c> newMap){
        sendEmailWithFormDocAttached(oldMap, newMap);
    }
    
    
    public static void onBeforeUpdate(Map<Id, QLY_SchoolForm__c> oldMap, Map<Id, QLY_SchoolForm__c> newMap){
        setQLYFormStatusToSentWhenDocumentIsGenerated(oldMap, newMap);
    }
    
        /*
* ------------------------------------------------------------
* -- - Purpose : Sends an email to QLY's contact with Latest File attached when QLY document is generated
* -- - Parameters: Map<Id, QLY_SchoolForm__c> oldMap, Map<Id, QLY_SchoolForm__c> newMap
* -- - Return: Nothing
* ------------------------------------------------------------
*/
    public static void sendEmailWithFormDocAttached(Map<Id, QLY_SchoolForm__c> oldMap, Map<Id, QLY_SchoolForm__c> newMap){
        List<QLY_SchoolForm__c> qlyToSendToContact = new List<QLY_SchoolForm__c>();
        for(QLY_SchoolForm__c qly : newMap.values()){
            if(qly.DocumentStatus__c == 'Généré' && oldMap.get(qly.Id).DocumentStatus__c != qly.DocumentStatus__c){
                qlyToSendToContact.add(qly);                
            }
        }
        QLY_Utils.SendLatestContentDocumentToContactByEmail(qlyToSendToContact);
    }
    
    
        /*
* ------------------------------------------------------------
* -- - Purpose : Sets FormStatus__c to "Envoyé" when Document is generated
* -- - Parameters: Map<Id, QLY_SchoolForm__c> oldMap, Map<Id, QLY_SchoolForm__c> newMap
* -- - Return: Nothing
* ------------------------------------------------------------
*/
    public static void setQLYFormStatusToSentWhenDocumentIsGenerated(Map<Id, QLY_SchoolForm__c> oldMap, Map<Id, QLY_SchoolForm__c> newMap){
        List<QLY_SchoolForm__c> qlyToUpdate = new List<QLY_SchoolForm__c>();
        for(QLY_SchoolForm__c qly : newMap.values()){
            if(qly.DocumentStatus__c == 'Généré' && oldMap.get(qly.Id).DocumentStatus__c != qly.DocumentStatus__c && qly.FormStatus__c != 'Envoyé'){
                qly.FormStatus__c = 'Envoyé';
                qlyToUpdate.add(qly);                
            }
        }
    }
}