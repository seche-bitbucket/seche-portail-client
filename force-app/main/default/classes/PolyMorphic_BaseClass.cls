/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Mohammed Amine IDRISSI TAGHKI <mohammed-amine.idrissi-taghki@capgemini.com>
 * @version        1.0
 * @created        2021-09-14
 * @systemLayer    Base Model
 *
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Desciption: Polymorphic base class used for a generic cutom lwc 
 * @Documentation: teams ppt
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

public without sharing abstract class PolyMorphic_BaseClass {

    protected PolyMorphic_BaseClass() { }

    // Initiate base model for an instance
    public static PolyMorphic_BaseClass getInstance(String rType){

        String classSuffix = (Schema.getGlobalDescribe().get(rType).getDescribe().isCustom()) ? 
                                    rType.substring(0,rType.length()-3) : rType;
        Type customType = Type.forName('CB_'+classSuffix);
        PolyMorphic_BaseClass baseClass = (PolyMorphic_BaseClass)customType.newInstance();
        return baseClass;

    }

    /* Abstract methods */
    // these classes mnust be overriden by your instanciate class

    /* Virtual methods */
    // these methods can be overriden if there is a difference fuinctionnality
    public virtual Object VSave(String metadata, String dataType){
        try {

            List<SObject> rq = (List<SObject>)JSON.deserialize(metaData, 
                                                               Type.forName('List<'+dataType+'>'));
                                                               System.debug(rq);
            upsert rq;
            return rq;
            
        } catch (Exception e) {

            Map<String, Object> errorMap = new Map<String, Object>();
            errorMap.put('message', e.getMessage());
            errorMap.put('line', e.getLineNumber());
            errorMap.put('source', 'CustomBanner_Controller.VGetRecord');
            errorMap.put('inputs', new Map<String, String>{'metadata' => metadata,
                                                            'dataType' => dataType});
                                                            System.debug(errorMap);
            return errorMap;

        }
    }

    public virtual Object VInit(Id recordId){ return null; }

    public virtual Object VCustomAction(String recordJSON, String action){ return null; }

    public virtual Object VInsert(String data, String apiName){ return null; }
    
    public virtual Object VGetRecord(String recordId){
        try {

            String objType = Id.valueOf(recordId).getSobjectType().getDescribe().getName();
            List<String> f = new List<String>(Id.valueOf(recordId).getSobjectType().getDescribe().fields.getMap().keySet());

            // for(String field :recordId.getSobjectType().getDescribe().fields.getMap().keySet()) {
            //     if(field.toLowerCase() == 'name' && field.toLowerCase().contains('name')) {
            //         f = field;
            //         break;
            //     }
            // }

            List<SObject> rc = Database.query('SELECT '+String.join(f, ', ')+' FROM '+objType+' WHERE ID = :recordId');
            
            return rc;

        } catch (Exception e) {

            Map<String, Object> errorMap = new Map<String, Object>();
            errorMap.put('message', e.getMessage());
            errorMap.put('line', e.getLineNumber());
            errorMap.put('source', 'CustomBanner_Controller.VGetRecord');
            errorMap.put('inputs', new Map<String, String>{'recordId' => recordId});
            return errorMap;

        }
    }

    /* Static method */
    // static methods
    public Object SGetPickListValuesIntoList(Object obj, String field){

        try{

            Schema.DescribeFieldResult fieldResult;
            if(obj instanceOf Id) {
                Id id = (Id)obj;
                fieldResult = id.getSobjectType().getDescribe().fields.getMap().get(field).getDescribe();
            } else if(obj instanceOf String) {
                fieldResult = Schema.getGlobalDescribe().get((String)obj).getDescribe().fields.getMap().get(field).getDescribe();
            }

            List<Map<String,String>> pickListValuesList= new List<Map<String,String>>();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                pickListValuesList.add(new Map<String,String>{'value' => pickListVal.getValue(), 'label' => pickListVal.getLabel()});
            }     
            return pickListValuesList;

        } catch(Exception e) {

            Map<String, Object> errorMap = new Map<String, Object>();
            errorMap.put('message', e.getMessage());
            errorMap.put('line', e.getLineNumber());
            errorMap.put('source', 'CustomBanner_Controller.SGetPickListValuesIntoList');
            errorMap.put('inputs', new Map<String, String>{'obj' => JSON.serialize(obj),
                                                            'field' => field});
            System.debug(errorMap);
            return errorMap;
        }

    }

    public Object SGetFieldsLabelAndValue(Id recordId, String fields){
        try {
            System.debug('getFieldsLabelAndValue');

            List<String> fList = fields.split(',');

            Sobject record = Database.query('SELECT '+fields+' FROM '+recordId.getSobjectType().getDescribe().getName()+' WHERE ID = :recordId');

            List<Map<String, Object>> returnLst = new List<Map<String, Object>>();
            Map<String, Schema.SObjectField> mapFields = recordId.getSObjectType().getDescribe().fields.getMap();

            for(String f: fList) {
                if(mapFields.containsKey(f.toLowerCase())) {
                    List<Map<String, String>> lstValues = new List<Map<String, String>>();

                    if(mapFields.get(f.toLowerCase()).getDescribe().getPicklistValues() != null &&  mapFields.get(f.toLowerCase()).getDescribe().getPicklistValues().size() > 0) {
                        for(Schema.PicklistEntry ple:  mapFields.get(f.toLowerCase()).getDescribe().getPicklistValues()) {
                            lstValues.add(new Map<String, String>{'label' => ple.getLabel(), 'value' => ple.getValue()});
                        }
                    }

                    returnLst.add(new Map<String, Object>{'name' => f,
                                                          'label' => String.valueOf(mapFields.get(f.toLowerCase()).getDescribe().getLabel()),
                                                          'value' => record.get(f),
                                                          'type' => String.valueOf(mapFields.get(f.toLowerCase()).getDescribe().getType()),
                                                          'options' => lstValues});
                }
            }

            System.debug(returnLst);

            return returnLst;

        } catch (Exception e) {
            
            Map<String, Object> errorMap = new Map<String, Object>();
            errorMap.put('message', e.getMessage());
            errorMap.put('line', e.getLineNumber());
            errorMap.put('source', 'CustomBanner_Controller.SGetFieldsLabelAndValue');
            errorMap.put('inputs', new Map<String, String>{'recordId' => recordId,
                                                            'fields' => fields});
            return errorMap;

        }
    }

}