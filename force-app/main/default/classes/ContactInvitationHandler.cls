public without sharing class ContactInvitationHandler {

    // Warning : this class is witout sharing
    
    public static void CheckDuplicate( List<Contacts_Invitation__c> contactInvInsertList) {

        //List<Contacts_Invitation__c> contactInvToDelete = new List<Contacts_Invitation__c>();
        List<Contacts_Invitation__c> contactInvToUpdate = new List<Contacts_Invitation__c>();

        Map<String,Contacts_Invitation__c> contactInvMapID = new Map<String,Contacts_Invitation__c>();

        for (Contacts_Invitation__c contactInv : contactInvInsertList) {
            if (contactInv.GUID__c != null) {
                contactInvMapID.put(contactInv.GUID__c, contactInv);
            }
        }

        List<Contacts_Invitation__c> contactInvList = [SELECT Id, GUID__c FROM Contacts_Invitation__c where Id IN :contactInvMapID.keySet()];

        if (contactInvList!= null) {
            for (Contacts_Invitation__c contactInvOld : contactInvList) {
                for (Contacts_Invitation__c contactInvNew : contactInvMapID.values()) {
                    if (contactInvOld.Id == contactInvNew.GUID__c) {
                        if (contactInvNew.InviteeEventValidation__c!= null) {
                            contactInvOld.InviteeEventValidation__c = contactInvNew.InviteeEventValidation__c;
                        }
                        if (contactInvNew.NumberOfCompanions__c!= null) {
                            contactInvOld.NumberOfCompanions__c = contactInvNew.NumberOfCompanions__c;
                        }
                        //contactInvNew.GUID__c = 'non';
                        //contactInvToDelete.add(contactInvNew);
                        contactInvToUpdate.add(contactInvOld);
                        //contactInvToUpdate.add(contactInvNew);
                    }
                }
            }
        }

        update contactInvToUpdate;
        //List<Database.DeleteResult> deleteResult = Database.delete(contactInvToDelete, true);
        //delete contactInvToDelete;
        

    }
}