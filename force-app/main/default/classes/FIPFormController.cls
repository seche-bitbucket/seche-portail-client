public with sharing class FIPFormController {
    
    @AuraEnabled
    public String ID {get; set;}
    
    @AuraEnabled
    public String ContactID {get; set;}
    
    @AuraEnabled
    public String UID {get; set;}
    
    @AuraEnabled
    public String LastName {get; set;}
    
    @AuraEnabled
    public String Collector {get; set;}
    
    @AuraEnabled
    public String TypeFIP {get; set;}
    
    @AuraEnabled
    public String Envoie {get; set;}
    
    @AuraEnabled
    public String NCAP {get; set;}
    
    @AuraEnabled
    public String Filiale {get; set;}
    
    @AuraEnabled
    public String NContrat {get; set;}
    
    @AuraEnabled
    public String CAP {get; set;}
    
    @AuraEnabled
    public String AcceptBy {get; set;}
    
    @AuraEnabled
    public Boolean PlateformeTransit {get; set;}
    
    @AuraEnabled
    public String Appelation {get; set;}
    
    @AuraEnabled
    public String CodeNomen {get; set;}
    
    @AuraEnabled
    public Decimal Quantite {get; set;}
    
    @AuraEnabled
    public String TypeAmiante {get; set;}
    
    @AuraEnabled
    public String TypeCondi {get; set;}
    
    @AuraEnabled
    public String NONU {get; set;}
    
    @AuraEnabled
    public String Classe {get; set;}
    
    @AuraEnabled
    public String GEmballage {get; set;}
    
    @AuraEnabled
    public String Rsociale {get; set;}
    
    @AuraEnabled
    public String Siret {get; set;}
    
    @AuraEnabled
    public String Adr1 {get; set;}
    
    @AuraEnabled
    public String Adr2 {get; set;}
    
    @AuraEnabled
    public String Adr3 {get; set;}
    
    @AuraEnabled
    public String CodePostal {get; set;}
    
    @AuraEnabled
    public String Ville {get; set;}
    
    @AuraEnabled
    public String AdrChantier {get; set;}
    
    @AuraEnabled
    public String RsocilialDemandeur {get; set;}
    
    @AuraEnabled
    public String SIRETDemandeur {get; set;}
    
    @AuraEnabled
    public String AdrDemandeur {get; set;}    
    
    @AuraEnabled
    public String CodePostalDemandeur {get; set;}   
    
    @AuraEnabled
    public String VilleDemandeur {get; set;}   
    
    @AuraEnabled
    public String CodeX3Fact {get; set;}  
    
    @AuraEnabled
    public String CodeAdrX3Fact {get; set;}  
    
    @AuraEnabled
    public String CodeX3Prod {get; set;}  
    
    @AuraEnabled
    public String CodeAdrX3Prod {get; set;}  
    
    @AuraEnabled
    public String CodeX3Dem {get; set;}  
    
    @AuraEnabled
    public String CodeAdrX3Dem {get; set;}  
    
    @AuraEnabled
    public String IdTVA {get; set;}  
    
     @AuraEnabled
     public static String getUserName() {
     return userinfo.getName();
  }
    
    
    
    
    @AuraEnabled
    public static List < String > getselectOptions(sObject objObject, string fld) {
        system.debug('getselectOptions START');
        List < String > allOpts = new list < String > ();
        Schema.sObjectType objType = objObject.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getLabel());
        }
        allOpts.sort();
        system.debug('getselectOptions END');
        return allOpts;
    }
    
    
    public static String getTokenAccess() {
        system.debug('FIPFormController.getTokenAccess = START');
        String endPointURL = Label.ORG_URL_LOGIN+'/services/oauth2/token';
        system.debug('endPointURL = '  + endPointURL);
        
        String tokenSF = '';
        Boolean found = false;
        String username = Label.USR_API_Portal;
        system.debug('username = '  + username);
        
        String password = Label.USR_API_Password;
        system.debug('password = '  + password);
        
        String clientID = Label.API_ClientID;
        system.debug('clientID = '  + clientID);
        
        String clientSecret = Label.API_Client_Secret;
        system.debug('clientSecret = '  + clientSecret);
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPointURL+'?grant_type=password&client_id='+clientID+'&client_secret='+clientSecret+'&username='+username+'&password='+password);
        req.setMethod('POST');
        HttpResponse res = h.send(req);
        system.debug('res2 = '  + res.getBody());
        
        
        if(res.getStatusCode() == 200){
            //Parse JSON to retrive Token
            System.JSONToken token;
            string text;
            JSONParser parser = JSON.createParser(res.getBody());
            System.debug('test restGetBody = '+res.getBody());
            parser.nextToken(); 
            while((token = parser.nextToken()) != null && !found) {
                
                text = parser.getText();
                if (token == JSONToken.FIELD_Name && text == 'access_token') {
                    token=parser.nextToken();
                    tokenSF = parser.getText();
                    found = true;
                }
            }
        }
        System.debug('test tokenSF = '+tokenSF);
        
        if(tokenSF != ''){
            system.debug('FIPFormController.getTokenAccess = END');
            return tokenSF;   
            
        }else{
            system.debug('FIPFormController.getTokenAccess = END2');
            return 'ERROR';
        }
        
    }
    
    
    
    public static Account getCalloutResponseContents(String ID, String UID, String access_token){
        system.debug('FIPFormController.getCalloutResponseContents = START');
        system.debug('getCalloutResponseContents ID = '+ID);
        system.debug('getCalloutResponseContents UID = '+UID);
        
        String endPointURL = Label.ORG_URL+'/services/data/v43.0/sobjects/Account/'+ID;
        system.debug('endPointURL = '+ endPointURL);
        Http h = new Http();
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPointURL);
        req.setMethod('GET');
        req.setHeader('Authorization','Bearer '+access_token);
        HttpResponse res = h.send(req);
        System.debug(res.getBody());
        return (Account) System.JSON.deserialize(res.getBody(), Account.class);
    }
    
    
    
    @AuraEnabled
    public static FIPFormController find_FIPById(String URL){
        system.debug('FIPFormController.find_FIPById = START');
        system.debug('FIPFormController.URL = '+ URL); 
        String UID ='';
        String ID ='';
        String CID ='';
        
        Account l = new Account();
        try{
            String[] parameters = URL.split('\\?');
            String[] parameter = parameters[1].split('&');
            if(parameter[0].contains('UID') && parameter[1].contains('ID') && parameter[2].contains('CID')){
                String[] temp = parameter[0].split('=');
                UID = temp[1];
                system.debug('UID1 = ' + UID);
                
                temp = parameter[1].split('=');
                ID = temp[1];
                system.debug('ID = ' + ID);
                
                temp = parameter[2].split('=');
                CID = temp[1];
                system.debug('CID = ' + CID);
                
                
                
            }

            
        }catch(Exception e){
            system.debug('find_FIPById catch = ');
            return new FIPFormController();
        }
        
        
        String token = getTokenAccess();
                     system.debug('token = '+ token);

        if(token != 'ERROR'){
            system.debug('token != error');
            l = getCalloutResponseContents(ID, UID, token);
        }else{
             system.debug('token = error');
            return new FIPFormController();
        }
        system.debug('l debug= ' +l);

        system.debug('l.TECH_UID__c= ' +l.TECH_UID__c);
        system.debug('UID = ' + UID);
        
        if(l.TECH_UID__c == UID){
            system.debug('l.TECH_UID__c == UID = true');
            FIPFormController cl =new FIPFormController();
           // Account vAccount = [SELECT OwnerID FROM Account WHERE ID=:ID];
            cl.ContactID = CID;
             system.debug('cl.ContactID =' +cl.ContactID);

            cl.Collector= ID;
            system.debug('cl.Collector =' +cl.Collector);

            
            system.debug('FIPFormController.find_FIPById = END');
            return cl;
        }else{
            system.debug('FIPFormController.find_FIPById = END2');
            return new FIPFormController();
        }
        
    }
    
    @AuraEnabled
    public static String saveContents(String lfcJson , String lfcJson2 ) {
        system.debug('FIPFormController.saveContents = START');
        
        lfcJson2=lfcJson2.replace('\"','');
        String[] NONUGC  = null;
        if(lfcJson2.contains('–')){
            NONUGC = lfcJson2.split('–');
            system.debug(NONUGC[0]);
            system.debug(NONUGC[1]);
            system.debug(NONUGC[2]);
        }

        String token_access = getTokenAccess();
        JSONParser parser = JSON.createParser(lfcJson);
       
        system.debug('test JSON3 = '+ lfcJson);
        
        FIPFormController lfc = (FIPFormController)parser.readValueAs(FIPFormController.class);
                system.debug('test lfc = '+ lfc);

        lfc = checkNullValue(lfc);
        JSONGenerator gen = JSON.createGenerator(true);
        
        system.debug('test gen = '+ gen);
        system.debug('test lfc.Collector = '+ lfc.Collector);
        system.debug('test lfc.ContactID = '+ lfc.ContactID);
        
        gen.writeStartObject();
        if(lfc.Collector != null)
        gen.writeStringField('Collector__c', lfc.Collector);

        gen.writeStringField('PreviousPACNumber__c', lfc.NCAP);
        gen.writeStringField('CAP__c', lfc.CAP);
        gen.writeStringField('WasteName__c', lfc.Appelation);
        gen.writeStringField('AnnualQuantity__c', String.valueOf(lfc.Quantite));
        gen.writeStringField('PackagingGroup__c', lfc.GEmballage);
        gen.writeStringField('ProductorBusinessName__c', lfc.Rsociale);
        gen.writeStringField('ProductorCompanyIdentificationSystem__c', lfc.Siret);
        gen.writeStringField('ProductorAdress1__c', lfc.Adr1);
        gen.writeStringField('ProductorAdress2__c', lfc.Adr2);
        gen.writeStringField('ProductorAdress3__c', lfc.Adr3);
        gen.writeStringField('ProductorPostalCode__c', lfc.CodePostal);
        gen.writeStringField('ProductorCity__c', lfc.Ville);
        gen.writeStringField('ConstructionSiteAdress__c', lfc.AdrChantier);
        gen.writeStringField('FIPType__c', lfc.TypeFIP);
        gen.writeStringField('WasteNamingCode__c', lfc.CodeNomen);
        gen.writeStringField('AsbestosType__c', lfc.TypeAmiante);
        gen.writeStringField('PackagingType__c', lfc.TypeCondi);
        if(NONUGC != null){
            gen.writeStringField('UNCode__c', NONUGC[0]);
            gen.writeStringField('PackagingGroup__c', NONUGC[1]);
            gen.writeStringField('Class__c', NONUGC[2]);
        }else {
            gen.writeStringField('UNCode__c', lfcJson2);
        }
        gen.writeStringField('ChargedBusinessName__c', lfc.RsocilialDemandeur);
        gen.writeStringField('ChargedCompanyIdentificationSystem__c', lfc.SIRETDemandeur);
        gen.writeStringField('ChargedAdress__c', lfc.AdrDemandeur);
        gen.writeStringField('ChargedPostalCode__c', lfc.CodePostalDemandeur);
        gen.writeStringField('ChargedCity__c', lfc.VilleDemandeur);
        gen.writeStringField('ChargedX3Code__c', lfc.CodeX3Fact);
        gen.writeStringField('ChargedX3AddressCode__c', lfc.CodeAdrX3Fact);
        gen.writeStringField('ProductorX3Code__c', lfc.CodeX3Prod);
        gen.writeStringField('ProductorX3AddressCode__c', lfc.CodeAdrX3Prod);
        gen.writeStringField('AskerCodeX3__c', lfc.CodeX3Dem);
        gen.writeStringField('AskerX3AddressCode__c', lfc.CodeAdrX3Dem);
        gen.writeStringField('FrenchTaxeID__c', lfc.IdTVA);
        if(lfc.ContactID != null)
        gen.writeStringField('Contact__c', lfc.ContactID);  
        gen.writeStringField('State__c', 'En attente qualification par le collecteur');
        gen.writeEndObject();           
        String JSONString = gen.getAsString();
        
        system.debug('JSONString' + JSONString);
        
        String endPointURL = Label.ORG_URL+'/services/data/v47.0/sobjects/FIP_FIP__c/';
        
        system.debug('saveContents.endPointURL =' + endPointURL);
        system.debug('saveContents.token_access =' + token_access);
        

        Http h = new Http();
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint(endPointURL);
        req.setMethod('POST');
        req.setHeader('Authorization','Bearer '+token_access);
        req.setHeader('Content-Type', 'application/json');        
        req.setBody(JSONString);
        
         system.debug('saveContents.req =' + req);
        HttpResponse res = h.send(req);
        
         system.debug('saveContents.res =' + res);
        System.debug('res.getStatus() '+res.getStatus());
        
        return res.getStatus();
        
    }
    
    
    public static FIPFormController checkNullValue(FIPFormController l){
        
        if(l.CAP == null)
            l.CAP = '';
        
        if(l.Appelation == null)
            l.Appelation = '';
        
        if(l.Quantite == null)
            l.Quantite = 0;
        
        
        if(l.Classe == null)
            l.Classe = '';
        
        if(l.GEmballage == null)
            l.GEmballage = '';
        
        if(l.Rsociale == null)
            l.Rsociale = '';
        
        if(l.Siret == null)
            l.Siret = '';
        
        if(l.Adr1 == null)
            l.Adr1 = '';
        
        if(l.Adr2 == null)
            l.Adr2 = '';
        
        if(l.Adr3 == null)
            l.Adr3 = '';
        
        if(l.CodePostal == null)
            l.CodePostal = '';
        
        if(l.Ville == null)
            l.Ville = '';
        
        if(l.AdrChantier == null)
            l.AdrChantier = '';       
        
        if(l.TypeFIP == null)
            l.TypeFIP = '';
        
        if(l.Envoie == null)
            l.Envoie = '';
        
        if(l.CodeNomen == null)
            l.CodeNomen = '';
        
        if(l.TypeAmiante == null)
            l.TypeAmiante = '';
        
        if(l.TypeCondi == null)
            l.TypeCondi = '';
        
        if(l.NONU == null)
            l.NONU = '';
        
        if(l.RsocilialDemandeur == null)
            l.RsocilialDemandeur = '';
        
        if(l.SIRETDemandeur == null)
            l.SIRETDemandeur = '';
        
        if(l.AdrDemandeur == null)
            l.AdrDemandeur = '';
        
        if(l.CodePostalDemandeur == null)
            l.CodePostalDemandeur = '';
        
        if(l.VilleDemandeur == null)
            l.VilleDemandeur = '';
        
        if(l.CodeX3Fact == null)
            l.CodeX3Fact = '';
        
        if(l.CodeAdrX3Fact == null)
            l.CodeAdrX3Fact = '';
        
        if(l.CodeX3Prod == null)
            l.CodeX3Prod = '';
        
        if(l.CodeAdrX3Prod == null)
            l.CodeAdrX3Prod = '';
        
        if(l.CodeX3Dem == null)
            l.CodeX3Dem = '';
        
        if(l.CodeAdrX3Dem == null)
            l.CodeAdrX3Dem = '';
        
        if(l.IdTVA == null)
            l.IdTVA = '';
        
         if(l.NCAP == null)
            l.NCAP = '';
        
        return l;
        
    }
    
}