public class OPP_NouvelleOpportuniteFille {
    
    public Opportunity oppMere = new Opportunity();
    public string recordType {get;set;} // search keyword
    public List<SelectOption> recordsTypesSelect{get;set;}
    public List<RecordType> recordTypes{get;set;}
    public Account acc = new Account();
    public Contact ctc = new Contact();
    
    Boolean ContactSet = false;
    
    public OPP_NouvelleOpportuniteFille(ApexPages.StandardSetController controller) {
        // get the current search string
        oppMere = [Select ID, name, Nature__c, AccountID, ContactName__c, TonnageN__c, CloseDate, StageName, Probability  From Opportunity Where ID = :Apexpages.currentPage().getParameters().get('id')];
        recordTypes = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SobjectType='Opportunity' AND DeveloperName LIKE :'%Intermediaire' ORDER BY Name ASC];
        recordsTypesSelect = new List<SelectOption>();
        for(RecordType r : recordTypes){
            recordsTypesSelect.add(new SelectOption(r.DeveloperName, r.Name));
        }
    }
    
    public PageReference redirectToCreation() {
        
        
        if(oppMere.ContactName__c != null){
            ctc =[Select Name From Contact Where ID = : oppMere.ContactName__c];
            contactSet = true;
        }
        
        // Transformation en Date le champ DateTime de l'événement
        String dateAO =  oppMere.CloseDate.day()+'/'+oppMere.CloseDate.month()+'/'+oppMere.CloseDate.year();
        String tonnage = '0';
        if(OppMere.TonnageN__c != null){
            tonnage = String.valueOf(OppMere.TonnageN__c);
            tonnage = tonnage.replace('.', ',');
        }
        String idMere = oppMere.ID;
        
        for(RecordType r : recordTypes){
            
            if(r.DeveloperName.equals(recordType)){
                String url = '/006/e?RecordType=' + r.Id+'&'
                    +Label.OPP_OppMere+'='+oppMere.Name+'&'+Label.OPP_OppMere+'_lkid='+oppMere.ID+'&'
                    +Label.OPP_Nature+'='+oppMere.Nature__c+'&'
                    +Label.OPP_Tonnage+'='+tonnage+'&'
                    +'opp9'+'='+dateAO+'&'
                    +'opp11'+'='+oppMere.StageName+'&'
                    +'opp12'+'='+oppMere.Probability;
                
                
                //Check if contact isn't null
                if(contactSet){
                    url += '&'+Label.OPP_Contact+'='+ctc.Name+'&'+Label.OPP_Contact+'_lkid='+oppMere.ContactName__c;
                    
                }
                
                url +='&retURL='+oppMere.ID+'&'+'nooverride=1';
                return new PageReference(url);
            }
            
        }
        return new PageReference('/'+oppMere.ID);
    }
}