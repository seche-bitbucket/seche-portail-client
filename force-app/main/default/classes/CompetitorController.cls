public with sharing class CompetitorController {
    
    public List<Address> address {get; set;}
    public List<Competitors> competitors {get; set;}
    String selectedCompetitorID;
    public Competitors selectCompetitor {get; set;}
    public String startDate {get; set;}
    public Date queryDate {get; set;}
    
    
    public void loadCompetitors(){
        address = new List<Address>();
        String str = ApexPages.currentpage().getparameters().get('Id');
             
        if(str == null && selectCompetitor != null)
            str = selectCompetitor.ID;
        
         System.debug('Test 3 : '+str);
        
        competitors = loadMainCompetitors(str);
        
        if(str != null){
            startDate = ApexPages.currentpage().getparameters().get('startDate');
            queryDate = date.parse(startDate);    
            address = findLostOpportunitiesWithSelectedCompetitor(str);
            
            
        }else{
            
            address = new List<Address>();
            
            Address add = new Address();
            
            add.Name = 'Séché Environnement';
            add.BillingStreet = '33 Avenue du Maine';
            add.BillingCity = 'Paris 15';
            add.BillingPostalCode = '75015';
            add.BillingCountry = 'France';
            address.add(add);
            
            selectCompetitor = new Competitors();
            selectCompetitor.Name = 'Sélectionner un concurrent';
            selectCompetitor.TurnoverFormatted = '-';
            selectCompetitor.TonnageFormatted = '-';
            
            queryDate = Date.today();
            queryDate = Date.newInstance(queryDate.year(), 1, 1);
            startDate = queryDate.format();
            
        }
    }
    
    public PageReference loadOpportunities(){
        String str = ApexPages.currentpage().getparameters().get('getId');
 
        PageReference pr=Page.CompetitorMap;
        pr.getParameters().put('id',str);
        pr.getParameters().put('startDate',startDate);
        return pr;
    }
    
    public List<Competitors> loadMainCompetitors(String id){
        List<Competitors> lstC = new List<Competitors>();
        Map<ID, Competitors> mapC = new Map<ID, Competitors>();
        for(Opportunity opp : [Select Id, Name, AMOUNT, TONNAGEN__C, ATTRIBUTAIRE__C, ATTRIBUTAIRE__r.Name, ATTRIBUTAIRE__r.Maison_mere__c, ATTRIBUTAIRE__r.Maison_mere__r.Name FROM Opportunity 
                               WHERE StageName = 'Clôturée / Perdue' AND AMOUNT != null AND TONNAGEN__C != null AND Recordtype.DeveloperName != 'OPP_OpportuniteMere' AND ATTRIBUTAIRE__C != null AND DateFinEngagement__c >=: queryDate]){
                                                                     
                                   //Globe
                                   if(opp.ATTRIBUTAIRE__r.Maison_mere__c == null){
                                       if(mapC.containsKey(opp.ATTRIBUTAIRE__C)){
                                           Competitors c = mapC.get(opp.ATTRIBUTAIRE__C);
                                           c.Turnover += opp.Amount; 
                                           c.Tonnage += opp.tonnageN__c;
                                           mapC.put(c.ID, c);
                                       }else{
                                           Competitors c = new Competitors();
                                           c.ID = opp.ATTRIBUTAIRE__C;
                                           c.Name = opp.Attributaire__r.Name; 
                                           c.Turnover = opp.Amount; 
                                           c.Tonnage = opp.tonnageN__c;
                                           mapC.put(c.ID, c);
                                       }
                                   }else{
                                       if(mapC.containsKey(opp.ATTRIBUTAIRE__r.Maison_mere__c)){
                                           Competitors c = mapC.get(opp.ATTRIBUTAIRE__r.Maison_mere__c);
                                           c.Turnover += opp.Amount; 
                                           c.Tonnage += opp.tonnageN__c;
                                           mapC.put(c.ID, c);
                                       }else{
                                           Competitors c = new Competitors();
                                           c.ID = opp.ATTRIBUTAIRE__r.Maison_mere__c;
                                           c.Name = opp.ATTRIBUTAIRE__r.Maison_mere__r.Name; 
                                           c.Turnover = opp.Amount; 
                                           c.Tonnage = opp.tonnageN__c;
                                           mapC.put(c.ID, c);
                                       }
                                   }
                               }
        
        selectCompetitor = mapC.get(id);
        
        for(Competitors c: mapC.values()) {
            c.TurnoverFormatted = formatDecimal(c.Turnover);
            c.TonnageFormatted = formatDecimal(c.Tonnage);
            lstC.add(c);
        }
        
        System.debug('List Competitor'+lstC);
        
        return lstC; 
    }
    
    public List<Address> findLostOpportunitiesWithSelectedCompetitor(String Id){
        List<Address> results = new List<Address>();
        
        Map<ID, Address> mapA = new Map<ID, Address>();
        for(Opportunity opp : [Select Id, AMOUNT, TONNAGEN__C, AccountId, Account.Name, Account.BillingStreet, Account.BillingCity, Account.BillingPostalCode, Account.BillingCountry  FROM Opportunity 
                               where StageName = 'Clôturée / Perdue' AND AMOUNT != null AND TONNAGEN__C != null AND DateFinEngagement__c >=: queryDate AND Recordtype.DeveloperName != 'OPP_OpportuniteMere' AND ATTRIBUTAIRE__C != null AND (ATTRIBUTAIRE__C = : ID OR ATTRIBUTAIRE__r.Maison_mere__c = : ID) LIMIT 50000]){
                                   
                                   if(mapA.containsKey(opp.AccountId)){
                                       Address add = mapA.get(opp.AccountId);
                                       add.Turnover += opp.Amount;
                                       add.Tonnage += opp.TonnageN__c;
                                       add.Count += 1;
                                       mapA.put(add.ID, add);
                                   }else{
                                       
                                       Address add = new Address();
                                       add.ID = opp.AccountId;
                                       add.Name = opp.Account.Name;
                                       add.BillingStreet = opp.Account.BillingStreet;
                                       add.BillingCity = opp.Account.BillingCity;
                                       add.BillingPostalCode = opp.Account.BillingPostalCode;
                                       add.BillingCountry = opp.Account.BillingCountry;
                                       add.Turnover = opp.Amount;
                                       add.Tonnage = opp.TonnageN__c;
                                       add.Count = 1;
                                       
                                       mapA.put(add.ID, add);
                                   }
                               }
        
        for(Address a: mapA.values()) {
            
            a.TurnoverFormatted = formatDecimal(a.Turnover);
            a.TonnageFormatted = formatDecimal(a.Tonnage);
            results.add(a);
        }
        
        return results;
        
    }
    
    public String formatDecimal(Decimal deci){
        
        List<String> args = new String[]{'0','number','### ### ###,##'};
            return String.format(deci.format(), args);
        
    }
    
    
    
    
    public class Address{
        
        public String ID {get; set;}
        public String Name {get; set;}
        public String BillingStreet {get; set;}
        public String BillingCity {get; set;}
        public String BillingPostalCode {get; set;}
        public String BillingCountry {get; set;}
        public Decimal Turnover {get; set;}
        public String TurnoverFormatted {get; set;}
        public Decimal Tonnage {get; set;}
        public String TonnageFormatted {get; set;}
        public Integer Count {get; set;}
        
    }
    
    public class Competitors{
        
        public String ID {get; set;}
        public String Name {get; set;}
        public Decimal Turnover {get; set;}
        public String TurnoverFormatted {get; set;}
        public Decimal Tonnage {get; set;}
        public String TonnageFormatted {get; set;}
        
    }
    
}