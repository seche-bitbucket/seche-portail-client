/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Mohammed Amine IDRISSI TAGHKI <mohammed-amine.idrissi-taghki@capgemini.com>
 * @version        1.0
 * @created        2021-09-15
 * @systemLayer    Manager
 *
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @Desciption: This class contains methods to handle DDE
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */
public with sharing class LWC_DDECtrl {
    
    @AuraEnabled
    public static String copyProducts(String type, String masterId){
        try {

            String queryParameter = '%'+type+'%';
            PricebookEntry [] lstPriceBooks = [SELECT Product2Id FROM PricebookEntry WHERE Pricebook2Id IN (SELECT id FROM Pricebook2 WHERE Name LIKE :queryParameter)];
            system.debug(lstPriceBooks);

            List<Id> lstIds = new List<Id>();

            for(PricebookEntry pb: lstPriceBooks) {
                lstIds.add(pb.Product2Id);
            }

            Product2 [] products = [SELECT ADR__c, N_CAP__c, Packaging__c, Name FROM Product2 WHERE ID IN :lstIds];
            
            List<WasteCollectRequestItem__c> lstWastes = new List<WasteCollectRequestItem__c>();
            for(PricebookEntry pb: lstPriceBooks) {
                for(Product2 prod: products) { 
                    if(prod.Id == pb.Product2Id) {
                        lstWastes.add(new WasteCollectRequestItem__c(ProductName__c=prod.Name, Product__c=pb.Product2Id, Demande_Enlevement__c=masterId, ADR__c=prod.ADR__c, CAP__c=prod.N_CAP__c, Packaging__c=prod.Packaging__c));
                        break;
                    }
                }
            }
            
            system.debug(lstWastes);

            insert lstWastes;
            return 'OK';

        } catch (Exception e) {
            system.debug(e.getMessage());
            return 'NOK';
        }
    }

    @AuraEnabled
    public static String getDefaultProd(){
        return [SELECT ID FROM Product2 WHERE TECH_ExternalID__c = 'DEFAUT'].Id;
    }

    @AuraEnabled
    public static void deleteAll(String ids){
        List<String> lst = ids.split(', ');
        delete [SELECT ID FROM WasteCollectRequestItem__c WHERE ID IN :lst];
    }

}