@isTest
public with sharing class VFC_GetStatusQuoteControllerTest {


static testMethod void initTest() {
     
    String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

    //Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;

    Account ac = new Account();
	ac.Name = 'testAccName';
	ac.CustomerNature__c = 'Administration';
	ac.Industry = 'Trader';
	ac.CurrencyIsoCode = 'EUR';
	ac.Producteur__c = false;
	ac.BillingPostalCode = '00000';
	ac.NAF_Number__c = '1234A';
	insert ac;

	Contact contactTest = new Contact();
	contactTest.LastName = 'testName';
	contactTest.AccountId = ac.Id;
	contactTest.Salesman__c = director.Id;
	contactTest.Email = 'aaa@yopmail.com';
	//contactTest.Phone = '0000000000';
	insert contactTest;

    
	Pricebook2 pb = new Pricebook2(Name='PricebookFille', isActive=true);
	insert pb;

	Product2 prd = new product2(name = 'Test',TECH_ExternalID__c = '012345',Family = 'Traitement');
	insert prd;

	PricebookEntry pbeMere = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=Test.getStandardPricebookId(),
	                                            isActive=true);
	insert pbeMere;

	PricebookEntry pbe = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=pb.Id,
	                                        isActive=true,usestandardprice = true);
	insert pbe;
	//Get a Record Type for Opportunity
	RecordType rt = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_OpportuniteMere' LIMIT 1];

	//Create Opportunity Mother
	Opportunity opport = new Opportunity(Name = 'OPPtest', RecordTypeId = rt.Id, AccountID= ac.Id, ContactName__c = contactTest.Id, Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', PriceBook2Id = pb.Id);

	insert opport;
	system.debug('opport '+opport.ID);
	Quote qu = new Quote(name='01010', opportunityId=opport.Id);
	insert qu;

	ContentVersion cv = new ContentVersion();
    cv.title = 'test content trigger';      
    cv.PathOnClient ='test';           
    cv.VersionData = Blob.valueOf('TEST PM');          
    insert cv;         
    
    ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
    
    ContentDocumentLink contentlink=new ContentDocumentLink();
    contentlink.LinkedEntityId=qu.id;
    contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
    contentlink.ShareType = 'V';
    insert contentlink;
    Package__c pkg = new Package__c(
        ConnectivePackageId__c='3bb6a2d1-35db-4a3a-a434-868c01bcca9d',
        ConnectivePackageStatus__c='Pending'
    );
    insert pkg;

    DocumentsForSignature__c doc=new DocumentsForSignature__c();
    doc.Name= cv.title;
    doc.SigningUrl__c='';
    doc.SentTo__c=contactTest.Id;
    doc.TECH_ExternalId__c=cv.Id;
    doc.ConnectiveDocumentId__c = cv.Id;
    doc.Status__c = 'PENDING';
    doc.Quote__c=qu.Id;
    doc.Package__c = pkg.Id;
    insert doc;


	
	System.RunAs(director) {
		List<Quote> quoteList = [SELECT Id,Name FROM Quote LIMIT 20];				
		PageReference pageRef = Page.VFP_GetStatusQuote;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(quoteList);
		apexPages.Currentpage().getParameters().put('id',quoteList[0].Id);
		VFC_GetStatusQuoteController myController = new VFC_GetStatusQuoteController(ssc);
		System.debug('##########TEST PM '+ quoteList[0].Id);
		Test.setMock(HttpCalloutMock.class, new ConnectiveMockGetStatus()); 
        ConnectiveMockGetStatus.externalIdContact = contactTest.Id;
        ConnectiveMockGetStatus.externalIdDocument = testContent.Id;
        ConnectiveMockGetStatus.connectiveIdPackage = pkg.ConnectivePackageId__c;
        myController.setIsTestRunnig(false);
        myController.init(); 
		myController.goback();        
        System.assertEquals(false, myController.getIsNoSelectedOpp());
	}
}

static testMethod void initTestWithNoSelected() {
     
    String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

    //Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;

    Account ac = new Account();
	ac.Name = 'testAccName';
	ac.CustomerNature__c = 'Administration';
	ac.Industry = 'Trader';
	ac.CurrencyIsoCode = 'EUR';
	ac.Producteur__c = false;
	ac.BillingPostalCode = '00000';
	ac.NAF_Number__c = '1234A';
	insert ac;

	Contact contactTest = new Contact();
	contactTest.LastName = 'testName';
	contactTest.AccountId = ac.Id;
	contactTest.Salesman__c = director.Id;
	contactTest.Email = 'aaa@yopmail.com';
	//contactTest.Phone = '0000000000';
	insert contactTest;

    
	Pricebook2 pb = new Pricebook2(Name='PricebookFille', isActive=true);
	insert pb;

	Product2 prd = new product2(name = 'Test',TECH_ExternalID__c = '012345',Family = 'Traitement');
	insert prd;

	PricebookEntry pbeMere = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=Test.getStandardPricebookId(),
	                                            isActive=true);
	insert pbeMere;

	PricebookEntry pbe = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=pb.Id,
	                                        isActive=true,usestandardprice = true);
	insert pbe;
	//Get a Record Type for Opportunity
	RecordType rt = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_OpportuniteMere' LIMIT 1];

	//Create Opportunity Mother
	Opportunity opport = new Opportunity(Name = 'OPPtest', RecordTypeId = rt.Id, AccountID= ac.Id, ContactName__c = contactTest.Id, Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', PriceBook2Id = pb.Id);

	insert opport;
	system.debug('opport '+opport.ID);
	Quote qu = new Quote(name='01010', opportunityId=opport.Id);
	insert qu;

	ContentVersion cv = new ContentVersion();
    cv.title = 'test content trigger';      
    cv.PathOnClient ='test';           
    cv.VersionData = Blob.valueOf('TEST PM');          
    insert cv;         
    
    ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
    
    ContentDocumentLink contentlink=new ContentDocumentLink();
    contentlink.LinkedEntityId=qu.id;
    contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
    contentlink.ShareType = 'V';
    insert contentlink;
    Package__c pkg = new Package__c(
        ConnectivePackageId__c='3bb6a2d1-35db-4a3a-a434-868c01bcca9d',
        ConnectivePackageStatus__c='Pending'
    );
    insert pkg;

    DocumentsForSignature__c doc=new DocumentsForSignature__c();
    doc.Name= cv.title;
    doc.SigningUrl__c='';
    doc.SentTo__c=contactTest.Id;
    doc.TECH_ExternalId__c=cv.Id;
    doc.ConnectiveDocumentId__c = cv.Id;
    doc.Status__c = 'PENDING';
    doc.Quote__c=qu.Id;
    doc.Package__c = pkg.Id;
    insert doc;


	
	System.RunAs(director) {
		List<Quote> quoteList = [SELECT Name FROM Quote LIMIT 20];				
		PageReference pageRef = Page.VFP_GetStatusQuote;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(quoteList);
        VFC_GetStatusQuoteController myController = new VFC_GetStatusQuoteController(ssc);
		pageRef.getParameters().put('QuoteId',quoteList[0].Id);
		Test.setMock(HttpCalloutMock.class, new ConnectiveMockGetStatus()); 
        ConnectiveMockGetStatus.externalIdContact = contactTest.Id;
        ConnectiveMockGetStatus.externalIdDocument = testContent.Id;
        ConnectiveMockGetStatus.connectiveIdPackage = pkg.ConnectivePackageId__c;
        myController.setIsTestRunnig(true);
        myController.init(); 
		myController.goback();        
        System.assertEquals(true, myController.getIsNoSelectedOpp());
	}
}

	@isTest
	static void getIsTestRunningTest(){
		ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<Quote>{new Quote()});
		VFC_GetStatusQuoteController ctrl = new VFC_GetStatusQuoteController(ssc);
		System.assert(!ctrl.getIsTestRunnig());
	}
}