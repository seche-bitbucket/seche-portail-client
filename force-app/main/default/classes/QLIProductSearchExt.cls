public class QLIProductSearchExt {
    public List<WrapperProduct> productList {get; set;}
    public List<WrapperProduct> productList2 {get; set;}
    private List<pricebookentry> selectedQProducts=new List<pricebookentry>();
    private String query {get;set;}
    List<pricebookentry> results {get;set;}
    Public string productFamily {get;set;}
    //private List<pricebookentry> selectedProducts{get;set;}
    public Integer size{get; set;}
    public Integer noOfRecords{get; set;}
    public List<SelectOption> paginationSizeOptions{get;set;}
    public String family { get; set; }
    public String keyword { get; set; }
    public Boolean selected {get; set;}
    public Integer lignes{get;set;}
    PriceBookEntry priceBE;
    public Set<Id> selectedRecordIds;
   // String OppName {get;set;}
    Map <id,PriceBookEntry> SelectedPbeMap = new Map <id,PriceBookEntry>();
    private List <QuoteLineItem> listQuoLineItems = new list<QuoteLineItem>();
    public List <QuoteLineItem> listQuoLineItemsToInsert = new list<QuoteLineItem>();
    private id QuoId = ApexPages.currentPage().getParameters().Get('QuoId');
    private String QuoName = ApexPages.currentPage().getParameters().Get('QuoName');
    private List<QuoteLineItem> QuolineItems {get; set;}
    private Id priceBookId;
    
      public QLIProductSearchExt(){
          size=200;
        paginationSizeOptions = new List<SelectOption>();
        paginationSizeOptions.add(new SelectOption('5','5'));
        paginationSizeOptions.add(new SelectOption('10','10'));
        paginationSizeOptions.add(new SelectOption('20','20'));
        paginationSizeOptions.add(new SelectOption('50','50'));
        paginationSizeOptions.add(new SelectOption('100','100'));
        paginationSizeOptions.add(new SelectOption('200','200'));
        // priceBE = (PriceBookEntry) Controller.getRecord();  
        Quote Quo = [select pricebook2Id from Quote where id =: QuoId]; 
        priceBookId = Quo.pricebook2Id;
      }
    public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) { 
                
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [select Product2.Name,Product2.Description,Product2.ProductCode,UnitPrice,Product2.Family,Name,LogoNature__c,Product2.EuropeanCode__c  from pricebookentry where Product2.Name != null and IsActive = true and pricebook2Id =:priceBookId  order by Product2.Name]));
                setCon.setPageSize(size);  
                noOfRecords = setCon.getResultSize();
                
                
            }            
            return setCon;
        }
        set;
    }
    
    public List<Pricebookentry> getResults() {
        
        
        return (List<Pricebookentry>) setCon.getRecords();
        
    }
    
    public List<WrapperProduct> getProduct() {
        
        productList2 = productList;
        ProductList = new List<WrapperProduct>();
        
        for(PriceBookEntry pr: getresults()) {
            
            ProductList.add(new WrapperProduct(pr));
        }
        
        return ProductList;
    }
    
    //Changes the size of pagination
    public PageReference refreshPageSize() {
        setCon.setPageSize(size);
        return null;
    }
    
    /*public String sortField {
        get  { if (sortField == null) {sortField = 'Product2.Name'; } return sortField;  }
        set;
    }*/
    
    public pageReference search() {
        string family = productFamily;
        if (keyword !=null)
        {
        	string keyword2 = '%'+keyword.Replace(' ','%')+'%';
        }
        Boolean isWhere = false;
        query = 'select Product2.Name,Product2.Description,Product2.ProductCode,UnitPrice,Product2.Family,Name,LogoNature__c,Product2.EuropeanCode__c  from pricebookentry Where IsActive = true and Pricebook2Id =: priceBookId' ;
        
        if (!String.IsBlank(keyword))
        {
            query+= ' And Product2.name like: keyword2';     
        }
        if(!String.IsBlank(family))
        {
            query+= ' And Product2.Family=:family';  
        }
        query+= ' Order by Product2.Name';
        this.InitCon(Database.Query (query));
        
        return null;
        
    }
    public PageReference processSelected()
    {
        getProduct();
        
        if (ProductList!=null)
        {
            for(WrapperProduct pPbe: productList2) {
                if(pPbe.selected == true) {
                    selectedQProducts.add(pPbe.pbe);
                    Integer i=pPbe.lignes;
                    while(i>1){
                        selectedQProducts.add(pPbe.pbe);
                        i=i-1;
                    }
                }
            }
            for(pricebookentry pbe: selectedQProducts) {
               
            }
        }
        if (selectedQProducts.size()>0)
        {
            PageReference pageRef = new PageReference ('/Apex/QLI_EditAllQuoLineItems?QuoId='+QuoId+'&QuoName='+QuoName);
            pageRef.setRedirect(false);
            return pageRef;  
        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, 'Veuillez sélectionner au moins une ligne de produit!'));
            return null;
        
        }
    }
     public PageReference saveProcess()
    {
        For(QuoteLineItem qoLI : getQuoLineItems() )
        {
            QuoteLineItem qoLINew = new QuoteLineItem (PriceBookEntryId=qoLI.PriceBookEntryId,QuoteId = QuoId,N_CAP__c = qoLI.N_CAP__c,Quantity = qoLI.Quantity,
                                                                   Description = qoLI.Description,Sector__c = qoLI.Sector__c,Unit__c = qoLI.Unit__c,IncludedTGAP__c = qoLI.IncludedTGAP__c,
                                                                  Packaging__c= qoLI.Packaging__c,TransportType__c = qoLI.TransportType__c,Treatment__c = qoLI.Treatment__c, UnitPrice = qoLI.UnitPrice,Product2Id = qoLI.Product2Id);
            listQuoLineItemsToInsert.add(qoLINew);
        }
        insert listQuoLineItemsToInsert;
        PageReference pageRef3 = new PageReference('/'+QuoId);
        pageRef3.setRedirect(false);
        return pageRef3;
    }
    Public List<QuoteLineItem> getQuoLineItems()
    {
        if (listQuoLineItems.size()==0)
        {
            for (pricebookentry pbeSelc : selectedQProducts)
            {
                QuoteLineItem oppLineItem =  New QuoteLineItem(PriceBookEntryId=pbeSelc.Id,QuoteId = QuoId,UnitPrice = pbeSelc.UnitPrice,Tech_Name2__c = pbeSelc.Name, product2Id =pbeSelc.Product2Id);
                listQuoLineItems.add(oppLineItem);
            }
        }
        return listQuoLineItems;  
    }
    public PageReference cancelProcess()
    {
        
        return new PageReference('/'+QuoId);
        
    }
    /*public List<pricebookentry> runQuery() {
        
        try {
            results = Database.query(query+ ' order by ' + sortField);
            return results;
            
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops!'));
            return null;
            
        }
        
    }*/
    public void initCon(List<pricebookentry> productsToInit){
        
        setCon = new ApexPages.StandardSetController(productsToInit);
        
        setCon.setPageSize(size);  
        
        noOfRecords = setCon.getResultSize();
        
    }
    public class WrapperProduct {
        public PriceBookEntry pbe {get; set;}
        public Boolean selected {get; set;}
        public Integer lignes {get; set;}
        //This is the contructor method. When we create a new cContact object we pass a Contact that is set to the con property. We also set the selected value to false
        public WrapperProduct(PriceBookEntry p) {
            pbe = p;
            selected = false; 
            lignes = 1;
        }
    }

}