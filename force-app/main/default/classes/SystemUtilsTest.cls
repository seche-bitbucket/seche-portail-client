@isTest
public with sharing class SystemUtilsTest {

    public class ToolingAPI implements HttpCalloutMock{
        
        public HttpResponse respond(HttpRequest req) {
            
            HttpResponse res = new HttpResponse();

            System.debug('req = ' + req.getEndpoint()+' '+req.getMethod());
            String reqMethod = req.getMethod();
            String reqEndpoint = req.getEndpoint();
            res.setStatusCode(200);
            res.setBody('{"size":2,"totalSize":2,"done":true,"queryLocator":null,"entityTypeName":"ProfileLayout","records":[{"attributes":{"type":"ProfileLayout","url":"/services/data/v52.0/tooling/sobjects/ProfileLayout/01G0D000004oK8YUAU"},"Layout":{"attributes":{"type":"Layout","url":"/services/data/v52.0/tooling/sobjects/Layout/00h0D000000O0evQAC"},"Name":"OPP-Speichim Layout","TableEnumOrId":"Opportunity"},"ProfileId":"00e58000000VRRVAA4","Profile":{"attributes":{"type":"Profile","url":"/services/data/v52.0/tooling/sobjects/Profile/00e58000000VRRVAA4"},"Name":"System Administrator"},"RecordTypeId":"0120D0000004mU7QAI"},{"attributes":{"type":"ProfileLayout","url":"/services/data/v52.0/tooling/sobjects/ProfileLayout/01G0D000004obdeUAA"},"Layout":{"attributes":{"type":"Layout","url":"/services/data/v52.0/tooling/sobjects/Layout/00h58000000YwbAAAS"},"Name":"Fermer la présentation de requête","TableEnumOrId":"CaseClose"},"ProfileId":"00e58000000VRRVAA4","Profile":{"attributes":{"type":"Profile","url":"/services/data/v52.0/tooling/sobjects/Profile/00e58000000VRRVAA4"},"Name":"System Administrator"},"RecordTypeId":"0120D0000004mU7QAI"}]}');

            return res;
        }
    }
    
    @isTest
    public static void returnSchemaTest() {
        List<Map<String, Object>> lstSchema = SystemUtils.returnSchema();
        System.assert(lstSchema.size() > 0);
    }

    @isTest 
    public static void getSObjectFieldsTest() {
        List<Map<String, Object>> lstFields = SystemUtils.getSObjectFields('OpportunityLineItem', false);
        System.assert(lstFields.size() > 0);
    }

    @isTest
    public static void getFieldsFromLayoutWithRtypeAndProfileTest() {
        String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

        //Get a profile to create User
        Profile p = [select id from profile where name in:profiLisList limit 1];
        
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);

        System.runAs(director){

            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new ToolingAPI());
            String lstFieldsNoSection = SystemUtils.getFieldsFromLayoutWithRtypeAndProfile([SELECT Id FROM RecordType WHERE DeveloperName = 'OPP_01_OffreSpeichimStandard' LIMIT 1][0].Id, false);
            System.assert(lstFieldsNoSection != null);
            String lstFieldsSection = SystemUtils.getFieldsFromLayoutWithRtypeAndProfile([SELECT Id FROM RecordType WHERE DeveloperName = 'OPP_01_OffreSpeichimStandard' LIMIT 1][0].Id, true);
            System.assert(lstFieldsSection != null);
            Test.stopTest();

        }
    }

    @isTest
    public static void testGUIDAndUploadAndDelete() {
        ByPassUtils.ByPass('QuoteTrigger');
            
        QteLineItemSyncTriggerV2Handler.stop();
        OppLineItemSyncTriggerV2Handler.stop();
        QuoteSyncTriggerV2Handler.stop();
        OppSyncTriggerV2Handler.stop();
        
        Contact ct = TestFactory.generateContact();
        Account acc = TestFactory.generateAccount();
        
        //Quote Section
        Opportunity opp = TestFactory.generateOpport(acc, null);
        Form__c form = TestFactory.generateForm(opp, ct);

        insert new URL__c(SITE_Prefip__c='test@test.com');

        opp.GUID__c = 'TEST001';
        opp.ContactName__c = ct.Id;
        update opp;

        SObject opp1 = SystemUtils.getRecordByGUID('TEST001', 'Opportunity', 'GUID__c');
        System.assert(opp1.get('Id') == opp.Id);

        String upload = SystemUtils.uploadFile(EncodingUtil.base64Encode(Blob.valueOf('Test Content Data')), 'Test.jpg', opp.Id, UserInfo.getUserId());
        System.assert(upload != null);

        // SystemUtils.OpportunityId oppId = new SystemUtils.OpportunityId();
        // oppId.opportunityId = opp.Id;
        // List<SystemUtils.OpportunityId> lst = new List<SystemUtils.OpportunityId>{oppId};

        SystemUtils.FormulaireId formId = new SystemUtils.FormulaireId();
        formId.formulaireId = form.Id;
        List<SystemUtils.FormulaireId> lst = new List<SystemUtils.FormulaireId>{FormId};

        SystemUtils.genGuid(lst);

        SystemUtils.deleteBySingleId(opp.Id);
        SystemUtils.deleteBySingleId(form.Id);
        
    }

}