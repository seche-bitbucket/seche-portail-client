public with sharing class LC_SendingForSigningController {
@AuraEnabled
public String message {get; set;}
@AuraEnabled
public String showToastMode {get; set;}
@AuraEnabled
public Quote objectQuote {get; set;}


@AuraEnabled
public static Quote getQuote(Id quoteId){
     System.debug('getQuote-----Start- parm: Quote ID : '+quoteId);	
	return [SELECT Id, Name From Quote Where Id =: quoteId];
}

@AuraEnabled
public static LC_SendingForSigningController requestForSign(Quote quot){
    System.debug('requestForSign-----Start- parm: quot : '+quot);	
    LC_SendingForSigningController wrapper = new LC_SendingForSigningController();
    wrapper.objectQuote = quot;
    String result ='';
        if(quot != null){           
            result = sendQuoteCommunityLink.requestSendQuoteForSignature(quot);
        }
        if(result.equals('KO')){
            wrapper.message = 'Document déjà envoyé pour signature';
            wrapper.showToastMode = 'warning';
            System.debug('requestForSign-----End- result:  '+wrapper);
            return wrapper;
        }else if(result.equals('OK')){
            wrapper.message = 'Document envoyé pour signature';
            wrapper.showToastMode = 'success';
            System.debug('requestForSign-----End- result:  '+wrapper);
            return wrapper;
        }else if(result.equals('Wait')){
            wrapper.message = 'Contact ou pièce jointe manquante sur le devis';
            wrapper.showToastMode = 'warning';
            System.debug('requestForSign-----End- result:  '+wrapper);
            return wrapper;
        }else{
            wrapper.message = 'Veuillez contacter votre administrateur salesforce';
            wrapper.showToastMode = 'warning';
            System.debug('requestForSign-----End- result:  '+wrapper);
            return wrapper;
        }
      
}
}