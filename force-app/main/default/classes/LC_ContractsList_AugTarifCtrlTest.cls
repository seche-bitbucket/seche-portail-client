@isTest
public with sharing class LC_ContractsList_AugTarifCtrlTest {
    
       @isTest 
    static void testIncreaseByValueAndSubmit(){
                    
        Contract__c c = new Contract__c();
        c.name='t';
        insert c;
        Amendment__c am1 = new Amendment__c();
        am1.Contrat2__c=c.id;
        am1.Nature__c='Conditionnement';
        am1.PriceCurrentYear__c=10;
        Amendment__c am2 = new Amendment__c();
        am2.Contrat2__c=c.id;
        am2.Nature__c='Traitement';
        am2.PriceCurrentYear__c=10;
        Amendment__c am3 = new Amendment__c();
        am3.Contrat2__c=c.id;
        am3.Nature__c='Prestation';
        am3.PriceCurrentYear__c=10;
        List<Amendment__c> amendments = new List<Amendment__c>();
        Amendments.add(am1);
        Amendments.add(am2);
        Amendments.add(am3);
        insert amendments;

        String response =  LC_ContractsList_AugTarifCtrl.percentAugmentation(amendments, 10, 0, 0, 0,'noRounding','noRounding','noRounding','noRounding','Test commentaire');       
        
        list<Amendment__c> am=[SELECT PriceNextYear__c,PriceCurrentYear__c, Contrat2__r.RecordLocked__c, Nature__c, TECH_Excluded__c, CompletedAmadement__c from Amendment__c];
        System.assertEquals(11, am[0].PriceNextYear__c);
        System.assertEquals(11, am[1].PriceNextYear__c);
        System.assertEquals(11, am[2].PriceNextYear__c);
        
        response =  LC_ContractsList_AugTarifCtrl.percentAugmentation(am, 0, 10, 0, 0,'supp','supp','supp','supp','Test commentaire');    
        list<Amendment__c> amTraitement=[SELECT PriceNextYear__c,PriceCurrentYear__c, Nature__c, Contrat2__r.RecordLocked__c, TECH_Excluded__c, CompletedAmadement__c from Amendment__c where Nature__c='Traitement'];       
        System.assertEquals(11, amTraitement[0].PriceNextYear__c);
        
        response =  LC_ContractsList_AugTarifCtrl.percentAugmentation(am, 0, 0, 0, 10,'supp','supp','supp','supp','Test commentaire');    
	    list<Amendment__c> amConditionnement =[SELECT PriceNextYear__c,PriceCurrentYear__c, Nature__c, Contrat2__r.RecordLocked__c, TECH_Excluded__c, CompletedAmadement__c from Amendment__c where Nature__c='Conditionnement'];
        System.assertEquals(11, amConditionnement[0].PriceNextYear__c);
        
        response =  LC_ContractsList_AugTarifCtrl.percentAugmentation(am, 0, 0, 20, 0 , 'supp','supp','supp','supp','Test commentaire');    
        list<Amendment__c> amPrestation =[SELECT PriceNextYear__c,PriceCurrentYear__c, Nature__c, Contrat2__r.RecordLocked__c, TECH_Excluded__c, CompletedAmadement__c from Amendment__c where Nature__c='Prestation'];
        System.assertEquals(11, amPrestation[0].PriceNextYear__c);       
        
        AP_AugmentationTarifmanager.RequestResponse rspAME = LC_ContractsList_AugTarifCtrl.validate_FaF_Contracts(new List<Contract__c>{c},String_Helper.CON_Filiale_Drimm,String_Helper.CON_Filiere_CentreDeTriDAE);
                        
    }
    
         @isTest 
    static void testUpdateContract(){
                    
        Contract__c c = new Contract__c();
        c.name='Name Contract';
        c.Etat__c = 'Courrier';
        insert c;
        
        c = [select Id, Name, Etat__c from Contract__c Limit 1];
        
        c.Etat__c = 'Convention';
        
        List<Contract__c> listC = new List<Contract__c>();
        listC.add(c);
       
        LC_ContractsList_AugTarifCtrl.updateContract(listC); 
        
        c = [select Id, Name, Etat__c from Contract__c Limit 1];
        
        System.assertEquals(c.Etat__c, 'Convention');   
                        
    }
    
        @isTest 
    static void testFetchContracts(){

        Account ac = new Account();
        ac.Name = 'testAccName';
        ac.CustomerNature__c = 'Industriel';
        ac.Industry = 'Trader';
        ac.CurrencyIsoCode = 'EUR';
        ac.Producteur__c = false;
        ac.BillingPostalCode = '00000';
        ac.NAF_Number__c = '1234A';
        ac.SIRET_Number__c = '10111121131151';
                    
        Contract__c c1 = new Contract__c();   
        c1.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;   
        c1.Status__c = String_Helper.CON_Status_Brouillon;
        c1.Account__c = ac.Id;
        insert c1;
        

        List<Contract__c> contractsList =  LC_ContractsList_AugTarifCtrl.fetchContracts( String_Helper.CON_Filiale_Drimm, String_Helper.CON_Filiere_CentreDeTriDAE, 'Industriel', String_Helper.CON_Status_Brouillon);       
        
        System.assertEquals(contractsList.size(), 0);    
        
                        
    }
    
    @isTest
    static void testMetadata() {
        
        List<Object> mtdata = LC_ContractsList_AugTarifCtrl.getMetadata(String_Helper.CON_Filiale_Drimm, String_Helper.CON_Filiere_CentreDeTriDAE, 'Industriel', String_Helper.CON_Status_Brouillon);
        
        System.assertNotEquals(0, mtdata.size());
        
        List<cDef__mdt> defs = LC_ContractsList_AugTarifCtrl.getDefinitions();
        List<Custom_Column__mdt> columns = LC_ContractsList_AugTarifCtrl.getColumnsLabels();
        
        System.assertNotEquals(0, defs.size());
        System.assertNotEquals(0, columns.size());
        
    }

    @isTest
    static void testAreStandardContractsSubmitted() {
        Boolean result = LC_ContractsList_AugTarifCtrl.AreStandardContractsSubmitted('Triadis Services Toulouse', 'Plateforme de transit');
        System.assertEquals(false, result);
    }
    

}