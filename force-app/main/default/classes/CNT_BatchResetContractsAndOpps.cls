/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-09-26
* @modified       2018-09-26
* @systemLayer    Functional Batch         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		 Batch that resets all contracts and opportunities after our courrier stage
				so that the convention stage starts
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global class CNT_BatchResetContractsAndOpps implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {	// retrieve contracts that have an opportunity linked to them
        String query = 'SELECT Opportunity__c, Name,Account__c,AssociatedSalesRep__c,Filiale__c, RegroupmentIndex__c,ContactRelated__c, Id,Account__r.Name,Status__c FROM Contract__c WHERE Opportunity__c!= Null AND Etat__c = \'Courrier\'';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Contract__c> scope)
    {
        List<Contract__c> Listcontracts=[SELECT Opportunity__c, Name,Account__c, Id,Account__r.Name,
                                         (SELECT Id, ByPassContract__c,Account__r.CustomerNature__c, QuantityPreviousYear__c, QuantityCurrentYear__c,
                                          PriceCurrentYear__c, PriceNextYear__c, WasteLabel__c,
                                          Contrat2__c, Nature__c FROM Avenants__r Order By Account__c)
                                         FROM Contract__c WHERE ID IN:Scope];
        Set<ID> opps=New Set<ID>(); 
        List<Amendment__c> AmendmentsToUpdate=New List<Amendment__c>();
        for(Contract__c c : Listcontracts)
         {   
             c.RegroupmentIndex__c=null;
             c.Status__c='Brouillon';
             c.Etat__c='Convention';
             c.RecordLocked__c=false;
             opps.add(c.Opportunity__c);
             c.Opportunity__c=null;
             for(Amendment__c am:c.Avenants__r){
                 if(am.ByPassContract__c==false){
                     am.PriceNextYear__c=am.PriceCurrentYear__c+am.PriceCurrentYear__c*6*0.01;
                 }else{
                     am.PriceNextYear__c=null;
                 }
                 AmendmentsToUpdate.add(am);
             }
         }
        List<Opportunity> Opportunities=[SELECT ID,Name,StageName FROM Opportunity Where ID IN:opps];
        for(Opportunity opp:Opportunities){
            opp.stageName='Abandonnée';
        }
        Update AmendmentsToUpdate;
        Update Listcontracts;
        Update Opportunities;
    }
    global void finish(Database.BatchableContext BC)
    {  
    }
}