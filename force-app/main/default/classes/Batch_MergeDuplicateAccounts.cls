/*
* ------------------------------------------------------------------------------------------------------------------
* -- Name : ConnectiveUpdateStatusBatchV4
* -- Author : PMB
* -- Company : Capgemini
* -- Purpose : ConnectiveUpdateStatusBatchV4
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 27-05-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
global class Batch_MergeDuplicateAccounts implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful{

    global List<Account> lstAcc {get; set;}
    
    global Database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(
            'SELECT SIRET_Number__c, TECH_IsDuplicate__c, TECH_HasBeenMerged__c, ELLISPHERE__Elli_Ellinumber__c, X3ChargedAddressCode__c, X3AddressCode__c, TECH_X3SPEICHIMID__c, ChargedX3Code__c, TECH_X3SECHEID__c, Industry  FROM Account WHERE TECH_IsDuplicate__c = true AND TECH_NotToMerge__c = false ORDER BY SIRET_Number__c'
        );
    }

    global void execute(Database.BatchableContext BC, List<Account> scope) {
        
        this.lstAcc = scope;
        System.debug(this.lstAcc.size());
        
        Map<String, List<Account>> mapAccsWithSamesSiret = new Map<String, List<Account>>();
        
        for(Account acc: lstAcc) {
            Account firstfound = acc;
            
            if(!mapAccsWithSamesSiret.containsKey(acc.SIRET_Number__c)) {
                mapAccsWithSamesSiret.put(acc.SIRET_Number__c, new List<Account>{acc});
            } else {
                List<Account> accs = new List<Account>();
                accs = mapAccsWithSamesSiret.get(acc.SIRET_Number__c);
                accs.add(acc);
                mapAccsWithSamesSiret.put(acc.SIRET_Number__c, accs);
            }
        }
        
        System.debug(mapAccsWithSamesSiret.size());
        
        try {
        
        	for(String siret: mapAccsWithSamesSiret.keySet()) {
            
                String x3_address;
                String x3_loadAddress;
                String x3_secheId;
                String x3_charge;
                String x3_speichim;
                String industry;
                Account master;
                
                List<Account> duplicates = new List<Account>();
                List<Account> accs = mapAccsWithSamesSiret.get(siret);
    
                for(Account acc: accs) {
                    if(acc.ELLISPHERE__Elli_Ellinumber__c != null) {
                        master = acc;
                        break;
                    }
                }
                
                for(Account acc: accs) {
                    
                    AccountTriggerHandler.stop = true;
                    RegionTriggerBeforInsertUpdateHandler.stop = true;
                    
                    if(master == null){
                        master = acc;
                    }else if(acc.Id != master.Id) {
                        duplicates.add(acc);
                    }
                    
                    if(acc.Industry != null && acc.Industry != 'Aucun') {
                        industry = acc.Industry;
                    }
                    
                    if(acc.TECH_X3SECHEID__c != null) {
                        x3_secheId = acc.TECH_X3SECHEID__c;
                    }
                    
                    if(acc.ChargedX3Code__c != null) {
                        x3_charge = acc.ChargedX3Code__c;
                    }
                    
                    if(acc.TECH_X3SPEICHIMID__c != null) {
                        x3_speichim = acc.TECH_X3SPEICHIMID__c;
                    }
                    
                    if(acc.X3AddressCode__c != null) {
                        x3_address = acc.X3AddressCode__c;
                    }
                    
                    if(acc.X3ChargedAddressCode__c != null) {
                        x3_loadAddress = acc.X3ChargedAddressCode__c;
                    }
                    
                }
                
                system.debug(duplicates);
                system.debug(master);
                
                Account finalOne;
                    
                Boolean empty = false;
                
                if(duplicates.size() == 0) {
                    empty = true;
                }
                
                if(duplicates.size() == 1) {
                    finalOne = duplicates[0];
                    duplicates.remove(0);
                }
                
                while(duplicates.size() > 0 && !empty) {
                    
                    System.debug(duplicates.size());
                    if(duplicates.size() > 1) {
                        
                        merge duplicates[0] duplicates[1];
                        duplicates.remove(1);
                        
                        /*List<Account> newLst = new List<Account>();
    
                        for(Integer i=0; i <= duplicates.size()/2; i++) {
                            Integer k = 2*i;
                            if(duplicates.size() == k) {
                            duplicates = newLst;
                            } else {
                            merge duplicates[k] duplicates[k+1];
                            newLst.add(duplicates[k]);
                            }
                        }*/
                        
                    } else if(duplicates.size() == 1) {
                        finalOne = duplicates[0];
                        duplicates.remove(0);
                    }
                }
                
                if(!empty) {
                    
                    System.debug('merging');
                    merge master finalOne;
                    
                    master.TECH_X3SECHEID__c = x3_secheId;
                    master.ChargedX3Code__c = x3_charge;
                    master.TECH_X3SPEICHIMID__c = x3_speichim;
                    master.X3AddressCode__c = x3_address;
                    master.X3ChargedAddressCode__c = x3_loadAddress;
                    master.TECH_HasBeenMerged__c = true;
                    master.TECH_IsDuplicate__c = false;
                    master.Industry = industry;
                    
                    update master;
                }
                
            } 
            
        } catch(Exception e) {
            System.debug('Exception => '+e.getMessage()+' at Line => '+e.getLineNumber());
        }
            
    }

    global void finish(Database.BatchableContext BC){ }
}