@IsTest
public class OpportunityListViewButtonControllerTest {
    
    @IsTest
    public static void oppSetToLostTest () {
        //List<Opportunity> oppList = New List<Opportunity>();
        List<Opportunity> oppList2 = New List<Opportunity>();
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'System Administrator' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    SubsidiaryCreationRight__c = 'Alcea',
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname');
        
        System.RunAs(currentUser) { 
            
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];

            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'SESSI-1', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try {
                insert ac;
                System.debug('Account ac inserted');
            } catch(Exception e){
                System.debug(e.getMessage()+' at '+e.getLineNumber());
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '03 00 00 00 00', Salesman__c = currentUser.ID);
            
            try{
                insert contactTest;
            }catch(Exception e) {
                System.debug(e.getMessage() + ' at ' + e.getLineNumber());
            }
            
            //Create Opportunity
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', Filiere__c ='DASRI');
            
            try {
                insert opportTest;
            } catch(Exception e2) {
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }
            PageReference pageRef = Page.Opp_SetOppToLost;
            
            pageRef.getParameters().put('OppId',opportTest.Id);
            id OppId = pageRef.getParameters().get('OppId');
            Test.setCurrentPage(pageRef);
            //Test sans Opp sélectionnée
            // ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(oppList);
            // stdSetController.setSelected(oppList);
            // OpportunityListViewButtonController myController = new OpportunityListViewButtonController(stdSetController);
            
            // myController.setToLost(); 
            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            // for(ApexPages.Message message : pageMessages) {
                
            //     system.assertEquals(Label.OPP_Select,message.getDetail() );
            // }
            pageMessages = null;
            
            //Test avec Opp attributaire non renseigné et date de fin contrat non renseigné
            Opportunity opp1  = [select id,Attributaire__c,DateFinEngagement__c, Nature__c, Name,account.name,ContactName__c,Filiale__c,Filiere__c,Amount,TonnageN__c from opportunity where id =: OppId];
            
            opp1.Attributaire__c = ac.Id;
            opp1.DateFinEngagement__c = Date.Today();
            opp1.Nature__c = 'Spot';
            
            System.debug('OPP1: ' + opp1);
            update opp1;
            oppList2.Add(opp1);
            
            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(oppList2);
            stdSetController.setSelected(oppList2);
            OpportunityListViewButtonController myController = new OpportunityListViewButtonController(stdSetController);
            
            myController.setToLost(); 
            myController.getOpprt();
            myController.getSaveButDisp();
            myController.getRetButDisp();
            myController.getIsNoSelectedOpp();
            myController.save();
            system.assertEquals('Clôturée / Perdue', [select StageName from Opportunity where id =: Opp1.Id].StageName);

            myController.returnBack();
        }
    }

    @IsTest
    public static void oppSetFailTest() {
        List<Opportunity> oppList = new List<Opportunity>();

        // Get a profile to create User
        Profile p = [select id from profile where name = :'System Administrator' limit 1];
        
        // Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    SubsidiaryCreationRight__c = 'Alcea',
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname');
        
        System.RunAs(currentUser) { 

            // Get a Record Type for Account
            RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'ACC_France' LIMIT 1];

            // Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];
            
            // Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'SESSI-1', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try {
                insert ac;
                System.debug('Account ac inserted');
            } catch(Exception e) {
                System.debug(e.getMessage()+' at '+e.getLineNumber());
            }
            
            // Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '03 00 00 00 00', Salesman__c = currentUser.ID);
            
            try{
                insert contactTest;
            }catch(Exception e) {
                System.debug(e.getMessage() + ' at ' + e.getLineNumber());
            }
            
            // Create Opportunity
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', Filiere__c ='DASRI');

            Opportunity opportTest2 = new Opportunity(Name = 'TESTOPPORT2', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', Filiere__c ='DASRI');
            
            try {
                insert opportTest;
            } catch(Exception e2) {
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }
            PageReference pageRef = Page.Opp_SetOppToLost;
            
            pageRef.getParameters().put('OppId',opportTest.Id);
            id OppId = pageRef.getParameters().get('OppId');
            Test.setCurrentPage(pageRef);
            
            //Test avec Opp attributaire non renseigné et date de fin contrat non renseigné
            Opportunity opp1  = [select id,Attributaire__c,DateFinEngagement__c, Nature__c, Name,account.name,ContactName__c,Filiale__c,Filiere__c,Amount,TonnageN__c from opportunity where id =: OppId];
            
            System.debug('OPP1: ' + opp1);
            update opp1;
            oppList.Add(opp1);

            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(oppList);
            stdSetController.setSelected(oppList);
            OpportunityListViewButtonController myController = new OpportunityListViewButtonController(stdSetController);
            
            myController.setToLost();
            myController.save();
            system.assertNotEquals('Clôturée / Perdue', [select StageName from Opportunity where id =: opp1.Id].StageName);

            try {
                insert opportTest2;
            } catch(Exception e2) {
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }
            pageRef = Page.Opp_SetOppToLost;

            pageRef.getParameters().put('OppId',opportTest2.Id);
            OppId = pageRef.getParameters().get('OppId');
            Test.setCurrentPage(pageRef);
            
            //Test avec Opp attributaire non renseigné et date de fin contrat non renseigné
            Opportunity opp2  = [select id,Attributaire__c,DateFinEngagement__c, Nature__c, Name,account.name,ContactName__c,Filiale__c,Filiere__c,Amount,TonnageN__c from opportunity where id =: OppId];

            opp2.Attributaire__c = ac.Id;
            opp2.DateFinEngagement__c = Date.Today();
            opp2.Nature__c = 'Spot';
            
            System.debug('OPP2: ' + opp2);
            update opp2;
            oppList.clear();
            oppList.Add(opp2);

            stdSetController = new ApexPages.StandardSetController(oppList);
            stdSetController.setSelected(oppList);
            myController = new OpportunityListViewButtonController(stdSetController);

            myController.setToCancel();
            myController.save();
            system.assertEquals('Abandonnée', [select StageName from Opportunity where id =: opp2.Id].StageName);
        }
    }
    
    @IsTest
    public static void oppSetToWonTest () {
        List<Opportunity> oppList = New List<Opportunity>();
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Chargé(e) d\'affaires' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    SubsidiaryCreationRight__c = 'Alcea',
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname');
        
        System.RunAs(currentUser) { 
            
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'SESSI-1', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try{
                insert ac;
            }catch(Exception e){
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '03 00 00 00 00', Salesman__c = currentUser.ID);
            
            try{
                insert contactTest;
            }catch(Exception e1){
            }
            
            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];
            
            //Create Opportunity
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', Filiere__c ='DASRI');
            
            try{
                insert opportTest;
            }catch(Exception e2){
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }  
            Opportunity opportTest2 = new Opportunity(Name = 'TESTOPPORT2', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', Filiere__c ='DASRI',Attributaire__c=ac.Id,DateFinEngagement__c = Date.today());
            
            try{
                insert opportTest2;
            }catch(Exception e2){
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }  
            PageReference pageRef = Page.OPP_SetOppToWin;
            
            pageRef.getParameters().put('OppId',opportTest.Id);
            id OppId = pageRef.getParameters().get('OppId');
            
            pageRef.getParameters().put('Opp2Id',opportTest2.Id);
            id Opp2Id = pageRef.getParameters().get('Opp2Id');
            
            Test.setCurrentPage(pageRef);
            //Test sans Opp sélectionnée
            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(oppList);
            stdSetController.setSelected(oppList);
            OpportunityListViewButtonController myController = new OpportunityListViewButtonController(stdSetController);
            
            myController.setToWon();

            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            for(ApexPages.Message message : pageMessages) {
                
                system.assertEquals(Label.OPP_Select,message.getDetail() );
                //Test avec Opp séléctionné
                Opportunity opp2  = [select id,Attributaire__c,DateFinEngagement__c,Name,account.name,ContactName__c,Filiale__c,Filiere__c,Amount,TonnageN__c from opportunity where id =: Opp2Id];
                oppList.Add(opp2);
                
                Opportunity opp1  = [select id,Attributaire__c,DateFinEngagement__c,Name,account.name,ContactName__c,Filiale__c,Filiere__c,Amount,TonnageN__c from opportunity where id =: OppId];
                oppList.Add(opp1);
                
                
                stdSetController = new ApexPages.StandardSetController(oppList);
                stdSetController.setSelected(oppList);
                myController = new OpportunityListViewButtonController(stdSetController);
                
                myController.setToWon();
                myController.save();

                system.assertEquals('Clôturée / Gagnée', [select StageName from Opportunity where id =: Opp1.Id].StageName);
                system.assertEquals('Clôturée / Gagnée', [select StageName from Opportunity where id =: Opp2.Id].StageName);
                
                
                
            }
        }
    }
    
    
}