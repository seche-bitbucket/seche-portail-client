@isTest
public class CompetitorControllerTest {

     @IsTest(SeeAllData=true) 
    public static void TestCustomLookupControllerTest(){
        //Set the current VF Page which use the ContactController
        PageReference pgCompetitorMap = Page.CompetitorMap;
        
        //Standard Set controller
        ApexPages.StandardSetController stdSetCtrl;
        
        //Standard controller
        ApexPages.StandardController stdCtrl;
        
        //Get a profile to create User
        Profile p = [select id from profile where name = : 'System Administrator' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    SubsidiaryCreationRight__c = 'Alcea',
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname');
        
        System.RunAs(currentUser) { 
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            //Create Father Account
            Account acFather = new Account(Name = 'Father__ACCO__UNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aéronautique', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try {
                insert acFather;
            } catch(Exception e) {
                System.debug(e.getMessage() + ' at ' + e.getLineNumber());
            }
            
            //Create Father Account 2
            Account acFather2 = new Account(Name = 'Father_ACCOUNT 2', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aéronautique', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try {
                insert acFather2;
            } catch(Exception e) {
                System.debug(e.getMessage() + ' at ' + e.getLineNumber());
            }
            
            //Create Son Account
            Account ac = new Account(Name = 'Son ACCOUNT', Maison_mere__c = acFather.ID, RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aéronautique', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try {
                insert ac;
            } catch(Exception e) {
                System.debug(e.getMessage() + ' at ' + e.getLineNumber());
            }
            
             //Create Son Account 2
            Account ac2 = new Account(Name = 'Son_ACCOUNT 2', Maison_mere__c = acFather2.ID, RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aéronautique', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try {
                insert ac2;
            } catch(Exception e) {
                System.debug(e.getMessage() + ' at ' + e.getLineNumber());
            }
            
          
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '0606060606', Salesman__c = currentUser.ID);
            
            try {
                insert contactTest;
            } catch(Exception e1) {
                System.debug(e1.getMessage() + ' at ' + e1.getLineNumber());
            }
            
            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];
            
            //Create Opportunity
            List<Opportunity> lstOpp = new List<Opportunity>();
            Opportunity opport1 = new Opportunity(Name = 'TESTOPPORT1', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName = 'Clôturée / Perdue', Amount=1234, TONNAGEN__C = 241,  ATTRIBUTAIRE__C = acFather.ID,DateFinEngagement__c = date.today(), Filiale__c = 'Alcea', Filiere__c ='DASRI');
            lstOpp.add(opport1);
            Opportunity opport2 = new Opportunity(Name = 'TESTOPPORT2', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName = 'Clôturée / Perdue', Amount=213412, TONNAGEN__C = 1241,  ATTRIBUTAIRE__C = ac.ID,DateFinEngagement__c = date.today(), Filiale__c = 'Alcea', Filiere__c ='DASRI');
            lstOpp.add(opport2);
            Opportunity opport3 = new Opportunity(Name = 'TESTOPPORT3', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName = 'Clôturée / Perdue', Amount=1231, TONNAGEN__C = 12,  ATTRIBUTAIRE__C = ac.ID,DateFinEngagement__c = date.today(), Filiale__c = 'Alcea', Filiere__c ='DASRI');
            lstOpp.add(opport3);
            Opportunity opport4 = new Opportunity(Name = 'TESTOPPORT4', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName = 'Clôturée / Perdue', Amount=12341, TONNAGEN__C = 123,  ATTRIBUTAIRE__C = acFather.ID,DateFinEngagement__c = date.today(), Filiale__c = 'Alcea', Filiere__c ='DASRI');
            lstOpp.add(opport4);
            Opportunity opport5 = new Opportunity(Name = 'TESTOPPORT4', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName = 'Clôturée / Perdue', Amount=654, TONNAGEN__C = 54,  ATTRIBUTAIRE__C = ac2.ID,DateFinEngagement__c = date.today(), Filiale__c = 'Alcea', Filiere__c ='DASRI');
            lstOpp.add(opport5);
            insert lstOpp;
            
            Test.startTest();
            
            CompetitorController compCtrl = new CompetitorController();
            compCtrl.loadCompetitors();
            
            // Set the currentPage to the test
            Test.setCurrentPage(pgCompetitorMap);
            
            
         
            pgCompetitorMap.getParameters().put('ID', acFather.Id );
            pgCompetitorMap.getParameters().put('startDate', '01/01/2017' );
            compCtrl = new CompetitorController();
            compCtrl.loadCompetitors();
            
            compCtrl.loadOpportunities();
            
            List<Object> lst = compCtrl.loadMainCompetitors(acFather.ID);
            System.assert(lst.size() > 0);
            
            Test.stopTest();
            
        }
    }
        
}