public without sharing class TRANSFO_DisplayQteForCustomerController {
    
    public static  List<QuoteLine__c> genericQuoteLines = new  List<QuoteLine__c>();
    
    private static ID OpportunityID, QuoteID, wstID;
    public static Decimal QuoteTotalPrice {get; set;}
    
    
    
    @AuraEnabled
    public static Boolean CheckQuoteStatus(ID wasteInstanceId){
        
        WST_Waste_Statement__c wst = [Select Opportunity__r.SyncedQuote.Status FROM WST_Waste_Statement__c WHERE ID = : wasteInstanceId];
        
        if(wst.Opportunity__r.SyncedQuote.Status == 'Présenté'){
            return true;
        }else{
            return false;
        }
    }
    
    @AuraEnabled
    public static List<QuoteLine__c> getQuoteLineItems(ID wasteInstanceId){
        List<QuoteLine__c> results = new List<QuoteLine__c>();
        
        WST_Waste_Statement__c wst = [Select ID, Opportunity__c, Opportunity__r.SyncedQuoteID, Opportunity__r.SyncedQuote.GrandTotal, Opportunity__r.SyncedQuote.Status FROM WST_Waste_Statement__c WHERE ID = : wasteInstanceId];
        List<QuoteLineItem> qlis = [Select Id, ProductServiceLabel__c, Product2.ProductCode ,UnitPrice, 	Quantity, TotalPrice, Nature__c FROM QuoteLineitem WHERE QuoteID = : wst.Opportunity__r.SyncedQuoteID  ORDER BY Nature__c ASC ];
        OpportunityID = wst.Opportunity__c;
        QuoteID = wst.Opportunity__r.SyncedQuoteID;
        wstID = wasteInstanceId;
        
        QuoteTotalPrice = wst.Opportunity__r.SyncedQuote.GrandTotal;
        System.debug('Total : '+QuoteTotalPrice);
        
        for(QuoteLineItem qli : qlis){
            QuoteLine__c ql = new QuoteLine__c();
            ql.Label__c = qli.ProductServiceLabel__c;
            ql.Code__c = qli.Product2.ProductCode;
            ql.Quantity__c = qli.Quantity;
            ql.TECH_PriceBookEntryPrice__c = qli.UnitPrice;
            ql.TECH_PriceBookEntryTotalPrice__c = qli.TotalPrice;
            
            results.add(ql);
            
        }
        
        
        
        return results;
    }
    
    @AuraEnabled
    public static Decimal GetTotalPrice(ID wasteInstanceId){
        return [Select Opportunity__r.SyncedQuote.GrandTotal FROM WST_Waste_Statement__c WHERE ID = : wasteInstanceId].Opportunity__r.SyncedQuote.GrandTotal;
    }
    
    @AuraEnabled
    public static List<QuoteLine__c> ValidateQuote(ID wasteInstanceId){
        List<QuoteLine__c> results = new List<QuoteLine__c>();
        
        WST_Waste_Statement__c wst = [Select ID, Opportunity__r.SyncedQuoteID FROM WST_Waste_Statement__c WHERE ID = : wasteInstanceId];
        List<QuoteLineItem> qlis = [Select Id, ProductServiceLabel__c, Product2.ProductCode ,UnitPrice, 	Quantity, TotalPrice, Nature__c FROM QuoteLineitem WHERE QuoteID = : wst.Opportunity__r.SyncedQuoteID  ORDER BY Nature__c ASC ];
        
        for(QuoteLineItem qli : qlis){
            QuoteLine__c ql = new QuoteLine__c();
            
            
            results.add(ql);
            
        }
        
        return results;
    }
    
    @AuraEnabled
    public static String getQuoteType(ID wasteInstanceId){
        
        WST_Waste_Statement__c wst = [Select RecordType.DeveloperName FROM WST_Waste_Statement__c WHERE ID = : wasteInstanceId];
        
		return wst.RecordType.DeveloperName;
    }
    
    @AuraEnabled
    public static Boolean AcceptQuoteController(ID wasteInstanceId){
        WST_Waste_Statement__c wst = [Select ID, Status__c, Opportunity__c, Opportunity__r.SyncedQuoteID, Opportunity__r.SyncedQuote.GrandTotal FROM WST_Waste_Statement__c WHERE ID = : wasteInstanceId];
        Opportunity opp = [Select ID, StageName FROM Opportunity WHERE ID = : wst.Opportunity__c];
        Quote quo = [Select ID, Status FROM Quote WHERE ID = : wst.Opportunity__r.SyncedQuoteID];
        ID contactId = [Select ContactID FROM User WHERE ID =: UserInfo.getUserId()].ContactID;
        
        wst.Status__c = 'Devis accepte';
        wst.QuoteAcceptedDate__c = Date.today();
        wst.QuoteAcceptedBy__c = contactId;
        opp.StageName = 'Clôturée / Gagnée';
        quo.Status = 'Accepté';
        
        try{
            update wst;
            update opp;
            update quo;
        }catch(Exception e){
            return false;
        }
        
        
        return true;
    }
    
    @AuraEnabled
    public static Boolean DeclineQuoteController(ID wasteInstanceId){
        
        Account acc = [Select Id, Name From Account Where Name = 'NON COMMUNIQUÉ'];
        
        WST_Waste_Statement__c wst = [Select ID, Status__c, Opportunity__c, Opportunity__r.SyncedQuoteID, Opportunity__r.SyncedQuote.GrandTotal FROM WST_Waste_Statement__c WHERE ID = : wasteInstanceId];
        Opportunity opp = [Select ID, StageName FROM Opportunity WHERE ID = : wst.Opportunity__c];
        Quote quo = [Select ID, Status FROM Quote WHERE ID = : wst.Opportunity__r.SyncedQuoteID];
		
        opp.StageName = 'Clôturée / Perdue';
        opp.DateFinEngagement__c = Date.today();
        opp.Attributaire__c = acc.Id;
        quo.Status = 'Refusé';
        
        try{
            update opp;
            update quo;
        }catch(Exception e){
            System.debug('Exception e :'+e);
            return false;
        }
        
        
        return true;
    }
    
    @AuraEnabled
    public static Boolean SendToEliminationController(ID wasteInstanceId){
        System.debug('ICI');
       Boolean result = TRANSFO_ManageEliminationController.sendWasteInstanceToElimination(wasteInstanceId);
        
     
        
        return result;
    }
    
    
    
}