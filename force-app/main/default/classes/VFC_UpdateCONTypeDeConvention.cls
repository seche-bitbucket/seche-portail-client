public with sharing class VFC_UpdateCONTypeDeConvention {    
        public List<Contract__c> contractsList = new List<Contract__c>();
    	public String contractIds { get; set;}
        public Boolean isUpdatable { get; set;}
    	public String typeDeConventionValues{ get; set;}
        public String filiale{ get; set;}
        ApexPages.StandardSetController stdSetController;

       

        public  VFC_UpdateCONTypeDeConvention(ApexPages.StandardSetController controller ) {
            stdSetController = controller;
            isUpdatable = true;
            if(controller.getSelected().size()>0){
                this.contractsList=[SELECT Id, Name, Filiale__c, Filiere__c FROM Contract__c WHERE ID IN:controller.getSelected() ];                
                this.contractIds = getContractsId();
                this.typeDeConventionValues = getTypeDeConventionValues();
                this.filiale = contractsList[0].Filiale__c;
            }               
    }

    public String getTypeDeConventionValues(){
        List<Schema.PicklistEntry>  typeDeConventionPicklist = Contract__c.Type_de_convention__c.getDescribe().getPicklistValues();
		String typeDeConventionValues = '';
		for(Schema.PicklistEntry p : typeDeConventionPicklist){
		    typeDeConventionValues+= p.getValue();
            typeDeConventionValues+=',';
		}
        typeDeConventionValues.removeEnd(',');        
        return typeDeConventionValues;
        
    }
    
    public String getContractsId(){
        List<Id> ids = new  List<Id>();
        String idSerialize;
        for (Contract__c c : contractsList) {
            ids.add(c.Id);
        }      
        idSerialize = JSON.serialize(ids);
        return idSerialize;
    }
    public void updateCheck(){  
        	
            if(contractsList.size()>0){                
                String filiale = contractsList[0].Filiale__c;
                for (Contract__c c : contractsList) {
                if(c.get('Filiale__c')!= filiale){
                    isUpdatable = false;
                    system.debug('IsUpdatable'+isUpdatable);
                } 
                if(!isUpdatable){ 
                system.debug('IsUpdatable 2 '+isUpdatable);
                ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Le type de convention ne peut pas être mis à jour, les contrats selectionnés  n\'ont pas la même filiale');
                ApexPages.addMessage(errorMessage);
            }                   
            }
            }else{                      
                isUpdatable = false;
                system.debug('IsUpdatable 3'+isUpdatable);
                ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Veuillez selectionner au moins un contract');
                ApexPages.addMessage(errorMessage);
            }
    }
    
    public pageReference returnBack()
	{
		return stdSetController.cancel();
	}

       

    }