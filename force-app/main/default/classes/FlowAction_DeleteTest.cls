@isTest(SeeAllData=false)
public with sharing class FlowAction_DeleteTest {
    
    @isTest
    public static void test() {

        List<Account> accs = new List<Account>{new Account(Name = 'Test 1 Test 1'), new Account(Name='SET 1 SAT 1')};
        insert accs;
        
        FlowAction_Delete.Parameter param = new FlowAction_Delete.Parameter();
        param.objectApiName = 'Account';

        List<String> idsList = new List<String>();

        idsList.add(accs[0].Id);

        param.listValues = idsList;

        FlowAction_Delete.deleteRecords(new List<FlowAction_Delete.Parameter>{param});
    }
}