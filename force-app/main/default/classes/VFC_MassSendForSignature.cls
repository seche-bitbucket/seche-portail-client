/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-11-09
* @modified       2018-11-09
* @systemLayer    Utility         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Controller class 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class VFC_MassSendForSignature {
    public Set<Quote> QuotesToSend {get;set;}
    public List<Quote> QuotesWithError{get;set;}
    public List<Quote> QuotesSent{get;set;}
    public Boolean Error{get;set;}
    public Boolean Sent{get;set;}
    public VFC_MassSendForSignature(ApexPages.StandardSetController controller){
        this.Error=false;
        this.Sent=false;
        this.QuotesWithError=new List<Quote>();
        this.QuotesSent=new List<Quote>();
        this.QuotesToSend=new Set<Quote>();
        if(controller.getSelected().size()>=1){
            List<Quote> Quotes=[SELECT id,Email,TECH_UID__c, Name,Opportunity.Salesman__c,ContactId,(SELECT ID,Name FROM Attachments) FROM Quote Where ID IN:controller.getSelected()];
            for(quote q:Quotes){
                if(q.Attachments.size()==1 && q.Opportunity.Salesman__c!=Null && q.ContactId!=Null && q.Email!=Null ){
                    QuotesTosend.add(q);
                }else{
                    QuotesWithError.add(q);
                    this.Error=true;
                }
                if(q.TECH_UID__c!=null){
                    QuotesSent.add(q);
                    this.Sent=true;
                }
            }
        }else{
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Veuillez selectionner un devis');
            ApexPages.addMessage(errorMessage);
        }
    }
    public PageReference Init(){
        PageReference returnPage;
        if(!this.Error && !this.Sent){
            for(Quote q:This.QuotesTosend){
                q.TECH_MassSignature__c=true;
                q.TECH_Assistante__c=UserInfo.getUserId();
            }
            Update  new list<Quote>(This.QuotesToSend);
            MassSendSignatureBatch sendSignature = new MassSendSignatureBatch();
            Id batchId = Database.executeBatch(sendSignature,10);
            ReturnPage= new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        }else{
            returnPage=Null;
        }
        return returnPage;
    }
}