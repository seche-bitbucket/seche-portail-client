public with sharing class csvDownloadCtrl {  
   
@AuraEnabled
   public static list <FIP_FIP__c> fetchFip(String [] selectedFipIds){
      System.debug('Start -- fetchFip : '+selectedFipIds);
      List <FIP_FIP__c> returnConList = new List < FIP_FIP__c >();
        
      for(FIP_FIP__c fip: [Select Id, FIPType__c, PreviousPACNumber__c,Exutoire__c, ContractRoot__c, AdressCode__c, TECH_Subsidiary_Key__c, AcceptedBy__r.Trigramme__c, PlateformeTransit__c,
                WasteName__c, WasteNamingCode__c, AnnualQuantity__c, AsbestosType__c, PackagingType__c, UNCode__c, Class__c, PackagingGroup__c,
                Collector__r.Name, Collector__r.BillingStreet, Collector__r.BillingPostalCode, Collector__r.BillingCity, CreatedBy.FirstName, CreatedBy.LastName, CreatedBy.Phone, CreatedBy.Email, Collector__r.SIRET_Number__c,
                ProductorBusinessName__c, ProductorCompanyIdentificationSystem__c, ProductorAdress1__c, ProductorAdress2__c, ProductorAdress3__c,ProductorPostalCode__c, ProductorCity__c, ConstructionSiteAdress__c,
                ChargedBusinessName__c, ChargedCompanyIdentificationSystem__c, ChargedAdress__c, ChargedPostalCode__c, ChargedCity__c, FrenchTaxeID__c,
                ProductorX3Code__c, ProductorX3AddressCode__c, ChargedX3Code__c, ChargedX3AddressCode__c, AskerCodeX3__c, AskerX3AddressCode__c, ContractNumber__c
                FROM FIP_FIP__c WHERE id in : selectedFipIds ]) {
             returnConList.add(fip);
          }
          System.debug('End -- fetchFip : '+returnConList);
         return returnConList;
   }

@AuraEnabled
   public static String generateCSV(List<FIP_FIP__c> fips){

	String csv = '"FIP_ID";"Type_amiante";"Cap_renouv";"Racine_Contrat";"Code_tiers_X3";"Code_Adresse";"Appelation_usuelle";"Code_CEE";"Quantite_annuelle";"Type_amiante";"Type_condit";"Code_ONU";"Classe";"Groupe_emb";"FACT_raison_soc";"FACT_adresse";"FACT_CP";"FACT_Ville";"FACT_Prenom_Contact";"FACT_Nom_Contact";"FACT_Telephone";"FACT_Siret";"PROD_raison_soc";"PROD_Siret";"PROD_adresse1";"PROD_adresse2";"PROD_adresse3";"PROD_CP";"PROD_Ville";"Adresse_Chantier";"FACT_Id_TVA_FR";"FACT_CODEX3";"FACT_Code_Adresse";"PROD_CODEX3";"PROD_Code_Adresse";"DEM_CodeX3";"DEM_Code_Adresse";"Filiale_Fac";"APPROB_Exploit";"Plateforme";"Exutoire";"Contrat_PLT"';
	csv += '\n';
	for(FIP_FIP__c fip : fips) {

		String tmp = '"'+fip.ID+ '";"';
		tmp += fip.FIPType__c+ '";"';
		tmp += fip.PreviousPACNumber__c+ '";"';
		tmp += fip.ContractRoot__c+ '";"';
		tmp += fip.ChargedX3Code__c+ '";"';
		tmp += fip.AdressCode__c+ '";"';
		tmp += fip.WasteName__c+ '";"';
		tmp += fip.WasteNamingCode__c+ '";"';
		tmp += fip.AnnualQuantity__c+ '";"';
		tmp += fip.AsbestosType__c+ '";"';
		tmp += fip.PackagingType__c+ '";"';
		tmp += fip.UNCode__c+ '";"';
		tmp += fip.Class__c+ '";"';
		tmp += fip.PackagingGroup__c+ '";"';
		tmp += checkReturnLine(fip.Collector__r.Name)+ '";"';
		tmp += checkReturnLine(fip.Collector__r.BillingStreet)+ '";"';
		tmp += fip.Collector__r.BillingPostalCode+ '";"';
		tmp += checkReturnLine(fip.Collector__r.BillingCity)+ '";"';
		tmp += fip.CreatedBy.FirstName+ '";"';
		tmp += fip.CreatedBy.LastName+ '";"';
		tmp += fip.CreatedBy.Phone+ '";"';
		tmp += fip.Collector__r.SIRET_Number__c+ '";"';
		tmp += checkReturnLine(fip.ProductorBusinessName__c)+ '";"';
		tmp += fip.ProductorCompanyIdentificationSystem__c+ '";"';
		tmp += checkReturnLine(fip.ProductorAdress1__c)+ '";"';
		tmp += checkReturnLine(fip.ProductorAdress2__c)+ '";"';
		tmp += checkReturnLine(fip.ProductorAdress3__c)+ '";"';
		tmp += fip.ProductorPostalCode__c+ '";"';
		tmp += fip.ProductorCity__c+ '";"';
		tmp += fip.ConstructionSiteAdress__c+ '";"';
		tmp += fip.FrenchTaxeID__c+ '";"';
		tmp += fip.ChargedX3Code__c+ '";"';
		tmp += fip.ChargedX3AddressCode__c+ '";"';
		tmp += fip.ProductorX3Code__c+ '";"';
		tmp += fip.ProductorX3AddressCode__c+ '";"';
		tmp += fip.AskerCodeX3__c+ '";"';
		tmp += fip.AskerX3AddressCode__c+ '";"';
		tmp += fip.TECH_Subsidiary_Key__c+ '";"';
		tmp += fip.AcceptedBy__r.Trigramme__c+ '";"';
		tmp += fip.PlateformeTransit__c+ '";"';
		tmp += fip.Exutoire__c+ '";"';
		tmp += fip.ContractNumber__c+ '";';
		tmp += '\n';

		csv += tmp;
	}
    if(csv!=null){
     updateFIPList(fips);
    }
  
	return csv;
}

public static void updateFIPList(List<FIP_FIP__c> fips){
	List<FIP_FIP__c> fipsToUpdate = new List<FIP_FIP__c>();

	for(FIP_FIP__c fip : fips) {
		fip.Is_Exported__c = true;
		fipsToUpdate.add(fip);
	}

	update fips;
}

public static String checkReturnLine(String pStringToManage){
	String result = '';
	if(pStringToManage != null ) {
		if(pStringToManage.contains('\n')) {
			result = pStringToManage.replace('\n', ' ');
		}else if(pStringToManage.contains('\r\n')) {
			result = pStringToManage.replace('\r\n', ' ');
		}else{
			result = pStringToManage;
		}
	}


	return result;

}

@AuraEnabled
public static List<ListView> getListViews() {
    List<ListView> listviews =
        [SELECT Id, Name FROM ListView WHERE SobjectType = 'FIP_FIP__c'];

    // Perform isAccessible() check here
    return listviews;
}
}