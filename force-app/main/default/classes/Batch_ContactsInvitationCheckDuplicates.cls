/*
* ------------------------------------------------------------------------------------------------------------------
* -- Name : Batch_ContactsInvitationCheckDuplicates
* -- Author : PMB
* -- Company : Capgemini
* -- Purpose : Batch_ContactsInvitationCheckDuplicates (Batch to check duplicates on Contacts_Invitation__c)
* --
* -- Date       Name Version Remarks
* -- ---------- ---- ------- -------------------------------------------------------------------------------------
* -- 06-05-2021 AID    1.0   - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
global class Batch_ContactsInvitationCheckDuplicates implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful{

    global List<Contacts_Invitation__c> lst {get; set;}
    
    global Database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(
            'SELECT Id, Email__c, IsDuplicate__c FROM Contacts_Invitation__c WHERE IsDuplicate__c = false AND Soiree__c = true ORDER BY Email__c'
        );
    }

    global void execute(Database.BatchableContext BC, List<Contacts_Invitation__c> scope) {
       
        this.lst = scope;
        
        Map<String, List<Contacts_Invitation__c>> mapCi2Email = new Map<String, List<Contacts_Invitation__c>>();
        
        for(Contacts_Invitation__c ci :lst) {
            
            if(mapCi2Email.containsKey(ci.Email__c)) {
                List<Contacts_Invitation__c> tmp = new List<Contacts_Invitation__c>();
                tmp = mapCi2Email.get(ci.Email__c);
                tmp.add(ci);
                mapCi2Email.put(ci.Email__c, tmp);
            } else {
                mapCi2Email.put(ci.Email__c, new List<Contacts_Invitation__c>{ci});
            }
            
        }
        
        system.debug(mapCi2Email);
                                
        Set<Contacts_Invitation__c> duplicates = new Set<Contacts_Invitation__c>();
                                
        for(String email: mapCi2Email.keySet()) {
                                    
            if(mapCi2Email.get(email).size() >= 2) {
                duplicates.addAll(mapCi2Email.get(email));
            }
                                    
        }
        
        for(Contacts_Invitation__c ci: duplicates) {
            ci.IsDuplicate__c = true;
        }
        
        update new List<Contacts_Invitation__c>(duplicates);
            
    }

    global void finish(Database.BatchableContext BC){ }
}