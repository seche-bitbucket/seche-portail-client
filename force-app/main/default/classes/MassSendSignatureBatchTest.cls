/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-11-13
* @modified       2018-11-13
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for Batch that sends the signature
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class MassSendSignatureBatchTest {
    @isTest static void testBatchExecute(){
    Profile directp = [select id from profile where name = :'Directeur commercial' limit 1];
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = directp.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        
        insert director;
        Profile p = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];
        
        String testemail = 'Commercial-_User_test@test.com';
        User Commercial = new User(profileId = p.id, username = testemail, email = testemail,
                                   emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                   languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                   alias='cs', lastname='lastname2', IsActive=true);
        insert Commercial;
        
        
        System.RunAs(Commercial) {
            Account a = new Account();
            a.Name = 'testAccName';
            a.CustomerNature__c = 'Administration';
            a.Industry = 'Trader';
            a.CurrencyIsoCode = 'EUR';
            a.Producteur__c = false;
            a.BillingPostalCode = '01000';
            a.NAF_Number__c = '1234A';
            insert a;
            
            Contact c = new Contact();
            c.LastName = 'testName';
            c.AccountId = a.Id;
            c.Salesman__c = director.Id;
            c.Email = 'aaa@groupe-seche.com';
            c.Phone = '03 00 00 00 00';
            insert c;
            Opportunity opp = new Opportunity();
            opp.Name = 'Mon OPP TEST';
            opp.StageName = 'Contractualisation';
            opp.CloseDate = System.today().addDays(10);
            opp.AccountId = a.Id;
            opp.TonnageN__c = 0;
            opp.Filiale__c = 'Tredi Hombourg';
            opp.Filiere__c = 'Physico';
            opp.ContactName__c = c.Id;
            opp.Salesman__c=director.Id;
            insert opp;
            
            Quote q = new Quote();
            q.OpportunityId = opp.ID;
            q.Name = opp.Name;
            q.DateDevis__c = Date.today();
            q.QuoteCode__c=opp.Name;
            q.ContactId = opp.ContactName__c;
            q.Email = 'secheemail@gmail.com';
            q.Phone = opp.ContactName__r.Phone;
            q.Fax = opp.ContactName__r.Fax;
            q.TECH_UID__c='9001';
            q.BillingStreet = opp.Account.BillingStreet;
            q.BillingCity = opp.Account.BillingCity;
            q.BillingState = opp.Account.BillingState;
            q.BillingPostalCode = opp.Account.BillingPostalCode;
            q.BillingCountry = opp.Account.BillingCountry;
            q.TECH_Assistante__c=Commercial.Id;
            q.TECH_MassSignature__c=true;
            insert q;
            Attachment att = new Attachment();
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            att.body=bodyBlob;
            att.Name = q.Name+'- Devis.pdf';	
            att.ParentId = q.Id;             
            insert att; 
            Test.startTest();
            MassSendSignatureBatch batch = new MassSendSignatureBatch(); 
            Id batchId = Database.executeBatch(batch);
            Test.stopTest();
            q.Name='Convention 2019'+opp.Name;
            Update q;
            MassSendSignatureBatch batch2 = new MassSendSignatureBatch(); 
            Id batchId2 = Database.executeBatch(batch2);
            Quote quote=[SELECT id, Name,ContactId,Opportunity.Salesman__c,Opportunity.Owner.Email,TECH_isApprouved__c,Status,TECH_MassSignature__c,
                         Opportunity.Owner.Name,Email, QuoteCode__c,TECH_UID__c,Opportunity.Filiale__c,Account.Name,TECH_Assistante__c FROM Quote LIMIT 1];
            System.assertEquals(true, quote.Tech_UID__c!=Null);
            System.assertEquals(true, quote.TECH_isApprouved__c);
            System.assertEquals('Envoyé pour signature', quote.Status);            
        }
    }
}