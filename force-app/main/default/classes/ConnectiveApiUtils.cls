public with sharing class ConnectiveApiUtils {
    
    //create a generic request with content type = json and timemout = 120000
    public static HttpRequest createRequest(String endpoint, String method){
        return createRequest(endpoint, method, 'application/json', 120000);
    }

    //create a request using infos
    public static HttpRequest createRequest(String endpoint, String method, String contentType, Integer timeout){
        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type', contentType);
        req.setEndpoint(endpoint);
        req.setMethod(method);
        req.setTimeout(timeout);
        return req;
    }

    //used for Client stakeholder
    //Uses information from a contact to create a generic 'Signer' type stakeholder
    //locations id are also required for the Signer type, they are retrieved from addDocumentToPackage
    public static PackageWrapper.Stakeholder getSignerStakeholderFromContact(Contact contct, List<String> locIdLs){
        PackageWrapper.Stakeholder sth = new PackageWrapper.Stakeholder();
        String firstName = '';
        if(contct.FirstName==null){
            firstName='DefaultFirstName';
        }else{
            firstName=contct.FirstName; 
        }
        
        sth.FirstName = firstName;
        sth.LastName = contct.LastName;
        sth.EmailAddress = contct.Email;
        sth.ExternalStakeholderReference = contct.Id;
        sth.Language = 'fr';
        sth.Type = 'Person';
        sth.SendNotifications = true;

        PackageWrapper.Actor act = new PackageWrapper.Actor();
        act.Type = 'Signer';
        act.OrderIndex = 1;
        //act.Phonenumber = '';
        act.SendNotifications = true;
        act.RedirectURL = 'https://www.groupe-seche.com/fr'; //TODO
        act.RedirectType = 'AfterCompletion';
        act.LocationIds = locIdLs;

        PackageWrapper.SigningField sf = new PackageWrapper.SigningField();
        sf.MarkerOrFieldId = Label.ConnectiveSigningFieldId;

        PackageWrapper.SigningType st = new PackageWrapper.SigningType();
        st.SigningType = 'manual';

        List<PackageWrapper.SigningField> sfLs = new List<PackageWrapper.SigningField>();
        sfLs.add(sf);
        List<PackageWrapper.SigningType> stLs = new List<PackageWrapper.SigningType>();
        stLs.add(st);

        act.SigningFields = sfLs;
        act.SigningTypes = stLs;
        List<PackageWrapper.Actor> actLs = new List<PackageWrapper.Actor>();
        actLs.add(act);
        sth.Actors = actLs;

        return sth;
    }

    //used for SF stakeholder
    //create a generic 'Receiver' stakeholder
    public static PackageWrapper.Stakeholder getReceiverStakeholder(){
        PackageWrapper.Stakeholder sth = new PackageWrapper.Stakeholder();
        sth.FirstName = 'Salesforce';
        sth.LastName = 'Salesforce';
        sth.EmailAddress = 'sechecrm69@gmail.com';
        sth.Language = 'fr';
        sth.Type = 'Person';
        sth.SendNotifications = true;

        PackageWrapper.Actor act = new PackageWrapper.Actor();
        act.Type = 'Receiver';
        act.SendNotifications = true;
        List<PackageWrapper.Actor> actLs = new List<PackageWrapper.Actor>();
        actLs.add(act);
        sth.Actors = actLs;

        return sth;
    }

    //used for Sales stakeholder
    //create a generic 'Receiver' stakeholder from the sales User
    public static PackageWrapper.Stakeholder getReceiverStakeholderFromUser(User usr){
        PackageWrapper.Stakeholder sth = new PackageWrapper.Stakeholder();
        sth.FirstName = usr.FirstName;
        sth.LastName = usr.LastName;
        sth.EmailAddress = usr.Email ;
        sth.Language = 'fr';
        sth.Type = 'Person';
        sth.SendNotifications = true;

        PackageWrapper.Actor act = new PackageWrapper.Actor();
        act.Type = 'Receiver';
        act.SendNotifications = true;
        List<PackageWrapper.Actor> actLs = new List<PackageWrapper.Actor>();
        actLs.add(act);
        sth.Actors = actLs;

        return sth;
    }

    //create an empty package and returns the Id created from connective
    public static String createPackage(String packageName){
        Http http = new Http();
        HttpRequest req = createRequest('callout:ConnectiveApiV3/packages', 'POST');

        Map<String, String> body = new Map<String, String>();
        body.put('Initiator',System.Label.Connective_Initiator);
        body.put('PackageName', packageName);
        body.put('ReassignEnabled', 'true');
        req.setBody(JSON.serialize(body));
        try{
            HttpResponse res = http.send(req);
            if(res.getStatusCode() == 201){
                Map<String, String> resBody = (Map<String, String>)JSON.deserialize(res.getBody(), Map<String,String>.class);
                String packId = resBody.get('PackageId');
                return packId;
            }else{
                Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiUtils.class.getName(),
                    ErrorMessage__c='request : '+ req.getBody() + ' response :' + res.getBody(),
                    ExceptionType__c='HTTP'
                );
                insert error;
                return null;
            }
        }
        catch(Exception ex){
            Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiUtils.class.getName(),
                    ErrorMessage__c=ex.getMessage(),
                    ExceptionType__c='Error APEX'
                );
            insert error;
            return null;
        }
    }

    //add a document to a connective package using package Id and ContentVersion document
    //returns Connective Id of the document
    public static Map<String, Object> addDocumentToPackage(String packageId, ContentVersion cv){
        Http http = new Http();
        HttpRequest req = createRequest('callout:ConnectiveApiV3/packages/'+packageId+'/documents', 'POST');

        PackageWrapper bodyWrap = new PackageWrapper();
        //set the document
        bodyWrap.Document = EncodingUtil.base64Encode(cv.VersionData);
        //set document infos
        bodyWrap.DocumentLanguage = 'fr';
        bodyWrap.DocumentName = cv.Title.substringBefore('.');
        bodyWrap.ExternalDocumentReference = cv.Id;
        bodyWrap.TargetType = 'PdfA2A';
        bodyWrap.PdfErrorHandling = 'DetectFixFail';
        
        //create signing fields
        PackageWrapper.SigningField signField = new PackageWrapper.SigningField();
        signField.MarkerOrFieldId = Label.ConnectiveSigningFieldId;
        List<PackageWrapper.SigningField> signFieldLs = new List<PackageWrapper.SigningField>();
        signFieldLs.add(signField); // a list is needed for api call
        bodyWrap.SigningFields = signFieldLs;

        req.setBody(JSON.serialize(bodyWrap));
        try{
            HttpResponse res = http.send(req);
            if(res.getStatusCode() == 200){
                Map<String, Object> mapRes = new Map<String, Object>();
                RespAddDoc resWrap = (RespAddDoc)JSON.deserialize(res.getBody(), RespAddDoc.class);
                List<String> locIdLs = new List<String>();
                for (RespLocation loc: resWrap.Locations){
                    locIdLs.add(loc.Id);
                }
                mapRes.put('docId', resWrap.DocumentId);
                mapRes.put('locIdLs', locIdLs);
                return mapRes;
            }else{
                Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiUtils.class.getName(),
                    ErrorMessage__c='request : '+ req.getBody() + ' response :' + res.getBody(),
                    ExceptionType__c='HTTP'
                );
                insert error;
                return null;
            }
        }
        catch(Exception ex){
            Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiUtils.class.getName(),
                    ErrorMessage__c=ex.getMessage(),
                    ExceptionType__c='Error APEX'
                );
            insert error;
            return null;
        }
    }

    //add a document to a connective package using package Id and StaticResource document
    //returns Connective Id of the document
    public static Map<String, Object> addStaticToPackage(String packageId, StaticResource sr){
        Http http = new Http();
        HttpRequest req = createRequest('callout:ConnectiveApiV3/packages/'+packageId+'/documents', 'POST');

        PackageWrapper bodyWrap = new PackageWrapper();
        //set the document
        bodyWrap.Document = EncodingUtil.base64Encode(sr.Body);
        //set document infos
        bodyWrap.DocumentLanguage = 'fr';
        bodyWrap.DocumentName = sr.Name;
        bodyWrap.ExternalDocumentReference = sr.Id;
        bodyWrap.TargetType = 'PdfA2A';
        bodyWrap.PdfErrorHandling = 'DetectFixFail';
        
        //create signing fields
        PackageWrapper.SigningField signField = new PackageWrapper.SigningField();
        signField.MarkerOrFieldId = Label.ConnectiveSigningFieldId;
        List<PackageWrapper.SigningField> signFieldLs = new List<PackageWrapper.SigningField>();
        signFieldLs.add(signField); // a list is needed for api call
        bodyWrap.SigningFields = signFieldLs;

        req.setBody(JSON.serialize(bodyWrap));
        try{
            HttpResponse res = http.send(req);
            if(res.getStatusCode() == 200){
                Map<String, Object> mapRes = new Map<String, Object>();
                RespAddDoc resWrap = (RespAddDoc)JSON.deserialize(res.getBody(), RespAddDoc.class);
                List<String> locIdLs = new List<String>();
                for (RespLocation loc: resWrap.Locations){
                    locIdLs.add(loc.Id);
                }
                mapRes.put('docId', resWrap.DocumentId);
                mapRes.put('locIdLs', locIdLs);
                return mapRes;
            }else{
                System.debug('api error');
                System.debug(JSON.serializePretty(res.getBody()));
                Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiUtils.class.getName(),
                    ErrorMessage__c=' response :' + res.getBody(),
                    ExceptionType__c='HTTP'
                );
                insert error;
                return null;
            }
        }
        catch(Exception ex){
            System.debug('apex error');
            Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiUtils.class.getName(),
                    ErrorMessage__c=ex.getMessage(),
                    ExceptionType__c='Error APEX'
                );
            insert error;
            return null;
        }
    }

    // call the process api url to set package signers informations
    //returns true / false depending on success
    //needs packageId, quoteId and list of locations id as paremeters
    public static Boolean processPackage(String packageId, Id quoteId, List<String> locIdLs){
        Http http = new Http();
        HttpRequest req = createRequest('callout:ConnectiveApiV3/packages/'+packageId+'/process', 'PUT');

        PackageWrapper bodyWrap = new PackageWrapper();
        Contact contct = (Contact)PackageWrapper.retrieveContact(quoteId);
        PackageWrapper.Stakeholder sthClient = getSignerStakeholderFromContact(contct, locIdLs);

        //PackageWrapper.Stakeholder sthSF = getReceiverStakeholder();
        //User usrSales = PackageWrapper.retrieveSalesman(quoteId);
        //PackageWrapper.Stakeholder sthSalesman = getReceiverStakeholderFromUser(usrSales);
        List<PackageWrapper.Stakeholder> stakeLs = new List<PackageWrapper.Stakeholder>{sthClient/*,sthSF,sthSalesman*/};

        bodyWrap.Stakeholders = stakeLs;

        req.setBody(JSON.serialize(bodyWrap));
        try{
            HttpResponse res = http.send(req);
            if(res.getStatusCode() == 200){
                return true;
            }else{
                Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiUtils.class.getName(),
                    ErrorMessage__c='request : '+ req.getBody() + ' response :' + res.getBody(),
                    ExceptionType__c='HTTP'
                );
                insert error;
                return false;
            }
        }
        catch(Exception ex){
            Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiUtils.class.getName(),
                    ErrorMessage__c=ex.getMessage(),
                    ExceptionType__c='Error APEX'
                );
            insert error;
            return false;
        }
    }

    // call the set status api url to set package status, it will trigger mail and actions for actors
    //returns true / false depending on success
    //needs packageId and status as parameters
    public static Boolean setPackageStatus(String packageId, String status){
        Http http = new Http();
        HttpRequest req = createRequest('callout:ConnectiveApiV3/packages/'+packageId+'/status', 'PUT');

        Map<String, String> bodyWrap = new Map<String, String>{
            'Status'=>status
        };

        req.setBody(JSON.serialize(bodyWrap));
        try{
            HttpResponse res = http.send(req);
            if(res.getStatusCode() == 200){
                return true;
            }else{
                Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiUtils.class.getName(),
                    ErrorMessage__c='request : '+ req.getBody() + ' response :' + res.getBody(),
                    ExceptionType__c='HTTP'
                );
                insert error;
                return false;
            }
        }
        catch(Exception ex){
            Log__c error = new Log__c(
                    ApexJob__c= ConnectiveApiUtils.class.getName(),
                    ErrorMessage__c=ex.getMessage(),
                    ExceptionType__c='Error APEX'
                );
            insert error;
            return false;
        }
    }

    //set status to Pending
    public static Boolean setPackagePending(String packageId){
        return setPackageStatus(packageId, 'Pending');
    }

    //set status to Revoked
    public static Boolean setPackageRevoked(String packageId){
        return setPackageStatus(packageId, 'Revoked');
    }

    //parse a get status response into a list of documents for signature
    //need the body of the response, packageId and quoteId as parameters
    public static List<DocumentsForSignature__c> parseStatusRespToDocList(PackageResponse bodyRes, String packId, String quoteId){
        List<DocumentsForSignature__c> res = new List<DocumentsForSignature__c>();

        String quoteCode = [SELECT QuoteCode__c FROM Quote WHERE Id = :quoteId].QuoteCode__c;
        List<PackageResponse.ResponsePackageDocument> docLs = bodyRes.PackageDocuments;
        for (PackageResponse.ResponsePackageDocument docResp: docLs){
            DocumentsForSignature__c doc = new DocumentsForSignature__c();
            doc.Name = docResp.DocumentName;
            if (doc.Name.contains('CGV')){
                doc.Name += ' ' + quoteCode;
            }
            doc.SigningUrl__c = bodyRes.F2FSigningURL;
            doc.SentTo__c = PackageResponse.getFirstSignerId(bodyRes);
            doc.TECH_ExternalId__c = docResp.ExternalDocumentReference;
            doc.ConnectiveDocumentId__c = docResp.DocumentId;
            doc.Status__c = bodyRes.PackageStatus.toUpperCase();
            doc.Quote__c = quoteId;
            doc.Package__c = packId;
            res.add(doc);
        }
        
        return res;
    }

    //multiple api calls to create and package and setup documents and actors on it
    //returns the connective package id if success
    public static String setupPackFromQuoteId(String quoteId){
        ContentDocumentLink cdl = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =:quoteId LIMIT 1];
        ContentVersion cv = [SELECT PathOnClient, ContentLocation, Origin, OwnerId, Title, 
                                VersionData, ContentDocumentId, CreatedDate
                            FROM ContentVersion 
                            WHERE ContentDocumentId = :cdl.ContentDocumentId ORDER BY CreatedDate DESC LIMIT 1];
        String packId = ConnectiveApiUtils.createPackage(cv.Title.substringBefore('.')); //use substring cause it's the old way to do (mandatory ?)
        System.debug('packId = ' + packId);
        Map<String,Object> addDocRes = ConnectiveApiUtils.addDocumentToPackage(packId, cv); //add linked document from quote
        List<String> locIdLs = new List<String>();
        locIdLs.addAll((List<String>)addDocRes.get('locIdLs'));

        //get the type and if it is not a Convention then upload the Static as well
        Quote qte = [SELECT Id, Quote_Type__c, Opportunity_Filiale__c FROM Quote WHERE Id = :quoteId];
        if (qte.Quote_Type__c == 'Proposition commerciale'){
            String cgvFileName = getStaticResourceNameFromFiliale(qte.Opportunity_Filiale__c);
            StaticResource sr = [SELECT Id, Name, Body FROM StaticResource WHERE Name=:cgvFileName]; 
            Map<String,Object> addStRes = ConnectiveApiUtils.addStaticToPackage(packId, sr); //add the static resource CGV from static resource
            locIdLs.addAll((List<String>)addStRes.get('locIdLs'));
        }

        Boolean successProcess = ConnectiveApiUtils.processPackage(packId, quoteId, locIdLs); //set signers
        if (successProcess == true){
            return packId;
        }
        return null;
    }

    //use the metadata to get the correct static resource for the linked Filiale
    public static String getStaticResourceNameFromFiliale(String filiale){
        List<CGVFile__mdt> cgvMdtLs = [SELECT Id, FileName__c, Filiale__c FROM CGVFile__mdt WHERE Filiale__c = :filiale OR Filiale__c = 'Default'];
        if (cgvMdtLs.size() == 0){
            //no medata found, default should at least be created
            Log__c error = new Log__c(
                ApexJob__c= ConnectiveApiUtils.class.getName(),
                ErrorMessage__c='Missing metadata Default',
                ExceptionType__c='Error Metadata CGVFile__mdt'
            );
            insert error;
            return null;
        }
        else if (cgvMdtLs.size() == 1){
            //only default then return that
            return cgvMdtLs[0].FileName__c;
        }
        else {
            //default + at least, then get the first equal to filiale name
            for (CGVFile__mdt cgvFile : cgvMdtLs){
                if (cgvFile.Filiale__c == filiale){ 
                    return cgvFile.FileName__c;
                }
            }
            //return statement if somehow everything goes wrong
            //should never reach here
            return cgvMdtLs[0].FileName__c;
        }
    }

    //set all status from a quote id
    //it will get the package status from connective then check docs and quote to update them
    //also download docs and send a mail per package
    public static Boolean updateAllStatus(String quoteId){
        Boolean res = false;
        List<DocumentsForSignature__c> quoteDocLs = [
            SELECT  Id, ConnectiveDocumentId__c, Status__c, Package__c 
            FROM DocumentsForSignature__c
            WHERE Quote__c = :quoteId
        ];
        Set<Id> packIdLs = new Set<Id>();
        for (DocumentsForSignature__c doc: quoteDocLs){
            packIdLs.add(doc.Package__c);
        }
        List<Package__c> quotePackLs = [
            SELECT Id,ConnectivePackageId__c,ConnectivePackageStatus__c
            FROM Package__c
            WHERE Id in :packIdLs
        ];
        //create a map of documents linked to a package
        Map<Package__c, List<DocumentsForSignature__c>> mapPackDoc = new Map<Package__c, List<DocumentsForSignature__c>>(); 
        for (Package__c pack : quotePackLs){
            List<DocumentsForSignature__c> docLs = new List<DocumentsForSignature__c>();
            for (DocumentsForSignature__c doc: quoteDocLs){
                if (doc.Package__c == pack.Id){
                    docLs.add(doc);
                }
            }
            mapPackDoc.put(pack, docLs);
        }
        //then use that map to go through each package and update the quote with linked documents
        //there is supposed to be only one package on the quote
        String qStatus = '';
        Boolean qESigned = false;
        String reason ='';
        List<DocumentsForSignature__c> docToUpd = new List<DocumentsForSignature__c>(); //list for soql update
        List<DocumentsForSignature__c> docToDownload = new List<DocumentsForSignature__c>(); //list for document download from batch
        List<Package__c> packToUpd = new List<Package__c>();
        for (Package__c pack: mapPackDoc.keySet()){
            String bodyStatus = ConnectiveApiManager.getStatusByConnectivePackageId(pack.ConnectivePackageId__c);				
            if (String.isNotBlank(bodyStatus)){
                res = true;
            }
            PackageResponse bodyRes = PackageResponse.deserializeResponsejson(bodyStatus);
            String status = bodyRes.PackageStatus;
            List<PackageResponse.ResponseActor> lstActor = new List<PackageResponse.ResponseActor>();
            for(PackageResponse.ResponseStakeholder sth : bodyRes.Stakeholders){
                lstActor.addAll(sth.Actors);
            }
            //setup the variables documents have in common 
            String urlDownload ='';
            for(PackageResponse.ResponseActor act : lstActor){
                if(act.Type == 'Receiver'){
                    urlDownload =  act.ActionUrl;
                }
                if(act.Type == 'Signer'){
                    reason = act.Reason;
                }
            }
            if(status == 'FINISHED'){
                qStatus = 'Signé';
                qESigned = true;
            }
            else if(status == 'Rejected'){
                qStatus = 'Refusé';
            }
            //update documents
            for (DocumentsForSignature__c doc: mapPackDoc.get(pack)){
                doc.Status__c = status;
                doc.DownloadSignedURL__c = urlDownload;
                doc.ReasonRejected__c = reason;
                docToUpd.add(doc);
                if (status == 'FINISHED'){
                    docToDownload.add(doc);
                }
            }

            if(docToDownload.size()>0){
                Batch_Connective_Get_Document batch = new Batch_Connective_Get_Document(docToDownload,null);
                system.enqueueJob(batch);
            }
            pack.ConnectivePackageStatus__c = status; //set package status as well
            packToUpd.add(pack);
        }
        //update the quote status + the list of documents
        if(String.isNotBlank(qStatus)){
            ConnectiveApiManager.updateQuoteStatus(quoteId,qStatus,qESigned);
            update docToUpd;
            update packToUpd;
            //prepare and send the email
            List<Messaging.SingleEmailMessage> msgLs = new List<Messaging.SingleEmailMessage>();
            Quote qt = [SELECT Id,Name,NameCustomer__c,Opportunity.Salesman__r.Email,
                                Opportunity.Salesman__r.Assistant__r.Email, Quote_Type__c,QuoteNumber 
                        FROM Quote 
                        WHERE Id =:quoteId];
            Messaging.SingleEmailMessage msg = ConnectiveApiManager.generateContentEmail(qt,qStatus,new DocumentsForSignature__c(ReasonRejected__c=reason));
            msgLs.add(msg);
            Messaging.sendEmail(msgLs);
        }
        return res;
    }

    //inner classes for parsing
    public class RespAddDoc {
        public String DocumentId {get;set;}
        public String CreationTimestamp {get;set;}
        public List<RespLocation> Locations {get;set;}
    }
    public class RespLocation {
        public String Id {get;set;}
        public String Label {get;set;}
        public Integer PageNumber {get;set;}
    }
}