/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ContentDocumentTriggerHandlerTest
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ContentDocumentTriggerHandlerTest trigger
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 23-03-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
@IsTest
public with sharing class ContentDocumentLinkTriggerHandlerTest {
    
    @TestSetup
    public static void initData()
    {
        Opportunity opp1 = TestDataFactory.createOpportunity('opp1');
        insert opp1;
        Quote q1 = TestDataFactory.createQuote(opp1);
        insert q1;

        ContentVersion cv = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert cv;
    }

    @isTest
    public static void blockContentDocumentLinkSizeTitleTest(){
        Opportunity opp1 = TestDataFactory.createOpportunity('opp1');
        insert opp1;
        Quote q1 = TestDataFactory.createQuote(opp1);
        insert q1;

        ContentVersion cv = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert cv;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = q1.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        
        ContentVersion cvCurrent = [SELECT Id, Title FROM ContentVersion LIMIT 1];
        cvCurrent.Title = 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest';
        
        try{
            test.startTest();
            update cvCurrent;
            test.stopTest();
        }catch(Exception ex){
            System.debug('############# '+ ex.getMessage());
            Boolean expectedExceptionThrown =  ex.getMessage().contains(Constants.STR_CVE_ERRORMESSAGE_SIZE_TITLE) ? true : false;
            System.assertEquals(expectedExceptionThrown, true);
        }
    }  
    
    @isTest
    public static void qlyDeletePresentDoc() {
        Account acc = TestFactory.generateAccount();
        Contact ct = TestFactory.generateContact();
        QLY_SchoolForm__c qly = new QLY_SchoolForm__c(Account__c=acc.Id, Contact__c=ct.Id);
        insert qly;
        
        ContentVersion cv1 = new ContentVersion(
            Title = 'QLY_TEST001',
            PathOnClient = 'QLY_TEST001.pdf',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert cv1;
        
        ContentVersion cv2 = new ContentVersion(
            Title = 'QLY_TEST002',
            PathOnClient = 'QLY_TEST002.pdf',
            VersionData = Blob.valueOf('Test Content Data 2'),
            IsMajorVersion = true
        );
        insert cv2;
        
        List<ContentDocument> cds = [SELECT Id FROM ContentDocument];
        
        insert new List<ContentDocumentLink>{
            new ContentDocumentLink(LinkedEntityId = qly.Id, ContentDocumentId = cds[0].Id),
            new ContentDocumentLink(LinkedEntityId = qly.Id, ContentDocumentId = cds[1].Id)
        };
        
    }
}