/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2019-03-06
* @modified       2019-03-07
* @systemLayer             
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Controller class for button that puts the quote to Signed status
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public inherited sharing class VFC_QuoteStatusToSigned {
    private ApexPages.StandardController standardController;
    private Quote q;
    public VFC_QuoteStatusToSigned(ApexPages.StandardController standardController)
    {
        this.standardController = standardController;
        this.q = [SELECT Id, Name,Status FROM Quote WHERE Id = :ApexPages.currentPage().getParameters().get('id')];

    }
    public PageReference updateQuote()
    {
        try{
            q.Status='Signé';
            Update q;     
        }catch(Exception e){
            system.debug('Failed to update record '+e);
        }
        PageReference pageRef = new PageReference('/'+q.Id);
        return pageRef;
    }
}