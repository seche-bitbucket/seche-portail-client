@isTest(SeeAllData=false)
public with sharing class Batch_ContactsInvCheckDupScheduleTest {
    
    @isTest
    public static void test() {
        
        insert new List<Contacts_Invitation__c>{new Contacts_Invitation__c(Email__c='test@test.com', Soiree__c = true), new Contacts_Invitation__c(Email__c='test@test.com', Soiree__c = true)};
        
        String CRON_EXP = '0 0 1 * * ? 2023';
        
        Test.startTest();
        
        String jobId = System.schedule('Batch_ContactsInvitationCheckDupSchedule',
            CRON_EXP,
            new Batch_ContactsInvitationCheckDupSchedule());
        
        Test.stopTest();
        
    }
    
}