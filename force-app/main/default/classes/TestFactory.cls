/**
 * @description       : 
 * @author            : MAIT - @moidriss
 * @group             : 
 * @last modified on  : 11-20-2023
 * @last modified by  : MAIT - @moidriss
**/
@isTest(SeeAllData=false)
public with sharing class TestFactory {

    public static CAP__c generateCap(Id accountId, Id contactId){
        CAP__c cap = new CAP__c(Name = 'CAP TEST', ComponentStatus__c = 'Brouillon');
        cap.Account__c = accountId;
        cap.Contact__c = contactId;
        insert cap;
        return cap;
    }
    public static Component__c generateComponent(Id capId){
        Component__c component = new Component__c(Name = 'TestComponet', CAP__c = capId);
        insert component;
        return component;
    }
    public static Attestation__c generateAttestation(Id accountId, Id contactId){
        Attestation__c att = new Attestation__c(Name='Test Attestation');
        att.Account__c = accountId;
        att.Contact__c = contactId;
        insert att;
        return att;
    }
    public static T5F_PreFip__c generatePreFip(Lead ld){
        T5F_PreFip__c prefip = new T5F_PreFip__c();
        prefip.Origin__c = 'INSTALLATIONS DE TRAITEMENT - Autre Provenance';
        prefip.WasteType__c = 'Autres déchets';
        prefip.OriginDescription__c = 'test';
        prefip.WasteDescription__c = 'test';
        prefip.Lead__c = ld.Id;
        prefip.Status__c = 'Doct final généré';
        prefip.DocumentStatus__c = 'Pré-FIP finale générée';
        insert prefip;

        return prefip;
    }

    public static Lead generateLead()
    {
        Lead ld = new Lead (LastName = 'Test Last Name', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Internet', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_DND),PostalCode = '69009',
                                 Waste__c = 'Déchet Amiante',AsbestosPackagingType__c = 'Palette',Quantity__c = 200,AsbestosPackagingSize__c = '3*4', Filiale__c = 'Séché Eco Industries Changé',Filiere__c = 'Biocentre',
                                 Packaging_1__c = 'Benne', WasteType1__c = 'test', Volume_1__c = '25 L', Estimated_Duration_Of_Commissioning__c = Constants.STR_DURATION_15_30,
                                 CustomerNature__c = 'Industriel', Industry = 'Chimie', Email = 'Test@Test.com');
        
        insert ld;
        return ld;
    }
    
    public static FIP_FIP__c generateFip(Contact cont, Account acc) {
        FIP_FIP__c fip = new FIP_FIP__c();
        fip.Collector__c = acc.ID;
        fip.Contact__c = cont.ID;
        insert fip; 
        return fip;
    }

    public static FIP_FIP__c generateFipAsbestos(Contact cont, Account acc) {
        FIP_FIP__c fip = new FIP_FIP__c();
        fip.recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName LIKE '%Asbestos'][0].Id;
        fip.Collector__c = acc.ID;
        fip.Contact__c = cont.ID;
        insert fip; 
        return fip;
    }

    public static Quote generateQuote(Contact cont, Account acc, Opportunity opp) {
        Quote quo = new Quote(Pricebook2Id=Test.getStandardPricebookId(), TECH_IsApprouved__c = true, Name = 'Convention '+System.Label.YearN1+' Test Quote',ContactId = cont.Id, OpportunityId = opp.Id, Email = 'Test@Test.com', QuoteCode__c='Code Devis', Status='Présenté' );
       insert quo;
       quo = [Select id,ReferenceWasteOwner__c,FuelPriceIndexation__c,Pricebook2Id, Name,ContactId,Opportunity.Salesman__c,Opportunity.OwnerId,Opportunity.Owner.Email,
	   Opportunity.Owner.Name,Email, QuoteCode__c,TECH_UID__c,Opportunity.Filiale__c,Account.Name FROM Quote WHERE ID = :quo.Id];
       return quo;
    }

    public static Quote generateQuote(Contact cont, Account acc, Opportunity opp, String status) {
        Quote quo = new Quote(Pricebook2Id=Test.getStandardPricebookId(), Status = status, TECH_IsApprouved__c = true, Name = 'Convention '+System.Label.YearN1+' Test Quote',ContactId = cont.Id, OpportunityId = opp.Id, Email = 'Test@Test.com', QuoteCode__c='Code Devis' );
       insert quo;
       quo = [Select id,ReferenceWasteOwner__c,FuelPriceIndexation__c,Pricebook2Id, Name,ContactId,Opportunity.Salesman__c,Opportunity.OwnerId,Opportunity.Owner.Email,
	   Opportunity.Owner.Name,Email, QuoteCode__c,TECH_UID__c,Opportunity.Filiale__c,Account.Name FROM Quote WHERE ID = :quo.Id];
       return quo;
    }
    
    public static Quote generateQuoteSent(Contact cont, Account acc, Opportunity opp) {
        Quote quo = new Quote(TECH_IsApprouved__c = true, Name = 'Convention '+System.Label.YearN1+' Test Quote',ContactId = cont.Id, OpportunityId = opp.Id, Email = 'Test@Test.com', QuoteCode__c='Code Devis', Status='Envoyé pour signature' );
       insert quo;
       quo = [Select id, Name,ContactId,Opportunity.Salesman__c,Opportunity.OwnerId,Opportunity.Owner.Email,
	   Opportunity.Owner.Name,Email, QuoteCode__c,TECH_UID__c,Opportunity.Filiale__c,Account.Name FROM Quote WHERE ID = :quo.Id];
       return quo;
    }
    
    public static Quote generateQuoteNoCnt(Account acc, Opportunity opp) {
        Quote quo = new Quote(TECH_IsApprouved__c = true, Name = 'Convention '+System.Label.YearN1+' Test Quote', OpportunityId = opp.Id, Email = 'Test@Test.com', QuoteCode__c='Code Devis', Status='Présenté' );
       insert quo;
       quo = [Select id, Name,ContactId,Opportunity.Salesman__c,Opportunity.OwnerId,Opportunity.Owner.Email,
	   Opportunity.Owner.Name,Email, QuoteCode__c,TECH_UID__c,Opportunity.Filiale__c,Account.Name FROM Quote WHERE ID = :quo.Id];
       return quo;
    }
    
    public static Quote generateQuoteWithUID(Contact cont, Account acc, Opportunity opp) {
        Quote quo = new Quote(TECH_IsApprouved__c = true, Name = 'Convention '+System.Label.YearN1+' Test Quote', ContactId = cont.Id, TECH_UID__c='test', OpportunityId = opp.Id, Email = 'Test@Test.com', QuoteCode__c='Code Devis', Status='Présenté' );
       insert quo;
       quo = [Select id, Name,ContactId,Opportunity.Salesman__c,Opportunity.OwnerId,Opportunity.Owner.Email,
	   Opportunity.Owner.Name,Email, QuoteCode__c,TECH_UID__c,Opportunity.Filiale__c,Account.Name FROM Quote WHERE ID = :quo.Id];
       return quo;
    }
    
    public static Opportunity generateOpport(Account acc, String stageName) {
        Opportunity opp = new Opportunity(Name= 'Test Opp', StageName = (stageName != null) ? stageName : 'En cours', CloseDate = Date.today().addDays(10), AccountId = acc.Id, Filiale__c = 'Séché Assainissement Les Ageux', Salesman__c= UserInfo.getUserId() );
        insert opp;
        return [Select id,ReferenceWasteOwner__c,FuelPriceIndexation__c FROM Opportunity WHERE ID = :opp.Id];
    }

    public static Opportunity generateOpport(Account acc, String stageName , Id syncedQuote) {
        Opportunity opp = new Opportunity(SyncedQuoteId=syncedQuote, Name= 'Test Opp', StageName = (stageName != null) ? stageName : 'En cours', CloseDate = Date.today().addDays(10), AccountId = acc.Id, Salesman__c= UserInfo.getUserId() );
        insert opp;
        return [Select id,ReferenceWasteOwner__c,FuelPriceIndexation__c FROM Opportunity WHERE ID = :opp.Id];
    }

    public static Form__c generateForm(Opportunity opp, Contact cont) {
        Form__c form = new Form__c(Opportunity__c = opp.Id, Contact__c = cont.Id);
        insert form;
        return [SELECT Id FROM Form__c WHERE ID = :form.Id];
    }

    public static Account generateAccount() {
        Account acc = new Account(Name = 'Test Fact Acc', SIRET_Number__c = '11111111111111');
        insert acc;
        return acc;
    }
    
    public static Account generateAccount(String siret, String contractRoot, String Industry, String customerNature, String X3SECHEID, String ChargedX3Code, String X3SPEICHIMID, String X3AddressCode, String X3ChargedAddressCode) {
        Account acc = new Account(Name = 'Test Fact Acc', 
                                  SIRET_Number__c=siret,
                                  CustomerNature__c=customerNature,
                                  ContractRoot__c= contractRoot,
                                  Industry = Industry,
                                  TECH_X3SECHEID__c=X3SECHEID,
                                  ChargedX3Code__c=ChargedX3Code,
                                  TECH_X3SPEICHIMID__c=X3SPEICHIMID,
                                  X3AddressCode__c=X3AddressCode,
                                  X3ChargedAddressCode__c=X3ChargedAddressCode);
        return acc;
    }

    public static Contact generateContact() {
        Contact cont = new Contact(LastName = 'Test Contact', FirstName = 'Test', Email = 'test@Test.com', Phone= '0606060606');
        insert cont;
        return cont;
    }

    public static Attachment linkAttachement(SObject parent, String name) {
        Attachment att = new Attachment(Name = name, Body = Blob.valueOf('TestBody') ,ContentType= 'txt',ParentId = (Id)parent.get('Id'));
        insert att;
        return att;
    }

    public static ContentDocumentLink linkDoc(Id linkedEntity, String name) {
        Blob beforeblob5=Blob.valueOf('Unit Test Attachment Body');

        ContentVersion cv5 = new ContentVersion();
        cv5.title = name;      
        cv5.PathOnClient ='test';           
        cv5.VersionData =beforeblob5;          
        insert cv5;         

        ContentVersion testContent5 = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv5.Id]; 

        ContentDocumentLink  contDocLink = new ContentDocumentLink(ContentDocumentId = testContent5.ContentDocumentId, LinkedEntityId = linkedEntity, ShareType = 'V' );
        insert contDocLink;

        return contDocLink;
    }

    public static String getResponsePackage(Id cvId) {
        return '{ "Id": "240593f4-0271-4d1c-97e5-37fb39ed9478", "Name": "test008", "Status": "Pending", "CreationDate": "'+System.Label.YearN1+'-03-26T08:47:57Z", "ExpiryDate": null, "Initiator": "sechecrm69@gmail.com", "Documents": [ { "Id": "65f7e71e-348f-41c6-908d-8d6d68d298d9", "PackageId": "240593f4-0271-4d1c-97e5-37fb39ed9478", "Name": "ttest001", "IsOptional": false, "CreationDate": "'+System.Label.YearN1+'-03-26T08:47:57Z", "MediaType": "application/pdf", "Status": "Pending", "Elements": [ { "Type": "signingfield", "UsedSigningMethod": null, "SigningMethods": [ "Manual" ], "LegalNotice": { "Text": null }, "Id": "ea9256eb-2c91-48b2-b858-bfb2abce336f", "ActorId": "66456b2c-4f01-4cc2-b38a-1313d2d281e2", "Location": { "Page": 9, "Top": 618.0, "Left": 49.0 }, "Dimensions": { "Width": 180.0, "Height": 70.0 }, "Status": "Pending", "ExternalReference": null, "CompletedDate": null } ], "ExternalReference": "'+cvId+'" } ], "Stakeholders": [ { "Type": "person", "Language": "fr", "FirstName": "testv4", "LastName": "amine", "EmailAddress": "amine.idrissi.taghki@gmail.com", "PhoneNumber": null, "BirthDate": null, "AdditionalProperties": {}, "Id": "205dfd4f-1e8c-472d-81d7-15d0c901a760", "PackageId": "240593f4-0271-4d1c-97e5-37fb39ed9478", "Actors": [ { "Type": "signer", "Elements": [ { "Type": "signingfield", "UsedSigningMethod": null, "SigningMethods": [ "Manual" ], "LegalNotice": { "Text": null }, "Id": "ea9256eb-2c91-48b2-b858-bfb2abce336f", "ActorId": "66456b2c-4f01-4cc2-b38a-1313d2d281e2", "Location": { "Page": 9, "Top": 618.0, "Left": 49.0 }, "Dimensions": { "Width": 180.0, "Height": 70.0 }, "Status": "Pending", "ExternalReference": null, "CompletedDate": null } ], "RedirectUrl": "https://www.groupe-seche.com/fr", "RedirectType": "AfterCompletion", "BackButtonUrl": null, "Result": null, "Id": "66456b2c-4f01-4cc2-b38a-1313d2d281e2", "SuppressNotifications": false, "Status": "Available", "Links": [ "https://sechegroup-dev.connective.eu/esig/signinit?packageSignId=66456b2c-4f01-4cc2-b38a-1313d2d281e2&token=FBUsvJwRzqRmUENE-DsUpdrRa95yJA2EgBHVpGKKFXvsbLQk9J1A-naAWJ6vJu7Y&expiryTag=NDA3MDkwODgwMHxufR18nyl7s9I_vvmL6rKxPAGjZC1cWJpecRdtIDsRGw" ], "MemberLinks": [] }, { "Type": "receiver", "Id": "bddb4134-a12a-4e18-941d-1990c724a72b", "SuppressNotifications": false, "Status": "Waiting", "Links": [], "MemberLinks": [] } ], "ExternalReference": null } ], "DefaultLegalNotice": { "Text": null }, "ExternalReference": null, "DocumentGroupCode": "00001", "ThemeCode": "00001", "CallBackUrl": null, "NotificationCallBackUrl": null, "F2fSigningUrl": "https://sechegroup-dev.connective.eu/esig/signinit?packageSignId=240593f4-0271-4d1c-97e5-37fb39ed9478&token=vWH0h3w3Ne1R_mV7ZcxODktc368tbBVKCzHeoVWlUWwhjkH3pZtzq6DnlexNrTDt&expiryTag=NDA3MDkwODgwMHy7B4ey7eD9nTS_iscImSjpm0pS7vmUrmaBHVB1jQGQRg&f2f=True", "F2fRedirectUrl": "https://www.groupe-seche.com/fr", "IsUnsignedContentDownloadable": false, "ActionUrlExpirationPeriodInDays": null, "ProofCorrelationId": null, "Warnings": [] }';
    }
    
    public static String getResponsePackage(Contact ct, Id cvId) {
        return '{ "Id": "240593f4-0271-4d1c-97e5-37fb39ed9478", "Name": "test008", "Status": "Pending", "CreationDate": "'+System.Label.YearN1+'-03-26T08:47:57Z", "ExpiryDate": null, "Initiator": "sechecrm69@gmail.com", "Documents": [ { "Id": "65f7e71e-348f-41c6-908d-8d6d68d298d9", "PackageId": "240593f4-0271-4d1c-97e5-37fb39ed9478", "Name": "ttest001", "IsOptional": false, "CreationDate": "'+System.Label.YearN1+'-03-26T08:47:57Z", "MediaType": "application/pdf", "Status": "Pending", "Elements": [ { "Type": "signingfield", "UsedSigningMethod": null, "SigningMethods": [ "Manual" ], "LegalNotice": { "Text": null }, "Id": "ea9256eb-2c91-48b2-b858-bfb2abce336f", "ActorId": "66456b2c-4f01-4cc2-b38a-1313d2d281e2", "Location": { "Page": 9, "Top": 618, "Left": 49 }, "Dimensions": { "Width": 180, "Height": 70 }, "Status": "Pending", "ExternalReference": null, "CompletedDate": null } ], "ExternalReference": "'+cvId+'" } ], "Stakeholders": [ { "Type": "person", "Language": "fr", "FirstName": "testv4", "LastName": "amine", "EmailAddress": "amine.idrissi.taghki@gmail.com", "PhoneNumber": null, "BirthDate": null, "AdditionalProperties": {}, "Id": "205dfd4f-1e8c-472d-81d7-15d0c901a760", "PackageId": "240593f4-0271-4d1c-97e5-37fb39ed9478", "Actors": [ { "Type": "signer", "Elements": [ { "Type": "signingfield", "UsedSigningMethod": null, "SigningMethods": [ "Manual" ], "LegalNotice": { "Text": null }, "Id": "ea9256eb-2c91-48b2-b858-bfb2abce336f", "ActorId": "66456b2c-4f01-4cc2-b38a-1313d2d281e2", "Location": { "Page": 9, "Top": 618, "Left": 49 }, "Dimensions": { "Width": 180, "Height": 70 }, "Status": "Pending", "ExternalReference": null, "CompletedDate": null } ], "RedirectUrl": "https://www.groupe-seche.com/fr", "RedirectType": "AfterCompletion", "BackButtonUrl": null, "Result": null, "Id": "66456b2c-4f01-4cc2-b38a-1313d2d281e2", "SuppressNotifications": false, "Status": "Available", "Links": [ "https://sechegroup-dev.connective.eu/esig/signinit?packageSignId=66456b2c-4f01-4cc2-b38a-1313d2d281e2&token=FBUsvJwRzqRmUENE-DsUpdrRa95yJA2EgBHVpGKKFXvsbLQk9J1A-naAWJ6vJu7Y&expiryTag=NDA3MDkwODgwMHxufR18nyl7s9I_vvmL6rKxPAGjZC1cWJpecRdtIDsRGw" ], "MemberLinks": [] }, { "Type": "receiver", "Id": "bddb4134-a12a-4e18-941d-1990c724a72b", "SuppressNotifications": false, "Status": "Waiting", "Links": [], "MemberLinks": [] } ], "ExternalReference": "'+ct.Id+'" } ], "DefaultLegalNotice": { "Text": null }, "ExternalReference": null, "DocumentGroupCode": "00001", "ThemeCode": "00001", "CallBackUrl": null, "NotificationCallBackUrl": null, "F2fSigningUrl": "https://sechegroup-dev.connective.eu/esig/signinit?packageSignId=240593f4-0271-4d1c-97e5-37fb39ed9478&token=vWH0h3w3Ne1R_mV7ZcxODktc368tbBVKCzHeoVWlUWwhjkH3pZtzq6DnlexNrTDt&expiryTag=NDA3MDkwODgwMHy7B4ey7eD9nTS_iscImSjpm0pS7vmUrmaBHVB1jQGQRg&f2f=True", "F2fRedirectUrl": "https://www.groupe-seche.com/fr", "IsUnsignedContentDownloadable": false, "ActionUrlExpirationPeriodInDays": null, "ProofCorrelationId": null, "Warnings": [] }';
    }
    
    public static string getResponseStatus(String packageId, String status, String reason, Id cvId) {
        String rsn = (reason != null) ? '"'+reason+'"': null;
        return '{ "Id": "'+packageId+'", "Name": "test006", "Status": "'+status+'", "CreationDate": "'+System.Label.YearN1+'-03-18T10:47:57Z", "ExpiryDate": null, "Initiator": "sechecrm69@gmail.com", "Documents": [ { "Id": "5f0f4d83-e2f3-48c7-bde9-91a676dbef97", "PackageId": "'+packageId+'", "Name": "ttest001", "IsOptional": false, "CreationDate": "'+System.Label.YearN1+'-03-18T10:47:58Z", "MediaType": "application/pdf", "Status": "'+status+'", "Elements": [ { "Type": "signingfield", "UsedSigningMethod": "Manual", "SigningMethods": [ "Manual" ], "LegalNotice": { "Text": null }, "Id": "186f5294-1468-47ba-91e8-92512d122df1", "ActorId": "cc3fe357-7c86-4edb-bd32-d02fad6836f0", "Location": { "Page": 9, "Top": 618, "Left": 49 }, "Dimensions": { "Width": 180, "Height": 70 }, "Status": "'+status+'", "ExternalReference": null, "CompletedDate": "'+System.Label.YearN1+'-03-18T10:50:49Z" } ], "ExternalReference": "'+cvId+'" }, { "Id": "5c9ad98e-bf86-4b80-a407-eb9a20f223da", "PackageId": "'+packageId+'", "Name": "ttest002", "IsOptional": false, "CreationDate": "'+System.Label.YearN1+'-03-18T10:48:00Z", "MediaType": "application/pdf", "Status": "'+status+'", "Elements": [ { "Type": "signingfield", "UsedSigningMethod": "Manual", "SigningMethods": [ "Manual" ], "LegalNotice": { "Text": null }, "Id": "60ec8e0c-e10d-4e6a-875b-02e10f2eee0e", "ActorId": "cc3fe357-7c86-4edb-bd32-d02fad6836f0", "Location": { "Page": 1, "Top": 75, "Left": 100 }, "Dimensions": { "Width": 200, "Height": 75 }, "Status": "'+status+'", "ExternalReference": null, "CompletedDate": "'+System.Label.YearN1+'-03-18T10:51:46Z" } ], "ExternalReference": "'+cvId+'" } ], "Stakeholders": [ { "Type": "person", "Language": "fr", "FirstName": "testv4", "LastName": "amine", "EmailAddress": "amine.idrissi.taghki@gmail.com", "PhoneNumber": null, "BirthDate": null, "AdditionalProperties": {}, "Id": "a39b8b9a-4d83-40f5-b780-8f6311a46be9", "PackageId": "'+packageId+'", "Actors": [ { "Type": "signer", "Elements": [ { "Type": "signingfield", "UsedSigningMethod": "Manual", "SigningMethods": [ "Manual" ], "LegalNotice": { "Text": null }, "Id": "186f5294-1468-47ba-91e8-92512d122df1", "ActorId": "cc3fe357-7c86-4edb-bd32-d02fad6836f0", "Location": { "Page": 9, "Top": 618, "Left": 49 }, "Dimensions": { "Width": 180, "Height": 70 }, "Status": "'+status+'", "ExternalReference": null, "CompletedDate": "'+System.Label.YearN1+'-03-18T10:50:49Z" }, { "Type": "signingfield", "UsedSigningMethod": "Manual", "SigningMethods": [ "Manual" ], "LegalNotice": { "Text": null }, "Id": "60ec8e0c-e10d-4e6a-875b-02e10f2eee0e", "ActorId": "cc3fe357-7c86-4edb-bd32-d02fad6836f0", "Location": { "Page": 1, "Top": 75, "Left": 100 }, "Dimensions": { "Width": 200, "Height": 75 }, "Status": "'+status+'", "ExternalReference": null, "CompletedDate": "'+System.Label.YearN1+'-03-18T10:51:46Z" } ], "RedirectUrl": "https://www.groupe-seche.com/fr", "RedirectType": "AfterCompletion", "BackButtonUrl": null, "Result": { "CompletedBy": { "Email": "amine.idrissi.taghki@gmail.com", "VerifiedName": null }, "CompletedDate": "'+System.Label.YearN1+'-03-18T10:50:49Z", "RejectReason": '+rsn+' }, "Id": "cc3fe357-7c86-4edb-bd32-d02fad6836f0", "SuppressNotifications": false, "Status": "'+status+'", "Links": [ "https://sechegroup-dev.connective.eu/esig/downloadapi/services/packages/'+packageId+'/download/yuit4ju1zPMkAEsH4SyT7os6ILyQisqEH662FIfFicZs708J7L4-HYlGGZgWzV1m?expiryTag=NDA3MDkwODgwMHyrDGHpm7UQUNfMVMDtiMUZBq8MEKK2KvA_pdeDfcxXZQ" ], "MemberLinks": [] }, { "Type": "receiver", "Id": "213f07e4-9d38-4b03-81bc-48f6c837c2df", "SuppressNotifications": false, "Status": "'+status+'", "Links": [ "https://sechegroup-dev.connective.eu/esig/downloadapi/services/packages/'+packageId+'/download/TTIVAu7iBvf9SPj-w8GMZ5jLCRve5cMfxonWuQ6l3_wJAYlB2mJaafsc6FGXXND3?expiryTag=NDA3MDkwODgwMHwynVdB8265tumUn6rLGjSP1cT7UGytYA4yDaPDOBi2xw&fromEmail=True" ], "MemberLinks": [] } ], "ExternalReference": null } ], "DefaultLegalNotice": { "Text": null }, "ExternalReference": null, "DocumentGroupCode": "00001", "ThemeCode": "00001", "CallBackUrl": null, "NotificationCallBackUrl": null, "F2fSigningUrl": null, "F2fRedirectUrl": "https://www.groupe-seche.com/fr", "IsUnsignedContentDownloadable": false, "ActionUrlExpirationPeriodInDays": null, "ProofCorrelationId": null, "Warnings": [] }';
    }
    
    public static void createSettings() {
        
        List<Quote_Field__c> lst1 = new List<Quote_Field__c>{new Quote_Field__c(Name='ReferenceWasteOwner__c', OppSyncField__c='ReferenceWasteOwner__c'), 
            new Quote_Field__c(Name='FuelPriceIndexation__c', OppSyncField__c='FuelPriceIndexation__c')};
                
        List<QuoteLineItem_Field__c> lst2 = new List<QuoteLineItem_Field__c>{new QuoteLineItem_Field__c(Name='Classe__c', OppLineSyncField__c='Classe__c'),
            new QuoteLineItem_Field__c(Name='Features__c', OppLineSyncField__c='Features__c')};
                
        insert lst1;
        insert lst2;
        
    }

    public static WasteCollectRequest__c generateDDE() {

        Account acc = generateAccount();
        Account acc_DDE = new Account(Name='SIREDOM');
        upsert acc_DDE;
        acc.ParentId = acc_DDE.Id;
        update acc;

        WasteCollectRequest__c dde = new WasteCollectRequest__c(RequestType__c='Classique',
                                                                Date_Enlevement__c=Date.today(),
                                                                CallDate__c=Date.today(),
                                                                Account__c=acc.Id);

        insert dde;
        return dde;

    }
    
}