/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-06-27
* @modified       2018-09-26
* @systemLayer    Test         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Controller class for button that changes the value increased on an amendment
  2018-09-26	Added changeAllInput method to make all inputvalues traitement/transport to the value of inputvalue if its filled
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class VFC_IncreaseFixedValue {
    public List<Amendment__c> AmendmentList {get;set;}
    public Double inputValue{get;set;}
    public Double inputValueTraitement{get;set;}
    public Double inputValueTransport{get;set;}
    public Double inputValueConditionnement{get;set;}
    public Boolean NoValue{get;set;}
    public Integer Year{get;set;}
    public VFC_IncreaseFixedValue(ApexPages.StandardSetController controller){
        this.Year = System.Today().year()+1;
        if(controller.getSelected().size()>=1){
            // Retrieve selected amendments
            this.AmendmentList=[SELECT Nature__c,PriceCurrentYear__c,PriceNextYear__c,Contrat2__r.RecordLocked__c FROM Amendment__c WHERE ID IN:controller.getSelected()];
            this.NoValue=false;
            // Check if the amendments are locked or not
            Boolean Locked=false;
            for(Amendment__c am: this.amendmentList){
                if(am.Contrat2__r.RecordLocked__c==true){
                    Locked=true;
                }
            }
            if(locked){
                this.Novalue=true;
                ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'L\'avenant sélectionné est soumis ou validé et ne peut pas être modifié');
                ApexPages.addMessage(errorMessage);
            }
        }else{
            this.Novalue=true;
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Veuillez selectionner un avenant');
            ApexPages.addMessage(errorMessage);
        }
    }
    public void changeAllInput(){
        this.inputValueTraitement=this.inputValue;
        this.inputValueTransport=this.inputValue;
        this.inputValueConditionnement=this.inputValue;
    }
    public PageReference save(){
        PageReference returnPage=ApexPages.currentPage();
        List<Amendment__c> AmendmentToUpdate=new List<Amendment__c>();
        // Add the value of price current year to the price next year 
        if(this.inputValue!=null && this.inputValue>0){
            for(Amendment__c am: this.amendmentList){
                am.PriceNextYear__c=this.inputValue;
                am.ByPassContract__c=false;
                AmendmentToUpdate.add(am);
            }
        }else{
            if((this.inputValueTraitement!=null && this.inputValueTraitement>0) 
               || (this.inputValueTransport!=null && this.inputValueTransport>0)
               || (this.inputValueConditionnement!=null && this.inputValueConditionnement>0) ){
                   for(Amendment__c am: this.amendmentList){
                       if(am.Nature__c=='Traitement' && (this.inputValueTraitement!=null && this.inputValueTraitement>0)){
                           am.PriceNextYear__c=this.inputValueTraitement;
                           am.ByPassContract__c=false;
                       }
                       if(am.Nature__c=='Prestation' && (this.inputValueTransport!=null && this.inputValueTransport>0)){
                           am.PriceNextYear__c=this.inputValueTransport;  
                           am.ByPassContract__c=false;
                       }
                       if(am.Nature__c=='Conditionnement' && (this.inputValueConditionnement!=null && this.inputValueConditionnement>0)){
                           am.PriceNextYear__c=this.inputValueConditionnement;  
                           am.ByPassContract__c=false;
                           
                       }
                       AmendmentToUpdate.add(am);
                   }
               }else{
                   ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.WARNING,'Veuillez saisir une valeur positive');
                   ApexPages.addMessage(errorMessage);
               }
        }

        If(AmendmentToUpdate.size()>0){
            Update AmendmentToUpdate;
            ReturnPage= new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        }
        return ReturnPage;
    }
}