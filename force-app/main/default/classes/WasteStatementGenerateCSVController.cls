public class WasteStatementGenerateCSVController {
    
    
    ApexPages.StandardSetController stdSetController;
    public String CSV {get;set;}
    public String fileName {get;set;}
    private static String separator = ';';
    private static String returnLine = '\n';

    private static List <QuoteLineItem> qlis = New List <QuoteLineItem>();
    private static List <WST_Waste_Statement__c> wsts = New List <WST_Waste_Statement__c>();
    private static Map<ID, WST_Waste_Statement__c> wstByQuoteID = New Map<ID, WST_Waste_Statement__c>();
        
    public WasteStatementGenerateCSVController(ApexPages.StandardSetController controller) {  
        
        stdSetController = controller;
        wsts = [SELECT ID, Status__c, SendTransfoToElim__c, Name, OilWeight__c, TotalWeight__c, DSNumber__c, WST_Service_Type__c, CAP_Number__c, OrderNumber__c, ServiceDemande__r.OrderNumber__c, Opportunity__c, Opportunity__r.SyncedQuoteID From WST_Waste_Statement__c WHERE ID = : stdSetController.getSelected() ];
        Set<ID> qs = getQuotesID(wsts);
        wstByQuoteID = getWstsByQuoteID(wsts);
        qlis = [Select ID, 	Product2.productCode, Product2.Param_1_Num_Min__c, Product2.Param_1_Num_Max__c, Product2.Param_3_Text__c, ProductServiceLabel__c, Quantity, UnitPrice, ProductCode__c, QuoteID, Quote.QuoteCode__c  FROM QuoteLineItem WHERE QuoteID in : qs];
        
    }
    
    public void generateCSV(){
        
        String header = generateHeader();
        String content = generateContent();
        CSV = header + content;
        
        udpateStatus();
    }
    
    public static String generateHeader(){
        String header = 'NUM_DS'+separator+
            'ID_SF_TRANSFO'+separator+
            'TYPE_SERVICE'+separator+
            'NUM_SERIE_TRANSFO'+separator+
            'NUM_CAP'+separator+
            'CODE_PRESTA'+separator+
            'DESIGNATION'+separator+
            'QUANTITE'+separator+
            'PRIX_UNITAIRE'+separator+
            'CODET'+separator+
            'CODE_DEVIS'+separator+
            'NUM_COMMANDE'+separator+
            'DESTRUCTION'+separator+
            'POIDS_HUILE'+separator+
            'POIDS_METAL'+separator+
            'FILLER'+returnLine;
        
        return header;   
    }
    
    public static String generateContent(){
        String content = '';
        
      
        for(QuoteLineItem qli : qlis){
            Double metalicWeight = 0;
           
            
            if((qli.Product2.Param_1_Num_Min__c == null &&  qli.Product2.Param_1_Num_Max__c == null) || qli.Product2.Param_3_Text__c == 'Destruction'){
                WST_Waste_Statement__c wst = wstByQuoteID.get(qli.QuoteID);
                try{
                    metalicWeight = wst.TotalWeight__c - wst.OilWeight__c;
                }catch(Exception e){
                    System.debug('Exeption : '+e);
                }
                
                String orderNum ='';   
                if(wst.OrderNumber__c != null){
                    orderNum = wst.OrderNumber__c;
                }else{
                    orderNum = wst.ServiceDemande__r.OrderNumber__c;
                }
               
                content += wst.DSNumber__c +separator+
                    wst.ID +separator+
                    wst.WST_Service_Type__c +separator+
                    wst.Name +separator+
                    wst.CAP_Number__c +separator+
                    qli.Product2.productCode +separator+
                    (qli.ProductServiceLabel__c).left(50)+separator+
                    qli.Quantity+separator+
                    qli.UnitPrice+separator+
                    qli.ProductCode__c+separator+
                    qli.Quote.QuoteCode__c+separator+
                    orderNum+separator+
                    wst.SendTransfoToElim__c+separator+
                    wst.OilWeight__c+separator+
                    metalicWeight+separator+
                    ';' +returnLine;
            }
            
          
        }
        
        return content;
    }
    
    
    private static Set<ID> getQuotesID(List<WST_Waste_Statement__c> wstList){
        Set<ID> qs = new Set<ID>();
        
        for(WST_Waste_Statement__c wst : wstList){
            qs.add(wst.Opportunity__r.syncedQuoteID);
        }
        
       return qs;
    }
    
    private static Map<ID, WST_Waste_Statement__c> getWstsByQuoteID(List<WST_Waste_Statement__c> wstList){
         Map<ID, WST_Waste_Statement__c> mapWST = New Map<ID, WST_Waste_Statement__c>();
        
        for(WST_Waste_Statement__c wst : wstList){
            mapWST.put(wst.Opportunity__r.SyncedQuoteID, wst);
        }
        
        return mapWST;
    }
    
    
     public void udpateStatus(){
        List<WST_Waste_Statement__c> lstWSTToUpdateStatus = new List<WST_Waste_Statement__c>();
         for(WST_Waste_Statement__c wst : wsts){
             wst.Status__c = 'Commande acceptee';
             lstWSTToUpdateStatus.add(wst);
         }
         
         if(lstWSTToUpdateStatus.size() > 0)
         {
             update lstWSTToUpdateStatus;
         }
         
     }
}