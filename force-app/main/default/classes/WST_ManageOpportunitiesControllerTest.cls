@isTest
public class WST_ManageOpportunitiesControllerTest {

    // FINISHED
    @isTest
    public static void WST_ManageOpportunitiesControllerTest(){

         Test.startTest();
         //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        
        //insert WST_Waste_Statement
        insert vWst1;
        
       
        //create List of WST_Waste_Statement
		List<WST_Waste_Statement__c> vLwsts = new List<WST_Waste_Statement__c>();
        vLwsts.add(vWst1);
		        
        //create controller
        Test.setCurrentPage(Page.WST_ManageEliminationOpportunity);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(vLwsts);
        stdSetController.setSelected(vLwsts);
        
        //call the method
        WST_ManageOpportunitiesController sdGenerateCSVController = new WST_ManageOpportunitiesController(stdSetController);
        System.assertNotEquals(null, sdGenerateCSVController);
        Test.stopTest();

    }



	// FINISHED
    @isTest
    public static void manageOpportunitesTest(){
 		
         //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        
        //insert WST_Waste_Statement
        insert vWst1;
       
        //create List of WST_Waste_Statement
		List<WST_Waste_Statement__c> vLwsts = new List<WST_Waste_Statement__c>();
        vLwsts.add(vWst1);
		         
        Test.startTest();
        
        //create controller
        Test.setCurrentPage(Page.WST_ManageEliminationOpportunity);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(vLwsts);
        stdSetController.setSelected(vLwsts);
        
        //call the method
        WST_ManageOpportunitiesController sdGenerateCSVController = new WST_ManageOpportunitiesController(stdSetController);
       
        //call method
        PageReference vResult =  WST_ManageOpportunitiesController.manageOpportunites();

        //check the result
        System.debug('manageOpportunitesTest.retPage =' + vResult);  
        System.assertNotEquals(null, vResult);
        Test.stopTest();

    }    
    
    
    
        
   
   
    
    
       
    // FINISHED
    @isTest
    public static void createQuotesTest(){
        Test.startTest();
 
        //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        
        //insert 
        insert vWst1;
        
        //create a list WST_Waste_Statement
        List<WST_Waste_Statement__c> vLwsts = new List<WST_Waste_Statement__c>();
        vLwsts.add(vWst1);
         
        //create a list of oppornunity
        List<Opportunity> vLOpp = WST_ManageOpportunitiesController.createOpportunities(vLwsts, false);
        
        //call method  
        List<Quote> vResult  =WST_ManageOpportunitiesController.createQuotes(vLwsts,vLOpp);
        System.assertNotEquals(Null, vResult );
        
        Test.stopTest();

    }
   
  
   
    
    @isTest
    public static void generateGenericLineItemTest(){
       
        
        Test.startTest(); 
        
         //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        
        //insert Waste_Statement
        insert vWst1;
        
               	

         //Create opportunity
		 Opportunity vOpportunity = TestDataFactory.createOpportunity('OpportunityName1');  
         vOpportunity.Transformateur__c = vWst1.id;
         
         //insert opportunity
         insert vOpportunity;
                 
         //link Waste_Statement to opportunity
          vWst1.Opportunity__c = vOpportunity.Id;
        
        //Create Quotelines linked with the WST
        PricebookEntry pbe = TestDataFactory.getPriceBookEntry();
        List<QuoteLine__c> qls = TestDataFactory.createCustomQuoteLineList_WST_Parent(vWst1, 3, pbe);
        insert qls;
        
        System.debug('createOpportunityLineItemsTest.vOpportunity.Transformateur__c = ' + vOpportunity.Transformateur__c);  
        
        //create list Opportunity
        List<Opportunity> vlOpportunity = new List<Opportunity>();
		vlOpportunity.add(vOpportunity);
        
        // insert list Opportunity
        update vlOpportunity;
        
        //call method
        Boolean vResult = WST_ManageOpportunitiesController.createOpportunityLineItems(vlOpportunity, new List<OpportunityLineItem>());
        
        //check the result
        System.debug('createOpportunityLineItemsTest.vResult' + vResult);  
        System.assertEquals(true, vResult);
        Test.stopTest();


    }
      
    // FINISHED TEST1 Label.RCT_PB_Depollution
    @isTest
    public static void manageDefaultPriceBookEntryTest(){
        //TEST1 
        //initialise Variable
        String vOppRTID = Label.RCT_PB_Depollution;
        System.debug('manageDefaultPriceBookEntryTest.vOppRTID = ' + vOppRTID);
              
        //call the method
        String vResult = WST_ManageOpportunitiesController.manageDefaultPriceBookEntry(vOppRTID);
        System.debug('manageDefaultPriceBookEntryTest.vResult = ' + vResult);
        
            
          
        //Check the result
        System.assert(vResult.contains(label.RCT_PBE_DEFAUT_Depollution));

    }
    
    
   
    // FINISHED TEST2 Label.RCT_PBE_DEFAUT_Reparation
    @isTest
    public static void manageDefaultPriceBookEntryTest2(){
        //TEST1 
        //initialise Variable
        String vOppRTID = Label.RCT_PB_Reparation;
        System.debug('manageDefaultPriceBookEntryTest.vOppRTID = ' + vOppRTID);
              
        //call the method
        String vResult = WST_ManageOpportunitiesController.manageDefaultPriceBookEntry(vOppRTID);
        System.debug('manageDefaultPriceBookEntryTest2.vResult = ' + vResult);
        
        //Check the result
        System.assert(vResult.contains(label.RCT_PBE_DEFAUT_Reparation));
       
    }
    
    

            
    //FINISHED
    @isTest
    public static void getQuoteEndDateTest(){
        
        String month = '';
        //TEST1 RCT_PB_Reparation
        //initialise Variable
        ID vPriceBookID = Test.getStandardPricebookId();
        vPriceBookID = Label.RCT_PB_Reparation;

        //call the method
        Date vResult= WST_ManageOpportunitiesController.getQuoteEndDate(vPriceBookID);
        System.debug('getQuoteEndDateTest.vResult = ' + vResult);
        
        //Check the result
        if(vResult.month() < 10){
            month = '0'+vResult.month();
        }else{
            month = vResult.month()+'';
        }
        System.assertEquals(Label.RCT_REPA_END_DATE_CONTRACT, vResult.day()+'/'+month+'/'+vResult.year());
        
        //TEST2 RCT_PB_Depollution
        // //initialise Variable
        ID vPriceBookID2 = Test.getStandardPricebookId();
        vPriceBookID2 = Label.RCT_PB_Depollution;

        //call the method
        Date vResult2= WST_ManageOpportunitiesController.getQuoteEndDate(vPriceBookID2);
        System.debug('getQuoteEndDateTest.vResult2 = ' + vResult2);
        //Check the result
        if(vResult2.month() < 10){
            month = '0'+vResult2.month();
        }else{
            month = vResult2.month()+'';
        }
        
        
        //Check the result
        System.assertEquals(Label.RCT_DEPOL_END_DATE_CONTRACT,vResult2.day()+'/'+month+'/'+vResult2.year());
        
        //TEST3 RCT_PB_Elimination_N 
        // //initialise Variable
        ID vPriceBookID3 = Test.getStandardPricebookId();
        vPriceBookID3 = Label.RCT_PB_Elimination_N ;

        //call the method
        Date vResult3= WST_ManageOpportunitiesController.getQuoteEndDate(vPriceBookID3);
        System.debug('getQuoteEndDateTest.vResult3 = ' + vResult3);
        if(vResult3.month() < 10){
            month = '0'+vResult3.month();
        }else{
            month = vResult3.month()+'';
        }
        //Check the result
        System.assertEquals(Label.RCT_ELIM_END_DATE_CONTRACT,vResult3.day()+'/'+month+'/'+vResult3.year());
        
        
        //TEST4 RCT_PB_Elimination_SF6 
        // //initialise Variable
        ID vPriceBookID4 = Test.getStandardPricebookId();
        vPriceBookID4 = Label.RCT_PB_Elimination_SF6 ;

        //call the method
        Date vResult4= WST_ManageOpportunitiesController.getQuoteEndDate(vPriceBookID4);
        System.debug('getQuoteEndDateTest.vResult4 = ' + vResult4);
        if(vResult4.month() < 10){
            month = '0'+vResult4.month();
        }else{
            month = vResult4.month()+'';
        }
        //Check the result
        System.assertEquals(Label.RCT_SF6_END_DATE_CONTRACT, vResult4.day()+'/'+month+'/'+vResult4.year());
        
        
    }
      
     // FINISHED
    @isTest
    public static void createOpportunitiesTest(){
        Test.startTest();
 
        //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        
        //insert 
        insert vWst1;
        
        //create a list WST_Waste_Statement
        List<WST_Waste_Statement__c> vLwsts = new List<WST_Waste_Statement__c>();
        vLwsts.add(vWst1);
        
        
        //call method
        List<Opportunity> vResult = WST_ManageOpportunitiesController.createOpportunities(vLwsts, false);
        
        //check the result
        System.debug('createOpportunityLineItemsTest.vResult =' + vResult);  
        Opportunity vResult2 = vResult.get(0);
        System.debug('createOpportunityLineItemsTest.vResult2.Nature__c = ' + vResult2.Nature__c  );  
        System.assertEquals('Développement récurrent', vResult2.Nature__c );
        Test.stopTest();

    }

    
    
        
        
    //FINISHED
    @isTest
    public static void generateMapOfQuoteLinesTest(){
     	 Test.startTest(); 
        
        //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        
        //insert Waste_Statement
        insert vWst1;
        
        
        //Create Service Demande
        String name = 'AccountName1';
        Account acc = TestDataFactory.createAccountWithInsert(name);

        SD_ServiceDemand__c vServiceD = TestDataFactory.createServiceDemand(acc, TestDataFactory.createContactWithInsert(acc), 'Repairs');
        
        //insert Service Demande
        insert vServiceD;
        
        
        List<QuoteLine__c> vLquoteLine = new List<QuoteLine__c>();
        
         //Create QuoteLine  
        QuoteLine__c vQuoteline = new QuoteLine__c();
 
         
        //link QuoteLine to ServiceDemande
        vQuoteline.ServiceDemand__c = vServiceD.Id;
          
        //link QuoteLine to TECH_PriceBookEntryID__c  
        vQuoteline.TECH_PriceBookEntryID__c='01u9E00000A0sGcQAJ';
          
        
        insert vQuoteline;
        
        vLquoteLine.add(vQuoteline);
        update vLquoteLine;
        
        //call method
         Map<ID, List<QuoteLine__c>> vResult =  WST_ManageOpportunitiesController.generateMapOfQuoteLines(vLquoteLine);	
            
        //check the result
        System.debug('generateMapOfQuoteLinesTest.vResult = ' + vResult);
        System.assertNotEquals(null, vResult);
        
        Test.stopTest();

    }     
    
    //FINISHED
    @isTest
    public static void createOpportunityLineItemsTest(){
       
        
        Test.startTest(); 
        List<WST_Waste_Statement__c> wsts = new List<WST_Waste_Statement__c>();
         //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        vWst1.KVA__c = 250;
        vWst1.PCBContent__c = 230;
        vWst1.RecordTypeId = Label.RCT_RT_WST_Remediation;
        //insert Waste_Statement
        insert vWst1;
        wsts.add(vWst1);
        
        //create a list of oppornunity
        List<Opportunity> vLOpp = WST_ManageOpportunitiesController.createOpportunities(wsts, false);
        List<Opportunity> oppToUpdate = new  List<Opportunity>();
        for(Opportunity opp : vLOpp){
            opp.PriceBook2Id = Label.RCT_PB_Depollution;
            oppToUpdate.add(opp);
        }
        
        update(oppToUpdate);
        
        
        List<OpportunityLineItem> olis =  WST_ManageOpportunitiesController.generateGenericLineItem();
        
     
        //call method
        //Boolean vResult = WST_ManageOpportunitiesController.createOpportunityLineItems(vlOpportunity, new List<OpportunityLineItem>());
        
        //check the result
        //System.debug('createOpportunityLineItemsTest.vResult' + vResult);  
        //System.assertEquals(true, vResult);
        Test.stopTest();


    }
     
}