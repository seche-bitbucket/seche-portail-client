public with sharing class SyncHandler {
    
    private static Boolean isTest = false;
 	public static void runTest(){
        isTest = true;
    }
    
    private static boolean run = true;
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        } else{
            return run;
        }
    }

    private static Map<String, String> TYPE_DEPENDENCIES = new Map<String, String>{
        'Quote' => 'Opportunity',
        'Opportunity' => 'Quote',
        'QuoteLineItem' => 'OpportunityLineItem',
        'OpportunityLineItem' => 'QuoteLineItem'
    };

    private static Map<String, List<String>> FIELDS_DEPENDENCIES = new Map<String, List<String>>{
        'Quote' => new List<String>(Schema.getGlobalDescribe().get('Quote').getDescribe().fields.getMap().keySet()),
        'Opportunity' => new List<String>(Schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap().keySet()),
        'QuoteLineItem' => new List<String>(Schema.getGlobalDescribe().get('QuoteLineItem').getDescribe().fields.getMap().keySet()),
        'OpportunityLineItem' => new List<String>(Schema.getGlobalDescribe().get('OpportunityLineItem').getDescribe().fields.getMap().keySet())
    };
        
    private static Map<String, Map<String, SObjectField>> SOBJECT_FIELDS_DEPENDENCIES = new Map<String, Map<String, SObjectField>>{
        'Quote' => Schema.getGlobalDescribe().get('Quote').getDescribe().fields.getMap(),
        'Opportunity' => Schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap(),
        'QuoteLineItem' => Schema.getGlobalDescribe().get('QuoteLineItem').getDescribe().fields.getMap(),
        'OpportunityLineItem' => Schema.getGlobalDescribe().get('OpportunityLineItem').getDescribe().fields.getMap()
    };

    private static List<Quote_Field__c> lstQuoteFields = [SELECT Name,OppSyncField__c FROM Quote_Field__c];
    private static List<QuoteLineItem_Field__c> lstQuoteLineFields = [SELECT Name,OppLineSyncField__c FROM QuoteLineItem_Field__c];

    public static void SyncHandler(List<SObject> lstRecords, Map<Id, SObject> oldMap, String sourceType) {
        
        system.debug(sourceType);

        List<Id> idsForQuery = new List<Id>();
        List<Id> sourceIds = new List<Id>();
        Map<Id, SObject> mapIdRecord = new Map<Id, SObject>();

        List<Quote_Field__c> lstQuoteFields = [SELECT Name,OppSyncField__c FROM Quote_Field__c];
        List<QuoteLineItem_Field__c> lstQuoteLineFields = [SELECT Name,OppLineSyncField__c FROM QuoteLineItem_Field__c];
        
        String filter;

        Boolean isOppLineItem = false;

        if(sourceType == 'Quote') {
            filter = ' SyncedQuoteId ';
        } else if(sourceType == 'Opportunity') {
            filter = ' OpportunityId ';
        } else if (sourceType == 'QuoteLineItem') {
            filter = ' ID ';
        }
        
        system.debug(filter);
 
        for(SObject record: lstRecords) {

            if(oldMap != null && oldMap.size() > 0) {
            
                if(sourceType.contains('Line')) {
                    for(QuoteLineItem_Field__c qlineItem: lstQuoteLineFields) {
                        if(sourceType.contains('Quote')) {
                            if(record.get(qlineItem.Name) != oldMap.get((Id)record.get('Id')).get(qlineItem.Name)) {
                                idsForQuery.add((Id)record.get('OpportunityLineItemId'));
                                sourceIds.add((Id)record.get('Id'));
                                mapIdRecord.put((Id)record.get('Id'), record);
                            }
                        } else {
                            if(record.get(qlineItem.OppLineSyncField__c) != oldMap.get((Id)record.get('Id')).get(qlineItem.OppLineSyncField__c)) {
                                idsForQuery.add((Id)record.get('Id'));
                                sourceIds.add((Id)record.get('Id'));
                                mapIdRecord.put((Id)record.get('Id'), record);
                            }
                            isOppLineItem = true;
                        }
                    }
                } else {
                    for(Quote_Field__c qField: lstQuoteFields) {
                        if(sourceType == 'Quote') {
                            if(record.get(qField.Name) != oldMap.get((Id)record.get('Id')).get(qField.Name)) {
                                idsForQuery.add((Id)record.get('Id'));
                                sourceIds.add((Id)record.get('Id'));
                                mapIdRecord.put((Id)record.get('Id'), record);
                            }
                        } else {
                            if(record.get(qField.OppSyncField__c) != oldMap.get((Id)record.get('Id')).get(qField.OppSyncField__c)) {
                                idsForQuery.add((Id)record.get('Id'));
                                sourceIds.add((Id)record.get('Id'));
                                mapIdRecord.put((Id)record.get('Id'), record);
                            }
                        }
                    }
                }
            } else {
                
                if(sourceType.contains('Line')) {
                    if(sourceType.contains('Quote')) {
                    	idsForQuery.add((Id)record.get('OpportunityLineItemId'));
                    } else {
                        idsForQuery.add((Id)record.get('Id'));
                    	isOppLineItem = true;
                    }
                } else {
                    idsForQuery.add((Id)record.get('Id'));
                }
                
                sourceIds.add((Id)record.get('Id'));
                mapIdRecord.put((Id)record.get('Id'), record);
                
            }

        }

        String operator;
        String operator2;
        String ids;
        String sourceIdsLine;

        if(idsForQuery.size() == 1) {
            operator = ' = ';
            ids= '\''+idsForQuery[0]+'\'';
        } else if(idsForQuery.size() > 1) {
            operator = ' IN ';
            List<String> tmp = new List<String>();
            for(Id id: idsForQuery) {
                tmp.add('\''+id+'\'');
            }

            ids = '('+String.join(tmp, ',')+')';
        } else {
            return;
        }

        if(sourceIds.size() == 1) {
            operator2 = ' = ';
            sourceIdsLine= '\''+sourceIds[0]+'\'';
        } else if(sourceIds.size() > 1) {
            operator2 = ' IN ';
            List<String> tmp = new List<String>();
            for(Id id: sourceIds) {
                tmp.add('\''+id+'\'');
            }

            sourceIdsLine = '('+String.join(tmp, ',')+')';
        } else {
            return;
        }

        system.debug(ids);

        List<String> std_fields_lst = new List<String>();
        String std_fields = '';
        if(sourceType.contains('Line')) {
            Map<String, Schema.SObjectField> fs = Schema.getGlobalDescribe().get(sourceType).getDescribe().fields.getMap();
            Map<String, Schema.SObjectField> fs_target = Schema.getGlobalDescribe().get(TYPE_DEPENDENCIES.get(sourceType)).getDescribe().fields.getMap();
            for(Schema.SObjectField f: fs.values()) {
                for(Schema.SObjectField f_t: fs_target.values()) {
                    if(f.getDescribe().getName() == f_t.getDescribe().getName() && f_t.getDescribe().getName() != 'TotalPrice' && f_t.getDescribe().isUpdateable()) {
                        std_fields_lst.add(f.getDescribe().getName());
                    }
                }
            }
        }
        
        std_fields = String.join(std_fields_lst, ',');
        
        System.debug('std_fields '+std_fields);

        String query = 'SELECT '+String.join(FIELDS_DEPENDENCIES.get(TYPE_DEPENDENCIES.get(sourceType)), ',')+' FROM '+TYPE_DEPENDENCIES.get(sourceType)+' WHERE '+filter+operator+ids;
        String sourceRecords = JSON.serialize(lstRecords); //'SELECT '+String.join(FIELDS_DEPENDENCIES.get(sourceType), ',')+' FROM '+sourceType+' WHERE ID'+operator2+sourceIdsLine;

        system.debug(query);
        system.debug(sourceType);
        system.debug(isOppLineItem);

        PostProcess(new List<String>{query, sourceRecords}, isOppLineItem, sourceType, std_fields);

    }

    public static void PostProcess(List<String> lstStr, Boolean isOppLineItem, String sourceType, String std_fields) {
        
        System.debug(sourceType);
        
        Boolean updt = true;
        Boolean updtSource = true;

        try{

            if(lstStr != null && lstStr.size() == 2) {
                
                if(sourceType == 'OpportunityLineItem') {
    				QteLineItemSyncTriggerV2Handler.stop();
                } else if (sourceType == 'QuoteLineItem') {
    				OppLineItemSyncTriggerV2Handler.stop();
                } else if (sourceType == 'Opportunity') {
    				QuoteSyncTriggerV2Handler.stop();
                } else if(sourceType == 'Quote') {
    				OppSyncTriggerV2Handler.stop();
                }

                List<SObject> lstTargets;
                Map<Id, SObject> mapIdRecord = new Map<Id, SObject>();

                for(SObject rcrd: (List<SObject>)JSON.deserialize(lstStr[1], List<SObject>.class)/*Database.query(lstStr[1])*/) {
                    mapIdRecord.put((Id)rcrd.get('Id'), rcrd);
                }

                if(isOppLineItem) {
                    List<Id> lstIds = new List<Id>();
                    for(SObject rcd: mapIdRecord.values()) {
                        lstIds.add((Id)rcd.get('Id'));
                    }
                    
                    lstTargets = Database.query('SELECT '+String.join(FIELDS_DEPENDENCIES.get('QuoteLineItem'), ',')+' FROM QuoteLineItem WHERE OpportunityLineItemId IN :lstIds');

                } else {
                    lstTargets = Database.query(lstStr[0]);
                }

                system.debug(lstTargets);
                system.debug(mapIdRecord);

                Boolean future = false;

                List<Opportunity> lstOpp = new List<Opportunity>();

                for(SObject target: lstTargets) {

                    Id id = (Id)target.get('Id');
                    Schema.SObjectType sobjectType = id.getSObjectType();
                    
                    System.debug(sobjectType);

                    if(sobjectType == Quote.sObjectType) {
                        //target is quote so the source is opportunity || we must get the fields chhges from the source to the target
                        //get the opportunity where the sync quote is the target
                        for(SObject source: mapIdRecord.values()) {
                            if(source.get('SyncedQuoteId') == target.get('Id')) {
                                //copy values
                                for(Quote_Field__c qField: lstQuoteFields) {
                                    target.put(qField.Name, source.get(qField.OppSyncField__c));
                                }
                            }
                        }
                    } else if(sobjectType == Opportunity.sObjectType) {
                        future = true;
                        //target is opportunity so the source is quote || we must get the fields chhges from the source to the target
                        //get the quote where the opportunityId is the target

                        Opportunity opp = new Opportunity();

                        for(SObject source: mapIdRecord.values()) {
                            if(source.get('OpportunityId') == target.get('Id')) {
                                //copy values
                                opp.Id = (Id)target.get('Id');
                                for(Quote_Field__c qField: lstQuoteFields) {
                                    opp.put(qField.OppSyncField__c, source.get(qField.Name));
                                }
                            }
                        }

                        lstOpp.add(Opp);

                    } else if(sobjectType == OpportunityLineItem.sObjectType) {
                        //target is OpportunityLineItem so the source is QuoteLineItem || we must get the fields chhges from the source to the target
                        //get the QuoteLineItem
                        for(SObject source: mapIdRecord.values()) {
                            if((source.get('OpportunityLineItemId') == target.get('Id') /*&& source.get('CreatedDate') != source.get('LastModifiedDate'))*/ || isTest)) {
                                //copy values
                                for(QuoteLineItem_Field__c qField: lstQuoteLineFields) {
                                    target.put(qField.OppLineSyncField__c, source.get(qField.Name));
                                }

                                for(String field: std_fields.split(',')) {
                                    System.debug('field '+field);
                                    target.put(field, source.get(field));
                                }
                            }
                        }
                    } else if(sobjectType == QuoteLineItem.sObjectType) {
                        //target is QuoteLineItem so the source is OpportunityLineItem || we must get the fields chhges from the source to the target
                        //get the OpportunityLineItem
                        for(SObject source: mapIdRecord.values()) {
                            System.debug(source);
                            if((source.get('Id') == target.get('OpportunityLineItemId') /*&& source.get('CreatedDate') != source.get('LastModifiedDate'))*/ || isTest)) {
                                //copy values
                                for(QuoteLineItem_Field__c qField: lstQuoteLineFields) {
                                    target.put(qField.Name, source.get(qField.OppLineSyncField__c));
                                }

                                for(String field: std_fields.split(',')) {
                                    target.put(field, source.get(field));
                                }
                            }
                        }
                    }

                }
                
                if(!future) {
                    update lstTargets;
                } else {
                    update lstOpp;
                    //updateF(JSON.serialize(lstTargets));
                }
               
            }

        } catch(Exception e) {
            System.debug(e.getLineNumber()+' '+e.getMessage()+' '+e.getCause());
        }

    }

    @future
    private static void updateF(String lst) {
        List<SObject> objs = (List<SObject>)JSON.deserialize(lst, List<SObject>.class);
        update objs;
    }

}