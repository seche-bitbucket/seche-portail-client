/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-07-19
* @modified       2018-07-19
* @systemLayer    Test Class         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Test class for the Batch Class that handles amendments with different accounts from the same contract
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class Test_CTR_BatchCleanAME {
    @isTest static void testBatchExecute(){
        Profile directp = [select id from profile where name = :'Directeur commercial' limit 1];
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = directp.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        
        insert director;
        Profile p = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];
        
        String testemail = 'Commercial-_User_test@test.com';
        User Commercial = new User(profileId = p.id, username = testemail, email = testemail,
                                   emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                   languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                   alias='cs', lastname='lastname2', IsActive=true);
        insert Commercial;
        Profile p2 = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];
        
        String testemail3 = 'admin-_User_test@test.com';
        User Admin = new User(profileId = p2.id, username = testemail3, email = testemail3,
                              emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                              languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                              alias='cs', lastname='lastname2', IsActive=true);
        insert Admin;
        System.RunAs(Admin) {
            
            Account ac = new Account();
            ac.Name = 'testAccName';
            ac.CustomerNature__c = 'Administration';
            ac.Industry = 'Trader';
            ac.CurrencyIsoCode = 'EUR';
            ac.Producteur__c = false;
            ac.BillingPostalCode = '00000';
            ac.NAF_Number__c = '1234A';
            insert ac;
            
            Account ac2 = new Account();
            ac2.Name = 'Account2';
            ac2.CustomerNature__c = 'Administration';
            ac2.Industry = 'Medical';
            ac2.CurrencyIsoCode = 'EUR';
            ac2.Producteur__c = false;
            ac2.BillingPostalCode = '00000';
            ac2.NAF_Number__c = '1234A';
            insert ac2;
            Account ac3 = new Account();
            ac3.Name = 'Account3';
            ac3.CustomerNature__c = 'Administration';
            ac3.Industry = 'Medical';
            ac3.CurrencyIsoCode = 'EUR';
            ac3.Producteur__c = false;
            ac3.BillingPostalCode = '00000';
            ac3.NAF_Number__c = '1234A';
            insert ac3;
            List<Contract__c> contractsFromAcc1=new List<Contract__c>();
            List<Contract__c> contractsFromAcc2=new List<Contract__c>();
            for(integer i=0;i<100;i++){
                Contract__c c = new Contract__c();
                c.Account__c=ac.id;
                c.RegroupmentIndex__c=null;
                c.Status__c=null;
                c.AssociatedSalesRep__c=Commercial.Id;
                c.name='Contract Test'+i;
                contractsFromAcc1.add(c);
            }
            for(integer i=1;i<101;i++){
                Contract__c c = new Contract__c();
                c.Account__c=ac2.id;
                c.RegroupmentIndex__c=1;
                c.Status__c=null;
                c.AssociatedSalesRep__c=director.Id;            
                c.name='Contract Test'+i*100;
                contractsFromAcc2.add(c);
            }
            
            insert contractsFromAcc1;
            insert contractsFromAcc2;
            List<Amendment__c> Amendments = new List<Amendment__c>();
            for(Contract__c contract:contractsFromAcc1){
                Amendment__c am = new Amendment__c();
                am.Name='amendment 1';
                am.Contrat2__c=contract.id;
                am.Account__c=ac2.id;
                am.WasteLabel__c='conditionnement amiante';
                am.Nature__c='Conditionnement';
                am.PriceNextYear__c=120;
                am.PriceCurrentYear__c=150;
                am.QuantityCurrentYear__c=10;
                Amendment__c am2 = new Amendment__c();
                am2.Name='amendment 2';
                am2.Contrat2__c=contract.id;
                am2.Account__c=ac.id;
                am2.WasteLabel__c='Transport amiante';
                am2.PriceNextYear__c=120;
                am2.PriceCurrentYear__c=150;
                am2.QuantityCurrentYear__c=20;
                am2.Nature__c='Prestation';
                Amendment__c am3 = new Amendment__c();
                am3.Name='amendment 3';
                am3.Contrat2__c=contract.id;
                am3.Account__c=ac3.id;
                am3.Nature__c='Traitement';
                am3.WasteLabel__c='traitement amiante';
                am3.PriceCurrentYear__c=120;
                am3.QuantityCurrentYear__c=40;
                Amendments.add(am);
                Amendments.add(am2);
                Amendments.add(am3);
            }     
            for(Contract__c contract:contractsFromAcc2){
                Amendment__c am = new Amendment__c();
                am.Name='amendment 4';
                am.Contrat2__c=contract.id;
                am.Account__c=ac.id; 
                am.WasteLabel__c='conditionnement amiante';
                am.Nature__c='Conditionnement';
                am.PriceCurrentYear__c=120;
                am.QuantityCurrentYear__c=10;
                Amendment__c am2 = new Amendment__c();
                am2.Contrat2__c=contract.id;
                am2.Name='amendment 5';
                am2.Account__c=ac.id;
                am2.WasteLabel__c='Transport amiante';
                am2.PriceCurrentYear__c=120;
                am2.QuantityCurrentYear__c=20;
                am2.Nature__c='Prestation';
                Amendment__c am3 = new Amendment__c();
                am3.Contrat2__c=contract.id;
                am3.Name='amendment 6';
                am3.Account__c=ac3.id;
                am3.Nature__c='Traitement';
                am3.WasteLabel__c='traitement amiante';
                am3.PriceCurrentYear__c=120;
                am3.QuantityCurrentYear__c=40;
                Amendments.add(am);
                Amendments.add(am2);
                Amendments.add(am3);
            }
            if(Amendments.size()>0){
                insert Amendments;
            }

        }
        Test.startTest();
        CTR_BatchCleanAME batch = new CTR_BatchCleanAME(); 
        Id batchId = Database.executeBatch(batch);
        Test.stopTest();
        List<Contract__c> Listcontracts=[SELECT Opportunity__c, Name,Account__c, Id,Account__r.Name FROM Contract__c];
        System.assertEquals(600,ListContracts.size());
        List<Amendment__c> AmendmentsList=[SELECT Name,Account__c, Id,Account__r.Name FROM Amendment__c];
        System.assertEquals(600,AmendmentsList.size());
        
        
    }
}