/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : ConnectiveAsyncSendReminder
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ConnectiveAsyncSendReminder
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 17-08-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
public with sharing class ConnectiveAsyncSendReminder implements Queueable, Database.AllowsCallouts  {

    Map<Id,Quote> scope = new Map<Id,Quote>();
    List<Quote> scopeSendEmail = new List<Quote>();

    public ConnectiveAsyncSendReminder(List<Quote> scope, List<Quote> allScope){
        this.scope=new Map<Id,Quote>(scope);
        this.scopeSendEmail = allScope;
    }

    public void execute(QueueableContext context) {
        Id currentId;
        //Integer NextYear = System.today().year()+1;
        Integer NextYear = 2021;
        try{
            for(Id qtId : scope.keySet()){
                currentId= qtId;
                String result = ConnectiveApiManager.sendRedminder(qtId);
                if(result == 'OK' || Test.isRunningTest()){
                    Quote qt = scope.get(qtId);
                    qt.NbSendReminder__c = qt.NbSendReminder__c+1;
                    //update qt;
                    updateLockedRecord updateLockedRecord = new updateLockedRecord();
                    updateLockedRecord.updateRecord(qt);
                    String subject = 'RELANCE : Convention '+NextYear+' - '+qt.Opportunity.Filiale__c+' - '+qt.Account.Name;
                    Task t = LC_EmailSend_AugTarifCtrl.createEmailActivity(subject,'Relance envoyee au client',qt.contactId,qtId);
                    insert t;
                }
                scope.remove(qtId);
                
                break;
            }
        }
        catch (Exception e){
            System.debug(LoggingLevel.ERROR, e.getMessage());
            if(currentId != null){
                this.scope.remove(currentId);
            }
        }
        finally{
            if(!Test.isRunningTest() && scope.size()>0){
                ID jobDoc = System.enqueueJob(new ConnectiveAsyncSendReminder(this.scope.values(),scopeSendEmail));
            }
        }
    }

    without sharing class updateLockedRecord {	  
        public void updateRecord(Quote q) { 		
            update q; 
        }
    }
}