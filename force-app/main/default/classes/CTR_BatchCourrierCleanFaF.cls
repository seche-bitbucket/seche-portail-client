global class CTR_BatchCourrierCleanFaF implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Id, Name,Contrat2__c, ByPassContract__c, PriceCurrentYear__c, PriceNextYear__c FROM Amendment__c Where ByPassContract__c=true AND PriceNextYear__c=Null AND Contrat2__r.Opportunity__c!=Null';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Amendment__c> scope)
    {                 
        // je recupère tt les avenants avec face a face puis je regarde tt les contrats de ses avenants 
        // puis tt les opportunitées liée a ses contrats
        // puis je supprime tt ses opportunitées.   
        List<Amendment__c> AmendmentList=new List<Amendment__c>();
        Set<ID> Contracts=new Set<ID>();
        for(Amendment__c am : scope)
        {   
            Contracts.add(am.Contrat2__c);
        }
        System.debug(Contracts.size());
        List<Contract__c> ContractsToDelete=[SELECT Etat__c,Status__c,Name,Account__c, Id,Opportunity__c
                                             FROM Contract__c WHERE ID IN:Contracts];
        System.debug(ContractsToDelete.size());
        Set<Id> Opportunities=new Set<ID>();
        for(Contract__c c : ContractsToDelete)
        {   System.debug(c.Opportunity__c);
            Opportunities.add(c.Opportunity__c);
        }
        System.debug(Opportunities.size());
        List<Opportunity> OpportunitiesToUpdate=[SELECT ID,Name,Account.name,TECH_FaceaFace__c FROM Opportunity Where ID IN:Opportunities AND TECH_FaceaFace__c!=true];
        for(Opportunity o:OpportunitiesToUpdate){
            o.Name='Convention 2019 F&F - '+o.Account.Name;
            o.TECH_FaceaFace__c=true;
        }
        Update OpportunitiesToUpdate;
        //if(OpportunitiesToDelete.size()>0){
            //Delete OpportunitiesToDelete;
        //}
        List<Quote> DevisaSupprimer=[Select Id, Name, CreatedDate,OpportunityId,Status FROM Quote Where OpportunityId In:OpportunitiesToUpdate];
        if(DevisaSupprimer.size()>0){
            Delete DevisaSupprimer;
        }

    }
    global void finish(Database.BatchableContext BC)
    {              
        if(!Test.isRunningTest()){
            List<OpportunityLineItem> Olis=[SELECT Id, Name,TECH_CatalogueMatch__c, Features__c,N_CAP__c  FROM OpportunityLineItem Where TECH_CatalogueMatch__c !=null];
            if(olis.size()>0){
                BatchProductInfoToOLI batch =new BatchProductInfoToOLI();
                id BatchId= Database.executeBatch(batch);
            }else{
                CourrierConventionBatch batch =new CourrierConventionBatch();
                id BatchId= Database.executeBatch(batch);
            }
        }
        
        // tt les devis courrier et remplir un champ qui va lancer le batch
        //List<Contract__c> ContractsToGenerateOnce=[SELECT Etat__c,Status__c,TurnoverCurrentYear__c,Name,Account__c, Id,Opportunity__c
        //                                    FROM Contract__c Where ];
        /*Integer year=System.today().Year()+1; 
        string inVar='Courrier '+year;
        string tempInput = '%' + inVar + '%';
        List<Quote> DevisCourrier=[Select Id, Name, CreatedDate,OpportunityId,Status,(Select Id From Attachments) 
                                   FROM Quote WHERE NAME Like :tempInput and status!='Présenté'];
        List<ID> OpportunitiesIds=new List<Id>();
        for(Quote q : DevisCourrier){
            if(q.Attachments.size()<1){
                //q.TECH_CongaAutomatedSending__c=true;
                //q.Status='Présenté';
                OpportunitiesIDs.add(q.OpportunityID);
            }
        }
        List<Contract__c> ContractsToGenerateOnce=[SELECT Etat__c,Status__c,TurnoverCurrentYear__c,Name,Account__c, Id,Opportunity__c,Account__r.Name
                                                   FROM Contract__c Where Opportunity__c IN: OpportunitiesIDs ORDER BY Account__c];
        Map<String,ID> OpportunitiesMap=new Map<String,ID>();
        for(Contract__c c:ContractsToGenerateOnce){
            for(Contract__c con:ContractsToGenerateOnce){
                if(c.Account__c==con.Account__c){
                    OpportunitiesMap.put(con.Account__r.Name,c.Opportunity__c);
                }
            }
        }
        List<ID> OpportunitiesID=new List<ID>();
        for(String i:OpportunitiesMap.keyset()){
            OpportunitiesID.add(OpportunitiesMap.get(i));
        }
        List<Quote> DevisCourrieraGenerer=[Select Id, Name, CreatedDate,OpportunityId,Status FROM Quote Where OpportunityId In:OpportunitiesID];
        for(Quote q : DevisCourrieraGenerer){
                q.TECH_CongaAutomatedSending__c=true;
        }
        Update DevisCourrieraGenerer;*/
    }
}