/** * @author Thomas Prouvot * @date Creation 07.09.2016 * @description AccountTriggerTest – TriggerTestClass */
@isTest
public class AccountTriggerTest {
	@isTest(seeAllData=true)
    static void testAccount(){
        
        final integer STRING_LENGTH = 5;
        final double MIN = Math.pow(10,STRING_LENGTH -1);
        final double MAX = Math.pow(10,STRING_LENGTH) - 1;
        
        Test.startTest();
        Integer nb = 50;        
        Account parentAc = new Account(Name='TestAccount 00000001',
                                       Producteur__c = false,
                                       BillingPostalCode='69800',
                                       TVA_Number__c = '2434234',
                                       NAF_Number__c = '1234B',
                                       SIRET_Number__c = '012345678'+String.valueOf(Math.Round(Math.Random() * (MAX-MIN) + MIN)));
        insert parentAc;
        
        List<Account> childAccounts = new List<Account>();
        for(integer i = 0;i < nb;i++){
            childAccounts.add(new Account(Name='TestAccount'+ i,
                                          BillingPostalCode='69800',
                                          TVA_Number__c = '2434234' + i,
                                          SIRET_Number__c = '012345678'+String.valueOf(Math.Round(Math.Random() * (MAX-MIN) + MIN)),
                                          ParentId = parentAc.Id,
                                          NAF_Number__c = '1234B',
                                          Maison_mere__c = parentAc.Id));
        }
        insert childAccounts;
                
        //on recharge l'objet après insertion
        parentAc = [SELECT Id,TECH_NbComptesEnfants__c FROM Account WHERE Id=:parentAc.Id ];
        System.assertEquals(nb, parentAc.TECH_NbComptesEnfants__c);
        
        delete childAccounts;
        //on recharge l'objet après supression
        parentAc = [SELECT Id,TECH_NbComptesEnfants__c FROM Account WHERE Id=:parentAc.Id ];
        System.assertEquals(0, parentAc.TECH_NbComptesEnfants__c);
        
        List<Account> accountUpdate = new List<Account>();
        for(integer i = 0;i < nb;i++){
            accountUpdate.add(new Account(Name='MonTest 000000000000000000000000001'+ i*2,
                                          BillingPostalCode='69890',
                                          TVA_Number__c = '42343242' + i*2,
                                          SIRET_Number__c = '012345678'+String.valueOf(Math.Round(Math.Random() * (MAX-MIN) + MIN)),
                                          NAF_Number__c = '1234B',
                                          ParentId = parentAc.Id));
        }
        insert accountUpdate;
		for(Account a : accountUpdate){
            a.Maison_mere__c = parentAc.Id;
        }        
        update accountUpdate;
        //on recharge l'objet après avoir été mis à jour par le trigger
        parentAc = [SELECT Id,TECH_NbComptesEnfants__c FROM Account WHERE Id=:parentAc.Id ];
        System.assertEquals(nb , parentAc.TECH_NbComptesEnfants__c);
        
        for(Account ac : accountUpdate){
            ac.Maison_mere__c = null;
        }
        update accountUpdate;
        //on recharge l'objet après avoir été mis à jour par le trigger
        parentAc = [SELECT Id,TECH_NbComptesEnfants__c FROM Account WHERE Id=:parentAc.Id ];
        System.assertEquals(0, parentAc.TECH_NbComptesEnfants__c);

        Test.stopTest();        
    }
}