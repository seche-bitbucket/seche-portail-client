@isTest
public class LC_AvenantsList_AugTarifCtrlTest {
   /* @isTest
    static void testSubmitAMEForValidation(){
        LC_AvenantsList_AugTarifCtrl ctrl = new LC_AvenantsList_AugTarifCtrl();
        List<Contract__c> listC = new  List<Contract__c>();
        List<Amendment__c> listA = new  List<Amendment__c>();

        Contract__c c1 = new Contract__c();   
        c1.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
        c1.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport;       
        
        Contract__c c2 = new Contract__c();   
        c2.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c2.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 

        Contract__c c3 = new Contract__c();   
        c3.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c3.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c3.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 

       	listC.add(c1); listC.add(c2);  listC.add(c3);
       	insert listC;
		
		Amendment__c a1 = new Amendment__c();
        a1.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;
        a1.Contrat2__c = listC[0].Id ;
        a1.PriceNextYear__c = 10;
        
        Amendment__c a2 = new Amendment__c();
        a2.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
        a2.Contrat2__c = listC[1].Id ;
        a2.PriceNextYear__c = 10; 

        Amendment__c a3 = new Amendment__c();
        a3.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a3.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
        a3.Contrat2__c = listC[2].Id ;
        a3.PriceNextYear__c = 10;

        listA.add(a1); listA.add(a2); listA.add(a3);
        insert listA;

        LC_AvenantsList_AugTarifCtrl response = LC_AvenantsList_AugTarifCtrl.submitAMEForValidation(listC);
        
        System.assertEquals(response.message , Label.ContractsSubmittedForValidation);
        System.assertEquals(response.toastMode , Label.ToastMode_Success);

    }

    @isTest
    static void testSubmitAMEForValidationError(){
        LC_AvenantsList_AugTarifCtrl ctrl = new LC_AvenantsList_AugTarifCtrl();
        List<Contract__c> listC = new  List<Contract__c>();
        List<Amendment__c> listA = new  List<Amendment__c>();

        Contract__c c1 = new Contract__c();   
        c1.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;               
        
        Contract__c c2 = new Contract__c();   
        c2.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c2.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 

        Contract__c c3 = new Contract__c();   
        c3.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c3.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c3.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 

       	listC.add(c1); listC.add(c2);  listC.add(c3);
       	insert listC;
		
		Amendment__c a1 = new Amendment__c();
        a1.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;
        a1.Contrat2__c = listC[0].Id ;
        a1.PriceNextYear__c = 10;
        
        Amendment__c a2 = new Amendment__c();
        a2.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
        a2.Contrat2__c = listC[1].Id ;
        a2.PriceNextYear__c = 10; 

        Amendment__c a3 = new Amendment__c();
        a3.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a3.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
        a3.Contrat2__c = listC[2].Id ;
        
        listA.add(a1); listA.add(a2); listA.add(a3);
        insert listA;

        LC_AvenantsList_AugTarifCtrl response = LC_AvenantsList_AugTarifCtrl.submitAMEForValidation(listC);
        
        System.debug(response);
        System.assertEquals(response.toastMode , Label.ToastMode_Warning);

    }*/
    
    @isTest
    static void testGetAmendmentById(){
        LC_AvenantsList_AugTarifCtrl ctrl = new LC_AvenantsList_AugTarifCtrl();
        
        List<Contract__c> listC = new  List<Contract__c>();
        List<Amendment__c> listA = new  List<Amendment__c>();

        Contract__c c1 = new Contract__c();   
        c1.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;               
        
        Contract__c c2 = new Contract__c();   
        c2.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c2.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 

        Contract__c c3 = new Contract__c();   
        c3.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c3.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c3.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 

       	listC.add(c1); listC.add(c2);  listC.add(c3);
       	insert listC;
		
		Amendment__c a1 = new Amendment__c();
        a1.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;
        a1.Contrat2__c = listC[0].Id ;
        a1.PriceNextYear__c = 10;
        
        Amendment__c a2 = new Amendment__c();
        a2.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
        a2.Contrat2__c = listC[1].Id ;
        a2.PriceNextYear__c = 10; 

        Amendment__c a3 = new Amendment__c();
        a3.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a3.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
        a3.Contrat2__c = listC[2].Id ;
        
        listA.add(a1); listA.add(a2); listA.add(a3);
        insert listA;
        
        listA = [select Id from Amendment__c];
		list<String> listIds = new list<String>();
        listIds.add(listA[0].Id);
        listIds.add(listA[1].Id);
        listIds.add(listA[2].Id);
        list<Amendment__c> response = LC_AvenantsList_AugTarifCtrl.getAmendmentById(listIds);
        System.assertEquals(response.size() , 3);
        
        List<Object> defs = LC_AvenantsList_AugTarifCtrl.getDefinitions();
        List<Custom_Column__mdt> columns = LC_AvenantsList_AugTarifCtrl.getColumnsLabels();
        
        System.assertNotEquals(0, defs.size());
        System.assertNotEquals(0, columns.size());
        
    }

    @isTest
    static void testSetAmendmentsUnderContract(){
        List<Contract__c> listC = new  List<Contract__c>();
        List<Amendment__c> listA = new  List<Amendment__c>();

        Contract__c c1 = new Contract__c();   
        c1.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;               
        
        Contract__c c2 = new Contract__c();   
        c2.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c2.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 

        Contract__c c3 = new Contract__c();   
        c3.Filiale__c = String_Helper.CON_Filiale_Drimm;       
        c3.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;         
        c3.Type_de_convention__c = String_Helper.CON_TypeDeConvention_DndTraitementTransport; 

       	listC.add(c1); listC.add(c2);  listC.add(c3);
       	insert listC;

        Amendment__c a1 = new Amendment__c();
        a1.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a1.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE;
        a1.Contrat2__c = listC[0].Id ;
        a1.PriceCurrentYear__c = 10;
        
        Amendment__c a2 = new Amendment__c();
        a2.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a2.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
        a2.Contrat2__c = listC[1].Id ;
        a2.PriceCurrentYear__c = 10; 

        Amendment__c a3 = new Amendment__c();
        a3.Filiale__c = String_Helper.CON_Filiale_Drimm;
        a3.Filiere__c = String_Helper.CON_Filiere_CentreDeTriDAE; 
        a3.Contrat2__c = listC[2].Id;
        a2.PriceCurrentYear__c = 10;

        listA.add(a1); listA.add(a2); listA.add(a3);
        insert listA;
    }
}