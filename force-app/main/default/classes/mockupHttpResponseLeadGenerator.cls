@isTest
global class mockupHttpResponseLeadGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        if(req.getMethod() == 'POST'){
            System.assertEquals('POST', req.getMethod());
        }else if(req.getMethod() == 'GET'){
            System.assertEquals('GET', req.getMethod());
        }
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"attributes" : {"type" : "Lead", "url" : "/services/data/v20.0/sobjects/Lead/00Q4E000002HTTU" },"TECH_UID__c" : "aea9619dcc3e438fa3","LastName" : "Valentin","FirstName" : "Karine","Salutation" : "Mme","Company" : "Séché","Phone" : "null","Email" : "sechecrm69@gmail.com","Website" : "null","Code_NAF__c" : "null","N_SIRET__c" : "null","Street" : "null","City" : "null","PostalCode" : "null","State" : "null","Country" : "null","IsInterestedBySeche__c" : "true","IsInterested__c" : "true","Tonnage__c" : "null","GatheringFrequency__c" : "null","InterestedOtherServices__c" : "null","Id" : "00Q4E000002HTTU" }');
        res.setStatusCode(200);
        return res;
        }
     
}