public with sharing class VFC_CreateOppRecord {
public String accountId { get; set; }
public String contactId { get; set; }
public String recordTypeId {get; set;}

    public VFC_CreateOppRecord() {
        accountId = ApexPages.currentPage().getParameters().Get('accountId');
        contactId = ApexPages.currentPage().getParameters().Get('contactId');
	    recordTypeId = ApexPages.currentPage().getParameters().Get('recordTypeId');
    }
}