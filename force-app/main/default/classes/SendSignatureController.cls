public with sharing class SendSignatureController {
    @AuraEnabled
    public String ID {get; set;}
    
    @AuraEnabled
    public String UID {get; set;}
    
    @AuraEnabled
    public String ContactId {get;set;}
    
    @AuraEnabled
    public String LastName {get; set;}
    
    @AuraEnabled
    public String FirstName {get; set;}
    
    @AuraEnabled
    public String Phone {get; set;}
    
    @AuraEnabled
    public String Email {get; set;}
    
    @AuraEnabled
    public String QuoteName {get; set;}  
    
    @AuraEnabled
    public String RequestResult {get; set;}
    
    @AuraEnabled
    public String ContactNameToUpdate{get;set;}
    
    @AuraEnabled
    public static SendSignatureController getQuoteId(String Uid,String Id){
        SendSignatureController ssc=new SendSignatureController();
        System.debug(Uid+'id'+Id);
        if(Id!=null && Uid!=null){
            String token = getTokenAccess();
            if(token != 'ERROR'){
                Quote q= new Quote();
                q = getCalloutResponseContents(ID,token);
                if(q!=Null){
                    if(q.TECH_UID__c==UID){
                        if(getDocumentForSignature(ID,token)){
                            ssc.RequestResult='ALREADYSENT';
                        }else{
                            ssc.QuoteName=q.QuoteCode__c;
                            if(q.ContactId!=Null){
                                Contact c=getContact(q.ContactId,token);
                                ssc.Email=c.Email;
                                ssc.FirstName=c.FirstName;
                                ssc.LastName=c.LastName;
                                ssc.Phone=c.Phone;
                                ssc.ContactId=q.ContactId;
                            }else{
                                ssc.ContactId='Unknown';
                            }
                            ssc.RequestResult='OK';
                        }
                    }else{
                        ssc.RequestResult='UIDNOTOK';
                    }
                }else{
                    ssc.RequestResult='ERROR';
                }
            }else{
                ssc.RequestResult='TOKENNOTOK';
            }
        }else{
            ssc.RequestResult='IDNULL';
        }
        return ssc;
    }
    
    public static String getTokenAccess() {
        System.debug('..................GenerateToken');
        String endPointURL = Label.ORG_URL_LOGIN+'/services/oauth2/token';
        String tokenSF = '';
        Boolean found = false;
        String username = Label.USR_API_Portal;
        String password = Label.USR_API_Password;
        String clientID = Label.API_ClientID;
        String clientSecret = Label.API_Client_Secret ;
        // Instantiate a new http object
        Http h = new Http();
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPointURL+'?grant_type=password&client_id='+clientID+'&client_secret='+clientSecret+'&username='+username+'&password='+password);
        req.setMethod('POST');
        // Send the request, and return a response
        HttpResponse res = h.send(req);
        if(res.getStatusCode() == 200){
            //Parse JSON to retrive Token
            System.JSONToken token;
            string text;
            JSONParser parser = JSON.createParser(res.getBody());
            System.debug(res.getBody());
            parser.nextToken(); // Eat first START_OBJECT {
            while((token = parser.nextToken()) != null && !found) {
                text = parser.getText();
                if (token == JSONToken.FIELD_Name && text == 'access_token') {
                    token=parser.nextToken();
                    tokenSF = parser.getText();
                    found = true;
                }
            }
            
        }
        if(tokenSF != ''){
            return tokenSF;   
        }else{
            return 'ERROR';
        }
    }
    public static Contact getContact(String Id, String access_token){
        String endPointURL = Label.ORG_URL+'/services/data/v43.0/sobjects/Contact/'+ID+'?fields=FirstName,LastName,Phone,Email';
        Http h = new Http();
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPointURL);
        req.setMethod('GET');
        req.setHeader('Authorization','Bearer '+access_token);
        HttpResponse res = h.send(req);
        if(res.getStatusCode() == 200){
            System.debug('body for contactcallout'+res.getBody());
            return (Contact) System.JSON.deserialize(res.getBody(), Contact.class);
        }else{
            return null;
        }
    }
    public static Quote getCalloutResponseContents(String ID, String access_token){
        String endPointURL = Label.ORG_URL+'/services/data/v43.0/sobjects/Quote/'+ID+'?fields=TECH_UID__c,ContactId,QuoteCode__c,TECH_SendForSignature__c';
        Http h = new Http();
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPointURL);
        req.setMethod('GET');
        req.setHeader('Authorization','Bearer '+access_token);
        HttpResponse res = h.send(req);
        System.debug('body for quotecallout'+res.getBody());
        if(res.getStatusCode() == 200){
            return (Quote) System.JSON.deserialize(res.getBody(), Quote.class);
        }else{
            return null;
        }
    }
    public static Boolean getDocumentForSignature(String ID, String access_token){
        Boolean Exists=true;
        String endPointURL = Label.ORG_URL+'/services/data/v43.0/query/?q=SELECT+id+from+DocumentsForSignature__c+WHERE+TECH_ExternalId__c=\''+ID+'\'';
        Http h = new Http();
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPointURL);
        req.setMethod('GET');
        req.setHeader('Authorization','Bearer '+access_token);
        HttpResponse res = h.send(req);
        System.debug('body for quotecallout'+res.getBody());
        if('{"totalSize":0,"done":true,"records":[]}'==res.getBody()){
            Exists=false;
        }
        return Exists;
    }
    public static void UpdateQuote(Id qid,String Information,String access_token){
        String endPointURL = Label.ORG_URL+'/services/data/v43.0/sobjects/Quote/'+qid+'?_HttpMethod=PATCH';
        Http h = new Http();
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPointURL);
        req.setMethod('POST');
        req.setHeader('Authorization','Bearer '+access_token);
        req.setHeader('Content-Type', 'application/json');
        req.setBody('{ "TECH_SendForSignature__c" : true, "TECH_UnknownSignerInformation__c" : "'+Information+'" }');
        /*JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeBooleanField('TECH_SendForSignature__c', true);
        gen.writeStringField('TECH_UnknownSignerInformation__c', Information);
        gen.writeEndObject();              
        String JSONString = gen.getAsString();
        req.setBody(JSONString);*/
        HttpResponse res = h.send(req);
        System.debug(res.getBody());
    }
    @AuraEnabled
    public static String SendForSignature(String Id,String FirstName,String LastName,String Email,String Phone,String ContactId){
        String Result='';
        String token = getTokenAccess();
        if(token != 'ERROR'){
            Quote q=getCalloutResponseContents(Id,token);
            if(q.TECH_SendForSignature__c==false){
                if(ContactId=='Unknown'){
                    if(FirstName!=Null && FirstName!='' && LastName!='' && LastName!=Null && Email!=Null && Email!=''){
                        UpdateQuote(q.Id,FirstName+'='+LastName+'='+Email+'='+Phone,token);
                    }
                }else{
                    UpdateQuote(q.Id,ContactId,token);
                }
                Result='OK';
            }else{
                Result='AlreadySent';
            }
        }else{
            Result='Error';
        }
        return Result;
    }
}