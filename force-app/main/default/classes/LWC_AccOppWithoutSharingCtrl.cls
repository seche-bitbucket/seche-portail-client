/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : LWC_AccOppWithoutSharingCtrl
* -- - Author : MGR
* -- - Company : Capgemini
* -- - Purpose : LWC_AccOppWithoutSharingCtrl
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 08-06-2020 MGR 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/

public without sharing class LWC_AccOppWithoutSharingCtrl {

    @AuraEnabled(cacheable=true)
    public static List<AggregateResult> getOpportunitiesWithoutSharingByAccount(Id accId){  
        return [SELECT  Owner.Name OwnerName,
                        Owner.CompanyName CompanyName,
                        Owner.Phone phone,
                        count(Id) cnt,
                        Owner.MobilePhone mobilePhone,
                        OwnerID ownerID
                FROM Opportunity
                WHERE Accountid = :accId
                AND CreatedDate >= :System.Today().addYears(-1) AND CreatedDate < :System.Today().addDays(1)
                GROUP BY Owner.Name,
                        Owner.CompanyName,
                        Owner.Phone,
                        Owner.MobilePhone, 
                        OwnerID];
    }
}