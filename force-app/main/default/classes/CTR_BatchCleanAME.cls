/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-07-06
* @modified       2018-07-06
* @systemLayer    Batch Class         
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Batch Class to handle amendments with different accounts from the same contract:
				if there is amendments in a contract that have an account different from the contract
				we create another contract with the same name and link the amendment to the new contract
				we also set the status to all contracts to "Brouillon" 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global class CTR_BatchCleanAME implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {	// retrieve all the contracts imported from X3 
        String query = 'SELECT Opportunity__c, Name, Id FROM Contract__c WHERE Opportunity__c=Null ';
        return Database.getQueryLocator(query);
    } 
    global void execute(Database.BatchableContext BC, List<Contract__c> scope)
    {	
        // retrieve contracts and amendments linked to those contracts   
        List<Contract__c> Listcontracts=[SELECT Opportunity__c, Name,Account__c, Id,Account__r.Name,
                                         (SELECT Id, ByPassContract__c, QuantityPreviousYear__c, QuantityCurrentYear__c,
                                          PricePreviousYear__c, PriceCurrentYear__c, PriceNextYear__c, WasteLabel__c,
                                          AnalyticSection__c, Contrat2__c, CompletedAmadement__c, Nature__c, 
                                          AugmentationRatio__c, UnitNextYear__c, Conditionnement__c, Quantity2YearsAgo__c, 
                                          State__c, ActifAcceptationContrat__c, MinimumTonnage__c, CEECode__c, 
                                          Keywords__c, ArticleCode__c, ThirdPartyProducerCode__c, ThirdPartyProducerName__c,
                                          Chiffre_d_affaires_N_du_Contrat__c, TreatmentRange__c, EstimateTurnoverNextYear__c,
                                          Indicateur__c, Contact__c, UnitCurrentYear__c, UnitPreviousYear__c, ProducerDepartement__c, 
                                          Account__c, Regroupement__c, Price2YearsAgo__c, Name ,Contrat2__r.Name,Contrat2__r.Filiale__c,
                                          Contrat2__r.AssociatedSalesRep__c,Contrat2__r.ContractCode__c,Contrat2__r.ContactRelated__c,
                                          Contrat2__r.ContractType__c,Contrat2__r.account__c,Account__r.Name FROM Avenants__r Order By Account__c)
                                         FROM Contract__c WHERE ID IN:Scope];
        Map<String,List<Amendment__c>> firstMap=new Map<String,List<Amendment__c>>();
        List<Amendment__c> AmendmentsToDelete=new List<Amendment__c>();
        // filter the amendments that have different account from their proper contracts
        // and store them in a map with the contract name as the key
        Set<Contract__c> ContractsToUpdateAccEmpty=new Set<Contract__c>();
        for(Contract__c c:Listcontracts){
            List<Amendment__c> Listam=new List<Amendment__c>();
            for(Amendment__c am:c.Avenants__r){
                if(c.Account__c!=null){     
                    if( c.Account__c!=am.Account__c){
                        listam.add(am);
                        AmendmentsToDelete.add(am);
                    }
                }else{
                    c.Account__c=am.Account__c;
                    ContractsToUpdateAccEmpty.add(c);
                }
            }
            if(Listam.size()>0){
                firstMap.put(c.Name,Listam);
            }
        }
        if(ContractsToUpdateAccEmpty.size()>0){
            List<Contract__c> l=new List<Contract__c>();
            l.addAll(ContractsToUpdateAccEmpty);
            Update l;
        }
        Map<String,Map<Id,Set<Amendment__c>>> AmendmentsByAccountAndContract=new Map<String,Map<Id,Set<Amendment__c>>>();
        Map<Id,Set<Amendment__c>> AmendmentsByAccount=new Map<Id,Set<Amendment__c>>();
        // group the amendments for every contract and regroup them by account so the contracts created 
        // are unique 
        
        for(String c:firstMap.keyset()){
            for(Integer i=0; i < firstMap.get(c).size(); i++){
                SET<Amendment__c> AmendmentListToMap=new SET<Amendment__c>();
                AmendmentListToMap.add(firstMap.get(c)[i]);
                for(Integer k = i+1; k < firstMap.get(c).size(); k++) {
                    if(firstMap.get(c)[i].Account__c == firstMap.get(c)[k].Account__c){
                        AmendmentListToMap.add(firstMap.get(c)[k]);
                    }
                }
                if(!AmendmentsByAccount.containskey(firstMap.get(c)[i].Account__c)){
                    AmendmentsByAccount.put(firstMap.get(c)[i].Account__c,AmendmentListToMap);
                }
            }
            AmendmentsByAccountAndContract.put(c,AmendmentsByAccount);
            AmendmentsByAccount=new Map<Id,Set<Amendment__c>>();
        }        
        List<Contract__c> ContractsToInsert=new List<Contract__c>();
        List<Amendment__c> AmendmentsToInsert=new List<Amendment__c>();
        // create the contracts prime that will be linked to the amendments that have different accounts
        for(String Contract:AmendmentsByAccountAndContract.keyset()){
            for(String Acc:AmendmentsByAccountAndContract.get(Contract).keyset()){
                Contract__c c=new Contract__c();
                c.Name=Contract;
                c.Account__c=acc;
                c.Status__c='Brouillon';
                c.TECH_ContractExternalId__c=Contract+acc;
                for(Amendment__c am:AmendmentsByAccountAndContract.get(Contract).get(Acc)){
                    c.Filiale__c=am.Contrat2__r.Filiale__c;
                    c.AssociatedSalesRep__c=am.Contrat2__r.AssociatedSalesRep__c;
                    c.ContractCode__c=am.Contrat2__r.ContractCode__c;
                    c.ContactRelated__c=am.Contrat2__r.ContactRelated__c;
                    c.ContractType__c=am.Contrat2__r.ContractType__c;
                    c.OwnerID=am.Contrat2__r.AssociatedSalesRep__c;
                    break;
                }
                ContractsToInsert.add(c);
            }
        }
        if(ContractsToInsert.size()>0){
            insert ContractsToInsert;
        }
        // create the amendments cloned and assign them to the correct contracts and delete previous amendments
        for(String Contract:AmendmentsByAccountAndContract.keyset()){
            for(String Acc:AmendmentsByAccountAndContract.get(Contract).keyset()){
                ID ContractId=null;
                for(Contract__c c:ContractsToInsert){
                    if(c.TECH_ContractExternalId__c==Contract+acc){
                        ContractId=c.Id;
                    }
                }
                for(Amendment__c am:AmendmentsByAccountAndContract.get(Contract).get(Acc)){
                    Amendment__c amToInsert=am.clone(false,true,false,false);
                    amToInsert.Contrat2__c=ContractId;
                    AmendmentsToInsert.add(amToInsert);  
                }
            }
        }
        if(AmendmentsToInsert.size()>0){
            insert AmendmentsToInsert;
        }
        if(AmendmentsToDelete.size()>0){
            Delete AmendmentsToDelete;
        }
        // this part updates the CAN price for the contracts imported and sets the status to brouillon and also adds the % wanted (6% for 2019)
        // also puts the pricecurrentyear to 1 if its null or at 0 for all amendments
        List<Contract__c> AllContracts=new List<Contract__c>();
        List<Amendment__c> AmendmentsToUpdate=new List<Amendment__c>();
        AllContracts.addAll(Listcontracts);
        AllContracts.addAll(ContractsToInsert);
        List<Contract__c> ContractsToUpdate=[SELECT Status__c,Name,Account__c, Id,Account__r.Name,
                                             (SELECT Id,PriceCurrentYear__c,ByPassContract__c,QuantityCurrentYear__c FROM Avenants__r Order By Account__c)
                                             FROM Contract__c WHERE ID IN:AllContracts];
        for(Contract__c c:ContractsToUpdate){
            for(Amendment__c am:c.Avenants__r){
                if(am.PriceCurrentYear__c==null || am.PriceCurrentYear__c==0){
                    am.PriceCurrentYear__c=1;
                }
                if(am.ByPassContract__c==false){
                    am.PriceNextYear__c=am.PriceCurrentYear__c+am.PriceCurrentYear__c*6*0.01;
                    AmendmentsToUpdate.add(am);
                }
            }
            c.Status__c='Brouillon';
        }
        if(AmendmentsToUpdate.size()>0){
            Update AmendmentsToUpdate;
        }
        if(Listcontracts.size()>0){
            Update ContractsToUpdate;
        }
    }
    global void finish(Database.BatchableContext BC)
    {  
    }
}