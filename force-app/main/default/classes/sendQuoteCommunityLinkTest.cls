@isTest
public class sendQuoteCommunityLinkTest {

     @isTest
    public static void testSendQuoteWithFiles(){
        Profile p = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];        
        String testemail2 = 'assistanttest@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        director.UserRoleId=[select Id from UserRole where Name='Direction Commerciale - Nicolas Rogeau'].Id;
        insert director;
        System.RunAs(director) {
            Account a;
            Contact c;
            Opportunity opp;
            Quote q;
            a = new Account();
            a.Name = 'testAccName';
            a.CustomerNature__c = 'Administration';
            a.Industry = 'Trader';
            a.CurrencyIsoCode = 'EUR';
            a.Producteur__c = false;
            a.BillingPostalCode = '00000';
            a.NAF_Number__c = '1234A';
            a.BillingStreet = '12 rue Pré Gaudry';
            insert a;
            
            c = new Contact();
            c.LastName = 'testName';
            c.AccountId = a.Id;
            c.Email = 'sechetestsfdc@gmail.com';
            c.Phone = '000000000';
            insert c;
            
            opp = new Opportunity();
            opp.Name = 'Mon OPP TEST';
            opp.StageName = 'Contractualisation';
            opp.CloseDate = System.today().addDays(10);
            opp.AccountId = a.Id;
            opp.TonnageN__c = 0;
            opp.Filiale__c = 'Tredi Hombourg';
            opp.Filiere__c = 'Physico';
            opp.ContactName__c = c.Id;
            opp.Salesman__c = director.Id;
            insert opp;
            
            q = new Quote();
            q.OpportunityId = opp.ID;
            q.Name = opp.Name;
            q.DateDevis__c = Date.today();
            q.QuoteCode__c=opp.Name+'test ';
            q.ContactId = opp.ContactName__c;
            q.Email = 'sechetestsfdc@gmail.com';
            q.Phone = opp.ContactName__r.Phone;
            q.Fax = opp.ContactName__r.Fax;
            q.BillingStreet = opp.Account.BillingStreet;
            q.BillingCity = opp.Account.BillingCity;
            q.BillingState = opp.Account.BillingState;
            q.BillingPostalCode = opp.Account.BillingPostalCode;
            q.BillingCountry = opp.Account.BillingCountry; 
            insert q;          

            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
                //Insert ContentVersion
            ContentVersion cVersion = new ContentVersion();
            cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
            cVersion.PathOnClient = q.Name;//File name with extention
            cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.      
            cVersion.Title = q.Name;//Name of the file
            cVersion.VersionData = bodyBlob;//File content
            Insert cVersion;
            
            //After saved the Content Verison, get the ContentDocumentId
            Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
            
            //Insert ContentDocumentLink
            ContentDocumentLink cDocLink = new ContentDocumentLink();
            cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
            cDocLink.LinkedEntityId = q.Id;//Add attachment parentId
            cDocLink.ShareType = 'I';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
            cDocLink.Visibility = 'InternalUsers';//AllUsers, InternalUsers, SharedUsers
            Insert cDocLink;
            
            List<Attachment> atts=[SELECT id, Name, body, ContentType,ParentId FROM Attachment WHERE ParentId=:q.Id];
            system.debug('attachements'+atts.size());
            test.startTest();
            sendQuoteCommunityLink.sendQuoteForSignature(q.Id);
        	test.stopTest();
            q.Name = 'Convention 2020'+opp.Name;
            Update q;
            sendQuoteCommunityLink.sendQuoteForSignature(q.Id);
            Quote quoteUpdate=[Select Id,Tech_UID__c,TECH_isApprouved__c,RecordTypeId FROM Quote Where ID=:q.ID];
            RecordType rt=[SELECT Id, Name, DeveloperName, Description FROM RecordType Where DeveloperName='Devis_approuv_Read_Only' LIMIT 1];
            System.assert(quoteUpdate.TECH_UID__c!=Null);
        }
    }

    @isTest
    public static void testSendQuoteWithAttachment(){
        Profile p = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];        
        String testemail2 = 'assistanttest@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        director.UserRoleId=[select Id from UserRole where Name='Direction Commerciale - Nicolas Rogeau'].Id;
        insert director;
        System.RunAs(director) {
            Account a;
            Contact c;
            Opportunity opp;
            Quote q;
            a = new Account();
            a.Name = 'testAccName';
            a.CustomerNature__c = 'Administration';
            a.Industry = 'Trader';
            a.CurrencyIsoCode = 'EUR';
            a.Producteur__c = false;
            a.BillingPostalCode = '00000';
            a.NAF_Number__c = '1234A';
            a.BillingStreet = '12 rue Pré Gaudry';
            insert a;
            
            c = new Contact();
            c.LastName = 'testName';
            c.AccountId = a.Id;
            c.Email = 'sechetestsfdc@gmail.com';
            c.Phone = '000000000';
            insert c;
            
            opp = new Opportunity();
            opp.Name = 'Mon OPP TEST';
            opp.StageName = 'Contractualisation';
            opp.CloseDate = System.today().addDays(10);
            opp.AccountId = a.Id;
            opp.TonnageN__c = 0;
            opp.Filiale__c = 'Tredi Hombourg';
            opp.Filiere__c = 'Physico';
            opp.ContactName__c = c.Id;
            opp.Salesman__c = director.Id;
            insert opp;
            
            q = new Quote();
            q.OpportunityId = opp.ID;
            q.Name = opp.Name;
            q.DateDevis__c = Date.today();
            q.QuoteCode__c=opp.Name+'test ';
            q.ContactId = opp.ContactName__c;
            q.Email = 'sechetestsfdc@gmail.com';
            q.Phone = opp.ContactName__r.Phone;
            q.Fax = opp.ContactName__r.Fax;
            q.BillingStreet = opp.Account.BillingStreet;
            q.BillingCity = opp.Account.BillingCity;
            q.BillingState = opp.Account.BillingState;
            q.BillingPostalCode = opp.Account.BillingPostalCode;
            q.BillingCountry = opp.Account.BillingCountry; 
            insert q;
            
            Attachment att = new Attachment();
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            att.body=bodyBlob;
            att.Name = 'Convention 2020'+q.QuoteCode__c+'.pdf';	
            att.ParentId = q.Id;             
            insert att; 
                //Insert ContentVersion
            ContentVersion cVersion = new ContentVersion();
            cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
            cVersion.PathOnClient = q.Name;//File name with extention
            cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.      
            cVersion.Title = q.Name;//Name of the file
            cVersion.VersionData = bodyBlob;//File content
            Insert cVersion;
            
            //After saved the Content Verison, get the ContentDocumentId
            Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
            
            //Insert ContentDocumentLink
            ContentDocumentLink cDocLink = new ContentDocumentLink();
            cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
            cDocLink.LinkedEntityId = q.Id;//Add attachment parentId
            cDocLink.ShareType = 'I';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
            cDocLink.Visibility = 'InternalUsers';//AllUsers, InternalUsers, SharedUsers
            Insert cDocLink;

            List<Attachment> atts=[SELECT id, Name, body, ContentType,ParentId FROM Attachment WHERE ParentId=:q.Id];
            system.debug('attachements'+atts.size());
            test.startTest();
            sendQuoteCommunityLink.sendQuoteForSignature(q.Id);
        	test.stopTest();
            q.Name = 'Convention 2020'+opp.Name;
            Update q;
            sendQuoteCommunityLink.sendQuoteForSignature(q.Id);
            Quote quoteUpdate=[Select Id,Tech_UID__c,TECH_isApprouved__c,RecordTypeId FROM Quote Where ID=:q.ID];
            RecordType rt=[SELECT Id, Name, DeveloperName, Description FROM RecordType Where DeveloperName='Devis_approuv_Read_Only' LIMIT 1];
            System.assert(quoteUpdate.TECH_UID__c!=Null);
        }
    }
    @isTest
    public static void testSendQuoteConvention(){
        Profile p = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];        
        String testemail2 = 'assistanttest@test.com';
        
        User director = new User(profileId = p.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        director.UserRoleId=[select Id from UserRole where Name='Direction Commerciale - Nicolas Rogeau'].Id;
        insert director;
        System.RunAs(director) {
            Account a;
            Contact c;
            Opportunity opp;
            Quote q;
            a = new Account();
            a.Name = 'testAccName';
            a.CustomerNature__c = 'Administration';
            a.Industry = 'Trader';
            a.CurrencyIsoCode = 'EUR';
            a.Producteur__c = false;
            a.BillingPostalCode = '00000';
            a.NAF_Number__c = '1234A';
            a.BillingStreet = '12 rue Pré Gaudry';
            insert a;
            
            c = new Contact();
            c.LastName = 'testName';
            c.AccountId = a.Id;
            c.Email = 'sechetestsfdc@gmail.com';
            c.Phone = '000000000';
            insert c;
            
            opp = new Opportunity();
            opp.Name = 'Mon OPP Convention';
            opp.StageName = 'Contractualisation';
            opp.CloseDate = System.today().addDays(10);
            opp.AccountId = a.Id;
            opp.TonnageN__c = 0;
            opp.Filiale__c = 'Tredi Hombourg';
            opp.Filiere__c = 'Physico';
            opp.ContactName__c = c.Id;
            opp.Salesman__c = director.Id;
            insert opp;
            
            q = new Quote();
            q.OpportunityId = opp.ID;
            q.Name = opp.Name;
            q.DateDevis__c = Date.today();
            q.QuoteCode__c=opp.Name+'test ';
            q.ContactId = opp.ContactName__c;
            q.Email = 'sechetestsfdc@gmail.com';
            q.Phone = opp.ContactName__r.Phone;
            q.Fax = opp.ContactName__r.Fax;
            q.BillingStreet = opp.Account.BillingStreet;
            q.BillingCity = opp.Account.BillingCity;
            q.BillingState = opp.Account.BillingState;
            q.BillingPostalCode = opp.Account.BillingPostalCode;
            q.BillingCountry = opp.Account.BillingCountry; 
            insert q;
            
            Attachment att = new Attachment();
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            att.body=bodyBlob;
            att.Name = 'Convention 2020'+q.QuoteCode__c+'.pdf';	
            att.ParentId = q.Id;             
            insert att; 
            List<Attachment> atts=[SELECT id, Name, body, ContentType,ParentId FROM Attachment WHERE ParentId=:q.Id];
            system.debug('attachements'+atts.size());
            test.startTest();
            sendQuoteCommunityLink.sendQuoteForSignature(q.Id);
        	test.stopTest();
            q.Name = 'Convention 2020'+opp.Name;
            Update q;
            sendQuoteCommunityLink.requestSendQuoteForSignature(q);
            Quote quoteUpdate=[Select Id,Tech_UID__c,TECH_isApprouved__c,RecordTypeId FROM Quote Where ID=:q.ID];
            RecordType rt=[SELECT Id, Name, DeveloperName, Description FROM RecordType Where DeveloperName='Devis_approuv_Read_Only' LIMIT 1];
            System.assert(quoteUpdate.TECH_UID__c!=Null);
        }
    }
}