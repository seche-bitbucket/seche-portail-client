public class ProductSearchExt {

public List<WrapperProduct> productList {get; set;}
public List<WrapperProduct> productList2 {get; set;}
private List<pricebookentry> selectedProducts=new List<pricebookentry>();
private String query {get; set;}
List<pricebookentry> results {get; set;}
public string productFamily {get; set;}
//private List<pricebookentry> selectedProducts{get;set;}
public Integer size {get; set;}
public Integer noOfRecords {get; set;}
public List<SelectOption> paginationSizeOptions {get; set;}
//public String family { get; set; }
public String keyword { get; set; }
public Boolean selected {get; set;}
public Integer lignes {get; set;}
PriceBookEntry priceBE;
public Set<Id> selectedRecordIds;
// String OppName {get;set;}
Map <id,PriceBookEntry> SelectedPbeMap = new Map <id,PriceBookEntry>();
private List <OpportunityLineItem> listOppLineItems = new list<OpportunityLineItem>();
public List <OpportunityLineItem> listOppLineItemsToInsert = new list<OpportunityLineItem>();
private id oppId;
private String OppName;
private id QuoteId;
private String QuoteName;
private List<OpportunityLineItem> OpplineItems {get; set;}
private Id priceBookId;
private Opportunity opp;
private ID quoteParentOppId;
private List <Opportunity> listOpp;
private List <Quote> listQuote;
public Boolean isPriceBookSet {get; set;}
public Boolean isRequestFromQuote  {get; set;}
public Boolean noAccess  {get; set;}

public ProductSearchExt(){
	oppId = ApexPages.currentPage().getParameters().Get('OppId');
	OppName = ApexPages.currentPage().getParameters().Get('OppName');
	QuoteId = ApexPages.currentPage().getParameters().Get('QuoteId');
	isPriceBookSet = false;
/*	if(!String.isBlank(QuoteName)){
		QuoteName = ApexPages.currentPage().getParameters().Get('QuoteName').replace('+','');
	}	*/

	size=200;	
	paginationSizeOptions = new List<SelectOption>();
	paginationSizeOptions.add(new SelectOption('5','5'));
	paginationSizeOptions.add(new SelectOption('10','10'));
	paginationSizeOptions.add(new SelectOption('20','20'));
	paginationSizeOptions.add(new SelectOption('50','50'));
	paginationSizeOptions.add(new SelectOption('100','100'));
	paginationSizeOptions.add(new SelectOption('200','200'));
	isRequestFromQuote = false;
	listOpp = new List<Opportunity>();
	listQuote = new List<Quote>();
	// priceBE = (PriceBookEntry) Controller.getRecord();
	if(oppId != null) {
		listOpp = [select Name, pricebook2Id from opportunity where id =: OppId];
	}else if(QuoteId != null) {
		isRequestFromQuote = true;
		listQuote= [SELECT Id, Name, OpportunityId, IsSyncing FROM QUOTE WHERE Id = :QuoteId ];
		oppId = listQuote[0].OpportunityId;
		QuoteName = listQuote[0].Name;
		if(!listQuote[0].IsSyncing) {
			noAccess = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, ' Vous ne pouvez pas ajouter d\'élément sur un devis non synchronisé'));
		}
		ApexPages.currentPage().getParameters().put('QuoteName', QuoteName);
		if(oppId != null) {
			listOpp = [select Name, pricebook2Id from opportunity where id =: oppId];
			if(listOpp[0].pricebook2Id != null) {
				isPriceBookSet = true;
			}
		}
	}

	if(listOpp.size() > 0) {
		opp = listOpp[0];
		OppName = opp.Name;
		priceBookId = opp.pricebook2Id;
	}
}

public pageReference selectPriceBook(){
	System.debug('selectPriceBook ---Start: ');

	// Case when adding product to the quote
	if(!isPriceBookSet && QuoteId != null ) {
		pageReference pr = Page.OPP_ChoosePriceBook;
		pr.getParameters().put('QuoteId', QuoteId);
		pr.getParameters().put('QuoteName', QuoteName);
		pr.setRedirect(true);
		return pr;
	}
// Case when adding product to the opportunity
	if(opp != null) {		
		ApexPages.currentPage().getParameters().put('OppName', OppName);
		If (opp.pricebook2Id == null){
			pageReference pr = Page.OPP_ChoosePriceBook;
			pr.getParameters().put('OppId', oppId);
			pr.getParameters().put('OppName', OppName);
			pr.setRedirect(true);
			return pr;
		}

	}
	return null;

}
public ApexPages.StandardSetController setCon {
	get {
		if(setCon == null) {

			setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
									     [select Product2.Name,Product2.Description,Product2.ProductCode,UnitPrice,Product2.Family,Name,LogoNature__c,Product2.EuropeanCode__c from pricebookentry where Product2.Name != null and pricebook2Id =: priceBookId and IsActive = true order by Product2.Name]));
			setCon.setPageSize(size);
			noOfRecords = setCon.getResultSize();
		}
		return setCon;
	}
	set;
}

public List<Pricebookentry> getResults() {
	return (List<Pricebookentry>) setCon.getRecords();
}

public List<WrapperProduct> getProduct() {

	productList2 = productList;

	ProductList = new List<WrapperProduct>();

	for(PriceBookEntry pr: getresults()) {
		ProductList.add(new WrapperProduct(pr));
	}
	return ProductList;
}

//Changes the size of pagination
public PageReference refreshPageSize() {
	setCon.setPageSize(size);
	return null;
}

/* public String sortField {
     get  { if (sortField == null) {sortField = 'Product2.Name'; } return sortField;  }
     set;
   }*/

public pageReference search() {
	string family = productFamily;
	system.debug(productFamily);
	system.debug(keyword);
	if (keyword!=null)
	{
		string keyword2 = '%'+keyword.Replace(' ','%')+'%';
	}
	Boolean isWhere = false;
	query = 'select Product2.Name,Product2.Description,Product2.ProductCode,UnitPrice,Product2.Family,Name,LogoNature__c,Product2.EuropeanCode__c  from pricebookentry Where IsActive = true and Pricebook2Id =: priceBookId';

	if (!String.IsBlank(keyword))
	{
		query+= ' And Product2.name like: keyword2';
	}
	if(!String.IsBlank(family))
	{
		query+= ' And Product2.Family=:family';
	}
	query+= ' Order by Product2.Name';
	this.InitCon(Database.Query (query));
	return null;
}
public PageReference processSelected()
{
	if (ProductList!=null) {
		getProduct();
		for(WrapperProduct pPbe: productList2) {
			if(pPbe.selected == true) {
				selectedProducts.add(pPbe.pbe);
				Integer i=pPbe.lignes;
				while(i>1) {
					selectedProducts.add(pPbe.pbe);
					i=i-1;
				}
			}
		}
		for(pricebookentry pbe: selectedProducts) {
			system.debug('pbe ='+pbe);
		}
	}
	if (selectedProducts.size()>0)
	{
		PageReference pageRef = new PageReference ('/Apex/OLI_EditAllOppLineItems?OppId='+OppId+'&OppName='+OppName);
		pageRef.setRedirect(false);
		return pageRef;
	}
	else
	{
		ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, ' Veuillez sélectionner au moins une ligne de produit!'));
		return null;

	}


}
public PageReference saveProcess(){

	For(OpportunityLineItem opLI: getOppLineItems() )
	{
		OpportunityLineItem opLINew = new OpportunityLineItem (PriceBookEntryId=opLI.PriceBookEntryId,OpportunityId = OppId,N_CAP__c = opLI.N_CAP__c,Quantity = opLI.Quantity,
		                                                       Description = opLI.Description,Sector__c = opLI.Sector__c,Unit__c = opLI.Unit__c,IncludedTGAP__c = opLI.IncludedTGAP__c,
															   Packaging__c= opLI.Packaging__c,TransportType__c = opLI.TransportType__c,
															   UnitPrice = opLI.UnitPrice, Outlet__c=opLI.Outlet__c, Incoterms__c=opLI.Incoterms__c, IncotermsDesc__c=opLI.IncotermsDesc__c,
															   CollectRate__c=opLI.CollectRate__c, VATRate__c=opLI.VATRate__c, GoLitrePrice__c=opLI.GoLitrePrice__c, FuelPart__c=opLI.FuelPart__c);
		listOppLineItemsToInsert.add(opLINew);
	}
	insert listOppLineItemsToInsert;
	PageReference pageRef3 ;
	if(!isRequestFromQuote) {
		pageRef3 = new PageReference('/'+OppId);
	} else{
		pageRef3 = new PageReference('/'+QuoteId);
	}

	pageRef3.setRedirect(false);
	return pageRef3;
}
Public List<OpportunityLineItem> getOppLineItems()
{
	if (listOppLineItems.size()==0)
	{
		for (pricebookentry pbeSelc : selectedProducts)
		{
			OpportunityLineItem oppLineItem =  New OpportunityLineItem(PriceBookEntryId=pbeSelc.Id,OpportunityId = OppId,Tech_Name2__c = pbeSelc.Name,UnitPrice = pbeSelc.UnitPrice);
			listOppLineItems.add(oppLineItem);
		}
	}
	return listOppLineItems;
}
public PageReference cancelProcess()
{
	if(!isRequestFromQuote) {
		return new PageReference('/'+OppId);
	} else{
		return new PageReference('/'+QuoteId);
	}



}
/* public List<pricebookentry> runQuery() {

     try {
         results = Database.query(query+ ' order by ' + sortField);
         return results;

     } catch (Exception e) {
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops!'));
         return null;

     }

   }*/
public void initCon(List<pricebookentry> productsToInit){

	setCon = new ApexPages.StandardSetController(productsToInit);

	setCon.setPageSize(size);

	noOfRecords = setCon.getResultSize();

}
public class WrapperProduct {
public PriceBookEntry pbe {get; set;}
public Boolean selected {get; set;}
public Integer lignes {get; set;}
//This is the contructor method. When we create a new cContact object we pass a Contact that is set to the con property. We also set the selected value to false
public WrapperProduct(PriceBookEntry p) {
	pbe = p;
	selected = false;
	lignes = 1;
}
}




}