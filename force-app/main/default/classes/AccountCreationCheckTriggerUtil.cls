public class AccountCreationCheckTriggerUtil {
    
    public static void CreateAccount(List<Account> pLaccount){
        User usr = [Select ID, profileID, SocieteF_t__c From User Where Id = : UserInfo.getUserId()];
        
        if(usr.ProfileId != Label.PRF_Administrator) {
            for(Account acc : pLaccount){                
                System.debug('acc.TECH_UID__c : '+acc.TECH_UID__c);
                //check Societe Francaise
                if( acc.ELLISPHERE__ELLI_IdRegisterestb2__c == null && usr.SocieteF_t__c == true  && acc.TECH_UID__c == null)  
                {
                    System.debug('CreateAccount = ' + 'Votre profil utilisateur n\'est pas autorisé à créer un compte');
                    acc.addError('Erreur : Votre profil utilisateur n\'est pas autorisé à créer un compte autrement que par Sociétés Française.');
                }   
                
                // else Societe Francaise nothing to do
            }
        }
    }
    /*
    public static Map<Id,Lead> generateLeadsMap(List<Lead> pLeads){
        Map<Id,Lead> leadMap = new Map<Id,Lead>();
        
        for(Lead l : pLeads){
            LeadMap.put(l.ConvertedAccountId ,l);
        }
        
        return leadMap;
    }
    */
}