global without sharing class FlowAction_Delete {
    
    global class Parameter {
        @InvocableVariable(Required=true)
        global String[] listValues;

        @InvocableVariable(Required=true)
        global String objectApiName;
    }


    @InvocableMethod(label = 'Delete records from flow' description = 'Apex method to delete records from SF using list of objects')
    public static void deleteRecords(Parameter[] parameters) {
        String[] recordsIds = parameters[0].listValues;
        String objectApiName = parameters[0].objectApiName;

        String query = 'SELECT Id FROM ' + objectApiName + ' WHERE Id IN :recordsIds';

        List<SObject> recordsToDelete = new List<SObject>{Database.query(query)};
        
        delete recordsToDelete;
    }
}