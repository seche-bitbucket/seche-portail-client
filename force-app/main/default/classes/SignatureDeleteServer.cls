// this class is to be executed once a week to empty the server of connective 
global class SignatureDeleteServer Implements Schedulable{

    global void execute(SchedulableContext sc)
    {
        deleteSignedFlows();
        
    }  
    global static void deleteSignedFlows(){
        List<DocumentsForSignature__c> docs=[SELECT Id, Name, Status__c, SentTo__c, TECH_ExternalId__c, FlowId__c, Document__c, CreatedById FROM DocumentsForSignature__c];
        List<DocumentsForSignature__c> docstodelete= new List<DocumentsForSignature__c>();
        for(DocumentsForSignature__c docum: docs){
                DocumentsForSignature__c docTodelete;            
                if(docum.Status__c=='SIGNED'){
                    ConnectiveCallout.deleteFlow(docum.FlowId__c,'DELETE',FALSE);
                    docTodelete =[SELECT Id FROM DocumentsForSignature__c WHERE Id=:docum.Id Limit 1];
                }else if(docum.Status__c=='REVOKED'){
                    docTodelete =[SELECT Id FROM DocumentsForSignature__c WHERE Id=:docum.Id Limit 1];
                    ConnectiveCallout.deleteFlow(docum.FlowId__c,'DELETE',FALSE);
                }else if(docum.Status__c=='REJECTED'){  
                    docTodelete =[SELECT Id FROM DocumentsForSignature__c WHERE Id=:docum.Id Limit 1];
                    ConnectiveCallout.deleteFlow(docum.FlowId__c,'DELETE',FALSE);
                }
                docstodelete.add(docTodelete);
            }
        delete docstodelete;
    }
}