/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : Batch_ContactsInvitationCheckDupSchedule
* -- - Author : AID
* -- - Company : Capgemini
* -- - Purpose : Batch_ContactsInvitationCheckDupSchedule
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 27-05-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
global class Batch_ContactsInvitationCheckDupSchedule implements Schedulable{
    global void execute(SchedulableContext sc) {
        Batch_ContactsInvitationCheckDuplicates b1 = new Batch_ContactsInvitationCheckDuplicates();
        ID batchprocessid = Database.executeBatch(b1);           
    }
}