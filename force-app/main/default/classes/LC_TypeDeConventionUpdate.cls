public with sharing class LC_TypeDeConventionUpdate {
@AuraEnabled
public static void updateContractsA(String lstContractsId, String typeDeConvention){
	 AP_AugmentationTarifManager.updateContractsA(lstContractsId, typeDeConvention);
}

@AuraEnabled
public static void updateContractsConv(List<Contract__c> lstCont, String typeDeConvention){
	 AP_AugmentationTarifManager.updateContractsConv(lstCont, typeDeConvention);
}

@AuraEnabled
public static Map<String,List<String>> getTypeValues(String ctrField, String depField) {
	return AP_AugmentationTarifManager.getTypeValues(ctrField, depField);    
}
}