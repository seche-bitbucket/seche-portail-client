public class OppLineItemSyncTriggerV2Handler {
    
    private static Boolean stop = false;
    
    public static void stop() {
        stop = true;
    }
    
    Public static void execute(List<OpportunityLineItem> newLst, Map<Id, OpportunityLineItem> oldMap) {
        if(!stop) {
            SyncHandler.SyncHandler(newLst, oldMap, 'OpportunityLineItem');
        }
    }

}