/*
* ------------------------------------------------------------------------------------------------------------------
* -- - Name : Batch_Connective_Get_Document
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : Batch_Connective_Get_Document
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- -------------------------------------------------------------------------------------
* -- 04-05-2020 PMB 1.0 - Initial version
* --------------------------------------------------------------------------------------------------------------------
*/
public class Batch_Connective_Get_Document implements Queueable, Database.AllowsCallouts {

    public Map<Id,DocumentsForSignature__c> docToUpload = new Map<Id,DocumentsForSignature__c>();
    private Set<SObject> objectToUpdate;
    public Id tempCurrentID;

    public Batch_Connective_Get_Document(List<DocumentsForSignature__c> docToUpload, List<SObject> postUpdate){
        Map<Id,DocumentsForSignature__c> docs = new Map<Id,DocumentsForSignature__c>(docToUpload);
        this.objectToUpdate = (postUpdate != null) ? new Set<SObject>(postUpdate) : null;
        this.docToUpload = new Map<Id,DocumentsForSignature__c>([
            SELECT Id,DownloadSignedURL__c,SigningUrl__c,Quote__c,Attestation__c,Quote__r.OwnerId, Name, CAP__c, Lead__c,
                    FIP_FIP__c,FIP_FIP__r.OwnerId,Package__r.ConnectivePackageId__c,ConnectiveDocumentId__c 
            FROM DocumentsForSignature__c 
            WHERE Id IN :docs.keySet()]);
    }

    public void execute(QueueableContext context) {
        try{
            for(Id docId : docToUpload.keySet()){
                tempCurrentID = docId;
                manageSaveDocument(docToUpload.get(docId));
                docToUpload.remove(docId);
                if(Test.isRunningTest()){
                    Integer testerror = 10/0;
                }
                break;
            }
        }
        catch(Exception ex){
            docToUpload.remove(tempCurrentID);
            Log__c error = new Log__c(
                ApexJob__c= Batch_Connective_Get_Document.class.getName(),
                ErrorMessage__c=ex.getMessage(),
                ExceptionType__c='APEX'
            );
            insert error;
        }
        finally{
            if(!Test.isRunningTest() && docToUpload.size()>0){
                System.enqueueJob(new Batch_Connective_Get_Document(docToUpload.values(), (objectToUpdate != null) ? new List<SObject>(objectToUpdate) : null));
            }
            else if(!Test.isRunningTest() && docToUpload.size()==0){
                System.debug(objectToUpdate);
                if(objectToUpdate != null && objectToUpdate.size() > 0) {
                    update new List<SObject>(objectToUpdate);
                }
            }
        }
    }

    public void manageSaveDocument(DocumentsForSignature__c doc){
        Id objId;
        if(doc.Quote__c !=null){
            objId = doc.Quote__c;
        }
        if(doc.FIP_FIP__c !=null){
            objId = doc.FIP_FIP__c;
        }
        if(doc.Attestation__c != null) {
            objId = doc.Attestation__c;
        }
        if(doc.Lead__c != null) {
            objId = doc.Lead__c;
        }
        if(doc.CAP__c != null) {
            objId = doc.CAP__c;
        }
        ConnectiveV4ApiManager.getDocumentByName(objId, doc.Package__r.ConnectivePackageId__c, doc.ConnectiveDocumentId__c, doc.Name);
    }
    
}