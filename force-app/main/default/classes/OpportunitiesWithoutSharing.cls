public Without Sharing class OpportunitiesWithoutSharing {
    private final Account acct;
    AggregateResult[] opp;
    
    public OpportunitiesWithoutSharing(ApexPages.StandardController stdController) {
        this.acct = (Account)stdController.getRecord();
    }  
    public AggregateResult[] GetOpp() {
        opp = [select Owner.Name OwnerName, Owner.CompanyName CompanyName, Owner.Phone phone, Owner.MobilePhone mobilePhone, OwnerID ownerID  ,count(Id) cnt from Opportunity where Accountid = :acct.Id Group By Owner.Name, Owner.CompanyName, Owner.Phone, Owner.MobilePhone, OwnerID];
        return opp;
    }

}