/*
Class for all system methods to get data
*/
global without sharing class SystemUtils {

    global class OpportunityId {
        @InvocableVariable
        global ID opportunityId;
        @InvocableVariable
        global Attestation__c[] prefips;
    }

    global class FormulaireId {
        @InvocableVariable
        global ID formulaireId;
        @InvocableVariable
        global Attestation__c[] prefips;
    }
    
    // @InvocableMethod(label='Set GUID' description='Set a GUID passcode for Opprtunity' category='Pré-Fip')
    // public static void genGuid(List<OpportunityId> request) {
    //     String preFipUrl = [SELECT SITE_Prefip__c FROM URL__c].SITE_Prefip__c;
    //     Opportunity opp = [SELECT ContactName__r.Email, Salesman__r.Name FROM Opportunity WHERE Id = :request[0].opportunityId];
    //     Blob b = Crypto.GenerateAESKey(128);
    //     String h = EncodingUtil.ConvertTohex(b);
    //     String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
    //     upsert new Opportunity(Id=request[0].opportunityId,
    //                                       GUID__c=guid);
    //     try{
    //         String owa = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = 'Séché Connect'].Id;
    //         String htmlBody = '<p>Cher client,</p><br><p>Pour compléter vos attestations producteurs en vue d\'une élimination en Installation de Stockage de Déchets Non Dangereux,</p><p>Veuillez cliquer sur le lien suivant : <a href="'+preFipUrl+'/s/?recordId='+guid+'">'+preFipUrl+'/s/?recordId='+guid+'</a></p><br><p>Après enregistrement de vos réponses, vous recevrez un mail de <strong>Connective</strong> vous invitant à signer électroniquement ces attestations.</p><br><p>'+opp.Salesman__r.Name+'</p>';
    //         List<Attestation__c> prefipsList = request[0].prefips;
    //         String email = [SELECT Email FROM Contact WHERE Id = :prefipsList[0].Contact__c].Email;
    //         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    //         mail.setOrgWideEmailAddressId(owa);
    //         mail.setTargetObjectId(prefipsList[0].Contact__c);
    //         mail.setWhatId(prefipsList[0].Id);
    //         mail.setToAddresses(new String[] {email});
    //         mail.setSubject('Attestations producteurs pour éliminitation ISDND');
    //         mail.setHtmlBody(htmlBody);
    //         if(prefipsList.size() > 1) {
    //             List<Task> tasksList = new List<Task>();
    //             for(Integer i = 1; i < prefipsList.size(); i++) {
    //                 Task t = LC_GenericEmailSend.createEmailActivity('Attestations producteurs pour éliminitation ISDND', htmlBody, null, prefipsList[i].Id);
    //                 tasksList.add(t);
    //             }
    //             insert tasksList;
    //         }
    //         Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    //     }catch(Exception ex){
    //         System.debug(ex.getMessage()+' '+ex.getLineNumber());
    //         return;
    //     }   
    //     return;
    // }

    @InvocableMethod(label='Set GUID' description='Set a GUID passcode for Formulaire' category='Pré-Fip')
    public static void genGuid(List<FormulaireId> request) {
        String preFipUrl = [SELECT SITE_Prefip__c FROM URL__c].SITE_Prefip__c;
        Form__c form = [SELECT Contact__r.Email, Opportunity__r.Salesman__r.Name, BusinessCase__c FROM Form__c WHERE ID = :request[0].formulaireId];
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        String businessCase = '';
        upsert new Form__c(Id=request[0].formulaireId, GUID__c=guid);
        try{
            if(form.BusinessCase__c != null) {
                businessCase = 'Affaire / dossier : ' + form.BusinessCase__c + '</br>';
            }
            String guideLabel = System.Label.AttProd_Guide;
            String owa = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = 'Séché Connect'].Id;
            String htmlBody = '<p>Cher client,</p><p>Pour compléter vos attestations producteurs en vue d\'une élimination en Installation de Stockage de Déchets Non Dangereux,</p>' + businessCase + '<p>Veuillez cliquer sur le lien suivant : <a href="'+preFipUrl+'/s/?recordId='+guid+'">'+preFipUrl+'/s/?recordId='+guid+'</a></p><p>Après enregistrement de vos réponses, vous recevrez un mail de <strong>Connective</strong> vous invitant à signer électroniquement ces attestations.</p><p>Pour vous aider dans cette démarche, consultez notre <a href="https://youtu.be/kEIXc_0_8bo">démo</a> et notre <a href="' + guideLabel + '">guide</a>.</p>';
            List<Attestation__c> prefipsList = request[0].prefips;
            String email = [SELECT Email FROM Contact WHERE Id = :form.Contact__r.Id].Email;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setOrgWideEmailAddressId(owa);
            mail.setTargetObjectId(form.Contact__c);
            mail.setWhatId(prefipsList[0].Id);
            mail.setToAddresses(new String[] {email});
            mail.setSubject('Attestations producteurs pour élimination ISDND');
            mail.setHtmlBody(htmlBody);
            if(prefipsList.size() > 1) {
                List<Task> tasksList = new List<Task>();
                for(Integer i = 1; i < prefipsList.size(); i++) {
                    Task t = LC_GenericEmailSend.createEmailActivity('Attestations producteurs pour élimination ISDND', htmlBody, null, prefipsList[i].Id);
                    tasksList.add(t);
                }
                insert tasksList;
            }
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }catch(Exception ex){
            System.debug(ex.getMessage()+' '+ex.getLineNumber());
            return;
        }   
        return;
    }



    @AuraEnabled
    public static List<Map<String, Object>> returnSchema(){
        try {
            List<Map<String, Object>> lstSettings = new List<Map<String, Object>>();
            for ( Schema.SObjectType o : Schema.getGlobalDescribe().values() )
            {
                Schema.DescribeSObjectResult objResult = o.getDescribe();
                if(!objResult.getName().containsignorecase('history') && !objResult.getName().containsignorecase('tag') &&
                    !objResult.getName().containsignorecase('share') && !objResult.getName().containsignorecase('feed') && 
                    !objResult.getName().containsignorecase('group') && !objResult.getName().containsignorecase('public') &&
                    !objResult.isCustomSetting() && objResult.isCreateable()) {
                        lstSettings.add(new Map<String, Object>{
                            'id' => objResult.getName(),
                            'label' => objResult.getLabel(),
                            'value' => objResult.getName(),
                            'type'=> objResult.getName(),
                            'icon' => (objResult.isCustom()) ? 'utility:sobject' : 'utility:standard_objects',
                            'selected' => false
                        });   
                }
            }
            System.debug(lstSettings);
            return lstSettings;
        } catch (Exception e) {
            System.debug(e.getMessage()+' '+e.getLineNumber());
            return null;
        }
    }

    private static Map<String, List<Map<String, String>>> getDependentPicklistValues(Schema.sObjectField dependToken) {

        Schema.DescribeFieldResult depend = dependToken.getDescribe();
        Schema.sObjectField controlToken = depend.getController();
        if (controlToken == null) {
            return new Map<String, List<Map<String, String>>>();
        }
     
        Schema.DescribeFieldResult control = controlToken.getDescribe();
        List<Schema.PicklistEntry> controlEntries;
        if(control.getType() != Schema.DisplayType.Boolean) {
            controlEntries = control.getPicklistValues();
        }
     
        String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/-,_éèçà&';
        Map<String, List<Map<String, String>>> dependentPicklistValues = new Map<String, List<Map<String, String>>>();
        for (Schema.PicklistEntry entry : depend.getPicklistValues()) {
            if (entry.isActive() && String.isNotEmpty(String.valueOf(((Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')))) {
                List<String> base64chars =
                        String.valueOf(((Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')).split('');
                for (Integer index = 0; index < (controlEntries != null ? controlEntries.size() : 2); index++) {
                    Object controlValue =
                            (controlEntries == null
                                    ?   (Object) (index == 1)
                                    :   (Object) (controlEntries[index].isActive() ? controlEntries[index].getLabel() : null)
                            );
                    Integer bitIndex = index / 6;
                    if (bitIndex > base64chars.size() - 1) {
                        break;
                    }
                    Integer bitShift = 5 - Math.mod(index, 6);
                    if  (controlValue == null || (base64map.indexOf( base64chars[ bitIndex ] ) & (1 << bitShift)) == 0)
                        continue;
                    if (!dependentPicklistValues.containsKey((String) controlValue)) {
                        dependentPicklistValues.put((String) controlValue, new List<Map<String, String>>());
                    }
                    dependentPicklistValues.get((String) controlValue).add(new Map<String, String>{'label' => entry.getLabel(), 'value' => entry.getValue()});
                }
            }
        }
        return dependentPicklistValues;
    }

    @AuraEnabled
    public static List<Map<String,Object>> getSObjectFields(String sobjectName, Boolean withRef){
        try {
            List<Map<String, Object>> lstFields = new List<Map<String, Object>>();
            for (Schema.SObjectField field : System.Schema.getGlobalDescribe().get(sobjectName).getDescribe().fields.getMap().values()) {
                String fieldType = String.valueOf(field.getDescribe().getType());
                List<Map<String, String>> values = new List<Map<String, String>>();
                if(fieldType.containsignorecase('picklist')) {
                    for(Schema.PicklistEntry picklistChoice: field.getDescribe().getPicklistValues()) {
                        values.add(new Map<String, String>{
                            'value' => picklistChoice.getValue(),
                            'label' => picklistChoice.getLabel()
                        });
                    }
                }
                Boolean dependant = false;
                Map<String, List<Map<String, String>>> controlledValues;
                if(field.getDescribe().getController() != null) {
                    dependant = true;
                    controlledValues = getDependentPicklistValues(field);
                }
                lstFields.add(new Map<String, Object>{
                    'id' => field.getDescribe().getName(),
                    'label' => field.getDescribe().getLabel(),
                    'value' => field.getDescribe().getName(),
                    'objectType' => sobjectName,
                    'type' => (fieldType.containsignorecase('picklist')) ? null : 
                                    (fieldType.containsignorecase('integer') || 
                                    fieldType.containsignorecase('double') || 
                                    fieldType.containsignorecase('long') || 
                                    fieldType.containsignorecase('currency')) ? 'number' : (!fieldType.containsignorecase('email')) ? 
                                                                                            'text' : 'email',
                    'numberOnly' => (fieldType.containsignorecase('integer') || 
                                    fieldType.containsignorecase('double') || 
                                    fieldType.containsignorecase('long') || 
                                    fieldType.containsignorecase('currency') || 
                                    fieldType.containsignorecase('phone')) ? true : false,
                    'list' => (fieldType.containsignorecase('picklist')) ? true : false,
                    'controlled' => dependant,
                    'controllingField' => (dependant) ? field.getDescribe().getController().getDescribe().getName() : null,
                    'controllingValues' => controlledValues,
                    'options' => values,
                    'edit' => false,
                    'editable' => false,
                    'icon' => 'utility:insert_tag_field',
                    'selected' => false
                });
            }
            return lstFields;
        } catch (Exception e) {
            System.debug(e.getMessage()+'at line '+e.getLineNumber());
            return null;
        }
    }

    @AuraEnabled
    public static String retrieveFieldsFromLayout(String layoutName, Boolean hasSections) {
        try {

            System.debug(layoutName);

            if(hasSections) {

                List<Map<String, Object>> lstAlex = new List<Map<String, Object>>();
                
                List<Metadata.Metadata> layouts = 
                Metadata.Operations.retrieve(Metadata.MetadataType.Layout, 
                new List<String> {layoutName});
                
                Metadata.Layout layoutMd = (Metadata.Layout)layouts.get(0);
                for(Metadata.LayoutSection section : layoutMd.layoutSections) {
                    Integer colIndex = 0;
                    List<Map<String, Object>> lstColumns = new List<Map<String, Object>>();
                    for (Metadata.LayoutColumn column : section.layoutColumns) {
                        Boolean areAllFieldsNull = true;
                        List<Map<String, Object>> lstFields = new List<Map<String, Object>>();
                        colIndex++;
                        if (column.layoutItems != null) {
                            for (Metadata.LayoutItem item : column.layoutItems) {
                                if(item.field != null) {
                                    areAllFieldsNull = false;
                                }
                                if(item.behavior != null && String.valueOf(item.behavior) != 'Readonly') {
                                    lstFields.add(new Map<String, Object>{'name' => item.field,
                                                                          'required' => (String.valueOf(item.behavior) == 'Required') ? true : false,
                                                                          'value' => ''});
                                }
                            }
                        }
                        if(!areAllFieldsNull) {
                            lstColumns.add(new Map<String, Object>{'index' => colIndex,
                                                                   'fields' => lstFields});
                        }
                    }
                    if(lstColumns.size() > 0) {
                        lstAlex.add(new Map<String, Object>{'sectionName' => section.label.replace(' ', '_'),
                                                            'sectionLabel' => section.label,
                                                            'columns' => lstColumns});
                    }
                }
                return JSON.serialize(lstAlex);

            } else {

                List<Map<String, Object>> lstFields = new List<Map<String, Object>>();

                List<Metadata.Metadata> layouts = 
                Metadata.Operations.retrieve(Metadata.MetadataType.Layout, 
                                            new List<String> {layoutName});
                System.debug(JSON.serialize(layouts));
                Metadata.Layout layoutMd = (Metadata.Layout)layouts.get(0);
                for(Metadata.LayoutSection section : layoutMd.layoutSections) {
                    for (Metadata.LayoutColumn column : section.layoutColumns) {
                        if (column.layoutItems != null) {
                            for (Metadata.LayoutItem item : column.layoutItems) {
                                if(item.behavior != null && String.valueOf(item.behavior) != 'Readonly') {
                                    lstFields.add(new Map<String, Object>{'name' => item.field,
                                                                        'required' => (String.valueOf(item.behavior) == 'Required') ? true : false,
                                                                        'value' => ''});
                                }
                            }
                        }
                    }
                }
                return JSON.serialize(lstFields);

            }

        } catch(Exception e) {
            System.debug(e.getMessage()+' at '+e.getLineNumber());
            return null;
        }
    }

    @AuraEnabled
    public static Id getRecordTypeId(String objectApiName, String recordTypeName) {
        Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = :objectApiName AND Name = :recordTypeName].Id;
        return recordTypeId;
    }

    @AuraEnabled
    public static Map<String, String> getLayoutByProfileAndRecordType(String recordTypeId) {
        String layoutId;
        String baseURL = Url.getOrgDomainUrl().toExternalForm();
        String profileId = UserInfo.getProfileId();
        
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer ' + SYS_SessionId_Controller.fetchUserSessionId());
        
        req.setHeader('Content-Type', 'application/json');
        
        req.setEndpoint(baseUrl + '/services/data/v52.0/tooling/query/?q=SELECT+Layout.Name,Layout.TableEnumOrId,ProfileId,Profile.Name,RecordTypeId+FROM+ProfileLayout+WHERE+recordTypeId=\'' + recordTypeId + '\'AND+profileId=\'' + profileId + '\'');
        req.setMethod('GET'); 
        Http h = new Http();
        HttpResponse res = h.send(req);
        system.debug(res.getBody());
        
        Map<String,Object> responseProfileLayout = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
        List<Object> objList = (List<Object>)responseProfileLayout.get('records');

        Map<String,Object> objMap = (Map<String,Object>)objList[0];
        Map<String,Object> layout = (Map<String,Object>)objMap.get('Layout');
        Map<String,Object> responseAttributes = (Map<String, Object>)layout.get('attributes');
        String layoutURL = (String)responseAttributes.get('url');
        String layoutName = (String)layout.get('Name');
        Map<String, String> results = new Map<String, String>{'layoutId' => layoutURL.substringAfterLast('/'),
                                                              'layoutName' => (String)layout.get('TableEnumOrId')+'-'+layoutName};
        return results;

    }

    @AuraEnabled
    public static String getFieldsFromLayoutWithRtypeAndProfile(String recordTypeId, Boolean hasSection){
        try {
            String layoutName = getLayoutByProfileAndRecordType(recordTypeId).get('layoutName');
            return retrieveFieldsFromLayout(layoutName, hasSection);
        } catch (Exception e) {
            System.debug(e.getMessage()+' '+e.getLineNumber());
            return null;
        }
    }

    @AuraEnabled
    public static SObject getRecordByGUID(String GUID, String objectApiName, String GUID_FIELD){
        if(GUID != null) {
        	try {
                List<String> fields = getObjectFields(objectApiName);
                System.debug(GUID);
                return Database.query('SELECT '+String.join(fields, ', ')+' FROM '+objectApiName+' WHERE '+GUID_FIELD+' = :GUID');
            } catch (Exception e) {
                System.debug(e.getMessage()+' '+e.getLineNumber());
                return null;
            }
        } else {
            return null;
        }
    }

    private static List<String> getObjectFields(String objectApiName) {
        return new List<String>(Schema.getGlobalDescribe().get(objectApiName).getDescribe().fields.getMap().keySet());
    }

    @AuraEnabled
    public static String uploadFile(String base64, String filename, String recordId, String ownerId){
        try {
            FileUploaderClass fupc = new FileUploaderClass();
            return fupc.uploadFile(base64, filename, recordId, ownerId);
        } catch(Exception ex) {
            System.debug(ex.getMessage()+' '+ex.getLineNumber());
            return null;
        }
    }

    public without sharing class FileUploaderClass {
        /*
        * @method uploadFile() 
        * @desc Creates a content version from a given file's base64 and name
        * 
        * @param {String} base64 - base64 string that represents the file
        * @param {String} filename - full file name with extension, i.e. 'products.csv'
        * @param {String} recordId - Id of the record you want to attach this file to
        * 
        * @return {ContentVersion} - returns the created ContentDocumentLink Id if the
        *   upload was successful, otherwise returns null
        */
        public String uploadFile(String base64, String filename, String recordId, String ownerId) {
            ContentVersion cv = createContentVersion(base64, filename, ownerId);
            ContentDocumentLink cdl = createContentLink(cv.Id, recordId);
            if (cv == null || cdl == null) { return null; }
            return cdl.Id;
        }
        /*
        * @method createContentVersion() [private]
        * @desc Creates a content version from a given file's base64 and name
        * 
        * @param {String} base64 - base64 string that represents the file
        * @param {String} filename - full file name with extension, i.e. 'products.csv'
        * 
        * @return {ContentVersion} - returns the newly created ContentVersion, or null
        *   if there was an error inserting the record
        */
        private ContentVersion createContentVersion(String base64, String filename, String ownerId) {
            ContentVersion cv = new ContentVersion();
            cv.VersionData = EncodingUtil.base64Decode(base64);
            cv.Title = filename;
            cv.PathOnClient = filename;
            try {
                insert cv;
                return cv;
            } catch(DMLException e) {
                System.debug(e);
                return null;
            }
        }
    
        /*
        * @method createContentLink() [private]
        * @desc Creates a content link for a given ContentVersion and record
        * 
        * @param {String} contentVersionId - Id of the ContentVersion of the file
        * @param {String} recordId - Id of the record you want to attach this file to
        * 
        * @return {ContentDocumentLink} - returns the newly created ContentDocumentLink, 
        *   or null if there was an error inserting the record
        */
        private ContentDocumentLink createContentLink(String contentVersionId, String recordId) {
            if (contentVersionId == null || recordId == null) { return null; }
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.ContentDocumentId = [
                SELECT ContentDocumentId 
                FROM ContentVersion 
                WHERE Id =: contentVersionId
            ].ContentDocumentId;
            cdl.LinkedEntityId = recordId;
            // ShareType is either 'V', 'C', or 'I'
            // V = Viewer, C = Collaborator, I = Inferred
            cdl.ShareType = 'V';
            try {
                insert cdl;
                return cdl;
            } catch(DMLException e) {
                System.debug(e);        
                return null;
            }
        }
    }

    @AuraEnabled
    public static Boolean deleteBySingleId(Id recordId){
        try {

            Database.DeleteResult dr = Database.delete(recordId, false);

            if (dr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully deleted record with ID: ' + dr.getId());
                return true;
            } else {
                // Operation failed, so get all errors                
                for(Database.Error err : dr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Record fields that affected this error: ' + err.getFields());
                }
            }

            return false;

        } catch(Exception ex) {
            System.debug(ex.getMessage()+' '+ex.getLineNumber());
            return null;
        }
    }
}