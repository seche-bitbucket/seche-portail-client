@Istest
public with sharing class ContactFacturationManagerTest {
   
    @isTest
    public static void updateContact() {
    
        Test.startTest();
        Contact ct = TestFactory.generateContact();

        Map<String,object> accountMap = new Map<String, object>( );
        accountMap.put ('ERP_BillingPhone__c', 'billing');
        accountMap.put ('PrimaryBillingContactEmail__c', 'test@test.com002');
        accountMap.put ('SecondaryBillingContactEmail__c', 'test@test.com003');
        accountMap.put ('GUID__c', 'ezzeez22223DDE');
        accountMap.put ('BillingContactUpdateLink__c', '');
        ContactFacturationManager.updateContact( ct.Id, accountMap);

        list <contact> contacts = [SELECT Id, ERP_BillingPhone__c, PrimaryBillingContactEmail__c, SecondaryBillingContactEmail__c, GUID__c, BillingContactUpdateLink__c   FROM Contact WHERE Id =: ct.Id]; 
        System.assertEquals(contacts[0].ERP_BillingPhone__c, 'billing', 'erreur update ERP_BillingPhone__c');
        Test.stopTest();
    }

    @isTest
    public static void testcreateCase() {
        Contact ct = TestFactory.generateContact();
        user u = testDataFactory.createAssistantCommercialUser();
        Map<String,object> caseMap = new Map<String, object>( );
        URL__c url = new URL__c ();
         url.Org_URL__c= 'http://test.com';
         insert url;
        list <RecordType> caseRecordTypes = [Select Id FROM RecordType where SobjectType = 'Case'];
        caseMap.put ('RecordTypeId', caseRecordTypes[0].Id);
        caseMap.put ('Status', 'Fermée');
        caseMap.put ('Origin', 'Mail');
        caseMap.put ('Definition__c', 'Information');
        caseMap.put ('ContactId', ct.Id);
        caseMap.put ('Subject', '');
        caseMap.put ('Reason', 'Commercial');
        caseMap.put ('Description', '');
        caseMap.put ('SuppliedEmail', 'test@test.commmmm1');
        caseMap.put ('Salesman__c', u.Id);
        caseMap.put ('OwnerId', u.Id);
        caseMap.put ('BillingSiteCode__c', '');

        ContactFacturationManager.createCase(caseMap);

        list <case> caseRcases = [Select Id FROM case];
        System.assertEquals(caseRcases.size() , 1, 'erreur creat case');
        //System.assertEquals(caseRcases[0].Status, 'Fermée', 'erreur update ERP_BillingPhone__c');
    }
}