/*
* --------------------------------------------------------------------------------------------------------------------------------------------
* -- - Name : ToolsDataAccessTest
* -- - Author : PMB
* -- - Company : Capgemini
* -- - Purpose : ToolsDataAccessTest
* --
* -- Maintenance History:
* --
* -- Date Name Version Remarks
* -- ----------- ---- ------- ----------------------------------------------------------------------------------------------------------------
* -- 27-02-2020 PMB 1.0 - Initial version
* -- 19-03-2020 PMB 1.1 - getOpportunitiesWithoutSharringByAccount_Test
* -- 08-06-2020 MGR 1.2 - Deletion of method getOpportunitiesWithoutSharringByAccount (which is now in the class LWC_AccOppWithoutSharingCtrl)
* --------------------------------------------------------------------------------------------------------------------------------------------
*/
@isTest
public with sharing class ToolsDataAccessTest {
    @isTest
    public static void getRecordTypeIdByName_Test(){
        RecordType rt = [SELECT Id,DeveloperName FROM RecordType WHERE SobjectType = 'Account' Limit 1];
        Id rtDeveloperName = ToolsDataAccess.getRecordTypeIdByName(Account.getSObjectType(),rt.DeveloperName);
        System.assertEquals(rt.Id, rtDeveloperName);
    }
}