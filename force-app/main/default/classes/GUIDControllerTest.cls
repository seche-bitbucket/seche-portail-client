@isTest
public with sharing class GUIDControllerTest {
    
    @isTest
    public static void testGUID() {
        List<Id> accList = new List<Id>();
        List<Id> conList = new List<Id>();

        Account acc = TestDataFactory.createAccount('Account name');
        insert acc;
        Contact con = TestDataFactory.createContact(acc);
        insert con;

        accList.add(acc.Id);
        conList.add(con.Id);

        Test.startTest();
        GUIDController.genGuid(accList);
        GUIDController.genGuid(conList);
        Test.stopTest();
    }

}