global class SchedulableBatchCreateOppQuote implements Schedulable{
    global void execute(SchedulableContext sc) {
        AVE_BatchCreateOPP  batchToExecute = new  AVE_BatchCreateOPP(); 
        database.executebatch(batchToExecute,10);
    }
}