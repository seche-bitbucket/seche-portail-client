@isTest(SeeAllData=true)
public class wasteStatementTriggerUtilTest {
    
    //FINISHED
    @isTest
    public static void removeQuotelineTest(){
        
         Test.startTest(); 
         
         //create Waste_Statement
         WST_Waste_Statement__c vWst = TestDataFactory.createWasteStatement('AccountName1');
         
         //Insert Waste_Statement
         insert vWst;
         Map<ID, QuoteLine__c> vtest = Transfo_AddPriceBookEntriesController.getQuoteLines(vWst.id);
         System.debug('removeQuotelineTest = ' +vtest);
        
        //create new ID
   		Set<ID> vToDeleteQuoteLine = new Set<ID>();
                  
        List<QuoteLine__c> vQls = [SELECT ID FROM QuoteLine__c WHERE Transformateur__c = : vWst.Id];
        
        //insert ID
    	vToDeleteQuoteLine.add(vWst.Id);

        wasteStatementTriggerUtil.removeQuoteline(vToDeleteQuoteLine);

        System.debug('removeQuotelineTest.vQls = ' +vQls);
        System.assertNotEquals(null,vQls);
        Test.stopTest();
    }

    
    //FINISHED
    @isTest
    public static void manageStateStatusTest(){
        
        Test.startTest(); 
         
         //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Demande effectuee';
        insert vWst1;
        
        WST_Waste_Statement__c vWst2 = TestDataFactory.createWasteStatement('AccountName2');
        vWst2.Status__c  ='Recu';
        insert vWst2;
        
        WST_Waste_Statement__c vWst3 = TestDataFactory.createWasteStatement('AccountName3');
        vWst3.Status__c  ='Commande acceptee';
        insert vWst3;
        
        WST_Waste_Statement__c vWst4 = TestDataFactory.createWasteStatement('AccountName4');
        vWst4.Status__c  ='En reparation';
        insert vWst4;
        
        WST_Waste_Statement__c vWst5 = TestDataFactory.createWasteStatement('AccountName5');
        vWst5.Status__c  ='En livraison';
        insert vWst5;
        
        //WST_Waste_Statement__c vWst6 = TestDataFactory.createWasteStatement('AccountName6');
        //vWst6.Status__c  ='Detruit';
        //insert vWst6;

        //Create liste WST_Waste_Statement
        List<WST_Waste_Statement__c> vWsts = new List<WST_Waste_Statement__c>();
        vWsts.add(vWst1);
        vWsts.add(vWst2);
        vWsts.add(vWst3);
        vWsts.add(vWst4);
        vWsts.add(vWst5);
        //vWsts.add(vWst6);

        //call the method
        wasteStatementTriggerUtil.manageStateStatus(vWsts);
        
        //check the result
        System.assertEquals('Demande effectuee', vWst1.Etat__c);
        System.assertEquals('Recu', vWst2.Etat__c);
        System.assertEquals('Devis accepte', vWst3.Etat__c);
        System.assertEquals('En reparation', vWst4.Etat__c);
        System.assertEquals('En livraison', vWst5.Etat__c);
        
        Test.stopTest();
    }

        @isTest
    public static void managePricebookAfterInsertTest(){
		Test.startTest(); 
        //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        
         vWst1.isTPC__c = true;
        insert vWst1;

        WST_Waste_Statement__c vWst2 = TestDataFactory.createWasteStatement('AccountName2');
        insert vWst2;
        
        //create Liste of WST_Waste_Statement
        List<WST_Waste_Statement__c> vWsts = new List<WST_Waste_Statement__c>();
        vWsts.add(vWst1);
        vWsts.add(vWst2);
        
        //call the Method
        wasteStatementTriggerUtil.managePricebookAfterInsert(vWsts);
        
        //check the result
        Test.stopTest();
        
    }
 
    
    
    @isTest
    public static void managePricebookBeforeUpdateTest(){
        Test.startTest(); 
        Map<ID,WST_Waste_Statement__c> vWstsMap = new Map<Id,WST_Waste_Statement__c>();
        List<WST_Waste_Statement__c> vWsts = new List<WST_Waste_Statement__c>();
       
           //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        
         vWst1.isTPC__c = true;
        insert vWst1;

        WST_Waste_Statement__c vWst2 = TestDataFactory.createWasteStatement('AccountName2');
        insert vWst2;
        
        vWst2.isTPC__c = false;    
        vWst1.isTPC__c = true;    
        vWsts.add(vWst2);
        vWsts.add(vWst1);
        
        //call the method
        wasteStatementTriggerUtil.managePricebookBeforeUpdate(vWsts);
        Test.stopTest();
    }

    
     //FINISHED
    @isTest
    public static void manageQuoteStatusTest(){ 
                
        Test.startTest(); 
        //create Waste_Statement
        WST_Waste_Statement__c vWst1 = TestDataFactory.createWasteStatement('AccountName1');
        vWst1.Status__c  ='Commande proposee';
        
        //insert 
        insert vWst1;
        
        //create a list WST_Waste_Statement
        List<WST_Waste_Statement__c> vWsts = new List<WST_Waste_Statement__c>();
        vWsts.add(vWst1);
       
        //call the method
        wasteStatementTriggerUtil.manageQuoteStatus(vWsts);      
        
        //check the result 
        Test.stopTest();
        
    }
       
}