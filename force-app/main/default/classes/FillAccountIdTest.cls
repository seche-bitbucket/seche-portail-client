@isTest
private class FillAccountIdTest {
    
    static testMethod void testWhoId() {
        User u = [Select Id From User Where ProfileId = '00e58000000VRRVAA4' Limit 1];
        
        Account acc = new Account();
        acc.Name = 'testAccName';
        acc.CustomerNature__c = 'Administration';
        acc.Industry = 'Aéronautique';
        acc.CurrencyIsoCode = 'EUR';
        acc.Producteur__c = false;
        acc.BillingPostalCode = '00000';
        acc.NAF_Number__c = '1234A';

        try {
            insert acc;
        } catch(Exception e) {
            System.debug(e.getMessage() + ' at ' + e.getLineNumber());
        }
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = acc.Id;
        c.Salesman__c = u.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '03 00 00 00 00';

        try {
            insert c;
        } catch(Exception e) {
            System.debug(e.getMessage() + ' at ' + e.getLineNumber());
        }
        
        Event e = new Event();
        e.Subject = 'Appel';
        e.StartDateTime = System.today();
        e.EndDateTime = System.today();
        e.WhoId = c.Id;
        e.Compte__c = null;

        try {
            insert e;
        } catch(Exception e1) {
            System.debug(e1.getMessage() + ' at ' + e1.getLineNumber());
        }
        
        Event eBis = [Select Id, Compte__c From Event Where Id =: e.Id];
        System.assert(eBis.Compte__c == acc.id);
    }
    
    static testMethod void testWhatId() {
        User u = [Select Id From User Where ProfileId = '00e58000000VRRVAA4' Limit 1];
        
        Account acc = new Account();
        acc.Name = 'testAccName';
        acc.CustomerNature__c = 'Administration';
        acc.Industry = 'Aéronautique';
        acc.CurrencyIsoCode = 'EUR';
        acc.Producteur__c = false;
        acc.BillingPostalCode = '00000';
        acc.NAF_Number__c = '1234A';

        try {
            insert acc;
        } catch(Exception e) {
            System.debug(e.getMessage() + ' at ' + e.getLineNumber());
        }
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = acc.Id;
        c.Salesman__c = u.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '03 00 00 00 00';

        try {
            insert c;
        } catch(Exception e) {
            System.debug(e.getMessage() + ' at ' + e.getLineNumber());
        }
        
        Opportunity opp = new Opportunity();
        opp.Name = 'test';
        opp.AccountId = acc.Id;
        opp.Salesman__c = u.Id;
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.ContactName__c = c.Id;
        opp.TonnageN__c = 0;
        
        try {
            insert opp;
        } catch(Exception e) {
            System.debug(e.getMessage() + ' at ' + e.getLineNumber());
        }
        
        Event e = new Event();
        e.Subject = 'Appel';
        e.StartDateTime = System.today();
        e.EndDateTime = System.today();
        e.WhatId = opp.Id;
        e.Compte__c = null;

        try {
            insert e;
        } catch(Exception e1) {
            System.debug(e1.getMessage() + ' at ' + e1.getLineNumber());
        }
        
        Event eBis = [Select Id, Compte__c From Event Where Id =: e.Id];
        System.assert(eBis.Compte__c == acc.Id);
    }
}