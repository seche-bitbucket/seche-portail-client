/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @modifiedBy     Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @maintainedBy   Alaeddine EL ASSAD  <alaeddine.el-assad@capgemini.com>
* @version        1.0
* @created        2018-11-26
* @modified       2018-11-26
* @systemLayer    Test
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		Class to test that documentForSignatureTrigger is operational and that all the updates are functional
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class DocumentForSignatureTriggerHandlerTest {
    
    @isTest static void DocumentForSignatureAfterUpdateTest(){
        Profile directp = [select id from profile where name = :'Directeur commercial' limit 1];
        String testemail2 = 'assistant_-_User_test@test.com';
        
        User director = new User(profileId = directp.id, username = testemail2, email = testemail2,
                                 emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                 languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                 alias='cspu', lastname='lastname', IsActive=true);
        
        insert director;
        Profile p = [Select Id from Profile where Name IN ('System Administrator', 'Administrateur Système') limit 1];
        
        String testemail = 'Commercial-_User_test@test.com';
        User Commercial = new User(profileId = p.id, username = testemail, email = testemail,
                                   emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                   languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                   alias='cs', lastname='lastname2', IsActive=true);
        insert Commercial;
        
        
        System.RunAs(Commercial) {    
            Account a = new Account();
            a.Name = 'testAccName';
            a.CustomerNature__c = 'Administration';
            a.Industry = 'Aéronautique';
            a.CurrencyIsoCode = 'EUR';
            a.Producteur__c = false;
            a.BillingPostalCode = '01000';
            a.NAF_Number__c = '1234A';
            insert a;
            
            Contact c = new Contact();
            c.LastName = 'testName';
            c.AccountId = a.Id;
            c.Salesman__c = director.Id;
            c.Email = 'aaa@groupe-seche.com';
            c.Phone = '03 00 00 00 00';
            insert c;
            Opportunity opp = new Opportunity();
            opp.Name = 'Mon OPP TEST';
            opp.StageName = 'Contractualisation';
            opp.CloseDate = System.today().addDays(10);
            opp.AccountId = a.Id;
            opp.TonnageN__c = 0;
            opp.Filiale__c = 'Tredi Hombourg';
            opp.Filiere__c = 'Physico';
            opp.ContactName__c = c.Id;
            opp.Salesman__c=director.Id;
            insert opp;
            
            Lead ld = new Lead (LastName = 'Test Last Name', Company = 'CAP',Status = 'Ouverte',LeadSource = 'Internet', WorkSiteZip__c = '97000', RecordTypeId = ToolsDataAccess.getRecordTypeIdByName(Lead.getSObjectType(),Constants.STR_LED_RET_DND),PostalCode = '69009',
                                 Waste__c = 'Déchet Amiante',AsbestosPackagingType__c = 'Palette',Quantity__c = 200,AsbestosPackagingSize__c = '3*4', Filiale__c = 'Séché Eco Industries Changé',Filiere__c = 'Biocentre',
                                 Packaging_1__c = 'Benne', WasteType1__c = 'test', Volume_1__c = '25 L', Estimated_Duration_Of_Commissioning__c = Constants.STR_DURATION_15_30,
                                 CustomerNature__c = 'Industriel', Industry = 'Aéronautique');
            insert ld;
                
            T5F_PreFip__c prefip = new T5F_PreFip__c();
            prefip.Origin__c = 'INSTALLATIONS DE TRAITEMENT - Autre Provenance';
            prefip.WasteType__c = 'Autres déchets';
            prefip.OriginDescription__c = 'test';
            prefip.WasteDescription__c = 'test';
            prefip.Contact__c = c.Id;
            prefip.Account__c = a.Id;
            prefip.Opportunity__c = opp.Id;
            prefip.Status__c = 'Doct final généré';
            prefip.Lead__c = ld.Id;
            prefip.DocumentStatus__c = 'Pré-FIP finale générée';
            insert prefip;
            
            DocumentsForSignature__c doc2 = new DocumentsForSignature__c(
                                     Status__c = 'PENDING',
                TECH_ExternalId__c = prefip.id,
                TECH_UpdatedWithApex__c=true,
                                     T5F_PreFip__c=prefip.Id);
            insert doc2;
            
            Quote q = new Quote();
            q.OpportunityId = opp.ID;
            q.Name = opp.Name;
            q.DateDevis__c = Date.today();
            q.QuoteCode__c=opp.Name;
            q.ContactId = opp.ContactName__c;
            q.Email = 'secheemail@gmail.com';
            q.Phone = opp.ContactName__r.Phone;
            q.Fax = opp.ContactName__r.Fax;
            q.TECH_UID__c='9001';
            q.BillingStreet = opp.Account.BillingStreet;
            q.BillingCity = opp.Account.BillingCity;
            q.BillingState = opp.Account.BillingState;
            q.BillingPostalCode = opp.Account.BillingPostalCode;
            q.BillingCountry = opp.Account.BillingCountry;
            insert q;
            DocumentsForSignature__c doc=new DocumentsForSignature__c();
            doc.Quote__c=q.Id;
            doc.TECH_ExternalId__c=q.Id;
            Insert doc;
            List<DocumentsForSignature__c> Docs=new List<DocumentsForSignature__c>();
            docs.add(doc);
            Test.startTest();
            doc.Status__c='FINISHED';
            doc2.Status__c='FINISHED';
            Update doc;
             Update doc2;
            prefip = [Select Status__c FROM T5F_PreFip__c WHERE Id = :prefip.Id];
            Test.stopTest();
            Opportunity o=[SELECT Id,StageName,isWon FROM Opportunity Where ID=:opp.Id];
            doc.Status__c='REJECTED';
            doc2.Status__c='REJECTED';
            Update doc;
             Update doc2;
            Quote qtoTest=[SELECT Id,Status FROM QUOTE WHERE Id=:q.Id];
            System.assertEquals('Refusé', qtoTest.Status);
            prefip = [Select Status__c FROM T5F_PreFip__c WHERE Id = :prefip.Id];
        }
    }
}