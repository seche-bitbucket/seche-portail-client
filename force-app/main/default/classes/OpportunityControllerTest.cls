@isTest
public class OpportunityControllerTest {
    @IsTest(SeeAllData=true) 
    public static void OpportunityController(){
        //Set the current VF Page which use the ContactController
        PageReference pgNouveauCROpportunite = Page.CR_NouveauCROpportunite;
        
        //Standard Set controller
        ApexPages.StandardSetController stdSetCtrl;
        
        //Standard controller
        ApexPages.StandardController stdCtrl;
        
        //Get a profile to create User
        Profile p = [select id from profile where name = :'Chargé(e) d\'affaires' limit 1];
        
        //Create a User for the Test 
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    SubsidiaryCreationRight__c = 'Alcea',
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname');
        
        System.RunAs(currentUser) { 
            
            //Get a Record Type for Account
            RecordType rt = [Select Id From RecordType Where SobjectType='Account' And DeveloperName ='ACC_France' LIMIT 1];
            
            //Create Account
            Account ac = new Account(Name = 'TEST ACCOUNT', RecordTypeId = rt.Id, CustomerNature__c = 'Administration', Industry = 'Aucun', BillingPostalCode = '69000', Producteur__c = false, NAF_Number__c='1234A');
            
            try{
                insert ac;
            }catch(Exception e){
            }
            
            //Create Contact
            Contact contactTest = new Contact(LastName = 'TESTCONTACT', AccountID= ac.Id, email ='test@test.com', Phone = '03 00 00 00 00', Salesman__c = currentUser.ID);
            
            try{
                insert contactTest;
            }catch(Exception e1){
            }
            
            //Get a Record Type for Opportunity
            RecordType rt2 = [Select Id From RecordType Where SobjectType='Opportunity' And DeveloperName ='OPP_06_AutreOffreSecteurPriveStandard' LIMIT 1];
            
            //Create Opportunity
            Opportunity opportTest = new Opportunity(Name = 'TESTOPPORT', RecordTypeId = rt2.Id, AccountID= ac.Id, ContactName__c = contactTest.Id , Nature__c = 'Spot', CloseDate = date.today(), StageName ='Analyse du cahier des charges', Amount=1234, Filiale__c = 'Alcea', Filiere__c ='DASRI');
            
            try{
                insert opportTest;
            }catch(Exception e2){
                System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! e2: ' + e2);
            }                        
            //    Start TEST
            
            Test.startTest();
            
            // Set the currentPage to the test
            Test.setCurrentPage(pgNouveauCROpportunite);
            //Set parameter 
            //System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! id Opport: ' + opportTest.Id);
            pgNouveauCROpportunite.getParameters().put('ID', opportTest.Id );
            
            //Initialize Controller
            OpportunityController ctrl = new OpportunityController(stdSetCtrl);
            
            System.assertEquals(ctrl.Account.Name, ac.Name);
            System.assertEquals(ctrl.Contact.Name, contactTest.LastName);
            System.assertEquals(ctrl.Opportunity.Name, opportTest.Name);
            
            PageReference pageRef = ctrl.generateURL();            
            //Initialize Controller
            ctrl = new OpportunityController(stdCtrl);
            
            Test.stopTest();
            
            //    End TEST
        }
    }
}