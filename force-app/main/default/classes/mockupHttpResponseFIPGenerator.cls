@isTest
global class mockupHttpResponseFIPGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        if(req.getMethod() == 'POST'){
            System.assertEquals('POST', req.getMethod());
        }else if(req.getMethod() == 'GET'){
            System.assertEquals('GET', req.getMethod());
        }
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
         res.setBody('{"Collector":"0019E00000cLWzSQAW","ContactID":"0039E00000YY4LUQA1","TypeFIP":"Nouveau dossier","NCAP":"1","CAP":"2","Appelation":"NomD","CodeNomen":"16 01 11* (patins de freins contenant de lamiante)","Quantite":2,"TypeAmiante":"Amiante libre","TypeCondi":"Body Benne","DepotBag":3,"NONU":"2590","Classe":"9","GEmballage":"III","Rsociale":"Raison socialeP","Siret":123456,"Adr1":"AdresseP1","Adr2":"AdresseP2","Adr3":"AdresseP3","CodePostal":"69000","Ville":"LYON","AdrChantier":"AdresseP4"}');
         res.setStatusCode(200);
        return res;
        }
     
}