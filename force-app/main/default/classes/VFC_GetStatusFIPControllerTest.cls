@isTest
public with sharing class VFC_GetStatusFIPControllerTest {
   static testMethod void initTest() {
     
    String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

    //Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;

    Account a = new Account();
    a.Name = 'testAccName';
    a.CustomerNature__c = 'Administration';
    a.Industry = 'Trader';
    a.CurrencyIsoCode = 'EUR';
    a.Producteur__c = false;
    a.BillingPostalCode = '00000';
    a.NAF_Number__c = '1234A';
    a.BillingStreet = '12 rue Pré Gaudry';
    insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName1';
        c.AccountId = a.Id;
        c.Email = 'aaaa@groupe-seche.com';
        c.Phone = '03 00 00 00 00';
        insert c;

        FIP_FIP__c fip = new FIP_FIP__c();
        system.debug('Collector__c = '+ a.ID);
        fip.Collector__c = a.ID;
        system.debug('Contact__c = '+ c.ID);
        fip.Contact__c = c.ID;
        insert fip;
    
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData = Blob.valueOf('TEST PM');          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=fip.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;
        Package__c pkg = new Package__c(
            ConnectivePackageId__c='3bb6a2d1-35db-4a3a-a434-868c01bcca9d',
            ConnectivePackageStatus__c='Pending'
        );
        insert pkg;

        DocumentsForSignature__c doc=new DocumentsForSignature__c();
        doc.Name= cv.title;
        doc.SigningUrl__c='';
        doc.SentTo__c=c.Id;
        doc.TECH_ExternalId__c=cv.Id;
        doc.ConnectiveDocumentId__c = cv.Id;
        doc.Status__c = 'PENDING';
        doc.FIP_FIP__c=fip.Id;
        doc.Package__c = pkg.Id;
        insert doc;

	System.RunAs(director) {
         Test.startTest();   
		List<FIP_FIP__c> fipList = [SELECT Name FROM FIP_FIP__c LIMIT 20];				
		PageReference pageRef = Page.VFP_GetStatusFIP;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',fipList[0].Id);
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(fipList);
         VFC_GetStatusFIPController myController = new VFC_GetStatusFIPController(ssc);
        Test.setMock(HttpCalloutMock.class, new ConnectiveMockGetStatus()); 
        ConnectiveMockGetStatus.externalIdContact = c.Id;
        ConnectiveMockGetStatus.externalIdDocument = testContent.Id;
        ConnectiveMockGetStatus.connectiveIdPackage = pkg.ConnectivePackageId__c;  
        myController.setIsTestRunnig(false);           
        myController.init();
        System.assertEquals(false, myController.getIsNoSelectedOpp());
        myController.goback(); 
        
	   Test.stopTest();    
	}
}

static testMethod void initTestWithNoSelected() {
     
    String[] profiLisList = new String[] {'Administrateur système', 'System Administrator'};

    //Get a profile to create User
	Profile p = [select id from profile where name in:profiLisList limit 1];

	String testemail2 = 'assistant_-_User_test@test.com';

	User director = new User(profileId = p.id, username = testemail2, email = testemail2,
	                         emailencodingkey = 'UTF-8', localesidkey = 'en_US',
	                         languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
	                         alias='cspu', lastname='lastname', IsActive=true);
	insert director;

    Account a = new Account();
    a.Name = 'testAccName';
    a.CustomerNature__c = 'Administration';
    a.Industry = 'Trader';
    a.CurrencyIsoCode = 'EUR';
    a.Producteur__c = false;
    a.BillingPostalCode = '00000';
    a.NAF_Number__c = '1234A';
    a.BillingStreet = '12 rue Pré Gaudry';
    insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName1';
        c.AccountId = a.Id;
        c.Email = 'aaaa@groupe-seche.com';
        c.Phone = '03 00 00 00 00';
        insert c;      
       
	
	System.RunAs(director) {
		List<FIP_FIP__c> fipList = [SELECT Name FROM FIP_FIP__c LIMIT 20];				
		PageReference pageRef = Page.VFP_GetStatusFIP;
        Test.setCurrentPage(pageRef);        
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(fipList);
        VFC_GetStatusFIPController myController = new VFC_GetStatusFIPController(ssc);	
        myController.setIsTestRunnig(true);
        myController.init();        
        System.assertEquals(true, myController.getIsNoSelectedOpp());
        System.assertEquals(true, myController.getIsTestRunnig());
       
	}
}
}