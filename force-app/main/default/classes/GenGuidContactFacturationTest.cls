@Istest
public with sharing class GenGuidContactFacturationTest {
    @isTest
    public static void testGenGuidContactFacturation() {
      URL__c url = new URL__c ();
      url.SITE_ContactFacturation__c= 'http://test.com';
      insert url;
        GenGuidContactFacturation.FormulaireId formId = new GenGuidContactFacturation.FormulaireId();
        Contact ct = TestFactory.generateContact();
        FormId.FormulaireId = ct.Id;
        List<GenGuidContactFacturation.FormulaireId> lst = new List<GenGuidContactFacturation.FormulaireId>{FormId};
        GenGuidContactFacturation.genGuidContactFacturation(lst);

    }

}