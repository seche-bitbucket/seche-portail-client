@isTest
public with sharing class AP_PreFipManagerTest {
    
    @isTest
    public static void test() {

        List<Producer_Certificate_Questions__mdt> qst = AP_PreFipManager.fetchQuestions('Collectivité et administration');

        System.assert(qst.size() > 0);

        ByPassUtils.ByPass('QuoteTrigger');
            
        QteLineItemSyncTriggerV2Handler.stop();
        OppLineItemSyncTriggerV2Handler.stop();
        QuoteSyncTriggerV2Handler.stop();
        OppSyncTriggerV2Handler.stop();

        Account a = new Account();
        a.Name = 'testAccName';
        a.CustomerNature__c = 'Administration';
        a.Industry = 'Trader';
        a.CurrencyIsoCode = 'EUR';
        a.Producteur__c = false;
        a.BillingPostalCode = '00000';
        a.NAF_Number__c = '1234A';
        a.BillingStreet = '12 rue Pré Gaudry';
        a.SIRET_Number__c = '11111111111111';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testName';
        c.AccountId = a.Id;
        c.Email = 'aaa@yopmail.com';
        c.Phone = '000000000';
        insert c;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Mon OPP TEST';
        opp.StageName = 'Contractualisation';
        opp.CloseDate = System.today().addDays(10);
        opp.AccountId = a.Id;
        opp.TonnageN__c = 0;
        opp.Filiale__c = 'Tredi Hombourg';
        opp.Filiere__c = 'Physico';
        opp.ContactName__c = c.Id;
        insert opp;
        
        Product2 prd = new product2(name = 'Test' ,TECH_ExternalID__c = '012345',Family = 'Traitement', isActive = true );
        insert prd;
        
        PricebookEntry pbe = new PricebookEntry(unitprice=0.01,Product2Id=prd.Id,Pricebook2Id=Test.getStandardPricebookId(),
                                 isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem (OpportunityID = opp.id,PriceBookEntryID=pbe.id, quantity=4, UnitPrice=4);
            insert oli;
            
        Id rtId = [SELECT Id FROM RecordType WHERE DeveloperName LIKE '%Attestation_Producteur_DND%'][0].Id;
        Form__c form = TestFactory.generateForm(opp, c);
        Attestation__c att = new Attestation__c(Account__c = a.Id, RecordTypeId = rtId, OpportunityLineItem__c = oli.Id, Form__c = form.Id);
        insert att;

        att = Database.query('SELECT '+String.join(new List<String>(Schema.getGlobalDescribe().get('Attestation__c').getDescribe().fields.getMap().keySet()), ', ')+' FROM Attestation__c WHERE Id = \''+att.Id+'\'');

        System.assert(AP_PreFipManager.fetchLinkedAttestation(new List<Id>{oli.Id}).size() == 1);

        System.assert(AP_PreFipManager.fetchLinkedAttestationsFromForm(new List<Id>{form.Id}).size() == 1);

        AP_PreFipManager.AttestationsList lst = new AP_PreFipManager.AttestationsList();
        lst.atts = new List<Attestation__c>{att};

        AP_PreFipManager.createPackages(new List<AP_PreFipManager.AttestationsList>{lst});

        opp.DateFinEngagement__c = Date.today();
        opp.StageName = 'Clôturée / Gagnée';
        update opp;

        String oppJson = JSON.serialize(new List<OpportunityLineItem>{oli});

        List<Feature__c> fs = new List<Feature__c>();
        for(Producer_Certificate_Questions__mdt q: qst) {
            fs.add(new Feature__c(
                Attestation__c = att.Id,
                Question__c = q.Question__c,
                Answer__c = 'Oui',
                SortingWasteInstruction__c = 'test',
                Description__c = 'test'
            ));
        }

        String featuresJson = JSON.serialize(fs);

        Map<String, String> producer = new Map<String, String>{
            'Address' => '43 RUE DE LA THIBAUDIERE, 69007 LYON',
            'Email' => 'test@test.com',
            'FirstName' => 'test',
            'LastName' => 'test',
            'Corp' => 'test'
        };

        String producerJson = JSON.serialize(producer);

        System.assertEquals(true, AP_PreFipManager.validateSign(oppJson, featuresJson, producerJson, '43 RUE DE LA THIBAUDIERE, 69007 LYON', JSON.serialize(new List<Attestation__c>{att})));

    }

}