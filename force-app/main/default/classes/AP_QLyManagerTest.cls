@isTest
public class AP_QLyManagerTest {


    private static Account createAccount(){
        /* Account account = new Account(
            Name = 'AccountName'
        );
        insert account; */
        Account account = TestDataFactory.createAccountWithInsert('AccountName');
        return account;
    }

    private static AutomationInputs__c createAutomationInput(){
        AutomationInputs__c automationInput = new AutomationInputs__c (
            QLYCollection__c = '1ère collecte mai/juin 2023',
            Attestation_counter__c = 4
        );
        insert automationInput;
        return automationInput;
    }

    private static Contact createContact(){
        Contact contact = new Contact(
            Email = 'test@test.com',
            LastName = 'ContactLastName'
        );
        insert contact;
        return contact;
    }

    private static QLY_SchoolForm__c createQLY(Id contactId, Id accountId){
        QLY_SchoolForm__c qly = new QLY_SchoolForm__c(
            DocumentStatus__c = 'Non Généré',
            Contact__c = contactId,
            Account__c = accountId,
            Name = 'QLyName',
            Collection__c = '1ère collecte mai/juin 2023'
        );
        insert qly;
        return qly;
    }

    private static Campaign createCampaign(){
        Campaign campaign = new Campaign(
            Name = 'QLY_CampaignName',
            CurrencyIsoCode  ='EUR',
            StartDate=System.today(),
            EndDate=System.today(),
            IsActive=true
        );
        insert campaign;
        return campaign;
    }

    private static CampaignMember createCampaignMember(Id campaignId, Id contactId){
        CampaignMember campaignMember = new CampaignMember(
            ContactId = contactId,
            CampaignId = campaignId        );
        insert campaignMember;
        return campaignMember;
    }

    private static Pricebook2 createPriceBook(){
        Pricebook2 priceBook = new Pricebook2(
            Name = 'PriceBookName'
        );
        insert priceBook;
        return priceBook;
    }

    private static Product2 createProduct(String productNumber){
        Product2 product = new Product2(
            IsActive=true,
            Name = 'ProductName '+productNumber,
            ProductCode = 'ProductCode ' + productNumber,
            TECH_ExternalID__c  = 'ExternalID'+productNumber,
            QLY_AvailablePackaging__c = 'Bidon 10L min avec bouchon;Bidon 20L min avec bouchon;Bidon 5L min avec bouchon;Cuve de 1000 litres de transport (Cuve + palette) type GRV - IBC ou équivalent'
        );
        insert product;
        return product;
    }

    private static PricebookEntry createPriceBookEntry(Id productId, Id priceBookId){
        Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = productId, UnitPrice = 2, IsActive = true);
        insert standardPBE;

        PricebookEntry priceBookEntry = new PricebookEntry(
            UnitPrice = 1,
            Product2Id = productId,
            Pricebook2Id = priceBookId,
            UseStandardPrice = false,
            IsActive = true
        );
        insert priceBookEntry;
        return priceBookEntry;
    }

    private static QLY_Service__c createQLY_Service(Id qlyId, Id productId){
        QLY_Service__c qlyService = new QLY_Service__c(
            QLY_SchoolForm__c = qlyId,
            Product__c = productId
        );
        insert qlyService;
        return qlyService;
    }

    private static String getFormString(String contactId, String companyId){
        return '{"name":"Lucas DORMOY LYCÉE","address":{"city":"Lyon","street":"Stade Gerland","postalCode":"69007","country":"France"},"companyId":"'+companyId+'","companyPhone":"0102030406","companyAddressText":"Groupama Stadium, 69150 Décines Charpieu","contactId":"'+contactId+'","phone":"0102030405","email":"lucas.dormoy@capgemini.com","company":"LYCÉE DORMOY","haveCollectThisYear":true,"wantChangeAddress":true,"isSameAddressForWaste":false,"days":[{"rank":0,"name":"Lundi","morning":{"morning":"matin","selected":true},"afternoon":{"afternoon":"après-midi","selected":true}},{"rank":1,"name":"Mardi","morning":{"morning":"matin","selected":true},"afternoon":{"afternoon":"après-midi","selected":true}},{"rank":2,"name":"Mercredi","morning":{"morning":"matin","selected":true},"afternoon":{"afternoon":"après-midi","selected":true}},{"rank":3,"name":"Jeudi","morning":{"morning":"matin","selected":true},"afternoon":{"afternoon":"après-midi","selected":true}},{"rank":4,"name":"Vendredi","morning":{"morning":"matin","selected":true},"afternoon":{"afternoon":"après-midi","selected":true}}],"mondayMorning":"Lundi matin","tuesdayMorning":"Mardi matin","wednesdayMorning":"","thursdayMorning":"Jeudi matin","fridayMorning":"Vendredi matin","mondayAfternoon":"","tuesdayAfternoon":"Mardi après-midi","wednesdayAfternoon":"","thursdayAfternoon":"Jeudi après-midi","fridayAfternoon":"Vendredi après-midi","selectedDays":[],"comment":"","isAccepted":true,"isNotify":true,"priceBookId":"01s3O000000TKxZQAW","access19TCheckBox":true,"access7TCheckBox":true,"access3TCheckBox":true,"firstCollectCheckBox":true,"firstCollect": "1ère collecte mai/juin 2023","companyAddress":{"street":"Groupama Stadium","city":"Décines Charpieu","province":"Auvergne-Rhône-Alpes","country":"France","postalCode":"69150"},"removalAddress":{"street":"Stade Gerland","city":"Lyon","province":"","country":"","postalCode":"69007"},"removalAddressText":"Stade Gerland, 69007 Lyon","selectedStep":"Step4"}';
    }

    private static String getWasteString(String productId){
        return '[{"files":[], "id":"checkbox", "name":"WasteTestName", "productId":"'+ productId +'", "rows":[{"addContainer":true, "adrIcon":"", "classification":"", "clpIcon":"", "containerId":"", "description":"null", "displayStyle":"Normal", "features":"null", "files":[], "index":"0", "isSelected":true, "nbrPackaging":"3", "numbContainer":"3", "originTreatmentService":"", "packaging":"ProductName 1", "peroxyName":"", "peroxydeQuantity":"", "photoListQuantity":"", "photoQuantity":"", "quantity":"2", "unitPrice":"0.3", "wasteId":""}]}]';
    }

    private static String getServiceString(String productId){
        return '[{"id":"", "isSelected":true, "name":"ServiceTestName", "productId":"'+ productId +'", "serviceIcon":"", "unitPrice":"200.0"}]';
    }

    private static String getContainerString(String productId){
        return '[{"addContainer":true, "containerId":"", "havePlug":false, "id":"", "isSelected":true, "name":"ContainerTestName", "numberContainer":"4", "numberPlug":"4", "productId":"'+productId+'", "unitPrice":"5.3"}]';
    }

    @isTest
    public static void fetchUserInfoTest() {
        Profile p = [select id from profile where name = :'Assistante commerciale' limit 1];
        String testemail = 'currentUserTest@cap.com';
        
        User currentUser = new User(profileId = p.id, username = testemail, email = testemail,
                                    emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                                    languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                    alias='cspu', lastname='lastname', SocieteF_t__c = true);
        
        insert currentUser;
        
        System.RunAs(currentUser){
            List<User> result = AP_QLyManager.fetchUserInfo(currentUser.Id);
            System.assert(result.size() > 0, 'fetchUserInfo returned no result');
        }
    }

    @isTest
    public static void fetchSchoolServicesTest(){
        Account account = createAccount();
        Contact contact = createContact();
        QLY_SchoolForm__c qly = createQLY(contact.Id, account.Id);
        List<QLY_SchoolForm__c> result = AP_QLyManager.fetchSchoolServices(account.Id, '1ère collecte mai/juin 2023');
        System.assert(result.size() > 0, 'fetchSchoolServices returned no result');
    }

    @isTest
    public static void fetchUserCampaignInfoTest(){
        Contact contact = createContact();
        Campaign campaign = createCampaign();
        CampaignMember campaignMember = createCampaignMember(campaign.Id, contact.Id);
        List<CampaignMember> result = AP_QLyManager.fetchUserCampaignInfo(contact.Id);
        System.assert(result.size() > 0, 'fetchUserCampaignInfo returned no result');
    }

    @isTest(seeAllData=true)
    public static void fetchCampaignProductsTest(){
        Product2 product = createProduct('1');
        Pricebook2 priceBook = createPriceBook();
        PricebookEntry priceBokEntry = createPriceBookEntry(product.Id, priceBook.Id);
        List<PricebookEntry> result = AP_QLyManager.fetchCampaignProducts(priceBook.Id);
        System.assert(result.size() > 0, 'fetchCampaignProducts returned no result');
    }

    @isTest(seeAllData=true)
    public static void getCampaignProductsPricesTest(){
        Product2 product = createProduct('1');
        Pricebook2 priceBook = createPriceBook();
        PricebookEntry priceBookEntry = createPriceBookEntry(product.Id, priceBook.Id);
        Map<String, Double> result = AP_QLyManager.getCampaignProductsPrices(priceBook.Id);
        System.assert(result.size() > 0, 'getCampaignProductsPrices returned no result');
    }

    @isTest
    public static void getAvailablePackagingTest() {
        Product2 product = createProduct('5');
        List<Product2> lstProd = AP_QLyManager.getAvailablePackaging();
        System.assertNotEquals(null, lstProd[0].QLY_AvailablePackaging__c);

    }

    @isTest(seeAllData=true)
    public static void processSaveTest(){
        Product2 product1 = createProduct('1');
        Product2 product2 = createProduct('2');
        Product2 product3 = createProduct('3');
        
        Account account = createAccount();
        Contact contact = createContact();
        /* AutomationInputs__c automationInput = createAutomationInput(); */

        String form = getFormString((String)contact.Id, (String)account.Id);
        String waste = getWasteString((String)product1.Id);
        String service = getServiceString((String)product2.Id);
        String container = getContainerString((String)product3.Id);
        String status = 'Terminé';

        Test.startTest();
        String stringResult = AP_QLyManager.processSave(form, waste, service, container, status);
        List<LwcQLyWrapper.Form> objListResult = (List<LwcQLyWrapper.Form>)JSON.deserialize(stringResult, List<LwcQLyWrapper.Form>.class);
        
        System.assert(objListResult.size() > 0, 'processSave returned no result');
        LwcQlyWrapper.Form objResult = objListResult[0];
        ID formId = objResult.formId;
        System.assert(formId != null , 'formId is null and should not be null');

        ID companyIDResult = account.Id;
        Account accountResult = [SELECT Id, BillingCity, BillingStreet, BillingPostalCode, Phone FROM Account WHERE Id = :companyIDResult];
        System.assert((String)accountResult.BillingCity == 'Décines Charpieu' && (String)accountResult.BillingStreet == 'Groupama Stadium' && (String)accountResult.BillingPostalCode == '69150', 'Company Address has not been saved correctly : ' + (String)accountResult.BillingCity + ' ' + (String)accountResult.BillingStreet + ' ' + (String)accountResult.BillingPostalCode);
        System.assert((String)accountResult.Phone == '0102030406', 'Company Phone has not been saved correctly : ' + (String)accountResult.Phone);

        ID contactIDResult = contact.Id;
        Contact contactResult = [SELECT Id, Phone FROM Contact WHERE Id = :contactIDResult];
        System.assert((String)contactResult.Phone == '0102030405', 'Contact Phone has not been saved correctly : ' + (String)contactResult.Phone);
        
        String cancelled = AP_QLyManager.cancelForm(form);
        System.assertEquals('NOK', cancelled);
        
        ContentVersion v = AP_QLyManager.uploadFileWithRecordId('VGVzdCBGaWxlIFVwbG9hZA==', 'Test', formId, product1.Id);
        System.assertNotEquals(null, v);
        
        v = AP_QLyManager.uploadFile('VGVzdCBGaWxlIFVwbG9hZA==', 'Test', product1.Id);
        System.assertNotEquals(null, v);
                
        Test.stopTest();
    }

    @isTest
    public static void processDeleteTest(){
        Product2 product1 = createProduct('1');
        Product2 product2 = createProduct('2');
        Account account = createAccount();
        Contact contact = createContact();
        /* AutomationInputs__c automationInput = createAutomationInput(); */
        QLY_SchoolForm__c qly = createQLY(contact.Id, account.Id);
        QLY_Service__c qlyService1 = createQLY_Service(qly.Id, product1.Id);
        QLY_Service__c qlyService2 = createQLY_Service(qly.Id, product2.Id);
        List<String> serviceIds = new List<String>();
        serviceIds.add((String)qlyService1.Id);

        Test.startTest();
        AP_QLyManager.processDelete(serviceIds);
        List<QLY_Service__c> services = [SELECT Id FROM QLY_Service__c];
        System.assertEquals(1, services.size());
        Test.stopTest();

    }

    @isTest
    public static void processCampaignMemberStatusTest(){
        /* AutomationInputs__c automationInput = createAutomationInput(); */
        Contact contact = createContact();
        Campaign campaign = createCampaign();
        CampaignMember campaignMember = createCampaignMember(campaign.Id, contact.Id);
        AP_QLyManager.processCampaignMemberStatus((String)campaignMember.Id);
        List<CampaignMember> campaignMembers = [SELECT Id, Status FROM CampaignMember WHERE Id = :campaignMember.Id];
        System.assert(campaignMembers[0].Status == 'Sent', 'processCampaignMemberStatus did not update status');
    }

    private static ContentDocumentLink createContentDocumentLink(QLY_SchoolForm__c qly){
        ContentVersion content=new ContentVersion(); 
        content.Title='ContentTest'; 
        content.PathOnClient='/' + content.Title + '.jpg'; 
        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
        content.VersionData=bodyBlob; 
        //content.LinkedEntityId=sub.id;
        content.origin = 'H';
        insert content;
        ContentDocumentLink contentLink = new ContentDocumentLink();
        contentLink.LinkedEntityId = qly.id;
        contentLink.contentdocumentid = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =: content.id].ContentDocumentId;
        contentLink.ShareType = 'I';
        contentLink.Visibility = 'AllUsers'; 
        insert contentlink;
        return contentLink;
    }

    @isTest
    public static void getContentDocumentFromFormTest(){
        Account account = createAccount();
        Contact contact = createContact();
        QLY_SchoolForm__c qly = createQLY(contact.Id, account.Id);
        /* AutomationInputs__c automationInput = createAutomationInput(); */

        ContentDocumentLink contentLink = createContentDocumentLink(qly);

        Test.startTest();
        List<ContentDocument> contents = AP_QLyManager.getContentDocumentFromForm((String)qly.Id, false);
        List<ContentVersion> versions = AP_QLyManager.getContentVersionFromForm((String)qly.Id, false);
        System.assertEquals(1, versions.size());
        System.assertEquals(1, contents.size());
        Test.stopTest();
        
    }

    @isTest
    public static void prefixSummaryTest(){
        Account account = createAccount();
        Contact contact = createContact();
        QLY_SchoolForm__c qly = createQLY(contact.Id, account.Id);
        /* AutomationInputs__c automationInput = createAutomationInput(); */

        ContentDocumentLink contentLink = createContentDocumentLink(qly);
        List<String> contentIds = new List<String>();
        contentIds.add((String)contentLink.ContentDocumentId);

        Test.startTest();
        List<ContentDocument> documents = AP_QLyManager.prefixSummary(contentIds);
        Boolean contentTitlesBeginWithSynthese = true;
        for(ContentDocument document : documents){
            if(!document.Title.contains('Synthèse - ')){
                contentTitlesBeginWithSynthese = false;
            }
        }
        System.assert(contentTitlesBeginWithSynthese, 'Titles of documents have not been correctly updated');
    }

    @isTest
    public static void deleteContentDocumentTest(){
        Account account = createAccount();
        Contact contact = createContact();
        QLY_SchoolForm__c qly = createQLY(contact.Id, account.Id);
        /* AutomationInputs__c automationInput = createAutomationInput(); */

        ContentDocumentLink contentLink = createContentDocumentLink(qly);
        String docId = (String)contentLink.ContentDocumentId;
        
        String vId = [SELECT Id FROM ContentVersion WHERE ContentDocumentId = :docId].Id;
		ContentDocument version = AP_QLyManager.getDocFromVersion(vId);
        System.assertNotEquals(null, version);
        
        Test.startTest();
        Boolean result = AP_QLyManager.deleteContentDocument(docId);
        System.assert(result, 'Document has not been deleted');
        Test.stopTest();
        
    }

    @isTest
    public static void deleteContentDocumentsTest(){
        Account account = createAccount();
        Contact contact = createContact();
        QLY_SchoolForm__c qly = createQLY(contact.Id, account.Id);
        /* AutomationInputs__c automationInput = createAutomationInput(); */

        ContentDocumentLink contentLink = createContentDocumentLink(qly);
        List<String> contentIds = new List<String>();
        contentIds.add((String)contentLink.ContentDocumentId);

        Test.startTest();
        Boolean result = AP_QLyManager.deleteContentDocuments(contentIds);
        System.assert(result, 'Documents have not been deleted');
    }

    @isTest
    public static void linkDocToFormTest(){
        Account account = createAccount();
        Contact contact = createContact();
        QLY_SchoolForm__c qly = createQLY(contact.Id, account.Id);
        /* AutomationInputs__c automationInput = createAutomationInput(); */

        ContentVersion content=new ContentVersion(); 
        content.Title='ContentTitle'; 
        content.PathOnClient='/' + content.Title + '.jpg'; 
        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
        content.VersionData=bodyBlob; 
        //content.LinkedEntityId=sub.id;
        content.origin = 'H';
        insert content;

        List<String> contentIds = new List<String>();
        Id contentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =: content.id].ContentDocumentId;
        contentIds.add((String)contentId);

        Test.startTest();
        List<ContentDocumentLink> result = AP_QLyManager.linkDocToForm(contentIds, (String)qly.Id);
        System.assert(result.size() > 0, 'Links have not been updated');
    }

    @isTest
    public static void getApprovStatusTest(){
        Account account = createAccount();
        Contact contact = createContact();
        QLY_SchoolForm__c qly = createQLY(contact.Id, account.Id);
        AutomationInputs__c automationInput = createAutomationInput();
        List<QLY_SchoolForm__c> result = AP_QLyManager.getApprovStatus((String)account.Id);
        System.assert(result.size() > 0, 'getApprovStatus returned no result');
    }
    
    @isTest
    public static void otherTests() {
        List < Map < String, String >> pickvals = AP_QLyManager.getPiklistValues('QLY_SchoolForm__c', 'FormStatus__c');
        System.assertNotEquals(0, pickvals.size());
    }


    @isTest
    public static void  isCampaignOverTest(){
        Test.startTest(); 
        
        //Create campaign
        Campaign campaign = createCampaign();
        
        //call method
		Boolean vResult = AP_QLyManager.isCampaignOver(campaign.Id);
        
         //check the result
         System.debug('isCampaignOverTest.vResult' + vResult);  
        System.assertEquals(false, vResult);

        Test.stopTest();
    }
}