public with sharing class LC_AvenantsList_AugTarifCtrl {
    /*@AuraEnabled
    public  string message {get;set;}
    @AuraEnabled       
    public  string toastMode {get;set;}
    
    @AuraEnabled
    public static LC_AvenantsList_AugTarifCtrl submitAMEForValidation(list<Contract__c> contractsList){       
        return AP_AugmentationTarifManager.submitAMEForValidation(contractsList);
    }

    @AuraEnabled
    public static String increaseAMEByPercentage(List<Amendment__c> lstAmend, Double inputValue,Double inputValueTraitement,
                              Double inputValueTransport,Double inputValueConditionnement, String RoundingType){
        return AP_AugmentationTarifManager.increaseValues(lstAmend, inputValue, inputValueTraitement, inputValueTransport, inputValueConditionnement, RoundingType);        
    }

    @AuraEnabled
    public static LC_AvenantsList_AugTarifCtrl increaseAMEFaf(list<Amendment__c> amendmentList){   
        return AP_AugmentationTarifManager.increaseAMEFaf(amendmentList);
    }

    @AuraEnabled
    public static LC_AvenantsList_AugTarifCtrl increaseAMEFixedPrice(list<Amendment__c> amendmentList, Double inputValue,Double inputValueTraitement,
                              Double inputValueTransport,Double inputValueConditionnement ){   
        return AP_AugmentationTarifManager.increaseAMEFixedPrice(amendmentList,  inputValue, inputValueTraitement, inputValueTransport, inputValueConditionnement);
    }

    @AuraEnabled
    public static LC_AvenantsList_AugTarifCtrl increaseAMEByValue(list<Amendment__c> amendmentList, Double inputValue,Double inputValueTraitement,
                              Double inputValueTransport,Double inputValueConditionnement ){   
        return AP_AugmentationTarifManager.increaseAMEByValue(amendmentList,  inputValue, inputValueTraitement, inputValueTransport, inputValueConditionnement);
    }

    @AuraEnabled
    public static LC_AvenantsList_AugTarifCtrl dontIncreaseAME(list<Amendment__c> amendmentList){   
        return AP_AugmentationTarifManager.dontIncreaseAME(amendmentList);
    }

    @AuraEnabled
    public static LC_AvenantsList_AugTarifCtrl regroupContractsFromAME(list<Amendment__c> amendmentList){   
        return AP_AugmentationTarifManager.regroupContractsFromAME(amendmentList);
    } */

    @AuraEnabled
  //Method to update a list of contracts
    public static  List<Amendment__c>  getAmendmentById(List<String> listIds ){
        return AP_AugmentationTarifManager.getAvenantsById(listIds);
    }

    @AuraEnabled
    public static Boolean doAmendmentsHaveNextYearPrice(List<Amendment__c> listAmendments) {
        return AP_AugmentationTarifManager.doAmendmentsHaveNextYearPrice(listAmendments);
    }

    @AuraEnabled
    public static List<Object> getDefinitions(){
          List<String> fields_lst = new List<String>(Schema.getGlobalDescribe().get('cDef__mdt').getDescribe().fields.getMap().keySet());
          List<cDef__mdt> defs = Database.query('SELECT '+String.join(fields_lst, ', ')+' FROM cDef__mdt');

          return new List<Object>{getColumnsLabels(), defs};
      }
    
    public static List<Custom_Column__mdt> getColumnsLabels(){
        return [SELECT Label, Filiale__c, Filiere__c, Columns_Contracts__c, Column_Avenant__c FROM Custom_Column__mdt];
    }
  
}