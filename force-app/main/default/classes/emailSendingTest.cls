@isTest
public class emailSendingTest {
    @isTest static void test(){
        
        System.debug('emailSendingTest START' );
        Test.StartTest();
        

            Account a = new Account();
            a.Name = 'testAccName';
            a.CustomerNature__c = 'Administration';
            a.Industry = 'Trader';
            a.CurrencyIsoCode = 'EUR';
            a.Producteur__c = false;
            a.BillingPostalCode = '01000';
            a.NAF_Number__c = '1234A';
            insert a;
            
            Contact c = new Contact();
            c.LastName = 'testName';
            c.AccountId = a.Id;
            //c.Salesman__c = director.Id;
            c.Email = 'aaa@groupe-seche.com';
            c.Phone = '03 00 00 00 00';
            insert c;
            
            Opportunity opp1 = new Opportunity();
            opp1.Name = 'test';
            //opp.Salesman__c = director.Id;
            opp1.StageName = 'Contractualisation';
            opp1.CloseDate = System.today().addDays(10);
            opp1.AccountId = a.Id;
            opp1.TonnageN__c = 0;
            opp1.Filiale__c = 'Tredi Hombourg';
            opp1.Filiere__c = 'Physico';
            opp1.Amount = 100000;
            opp1.ContactName__c = c.Id;
            insert opp1;
            
            Quote quo1 = new Quote();
            quo1.OpportunityId = opp1.ID;
            quo1.name = 'Mon Devis Test';
            insert quo1;

        
         PageReference pgNouveauCRContact = Page.emailSending;
        
        // Set the currentPage to the test
        Test.setCurrentPage(pgNouveauCRContact);
        //Set parameter 
        pgNouveauCRContact.getParameters().put('ID', quo1.Id );
        ApexPages.StandardController stdController = new ApexPages.StandardController(quo1);

    //    emailSending vEmailSend = new emailSending(stdController);
     //   emailSending.sendMail(); 

        Test.StopTest();
        
        System.debug('emailSendingTest END' );
    }
    
}