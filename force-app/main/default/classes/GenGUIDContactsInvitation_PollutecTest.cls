@isTest
public with sharing class GenGUIDContactsInvitation_PollutecTest {

    @isTest
    public static void genUIDTest() {

        Contacts_Invitation__c conInv = new Contacts_Invitation__c(Status__c = 'Contact à nettoyer');
        insert conInv;
        List<Id> conInvIds = new List<Id>();
        conInvIds.add(conInv.Id);

        GenGUIDContactsInvitation_Pollutec.genGuid(conInvIds);
    }
}